<?php

$modulename = $infos['nameofmodule'];
$levelarray = '';
foreach($levels as $level) $levelarray .= ($levelarray == ''?'':',').'"'.$level[0].'"';

$dbget = '
	function get_levelarray(){
		// returns an array of the levels (top to bottom)
		return array('.$levelarray.');
	}

	function get_modulehierarchy($level=false){
		$hierarchy = array();';
	$i = 0;
	foreach($levels as $level){
		$i++;
		$dbget .= '
			$hierarchy['.$i.'] = "'.$level[0].'";';
	}
	$dbget .= '
		return $hierarchy;
	}
';

if($infos['templatelevel'] != 0 && $infos['templatelevel'] != $infos['nblevels']){
	$dbget .= '
	function get_finaltemplate($currenttable, $parentid){
		// goes from parent to parent until the template level is reached, and return the according template
		$db =& $this->GetDb();
		$templatelevel = '.$infos['templatelevel'].';
		$hierarchy = $this->get_modulehierarchy();
		$currentlevel = count($hierarchy) - 1;
		while($currentlevel > $templatelevel){
			$query = "SELECT parent FROM ".cms_db_prefix()."module_'.$modulename.'_".$hierarchy[$currentlevel]." WHERE id=?";
			$dbresult = $db->Execute($query,array($parentid));
			if ($dbresult && $row = $dbresult->FetchRow()){
				$parentid = $row["parent"];
			}else{
				return false;
			}
			$currentlevel = $currentlevel - 1;
		}
		$query = "SELECT template FROM ".cms_db_prefix()."module_'.$modulename.'_".$hierarchy[$currentlevel]." WHERE id=?";
		$dbresult = $db->Execute($query,array($parentid));
		if ($dbresult && $row = $dbresult->FetchRow()){
			return $row["template"];
		}else{
			return false;
		}
	}
';
}

$dbget .= '

	function get_moduleGetVars(){
		// unorthodox hack so that different calls of the module speak with each other
		global $_GET;
		$globalmodulevars = array();
		if(isset($_GET["mact"])) $modinfo = explode(",",$_GET["mact"]);
		if(isset($modinfo) && $modinfo[0] == "'.$modulename.'"){
			if(isset($_GET[$modinfo[1]."parent"])) $globalmodulevars["parent"]=$_GET[$modinfo[1]."parent"];
			if(isset($_GET[$modinfo[1]."what"])) $globalmodulevars["what"]=$_GET[$modinfo[1]."what"];
			if(isset($_GET[$modinfo[1]."alias"])) $globalmodulevars["alias"]=$_GET[$modinfo[1]."alias"];
			if(isset($_GET[$modinfo[1]."finalalias"])) $globalmodulevars["finalalias"]=$_GET[$modinfo[1]."finalalias"];
			if(isset($_GET[$modinfo[1]."pageindex"])) $globalmodulevars["pageindex"]=$_GET[$modinfo[1]."pageindex"];
		}
		return $globalmodulevars;
	}

	function get_objtree($curid, $curlevel="'.$levels[count($levels) -1][0].'", $field="id"){
		// this builds an object tree ($item->parent_object->parent_object...)
		// we first put all the parents in an array
		$parents = array();
		$levels = $this->get_levelarray();
		$i = count($levels);
		$started = false;
		while($curid && $i > 0){
			if($levels[$i -1] == $curlevel)	$started = true;
			if($started){
				$getfunction = "get_level_".$levels[$i -1];
				$item = $this->$getfunction(array($field=>$curid));
				$item = is_array($item)?$item[0]:$item;
				$parents[] = $item;
				$field = "id";
				$curid = isset($item->parent_id)?$item->parent_id:false;
			}
			$i--;
		}
		
		// next, we process the array of parents to build the parent tree
		$parenttree = false;
		$i = count($parents) - 1;
		while($i >= 0){
			if($parenttree){
				$newtree = $parents[$i];
				$newtree->parent_object = $parenttree;
				$parenttree = $newtree;
			}else{
				$parenttree = $parents[$i];
			}
			$i--;
		}
		return $parenttree;
	}

/* Has been replaced by the function below, which uses the object tree
	function get_module_breadcrumbs($params, $id, $returnid){
		$parentid = $params["parent"];
		$db =& $this->GetDb();
		$hierarchy = $this->get_modulehierarchy();
		$distance = $this->get_distancetolevel($params["what"]);
		$currentlevel = count($hierarchy) - $distance - 1;
		$links = array();
		while($currentlevel > 0){
			// we go through each level, from bottom to top, following the parent lead
			$query = "SELECT alias, name".($currentlevel != 1?", parent":"")." FROM ".cms_db_prefix()."module_'.$modulename.'_".$hierarchy[$currentlevel]." WHERE ".((count($links)>0)?"id":"alias")."=?";
			$dbresult = $db->Execute($query,array($parentid));
			if ($dbresult && $row = $dbresult->FetchRow()){
				if(isset($row["parent"])) $parentid = $row["parent"];
				$links[$currentlevel] = array($hierarchy[$currentlevel+1],$row["alias"],$row["name"]);
			}else{
				$currentlevel = 0;
			}
			$currentlevel--;
		}
		// once we\'ve got all parents, we do the breadcrumbs
		$output = "";
		$parents = array();
		$delimiter = $this->Lang("breadcrumbs_delimiter");
		if(isset($params["alias"])) unset($params["alias"]);
		foreach($links as $link){
			$parents[$link[0]] = $link[1];
			$params["what"] = $link[0];
			$params["parent"] = $link[1];
			$prettyurl = $this->BuildPrettyUrls($params, $returnid);
			$output = "<span class=\"modbreadcrumbs\">".$this->CreateLink($id, "default", $returnid, $link[2], $params, "", false, false, "", false, $prettyurl)."</span>".($output == ""?"":$delimiter.$output);
		}
		// we return an array :
		// 1 => the array of parents, as we might need it for some special cases
		// 2 => the breadcrumbs as a string
		return array($parents,(($output != "")?$this->Lang("youarehere"):"").$output);
	}
*/
	function get_module_breadcrumbs($params, $id, $returnid, $item, $skip=false){
		$what = isset($params["what"])?$params["what"]:"'.$levels[count($levels) -1][0].'";
		if($skip) $what = $this->get_nextlevel($what, false);
		$delimiter = $this->Lang("breadcrumbs_delimiter");
		$newparams = array();
		if(isset($params["forcelist"]) && $params["forcelist"])	$newparams["forcelist"] = 1;
		if(isset($params["inline"]) && $params["inline"])	$newparams["inline"] = 1;

		$crumbs = "<span class=\"lastitem\">".$item->name."</span>";
		while(isset($item->parent_object->name)){
			$item = $item->parent_object;
			$newparams["what"] = $what;
			$what = $this->get_nextlevel($what, false);
			$newparams["parent"] = $item->alias;
			$prettyurl = $this->BuildPrettyUrls($newparams, $returnid);
			$crumbs = "<span>".$this->CreateLink($id, "default", $returnid, $item->name, $newparams, "", false, false, "", false, $prettyurl)."</span>".$delimiter.$crumbs;
		}

		return $crumbs;		
	}
';
/* TEST FOR ADVANCED BREADCRUMBS:
 * This test has met some problems which I don't have time to correct right now...
	function get_module_breadcrumbs($parms=array(), $params=false, $id, $returnid){
		// This function is rather twisted... it is called as to make the tag in the default action, or by the breadcrumbs action
		// First, we check if we have enough informations to create the breadcrumbs. If called as an independant action and we aren\'t in a module link, we can\'t
		if(!$params || count($params) == 0)	$params = $this->get_moduleGetVars();
		if( (!isset($params["parent"]) && !isset($params["alias"]))	|| !isset($params["what"]) )	return false;
		if(isset($params["parent"])){
			$parentid = $params["parent"];
			$wantedfield = "alias";
		}
		if(isset($params["alias"])){
			$getfunction = "get_level_".$params["what"];
			$item = $this->$getfunction(array("alias"=>$params["alias"]));
			if(!isset($parentid)){
				if(isset($item[0]))		$parentid = $item[0]->parent_id;
				$wantedfield = "id";
			}
		}
		if(!isset($parentid))	return false;
		if(isset($params["alias"])) unset($params["alias"]);
		
		// we have enough info... we process the function parameters:
		$classid = isset($parms["classid"])?" class=\"".$parms["classid"]."\"":"";
		$currentclassid = isset($parms["currentclassid"])?" class=\"".$parms["currentclassid"]."\"":" class=\"lastitem\"";
		$startlevel = isset($parms["startlevel"])?$parms["startlevel"]:false;
		$delimiter = isset($parms["delimiter"])?$parms["delimiter"]:" &gt;";
		$initial = isset($parms["initial"])?$parms["initial"]:$this->Lang("youarehere");

		// we create the breadcrumbs		
		$db =& $this->GetDb();
		$hierarchy = $this->get_modulehierarchy();
		$distance = $this->get_distancetolevel($params["what"]);
		$currentlevel = count($hierarchy) - $distance - 1;
		$links = array();
		$startlevelreached = false;
		while($currentlevel > 0){
			$query = "SELECT alias, name".($currentlevel != 1?", parent":"")." FROM ".cms_db_prefix()."module_'.$modulename.'_".$hierarchy[$currentlevel]." WHERE ".$wantedfield."=?";
			$dbresult = $db->Execute($query,array($parentid));
			if ($dbresult && $row = $dbresult->FetchRow()){
				if(isset($row["parent"])) $parentid = $row["parent"];
				if($hierarchy[$currentlevel] == $startlevel)	$startlevelreached = true;
				if(!$startlevelreached)	$links[$currentlevel] = array($hierarchy[$currentlevel+1],$row["alias"],$row["name"]);
			}else{
				$currentlevel = 0;
			}
			$wantedfield = "id";
			$currentlevel--;
		}
		$output = "";
		$i = 0;
		while($i < count($links)){
			$link = $links[$i];
			$params["what"] = $link[0];
			$params["parent"] = $link[1];
			$prettyurl = $this->BuildPrettyUrls($params, $returnid);
			$tmpclass = (!isset($item[0]) && $i == (count($links) - 1))?$currentclassid:$classid;
			$output = "<span".$tmpclass."/>".$this->CreateLink($id, "default", $returnid, $link[2], $params, "", false, false, "", false, $prettyurl)."</span>".($output == ""?"":$delimiter." ".$output);
			$i++;
		}
		$output = $initial.$output;
		if(isset($item[0]))	$output .= $delimiter."<span".$currentclassid."><a>".$item[0]->name."</a></span>";
		return $output;
	}
*/

$dbget .= '

	function countsomething($tablename,$what="id",$where=array()){
		// returns the number of elements in a table
		$db =& $this->GetDb();
		$wherestring = "";
		$wherevalues = array();
		foreach($where as $key=>$value){
			$wherestring .= ($wherestring == ""?" WHERE ":" AND ").$key."=?";
			$wherevalues[] = $value;
		}
		$query = "SELECT COUNT($what) ourcount FROM ".cms_db_prefix()."module_'.$modulename.'_$tablename".$wherestring;
		$dbresult = $db->Execute($query,$wherevalues);
		if ($dbresult && $row = $dbresult->FetchRow()){
			return $row["ourcount"];
		}else{
			return 0;
		}
	}
';

$dbget .= '
	function get_distancetolevel($parentname,$childname="'.$levels[count($levels) -1][0].'"){
		// get the distance between two levels (most likely between a level and the final level)
		$levels = $this->get_levelarray();
		$parentposition = false;
		$childposition = false;
		$counter = 0;
		foreach($levels as $level){
			$counter++;
			if($level == $parentname) $parentposition = $counter;
			if($level == $childname) $childposition = $counter;
		}
		if($childposition && $parentposition){
			return abs($parentposition - $childposition);
		}
	}
	function get_nextlevel($curlevel,$findchild=true){
		// return the name of the level below ($findchild=true) or above ($findchild=false)
		$levels = $this->get_levelarray();
		$i = 0;
		$wantedlevel = false;
		while($i < count($levels)){
			$next = $findchild?$i+1:$i-1;
			if($levels[$i] == $curlevel && isset($levels[$next])) $wantedlevel = $levels[$next];
			$i++;
		}
		return $wantedlevel;
	}

	function get_parents($parentname,$childname,$childid){
		// function with the would-be "sharechildren" option...
		$db =& $this->GetDb();
		$query = "SELECT ".$parentname."_id parentid FROM ".cms_db_prefix()."module_'.$modulename.'_".$parentname."_has_".$childname." WHERE ".$childname."_id=?";
		$dbresult = $db->Execute($query,array($childid));
		$parents = array();
		while ($dbresult && $row = $dbresult->FetchRow()){
			$parents[] = $row["parentid"];
		}
		return $parents;
	}

	function get_options($tablename){
		// returns the options from a predefined list
		$db =& $this->GetDb();
		$query = "SELECT * FROM ".cms_db_prefix()."module_'.$modulename.'_".$tablename;
		$dbresult = $db->Execute($query);
		$options = array();
		while ($dbresult && $row = $dbresult->FetchRow()){
			$options[$row["name"]] = $row["id"];
		}
		return $options;
	}

	function get_id2namearray($tablename){
		// returns an $id=>$name array of any table, for use with dropdown lists
		$db =& $this->GetDb();
		$query = "SELECT * FROM ".cms_db_prefix()."module_'.$modulename.'_".$tablename;
		$dbresult = $db->Execute($query);
		$id2name = array();
		while ($dbresult && $row = $dbresult->FetchRow()){
			$id2name[$row["id"]] = $row["name"];
		}
		return $id2name;
	}

	function get_pageid($alias){
		// returns the page id from an alias
		global $gCms;
		$manager =& $gCms->GetHierarchyManager();
		$node =& $manager->sureGetNodeByAlias($alias);
		if (isset($node)) {
			$content =& $node->GetContent();	
			if (isset($content))	return $content->Id();
		}else{
			$node =& $manager->sureGetNodeById($alias);
			if (isset($node)) return $alias;
		}
	}

	function addadminlinks($item,$prefix,$tablename,$child=false,$levelname,$id,$returnid,$parentdefault=false,$orderbyparent=false,$addfiles=""){
		// add the admin links to the level items
		global $gCms;
		$admintheme = $gCms->variables["admintheme"];

		$moveparams = array($prefix."id"=>$item->id, "prefix"=>$prefix, "currentorder"=>$item->item_order, "tablename"=>$tablename, "active_tab"=>$levelname);
		if($child) $moveparams["child"] = $child;
		if($orderbyparent && isset($item->parent_id)) $moveparams["parent"] = $item->parent_id;

		$item->editlink = $this->CreateLink($id, "edit".$prefix, $returnid, $item->name, array($prefix."id"=>$item->id));
		$item->deletelink = $this->CreateLink($id, "movesomething", $returnid, $admintheme->DisplayImage("icons/system/delete.gif",$this->Lang("delete"),"","","systemicon"), array_merge(array("move"=>"delete", "addfiles"=>$addfiles),$moveparams),$this->Lang("promt_delete".$levelname, $item->name));
		$item->moveuplink = $this->CreateLink($id, "movesomething", $returnid, $admintheme->DisplayImage("icons/system/arrow-u.gif",$this->Lang("up"),"","","systemicon"), array_merge(array("move"=>"up"),$moveparams));
		$item->movedownlink = $this->CreateLink($id, "movesomething", $returnid, $admintheme->DisplayImage("icons/system/arrow-d.gif",$this->Lang("down"),"","","systemicon"), array_merge(array("move"=>"down"),$moveparams));
		$item->movelinks = $item->moveuplink ." ". $item->movedownlink;
		if ($item->active == 1){
			$item->toggleactive = $this->CreateLink($id, "toggle", $returnid, $admintheme->DisplayImage("icons/system/true.gif",$this->Lang("setfalse"),"","","systemicon"), array_merge(array("what"=>"active","newval"=>0),$moveparams));
		}else{
			$item->toggleactive = $this->CreateLink($id, "toggle", $returnid, $admintheme->DisplayImage("icons/system/false.gif",$this->Lang("settrue"),"","","systemicon"), array_merge(array("what"=>"active","newval"=>1),$moveparams));
		}
		$defaultparams = array($prefix."id"=>$item->id, "prefix"=>$prefix, "tablename"=>$tablename, "active_tab"=>$levelname, "what"=>"default");
		if($parentdefault && isset($item->parent_id)){
			$defaultparams["parent"] = $item->parent_id;
			$defaultparams["byparent"] = 1;
		}
		if ($item->isdefault == 1){
			$item->toggledefault = $this->CreateLink($id, "toggle", $returnid, $admintheme->DisplayImage("icons/system/true.gif",$this->lang("setfalse"),"","","systemicon"), $defaultparams);
		}else{
			$item->toggledefault = $this->CreateLink($id, "toggle", $returnid, $admintheme->DisplayImage("icons/system/false.gif",$this->lang("settrue"),"","","systemicon"), array_merge(array("newval"=>1),$defaultparams));
		}
		return $item;
	}
';

$pdoptionsarr = array();
$pdinversearr = array();
foreach($predefinedlists as $list){
	// 	$list[] = array(levelprefix, fieldname, listoptions)
	$pdoptions = '';
	$pdinverse = '';
	$i = 0;
	$list[2] = explode(',',$list[2]);
	foreach($list[2] as $option){
		$pdoptions .= ($pdoptions == ''?'':', ').'$this->Lang("'.$list[0].'_'.$list[1].'_option_'.$i.'")=>"'.$i.'"';
		$pdinverse .= ($pdinverse == ''?'':', ').'"'.$i.'"=>$this->Lang("'.$list[0].'_'.$list[1].'_option_'.$i.'")';
		$i++;
	}
	$pdoptionsarr[$list[0].'_'.$list[1]] = $pdoptions;
	$pdinversearr[$list[0].'_'.$list[1]] = $pdinverse;
}

if(count($pdinversearr) > 0){
$dbget .= '
	function get_predefinedoptions($field,$retrieve=false){
		// return the options for a predefined list
		if($retrieve){
			switch($field){';
	foreach($pdinversearr as $key=>$value){
		$dbget .= '
				case "'.$key.'":
					return array('.$value.');
					break;';
	}
$dbget .= '
			}
		}else{
			switch($field){';
	foreach($pdoptionsarr as $key=>$value){
		$dbget .= '
				case "'.$key.'":
					return array('.$value.');
					break;';
	}
$dbget .= '
			}
		}
	}
';
}

if(count($multiplefiles) > 0){
$dbget .= '
	function getaddfiles($fieldname,$itemid,$returnobj=true){
		// return the filepaths for fields of undefined amount of files
		$db =& $this->GetDb();
		$query = "SELECT fileid, filepath FROM ".cms_db_prefix()."module_'.$modulename.'_multiplefilesfields WHERE itemid=? AND fieldname=?";
		$dbresult = $db->Execute($query, array( $itemid, $fieldname ));
		$files = array();
		if($returnobj){
		while ($dbresult && $row = $dbresult->FetchRow()){
			$file = new stdClass();
			$file->fileid = $row["fileid"];
			$file->filepath = $row["filepath"];
			array_push($files, $file);
		}
		}else{
			while ($dbresult && $row = $dbresult->FetchRow()){
				array_push($files, $row["filepath"]);
			}
		}
		return $files;

	}
';
}

require 'function.createLevelGetFunctions.php';

?>
