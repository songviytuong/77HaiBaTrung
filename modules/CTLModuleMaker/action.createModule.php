<?php
	if(!isset($gCms)) exit;
	if(!isset($params['infos']) || !isset($params['levels'])) exit;

	if($config['debug'] && $this->getInnerDebug()) error_reporting(E_ALL);

	// we don't know exactly from which step we're coming
	$infos = $params["infos"];
	$levels = $params["levels"];
	if(!is_array($infos)) $infos = unserialize($infos);
	if(!is_array($levels)) $levels = unserialize($levels);
	
	if(!isset($infos['nameofmodule'])) $infos['nameofmodule'] = $infos['name'];
	
	$receivedparams = array("infos"=>$infos, "levels"=>$levels);

	// ADDING THE TEMPLATE FIELD TO THE APPROPRIATE LEVEL
	if(!isset($infos['templatelevel'])){
		$infos['templatelevel'] = isset($params['templatelevel'])?$params['templatelevel']:0;
		if($infos['templatelevel'] != 0)	$levels[$infos['templatelevel'] - 1][4][] = array('template',5,0,0,'');
	}
	
	if(!isset($infos['version']))	$infos['version'] = '1.0';
	
	// We keep values here for eventual export
	$params = array();
	$params['levels'] = $levels;
	$params['infos'] = $infos;
	
	if($this->GetPreference('autosave',true)){
		// Auto-backup:
		$output = array('infos' => $params['infos'], 'levels' => $params['levels']);
		$this->CreateFile(dirname(__FILE__).DIRECTORY_SEPARATOR.'autosaved'.DIRECTORY_SEPARATOR.(isset($output['infos']['nameofmodule'])?$output['infos']['nameofmodule'].'_':'').'auto_export.dna',serialize($output));
	}
	
echo '<h1>'.$this->GetFriendlyName().' - '.$this->Lang('creatingfiles').'</h1><br/><p>'.$this->Lang('warning_takesometime').'</p>';
//ini_set ('max_execution_time', 300);


	// PARSING FIELDS
	$linktables = array();
	$filefields = array();
	$listfields = array();
	$predefinedlists = array();
	$multiplefiles = array();
	$uploaddirs = array();
	$i = 0;
	while($i < count($levels)){
		$levels[$i][5] = explode(',',$levels[$i][5]);
		if(isset($levels[$i - 1]) && $levels[$i - 1][2] == 0){
			$levels[$i][4][] = array('parent',0,0,0,'');
		}elseif(isset($levels[$i - 1]) && $levels[$i - 1][2] == 1){
			$linktables[] = array($levels[$i - 1][0],$levels[$i][0]);
		}
		foreach($levels[$i][4] as $onefield){
			if(!empty($onefield[5]['listoptions']) && is_array($onefield[5]['listoptions'])) $onefield[5]['listoptions'] = explode(',',$onefield[5]['listoptions']);
			if($onefield[1] == 8 || $onefield[1] == 9) {
				if(!isset($filefields[$levels[$i][0]])) $filefields[$levels[$i][0]] = array();
				$filefields[$levels[$i][0]][] = array($levels[$i][1],$onefield[0],($onefield[1] == 8?'images':'files'),$onefield[5]['upfolder'],$onefield[5]['size'],$onefield[5]['thumb']);
				if(isset($onefield[5]['upfolder']) && $onefield[5]['upfolder'] == '0' ) $onefield[5]['upfolder'] = '';
				if(isset($onefield[5]['upfolder']) && $onefield[5]['upfolder'] != '') $uploaddirs[] = $onefield[5]['upfolder'];
			}elseif($onefield[1] == 6){
				$predefinedlists[] = array($levels[$i][0],$onefield[0],$onefield[5]['listoptions']);
			}elseif($onefield[1] == 7){
				$listfields[] = array($levels[$i][1],$onefield[0]);
			}elseif($onefield[1] == 11){
				if($onefield[5]['upfolder'] == '0' ) $onefield[5]['upfolder'] = '';
				if(isset($onefield[5]['upfolder']) && $onefield[5]['upfolder'] != '') $uploaddirs[] = $onefield[5]['upfolder'];
				$multiplefiles = array($levels[$i][0],$onefield[0]);
			}
			if( ($onefield[1] == 6 || $onefield[1] == 7) && !isset($onefield[5]['listmode']) ){
				// dna comes from an older version without list modes
				if(isset($onefield[5]['multiple']) && $onefield[5]['multiple'] != 0){
					$onefield[5]['listmode'] = 2;
				}else{
					$onefield[5]['listmode'] = 1;
				}
			}
		}
		// Adding default fields
		$levels[$i][4][] = array('id',0,0,0,'');
		$levels[$i][4][] = array('name',3,1,1,'');
		$levels[$i][4][] = array('alias',3,0,0,'');
		$levels[$i][4][] = array('item_order',0,0,0,'');
		$levels[$i][4][] = array('active',0,0,0,1);
		$levels[$i][4][] = array('isdefault',0,0,0,0);

		// if we come from earlier versions, we must add these values :
		if(!isset($levels[$i][6])) $levels[$i][6] = 1;
		if(!isset($levels[$i][7])) $levels[$i][7] = 0;
		foreach($levels as $level) if(!isset($level[8])) $level[8] = 0;
		
		// we cannot share children and order them by parent at the same time :
		if($levels[$i][2]){
			$levels[$i][6] = 0;
			$levels[$i][7] = 0;
		}
		$i++;

	}
	$infos['hasfilefields'] = ( count($filefields) > 0 || count($multiplefiles > 0) );


#############################################################
## CREATING FILES
##

$basedir = $this->CreateFolders($infos['nameofmodule'],$uploaddirs);
if(is_array($basedir) && count($basedir) > 0){
	$this->display_errors($basedir);
	$this->EchoExportForm($params, false, 'prompt_exportdna2', true);
}else{
	$results = array();
	echo '<br/><br/><ul>';
	$this->displayResult('',true,$what='folders');

	// Creating action.editlevel.php files...
	$parentname = false;
	$sharedbyparents = false;
	foreach($levels as $level){
		$filename = 'action.edit'.$level[1].'.php';
		$content = $this->createEditLevel($level, $infos, $parentname, $sharedbyparents);
		$results[] = $this->displayResult($filename, $this->CreateFile($basedir.$filename,$content) );

		$filename = 'templates'.DIRECTORY_SEPARATOR.'edit'.$level[1].'.tpl';
		$content = $this->createEditTemplates($level,(!$sharedbyparents && $parentname != false)?true:false);
		$results[] = $this->displayResult($filename, $this->CreateFile($basedir.$filename,$content) );

		$parentname = $level[0];
		$sharedbyparents = $level[2];
	}

	// Creating method files
	$filename = 'method.install.php';
	$content = $this->createInstall($infos['nameofmodule'], $levels, $linktables, $listfields, ($infos['templatelevel'] == 0), $multiplefiles);
	$results[] = $this->displayResult($filename, $this->CreateFile($basedir.$filename,$content) );
	$filename = 'method.uninstall.php';
	$content = $this->createUninstall($infos['nameofmodule'], $levels, $linktables, $listfields, $multiplefiles);
	$results[] = $this->displayResult($filename, $this->CreateFile($basedir.$filename,$content) );
	$filename = 'method.upgrade.php';
	$content = $this->createUpgrade($infos, $levels);
	$results[] = $this->displayResult($filename, $this->CreateFile($basedir.$filename,$content) );


	// Defaultadmin
	$filename = 'action.defaultadmin.php';
	$content = $this->createDefaultadmin($infos, $levels, $listfields);
	$results[] = $this->displayResult($filename, $this->CreateFile($basedir.$filename,$content) );

	$filename = 'action.movesomething.php';
	$content = $this->createMoveAction($infos['nameofmodule']);
	$results[] = $this->displayResult($filename, $this->CreateFile($basedir.$filename,$content) );

	// Frontend actions
	$filename = 'action.default.php';
	$content = $this->createActionDefault($infos, $levels);
	$results[] = $this->displayResult($filename, $this->CreateFile($basedir.$filename,$content) );
	
	$filename = 'action.search.php';
	$content = $this->createActionSearch($levels);
	$results[] = $this->displayResult($filename, $this->CreateFile($basedir.$filename,$content) );

	// Module File
	$filename = $infos['nameofmodule'].'.module.php';
	$content = $this->createModuleFile($infos, $levels, $predefinedlists, $multiplefiles, $this->GetVersion());
	$results[] = $this->displayResult($filename, $this->CreateFile($basedir.$filename,$content) );

	// Lang Files
	$createdlang = '';
	$dh  = opendir(dirname(__FILE__).DIRECTORY_SEPARATOR.'createdlang');
	while (false !== ($langfile = readdir($dh))){
		if(strtolower(strrchr($langfile, ".")) == '.php'){
			$thislang = substr($langfile, 0, 5);
			$filename = 'lang'.DIRECTORY_SEPARATOR.$langfile;
			$content = $this->createLangFile($infos, $levels, $thislang);
			$results[] = $this->displayResult($filename, $this->CreateFile($basedir.$filename,$content) );
			chmod($basedir.$filename, 0777);
			$createdlang .= ($createdlang == ''?'':', ').$thislang;			
		}
	}
	closedir($dh);

	// Copying files
	$filename = 'action.toggle.php';
	$content = $this->getFileContent($filename);
	$results[] = $this->displayResult($filename, $this->CreateFile($basedir.$filename,$content) );

	$filename = 'action.editTemplate.php';
	$content = $this->getFileContent($filename);
	$results[] = $this->displayResult($filename, $this->CreateFile($basedir.$filename,$content) );

	$filename = 'function.modbreadcrumbs.php';
	$content = $this->getFileContent($filename);
	$results[] = $this->displayResult($filename, $this->CreateFile($basedir.$filename,$content) );

	if($infos['hasfilefields']){
		// Copying file-related actions
		$filename = 'action.assignfile.php';
		$content = $this->getFileContent($filename);
		$results[] = $this->displayResult($filename, $this->CreateFile($basedir.$filename,$content) );

		$filename = 'action.browsefiles.php';
		$content = $this->getFileContent($filename);
		$results[] = $this->displayResult($filename, $this->CreateFile($basedir.$filename,$content) );

		$filename = 'action.uploadFile.php';
		$content = $this->getFileContent($filename);
		$results[] = $this->displayResult($filename, $this->CreateFile($basedir.$filename,$content) );
	}

	$filename = 'adminpanel.tpl';
	$content = $this->getFileContent($filename);
	$filename = 'templates'.DIRECTORY_SEPARATOR.$filename;
	$results[] = $this->displayResult($filename, $this->CreateFile($basedir.$filename,$content) );
	$filename = 'search.tpl';
	$content = $this->getFileContent($filename);
	$filename = 'templates'.DIRECTORY_SEPARATOR.$filename;
	$results[] = $this->displayResult($filename, $this->CreateFile($basedir.$filename,$content) );

echo '</ul><br/><br/>';

	// PROCESSING THE RESULT ARRAY (looking for errors)

	$errors = 0;
	foreach($results as $result){
		if(!$result)	$errors++;
	}
	if($errors == 0){
		if(isset($infos['tempfile'])){
			$filepath = dirname(__FILE__).DIRECTORY_SEPARATOR.$infos['tempfile'];
			if(file_exists($filepath))	unlink($filepath);
		}
		echo '<h2>'.$this->Lang('creationsucces_title').'</h2><p>'.$this->Lang('creationsucces1').'</p>';
		echo '<p>'.$this->CreateLink($id, 'installmod', $returnid, $this->Lang('installmodule'), array('modulename'=>$infos['nameofmodule'])).'</p>';
		echo '<p>'.$this->Lang('creationsucces2').'<br/>'.$createdlang.'<br/>';
		echo $this->Lang('creationsucces3').'modules/'.$infos['nameofmodule'].'/lang/</p>';
	}else{
		echo '<h2>'.$this->Lang('error_creation').'</h2>';
	}
	$this->EchoExportForm($params, false, 'prompt_exportdna2', true);

}

$this->getInnerDebug($receivedparams, $levels);

?>
