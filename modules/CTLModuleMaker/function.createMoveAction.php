<?php

$phpfile = '<?php
if (!isset($gCms)) exit;

$newparams = isset($params["active_tab"])?array("active_tab"=>$params["active_tab"]):array();

if (!isset($params["move"]) || !isset($params["tablename"]) || !isset($params["prefix"]) || !isset($params["currentorder"]) )
$this->Redirect($id, "defaultadmin", $returnid, $newparams);

$db =& $this->GetDb();
$dbtable = cms_db_prefix()."module_".$params["tablename"];
$order = $params["currentorder"];
$itemid = $params[$params["prefix"]."id"];
$wparent = isset($params["parent"])?" AND parent=\'".$params["parent"]."\'":"";

switch($params["move"]){
	case "delete":
		if(isset($params["child"]) && $params["child"] != ""){
			$childtable = cms_db_prefix()."module_".$params["child"];
			$query = "DELETE FROM $childtable WHERE parent = ?";
			$db->Execute($query, array($itemid));
		}
		if(isset($params["addfiles"]) && $params["addfiles"] != ""){
			$fields = explode(",",$params["addfiles"]);
			if(count($fields) > 0){
				$where = "";
				$values = array();
				foreach($fields as $field){
					$where .= ($where == ""?"":" OR ")." fieldname=?";
					$values[] = $field;
				}
				$addfilestable = cms_db_prefix()."module_'.$modulename.'_multiplefilesfields";
				$query = "DELETE FROM $addfilestable WHERE ($where) AND itemid=".$itemid;
				$db->Execute($query, $values);
			}
		}
		$query = "DELETE FROM $dbtable WHERE id = ? LIMIT 1";
		$db->Execute($query, array($itemid));

		// UPDATE THE ORDER OF THE ITEMS
		$query = "UPDATE $dbtable SET item_order=(item_order-1) WHERE item_order > ? ".$wparent;
		$db->Execute($query, array($order));

		if(isset($params["active_tab"]))	$this->SendEvent("'.$modulename.'_deleted", array("what"=>$params["active_tab"]));
		$newparams["module_message"] = $this->lang("message_deleted");
		break;	

	case "up":
		if ($order != 1){
			$query = "UPDATE $dbtable SET item_order=(item_order+1) WHERE item_order = ? $wparent LIMIT 1;";
			$db->Execute($query, array($order-1));
			$query = "UPDATE $dbtable SET item_order=(item_order-1) WHERE id = ? LIMIT 1;";
			$db->Execute($query, array($itemid));
		}
		break;
	case "down":
		$query = "UPDATE $dbtable SET item_order=(item_order-1) WHERE item_order = ? $wparent LIMIT 1;";
		$db->Execute($query, array($order+1));
		if (mysql_affected_rows()){
			$query = "UPDATE $dbtable SET item_order=(item_order+1) WHERE id = ? LIMIT 1;";
			$db->Execute($query, array($itemid));
		}
		break;
}

$this->Redirect($id, "defaultadmin", $returnid, $newparams);
?>';

?>
