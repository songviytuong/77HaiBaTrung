<?php
if(!isset($old) || !isset($new))	exit;

$oldfields = array();
$newfields = array();
$created_fields = array();
$deleted_fields = array();
$modified_fields = array();

foreach($new[4] as $field)	$newfields[$field[0]] = $field;
foreach($old[4] as $field){
	$oldfields[$field[0]] = $field;
	if(!isset($newfields[$field[0]]))	$deleted_fields[] = $field;
}
foreach($new[4] as $newfield){
	if(isset($oldfields[$newfield[0]])){
		// field already existed
		$oldfield = $oldfields[$newfield[0]];
		$oldtype = $this->GetDBFieldType($oldfield[1], $oldfield[5]['listmode']);
		$newtype = $this->GetDBFieldType($newfield[1], $newfield[5]['listmode']);
		if($oldtype != $newtype){
			$modified_fields[] = $newfield;
		}elseif($newfield[1] == 7 && $oldfield[1] != $newfield[1]){
			$queries[] = array($new[0], 'List', $field[0]);
		}
	}else{
		$created_fields[] = $newfield;
		if($newfield[1] == 7)	$queries[] = array($new[0], 'List', $field[0]);
	}
}

$queries = array();
foreach($modified_fields as $field){
	$newtype = $this->GetDBFieldType($field[1], $field[5]['listmode']);
	$queries[] = array($new[0], 'Alter', $field[0],$newtype);
}
foreach($deleted_fields as $field){
	$newtype = $this->GetDBFieldType($field[1], $field[5]['listmode']);
	$queries[] = array($new[0], 'Drop', $field[0],$newtype);
}
foreach($created_fields as $field){
	$newtype = $this->GetDBFieldType($field[1], $field[5]['listmode']);
	$queries[] = array($new[0], 'Add', $field[0],$newtype);
}

$changes = array(count($created_fields), count($modified_fields), count($deleted_fields));

?>
