<?php
if(!isset($infos))	exit;

$modulename = $infos["nameofmodule"];

$method = '<?php
if (!isset($gCms)) exit;

$db =& $this->GetDb();

// This does the upgrade for the CTLModuleMaker part
// Unfortunately, this is only backward compatible to CTLModuleMaker 1.6.3. I couldn\'t go any further behind because at that time the maker version wasn\'t saved anywhere

$oldmaker = $this->GetPreference("makerversion", "veryold");

switch($oldmaker){
	// BEGIN SWITCH($oldmaker)

	case "veryold":
		// the module was created with version 1.6.3 or prior.
		$this->CreateEvent("'.$modulename.'_added");
		$this->CreateEvent("'.$modulename.'_modified");
		$this->CreateEvent("'.$modulename.'_deleted");
		break;


	// END SWITCH($oldmaker)
}

$this->SetPreference("makerversion", "'.$this->GetVersion().'");
';

if(isset($infos['upgradequeries']) && count($infos['upgradequeries']) > 0){
	$method .= '
// ########################################################################
// This does the upgrade for field changes
// This is written automatically... so it\'s a mess.
// YOU SHOULD CHECK THIS CODE BEFORE USING IT!

if(!isset($current_version)) $current_version = $oldversion;

if($db->dbtype == "mysql" || $db->dbtype == "mysqli"){
	// BEGIN OF UPGRADE CODE FOR MYSQL...
switch($current_version){
';

	foreach($infos['upgradequeries'] as $version=>$queries){
		$method .= '	case "'.$version.'":
			$queries = array();
			';
		foreach($queries as $pre){
			$tablename = 'module_'.$infos['nameofmodule'].'_'.$pre[0];
			switch($pre[1]){
				case 'Add':
					$method .= '$queries[] = "ALTER TABLE ".cms_db_prefix()."'.$tablename.' ADD COLUMN '.$pre[2].' '.$pre[3].'";
			';
					break;
				case 'Alter':
					$method .= '$queries[] = "ALTER TABLE ".cms_db_prefix()."'.$tablename.' ALTER COLUMN '.$pre[2].' '.$pre[2].' '.$pre[3].'";
			';
					break;
				case 'Drop':
					$method .= '$queries[] = "ALTER TABLE ".cms_db_prefix()."'.$tablename.' DROP COLUMN '.$pre[2].'";
			';
					break;
				case 'List':
				
$method .= '
// Creates the '.$pre[1].' options table
$flds = "
	id I,
	name C(32),
	item_order I
	";

$sqlarray = $dict->CreateTableSQL(cms_db_prefix()."module_'.$infos['nameofmodule'].'_'.$pre[0].'_'.$pre[1].'_options", $flds, $tabopt);
$dict->ExecuteSQLArray($sqlarray);
$db->CreateSequence(cms_db_prefix()."module_'.$infos['nameofmodule'].'_'.$pre[0].'_'.$pre[1].'_options_seq");

';					
					break;
			}
		}
		$method .= 'foreach($queries as $query)		mysql_query($query);
			';
	}
		
$method .= '
	// end switch
	}
}else{
	// BEGIN OF UPGRADE CODE FOR NON-MYSQL (using ADODB, which may likely not be fully supported)
$dict = NewDataDictionary($db);
$tabopt = array("mysql" => "TYPE=MyISAM");
switch($current_version){
	// BEGIN SWITCH($current_version)
';

	foreach($infos['upgradequeries'] as $version=>$queries){
		$method .= '	case "'.$version.'":
		';
		foreach($queries as $pre){
			$tablename = 'module_'.$infos['nameofmodule'].'_'.$pre[0];
			switch($pre[1]){
				case 'Add':
					$method .= '
			$dict->AddColumnSQL(cms_db_prefix()."'.$tablename.'", "'.$pre[2].' '.$pre[3].'");';
					break;
				case 'Alter':
				case 'Drop':
				
$ourlevel = false;
foreach($levels as $level) if($level[0] == $pre[0]) $ourlevel = $level;
$method .= '
		$flds = "';
$firstfield = true;
foreach($ourlevel[4] as $field){
	$fieldtype = $this->GetDBFieldType($field[1], $field[5]['listmode']);
	if($field[0] == "id") $fieldtype = "I KEY";
	if($field[0] == "active" || $field[0] == "isdefault") $fieldtype = "L";
	if($field[1] != 11){
		$method .= ($firstfield?"":",").'
		'.$field[0].' '.$fieldtype;
		$firstfield = false;
	}
}
$method .= ',
 	   date_modified ".CMS_ADODB_DT."
		";

		$sqlarray = $dict->'.$pre[1].'ColumnSQL(cms_db_prefix()."'.$tablename.'", "'.$pre[2].' '.$pre[3].'", $flds, $tabopt);';
					break;
				case 'List':
				
$method .= '
// Creates the '.$pre[1].' options table
$flds = "
	id I,
	name C(32),
	item_order I
	";

$sqlarray = $dict->CreateTableSQL(cms_db_prefix()."module_'.$infos['nameofmodule'].'_'.$pre[0].'_'.$pre[1].'_options", $flds, $tabopt);
$dict->ExecuteSQLArray($sqlarray);
$db->CreateSequence(cms_db_prefix()."module_'.$infos['nameofmodule'].'_'.$pre[0].'_'.$pre[1].'_options_seq");

';					
					break;
			}
			$method .= '
		$dict->ExecuteSQLArray($sqlarray);';
		}
			$method .= '
		break;';
	}
	$method .= '	
	// END SWITCH($current_version)
	}
}
';
}

$method .= '?'.'>';
?>
