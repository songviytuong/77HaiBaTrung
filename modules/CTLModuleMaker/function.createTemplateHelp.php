<?php
$helps = array();
$i = 0;
while($i <= count($levels)){
	$help = '<ul>
	<li>$leveltitle</li>
	';
	if($i == count($levels)){
		$level = $levels[$i - 1];
		$level[0] = 'final_level_template';
	}else{
		$level = $levels[$i];
		$help .= '<li>$parentobj (if parent is specified)</li>
		<li>$itemlist (array of items)</li>
		<li>$item-&gt;is_selected</li>
	';
	}
	$help .= '<li>$item-&gt;name</li>
	<li>$item-&gt;alias</li>
	';
	if($i < count($levels)){
		$help .= '<li>$item-&gt;detaillink</li>
	<li>$item-&gt;detailurl</li>
	';
	}
	foreach($level[4] as $field){
		if($field[0] == 'item_order' || $field[0] == 'active' || $field[0] == 'id' || $field[0] == 'name' || $field[0] == 'alias'){
		}elseif($field[0] == 'parent'){
			$help .= '<li>$item-&gt;parent_id</li>
	<li>$item-&gt;parent_alias</li>
	<li>$item-&gt;parent_name</li>
	<li>$item-&gt;parentlink</li>
	<li>$tiem-&gt;parenturl</li>
	';
		}else{
			$help .= '<li>$item-&gt;'.$field[0].($field[1] == 11?' (array)':'').'</li>
	';
		}
		if($field[1] == 6 || $field[1] == 7){
			$help .= '<li>$item-&gt;'.$field[0].'_namevalue</li>
	';
		}
		if($field[1] == 8 && $field[5]['thumb'] != ''){
			$help .= '<li>$item-&gt;'.$field[0].'_thumbnail</li>
	';
		}
	}
	if($level[0] == 'final_level_template')	$help .= '<li>$labels->...</li>
	';
	$help .= '</ul>';

	$helps[$level[0]] = $help;
	$i++;
}

?>
