<?php
$lang['friendlyname'] = 'CAT-like ModuleMaker';
$lang['title'] = 'Creating a catalogue-like module...';
$lang['moddescription'] = 'A module maker designed for front-end catalogue-like modules, letting you choose from a variety of field types.';

$lang['preferences'] = 'Preferences';
$lang['Yes'] = 'Yes';
$lang['No'] = 'No';
$lang['Createamodule'] = 'Create a new module';
$lang['Modulecreation'] = 'Module Creation';
$lang['helptab'] = 'Help';
$lang['Step'] = 'Step';
$lang['Level'] = 'Level';
$lang['Step1title'] = 'Basic Module Informations';
$lang['Step2title'] = 'Creating levels';
$lang['Step3title'] = 'Final step (last element before the creation of files...)';
$lang['creatingfiles'] = 'Creating files...';
$lang['Currentstructure'] = 'Current structure';
$lang['Next'] = 'Next';
$lang['Previous'] = 'Previous';
$lang['Field'] = 'Field';
$lang['nofield'] = 'You chose to create no fields for this level. Click next to continue.';
$lang['possiblefields'] = 'Possible fields:';
$lang['onlyonetemplate'] = 'Only one template';
$lang['folderscreation'] = 'Folders creation';
$lang['filecreation'] = 'File creation: ';
$lang['filecopy'] = 'File copy: ';
$lang['installmodule'] = 'Install module';
$lang['creationsucces_title'] = 'The creation process has come to an end, and it seems to have worked!';
$lang['creationsucces1'] = 'You should now be able to install and use the module, which you may do by clicking the following link.';
$lang['creationsucces2'] = 'However, it is most likely that the language files will need some reworking...<br/>Language files were created for:';
$lang['creationsucces3'] = 'You may find them in: ';
$lang['prompt_exportdna'] = 'Wish to save and continue later?';
$lang['prompt_exportdna2'] = 'Think you might need to create this module again?';
$lang['exportdna'] = 'Export module DNA';
$lang['importmoduledna'] = 'Import module DNA';
$lang['createit'] = 'Create it';
$lang['modifyit'] = 'Modify it';
$lang['onlyonedefault'] = 'Only one per level';
$lang['defaultbyparent'] = 'One for each parent';
$lang['checkbox'] = 'Checkbox (multiple)';
$lang['radiobuttons'] = 'Radio buttons';
$lang['dropdown'] = 'Dropdown list';
$lang['select'] = 'Select list (multiple)';
$lang['serror'] = 'Error';
$lang['postinstall'] = 'Module successfully added.';
$lang['postuninstall'] = 'Module successfully removed.';
$lang['disvcheck'] = 'Disable version check';
$lang['envcheck'] = 'Enable version check';
$lang['moduleinstalled'] = 'The module has been installed. Upon reloading, it should appear in the content menu.';
$lang['moduleupgrade'] = 'Module upgrade function';
$lang['modifiedfields'] = 'Modified fields';
$lang['createdfields'] = 'Created fields';
$lang['deletedfields'] = 'Deleted fields';
$lang['autosaved'] = 'Autosaved module dna (in CTLModuleMaker/autosaved):';

// PROMPTS
$lang['prompt_nameofmodule'] = 'Enter the name of the module.<br/><i>This is for internal use only. It should be alpha-numerical, without accents, spaces, or other symbols.</i>';
$lang['prompt_friendlyname'] = 'Enter the module\'s friendly name. This will be shown in the admin menus.';
$lang['prompt_moduledescription'] = 'Enter a description for the module.';
$lang['prompt_version'] = 'Enter the module\'s version.';
$lang['prompt_howmanylevels'] = 'How many levels should there be in the module?';
$lang['prompt_nameoflevel'] = 'How should this level be named? (i.e. "Category")<br/><i>It should be alpha-numerical, without accents, spaces, or other symbols.<br/>You will be able to change the front name of the level in the language files.</i>';
$lang['prompt_paramprefix'] = 'Enter a parameter prefix for the level.<br/><i>This is for internal use only. It should be alpha-numerical, without accents, spaces, or other symbols, and rather short.</i>';
$lang['prompt_howmanyfields'] = 'How many fields should this level have?<br/><i>Some additional fields not directly defined by you will also be created (id, name, alias, item_order, parent, active and default).<br/>Do <b>not</b> plan fields for this or for templates.</i>';
$lang['prompt_sharechildren'] = 'Do members of this level share their children? (can a child belong to several parents?)<br/><i>If you answer yes for any level, you will not have the possibility to have differents final level displays.</i>';
$lang['prompt_templatelevel'] = 'What level should determine the template for the final item (final child)?<br/><i>In addition, there will be for each level a template for the list of it\'s members.</i>';
$lang['prompt_name'] = 'Field Name<br/><i>This is for internal use only. It should be alpha-numerical, without accents, spaces, or other symbols. You will be able to change the display name of the fields in the language files.</i>';
$lang['prompt_type'] = 'Field Type (What does it contain?)';
$lang['prompt_needed'] = 'Is this field mandatory?';
$lang['prompt_indexed'] = 'Should this field be indexed for the Search Module?';
$lang['prompt_default'] = 'Enter a default value for the field (empty = no default value)<br/><i>This will only work with text or numerical fields. Make sure that the value you enter matches the field type!</i>';
$lang['prompt_listoptions'] = 'Enter the options for the predefined list of values, separated by a comma (,).<br/><i>(Minimum 2 values)</i>';
$lang['prompt_upfolder'] = 'Enter the path of the folder in which the files should be uploaded.<br/><i>This path will be added to the upload folder (empty = root of the upload 
folder).</i>';
$lang['prompt_size'] = 'Should the uploaded images be resized to a maximum size?<br/><i>Enter the size in pixels, format "widthxheight"<br/>(empty = images not resized).</i>';
$lang['prompt_thumb'] = 'Should a thumbnail be created, what should be its maximum size?<br/><i>Enter the size in pixels, format "widthxheight"<br/>(empty = no thumbnail created).</i>';
$lang['prompt_listmode'] = 'List mode';
$lang['prompt_itemorder'] = 'Should elements of this level be ordered by parents?';
$lang['prompt_nbdefaults'] = 'Should there be only one default element for this level, or one for each parent?<br/>(Note that any answer will make some functions impossible)';
$lang['prompt_newontop'] = 'Should new elements appear at the top of the list? (otherwise they will appear at the bottom)';
$lang['prompt_import'] = 'The module seems complete. What do you want to do?';
$lang['prompt_adminpanel'] = '<p>Please choose the fields that should be shown in the admin table of this level. Write the name of each field in the order you would like them to appear, seperated by a comma (,).</p><p><b>The "name" field, which will be the edit link, is mandatory.</b> (There will also be a delete link added).</p><i>(Please note that values will not be displayed for list fields with multiple values.)</i>';
$lang['prompt_moduleupgrade'] = 'Would you like to create an upgrade function for this module?';

// WARNING AND ERRORS
$lang['unsupported'] = 'Not yet fully supported';
$lang['warning_resize']  = 'The resize functions require the gd librairies and will only work with .jpeg, .jpg, .png or .gif files';
$lang['warning_previous'] = 'Changes in this page will be lost. Are you sure you want to go back to the previous step? (To save changes and go back, click Next and come back two steps)';
$lang['warning_nexttolaststep'] = 'You are about to enter the last step of the module creation. You will not be able to go back to these options anymore. Do you wish to continue?';
$lang['warning_laststep'] = 'You are about to finish the module creation, and files will be created. Do you wish to continue?';
$lang['warning_takesometime'] = 'Creating files... this may take a while, depending of the complexity of the levels it may even reach a few seconds...';
$lang['errorwithrequest'] = 'There was an error with your request';
$lang['error_writetest'] = 'The write test failed : the module folder is not writable. The module creation process cannot continue';
$lang['error_nameofmodule'] = 'You must enter a valid name for the module';
$lang['error_nameinvalid'] = 'The name you have chosen is incorrect (should be alphanumerical). We suggested a new one.';
$lang['error_friendlyname'] = 'You must enter a friendly name for the module';
$lang['error_howmanylevels'] = 'The amount of levels you have entered is invalid (must be a number between 2 and 9)';
$lang['error_nameoflevel'] = 'The name you have entered is incorrect (should be alphanumerical). We suggested a new one.';
$lang['error_paramprefix'] = 'The parameter prefix you have entered is incorrect (should be alphanumerical). We suggested a new one.';
$lang['error_missingvalue'] = 'A necessary value is missing.';
$lang['error_howmanyfields'] = 'The amount of fields you have entered is invalid (must be a number between 1 and 30)';
$lang['error_adminfields_name'] = 'The "name" field, which is mandatory, is missing.';
$lang['error_folderalreadythere'] = 'The module folder already exists! Operation aborted.<br/>(If you wish, you may delete the folder and reload the current page to continue)';
$lang['error_general'] = 'One or more values are either missing or invalid.';
$lang['error_emptyname'] = 'The name field cannot be empty.';
$lang['error_invalid'] = 'The value is invalid.';
$lang['error_unknown'] = 'Unknown error.';
$lang['error_folders'] = 'There was an error at the very beginning of the file creation process (during folder creation).';
$lang['error_creation'] = 'One or more error(s) occured during the creation of the module files. This shouldn\'t be happening.<br/>You are invited to report problems at the <a href="http://dev.cmsmadesimple.org/projects/ctlmodulemaker" target="_blank">forge</a>.<br/>So that you don\'t have to start everything over, a dna file of your module has been saved in modules/CTLModuleMaker/autosaved/';
$lang['error_foldercreation'] = 'could not be created.';
$lang['error_reserved'] = 'You have used a php/mysql reserved word as a name: ';
$lang['error_reserved2'] = 'Please choose another.';
$lang['error_namealreadyused'] = 'The name you have chosen is already used, either by you or by the module. Please choose another.';
$lang['error_listoptions'] = 'The list\'s options are invalid. (For a predefined list, you have to specify the options.)';
$lang['error_upfolder'] = 'The upload folder you have entered is invalid.';
$lang['error_imgsize'] = 'The image size you have entered is invalid.';
$lang['error_thumbsize'] = 'The thumbnail size you have entered is invalid.';
$lang['error_javascript'] = 'Javascript should be actived to go further.';
$lang['error_dnafile'] = 'The file is not a valid module DNA.';
$lang['error_installfailed'] = 'The module could not be installed. If you encounter problems with the admin panel, delete the created module\'s folder.';


// FIELD TYPES
$lang['Text'] = 'Short text';
$lang['LongText'] = 'Long text';
$lang['Number'] = 'Number (int)';
$lang['chars'] = 'chars.';
$lang['PredefinedList'] = 'Chosen from a predefined list';
$lang['List'] = 'Chosen from a dynamic list';
$lang['Image'] = 'Image';
$lang['File'] = 'Other file';
$lang['Date'] = 'Date-Time';
$lang['undefined_files'] = 'Undefined amount of files';


// PREFERENCES
$lang['pref_checkversion'] = 'Should CTLModuleMaker check online for warnings or new versions?';
$lang['pref_autosave'] = 'Should a copy of the module dna be automatically saved in the \'autosaved\' folder?';
$lang['pref_innerdebug'] = 'Should we show the module creation parameters when the cms is in debug mode?';
$lang['pref_doupgrade'] = 'Should we attempt to create upgrade functions for modified modules?<br/>(This might not be supported by all databases and should be monitored)';
$lang['pref_allowsinglelevel'] = 'Allow the creation of single-level modules?<br/>(It is suggested that all modules have at least two levels, for single-level modules may not support some functionalities)';


// HELP TEXT :
$lang['adminfields'] = '
<li>name*</li>
<li>alias <i>(used for links)</i></li>
<li>isdefault <i>(make/unmake item as default item)</i></li>
<li>date_modified</li>
<li>active <i>(set item active or inactive)</i></li>
<li>parent <i>(name of the parent, if applicable)</i></li>
<li>nbchildren <i>(how many children this item has, if applicable)</i></li>
<li>movelinks <i>(to change the order of the items)</i></li>
';
$lang['help_import'] = "If you would like to create a module from another previously created through this module maker, or continue an interrupted creation process, you may import the module DNA file here:";
$lang['help_import2'] = "(Lost a module? check in<br/>modules/CTLModuleMaker/autosaved/ ...)";
$lang['help_export'] = "(Exporting the module DNA will allow you to later modify your module)";
$lang['help_think'] = "In the next steps, you will be asked informations about each of these levels (<b>from top to bottom</b>). It is suggested to think about it well before going through these steps...";
$lang['help_structure'] = "\"Level\" is here meant as a level of organization. For example, should this module be about books, you might want to classify them in a hierarchical way :<br>
Library Module<ul>
	<li style=\"list-style: square;\"><u>Fiction</u><ul>
		<li style=\"list-style: disc;\">Science-Fiction</li>
		<li style=\"list-style: disc;\">Romance</li>
		<li style=\"list-style: disc;\">Suspense</li>
		</ul></li>
	<li style=\"list-style: square; margin-top: 5px;\"><u>Non-fiction</u><ul>
		<li style=\"list-style: disc;\">Philosophy<ul>
			<li style=\"list-style: circle;\">La Gaya Scienza</li>
			</ul></li>
		<li style=\"list-style: disc;\">Science</li>
		</ul></li>
</ul><br/>
<p>In this case, we would have 3 levels : we may call the first 'Type' (Fiction/Non-fiction) and the second 'Sub-type' (Science-Fiction/Romance/Philosophy), the third being the books themselves (La Gaya Scienza). We will then say that Science-Fiction and Romance sub-types are children of the type Fiction (which is their parent).<br/>
Typically, the last level (here 'books') will have the most fields, the others being for classification purposes, but you may like a category description or something...
Up to 9 levels are supported, which is completely arbitrary, but I couldn't see why we'd need more.<br/>".$lang['help_think']."</p>";
$lang['help_moduleupgrade'] = '<p>CTLModuleMaker has detected that you may changes to the initial dna file you imported. If you wish, an upgrade function will be created with the module, so that these changes can be made in the database without the need to reinstall the module. Obviously, this upgrade will only work with modules that were created with the dna you first imported.</p>
<p>Any field that was renamed is considered to be deleted and recreated (which means that upgrading would lose the data in those fields). Therefore, please make sure that you really made the following changes:</p>';

$lang['help_adminlinks'] = '<p>For each level, you must decide which fields will be shown in the listing table of the admin panel. Remember that this is mostly for the editor to find his way through the items or seek quick information. The other fields will still be editable when an item is clicked.<br/>In uncertainty, leave what\'s already there.<p><p>To give you an idea, here is what an adminpanel for a level may look like:</p><div style="background-color: #fff; margin: 10px; text-align: center;"><img src="../modules/CTLModuleMaker/images/help_adminfields.gif" /></div>';
$lang['help_none'] = '<p>Unfortunately, there is currently no help specific to this step of the creation process. You may consult the module <a href="listmodules.php?action=showmodulehelp&module=CTLModuleMaker" target="_blank">general help</a>.</p><p>If you think this step needs more explanation, please post your request on <a href="http://dev.cmsmadesimple.org/projects/ctlmodulemaker/" target="_blank">The Forge</a>.</p>';
$lang['help'] = '<h2>General help</h2><br/>
<p>This should be pretty easy to use without help... but for each step of the creation process, you may look at the help tab for advices on this particular step. Although this creates fully functional modules, please keep in mind that the primary intent behind this project was at first to provide basic modules for programmers to modify.</p>
<p>Before starting the creation process, make sure the module folder is writable (a writing test will be done) and that no folder with the name of the module you wish to create exist. JavaScript should also be activated.</p>
<p>Obviously, the installation/upgrade/uninstallation of <b>this</b> module will have <b>no</b> effect on modules already created.</p>
<br/><h2>Recent changes that you should be aware of...</h2>
<ul>
<li style="list-style-type: disc; margin-top: 4px; margin-bottom: 4px;">A "search" action has been added (see help of the created modules), allowing to search by fields (within text fields and single-value list fields).</li>
<li style="list-style-type: disc; margin-top: 4px; margin-bottom: 4px;">In 1.7, a full <b>upgrade</b> function has been added to created modules. This means that you will be able to, for example, add fields to existing modules without the need to reinstall them. (Keep in mind that some databases may not support this)</li>
<li style="list-style-type: disc; margin-top: 4px; margin-bottom: 4px;">In 1.6.4, classes have been added to the pagemenu when separating into pages. For example, you may use page menu css from <a href="http://www.mis-algoritmos.com/2007/03/16/some-styles-for-your-pagination/" target="_blank">here</a>; I tried to use the same classes, but for uniformity reasons I didn\'t use spans, so double-check the css.</li>
<li style="list-style-type: disc; margin-top: 4px; margin-bottom: 4px;">From 1.6 and up, the "list" action has been removed. Now the "default" action handles both - so you don\'t have to specify the action anymore. This means, however, that when you display the last level and there is only one item, the detail template will be used. If you don\'t want that, use the "forcelist" parameter.</li>
<li style="list-style-type: disc; margin-top: 4px; margin-bottom: 4px;">From 1.5 and up, the templates are managed by the cms. This allows for multiple templates, but has slightly changed the way it works.</li>
</ul>
<br/><h2>What\'s a .dna file?</h2><br/>
<p>DNA files are not modules, and have nothing to do with the module xml files. They are used by CTLModuleMaker to store the input data of the module you create - that is, every informations you enter to build your module. This means that if you wish to recreate the same module or change something in it, you don\'t have to get through all the creation process again and may simply enter the DNA file.<br/>Unless you change your preferences, at the module creation a dna file of your modules are automatically saved in modules/CTLModuleMaker/autosaved/.<br/>Finally, should you have to report a bug in the forge, it would help a lot to include a dna file!</p>
<br/><br/><p>Please submit any problem you encounter or any suggestion at the <a href="http://dev.cmsmadesimple.org/projects/ctlmodulemaker/" target="_blank">Forge</a>!</p><br/><br/>';
