<?php
$lang['friendlyname'] = 'Cr&eacute;ateur de modules-CTL';
$lang['title'] = 'Cr&eacute;ation de modules-catalogues...';
$lang['moddescription'] = 'Un g&eacute;n&eacute;rateur de modules destin&eacute; &agrave; cr&eacute;er des modules du genre "catalogue", permettant une vari&eacute;t&eacute; de fonctionalit&eacute;s et de types de champs.';

$lang['preferences'] = 'Pr&eacute;f&eacute;rences';
$lang['Yes'] = 'Oui';
$lang['No'] = 'Non';
$lang['Createamodule'] = 'Cr&eacute;er un nouveau module';
$lang['Modulecreation'] = 'Cr&eacute;ation de module';
$lang['helptab'] = 'Aide';
$lang['Step'] = '&Eacute;tape';
$lang['Level'] = 'Niveau';
$lang['Step1title'] = 'Informations de base du module';
$lang['Step2title'] = 'Creation des niveaux';
$lang['Step3title'] = '&Eacute;tape finale (dernier point avant la cr&eacute;ation des fichiers...)';
$lang['creatingfiles'] = 'Cr&eacute;ation des fichiers...';
$lang['Currentstructure'] = 'Structure actuelle';
$lang['Next'] = 'Suivant';
$lang['Previous'] = 'Pr&eacute;c&eacute;dent';
$lang['Field'] = 'Champ';
$lang['nofield'] = 'Vous avez choisi de ne pas cr&eacute;er de champs pour ce niveau. Cliquez Suivant pour continuer.';
$lang['possiblefields'] = 'Champs possibles:';
$lang['onlyonetemplate'] = 'Un seul gabarit';
$lang['folderscreation'] = 'Cr&eacute;ation des dossiers';
$lang['filecreation'] = 'Cr&eacute;ation du fichier: ';
$lang['filecopy'] = 'Copie du fichier: ';
$lang['installmodule'] = 'Installer le module';
$lang['creationsucces_title'] = 'Le processus de cr&eacute;ation semble s\'&ecirc;tre termin&eacute; avec succ&egrave;s!';
$lang['creationsucces1'] = 'Vous devriez maintenant &ecirc;tre en mesure d\'installer et d\'utiliser le module. Vous pouvez l\'installer en utilisant le lien ci-dessous.';
$lang['creationsucces2'] = 'Cependant, il est probable que les fichiers de langue n&eacute;cessitent un certain remaniement...<br/>Des fichiers de langue ont &eacute;t&eacute; cr&eacute;&eacute;s pour :';
$lang['creationsucces3'] = 'Vous pouvez les trouver dans: ';
$lang['prompt_exportdna'] = 'Vous voulez sauvegarder et continuer plus tard?';
$lang['prompt_exportdna2'] = 'Vous pensez avoir &agrave; cr&eacute;er &agrave; nouveau ce module?';
$lang['exportdna'] = 'Exporter l\'ADN du module';
$lang['importmoduledna'] = 'Importer l\'ADN d\'un module';
$lang['createit'] = 'Le cr&eacute;er';
$lang['modifyit'] = 'Le modifier';
$lang['onlyonedefault'] = 'Un seul';
$lang['defaultbyparent'] = 'Un par parent';
$lang['checkbox'] = 'Cases &agrave; cocher (multiple)';
$lang['radiobuttons'] = 'Boutons radio';
$lang['dropdown'] = 'Liste d&eacute;roulante';
$lang['select'] = 'Liste &agrave; choix multiples';
$lang['serror'] = 'Erreur';
$lang['postinstall'] = 'Module install&eacute;.';
$lang['postuninstall'] = 'Module d&eacute;sinstall&eacute;.';
$lang['disvcheck'] = 'D&eacute;sactiver la v&eacute;rification';
$lang['envcheck'] = 'Activer la v&eacute;rification<br/>de version';
$lang['moduleinstalled'] = 'Le module a &eacute;t&eacute; install&eacute;. Apr&egrave;s un rafra&icirc;chissement, vous devriez le trouver dans le menu Contenu.';
$lang['moduleupgrade'] = 'Fonction de mise-&agrave;-jour du module';
$lang['modifiedfields'] = 'Champs modifi&eacute;s';
$lang['createdfields'] = 'Champs cr&eacute;&eacute;s';
$lang['deletedfields'] = 'Champs supprim&eacute;s';
$lang['autosaved'] = 'Adn de module sauvegard&eacute;s<br/>(dans CTLModuleMaker/autosaved):';

// PROMPTS
$lang['prompt_nameofmodule'] = 'Entrez le nom du module.<br/><i>Pour usage interne seulement. Le nom devrait &ecirc;tre alpha-num&eacute;rique, sans accents, espaces ou autres symboles.</i>';
$lang['prompt_friendlyname'] = 'Entrez le nom courant (friendlyname) du module. C\'est ce nom qui sera affich&eacute; dans les menus de l\'administration.';
$lang['prompt_moduledescription'] = 'Entrez une description pour le module.';
$lang['prompt_version'] = 'Entrez la version du module.';
$lang['prompt_howmanylevels'] = 'Combien de niveaux le module devrait-il compter?';
$lang['prompt_nameoflevel'] = 'Entrez un nom pour le niveau (i.e. "Cat&eacute;gorie")<br/><i>Le nom devrait &ecirc;tre alpha-num&eacute;rique, sans accents, espaces ou autres symboles.<br/>Vous pourrez par la suite le changer dans les fichiers de langue.</i>';
$lang['prompt_paramprefix'] = 'Entrez un pr&eacute;fixe de param&egrave;tre pour le niveau.<br/><i>Pour usage interne seulement. Le pr&eacute;fixe devrait &ecirc;tre alpha-num&eacute;rique, sans accent, espace ou autre symbole, et devrait &ecirc;tre plut&ocirc;t court.</i>';
$lang['prompt_howmanyfields'] = 'Combien de champs ce niveau devrait-il compter?<br/><i>Des champs additionnels seront aussi cr&eacute;&eacute;s (id, name, alias, item_order, parent, active and default).<br/>Ne planifiez <b>pas</b> de champs pour ces &eacute;l&eacute;ments ou pour les gabarits.</i>';
$lang['prompt_sharechildren'] = 'Est-ce que les membres de ce niveau partagent leur enfants? (un enfant peut-il appartenir &agrave; plusieurs parents?)<br/><i>Si vous r&eacute;pondez oui pour n\'importe quel niveau, vous n\'aurez plus la possibilit&eacute; de d&eacute;finir plusiuers gabarits d\'affichage diff&eacute;rents pour le niveau final.</i>';
$lang['prompt_templatelevel'] = 'Quel niveau devrait d&eacute;terminer le gabarit d\'affichage des &eacute;l&eacute;ments du niveau final?<br/><i>Il y aura par ailleurs un gabarit pour chaque niveau pour les listes d\'&eacute;l&eacute;ments du niveau enfant.</i>';
$lang['prompt_name'] = 'Nom du champ<br/><i>Pour usage interne seulement. Le nom devrait &ecirc;tre alpha-num&eacute;rique, sans accents, espaces ou autres symboles. Vous pourrez par la suite changer le nom d\'affichage dans les fichiers de langue.</i>';
$lang['prompt_type'] = 'Type de champ (Que contient-il?)';
$lang['prompt_needed'] = 'Ce champ est-il obligatoire?';
$lang['prompt_indexed'] = 'Ce champ devrait-il &ecirc;tre index&eacute; pour le module de recherche?';
$lang['prompt_default'] = 'Entrez une valeur par d&eacute;faut pour le champ (vide = aucune valeur par d&eacute;faut)<br/><i>Fonctionne uniquement avec les champs num&eacute;riques ou de texte court. Assurez-vous que le type de donn&eacute;es correspond au type de champ!</i>';
$lang['prompt_listoptions'] = 'Entrez les options pour la liste de valeurs pr&eacute;d&eacute;finies, s&eacute;par&eacute;es par une virgule (,).<br/><i>(Minimum 2 valeurs)</i>';
$lang['prompt_upfolder'] = 'Entrez le chemin d\'acc&egrave;s dans lequel les fichiers devraient &ecirc;tre upload&eacute;s.<br/><i>Ce chemin sera ajout&eacute; au chemin d\'acc&egrave;s d\'upload du CMS (vide = racine du dossier d\'upload).</i>';
$lang['prompt_size'] = 'Taille maximum de l\'image upload&eacute;e.<br/><i>Entrez la taille en pixels, dans le format "largeurxhauteur"<br/>(vide = aucune redimension).</i>';
$lang['prompt_thumb'] = 'Devrait-on cr&eacute;er une vignette pour l\'image? Si oui, entrez la taille en pixels de la vignette.<br/><i>Format "largeurxhauteur",(vide = pas de vignette).</i>';
$lang['prompt_listmode'] = 'List mode';
$lang['prompt_itemorder'] = 'Les &eacute;l&eacute;ments de ce niveau devraient-ils &ecirc;tre ordonn&eacute;s par parent?';
$lang['prompt_nbdefaults'] = 'Devrait-il y avoir un seul &eacute;l&eacute;ment par d&eacute;faut pour tout le niveau, ou un pour chaque parent?<br/>(Notez que chaque cas rend certaines fonctionalit&eacute;s impossibles)';
$lang['prompt_newontop'] = 'Les nouveaux &eacute;l&eacute;ments devraient-ils appara&icirc;tre au haut de la liste? (Plut&ocirc;t qu\'au bas)';
$lang['prompt_import'] = 'Le module que vous tentez d\'importer semble &ecirc;tre complet. Que voulez-vous faire?';
$lang['prompt_adminpanel'] = '<p>Choisissez les champs qui devraient &ecirc;tre affich&eacute;s dans le tableau d\'administration du niveau. Entrez le nom de chaque champ, s&eacute;par&eacute; par une virgule, dans l\'ordre o&ugrave; vous voulez les voir appara&icirc;tre.</p><p><b>Le champ "name", qui sera le lien d\'&eacute;dition, est obligatoire.</b> (Un champ de suppression sera aussi ajout&eacute;).</p><i>(Notez que les valeurs ne seront pas affich&eacute;es pour les champs de liste avec valeurs multiples.)</i>';
$lang['prompt_moduleupgrade'] = 'Voulez-vous cr&eacute;er une fonction de mise-&agrave;-jour pour ce module?';



// WARNING AND ERRORS
$lang['unsupported'] = 'Pas encore pleinement support&eacute;';
$lang['warning_resize']  = 'La fonction de redimensionnement automatique n&eacute;cessite les libraires gd et ne fonctionnera qu\'avec les fichiers .jpeg, .jpg, .png ou .gif';
$lang['warning_previous'] = 'Les changements dans cette page seront perdus. Voulez-vous vraiment retourner &agrave; l\'&eacute;tape pr&eacute;c&eacute;dente?';
$lang['warning_nexttolaststep'] = 'Vous &ecirc;tes sur le point d\'entrer dans la derni&egrave;re &eacute;tape de la cr&eacute;ation de module. Vous ne pourrez plus revenir sur ces options. Voulez-vous poursuivre?';
$lang['warning_laststep'] = 'Vous &ecirc;tes sur le point de terminer la cr&eacute;ation du module... les fichiers seront cr&eacute;&eacute;s. Voulez-vous continuer?';
$lang['warning_takesometime'] = 'Cr&eacute;ation des fichiers... ceci pourrait prendre un certain temps, voire m&ecirc;me quelques secondes...';
$lang['errorwithrequest'] = 'Il y a eu une erreur avec votre requ&ecirc;te';
$lang['error_writetest'] = 'Le test d\'&eacute;criture a &eacute;chou&eacute; : le dossier de modules n\'est pas ouvert en &eacute;criture. La cr&eacute;ation de module ne peut pas continuer.';
$lang['error_nameofmodule'] = 'Vous devez entrer un nom pour le module';
$lang['error_nameinvalid'] = 'Le nom que vous avez choisi est invalide (il devrait &ecirc;tre alphanum&eacute;rique). Nous en avons sugg&eacute;r&eacute; un nouveau.';
$lang['error_friendlyname'] = 'Vous devez entrer un nom courant (friendlyname) pour le module.';
$lang['error_howmanylevels'] = 'Le nombre de niveaux que vous avez entr&eacute; est invalide (devrait &ecirc;tre un nombre entre 2 et 9)';
$lang['error_nameoflevel'] = 'Le nom que vous avez choisi est invalide (il devrait &ecirc;tre alphanum&eacute;rique). Nous en avons sugg&eacute;r&eacute; un nouveau.';
$lang['error_paramprefix'] = 'Le pr&eacute;fixe de param&egrave;tre que vous avez choisi est invalide (il devrait &ecirc;tre alphanum&eacute;rique). Nous en avons sugg&eacute;r&eacute; un nouveau.';
$lang['error_missingvalue'] = 'Une valeur n&eacute;cessaire est manquante.';
$lang['error_howmanyfields'] = 'Le nombre de champs que vous avez entr&eacute; est invalide (devrait &ecirc;tre un nombre entre 1 et 30)';
$lang['error_adminfields_name'] = 'Le champ "name" est obligatoire.';
$lang['error_folderalreadythere'] = 'Le dossier du module existe d&eacute;j&agrave;! Le processus est avort&eacute;.<br/>(Si le dossier n\'est pas important, vous pouvez le supprimer et recharger la page actuelle pour poursuivre)';
$lang['error_general'] = 'Une ou plusieurs valeurs sont manquantes ou invalides.';
$lang['error_emptyname'] = 'Le champ de nom ne peut pas &ecirc;tre vide.';
$lang['error_invalid'] = 'Valeur invalide.';
$lang['error_unknown'] = 'Erreur inconnue.';
$lang['error_folders'] = 'Il y a eu une erreur au tout d&eacute;but du processus de cr&eacute;ation de fichiers (pendant la cr&eacute;ation des dossiers).';
$lang['error_creation'] = 'Une ou plusieurs erreurs sont survenues lors de la cr&eacute;ation des fichiers du module. Cela ne devrait pas arriver.<br/>Vous &ecirc;tes invit&eacute;s &agrave; rapporter vos problemes &agrave; la <a href="http://dev.cmsmadesimple.org/projects/ctlmodulemaker" target="_blank">forge</a>.<br/>Pour que vous n\'ayez pas &agrave; tout recommencer du d&eacute;but, un fichier d\'adn de votre module &agrave; &eacute;t&eacute; enregistr&eacute; dans /modules/CTLModuleMaker/autosaved/';
$lang['error_foldercreation'] = 'n\'a pas pu &ecirc;tre cr&eacute;&eacute;.';
$lang['error_reserved'] = 'Vous avez utilis&eacute; un nom r&eacute;serv&eacute; par php/mysql : ';
$lang['error_reserved2'] = 'Veuillez en choisir un autre.';
$lang['error_namealreadyused'] = 'Le nom que vous avez choisi est d&eacute;j&agrave; utilis&eacute;, par vous ou par le module. Veuillez en choisir un autre.';
$lang['error_listoptions'] = 'Les options de la liste sont invalides. (Pour une liste pr&eacute;d&eacute;finie, les options doivent &ecirc;tre sp&eacute;cifi&eacute;es.)';
$lang['error_upfolder'] = 'Le dossier choisi est invalide.';
$lang['error_imgsize'] = 'La taille d\'image entr&eacute;e est invalide.';
$lang['error_thumbsize'] = 'La taille de vignette entr&eacute;e est invalide.';
$lang['error_javascript'] = 'Javascript devrait &ecirc;tre activ&eacute; pour poursuivre.';
$lang['error_dnafile'] = 'Le fichier n\'est pas un ADN de module valide.';
$lang['error_installfailed'] = 'Le module n\'a pas pu &ecirc;tre install&eacute;. Si vous rencontrez des probl&egrave;mes avec le panneau d\'administration, supprimez le dossier du module cr&eacute;&eacute;.';



// FIELD TYPES
$lang['Text'] = 'Court texte';
$lang['LongText'] = 'Texte long';
$lang['Number'] = 'Nombre (int)';
$lang['chars'] = 'cars.';
$lang['PredefinedList'] = 'Choisi &agrave; partir d\'une liste pr&eacute;d&eacute;finie';
$lang['List'] = 'Choisi &agrave; partir d\'une liste dynamique';
$lang['Image'] = 'Image';
$lang['File'] = 'Autre fichier';
$lang['Date'] = 'Date-Time';
$lang['undefined_files'] = 'Nombre ind&eacute;termin&eacute; de fichiers';


// PREFERENCES
$lang['pref_checkversion'] = 'CTLModuleMaker devrait-il automatiquement v&eacute;rifier si des avertissements ou de nouvelles versions sont disponibles?';
$lang['pref_autosave'] = 'Une copie du dna de chaque module cr&eacute;&eacute; devrait-elle &ecirc;tre automatiquement enregistr&eacute;e dans le dossier autosaved?';
$lang['pref_innerdebug'] = 'Les param&egrave;tres de creation de module devraient-ils &ecirc;tre affich&eacute;s lorsque le cms est en mode \'debug\'?';
$lang['pref_doupgrade'] = 'Devrait-on tenter de cr&eacute;er une fonction de mise-&agrave;-jour pour les modules modifi&eacute;s?<br/>(Pas support&eacute; par toutes les bases de donn&eacute;es)';
$lang['pref_allowsinglelevel'] = 'Permettre la creation de module comportant un seul niveau?<br/>(Ceci n\'est pas recommand&eacute;, et les modules ainsi cr&eacute;&eacute;s pourraient ne pas supporter certaines fonctionalit&eacute;s)';


// HELP TEXT :
$lang['adminfields'] = '
<li>name*</li>
<li>alias <i>(utilis&eacute; pour les liens)</i></li>
<li>isdefault <i>(pour faire d\'un &eacute;l&eacute;ment l\'&eacute;l&eacute;ment par d&eacute;faut)</i></li>
<li>date_modified</li>
<li>active <i>(pour activer/d&eacute;sactiver un &eacute;l&eacute;ment)</i></li>
<li>parent <i>(nom du parent, si applicable)</i></li>
<li>nbchildren <i>(nombre d\'enfants de l\'&eacute;l&eacute;ment, si applicable)</i></li>
<li>movelinks <i>(pour changer l\'ordre des &eacute;l&eacute;ments)</i></li>
';
$lang['help_import'] = "Si vous voulez cr&eacute;er un module &agrave; partir d'un autre ayant d&eacute;j&agrave; &eacute;t&eacute; cr&eacute;&eacute; avec ce g&eacute;n&eacute;rateur de modules, ou poursuivre la cr&eacute;ation interrompue d'un module, vous pouvez importer l'ADN du module ici:";
$lang['help_import2'] = "(Vous avez perdu un module? jetez un coup d'oeil dans modules/CTLModuleMaker/autosaved/ ...)";
$lang['help_export'] = "(Exporter l'ADN du module vous permettra de le modifier plus tard)";
$lang['help_think'] = "Dans les prochaines &eacute;tapes, vous aurez &agrave; d&eacute;finir certaines caract&eacute;ristiques de chacun des niveaux (<b>de haut en bas</b>). Il est sugg&eacute;r&eacute; de bien penser &agrave; tout &ccedil;a avant de passer &agrave; travers ces &eacute;tapes...";
$lang['help_structure'] = "\"Niveau\" signifie ici niveau d\'organisation. Par exemple, si ce module &eacute;tait &agrave; propos de livres, on pourrait vouloir les classifier de mani&egrave;re hi&eacute;rarchique :<br>
Module biblioth&egrave;que<ul>
	<li style=\"list-style: square;\"><u>Fiction</u><ul>
		<li style=\"list-style: disc;\">Science-Fiction</li>
		<li style=\"list-style: disc;\">Romance</li>
		<li style=\"list-style: disc;\">Suspense</li>
		</ul></li>
	<li style=\"list-style: square; margin-top: 5px;\"><u>Non-fiction</u><ul>
		<li style=\"list-style: disc;\">Philosophie<ul>
			<li style=\"list-style: circle;\">La Gaya Scienza</li>
			</ul></li>
		<li style=\"list-style: disc;\">Science</li>
		</ul></li>
	</ul><br/>
<p>Dans ce cas, nous aurions 3 niveaux : nous pourrions appeler le premier 'type' (Fiction/Non-Fiction) et le second 'sous-type' (Science-Fiction/Romance/Philosophy), le troisi&egrave;me &eacute;tant les livres eux-m&ecirc;mes (La Gaya Scienza). Nous dirions alors que les sous-types Science-Fiction et Romance sont des enfants du type Fiction (qui est leur parent).<br/>
Normalement, le dernier niveau (ici les livres) aura le plus de champs, puisque les autres niveaux servent pour les classifier, mais l'on pourrait souhaiter une description de cat&eacute;gorie ou quelque chose...<br/>
Jusqu'&agrave; 9 niveaux sont support&eacute;s, ce qui est compl&egrave;tement arbitraire, mais je ne voyais pas pourquoi l'on pourrait en vouloir davantage.<br/>
".$lang['help_think']."</p>";

$lang['help_moduleupgrade'] = '<p>CTLModuleMaker a d&eacute;tect&eacute; que vous avez fait des modifications par rapport au fichier dna import&eacute;. Si vous le d&eacute;sirez, une fonction de mise-&agrave;-jour peut &ecirc;tre cr&eacute;&eacute;e, mais elle ne fonctionnera qu\'avec un module cr&eacute;&eacute; &agrave; partir du fichier import&eacute;.</p>
<p>Un champ renomm&eacute; est consid&eacute;r&eacute; supprim&eacute; et recr&eacute;&eacute; (ce qui implique que les donn&eacute;es qu\'il contient sont perdues lors de la mise-&agrave;-jour. Ainsi, il est important de v&eacute;rifier que vous avez bien fait les modifications suivante:</p>';

$lang['help_adminlinks'] = '<p>Pour chaque niveau, vous devez choisir quels champs seront affich&eacute;s dans la liste d\'&eacute;l&eacute;ments du panneau d\'administration du module. Gardez en t�te que ces champs ne serviront qu\'&agrave; l\'&eacute;diteur, pour lui permettre de se retrouver et d\'acc&eacute;der rapidement &agrave; certaines informations. Les autres champs seront bien s&ucirc;r modifiables en cliquant sur l\'&eacute;l&eacute;ment.<br/>En cas d\'incertitude, vous pouvez laisser ce qui est d&eacute;j&agrave; s&eacute;lectionn&eacute;.<p><p>Pour vous donner une id&eacute;e, voici ce dont pourrait avoir l\'air le panneau d\'administration d\'un niveau:</p><div style="background-color: #fff; margin: 10px; text-align: center;"><img src="../modules/CTLModuleMaker/images/help_adminfields.gif" /></div>';
$lang['help_none'] = '<p>Malheureusement, il n\'y a pas pour le moment d\'aide sp&eacute;cifique &agrave; cette &eacute;tape du processus de cr&eacute;ation. Vous pouvez toutefois consulter l\'<a href="listmodules.php?action=showmodulehelp&module=CTLModuleMaker" target="_blank">aide g&eacute;n&eacute;rale</a>.</p><p>Si vous trouvez que cette &eacute;tape n&eacute;cessiterait davantage d\'explications, vous &ecirc;tes invit&eacute; &agrave; poser vos question &agrave; <a href="http://dev.cmsmadesimple.org/projects/ctlmodulemaker/" target="_blank">La Forge</a>.</p>';

$lang['help'] = '<h2>Aide g&eacute;n&eacute;rale</h2><br/>
<p>Ce devrait &ecirc;tre assez facile &agrave; utiliser sans aide... vous pouvez toutefois regarder l\'onglet d\'aide &agrave; chaque &eacute;tape du processus de cr&eacute;ation.</p>
<p>Avant de commencer le processus de cr&eacute;ation, assurez vous que le dossier de modules est ouvert en &eacute;criture (un test d\'&eacute;criture sera ex&eacute;cut&eacute;) et qu\'il n\'y a pas d&eacute;j&agrave; un dossier portant le nom du module que vous voulez cr&eacute;er. JavaScript devrait aussi &ecirc;tre activ&eacute;.</p>
<p>Naturellement, l\'installation/d&eacute;sinstallation/mise &agrave; jour de ce module sera sans effet sur les modules d&eacute;j&agrave; cr&eacute;&eacute;s.</p>
<br/><h2>Changements r&eacute;cents &agrave; consid&eacute;rer...</h2>
<ul>
<li style="list-style-type: disc; margin-top: 4px; margin-bottom: 4px;">Une action de recherche a &eacute;t&eacute; ajout&eacute;e (voir l\'aide des modules cr&eacute;&eacute;s), permettant de rechercher par champs (dans les champs de type texte et les listes &agrave; un seul choix).</li>
<li style="list-style-type: disc; margin-top: 4px; margin-bottom: 4px;">Dans 1.7, des fonctions de <b>mise-$agrave;-jour</b> ont $eacute;t$eacute; ajout$eacute;es aux modules cr$eacute;$eacute;s. Il vous est maintenant possible, par exemple, d\'ajouter des champs &agrave; des modules existants, et ce sans avoir &agrave; les r&eacute;installer. Toutes les bases de donn&eacute;es ne supportent peut-&ecirc;tre pas cette fonctionnalit&eacute;.</li>
<li style="list-style-type: disc; margin-top: 4px; margin-bottom: 4px;">Dans 1.6.4, des classes ont &eacute;t&eacute; ajout&eacute;es au menu de pages lorsque vous s&eacute;parez vos &eacute;l&eacute;ments en pages. Vous pouvez par exemple utiliser des css comme ceux offerts <a href="http://www.mis-algoritmos.com/2007/03/16/some-styles-for-your-pagination/" target="_blank">ici</a>; j\'ai essay&eacute; d\'utiliser les m&ecirc;mes classes, mais pour des raisons d\'uniformit&eacute; je n\'ai pas utilis&eacute; de spans, donc v&eacute;rifiez les css.</li>
<li style="list-style-type: disc; margin-top: 4px; margin-bottom: 4px;">&Agrave; partir de 1.6, l\'action "list" a &eacute;t&eacute; supprim&eacute;e. L\'action "default" s\'occupe maintenant des deux - inutile, donc, de sp&eacute;cifier l\'action. Cela signifie donc que lorsque vous affichez le dernier niveau, s\'il n\'y a qu\'un seul &eacute;l&eacute;ment, le gabarit de d&eacute;tail sera utilis&eacute;, &agrave; moins que vous n\'activiez le param&egrave;tre "forcelist".</li>
<li style="list-style-type: disc; margin-top: 4px; margin-bottom: 4px;">&Agrave; partir de 1.5, les templates sont g&eacute;r&eacute;s par le cms.</li>
</ul>
<br/><br/><p>Vous &ecirc;tes invit&eacute;s &agrave; soumettre tout bug ou suggestion &agrave; la <a href="http://dev.cmsmadesimple.org/projects/ctlmodulemaker/" target="_blank">Forge</a>!</p><br/><br/>';
