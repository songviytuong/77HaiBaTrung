<?php

$actionswitch = '
		switch($action){
			case "link":
				echo $this->CreateLink($id,"default",$returnid,"",$params,"",true);
				break;
			case "changedeftemplates":
				foreach($params as $key=>$value){
				    if($key != "submit")	   $this->setPreference($key, $value);
				}
				$params = array("active_tab"=>"templates", "module_message"=>$this->lang("message_modified"));
				// no break, natural redirect to defaultadmin
			case "defaultadmin":
				require "action.defaultadmin.php";
				break;
			case "movesomething":
				require "action.movesomething.php";
				break;
			case "toggle":
				require "action.toggle.php";
				break;
			case "deletetpl":
				$newparams = array("active_tab"=>"templates");
				$deftemplates = $this->getDefaultTemplates();
			    if(isset($params["tplname"]) && !in_array($params["tplname"], $deftemplates)){
				    if($this->DeleteTemplate($params["tplname"]))	   $newparams["module_message>"] = $this->lang("message_modified");
				}
				$this->Redirect($id, "defaultadmin", $returnid, $newparams);
				break;
			case "editTemplate":
				require "action.editTemplate.php";
				break;
			case "search":
				require "action.search.php";
				break;
';



if($infos['hasfilefields']){
	$actionswitch .= '
			case "browsefiles":
				require "action.browsefiles.php";
				break;
			case "uploadFile":
				require "action.uploadFile.php";
				break;
			case "assignfile":
				require "action.assignfile.php";
				break;
';
}

foreach($levels as $level){
	$actionswitch .= '
			case "edit'.$level[1].'":
				require "action.edit'.$level[1].'.php";
				break;
';
}


$actionswitch .= '
			case "add_optionvalue":
				if(isset($params["tablename"]) && isset($params["optionname"]) && $params["optionname"] != ""){
					$tablename = cms_db_prefix()."module_'.$infos['nameofmodule'].'_".$params["tablename"]."_options";
					$db =& $this->GetDb();
					$itemid = $db->GenID($tablename."_seq");
					$query = "INSERT INTO ".$tablename." SET id=?, name=?";
					$db->Execute($query,array($itemid,$params["optionname"]));
				}
				$this->Redirect($id, "defaultadmin", $returnid, array("active_tab"=>"fieldoptions") );
				break;
			case "delete_optionvalue":
				if(isset($params["tablename"]) && isset($params["optionid"]) && $params["optionid"] > 0){
					$tablename = cms_db_prefix()."module_'.$infos['nameofmodule'].'_".$params["tablename"]."_options";
					$db =& $this->GetDb();
					$query = "DELETE FROM ".$tablename." WHERE id=? LIMIT 1";
					$db->Execute($query,array($params["optionid"]));
				}
				$this->Redirect($id, "defaultadmin", $returnid, array("active_tab"=>"fieldoptions") );
				break;
			case "rename_optionvalue":
				if(!isset($params["cancel"]) && isset($params["optionid"]) && isset($params["tablename"]) && isset($params["optionname"]) ){
					if(isset($params["submit"]) && $params["optionname"] != "" ){
						$tablename = cms_db_prefix()."module_'.$infos['nameofmodule'].'_".$params["tablename"]."_options";
						$db =& $this->GetDb();
						$query = "UPDATE ".$tablename." SET name=? WHERE id=?";
						$db->Execute($query,array($params["optionname"], $params["optionid"]));
						$this->Redirect($id, "defaultadmin", $returnid, array("active_tab"=>"fieldoptions") );
					}else{
						echo "<h2>".$this->Lang("modifyanoption")."</h2>";
						echo $this->CreateFormStart($id, "rename_optionvalue", $returnid);
						echo "<p>".$this->CreateInputText($id,"optionname",$params["optionname"],40,64)."</p>";
						echo "<p>".$this->CreateInputSubmit($id, "submit", lang("submit")).$this->CreateInputSubmit($id, "cancel", lang("cancel"))."</p>";
						echo $this->CreateInputHidden($id, "optionid", $params["optionid"]).$this->CreateInputHidden($id, "tablename", $params["tablename"]);
						echo $this->CreateFormEnd();
					}
				}else{
					$this->Redirect($id, "defaultadmin", $returnid, array("active_tab"=>"fieldoptions") );
				}
				break;
			case "default":
			default:
				require "action.default.php";
				break;
		}
';

?>
