<?php
if(!isset($gCms)) exit;

echo '<h1>'.$this->Lang('title').'</h1><br/>';

echo $this->StartTabHeaders();

	
	echo $this->SetTabHeader("creation", $this->Lang("Modulecreation"), true);
	echo $this->SetTabHeader("preferences", $this->Lang("preferences"), false);		
	echo $this->EndTabHeaders();
	echo $this->StartTabContent();
	echo $this->StartTab("creation");	
	
	####### DOING A WRITE TEST ON THE MODULE FOLDER
	if(isset($params['dotest'])){
		if($this->writeTest()){
			$this->Redirect($id, 'creation', $returnid, array('step'=>1));
		}else{
			echo '<div class="pageerrorcontainer">'.$this->Lang('error_writetest').'</div>
			';
		}
	}

	$this->echo_moduleinfos($id, $returnid);
	echo '<div class="pageerrorcontainer" id="jsen_text">'.$this->Lang('error_javascript').'</div>';
	echo '<p><h3 id="jsen_link" style="display: none;">'.$this->CreateLink($id,'defaultadmin',$returnid,'<img src="themes/default/images/icons/system/newobject.gif" border=0/> '.$this->Lang('Createamodule'),array('dotest'=>true)).'</h3></p><br/>
	<fieldset id="jsen_import" style="display: none; width: 400px;"><legend>'.$this->Lang('importmoduledna').'</legend>';
	echo '<p>'.$this->Lang('help_import').'</p>';
	echo $this->CreateFormStart($id, 'dnaimport', $returnid, 'post', 'multipart/form-data');
	echo $this->CreateFileUploadInput($id,'uploadfile').' '.$this->CreateInputSubmit($id,'submit',lang('send'));
	//if($this->GetPreference('autosave',true))	echo '<p>'.$this->Lang('help_import2').'</p>';
	echo $this->CreateFormEnd();
	$autosaved = $this->get_autosaved($id, $returnid);
	if($autosaved != '')	echo '<br/><br/><b>'.$this->Lang('autosaved').'</b><ul>'.$autosaved.'</ul>';
	echo '</fieldset>';

	echo '
		<script>
			document.getElementById("jsen_link").style.display = "block";
			document.getElementById("jsen_import").style.display = "block";
			document.getElementById("jsen_text").style.display = "none";
		</script>';

		echo $this->EndTab();

		echo $this->StartTab("preferences");
			$myprefs = $this->get_modulePreferences();
			echo $this->CreateFormStart($id, 'changeprefs', $returnid, 'post', 'multipart/form-data').'<table>';
			foreach($myprefs as $onepref){
				echo '<tr style="padding-top: px;"><td>';
				echo $this->CreateInputCheckbox($id, $onepref[0], true, $this->GetPreference($onepref[0],$onepref[1]));
				echo '</td><td>'.$this->Lang('pref_'.$onepref[0]).'</td></tr>';
			}
			echo '</table><p><br/>'.$this->CreateInputSubmit($id,'submit',lang('send')).'</p>'.$this->CreateFormEnd();
		echo $this->EndTab();

echo $this->EndTabContent();

?>
