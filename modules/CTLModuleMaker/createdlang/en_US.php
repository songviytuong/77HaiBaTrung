<?php
// this file is used by the CTLModuleMaker to create the en_US language file.
// THIS FILE IS NECESSARY
// to create other language file, simple copy, rename and edit this file. (you may also share it!)

$mylang['Add'] = 'Add';
$mylang['Edit'] = 'Edit';
$mylang['Filterby'] = 'Filter by';
$mylang['Select'] = 'Select';
$mylang['deleteprompt1'] = 'You are about to delete this';
$mylang['deleteprompt2'] = 'All children will be lost.';
$mylang['deleteprompt3'] = 'Do you wish to continue?';
$mylang['templatevars_final'] = 'Smarty variables for the detail template';
$mylang['templatevars_list'] = 'Smarty variables for list template of:';
$mylang['parenttree'] = 'You may reach the parent objects using $item->parent_object->parent_object->... and so on.';
$mylang['templatelabels'] = 'In the final level detail template, use the object $labels to print language-sensible field labels ($labels->fieldname).';


$mylang['filefields'] = '
// For file fields
$lang["Remove"] = "Remove";
$lang["browsefilestitle"] = "Select a file or upload a new file below.";
$lang["showingdir"] = "Showing directory";
$lang["browsefilesresize"] = "The picture will be automatically resized for the module.";
$lang["browsefilecurrentpath"] = "Currently seeing files in : ";
$lang["parentdir"] = "Parent Directory";
$lang["addafile"] = "Add a file";
';

$mylang['general'] = '
// strings for general fields
$lang["id"] = "id";
$lang["name"] = "Name";
$lang["alias"] = "Alias";
$lang["isdefault"] = "Is default?";
$lang["active"] = "Active";
$lang["parent"] = "Parent";
$lang["nbchildren"] = "Nb of items";
	
// GENERAL
$lang["activate"] = "Activate";
$lang["unactivate"] = "Turn off";
$lang["Yes"] = "Yes";
$lang["No"] = "No";
$lang["Actions"] = "Actions";
$lang["reorder"] = "Reorder";
$lang["listtemplate"] = "List template for";
$lang["templates"] = "Templates";
$lang["template"] = "Template";
$lang["defaulttemplates"] = "Default templates";
$lang["templatevars"] = "Template variables";
$lang["edittemplate"] = "Edit template";
$lang["deftemplatefor"] = "Default list template for level ";
$lang["defdetailtemplate"] = "Default detail template";
$lang["addtemplate"] = "Add template";
$lang["filterby"] = "Filter by";
$lang["showall"] = "Show all (no filter)";
$lang["fieldoptions"] = "Field options";
$lang["addoption"] = "Add an option";
$lang["modifyanoption"] = "Modify an option";
$lang["message_deleted"] = "Element deleted";
$lang["message_modified"] = "Modification saved";
$lang["warning_tab"] = "Notice: Save changes in other tabs before working in this one...";
$lang["error_missginvalue"] = "One or more necessary values have not been entered.";
$lang["error_alreadyexists"] = "There is already an element bearing that name.";
$lang["error_date"] = "The date you have entered is invalid.";
$lang["error_noparent"] = "No parent is defined!";
$lang["error_notfound"] = "The item could not be found.";
$lang["error_noitemfound"] = "No item found.";
$lang["finaltemplate"] = "Display template for final level ('.$finallevel.')";
$lang["prompt_deleteoption"] = "Do you really want to delete this option?";

// BREADCRUMBS :
$lang["youarehere"] = "You are here: ";
$lang["breadcrumbs_delimiter"] = " &gt; ";

// SEARCH :
$lang["searchtitle"] = "Search";
$lang["searchagain"] = "Do another search";
$lang["searchbtn"] = "Search!";
$lang["contains"] = "Contains";
$lang["isexactly"] = "Is exactly";

// MODULE INTERACTION
$lang["delete"] = "Delete";
$lang["up"] = "Up";
$lang["down"] = "Down";
$lang["settrue"] = "Set true";
$lang["setfalse"] = "Set false";
$lang["postinstall"] = "Module successfully added.";
$lang["postuninstall"] = "Module successfully removed.";
$lang["really_uninstall"] = "All of this module\'s content will be lost. Continue?";
$lang["uninstalled"] = "Module Uninstalled.";
$lang["installed"] = "Module version %s installed.";
$lang["help"] = "<h2>General Help</h2><br/>
				<p>To call the module, simply use the following tag:<br/>
				{cms_module module=\"'.$modulename.'\"}</p>
				<p>In this case the list of the last level elements ('.$finallevel.') will be displayed. To select a level, use the \"what\" parameter:<br/>
				{cms_module module=\"'.$modulename.'\" what=\"'.$finallevel.'\"}<br/>
				<i>The possible values for the \"what\" parameter are : '.$levelnames.'</i></p>
				<p>You may also ask for elements who belong to a specific parent:<br/>
				{cms_module module=\"'.$modulename.'\" parent=\"alias_of_parent\"}</p>
				<p>You may finally ask for a specific element:<br>
				{cms_module module=\"'.$modulename.'\" alias=\"alias_of_item\"}</p>
				<br/><h2>Separating into pages</h2>
				<p>You may limit the number of items to be shown on one page:<br/>
				{cms_module module=\"'.$modulename.'\" nbperpage=\"5\"}<br/>
				The page menu should then be shown with the {".\'$\'."pagemenu} tag.</p>
				<p>The elements of the page menu have some classes assigned so that you may customize it.</p><br/>
				<p>Vous pouvez utiliser l\'action \"search\" pour afficher un formulaire de recherche:<br/>
				{cms_module module=\"'.$modulename.'\" action=\"search\" what=\"'.$finallevel.'\"}</p><br/><br/>";

//EVENTS
$lang["eventdesc_modified"] = "Called after an element has been modified. Params: \"what\"=>level of the element, \"itemid\"=>id of the element, \"alias\"=>alias of the element.";
$lang["eventdesc_deleted"] = "Called after an element has been deleted. Params: \"what\"=>level of the element.";
$lang["eventdesc_added"] = "Called after an element has been added. Params: \"what\"=>level of the element, \"itemid\"=>id of the element, \"alias\"=>alias of the element.";

//PARAMETERS
$lang["phelp_action"] = "Either \"link\", \"search\" or \"default\".";
$lang["phelp_what"] = "Allows you to specify the level you wish to display. Possible values are : <i>'.$levelnames.'</i>";
$lang["phelp_alias"] = "Alias of the item you wish to display.";
$lang["phelp_parent"] = "If you wish to limit the displayed elements to those who belong to a specific parent, enter the parent alias here.";
$lang["phelp_limit"] = "Limit the number of item returned by the query (0 = no limit)";
$lang["phelp_nbperpage"] = "Set the number of items displayed on each page.";
$lang["phelp_orderby"] = "You can set to \"modified\", \"created\" or \"name\" to order items in this way. Any other value will order with the item order.";
$lang["phelp_detailpage"] = "Specify the alias of the page in which links to child elements should be sent (if none is specified, current page is used)";
$lang["phelp_showdefault"] = "Set to \"true\" if you wish to display the default element.";
$lang["phelp_random"] = "Set to a number to show a number of random elements from your query.";
$lang["phelp_finaltemplate"] = "Specify the template you wish to use for the detail view of the final level.";
$lang["phelp_listtemplate"] = "Specify the template you wish to use for the list view.";
$lang["phelp_forcelist"] = "Set to 1 if you wish to display a list view even when there is only one element.";
$lang["phelp_internal"] = "For internal use; specify the page (when using nbperpage).";
$lang["phelp_inline"] = "Makes the links inline.";
';

?>
