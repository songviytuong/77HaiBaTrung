<?php
// this file is used by the CTLModuleMaker to create the fr_FR language file.
// to create other language file, simple copy, rename and edit this file. (you may also share it!)

$mylang['Add'] = 'Ajouter';
$mylang['Edit'] = 'Modifier';
$mylang['Filterby'] = 'Filtrer par';
$mylang['Select'] = 'S&eacute;lectionner';
$mylang['deleteprompt1'] = 'Vous &ecirc;tes sur le point de supprimer ce';
$mylang['deleteprompt2'] = 'Tous les &eacute;l&eacute;ments rattach&eacute;s seront perdus.';
$mylang['deleteprompt3'] = 'Voulez-vous continuer?';
$mylang['templatevars_final'] = 'Variables Smarty pour le gabarit de d&eacute;tail';
$mylang['templatevars_list'] = 'SVariables Smarty pour le gabarit de liste de:';
$mylang['parenttree'] = 'Vous pouvez acc&eacute;der aux objets parents en utilisant $item->parent_object->parent_object->... (et ainsi de suite)';
$mylang['templatelabels'] = 'Dans le gabarit de d&eacute;tail du niveau final, utilisez l\\\'objet $labels pour afficher les &eacute;tiquettes de champs selon la langue ($labels->nom_du_champ).';

$mylang['filefields'] = '
// For file fields
$lang["Remove"] = "Enlever";
$lang["browsefilestitle"] = "Choisissez un fichier ou uploadez un nouveau fichier plus bas.";
$lang["showingdir"] = "R&eacute;pertoire";
$lang["browsefilesresize"] = "L\'image sera automatiquement redimensionn&eacute;e pour le module.";
$lang["browsefilecurrentpath"] = "R&eacute;pertoire : ";
$lang["parentdir"] = "R&eacute;pertoire parent";
$lang["addafile"] = "Ajouter un fichier";
';

$mylang['general'] = '
// strings for general fields
$lang["id"] = "id";
$lang["name"] = "Nom";
$lang["alias"] = "Alias";
$lang["isdefault"] = "Par d&eacute;faut?";
$lang["active"] = "Actif";
$lang["parent"] = "Parent";
$lang["nbchildren"] = "Nb d\'&eacute;l&eacute;ments";
	
// GENERAL
$lang["activate"] = "Activer";
$lang["unactivate"] = "D&eacute;sactiver";
$lang["Yes"] = "Oui";
$lang["No"] = "Non";
$lang["Actions"] = "Actions";
$lang["reorder"] = "R&eacute;ordonner";
$lang["listtemplate"] = "Gabarit de liste pour";
$lang["defaulttemplates"] = "Gabarits par d&eacute;faut";
$lang["templates"] = "Gabarits";
$lang["template"] = "Gabarit";
$lang["edittemplate"] = "Modifier le gabarit";
$lang["templatevars"] = "Variables de gabarit";
$lang["deftemplatefor"] = "Gabarit de liste par d&eacute;faut pour le niveau ";
$lang["defdetailtemplate"] = "Gabarit de d&eacute;tail par d&eacute;faut";
$lang["addtemplate"] = "Ajouter un gabarit";
$lang["filterby"] = "Filtrer par";
$lang["showall"] = "Tout voir (aucun filtre)";
$lang["fieldoptions"] = "Options de champs";
$lang["addoption"] = "Ajouter une option";
$lang["modifyanoption"] = "Modifier une option";
$lang["message_deleted"] = "&Eacute;l&eacute;ment supprim&eacute;.";
$lang["message_modified"] = "Modification sauvegard&eacute;e.";
$lang["warning_tab"] = "Attention: Sauvegardez les changements dans les autres onglets avant de travailler dans celui-ci...";
$lang["error_missginvalue"] = "Une ou plusieurs valeur obligatoires n\'ont pas &eacute;t&eacute; entr&eacute;es.";
$lang["error_alreadyexists"] = "Il y a d&eacute;j&agrave; un &eacute;l&eacute;ment portant ce nom.";
$lang["error_date"] = "La date que vous avez entr&eacute;e est invalide.";
$lang["error_noparent"] = "Aucun parent d&eacute;fini!";
$lang["error_notfound"] = "L\'&eacute;l&eacute;ment n\'a pas pu &ecirc;tre trouv&eacute;.";
$lang["error_noitemfound"] = "Aucun &eacute;l&eacute;ment trouv&eacute;.";
$lang["finaltemplate"] = "Gabarit pour le niveau final ('.$finallevel.')";
$lang["prompt_deleteoption"] = "Voulez-vous vraiment supprimer cette option?";

// BREADCRUMBS :
$lang["youarehere"] = "Vous &ecirc;tes ici: ";
$lang["breadcrumbs_delimiter"] = " &gt; ";

// SEARCH :
$lang["searchtitle"] = "Recherche";
$lang["searchagain"] = "Recommencer la recherche";
$lang["searchbtn"] = "Chercher!";
$lang["contains"] = "Contiens";
$lang["isexactly"] = "Est egal &agrave;";

// MODULE INTERACTION
$lang["delete"] = "Supprimer";
$lang["up"] = "Monter";
$lang["down"] = "Baisser";
$lang["settrue"] = "Activer";
$lang["setfalse"] = "D&eacute;sactiver";
$lang["postinstall"] = "Module ajout&eacute;.";
$lang["postuninstall"] = "Module d&eacute;sinstall&eacute;.";
$lang["really_uninstall"] = "Tout le contenu du module sera perdu. Poursuivre?";
$lang["uninstalled"] = "Module d&eacute;sinstall&eacute;.";
$lang["installed"] = "Module version %s install&eacute;.";
$lang["help"] = "<h2>Aide g&eacute;n&eacute;rale</h2><br/>
				<p>Pour appeler le module, utilisez la balise suivante :<br/>
				{cms_module module=\"'.$modulename.'\"}</p>
				<p>Dans ce cas une liste des &eacute;l&eacute;ments du dernier niveau ('.$finallevel.') sera affich&eacute;e. Pour sp&eacute;cifier un niveau, utilisez le param&egrave;tre \"what\" :<br/>
				{cms_module module=\"'.$modulename.'\" what=\"'.$finallevel.'\"}<br/>
				<i>Les valeurs possibles pour \"what\" sont : '.$levelnames.'</i></p>
				<p>Vous pouvez aussi demander les &eacute;l&eacute;ments qui appartiennent &agrave; un parent sp&eacute;cifique:<br/>
				{cms_module module=\"'.$modulename.'\" parent=\"alias_of_parent\"}</p>
				<p>Vous pouvez finalement appeler un eacute;leacute;ment en particulier:<br>
				{cms_module module=\"'.$modulename.'\" alias=\"alias_of_item\"}</p>
				<br/><h2>S&eacute;paration en pages</h2>
				<p>Vous pouvez limiter le nombre d\'&eacute;l&eacute;ments devant &ecirc;tre affich&eacute;s sur une m&ecirc;me page:<br/>
				{cms_module module=\"'.$modulename.'\" nbperpage=\"5\"}<br/>
				Le menu des pages est ensuite affich&eacute; avec la balise {".\'$\'."pagemenu}</p>
				<p>Des classes ont &eacute;t&eacute; assign&eacute;es aux &eacute;l&eacute;ments du menu de page pour que vous puissez le personnaliser.</p><br/>
				<p>Vous pouvez utiliser l\'action \"search\" pour afficher un formulaire de recherche:<br/>
				{cms_module module=\"'.$modulename.'\" action=\"search\" what=\"'.$finallevel.'\"}</p><br/><br/>";

//EVENTS
$lang["eventdesc_modified"] = "Appel&eacute; apr&egrave;s qu\'un &eacute;l&eacute;ment soit modifi&eacute;.";
$lang["eventdesc_deleted"] = "Appel&eacute; apr&egrave;s qu\'un &eacute;l&eacute;ment soit supprim&eacute;.";
$lang["eventdesc_added"] = "Appel&eacute; apr&egrave;s qu\'un &eacute;l&eacute;ment soit ajout&eacute;.";
$lang["eventhelp_modified"] = "Param&egrave;tres: \"what\"=>niveau de l\'&eacute;l&eacute;ment, \"itemid\"=>id de  l\'&eacute;l&eacute;ment, \"alias\"=>alias de l\'&eacute;l&eacute;ment.";
$lang["eventhelp_deleted"] = "Param&egrave;tres: \"what\"=>niveau de l\'&eacute;l&eacute;ment.";
$lang["eventhelp_added"] = "Param&egrave;tres: \"what\"=>niveau de l\'&eacute;l&eacute;ment, \"itemid\"=>id de  l\'&eacute;l&eacute;ment, \"alias\"=>alias de l\'&eacute;l&eacute;ment.";

//PARAMETERS
$lang["phelp_action"] = "Soit \"link\", \"search\" ou \"default\".";
$lang["phelp_what"] = "Permet de sp&eacute;cifier le niveau. Les valeurs possibles sont : <i>'.$levelnames.'</i>";
$lang["phelp_alias"] = "Alias de l\'&eacute;l&eacute;ment que vous voulez afficher.";
$lang["phelp_parent"] = "Si vous voulez limiter les &eacute;l&eacute;ments affich&eacute;s &agrave; ceux appartenant &agrave; un parent particulier, entrez ici l\'alias du parent.";
$lang["phelp_limit"] = "Limite le nombre d\'item retourn&eacute;s par la requ&ecirc;te (0 = pas de limite)";
$lang["phelp_nbperpage"] = "Limite le nombre d\'&eacute;l&eacute;ments affich&eacute;s par page.";
$lang["phelp_orderby"] = "Valeurs possibles : \"modified\", \"created\" et \"name\". Toute autre valeur ordonnera en fonction de l\'ordre normal des items.";
$lang["phelp_detailpage"] = "Sp&eacute;cifie l\'alias de la page vers laquelle pointeront les liens (si rien n\'est sp&eacute;cifi&eacute;, la page actuelle est utilis&eacute;e)";
$lang["phelp_showdefault"] = "Mettre \"true\" si vous voulez afficher l\'&eacute;l&eacute;ment par d&eacute;faut.";
$lang["phelp_random"] = "Entrez un nombre pour afficher ce nombre d\'&eacute;l&eacute;ments al&eacute;atoires.";
$lang["phelp_finaltemplate"] = "Permet de sp&eacute;cifier le gabarit &agrave; utiliser pour l\'affichage .";
$lang["phelp_listtemplate"] = "Permet de sp&eacute;cifier le gabarit &agrave; utiliser pour l\'affichage de liste d&eacute;taill&eacute; du niveau final.";
$lang["phelp_forcelist"] = "Mettre &agrave; 1 si vous voulez afficher en format list m&ecirc;me lorsqu\'il n\'y a qu\'un &eacute;l&eacute;ment (affecte le niveau final seulement).";
$lang["phelp_internal"] = "Pour usage interne; sp&eacute;cifie la page (lorsque nbperpage est utilis&eacute;.";
$lang["phelp_inline"] = "Met les liens \"inline\".";
';

?>
