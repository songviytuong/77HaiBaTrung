<?php
if(!isset($levels) || !isset($linktables)) exit;

$installmethod = '<?php
if (!isset($gCms)) exit;
$db = $this->GetDb();
$dict = NewDataDictionary($db);	
// mysql-specific, but ignored by other database
$taboptarray = array("mysql" => "TYPE=MyISAM");
		
';

$leveldepth = 0;
$nblevel = count($levels);

foreach($levels as $level){
// ###################################### BEGINNING OF LEVEL

$leveldepth++;
$installmethod .= '
// Creates the '.$level[0].' table
$flds = "';

$firstfield = true;
foreach($level[4] as $field){
	$fieldtype = $this->GetDBFieldType($field[1], $field[5]['listmode']);
	if($field[0] == "id") $fieldtype = "I KEY";
	if($field[0] == "active" || $field[0] == "isdefault") $fieldtype = "L";

	if($field[1] != 11){
		$installmethod .= ($firstfield?"":",").'
	'.$field[0].' '.$fieldtype;
		$firstfield = false;
	}
}
$installmethod .= ',
    date_modified ".CMS_ADODB_DT."
	";

$sqlarray = $dict->CreateTableSQL(cms_db_prefix()."module_'.$modulename.'_'.$level[0].'", $flds, $taboptarray);
$dict->ExecuteSQLArray($sqlarray);
$db->CreateSequence(cms_db_prefix()."module_'.$modulename.'_'.$level[0].'_seq");

';
// ###################################### END OF LEVEL
}

foreach($linktables as $linktable){

$installmethod .= '
// Creates the link table '.$linktable[0].'_has_'.$linktable[1].'
$flds = "
	'.$linktable[0].'_id I,
	'.$linktable[1].'_id I
	";

$sqlarray = $dict->CreateTableSQL(cms_db_prefix()."module_'.$modulename.'_'.$linktable[0].'_has_'.$linktable[1].'", $flds, $taboptarray);
$dict->ExecuteSQLArray($sqlarray);

';

}

foreach($listfields as $oneitem){

$installmethod .= '
// Creates the '.$oneitem[1].' options table
$flds = "
	id I,
	name C(32),
	item_order I
	";

$sqlarray = $dict->CreateTableSQL(cms_db_prefix()."module_'.$modulename.'_'.$oneitem[0].'_'.$oneitem[1].'_options", $flds, $taboptarray);
$dict->ExecuteSQLArray($sqlarray);
$db->CreateSequence(cms_db_prefix()."module_'.$modulename.'_'.$oneitem[0].'_'.$oneitem[1].'_options_seq");

';

}


if(count($multiplefiles) > 0) {

$installmethod .= '
// Creates the table for multiple files
$flds = "
    fileid I,
	itemid I,
	fieldname C(64),
	filepath C(255)
	";

$sqlarray = $dict->CreateTableSQL(cms_db_prefix()."module_'.$modulename.'_multiplefilesfields", $flds, $taboptarray);
$dict->ExecuteSQLArray($sqlarray);
$db->CreateSequence(cms_db_prefix()."module_'.$modulename.'_multiplefilesfields_seq");


';

}

$installmethod .= '
// INSERTING EXAMPLE TEMPLATES
	$template = \'<h2>{$leveltitle}</h2><p>{$breadcrumbs}</p>
<ul>
{foreach from=$itemlist item="item"}
	<li {if $item->is_selected}class="active"{/if}>{$item->detaillink}</li>
{/foreach}
</ul>\';
$this->SetTemplate("list_default",$template,$this->GetName());';

foreach($levels as $level){
	$installmethod .= '
    $this->SetPreference("listtemplate_'.$level[0].'","list_default");';
}
if($globaltemplate){
	$installmethod .= '
	$template = \'<p>{$breadcrumbs}</p><h3>{$item->name}</h3>\';
	$this->SetTemplate("final_default",$template,$this->GetName());
    $this->SetPreference("finaltemplate","final_default");
';
}


// CREATING PERMISSIONS :

$installmethod .= '

// permissions';

foreach($levels as $level){
	$installmethod .= '
	$this->CreatePermission("'.$modulename.'_manage_'.$level[0].'", "'.$modulename.': Manage '.$level[0].'");';
}


$installmethod .= '
// events
	$this->CreateEvent("'.$modulename.'_added");
	$this->CreateEvent("'.$modulename.'_modified");
	$this->CreateEvent("'.$modulename.'_deleted");
	
// prepare information for an eventual upgrade
	$this->SetPreference("makerversion","'.$this->GetVersion().'");

// put mention into the admin log
	$this->Audit( 0, $this->Lang("friendlyname"), $this->Lang("installed",$this->GetVersion()));

?>
';

?>
