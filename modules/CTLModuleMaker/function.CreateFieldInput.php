<?php
if(!isset($gCms) || !isset($innerid)) exit;
$YesNoOptions = array($this->Lang('No')=>0,$this->Lang('Yes')=>1);

echo '<fieldset style="width: 625px;"><legend><b>'.$this->Lang('Field').' '.$innerid.'</b></legend>';
$this->display_errors($fieldvalues[6]);
echo $this->DoInputLine('name',$this->CreateInputText($id, 'field'.$innerid.'_name', isset($fieldvalues[0])?$fieldvalues[0]:'', 30, 16),true);
echo $this->DoInputLine('type',$this->CreateInputDropdown($id, 'field'.$innerid.'_type', $this->GetFieldTypes(), -1, isset($fieldvalues[1])?$fieldvalues[1]:'',' onchange="displayoptions('.$innerid.',this.value);"'),true);
echo $this->DoInputLine('needed',$this->CreateInputDropdown($id, 'field'.$innerid.'_needed', $YesNoOptions, -1, isset($fieldvalues[2])?$fieldvalues[2]:0));
echo $this->DoInputLine('indexed',$this->CreateInputDropdown($id, 'field'.$innerid.'_indexed', $YesNoOptions, -1, isset($fieldvalues[3])?$fieldvalues[3]:0));

// We hide the default-value input, as it's not yet fully supported...
echo $this->DoInputLine('default',$this->CreateInputText($id, 'field'.$innerid.'_default', isset($fieldvalues[4])?$fieldvalues[4]:'', 30),false,'style="display: none;"');

echo $this->DoInputLine('listoptions',$this->CreateInputText($id, 'field'.$innerid.'_listoptions', isset($fieldvalues[5]['listoptions'])?$fieldvalues[5]['listoptions']:'', 30),true,'id="field'.$innerid.'optgroup1" style="display: none;"');
echo $this->DoInputLine('upfolder',$this->CreateInputText($id, 'field'.$innerid.'_upfolder', isset($fieldvalues[5]['upfolder'])?$fieldvalues[5]['upfolder']:'', 30),false,'id="field'.$innerid.'optgroup2" style="display: none;"');
echo $this->DoInputLine('size',$this->CreateInputText($id, 'field'.$innerid.'_size', isset($fieldvalues[5]['size'])?$fieldvalues[5]['size']:'', 30),false,'id="field'.$innerid.'optgroup3" style="display: none;"', '<br/>'.$this->Lang('warning_resize'));
echo $this->DoInputLine('thumb',$this->CreateInputText($id, 'field'.$innerid.'_thumb', isset($fieldvalues[5]['thumb'])?$fieldvalues[5]['thumb']:'', 30),false,'id="field'.$innerid.'optgroup4" style="display: none;"');
echo $this->DoInputLine('listmode',$this->CreateInputDropdown($id, 'field'.$innerid.'_listmode', array($this->Lang('dropdown') => 1, $this->Lang('select') => 2, $this->Lang('radiobuttons') => 3, $this->Lang('checkbox') => 4), -1, isset($fieldvalues[5]['listmode'])?$fieldvalues[5]['listmode']:0),false,'id="field'.$innerid.'optgroup5" style="display: none;"');
echo '</fieldset><br/><br/>';
if (isset($fieldvalues[1]))	echo '<script>	displayoptions('.$innerid.','.$fieldvalues[1].');	</script>'
?>
