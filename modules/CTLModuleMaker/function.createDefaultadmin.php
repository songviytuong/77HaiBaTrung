<?php

$modulename = $infos['nameofmodule'];

$defaultadmin = '<?php
if (!isset($gCms)) exit;
$admintheme = $gCms->variables["admintheme"];
$active_tab = isset($params["active_tab"])?$params["active_tab"]:"'.$levels[0][0].'";

echo $this->StartTabHeaders();';

foreach($levels as $level){
	$defaultadmin .= '
	if( $this->CheckPermission("'.$modulename.'_manage_'.$level[0].'") ) {
		echo $this->SetTabHeader("'.strtolower($level[0]).'", $this->Lang("'.$level[0].'_plural"), "'.$level[0].'" == $active_tab ? true : false);
	}';
}
if(count($listfields) > 0){
	$defaultadmin .= '
	if( $this->CheckPermission("Modify Templates") ) {
		echo $this->SetTabHeader("fieldoptions", $this->Lang("fieldoptions"), "fieldoptions" == $active_tab ? true : false);		
	}';
}
$defaultadmin .= '
	if( $this->CheckPermission("Modify Templates") ) {
		echo $this->SetTabHeader("templates", $this->Lang("templates"), "templates" == $active_tab ? true : false);		
	}
echo $this->EndTabHeaders();



echo $this->StartTabContent();
';

#####################################
## CONTENT

$i = 0;
while($i < count($levels)){
	$level = $levels[$i];
	$defaultadmin .= '

if( $this->CheckPermission("'.$modulename.'_manage_'.$level[0].'") ) {
	echo $this->StartTab("'.strtolower($level[0]).'");
';
	if($i != 0) $defaultadmin .= '
	if($this->countsomething("'.$levels[$i - 1][0].'") > 0){
';
	if($i != 0 && $levels[$i -1][2] == 0){

		$defaultadmin .= '
			echo \'<div style="float: right;">\';
			if(!isset($params["showonly"])){
				$filteroptions = $this->get_options("'.$levels[$i-1][0].'");
				echo $this->CreateFormStart($id, "defaultadmin", $returnid);
					echo $this->Lang("filterby_'.$levels[$i-1][0].'")." : ";
					echo $this->CreateInputDropdown($id, "showonly", $filteroptions, -1);
					echo $this->CreateInputHidden($id, "active_tab", "'.$level[0].'");
					echo " ".$this->CreateInputSubmit($id, "submit", lang("submit"));
				echo $this->CreateFormEnd();
			}else{
				echo "<p>".$this->CreateLink($id, "defaultadmin", $returnid, $this->Lang("showall"), array("active_tab" => "'.$level[0].'"))."</p>";
				$whereclause = array("parent_id"=>$params["showonly"]);
			}
			echo "</div>";
			';
		}
		$defaultadmin .= '
			echo "<p>".$this->CreateLink($id, "edit'.$level[1].'", $returnid, $admintheme->DisplayImage("icons/system/newobject.gif", "","","","systemicon")." ".$this->Lang("add_'.$level[0].'"))."</p>";
			$itemlist = $this->get_level_'.$level[0].'(isset($whereclause)?$whereclause:array(),true, $id, $returnid);
			$this->smarty->assign("itemlist", $itemlist);
			$adminshow = array(';
		foreach($level[5] as $field){
			$fieldname = $level[0].'_'.$field;
			if($field == 'editlink' || $field == 'name'){
				$fieldname = 'name';
				$field = 'editlink';
			}elseif($field == 'alias'){
				$fieldname = 'alias';
				$field = 'alias';
			}elseif($field == 'movelinks'){
				$fieldname = 'reorder';
			}elseif($field == 'parent'){
				$fieldname = $levels[$i -1][0];
				$field = 'parent_name';
			}elseif($field == 'active'){
				$fieldname = 'active';
				$field = 'toggleactive';
			}elseif($field == 'isdefault'){
				$fieldname = 'isdefault';
				$field = 'toggledefault';
			}elseif($field == 'nbchildren'){
				$fieldname = $field = 'nbchildren';
			}else{
				// we find out what kind of field this is:
				$realfield = false;
				foreach($level[4] as $tmpfield){
					if($tmpfield[0] == $field) $realfield = $tmpfield;
				}
				if($realfield[1] == 6 || $realfield[1] == 7){
					// if it's a list field, display only with single values
					if(isset($realfield[5]['multiple']) && $realfield[5]['multiple'] == 1){
						$field = false;
					}else{
						$field = $field.'_namevalue';
					}
				}
			}
			if($field && ($field != 'parent_name' || $levels[$i-1][2] == 0)){
				$defaultadmin .= '
				array($this->Lang("'.$fieldname.'"),"'.$field.'"),';
			}
		}
			$defaultadmin .= '
				array($this->Lang("Actions"),"deletelink")		
				);
			$this->smarty->assign("adminshow", $adminshow);
			echo $this->ProcessTemplate("adminpanel.tpl");
			echo "<p>".$this->CreateLink($id, "edit'.$level[1].'", $returnid, $admintheme->DisplayImage("icons/system/newobject.gif", "","","","systemicon")." ".$this->Lang("add_'.$level[0].'"))."</p>";
';
	if($i != 0) $defaultadmin .= '
	}else{
		echo "<p>".$this->Lang("error_noparent")."</p>";
	}
';

$defaultadmin .= '
	echo $this->EndTab();

}';
	$i++;
}

if(count($listfields) > 0){

$defaultadmin .= '

if( $this->CheckPermission("Modify Templates") || $this->CheckPermission("'.$modulename.'_manage_'.$levels[0][0].'") ) {
	echo $this->StartTab("fieldoptions");
';

foreach($listfields as $field){
	$defaultadmin .= '
	echo "<fieldset><legend><b>('.$field[0].') '.$field[1].' :</b></legend><ul>";
	$options = $this->get_options("'.$field[0].'_'.$field[1].'_options");
	foreach($options as $key=>$value){
		echo "<li> $key ";
		echo $this->CreateLink($id, "rename_optionvalue", $returnid, $admintheme->DisplayImage("icons/system/edit.gif", lang("edit"),"","","systemicon"), array("optionname"=>$key, "tablename"=>"'.$field[0].'_'.$field[1].'", "optionid"=>$value))." ";
		echo $this->CreateLink($id, "delete_optionvalue", $returnid, $admintheme->DisplayImage("icons/system/delete.gif",$this->Lang("delete"),"","","systemicon"), array("optionid"=>$value, "tablename"=>"'.$field[0].'_'.$field[1].'"), $this->Lang("prompt_deleteoption"));
		echo "</li>";
	}
	echo "</ul>";
	echo "<p>".$this->CreateFormStart($id, "add_optionvalue");
		echo "<p>".$this->Lang("addoption").": ".$this->CreateInputText($id, "optionname", "", 20, 64);
		echo $this->CreateInputHidden($id, "tablename", "'.$field[0].'_'.$field[1].'");
		echo " ".$this->CreateInputSubmit($id, "submit", lang("submit"))."</p>";
	echo $this->CreateFormEnd();
	echo "</p></fieldset><br/><br/>";';
}

$defaultadmin .= '
	echo $this->EndTab();
}
';
}


$defaultadmin .= '

if( $this->CheckPermission("Modify Templates") ) {
	echo $this->StartTab("templates");

    echo "<h2>".$this->Lang("defaulttemplates")."</h2>";
    echo $this->CreateFormStart($id, "changedeftemplates", $returnid);
    $templatelist = $this->ListTemplates($this->GetName());
    $deftpls = $this->getDefaultTemplates();
    $tploptions = array();
    $itemlist = array();
    foreach($templatelist as $onetpl){
	   $tploptions[$onetpl] = $onetpl;
	   $tpl = new stdClass();
	   $tpl->editlink = $this->CreateLink( $id, "editTemplate", $returnid, $onetpl, array("tplname"=>$onetpl) );
	   $tpl->deletelink = in_array($onetpl, $deftpls)?"":$this->CreateLink( $id, "deletetpl", $returnid, $admintheme->DisplayImage("icons/system/delete.gif", $this->Lang("delete"), "", "", "systemicon"), array("tplname"=>$onetpl) );
	   array_push($itemlist, $tpl);
    }
';
    foreach($levels as $level){
	   $defaultadmin .= '
	   echo "	<div class=\"pageoverflow\">
			 <p class=\"pagetext\">".$this->Lang("deftemplatefor")." \"'.$level[0].'\":</p>
			 <p class=\"pageinput\">".$this->CreateInputDropdown($id,"listtemplate_'.$level[0].'",$tploptions,-1,$this->GetPreference("listtemplate_'.$level[0].'"))."</p>
		</div>
    ";';
    }
$defaultadmin .= '
    echo "	<div class=\"pageoverflow\">
			 <p class=\"pagetext\">".$this->Lang("defdetailtemplate").":</p>
			 <p class=\"pageinput\">".$this->CreateInputDropdown($id,"finaltemplate",$tploptions,-1,$this->GetPreference("finaltemplate"))."</p>
		</div>
    <p>".$this->CreateInputSubmit($id, "submit", lang("submit"))."</p>";
    echo $this->CreateFormEnd();

    echo "<br/><br/><hr/><h2>".$this->Lang("templates")."</h2>";
    $this->smarty->assign("itemlist", $itemlist);
    $adminshow = array(	array($this->Lang("template"), "editlink"), array($this->Lang("Actions"), "deletelink")	);
    $this->smarty->assign("adminshow", $adminshow);
    echo $this->ProcessTemplate("adminpanel.tpl");
    echo "<p>".$this->CreateLink($id, "editTemplate", $returnid, $admintheme->DisplayImage("icons/system/newobject.gif", "","","","systemicon")." ".$this->Lang("addtemplate"))."</p>";

	echo $this->EndTab();
}

echo $this->EndTabContent();

?';
$defaultadmin .= '>';

?>
