	
function plResize($fullpath, $prefix, $newwidth, $newheight=false, $transparency=true) {
	$name = str_replace('//','/',$fullpath);
	if(!file_exists($name))	return false;
	$image_infos = getimagesize($name);
	if($image_infos[0] > $newwidth || ($newheight != false && $image_infos[1] > $newheight)){
		if($prefix == ''){
			$newname = $name;
		}else{
			$tmparr = explode('/',$name);
			$filename = $tmparr[count($tmparr) - 1];
			$newname = str_replace($filename, $prefix.$filename, $name);
		}
		$arr = split("\.",$name);
		$ext = $arr[count($arr)-1];

		if($ext=="jpeg" || $ext=="jpg"){
			$img = @imagecreatefromjpeg($name);
		} elseif($ext=="png"){
			$img = @imagecreatefrompng($name);
		} elseif($ext=="gif") {
			$img = @imagecreatefromgif($name);
		}
		if(!$img)   return false;

		$old_x = imageSX($img);
		$old_y = imageSY($img);

		$ratio = $newheight?min($newwidth / $image_infos[0], $newheight / $image_infos[1]):($newwidth / $image_infos[0]);
		$thumb_h = $image_infos[1] * $ratio;
		$thumb_w = $image_infos[0] * $ratio;

		$new_img = ImageCreateTrueColor($thumb_w, $thumb_h);
	   
		if($transparency) {
			if($ext=="png") {
				imagealphablending($new_img, false);
				$colorTransparent = imagecolorallocatealpha($new_img, 0, 0, 0, 127);
				imagefill($new_img, 0, 0, $colorTransparent);
				imagesavealpha($new_img, true);
			} elseif($ext=="gif") {
				$trnprt_indx = imagecolortransparent($img);
				if ($trnprt_indx >= 0) {
					//its transparent
					$trnprt_color = imagecolorsforindex($img, $trnprt_indx);
					$trnprt_indx = imagecolorallocate($new_img, $trnprt_color['red'], $trnprt_color['green'], $trnprt_color['blue']);
					imagefill($new_img, 0, 0, $trnprt_indx);
					imagecolortransparent($new_img, $trnprt_indx);
				}
			}
		} else {
			Imagefill($new_img, 0, 0, imagecolorallocate($new_img, 255, 255, 255));
		}
		imagecopyresampled($new_img, $img, 0,0,0,0, $thumb_w, $thumb_h, $old_x, $old_y);

		if(file_exists($newname))	@unlink($newname);

		if($ext=="jpeg" || $ext=="jpg"){
			imagejpeg($new_img, $newname);
			$return = true;
		} elseif($ext=="png"){
			imagepng($new_img, $newname);
			$return = true;
		} elseif($ext=="gif") {
			imagegif($new_img, $newname);
			$return = true;
		}
		imagedestroy($new_img);
		imagedestroy($img);
		if($return) $return = $newname;
		return $return;
	}else{
		return $name;
	}
}