<?php
## action.Browsefiles.php
## By: Pierre-Luc Germain
## PARAMS:
## $params['mode'] : images || files
## $params['extensions'] : array of shown file extensions
## $params['startdir'] : the folder to start at ( uploads/ [startdir] / )
## $params['tablename'] : the name of the db table
## $params['prefix'] : the variable prefix
## $params[prefix.'id'] : the id of the item to which the image will be assigned (value is not used, but returned at the end)
## $params['field'] : only a pass-through to the assign action

if (!isset($params['mode']) || !isset($params['field']) || !isset($params['prefix']) || !isset($params['field']) || !isset($params['tablename']) ) exit;

## defining the allowed extensions:
$mediaext = array('swf','flv','jpeg','jpg','png','gif');
$docext = array('doc','docx','xls','xlsx','rtf','txt','pdf');

if($params['mode'] == 'images' || $params['mode'] == 'image') {
	$mode = 'image';
	$allowedext = $mediaext;
}else{
	$mode = 'other';
	$allowedext = array_merge($mediaext,$docext);
}
if(isset($params['extensions']))	$allowedext = $params['extensions'];

$outputstuff = '';

function strlimit($string){
	if (substr($string,0,21) == $string){
		return $string;
	}else{
		return substr($string,0,19).'...';
	}
}

function buildlinkcontent($baseurl, $filename, $mode, $dir=''){
	$extension = strtolower(substr(strrchr($filename, "."), 1));
	if($extension == 'flv' || $extension == 'swf'){
		return ($mode == 'image'?'<div style="text-align: center;">':'').'<img border=0 src="themes/default/images/icons/filetypes/fmedia.gif" style="padding-right: 5px;"/> '.($mode == 'image'?'</div>'.strlimit($filename):$filename);
	}elseif(in_array($extension, array('jpeg','jpg','png','gif')) && $mode == 'image'){
		$thumbname = 'thumb_'.$filename;
		list($width, $height, $type, $attr) = getimagesize($dir.DIRECTORY_SEPARATOR.$filename);
		if (file_exists($dir.DIRECTORY_SEPARATOR.$thumbname)) {
			return '<div style="text-align: center;"><img border=0 src="'.$baseurl.'/'.$thumbname.'" /></div>'.strlimit($filename).'<br>'.$width.'x'.$height;
		}else{
			$thumbnailratio = max($width / 106,$height / 106);
			if ($thumbnailratio < 1) $thumbnailratio = 1;
			$theight = floor($height / $thumbnailratio);
			$twidth = floor($width / $thumbnailratio);
			return '<div style="text-align: center;"><img border=0 src="'.$baseurl.'/'.$filename.'" width="'.$twidth.'" height="'.$theight.'" /></div>'.strlimit($filename).'<br>'.$width.'x'.$height;
		}
	}else{
		return ($mode == 'image'?'<div style="text-align: center;">':'').'<img border=0 src="themes/default/images/icons/filetypes/fdoc.gif" style="padding-right: 5px;"/> '.($mode == 'image'?'</div>'.strlimit($filename):$filename);
	}
}
$divoption = ($mode == 'image')?'<div style="text-align: center; float: left; width: 165px; height: 180px;"><br/>':'<li style="margin: 5px;">';
$closediv = ($mode == 'image')?'</div>':'</li>
';

$startdir = isset($params['startdir'])?$params['startdir']:'';
if($startdir != '' && substr($startdir,0,1) != '/' && substr($startdir,0,1) != '\\') $startdir = '/'.$startdir;
if(!file_exists($gCms->config['uploads_path'].$startdir)) mkdir($gCms->config['uploads_path'].'/'.$startdir);


$parentdir = (isset($params['parentdir']) ? $params['parentdir'] : '');
$curdir = (isset($params['curdir']) ? $params['curdir'] : '');
$dir = $gCms->config['uploads_path'].$startdir.$curdir;
if (FALSE == is_dir($dir)){
	$dir = $gCms->config["uploads_path"];
	$curdir = '';
	$parentdir = '';
}
$baseurl = $gCms->config['uploads_url'].$startdir.$curdir;

if (isset($params['deletefile'])){
	if(file_exists($dir.'/'.$params['deletefile'])) unlink($dir.'/'.$params['deletefile']);
}

echo '<h2>'.$this->Lang('browsefilestitle').'</h2>';

$dh  = opendir($dir);

echo '<p>'.$this->Lang('showingdir').' : '.$dir.'</p>';

echo '<div id="fileselect" class="pageoverflow" style="padding: 4px; margin-bottom: 10px; border: 1px solid #666666; background: #FFFFFF; overflow: auto;">';
echo ($mode == 'image'?'<div style="clear: both;">':'<ul>');


#################### POPULATING FOLDERS ####################

while (false !== ($filename = readdir($dh)))
{
	if (is_dir($dir.DIRECTORY_SEPARATOR.$filename) && $filename!='.'){
			if ($filename=='..')
			{
				if ($curdir != ''){
					echo $divoption;
					$newparams = array_merge($params, array('curdir' => $parentdir));
					echo $this->CreateLink($id, $action, $returnid, '<img border=0 src="themes/default/images/icons/filetypes/upfolder.gif" /> '.($mode=='image'?'<br/>':'').$this->Lang('parentdir'), $newparams);
					echo $closediv;
				}
			}
			else
			{
				echo $divoption;
				$newparams = array_merge($params, array('curdir' => $curdir.'/'.$filename, 'parentdir' => $curdir));
				echo $this->CreateLink($id, $action, $returnid, ($mode=='image'?'<div style="text-align: center;"><img border=0 src="../lib/filemanager/ImageManager/img/folder.gif" /></div>'.$filename:'<img border=0 src="themes/default/images/icons/filetypes/folder.gif" /> '.$filename), $newparams);
				echo $closediv;
			}
	}
}

if (isset($dh))	closedir($dh);
$dh  = opendir($dir);


################### POPULATING FILES #######################


while (false !== ($filename = readdir($dh)))
{
	$extension = strtolower(substr(strrchr($filename, "."), 1));
	if (in_array($extension, $allowedext)) {
		if (substr($filename,0,6) != 'thumb_' && substr($filename,0,8) != 'plthumb_') {
			echo $divoption;
			$newparams = array_merge($params, array('filename'=>$filename, 'filepath' => $curdir.'/'.$filename));
			echo $this->CreateLink($id, 'assignfile', $returnid, buildlinkcontent($baseurl, $filename, $mode, $dir), $newparams);
			$newparams = array_merge($params, array('deletefile'=>$filename));
			echo ' '.$this->CreateLink($id, $action, $returnid, '<img border=0 src="themes/default/images/icons/system/delete.gif"/>',$newparams);
			echo $closediv;
		}
	}
}

echo ($mode == 'image'?'</div>':'</ul>');
echo '</div>';

if (isset($dh))	closedir($dh);

// UPLOAD FORM
echo $this->CreateFormStart($id, 'uploadFile', $returnid, 'post', 'multipart/form-data');
	echo '<div><h3>'.lang('uploadfile').':</h3>';
	if($params['mode'] == 'images') echo '<p>('.$this->Lang('browsefilesresize').')</p>';
	echo '<p>';
		echo $this->CreateFileUploadInput($id,'uploadfile');
		echo $this->CreateInputSubmit($id,'submit',lang('send'));
		echo $this->CreateInputHidden($id,'curdir',$curdir);
		echo $this->CreateInputHidden($id,'field',$params['field']);
		echo $this->CreateInputHidden($id,'tablename',$params['tablename']);
		if(isset($params['extensions'])) echo $this->CreateInputHidden($id,'extensions',$params['extensions']);
		if(isset($params['startdir'])) echo $this->CreateInputHidden($id,'startdir',$params['startdir']);
		if(isset($params['size'])) echo $this->CreateInputHidden($id,'size',$params['size']);
		if(isset($params['thumb'])) echo $this->CreateInputHidden($id,'thumb',$params['thumb']);
		if(isset($params['addfiles'])) echo $this->CreateInputHidden($id,'addfiles',$params['addfiles']);
		if(isset($params['active_tab'])) echo $this->CreateInputHidden($id,'active_tab',$params['active_tab']);
		echo $this->CreateInputHidden($id,'mode',$params['mode']);
		echo $this->CreateInputHidden($id,'prefix',$params['prefix']);
		echo $this->CreateInputHidden($id,$params['prefix'].'id',$params[$params['prefix'].'id']);
	echo '</p></div>';		
echo $this->CreateFormEnd();
?>
