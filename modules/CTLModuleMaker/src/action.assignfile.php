<?php
if(!isset($gCms)) exit;

if(!isset($params['field']) || !isset($params['prefix']) || !isset($params[$params['prefix'].'id']) || !isset($params['tablename']))	$this->Redirect($id, 'defaultadmin', $returnid, array());

$filepath = isset($params['filepath'])?str_replace('//','/',$params['filepath']):'';
$tablename = cms_db_prefix().'module_'.$params['tablename'];
$field = $params['field'];
$itemid = $params[$params['prefix'].'id'];

if($filepath != '' && isset($params['thumb'])) {
	$thumbsize = explode('x',$params['thumb']);
	$thumbpath = '';
	if(count($thumbsize) == 2){
		$fullpath = $gCms->config["uploads_path"];
		if(isset($params['startdir'])  && $params['startdir'] != '/'){
			$fullpath .= (substr($params['startdir'],0,1) == '/'?'':'/').$params['startdir'];
		}
		$this->plResize($fullpath.$filepath, 'plthumb_', $thumbsize[0], $thumbsize[1]);
	}
}
$filepath = str_replace('//','/',$params['filepath']);

$db = $this->GetDb();

if( isset($params['addfiles']) && $params['addfiles'] ) {
	$newid = $db->GenID($tablename.'_seq');
	$query = "INSERT INTO $tablename SET fileid=?, itemid=?, fieldname=?, filepath=?";
	$dbresult = $db->Execute( $query, array($newid, $itemid, $field, $filepath) );
}else{
	$query = "UPDATE $tablename SET $field=? WHERE id=? LIMIT 1";
	$db = $this->GetDb();
	$dbresult = $db->Execute($query,array($filepath,$itemid));
}
if(mysql_error()){
	echo '<div class="pageerrorcontainer">'.mysql_error().'</div>';
}else{
	$newparams = array( $params['prefix'].'id' => $itemid );
	if(isset($params['active_tab'])) $newparams['active_tab'] = $params['active_tab'];
	$this->Redirect($id, 'edit'.$params['prefix'], $returnid, $newparams );		
}

?>