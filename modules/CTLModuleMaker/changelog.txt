VERSION 1.7:
	- Added full upgrade functionality for created modules
	- Corrected the bug in list fields with multiple choices
	- Added a search action

VERSION 1.6.4:
    	- Events handling added
    	- Install link corrected
    	- Upgrade functionality added to created modules (for compatibility with future releases of CTLModuleMaker)
    	- Removed the code that was php5 only
    	- Added classes to the page menu (when separating into pages)

VERSION 1.6:
	- List and Default actions were merged together
	- Mysql-specific code has been changed to ADODB for compatibility reasons
	- Bugs corrected (#2949 and #2945)

VERSION 1.5:
	- Added: support for multiple templates. Templates are now managed by the cms, and you may specify which template to use
