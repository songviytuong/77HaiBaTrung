<?php
$templatefile = '
<span style="float: right; font-size: 80%">{$itemalias}</span><h1>{$edittitle}</h1>
<p>{$submit} {$apply} {$cancel}</p>
<br/><br/>
	<div class="pageoverflow">
		<p class="pagetext">{$name_label}* :</p>
		<p class="pageinput">{$name_input}</p>
	</div>
';

if($parentinput){
	$templatefile .= '
	<div class="pageoverflow">
		<p class="pagetext">{$parent_label}* :</p>
		<p class="pageinput">{$parent_input}</p>
	</div>';
}


foreach($level[4] as $field){
	if(!in_array($field[0],array('name','id','item_order','isdefault','active','parent','alias')) && $field[1] < 10){
		$templatefile .= '
	<div class="pageoverflow">
		<p class="pagetext">{$'.$field[0].'_label}'.($field[2] == 1?'*':'').' :</p>
		<p class="pageinput">{$'.$field[0].'_input}</p>
	</div>';
	}elseif($field[1] == 10){
		$templatefile .= '
	<div class="pageoverflow">
		<p class="pagetext">{$'.$field[0].'_label}'.($field[2] == 1?'*':'').' :</p>
		<p class="pageinput">{html_select_date prefix=$'.$level[1].'_'.$field[0].'_prefix time=$'.$level[1].'_'.$field[0].' start_year="-10" end_year="+10"} {html_select_time prefix=$'.$level[1].'_'.$field[0].'_prefix time=$'.$level[1].'_'.$field[0].' }</p>
	</div>';
	}
}

$templatefile .= '
<br/>
<p>{$submit} {$apply} {$cancel}</p>
';

?>