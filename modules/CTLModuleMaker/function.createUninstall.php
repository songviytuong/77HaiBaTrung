<?php
if(!isset($levels) || !isset($linktables)) exit;

$uninstallmethod = '<?php
if (!isset($gCms)) exit;
$db = $this->GetDb();
$dict = NewDataDictionary($db);	
';

foreach($levels as $level){
	$levelname = $level[0];
	$uninstallmethod .= '

	$sqlarray = $dict->DropTableSQL(cms_db_prefix()."module_'.$modulename.'_'.$levelname.'");
	$dict->ExecuteSQLArray($sqlarray);
	$db->DropSequence(cms_db_prefix()."module_'.$modulename.'_'.$levelname.'_seq");
	';
}

foreach($linktables as $linktable){
	$levelname = $linktable[0].'_has_'.$linktable[1];
	$uninstallmethod .= '

	$sqlarray = $dict->DropTableSQL(cms_db_prefix()."module_'.$modulename.'_'.$levelname.'");
	$dict->ExecuteSQLArray($sqlarray);
	';
}

foreach($listfields as $oneitem){
	$uninstallmethod .= '
	$sqlarray = $dict->DropTableSQL(cms_db_prefix()."module_'.$modulename.'_'.$oneitem[0].'_'.$oneitem[1].'_options");
	$dict->ExecuteSQLArray($sqlarray);
	$db->DropSequence(cms_db_prefix()."module_'.$modulename.'_'.$oneitem[0].'_'.$oneitem[1].'_options_seq");
	';
}

if(count($multiplefiles) > 0) {
	$uninstallmethod .= '
	$sqlarray = $dict->DropTableSQL(cms_db_prefix()."module_'.$modulename.'_multiplefilesfields");
	$dict->ExecuteSQLArray($sqlarray);
	$db->DropSequence(cms_db_prefix()."module_'.$modulename.'_multiplefilesfields_seq");
	';
}


$uninstallmethod .= '

	//$sqlarray = $dict->DropTableSQL(cms_db_prefix()."module_'.$modulename.'_templates");
	//$dict->ExecuteSQLArray($sqlarray);
	$this->DeleteTemplate("",$this->GetName());
';

// DELETING PERMISSIONS AND EVENTS :

$uninstallmethod .= '
// permissions';

foreach($levels as $level){
	$uninstallmethod .= '
	$this->RemovePermission("'.$modulename.'_manage_'.$level[0].'");';
}
$uninstallmethod .= '

// events
	$this->RemoveEvent("'.$modulename.'_added");
	$this->RemoveEvent("'.$modulename.'_modified");
	$this->RemoveEvent("'.$modulename.'_deleted");

// put mention into the admin log
	$this->Audit( 0, $this->Lang("friendlyname"), $this->Lang("uninstalled"));

?>
';

?>
