<?php
$finallevel = $levels[count($levels) - 1][0];
$modulename = $infos['nameofmodule'];
$levelnames = '';
foreach($levels as $level){
	$levelnames .= ($levelnames == ''?'':', ').$level[0];
}

// loading the language :
$mylang = array();
require 'createdlang/en_US.php';
if($inlang && $inlang != 'en_US'){
	require 'createdlang/'.$inlang.'.php';	
}

$defaultfields = $this->GetDefaultFields();
$description = str_replace('"','&quot;',$infos['description']);
$langfile = '<?php
	$lang["friendlyname"] = "'.$infos['friendlyname'].'";
	$lang["moddescription"] = "'.$description.'";
	$lang["admindescription"] = "'.$description.'";

	$lang["pagemenudelimiter"] = "&nbsp;&#124;&nbsp;";
	$lang["pagemenuoverflow"] = "&nbsp;...&nbsp;";
	';

$currentlevel = 0;
$tplhelps = $this->createTemplateHelp($levels);
foreach($levels as $level){
	$currentlevel++;
	$langfile .= '
// strings for '.$level[0].'
	$lang["'.$level[0].'"] = "'.ucfirst($level[0]).'";
	$lang["'.$level[0].'_plural"] = "'.ucfirst(substr($level[0],-8,8)=='category'?substr($level[0],0,-1).'ies':$level[0].'s').'";
	$lang["add_'.$level[0].'"] = "'.$mylang['Add'].' '.$level[0].'";
	$lang["edit_'.$level[0].'"] = "'.$mylang['Edit'].' '.$level[0].'";
	$lang["filterby_'.$level[0].'"] = "'.$mylang['Filterby'].' '.$level[0].'";
	';

	foreach($level[4] as $field){
		if(!in_array($field[0],$defaultfields))		$langfile .= '$lang["'.$level[0].'_'.$field[0].'"] = "'.ucfirst($field[0]).'";
	';
		if($field[1] == 6){
			$optnb = 0;
			$listoptions = explode(',',$field[5]['listoptions']);
			foreach($listoptions as $option){
				$langfile .= '$lang["'.$level[0].'_'.$field[0].'_option_'.$optnb.'"] = "'.trim($option).'";
	';
				$optnb++;
			}
		}elseif($field[1] == 8 || $field[1] == 9){
			$langfile .= '$lang["'.$level[0].'_select_'.$field[0].'"] = "'.$mylang['Select'].' '.$field[0].'";
	';
			if(isset($field[5]['thumb']) && $field[5]['thumb'] != '') $templatehelp .= '<li>$'.$level[0].'->'.$field[0].'_thumbnail</li>
		';
		}
	}
	$langfile .= '$lang["promt_delete'.$level[0].'"] = "'.$mylang['deleteprompt1'].' '.$level[0].' (%s)? '.(($currentlevel == count($levels) || $level[2] == 1)?'':$mylang['deleteprompt2'].' ').$mylang['deleteprompt3'].'";
	';

}

$templatehelp = "";
foreach($tplhelps as $key=>$value){
  $templatehelp .= ($templatehelp == ''?'':'<br/><hr/><br/>').'<h3>'.($key == 'final_level_template'?$mylang['templatevars_final']:$mylang['templatevars_list'].' '.$key).'</h3>'.$value;
  if($key == 'final_level_template')	$templatehelp .= '<br/><p>'.$mylang['templatelabels'].'</p><p>'.$mylang['parenttree'].'</p>';
}
$langfile .= '$lang["templatehelp"] = \''.$templatehelp.'\';
';

if($infos['hasfilefields']){
	$langfile .= $mylang['filefields'];
}

$langfile .= $mylang['general'];
$langfile .= '
?'.'>';
?>
