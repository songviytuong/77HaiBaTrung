<?php
if(!isset($gCms)) exit;

echo '<h1>'.$this->Lang('title').'</h1><br/>';

$params['levels'] = isset($params['levels'])?unserialize($params['levels']):array();
$params['infos'] = isset($params['infos'])?unserialize($params['infos']):array();

echo $this->StartTabHeaders();
	echo $this->SetTabHeader("creation", $this->Lang("Modulecreation"), true);
	echo $this->SetTabHeader("help", $this->Lang("helptab"), false);		
echo $this->EndTabHeaders();


echo $this->StartTabContent();
	echo $this->StartTab("creation");

	// MODULE CREATION...
	$currentlevel = isset($params['currentlevel'])?$params['currentlevel']:1;
	if(isset($params['submitprevious']) && $params['step'] > 1){    // GOING BACK TO PREVIOUS STEP/SUBSTEP
		if($params['step'] == 3){
			$params['step'] = 2;
		}elseif($currentlevel == 1 && $params['substep'] == 1){
			$params['step'] = 1;
		}elseif($params['substep'] > 1) {
			$params['substep'] = $params['substep'] - 1;
		}else{
			$currentlevel = $currentlevel - 1;
			$params['substep'] = 3;
		}
	}elseif(isset($params['submitnext'])) {
#################################################################################
######## BEGIN FORM SUBMISSION (PROCESSING INPUTS)                              #
#################################################################################
		$errors = array();
		if($params['step'] == 1){
			// NAME OF MODULE
			if(!isset($params['nameofmodule']) || $params['nameofmodule'] == ''){
				$errors[] = $this->Lang('error_nameofmodule');
			}else{
				$newname = str_replace('-','',munge_string_to_url($params['nameofmodule'], false));
				if($params['nameofmodule'] != $newname || $this->isreservedword($params['nameofmodule']) || strtolower($params['nameofmodule']) == 'products'){
					$errors[] = $this->Lang('error_nameinvalid');
				}
				$params['infos']['name'] = strtolower($newname);
			}
			// FRIENDLY NAME
			if(!isset($params['friendlyname']) || $params['friendlyname'] == ''){
				$errors[] = $this->Lang('error_friendlyname');
			}else{
				$params['infos']['friendlyname'] = $params['friendlyname'];
			}
			if(isset($params['moduledescription'])) $params['infos']['description'] = $params['moduledescription'];
			
			if($this->ValidateVersion($params['version'])){
				$params['infos']['version'] = $params['version'];
			}else{
				$params['infos']['version'] = '1.0';
			}
			
			// HOW MANY LEVELS
			$minlevels = ($this->GetPreference('allowsinglelevel', false))?1:2;
			if(!isset($params['howmanylevels']) || !($params['howmanylevels'] >= $minlevels) ){
				$errors[] = $this->Lang('error_howmanylevels');
			}else{
				$params['infos']['nblevels'] = $params['howmanylevels'];
				$params['levels'] = array();
				$i = 0;
				while($i < $params['infos']['nblevels']){
					$params['levels'][] = array();
					$i++;
				}
			}
		}else{
			// WE ARE DEFINING LEVELS
			if($params['substep'] == 3) {
				// WE ARE CHOOSING ADMIN
				$params['levels'][$currentlevel - 1][5] = $params['adminfields'];
				$errors = array_merge($errors,$this->checkAdminFields($params['adminfields'],$params['levels'][$currentlevel - 1][4]));
			}elseif($params['substep'] == 2){
				// WE ARE DEFINING FIELDS
				$tmpfield = 1;
				$tmperrors = 0;
				if(!isset($params['levels'][$currentlevel - 1][4])) $params['levels'][$currentlevel - 1][4] = array();
				while($tmpfield <= $params['levels'][$currentlevel - 1][3]) {
					$thislevel = isset($params['levels'][$currentlevel - 1][4][$tmpfield - 1])?$params['levels'][$currentlevel - 1][4][$tmpfield - 1]:array();
					$thislevel[0] = strtolower($params['field'.$tmpfield.'_name']);
					$thislevel[1] = $params['field'.$tmpfield.'_type'];
					$thislevel[2] = $params['field'.$tmpfield.'_needed'];
					$thislevel[3] = $params['field'.$tmpfield.'_indexed'];
					$thislevel[4] = $params['field'.$tmpfield.'_default'];
					$thislevel[5] = array(	"listoptions"=>$params['field'.$tmpfield.'_listoptions'],
											"listmode"=>$params['field'.$tmpfield.'_listmode'],
											"upfolder"=>$params['field'.$tmpfield.'_upfolder'],
											"thumb"=>$params['field'.$tmpfield.'_thumb'],
											"size"=>$params['field'.$tmpfield.'_size']  );
					$thislevel[6] = $this->ValidateField($thislevel);
					$params['levels'][$currentlevel - 1][4][$tmpfield - 1] = $thislevel;
					if(count($thislevel[6])>0) $tmperrors++;
					$tmpfield++;
				}
				if($tmperrors != 0) $errors[] = $this->Lang('error_general');
			}elseif($params['substep'] == 1){
				// WE ARE DEFINING THE LEVEL INFORMATIONS
				if(!isset($params['nameoflevel']) || $params['nameoflevel'] == ''){
					$errors[] = $this->Lang('error_missingvalue');
				}else{
					$j = 0;
					$newname = str_replace('-','',munge_string_to_url($params['nameoflevel'], false));
					if($params['nameoflevel'] != $newname)	$errors[] = $this->Lang('error_nameoflevel');
						while($j < ($currentlevel -1)) {
							if($newname == $params['levels'][$j][0]) $errors[] = $this->Lang('error_namealreadyused');
							$j++;
						}
					if($this->isreservedword($newname)) $errors[] = $this->Lang('error_reserved');
					$params['levels'][$currentlevel - 1][0] = strtolower($newname);
				}
				if(!isset($params['paramprefix']) || $params['paramprefix'] == ''){
					$errors[] = $this->Lang('error_missingvalue');
				}else{
					$newprefix = str_replace('-','',munge_string_to_url($params['paramprefix'], false));
					if($params['paramprefix'] != $newprefix)	$errors[] = $this->Lang('error_paramprefix');
					$params['levels'][$currentlevel - 1][1] = $newprefix;
				}
				$params['levels'][$currentlevel - 1][2] = isset($params['sharechildren'])?$params['sharechildren']:0;
				if($params['levels'][$currentlevel - 1][2] == 1) $params['infos']['shared'] = true;
				if(!isset($params['howmanyfields']) || !($params['howmanyfields'] > 0) || !($params['howmanyfields'] < 31)){
					$errors[] = $this->Lang('error_howmanyfields');
				}else{
					$params['levels'][$currentlevel - 1][3] = $params['howmanyfields'];
				}
				$params['levels'][$currentlevel - 1][6] = isset($params['itemorder'])?$params['itemorder']:0;
				$params['levels'][$currentlevel - 1][7] = isset($params['nbdefaults'])?$params['nbdefaults']:0;
				$params['levels'][$currentlevel - 1][8] = isset($params['newontop'])?$params['newontop']:0;
			}
		}

#################################################################################
######## END FORM SUBMISSION                                                    #
#################################################################################
		
		if(count($errors) == 0){
		// GOING TO NEXT STEP
			if($params['step'] == 1){
				$params['step']++;
			}elseif($params['substep'] == 3){
				if($currentlevel == count($params['levels'])) {
					$params['step']++;
				}else{
					$currentlevel++;
					$params['substep'] = 1;
				}
			}else{
				$params['substep']++;
			}
		}else{
			$this->display_errors($errors, $head='errorwithrequest');
		}
	}
	if($params['step'] < 3) {
		echo $this->CreateFormStart($id, 'creation', $returnid, 'post');
		$YesNoOptions = array($this->Lang('No')=>0,$this->Lang('Yes')=>1);
		// HEADER
		echo '<h2>'.$this->Lang('Step').' '.$params['step'].' : '.$this->Lang('Step'.$params['step'].'title');
		if ($params['step'] == 2){
			// IF WE ARE WORKING WITH STRUCTURE
			echo '('.$this->Lang('Level').' '.$currentlevel.'/'.count($params['levels']).')</h2><br/>';
			echo '<h3>'.$this->Lang('Currentstructure').': ';
			$tmpcounter = 0;
			// onelevel = array(name,prefix,sharechildren,#fields,fields)
			foreach($params['levels'] as $onelevel){
				$tmpcounter++;
				if($tmpcounter > 1) echo ' -&gt; ';
				$levelname = (isset($onelevel[0])?$onelevel[0].'('.$tmpcounter.')':$this->Lang('Level').' '.$tmpcounter);
				echo ($tmpcounter == $currentlevel?'<span style="color: green;">':'').$levelname.($tmpcounter == $currentlevel?'</span>':'');
			}
			echo '</h3>';
		// END OF HEADER
			// STILL WORKING WITH STRUCTURE...
			$level = $params['levels'][$currentlevel-1];
			if($params['substep'] == 3) {
				// WE ARE CHOOSING ADMIN
				echo '<p>'.$this->Lang('prompt_adminpanel').'</p>';
				echo '<p>'.$this->CreateInputText($id, 'adminfields', isset($level[5])?$level[5]:'name,active,'.(($currentlevel == $params['infos']['nblevels'])?'isdefault,':'').'movelinks', 70).'</p>';
				echo '<p>'.$this->Lang('possiblefields').'<ul>';
				/* $defaultfields = $this->GetDefaultFields();
				foreach($defaultfields as $onefield){
					echo '<li>'.$onefield.'</li>';
				}*/
				echo $this->Lang('adminfields');
				foreach($level[4] as $onefield){
					echo '<li>'.$onefield[0].'</li>';
				}
				echo '</ul></p>';
			}elseif($params['substep'] == 2) {
				// WE ARE CREATING FIELDS
				echo '<script type="text/javascript">
						function displayoptions(id,type){
							document.getElementById("field"+id+"optgroup1").style.display = "none";
							document.getElementById("field"+id+"optgroup2").style.display = "none";
							document.getElementById("field"+id+"optgroup3").style.display = "none";
							document.getElementById("field"+id+"optgroup4").style.display = "none";
							document.getElementById("field"+id+"optgroup5").style.display = "none";
							if(type == 7){
								document.getElementById("field"+id+"optgroup5").style.display = "block";
							}else if(type == 6){
								document.getElementById("field"+id+"optgroup1").style.display = "block";
								document.getElementById("field"+id+"optgroup5").style.display = "block";
							}else if(type == 8){
								document.getElementById("field"+id+"optgroup2").style.display = "block";
								document.getElementById("field"+id+"optgroup3").style.display = "block";
								document.getElementById("field"+id+"optgroup4").style.display = "block";
							}else if(type == 9 || type == 11){
								document.getElementById("field"+id+"optgroup2").style.display = "block";
							}
						}
					</script>';
				$tmpfield = 1;
				while($tmpfield <= $level[3]){
					$this->CreateFieldInput($id,$tmpfield,isset($level[4])?$level[4][$tmpfield-1]:array());
					$tmpfield++;
				}
				if($level[3] == 0) echo '<p>'.$this->Lang('nofield').'</p>';
			}else{
				// BASIC LEVEL INFORMATION
				echo $this->DoInputLine('nameoflevel',$this->CreateInputText($id, 'nameoflevel', isset($level[0])?$level[0]:'', 30));
				echo $this->DoInputLine('paramprefix',$this->CreateInputText($id, 'paramprefix', isset($level[1])?$level[1]:'', 30,6));
				if($currentlevel != $params['infos']['nblevels']) echo $this->DoInputLine('sharechildren',$this->CreateInputDropdown($id, 'sharechildren', $YesNoOptions, '', isset($level[2])?$level[2]:0,' disabled=true').'<br/><span style="color: red;">('.$this->Lang("unsupported").')</span>');
				echo $this->DoInputLine('howmanyfields',$this->CreateInputText($id, 'howmanyfields', isset($level[3])?$level[3]:'', 30, 2));
				echo $this->DoInputLine('itemorder',$this->CreateInputDropdown($id, 'itemorder', $YesNoOptions, '', isset($level[6])?$level[6]:1));
				echo $this->DoInputLine('newontop',$this->CreateInputDropdown($id, 'newontop', $YesNoOptions, '', isset($level[8])?$level[8]:0));
				echo $this->DoInputLine('nbdefaults',$this->CreateInputDropdown($id, 'nbdefaults', array($this->Lang('onlyonedefault')=>0,$this->Lang('defaultbyparent')=>1), '', isset($level[7])?$level[7]:0));
			}
		}else{
			// WE ARE IN THE FIRST STEP... COLLECTING BASIC MODULE INFORMATION:
			echo '</h2>';
			echo $this->DoInputLine('nameofmodule',$this->CreateInputText($id, 'nameofmodule', isset($params['infos']['name'])?$params['infos']['name']:'', 30));
			echo $this->DoInputLine('moduledescription',$this->CreateInputText($id, 'moduledescription', isset($params['infos']['description'])?$params['infos']['description']:'', 60));
			echo $this->DoInputLine('friendlyname',$this->CreateInputText($id, 'friendlyname', isset($params['infos']['friendlyname'])?$params['infos']['friendlyname']:'', 30));
			echo $this->DoInputLine('version',$this->CreateInputText($id, 'version', isset($params['infos']['version'])?$params['infos']['version']:'1.0', 10));
			echo $this->DoInputLine('howmanylevels',$this->CreateInputText($id, 'howmanylevels', isset($params['infos']['nblevels'])?$params['infos']['nblevels']:2, 30, 1));
			echo '<p>'.$this->Lang('help_think').'</p><br/>';
		}
		echo $this->DoHiddenInputs($id, $params, isset($currentlevel)?$currentlevel:false);
		echo '<br/><p>'.$this->CreateInputSubmit($id, 'submitnext', $this->Lang('Next')).'</p>';
		if($params['step'] > 1){
			echo '<p>'.$this->CreateInputSubmit($id, 'submitprevious', $this->Lang('Previous'), '', '', isset($params['submitprevious'])?'':$this->Lang('warning_previous')).'</p>';
			echo $this->CreateFormEnd();
			$this->EchoExportForm($params, isset($currentlevel)?$currentlevel:false);
		}else{
			echo $this->CreateFormEnd();
		}

	}else{
		//FINISHING STEP (STEP 3)
		if(isset($params['infos']['shared']) && $params['infos']['shared'] == true){
			$params['skippedtemplate'] = true;
			$params['templatelevel'] = 0;
			$this->Redirect($id, 'createModule', $returnid, $params);
		}else{
			$leveloptions = array($this->Lang('onlyonetemplate') => 0);
			$tmpflag = 0;
			foreach($params['levels'] as $onelevel){
				$tmpflag++;
				if($tmpflag < $params['infos']['nblevels'])		$leveloptions[$onelevel[0]] = $tmpflag;
			}
			echo '<h2>'.$this->Lang('Step').' 3 : '.$this->Lang('Step3title').'</h2><br/>';
			echo $this->CreateFormStart($id, ($this->GetPreference('doupgrade', true)?'modop':'createModule'), $returnid, 'post');
			echo '<p>'.$this->Lang('prompt_templatelevel').' '.$this->CreateInputDropdown($id, 'templatelevel', $leveloptions).'</p>';
			echo $this->DoHiddenInputs($id, $params, isset($currentlevel)?$currentlevel:false);
			echo '<br/><p>'.$this->CreateInputSubmit($id, 'submitnext', $this->Lang('Next'), '', '', $this->Lang('warning_laststep')).'</p>';
			echo $this->CreateFormEnd();
			echo $this->CreateFormStart($id, 'creation', $returnid, 'post');
			echo $this->DoHiddenInputs($id, $params, isset($currentlevel)?$currentlevel:false);
			echo '<p>'.$this->CreateInputSubmit($id, 'submitprevious', $this->Lang('Previous')).'</p>';
			echo $this->CreateFormEnd();
			$this->EchoExportForm($params, $params['currentlevel']);
		}
	}

	$this->getInnerDebug($params);

echo $this->EndTab();

echo $this->StartTab("help");
		echo "<h2>".$this->Lang("helptab")."</h2><br/>";

		$shownhelp = "help_none";
		if(!isset($params["step"])){
		}elseif( $params["step"] == 1 ){
			$shownhelp = "help_structure";
		}elseif( $params["step"] == 2 ){
			if(!isset($params["substep"]) || $params["substep"] == 1){
				$shownhelp = "help_structure";
			}elseif($params["substep"] == 3) {
				$shownhelp = "help_adminlinks";
			}
		}
		echo $this->Lang($shownhelp);

echo $this->EndTab();

echo $this->EndTabContent();

?>
