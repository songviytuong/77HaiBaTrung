<?php

	if(!isset($gCms)) exit;
	if(!isset($params['infos']) || !isset($params['levels'])) exit;

	$infos = $params['infos'];
	$levels = $params['levels'];
	if(!is_array($infos)) $infos = unserialize($infos);
	if(!is_array($levels)) $levels = unserialize($levels);

	$errors = 0;	
	if(isset($infos['tempfile'])){
		$content = $this->getFileContent($infos['tempfile'], false);
		if (count($content) == 0)	$errors++;
		$cntarray = unserialize($content);
		if(!is_array($cntarray) || !isset($cntarray['infos']) || !isset($cntarray['levels'])) $errors++;	
	}else{
		$errors++;
	}
	
	if($errors > 0 || !$this->compareArchitecture($cntarray['levels'], $levels)){
		$this->Redirect($id, 'createModule', $returnid, $params);
	}else{
		if(!isset($infos['nameofmodule'])) $infos['nameofmodule'] = $infos['name'];
		$i = 0;
		$modifs = array('created'=>0, 'modified'=>0, 'deleted'=>0);
		$newqueries = array();
		while($i < count($levels)){
			$newlevel = $levels[$i];
			$oldlevel = $cntarray['levels'][$i];
			$result = $this->compareLevels($oldlevel, $newlevel);
			$modifs['created'] += $result[0][0];
			$modifs['modified'] += $result[0][1];
			$modifs['deleted'] += $result[0][2];
			$newqueries = array_merge($newqueries, $result[1]);
			$i++;
		}
		if(count($newqueries) > 0){
			// THERE ARE SOME CHANGES, WE COULD DO AN UPGRADE
			if(!isset($infos['upgradequeries'])) $infos['upgradequeries'] = array();
			if(!isset($infos['version']))	$infos['version'] = '1.0';
			$infos['modifications'] = $modifs;
			$infos['upgradequeries'][$infos['version']] = $newqueries;
			$vparts = explode('.',$infos['version']);
			$infos['version'] = $vparts[0].'.'.($vparts[1]+1);
					
			echo '<h1>'.$this->Lang('title').'</h1><br/>';
			echo '<h2>'.$this->Lang('moduleupgrade').'</h2><br/>'.$this->Lang('help_moduleupgrade');
			echo '<ul><li>'.$this->Lang('deletedfields').': '.$modifs['deleted'].'</li>';
			echo '<li>'.$this->Lang('createdfields').': '.$modifs['created'].'</li>';
			echo '<li>'.$this->Lang('modifiedfields').': '.$modifs['modified'].'</li></ul>';
			echo '<p>'.$this->Lang('prompt_moduleupgrade').'</p>';
			echo '<p>'.$this->CreateFormStart($id, 'createModule', $returnid, 'POST').$this->CreateInputSubmit($id, 'withupgrade', $this->Lang('Yes'));
			echo $this->CreateInputHidden($id, 'levels', $params['levels']).$this->CreateInputHidden($id, 'infos', serialize($infos));
			echo $this->CreateFormEnd().'</p>';
			echo '<p>'.$this->CreateFormStart($id, 'createModule', $returnid, 'POST').$this->CreateInputSubmit($id, 'withoutupgrade', $this->Lang('No'));
			echo $this->CreateInputHidden($id, 'levels', $params['levels']).$this->CreateInputHidden($id, 'infos', $params['infos']);
			echo $this->CreateFormEnd().'</p>';

		}else{
			$this->Redirect($id, 'createModule', $returnid, $params);
		}
	}

$this->getInnerDebug($params);

?>
