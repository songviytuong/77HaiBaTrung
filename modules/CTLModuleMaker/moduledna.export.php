<?php
if(!isset($_POST['infos']) || !isset($_POST['levels'])) exit;

$output = array('infos' => unserialize(urldecode($_POST['infos'])), 'levels' => unserialize(urldecode($_POST['levels'])));

if(isset($_POST['currentlevel'])) $output['currentlevel'] = $_POST['currentlevel'];
if(isset($_POST['step'])) $output['step'] = $_POST['step'];
if(isset($_POST['substep'])) $output['substep'] = $_POST['substep'];

$filename = 'module_'.(isset($output['infos']['nameofmodule'])?$output['infos']['nameofmodule'].'_':'').'export.dna';

header('Content-Type: text/force-download');
header("Content-Disposition: attachment; filename=$filename");

echo serialize($output);

?>