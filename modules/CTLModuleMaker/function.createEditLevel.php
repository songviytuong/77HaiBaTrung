<?php
if(!isset($level) || !isset($infos)) exit;

$prefix = $level[1];

$phpfile = '<?php
if (!isset($gCms)) exit;
$admintheme = $gCms->variables["admintheme"];

if (isset($params["cancel"]))	$this->Redirect($id, "defaultadmin", $returnid, array("active_tab" => "'.$level[0].'"));

$db =& $this->GetDb();
';

if($parentname != false)	$phpfile .= '$parentoptions = $this->get_options("'.$parentname.'");
';
if($parentname != false && $sharedbyparents)	$phpfile .= '$selectedparents = (isset($params["'.$prefix.'id"])?$this->get_parents("'.$parentname.'","'.$level[0].'",$params["'.$prefix.'id"]):array());
';

###################################################
## PART I : FORM SUBMISSION...  You're really sure you want to get into this? Sorry for the lack of comprehension...

// parsing fields :
$filefields = array();
$neededfields = '';
$indexedfields = '';
$retrievefields = '';
$sqlfields = '';
$sqlvalues = '';
$multiplefiles = array();
foreach($level[4] as $field){
	switch($field[1]){
		case 6:
			$phpfile .= '$'.$field[0].'options = $this->get_predefinedoptions("'.$level[0].'_'.$field[0].'");
';
			break;
		case 7:
			$phpfile .= '$'.$field[0].'options = $this->get_options("'.$level[1].'_'.$field[0].'_options");
';
			break;
		case 8:
		case 9:
			$filefields[] = array($level[1],$field[0],($field[1] == 8?'image':'file'),$field[5]['upfolder'],$field[5]['size'],$field[5]['thumb']);
			break;
		case 11:
			$multiplefiles[] = $field;
			break;
	}
	if($field[1] != 8 && $field[1] != 9 && $field[1] != 11 && !in_array($field[0],array('id','item_order','isdefault','active','alias')) && ($field[0] != 'parent' || !$sharedbyparents)) {
		if($field[1] == 10){
			$retrievefields .= '$item->'.$field[0].' = time();
			if (isset($params["'.$prefix.'_'.$field[0].'_Month"])) {
				$item->'.$field[0].' = mktime($params["'.$prefix.'_'.$field[0].'_Hour"], $params["'.$prefix.'_'.$field[0].'_Minute"], $params["'.$prefix.'_'.$field[0].'_Second"], $params["'.$prefix.'_'.$field[0].'_Month"], $params["'.$prefix.'_'.$field[0].'_Day"], $params["'.$prefix.'_'.$field[0].'_Year"]);
			}
			$item->'.$field[0].' = str_replace("\'","",$db->DBTimeStamp($item->'.$field[0].'));
		';
		}elseif($field[4] != ''){
			$retrievefields .= '$item->'.$field[0].' = (isset($params["'.$prefix.$field[0].'"]) && $params["'.$prefix.$field[0].'"] != "")?$params["'.$prefix.$field[0].'"]:"'.$field[4].'";
		';
		}else{
			$retrievefields .= '$item->'.$field[0].' = $params["'.$prefix.$field[0].'"];
		';
		}
		$sqlfields .= ($sqlfields == ''?'':',
			').$field[0].'=?';
		$sqlvalues .= ($sqlvalues == ''?'':',
			');
		if($field[1] < 5 && $field[1] != 0){
			$sqlvalues .= 'addslashes($item->'.$field[0].')';
		}elseif( ($field[1] == 6 || $field[1] == 7) && ($field[5]['listmode'] == 2 || $field[5]['listmode'] == 4) ) {
			$sqlvalues .= 'serialize($item->'.$field[0].')';
		}else{
			$sqlvalues .= '$item->'.$field[0];
		}
		if($field[2] == 1 && $field[1] != 10) $neededfields .= ($neededfields == ''?'':' || ').'!isset($params["'.$prefix.$field[0].'"]) || $params["'.$prefix.$field[0].'"] == ""
		';
		if($field[3] == 1) $indexedfields .= ($indexedfields == ''?'':' ').'$item->'.$field[0];
	}
}
$sqlfields .= ',
		alias=?,
		date_modified=?,
		active=".(isset($item->active)?$item->active:1).",
		isdefault=".(isset($item->isdefault)?$item->isdefault:0)."';
$sqlvalues .= ',$item->alias,str_replace("\'","",$db->DBTimeStamp(time()))';

$redirect = '';

if(count($multiplefiles) > 0){
$phpfile .= '
if( isset($params["deletefile"]) && $params["deletefile"] > 0 ){
	// this is for the multiple files fields (additionnal tabs)
	$query = "DELETE FROM ".cms_db_prefix()."module_'.$infos['nameofmodule'].'_multiplefilesfields WHERE fileid=? LIMIT 1";
	$db->Execute( $query, array($params["deletefile"]) );
}
';
}

$phpfile .= '

if(isset($params["'.$prefix.'id"])) {
	// if we are working on an item that exists, we load it. We must do this even when the form is submitted, otherwise we won\'t have the file fields
	$items = $this->get_level_'.$level[0].'(array("id"=>$params["'.$prefix.'id"]));
	$item = $items[0];
}

// CHECK IF THE FORM IS BEING SUBMITTED :
// (we must detect all kinds of submit buttons, including files, since information must be saved before we go to file submission)
if (isset($params["submit"]) || 
	isset($params["apply"]) ';
foreach ( $filefields as $field ) {
	// WE PREPARE REDIRECTION AT THE SAME TIME:
	if($redirect != '') $redirect .= 'else';
	$redirect .= 'if(isset($params["add_'.$field[1].'"])){
				$params = array("'.$prefix.'id"=>$item->id,"field"=>"'.$field[1].'","tablename"=>"'.$infos['nameofmodule'].'_'.$level[0].'","prefix"=>"'.$prefix.'", "startdir"=>"'.$field[3].'", ';
	$redirect .= ($field[4] != ''?'"size"=>"'.$field[4].'", ':'').($field[5] != ''?'"thumb"=>"'.$field[5].'", ':'').'"mode"=>"'.$field[2].'");
				$this->Redirect($id, "browsefiles", $returnid, $params);
			}elseif(isset($params["remove_'.$field[1].'"])){
				$params = array("'.$prefix.'id"=>$item->id,"field"=>"'.$field[1].'","level"=>"'.$level[0].'","tablename"=>"'.$infos['nameofmodule'].'_'.$level[0].'","prefix"=>"'.$prefix.'");
				$this->Redirect($id, "assignfile", $returnid, $params);
			}';
	// DETECT SUBMIT BUTTONS
	$phpfile .= '||
	isset($params["add_'.$field[1].'"]) ||
	isset($params["remove_'.$field[1].'"]) ';
}

$phpfile .= '
	)
{
	debug_buffer("Edit Form has been submitted".__LINE__);

	// RETRIEVING THE FORM VALUES (and escaping it, if needed)
	if(!isset($item)) $item = new stdClass();
	if(isset($params["'.$prefix.'item_order"])) $item->item_order = $params["'.$prefix.'item_order"];
	'.$retrievefields.'
	$item->alias = $this->plcreatealias($item->name);

	// CHECK IF THE NEEDED VALUES ARE THERE
	if(	'.$neededfields.' )
	{
		echo $this->ShowErrors($this->Lang("error_missginvalue"));
	}elseif(false == $this->checkalias("module_'.$infos['nameofmodule'].'_'.$level[0].'", $item->alias, isset($params["'.$prefix.'id"])?$params["'.$prefix.'id"]:false)){
		echo $this->ShowErrors($this->Lang("error_alreadyexists"));
	}else{
		############ DOING THE UPDATE

		// FIELDS TO UPDATE
		$query = (isset($item->id)?"UPDATE ":"INSERT INTO ").cms_db_prefix()."module_'.$infos['nameofmodule'].'_'.$level[0].' SET 
			'.$sqlfields.'";
			
		// VALUES
		$values = array('.$sqlvalues.');

		if(isset($item->id)){
			$event = "'.$infos['nameofmodule'].'_modified";
			$query .= " WHERE id=?;";
			array_push($values,$item->id);
		}else{
			// NEW ITEM
			$event = "'.$infos['nameofmodule'].'_added";
			// get a new id from the sequence table
			$item->id = $db->GenID(cms_db_prefix()."module_'.$infos['nameofmodule'].'_'.$level[0].'_seq");';
if($level[8] == 1){
	$phpfile .= '
			// new items get to the top - so we must put all other items down from one step, and then set this item\'s order to 1
			$query2 = "UPDATE ".cms_db_prefix()."module_'.$infos['nameofmodule'].'_'.$level[0].' SET item_order=(item_order+1)'.(($level[6] && $parentname != false && $sharedbyparents == 0)?' WHERE parent=?':'').'";
			$db->Execute($query2'.(($level[6] && $parentname != false && $sharedbyparents == 0)?', array($item->parent)':'').');
			$query .= ",item_order=1, id=".$item->id;';
}else{
	$phpfile .= '
			// new items get to the bottom - so we must set the item_order to the number of items + 1
			$item_order = $this->countsomething("'.$level[0].'"'.(($level[6] && $parentname != false && $sharedbyparents == 0)?',"id",array("parent"=>$item->parent)':'').') + 1;
			$query .= ",item_order=".$item_order.", id=".$item->id;';
}
$phpfile .= '
		}
		$db->Execute($query, $values);
';
if($level[6] && $parentname != false && $sharedbyparents == 0){
	if($level[8]){
		$phpfile .= '
		if(isset($params["oldparent"]) && $params["oldparent"] != $item->parent){
			// the item is changing parent, and we\'re ordering by parents
			// UPDATE THE ORDER OF THE ITEMS WITH THE OLD PARENT
			$query = "UPDATE ".cms_db_prefix()."module_'.$infos['nameofmodule'].'_'.$level[0].' SET item_order=(item_order-1) WHERE item_order > ? AND parent=?";
			$db->Execute($query, array($item->item_order, $params["oldparent"]));
			// GET NEW ITEM ORDER
			$item->item_order = $this->countsomething("'.$level[0].'","id",array("parent"=>$item->parent)) + 1;
			$query = "UPDATE ".cms_db_prefix()."module_'.$infos['nameofmodule'].'_'.$level[0].' SET item_order=? WHERE id=?";
			$db->Execute($query, array($item->item_order, $item->id));
		}
';
	}else{
		$phpfile .= '
		if(isset($params["oldparent"]) && $params["oldparent"] != $item->parent){
			// the item is changing parent, and we\'re ordering by parents
			// UPDATE THE ORDER OF THE ITEMS WITH THE OLD PARENT
			$query = "UPDATE ".cms_db_prefix()."module_'.$infos['nameofmodule'].'_'.$level[0].' SET item_order=(item_order-1) WHERE item_order > ? AND parent=?";
			$db->Execute($query, array($item->item_order, $params["oldparent"]));
			// UPDATE NEW PARENT
			$query = "UPDATE ".cms_db_prefix()."module_'.$infos['nameofmodule'].'_'.$level[0].' SET item_order=(item_order+1) WHERE parent=?";
			$db->Execute($query, array($item->parent));
			// GET NEW ITEM ORDER
			$item->item_order = 1;
			$query = "UPDATE ".cms_db_prefix()."module_'.$infos['nameofmodule'].'_'.$level[0].' SET item_order=? WHERE id=?";
			$db->Execute($query, array($item->item_order, $item->id));
		}
';
	}
}

$phpfile .= '
		$redirect = true;
		//if(mysql_affected_rows()){	// mysql-only
		if($db->Affected_Rows()){
			// IF ANYTHING WAS MODIFIED, WE MUST UPDATE THE SEARCH INDEX AND SEND AN EVENT...
			if(isset($event))	$this->SendEvent($event, array("what"=>"'.$level[0].'", "itemid" => $item->id, "alias"=>$item->alias));
			debug_buffer("SEARHC INDEX WAS UPDATED ".__LINE__);
			$module =& $this->GetModuleInstance("Search");
			if ($module != FALSE) {
				$text = "'.$indexedfields.'";
				$module->AddWords($this->GetName(), $item->id, "'.$level[0].'", $text, NULL);
			  }
		}elseif(mysql_error()){
			// do not redirect :
			$redirect = false;
			echo $this->ShowErrors(mysql_error());
		}

		';

if($sharedbyparents && $parentname != false){
	$phpfile .= '		if(	$selectedparents != $params["parents"] ){
			$query = "DELETE FROM ".cms_db_prefix()."module_'.$infos['nameofmodule'].'_'.$parentname.'_has_'.$level[0].' WHERE '.$level[0].'_id=?";
			$db->Execute($query,array($item->id));
			foreach($params["parents"] as $oneparent){
				$query = "INSERT INTO ".cms_db_prefix()."module_'.$infos['nameofmodule'].'_'.$parentname.'_has_'.$level[0].' SET '.$level[0].'_id=? AND '.$parentname.'_id=?";
				$db->Execute($query,array($item->id,$oneparent));
			}
		}

		';
}

$phpfile .= '// REDIRECTING...
			';
if($redirect != '')	$phpfile .= $redirect.'else';
$phpfile .= 'if($redirect == false){
			}elseif(isset($params["apply"])){
				$params["module_message"] = $this->lang("message_modified");
			}else{
				$params = array("module_message" => $this->lang("message_modified"), "active_tab"=>"'.$level[0].'");
				$this->Redirect($id, "defaultadmin", $returnid, $params);	
			}
	}
	// END OF FORM SUBMISSION
}


';

/*###################################################
## PART II : PREPARING INPUTS & DISPLAY
*/
$phpfile .= '
/* ## PREPARING SMARTY ELEMENTS
CreateInputText : (id,name,value,size,maxlength)
CreateInputTextArea : (wysiwyg,id,text,name)
CreateInputSelectList : (id,name,items,selecteditems,size)
CreateInputDropdown : (id,name,items,sindex,svalue)
*/
';

foreach($level[4] as $field){
	if(!in_array($field[0],array('id','item_order','isdefault','active','parent','alias')) && $field[1] < 10) {
		$fieldlabel = ($field[0] == 'name')?'name':$level[0].'_'.$field[0];
		$phpfile .= '
$this->smarty->assign("'.$field[0].'_label", $this->Lang("'.$fieldlabel.'"));
$this->smarty->assign("'.$field[0].'_input", ';
		switch($field[1]){
			case 0: case 1:
				$phpfile .= '$this->CreateInputText($id,"'.$prefix.$field[0].'",isset($item)?$item->'.$field[0].':"",30,10));';
				break;
			case 2:
				$phpfile .= '$this->CreateInputText($id,"'.$prefix.$field[0].'",isset($item)?$item->'.$field[0].':"",30,32));';				
				break;
			case 3:
				$phpfile .= '$this->CreateInputText($id,"'.$prefix.$field[0].'",isset($item)?$item->'.$field[0].':"",50,64));';				
				break;
			case 5:
				$phpfile .= '$this->CreateTextArea('.($field[0] == 'template'?'false':'true').',$id,isset($item)?$item->'.$field[0].':"","'.$prefix.$field[0].'"));';				
				break;
			case 6: case 7:
				if($field[5]['listmode'] == 1){
					$phpfile .= '$this->CreateInputDropdown($id,"'.$prefix.$field[0].'",$'.$field[0].'options,-1,isset($item)?$item->'.$field[0].':0));';
				}elseif($field[5]['listmode'] == 2){
					$phpfile .= '$this->CreateInputSelectList($id,"'.$prefix.$field[0].'[]",$'.$field[0].'options,isset($item)?$item->'.$field[0].':array()));';
				}elseif($field[5]['listmode'] == 3){
					$phpfile .= '$this->CreateInputRadioGroup($id, "'.$prefix.$field[0].'",$'.$field[0].'options,isset($item)?$item->'.$field[0].':"", "", " "));';
				}elseif($field[5]['listmode'] == 4){
					$phpfile .= '$this->DoCheckboxes($id, "'.$prefix.$field[0].'", $'.$field[0].'options, isset($item)?$item->'.$field[0].':0));';
				}
				break;
			case 8:
				$phpfile .= '"<p>".((isset($item->'.$field[0].') && $item->'.$field[0].' != "")?\'<img src="../uploads'.($field[5]['upfolder'] != ''?'/'.$field[5]['upfolder']:'').'\'.$item->'.$field[0].'.\'" /><br/>\'.$this->CreateInputSubmit($id,"remove_'.$field[0].'",$this->Lang("Remove"))." ":"").$this->CreateInputSubmit($id,"add_'.$field[0].'",$this->Lang("'.$level[0].'_select_'.$field[0].'"))."</p>");';
				break;
			case 9:
				$phpfile .= '"<p>".((isset($item->'.$field[0].') && $item->'.$field[0].' != "")?\'<a href="../uploads'.($field[5]['upfolder'] != ''?'/'.$field[5]['upfolder']:'').'\'.$item->'.$field[0].'.\'" >\'.$item->'.$field[0].'.\'</a><br/>\'.$this->CreateInputSubmit($id,"remove_'.$field[0].'",$this->Lang("Remove"))." ":"").$this->CreateInputSubmit($id,"add_'.$field[0].'",$this->Lang("'.$level[0].'_select_'.$field[0].'"))."</p>");';
				break;
			default:
				$phpfile .= '$this->CreateInputText($id,"'.$prefix.$field[0].'",isset($item)?$item->'.$field[0].':"",50,255));';
				break;				

		}
	}elseif($field[1] == 10){
		$fieldlabel = ($field[0] == 'name')?'name':$level[0].'_'.$field[0];
		$phpfile .= '
$this->smarty->assign("'.$field[0].'_label", $this->Lang("'.$fieldlabel.'"));
$this->smarty->assign("'.$prefix.'_'.$field[0].'", isset($item)?$db->UnixTimeStamp($item->'.$field[0].'):time());
$this->smarty->assign("'.$prefix.'_'.$field[0].'_prefix", $id."'.$prefix.'_'.$field[0].'_");';

	}

}

if($parentname != false){
	$phpfile .= '
$this->smarty->assign("parent_label", $this->Lang("'.$parentname.'"));
$this->smarty->assign("parent_input", ';
	if($sharedbyparents){
		$phpfile .= '$this->CreateInputSelectList($id,"'.$prefix.'parent[]",$'.'parentoptions,isset($selectedparents)?$selectedparents:array()));';
	}else{
		$phpfile .= '$this->CreateInputDropdown($id,"'.$prefix.'parent",$'.'parentoptions,-1,isset($item)?$item->parent:0));';
		$phpfile .= 'if(isset($item)) $this->CreateInputHidden($id, "oldparent", $item->parent);';
	}
}


$phpfile .= '
$this->smarty->assign("itemalias",isset($item->alias)?"(alias : ".$item->alias.")":"");
$this->smarty->assign("edittitle", $this->Lang("edit_'.$level[0].'"));

$this->smarty->assign("submit", $this->CreateInputSubmit($id, "submit", lang("submit")));
$this->smarty->assign("apply", (isset($item) && isset($item->id))?$this->CreateInputSubmit($id, "apply", lang("apply")):"");
$this->smarty->assign("cancel", $this->CreateInputSubmit($id, "cancel", lang("cancel")));


// DISPLAYING
if(isset($item) && isset($item->id)){';
if(count($multiplefiles) > 0){
	$tabheaders = 'echo $this->SetTabHeader("'.$level[0].'", $this->Lang("'.$level[0].'"), ($tab=="'.$level[0].'"?true:false));';
	$tabcontents = '';
	foreach($multiplefiles as $field){
		$tabheaders .= 'echo $this->SetTabHeader("'.$field[0].'", $this->Lang("'.$level[0].'_'.$field[0].'"), ($tab=="'.$field[0].'"?true:false));
		';
		$tabcontents .= '
	echo $this->StartTab("'.$field[0].'");
		echo "<p>".$this->Lang("warning_tab")."</p>";
		$newparams = array("'.$prefix.'id"=>$item->id,"field"=>"'.$level[0].'_'.$field[0].'","tablename"=>"'.$infos['nameofmodule'].'_multiplefilesfields","prefix"=>"'.$prefix.'", "startdir"=>"'.($field[3]?$field[3]:'').'", "mode"=>"other", "addfiles"=>true, "active_tab"=>"'.$field[0].'");
		echo "<p>".$this->CreateLink($id, "browsefiles", $returnid, $admintheme->DisplayImage("icons/system/newobject.gif", "","","","systemicon")." ".$this->Lang("addafile"), $newparams)."</p>";
		$item->'.$field[0].' = $this->getaddfiles("'.$level[0].'_'.$field[0].'",$item->id);
		if(count($item->'.$field[0].')>0){
			echo "<table>";
			foreach($item->'.$field[0].' as $addfile){
				echo \'<tr><td><a href="'.str_replace('//','/','../uploads/'.$field[3]).'\'.$addfile->filepath.\'" target="blank">\'.$addfile->filepath."</a></td><td>";
				echo $this->CreateLink($id, $action, $returnid, $admintheme->DisplayImage("icons/system/delete.gif", $this->lang("delete"),"","","systemicon"), array("'.$prefix.'id"=>$item->id, "deletefile"=>$addfile->fileid, "active_tab"=>"'.$field[0].'"));
				echo "</td></tr>";
			}
			echo "</table>";
		}
	echo $this->EndTab();
	';
	}

	$phpfile .= '
	$tab = isset($params["active_tab"])?$params["active_tab"]:"'.$level[0].'";
	echo $this->StartTabHeaders();
		'.$tabheaders.'
	echo $this->EndTabHeaders();
	echo $this->StartTabContent();
	echo $this->StartTab("'.$level[0].'");
';
}
$phpfile .= '
		echo $this->CreateFormStart($id, "edit'.$prefix.'", $returnid);
		echo $this->ProcessTemplate("edit'.$prefix.'.tpl");
		echo $this->CreateInputHidden($id, "'.$prefix.'id", $item->id);
		if(isset($item) && isset($item->parent)) echo $this->CreateInputHidden($id, "oldparent", $item->parent);
		echo $this->CreateInputHidden($id, "'.$prefix.'item_order", $item->item_order);
		echo $this->CreateFormEnd();
	';
if(count($multiplefiles) > 0){
	$phpfile .= '
	echo $this->EndTab();
	'.$tabcontents.'
	echo $this->EndTabContent();
	';
}
	$phpfile .= '

}else{
	echo $this->CreateFormStart($id, "edit'.$prefix.'", $returnid);
	echo $this->ProcessTemplate("edit'.$prefix.'.tpl");
	echo $this->CreateFormEnd();
}
?>';


?>
