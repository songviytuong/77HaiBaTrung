<?php

$moduleFile = '<?php
#-------------------------------------------------------------------------
# Module: '.$infos['nameofmodule'].'
# Version: 1.0, 
#
#-------------------------------------------------------------------------
# CMS - CMS Made Simple is (c) 2008 by Ted Kulp (wishy@cmsmadesimple.org)
# This project"s homepage is: http://www.cmsmadesimple.org
#
# This module was created with CTLModuleMaker '.$makerversion.', which is
# based on material from the ModuleMaker module version 0.2
# Copyright (c) 2008 by Samuel Goldstein (sjg@cmsmadesimple.org) 
#
#-------------------------------------------------------------------------
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
# Or read it online: http://www.gnu.org/licenses/licenses.html#GPL
#
#-------------------------------------------------------------------------

class '.$infos['nameofmodule'].' extends CMSModule
{

	function GetName()
	{
		return "'.$infos['nameofmodule'].'";
	}

	/*---------------------------------------------------------
	   GetFriendlyName()
	   This can return any string, preferably a localized name
	   of the module. This is the name that"s shown in the
	   Admin Menus and section pages (if the module has an admin
	   component).
	   
	   See the note on localization at the top of this file.
	  ---------------------------------------------------------*/
	function GetFriendlyName()
	{
		return $this->Lang("friendlyname");
	}
	
	/*---------------------------------------------------------
	   GetVersion()
	   This can return any string, preferably a number or
	   something that makes sense for designating a version.
	   The CMS will use this to identify whether or not
	   the installed version of the module is current, and
	   the module will use it to figure out how to upgrade
	   itself if requested.	   
	  ---------------------------------------------------------*/
	function GetVersion()
	{
		return "'.$infos['version'].'";
	}


	/*---------------------------------------------------------
	   GetDependencies()
	   Your module may need another module to already be installed
	   before you can install it.
	   This method returns a list of those dependencies and
	   minimum version numbers that this module requires.
	   
	   It should return an hash, eg.
	   return array("somemodule"=>"1.0", "othermodule"=>"1.1");
	  ---------------------------------------------------------*/
	function GetDependencies()
	{
		return array();
	}

	/*---------------------------------------------------------
	   GetHelp()
	   This returns HTML information on the module.
	   Typically, you"ll want to include information on how to
	   use the module.
	   
	   See the note on localization at the top of this file.
	  ---------------------------------------------------------*/
	function GetHelp()
	{
		return $this->Lang("help");
	}

	/*---------------------------------------------------------
	   GetAuthor()
	   This returns a string that is presented in the Module
	   Admin if you click on the "About" link.
	  ---------------------------------------------------------*/
	function GetAuthor()
	{
		return "CTLModuleMaker '.$makerversion.'";
		// of course you may change this, but it would be nice
		// to keep a mention of the CTLModuleMaker somewhere
	}


	/*---------------------------------------------------------
	   GetAuthorEmail()
	   This returns a string that is presented in the Module
	   Admin if you click on the "About" link. It helps users
	   of your module get in touch with you to send bug reports,
	   questions, cases of beer, and/or large sums of money.
	  ---------------------------------------------------------*/
	function GetAuthorEmail()
	{
		return "";
	}


	/*---------------------------------------------------------
	   IsPluginModule()
	   This function returns true or false, depending upon
	   whether users can include the module in a page or
	   template using a smarty tag of the form
	   {cms_module module="Prod" param1=val param2=val...}
	   
	   If your module does not get included in pages or
	   templates, return "false" here.
	  ---------------------------------------------------------*/
	function IsPluginModule()
	{
		return true;
	}


	/*---------------------------------------------------------
	   HasAdmin()
	   This function returns a boolean value, depending on
	   whether your module adds anything to the Admin area of
	   the site. For the rest of these comments, I"ll be calling
	   the admin part of your module the "Admin Panel" for
	   want of a better term.
	  ---------------------------------------------------------*/
	function HasAdmin() {	return true;	}
	function GetAdminSection() {return "content";}
	function GetAdminDescription() {return $this->Lang("admindescription");}

	function VisibleToAdminUser(){
		return (';

$tmppermissions = '';
foreach($levels as $level){
	$tmppermissions .= ($tmppermissions==''?'':' || ').'$this->CheckPermission("'.$infos['nameofmodule'].'_manage_'.$level[0].'")';
}	

$moduleFile .= $tmppermissions.');
	}

	/*---------------------------------------------------------
	   SetParameters()
	   This function enables you to create mappings for
	   your module when using "Pretty Urls".
	   
	   Typically, modules create internal links that have
	   big ugly strings along the lines of:
	   index.php?mact=ModName,cntnt01,actionName,0&cntnt01param1=1&cntnt01param2=2&cntnt01returnid=3
	   
	   You might prefer these to look like:
	   /ModuleFunction/2/3
	   
	   To do this, you have to register routes and map
	   your parameters in a way that the API will be able
	   to understand.

	   Also note that any calls to CreateLink will need to
	   be updated to pass the pretty url parameter.
	   
	   Since the Skeleton doesn"t really create any links,
	   the section below is commented out, but you can
	   use it to figure out pretty urls.
	   
	   ---------------------------------------------------------*/
	function SetParameters()
	{
';
$modname = '['.substr($infos['nameofmodule'],0,1).strtoupper(substr($infos['nameofmodule'],0,1)).']'.substr($infos['nameofmodule'],1);

$moduleFile .= '
		// these are for internal pretty URLS.
		// you may change these, but you will also need to change the BuildPrettyURLs function below accordingly
		$this->RegisterRoute("/'.$modname.'\\/([Dd]etail)\\/(?P<alias>[^\\/]+)\\/(?P<returnid>[0-9]+)$/");
		$this->RegisterRoute("/'.$modname.'\\/(?P<what>[^\\/]+)\\/(?P<returnid>[0-9]+)$/");
		$this->RegisterRoute("/'.$modname.'\\/(?P<what>[^\\/]+)\\/(?P<parent>[^\\/]+)\\/(?P<returnid>[0-9]+)$/");
		$this->RegisterRoute("/'.$modname.'\\/(?P<what>[^\\/]+)\\/(?P<pageindex>[0-9]+)\\/(?P<nbperpage>[0-9]+)\\/(?P<returnid>[0-9]+)$/");
		$this->RegisterRoute("/'.$modname.'\\/(?P<what>[^\\/]+)\\/(?P<parent>[^\\/]+)\\/(?P<pageindex>[0-9]+)\\/(?P<nbperpage>[0-9]+)\\/(?P<returnid>[0-9]+)$/");

		$this->CreateParameter("action", "default", $this->Lang("phelp_action"));
		$this->CreateParameter("what", "", $this->Lang("phelp_what"));
		$this->CreateParameter("alias", "", $this->Lang("phelp_alias"));
		$this->CreateParameter("showdefault", false, $this->Lang("phelp_showdefault"));
		$this->CreateParameter("parent", "", $this->Lang("phelp_parent"));
		$this->CreateParameter("limit", 0, $this->Lang("phelp_limit"));
		$this->CreateParameter("nbperpage", 0, $this->Lang("phelp_nbperpage"));
		$this->CreateParameter("orderby", 0, $this->Lang("phelp_orderby"));
		$this->CreateParameter("detailpage", "", $this->Lang("phelp_detailpage"));
		$this->CreateParameter("random", 0, $this->Lang("phelp_random"));
		$this->CreateParameter("listtemplate", "", $this->lang("phelp_listtemplate"));
		$this->CreateParameter("finaltemplate", "", $this->lang("phelp_finaltemplate"));
		$this->CreateParameter("forcelist", "0", $this->lang("phelp_forcelist"));
		$this->CreateParameter("inline", 0, $this->lang("phelp_inline"));
		$this->CreateParameter("pageindex", "", $this->lang("phelp_internal"));
	}

    function GetEventDescription($eventname)
    {
		$eventname = str_replace("'.$infos['nameofmodule'].'", "", $eventname);
		return $this->lang("eventdesc".$eventname);
    }
    
	function InstallPostMessage()
	{
		return $this->Lang("postinstall");
	}
	function UninstallPostMessage()
	{
		return $this->Lang("postuninstall");
	}
	function UninstallPreMessage()
	{
		return $this->Lang("really_uninstall");
	}


	/*---------------------------------------------------------
	   Install()
	   When your module is installed, you may need to do some
	   setup. Typical things that happen here are the creation
	   and prepopulation of database tables, database sequences,
	   permissions, preferences, etc.
	   	   
	   For information on the creation of database tables,
	   check out the ADODB Data Dictionary page at
	   http://phplens.com/lens/adodb/docs-datadict.htm
	   
	   This function can return a string in case of any error,
	   and CMS will not consider the module installed.
	   Successful installs should return FALSE or nothing at all.
	  ---------------------------------------------------------*/
	function Install()
	{
		global $gCms;
		require "method.install.php";
	}

	/*---------------------------------------------------------
	   Uninstall()
	   Sometimes, an exceptionally unenlightened or ignorant
	   admin will wish to uninstall your module. While it
	   would be best to lay into these idiots with a cluestick,
	   we will do the magnanimous thing and remove the module
	   and clean up the database, permissions, and preferences
	   that are specific to it.
	   This is the method where we do this.
	  ---------------------------------------------------------*/
	function Uninstall()
	{
		global $gCms;
		require "method.uninstall.php";
	}


    function SearchResult($returnid, $itemid, $level = "")
    {
		$result = array();
		$wantedparam = false;
		$newparams = array();
		if($level == "'.$levels[count($levels)-1][0].'"){
			// we seek an element of the last level, and will display the detail view
			$wantedparam = "alias";
		}else{
			if($newparams["what"] = $this->get_nextlevel($level)){
			// we seek an element of another level, and will display the list view of its children
				$wantedparam = "parent";
			}
		}
		if ($wantedparam){
			$tablename = cms_db_prefix()."module_'.$infos['nameofmodule'].'_".$level;
			$db =& $this->GetDb();
			$query = "SELECT name, alias FROM $tablename WHERE id = ?";
			$dbresult = $db->Execute( $query, array( $itemid ) );
			if ($dbresult){
				$row = $dbresult->FetchRow();
				$newparams[$wantedparam] = $row["alias"];

				//0 position is the prefix displayed in the list results.
				$result[0] = $this->GetFriendlyName();

				//1 position is the title
				$result[1] = $row["name"];
		
				//2 position is the URL to the title.
				$result[2] = $this->CreateLink($id, "default", $returnid, "", $newparams, "", true, false, "", false, $this->BuildPrettyUrls($newparams, $returnid));
			}
		}

		return $result;
	}

/* ---------------------------------------------
NOT PART OF THE NORMAL MODULE API
----------------------------------------------*/

	function plcreatealias($name){
		// creates the alias for new elements...
		// as a suggestion from AMT, the first part deals with smart quotes
 		$search = array(chr(0xe2) . chr(0x80) . chr(0x98),
						  chr(0xe2) . chr(0x80) . chr(0x99),
						  chr(0xe2) . chr(0x80) . chr(0x9c),
						  chr(0xe2) . chr(0x80) . chr(0x9d),
						  chr(0xe2) . chr(0x80) . chr(0x93),
						  chr(0xe2) . chr(0x80) . chr(0x94));
 		$name = str_replace($search, "", $name);
 		// the second part uses the cms version
 		$alias = munge_string_to_url($name, false);
 		return $alias;
	} 
		
	function checkalias($dbtable, $alias, $itemid=false, $idfield="id", $aliasfield="alias"){
		// checks if this alias already exists in the level
		$query = "SELECT ".$idfield." FROM ".cms_db_prefix().$dbtable." WHERE ".$aliasfield." = ?";
		if($itemid) $query .= " AND ".$idfield."!=".$itemid;
		$db = $this->GetDb();
		$dbresult = $db->Execute($query,array($alias));
		$targetid = 0;
		if($dbresult && $row = $dbresult->FetchRow()) $targetid = $row["id"];
		return ($targetid == 0);
	}

    function getDefaultTemplates(){
    	// returns an array of the templates that are selected as default (just so that we don\'t delete them)
	   $result = array();
	   $result[] = $this->GetPreference("finaltemplate");
	   ';
foreach($levels as $level){
	 $moduleFile .= '$result[] = $this->GetPreference("listtemplate_'.$level[0].'");
	   ';
}
$moduleFile .= 'return $result;
    }

	function BuildPrettyUrls($params, $returnid=-1){
		$prettyurl = "'.$infos["nameofmodule"].'/";
		if(isset($params["alias"])){
			$prettyurl .= "detail/".$params["alias"];
		}elseif(isset($params["parent"])){
			$prettyurl .= $params["what"]."/".$params["parent"];
		}else{
			$prettyurl .= $params["what"];
		}
		if(!isset($params["alias"]) && isset($params["pageindex"]) && isset($params["nbperpage"]))	$prettyurl .= "/".$params["pageindex"]."/".$params["nbperpage"];
		$prettyurl .= "/".$returnid;
		return $prettyurl;
	}


	function DoAction($action, $id, $params, $returnid=-1){
		global $gCms;
		';

$moduleFile .= $this->createActions($infos, $levels);
$moduleFile .= '

	}

	function DoCheckboxes($id, $name, $choices, $selected=array(), $delimiter="<br/>"){
		$output = "";
		foreach($choices as $key=>$value){
			$output .= $this->CreateInputCheckbox($id, $name."[]", $value, (in_array($value, $selected)?$value:0))." ".$key.$delimiter;
		}
		return $output;
	}

	function parsekeywords($string){
		$inside = (substr($string,0,1) == \'"\');
		$parts = explode(\'"\',$string);
	
		if(count($parts) < 2){
			if($inside){
				return array(str_replace(\'"\',"",$string));
			}else{
				return explode(" ",$string);
			}
		}
	
		$keywords = array();
		foreach($parts as $part){
			if($part != ""){
				if($inside){
					$keywords[] = $part;
				}else{
					$words = explode(" ",trim($part));
					foreach($words as $word){
						if(trim($word) != "")	$keywords[] = $word;
					}
				}
				$inside = !$inside;
			}
		}
		return $keywords;
	}

';

$moduleFile .= $this->createDBGetFunctions($infos, $levels, $predefinedlists, $multiplefiles);

if($infos['hasfilefields']) $moduleFile .= $this->getFileContent('functions.images.php');

$moduleFile .= '

}

?>
';

?>
