<?php
if(!isset($levels)) exit;
########################################################
## LEVELS

$i = 0;
$sharedbyparents = false;
while($i < count($levels)){
	if($i != 0){
		$sharedbyparents = ($levels[$i - 1][2] == 1);
		$parentname = $levels[$i - 1][0];
	}
	$level = $levels[$i];
	$dbget .= '
	function get_level_'.$level[0].'($where=array(),$admin=false,$id="",$returnid="",$order=false,$limit=0,$customwhere=false,$customvalues=array()){
		if(!$order)	$order = "normal";
		$db =& $this->GetDb();
		$wherestring = "";
		$wherevalues = array();
		
';
	if($i == 0){
	// THIS IS THE HIGHEST LEVEL
		$dbget .= '
		if($customwhere){
			$wherestring = $customwhere;
			$wherevalues = $customvalues;
		}else{
			foreach($where as $key=>$value){
				$wherestring .= ($wherestring == ""?"":" AND ").$key."=?";
				$wherevalues[] = $value;
			}
		}
		$query = "SELECT * FROM ".cms_db_prefix()."module_'.$modulename.'_'.$level[0].' A ".($wherestring == ""?"":" WHERE ".$wherestring)." ORDER BY item_order";

		';
	

	}elseif($sharedbyparents == true){
	// THIS LEVEL MAY HAVE SEVERAL PARENTS
		$dbget .= '
		$parentalias = false;
		$parentid = false;
		if($customwhere){
			$wherestring = $customwhere;
			$wherevalues = $customvalues;
		}else{
			foreach($where as $key=>$value){
				if($key == "parent"){
					$parentalias = $value;
				}elseif($key == "parent_id"){
					$parentid = $value;
				}else{
					$wherestring .= ($wherestring == ""?"":" AND ")."A.".$key."=?";
					$wherevalues[] = $value;
				}
			}
		}
		
		$query = "SELECT A.*";
		if($parentalias || $parentid) $query .= ", C.id parent_id, C.name parent_name, C.alias parent_alias";
		$query .= " FROM ".cms_db_prefix()."module_'.$modulename.'_'.$level[0].' A LEFT JOIN ".cms_db_prefix()."module_'.$modulename.'_'.$parentname.'_has_'.$level[0].' B ON A.id = B.'.$level[0].'_id LEFT JOIN ".cms_db_prefix()."module_'.$modulename.'_'.$parentname.' C ON B.'.$parentname.'_id = C.id";
		if($parentalias){
			$query .= " WHERE C.alias = \'$parentalias\'";
			if($wherestring != "") $query .= " AND ".$wherestring;
		}elseif($parentid){
			$query .= " WHERE C.id = \'$parentid\'";
			if($wherestring != "") $query .= " AND ".$wherestring;			
		}else{
			if($wherestring != "") $query .= " WHERE ".$wherestring." GROUP BY A.id";
		}
		if($order == "modified"){
			 $query .= " ORDER BY A.date_modified DESC";
		}elseif($order == "created"){
			 $query .= " ORDER BY A.id DESC";
		}elseif($order == "name"){
			 $query .= " ORDER BY A.name";	
		}else{
			 $query .= " ORDER BY A.item_order";
		}

		';
	}elseif($sharedbyparents == false){
	// THIS LEVEL HAS ONLY ONE PARENT
		$dbget .= '
		if($customwhere){
			$wherestring = $customwhere;
			$wherevalues = $customvalues;
		}else{
			foreach($where as $key=>$value){
				if($key == "parent"){
					$wherestring .= ($wherestring == ""?"":" AND ")."B.alias=?";
				}elseif($key == "parent_id"){
					$wherestring .= ($wherestring == ""?"":" AND ")."B.id=?";
				}else{
					$wherestring .= ($wherestring == ""?"":" AND ")."A.".$key."=?";
				}
				$wherevalues[] = $value;
			}
		}
		
		$query = "SELECT A.*, B.id parent_id, B.name parent_name, B.alias parent_alias FROM ".cms_db_prefix()."module_'.$modulename.'_'.$level[0].' A, ".cms_db_prefix()."module_'.$modulename.'_'.$parentname.' B WHERE A.parent = B.id ".($wherestring == ""?"":" AND ").$wherestring;
		if($order == "modified"){
			 $query .= " ORDER BY A.date_modified DESC";
		}elseif($order == "created"){
			 $query .= " ORDER BY A.id DESC";
		}elseif($order == "name"){
			 $query .= " ORDER BY A.name";	
		}else{
			 $query .= " ORDER BY '.($level[6]?'B.item_order, ':'').'A.item_order";
		}
		';
	}

$dbget .= 'if($limit > 0)	$query .= " LIMIT ".$limit;
';

// PREPARING nbchildren
	$childrencount = '
';
	if(!isset($levels[$i + 1])){

	}elseif($level[2] == 1){
		$childrencount = '
		$item->nbchildren = $this->countsomething("'.$level[0].'_has_'.$levels[$i + 1][0].'","'.$levels[$i + 1][0].'_id",array("'.$level[0].'_id" => $item->id));
		';
	}elseif($level[2] == 0){
		$childrencount = '
		$item->nbchildren = $this->countsomething("'.$levels[$i + 1][0].'","id",array("parent" => $item->id));
		';
	}

// PREPARING FIELDS PARSING
	$parsedfields = '';
	$addfiles = array();
	foreach($level[4] as $field){
		// Preparing list options
		if($field[1] == 6){
			$dbget .= '
			$options_'.$field[0].' = $this->get_predefinedoptions("'.$level[0].'_'.$field[0].'",true);';
		}elseif($field[1] == 7){
			$dbget .= '
			$options_'.$field[0].' = $this->get_id2namearray("'.$level[1].'_'.$field[0].'_options");';
		}

		if($field[1] < 5 && $field[1] != 0){
			$parsedfields .= '
			$item->'.$field[0].' = stripslashes($item->'.$field[0].');';
		}elseif( ($field[1] == 6 || $field[1] == 7) && ($field[5]['listmode'] == 2 || $field[5]['listmode'] == 4) ) {
			$parsedfields .= '
			$tmpoptions = unserialize($item->'.$field[0].');
			if(!is_array($tmpoptions)) $tmpoptions = array();
			$item->'.$field[0].' = array();
			$item->'.$field[0].'_namevalue = array();
			if(count($tmpoptions)>0){
				foreach($tmpoptions as $onevalue){
					if(isset($options_'.$field[0].'[$onevalue])){
						$item->'.$field[0].'[] = $onevalue;
						$item->'.$field[0].'_namevalue[] = $options_'.$field[0].'[$onevalue];
					}
				}
			}';
		}elseif( ($field[1] == 7 || $field[1] == 6) && ($field[5]['listmode'] == 1 || $field[5]['listmode'] == 3) ) {
			$parsedfields .= '
			$item->'.$field[0].' = (isset($options_'.$field[0].'[$item->'.$field[0].']))?$item->'.$field[0].':false;
			$item->'.$field[0].'_namevalue = (isset($options_'.$field[0].'[$item->'.$field[0].']))?$options_'.$field[0].'[$item->'.$field[0].']:"";';
		}elseif( $field[1] == 8 && $field[5]['thumb'] != '' ){
			$parsedfields .= '
			if($item->'.$field[0].' != "" && $item->'.$field[0].' != "/") $item->'.$field[0].'_thumbnail = preg_replace("/(.+)?\\/([^\\/]+)\\.(.+)/","$1/plthumb_$2.$3",$item->'.$field[0].');';
		}elseif( $field[1] == 11 ){
			$parsedfields .= '
			$item->'.$field[0].' = $this->getaddfiles("'.$level[0].'_'.$field[0].'",$item->id,false);';
			$addfiles[] = $level[0].'_'.$field[0];
		}
	}
	$addfiles = (count($addfiles) > 1)?implode(',',$addfiles):'';


	
	$dbget .= '
		$dbresult = $db->Execute($query,$wherevalues);
		$itemlist = array();
		while ($dbresult && $row = $dbresult->FetchRow()){
			$item = new stdClass();
			foreach($row as $key=>$value){
				$item->$key = $value;
			}';

	$dbget .= $parsedfields;
	$dbget .= $childrencount;

	$dbget .= '
			if($admin == true) $item = $this->addadminlinks($item,"'.$level[1].'","'.$modulename.'_'.$level[0].'",'.(isset($levels[$i+1])?'"'.$modulename.'_'.$levels[$i+1][0].'"':'false').',"'.$level[0].'",$id,$returnid,'.$level[7].','.$level[6].',"'.$addfiles.'");

			array_push($itemlist,$item);
		}

		return $itemlist;
	}
';

$i++;
}
?>
