<?php
if (!isset($gCms)) exit;
$admintheme = $gCms->variables["admintheme"];

if (isset($params["cancel"]))	$this->Redirect($id, "defaultadmin", $returnid, array("active_tab" => "items"));

$db =& $this->GetDb();


if(isset($params["itemsid"])) {
	// if we are working on an item that exists, we load it. We must do this even when the form is submitted, otherwise we won't have the file fields
	$items = $this->get_level_items(array("id"=>$params["itemsid"]));
	$item = $items[0];
}

// CHECK IF THE FORM IS BEING SUBMITTED :
// (we must detect all kinds of submit buttons, including files, since information must be saved before we go to file submission)
if (isset($params["submit"]) || 
	isset($params["apply"]) 
	)
{
	debug_buffer("Edit Form has been submitted".__LINE__);

	// RETRIEVING THE FORM VALUES (and escaping it, if needed)
	if(!isset($item)) $item = new stdClass();
	if(isset($params["itemsitem_order"])) $item->item_order = $params["itemsitem_order"];
	$item->phone = $params["itemsphone"];
		$item->sex = $params["itemssex"];
		$item->address = $params["itemsaddress"];
		$item->birthday = $params["itemsbirthday"];
		$item->name = $params["itemsname"];
		
	$item->alias = $this->plcreatealias($item->name);

	// CHECK IF THE NEEDED VALUES ARE THERE
	if(	!isset($params["itemsname"]) || $params["itemsname"] == ""
		 )
	{
		echo $this->ShowErrors($this->Lang("error_missginvalue"));
	}elseif(false == $this->checkalias("module_vipmember_items", $item->alias, isset($params["itemsid"])?$params["itemsid"]:false)){
		echo $this->ShowErrors($this->Lang("error_alreadyexists"));
	}else{
		############ DOING THE UPDATE

		// FIELDS TO UPDATE
		$query = (isset($item->id)?"UPDATE ":"INSERT INTO ").cms_db_prefix()."module_vipmember_items SET 
			phone=?,
			sex=?,
			address=?,
			birthday=?,
			name=?,
		alias=?,
		date_modified=?,
		active=".(isset($item->active)?$item->active:1).",
		isdefault=".(isset($item->isdefault)?$item->isdefault:0)."";
			
		// VALUES
		$values = array(addslashes($item->phone),
			addslashes($item->sex),
			addslashes($item->address),
			addslashes($item->birthday),
			addslashes($item->name),$item->alias,str_replace("'","",$db->DBTimeStamp(time())));

		if(isset($item->id)){
			$event = "vipmember_modified";
			$query .= " WHERE id=?;";
			array_push($values,$item->id);
		}else{
			// NEW ITEM
			$event = "vipmember_added";
			// get a new id from the sequence table
			$item->id = $db->GenID(cms_db_prefix()."module_vipmember_items_seq");
			// new items get to the top - so we must put all other items down from one step, and then set this item's order to 1
			$query2 = "UPDATE ".cms_db_prefix()."module_vipmember_items SET item_order=(item_order+1)";
			$db->Execute($query2);
			$query .= ",item_order=1, id=".$item->id;
		}
		$db->Execute($query, $values);

		$redirect = true;
		//if(mysql_affected_rows()){	// mysql-only
		if($db->Affected_Rows()){
			// IF ANYTHING WAS MODIFIED, WE MUST UPDATE THE SEARCH INDEX AND SEND AN EVENT...
			if(isset($event))	$this->SendEvent($event, array("what"=>"items", "itemid" => $item->id, "alias"=>$item->alias));
			debug_buffer("SEARHC INDEX WAS UPDATED ".__LINE__);
			$module =& $this->GetModuleInstance("Search");
			if ($module != FALSE) {
				$text = "$item->phone $item->sex $item->address $item->birthday $item->name";
				$module->AddWords($this->GetName(), $item->id, "items", $text, NULL);
			  }
		}elseif(mysql_error()){
			// do not redirect :
			$redirect = false;
			echo $this->ShowErrors(mysql_error());
		}

		// REDIRECTING...
			if($redirect == false){
			}elseif(isset($params["apply"])){
				$params["module_message"] = $this->lang("message_modified");
			}else{
				$params = array("module_message" => $this->lang("message_modified"), "active_tab"=>"items");
				$this->Redirect($id, "defaultadmin", $returnid, $params);	
			}
	}
	// END OF FORM SUBMISSION
}



/* ## PREPARING SMARTY ELEMENTS
CreateInputText : (id,name,value,size,maxlength)
CreateInputTextArea : (wysiwyg,id,text,name)
CreateInputSelectList : (id,name,items,selecteditems,size)
CreateInputDropdown : (id,name,items,sindex,svalue)
*/

$this->smarty->assign("phone_label", $this->Lang("items_phone"));
$this->smarty->assign("phone_input", $this->CreateInputText($id,"itemsphone",isset($item)?$item->phone:"",30,32));
$this->smarty->assign("sex_label", $this->Lang("items_sex"));
$this->smarty->assign("sex_input", $this->CreateInputText($id,"itemssex",isset($item)?$item->sex:"",30,10));
$this->smarty->assign("address_label", $this->Lang("items_address"));
$this->smarty->assign("address_input", $this->CreateInputText($id,"itemsaddress",isset($item)?$item->address:"",50,64));
$this->smarty->assign("birthday_label", $this->Lang("items_birthday"));
$this->smarty->assign("birthday_input", $this->CreateInputText($id,"itemsbirthday",isset($item)?$item->birthday:"",30,32));
$this->smarty->assign("name_label", $this->Lang("name"));
$this->smarty->assign("name_input", $this->CreateInputText($id,"itemsname",isset($item)?$item->name:"",50,64));
$this->smarty->assign("itemalias",isset($item->alias)?"(alias : ".$item->alias.")":"");
$this->smarty->assign("edittitle", $this->Lang("edit_items"));

$this->smarty->assign("submit", $this->CreateInputSubmit($id, "submit", lang("submit")));
$this->smarty->assign("apply", (isset($item) && isset($item->id))?$this->CreateInputSubmit($id, "apply", lang("apply")):"");
$this->smarty->assign("cancel", $this->CreateInputSubmit($id, "cancel", lang("cancel")));


// DISPLAYING
if(isset($item) && isset($item->id)){
		echo $this->CreateFormStart($id, "edititems", $returnid);
		echo $this->ProcessTemplate("edititems.tpl");
		echo $this->CreateInputHidden($id, "itemsid", $item->id);
		if(isset($item) && isset($item->parent)) echo $this->CreateInputHidden($id, "oldparent", $item->parent);
		echo $this->CreateInputHidden($id, "itemsitem_order", $item->item_order);
		echo $this->CreateFormEnd();
	

}else{
	echo $this->CreateFormStart($id, "edititems", $returnid);
	echo $this->ProcessTemplate("edititems.tpl");
	echo $this->CreateFormEnd();
}
?>