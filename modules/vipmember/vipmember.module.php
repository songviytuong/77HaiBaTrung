<?php
#-------------------------------------------------------------------------
# Module: vipmember
# Version: 1.0, 
#
#-------------------------------------------------------------------------
# CMS - CMS Made Simple is (c) 2008 by Ted Kulp (wishy@cmsmadesimple.org)
# This project"s homepage is: http://www.cmsmadesimple.org
#
# This module was created with CTLModuleMaker 1.7, which is
# based on material from the ModuleMaker module version 0.2
# Copyright (c) 2008 by Samuel Goldstein (sjg@cmsmadesimple.org) 
#
#-------------------------------------------------------------------------
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
# Or read it online: http://www.gnu.org/licenses/licenses.html#GPL
#
#-------------------------------------------------------------------------

class vipmember extends CMSModule
{

	function GetName()
	{
		return "vipmember";
	}

	/*---------------------------------------------------------
	   GetFriendlyName()
	   This can return any string, preferably a localized name
	   of the module. This is the name that"s shown in the
	   Admin Menus and section pages (if the module has an admin
	   component).
	   
	   See the note on localization at the top of this file.
	  ---------------------------------------------------------*/
	function GetFriendlyName()
	{
		return $this->Lang("friendlyname");
	}
	
	/*---------------------------------------------------------
	   GetVersion()
	   This can return any string, preferably a number or
	   something that makes sense for designating a version.
	   The CMS will use this to identify whether or not
	   the installed version of the module is current, and
	   the module will use it to figure out how to upgrade
	   itself if requested.	   
	  ---------------------------------------------------------*/
	function GetVersion()
	{
		return "1.0";
	}


	/*---------------------------------------------------------
	   GetDependencies()
	   Your module may need another module to already be installed
	   before you can install it.
	   This method returns a list of those dependencies and
	   minimum version numbers that this module requires.
	   
	   It should return an hash, eg.
	   return array("somemodule"=>"1.0", "othermodule"=>"1.1");
	  ---------------------------------------------------------*/
	function GetDependencies()
	{
		return array();
	}

	/*---------------------------------------------------------
	   GetHelp()
	   This returns HTML information on the module.
	   Typically, you"ll want to include information on how to
	   use the module.
	   
	   See the note on localization at the top of this file.
	  ---------------------------------------------------------*/
	function GetHelp()
	{
		return $this->Lang("help");
	}

	/*---------------------------------------------------------
	   GetAuthor()
	   This returns a string that is presented in the Module
	   Admin if you click on the "About" link.
	  ---------------------------------------------------------*/
	function GetAuthor()
	{
		return "CTLModuleMaker 1.7";
		// of course you may change this, but it would be nice
		// to keep a mention of the CTLModuleMaker somewhere
	}


	/*---------------------------------------------------------
	   GetAuthorEmail()
	   This returns a string that is presented in the Module
	   Admin if you click on the "About" link. It helps users
	   of your module get in touch with you to send bug reports,
	   questions, cases of beer, and/or large sums of money.
	  ---------------------------------------------------------*/
	function GetAuthorEmail()
	{
		return "";
	}


	/*---------------------------------------------------------
	   IsPluginModule()
	   This function returns true or false, depending upon
	   whether users can include the module in a page or
	   template using a smarty tag of the form
	   {cms_module module="Prod" param1=val param2=val...}
	   
	   If your module does not get included in pages or
	   templates, return "false" here.
	  ---------------------------------------------------------*/
	function IsPluginModule()
	{
		return true;
	}


	/*---------------------------------------------------------
	   HasAdmin()
	   This function returns a boolean value, depending on
	   whether your module adds anything to the Admin area of
	   the site. For the rest of these comments, I"ll be calling
	   the admin part of your module the "Admin Panel" for
	   want of a better term.
	  ---------------------------------------------------------*/
	function HasAdmin() {	return true;	}
	function GetAdminSection() {return "content";}
	function GetAdminDescription() {return $this->Lang("admindescription");}

	function VisibleToAdminUser(){
		return ($this->CheckPermission("vipmember_manage_items"));
	}

	/*---------------------------------------------------------
	   SetParameters()
	   This function enables you to create mappings for
	   your module when using "Pretty Urls".
	   
	   Typically, modules create internal links that have
	   big ugly strings along the lines of:
	   index.php?mact=ModName,cntnt01,actionName,0&cntnt01param1=1&cntnt01param2=2&cntnt01returnid=3
	   
	   You might prefer these to look like:
	   /ModuleFunction/2/3
	   
	   To do this, you have to register routes and map
	   your parameters in a way that the API will be able
	   to understand.

	   Also note that any calls to CreateLink will need to
	   be updated to pass the pretty url parameter.
	   
	   Since the Skeleton doesn"t really create any links,
	   the section below is commented out, but you can
	   use it to figure out pretty urls.
	   
	   ---------------------------------------------------------*/
	function SetParameters()
	{

		// these are for internal pretty URLS.
		// you may change these, but you will also need to change the BuildPrettyURLs function below accordingly
		$this->RegisterRoute("/[vV]ipmember\/([Dd]etail)\/(?P<alias>[^\/]+)\/(?P<returnid>[0-9]+)$/");
		$this->RegisterRoute("/[vV]ipmember\/(?P<what>[^\/]+)\/(?P<returnid>[0-9]+)$/");
		$this->RegisterRoute("/[vV]ipmember\/(?P<what>[^\/]+)\/(?P<parent>[^\/]+)\/(?P<returnid>[0-9]+)$/");
		$this->RegisterRoute("/[vV]ipmember\/(?P<what>[^\/]+)\/(?P<pageindex>[0-9]+)\/(?P<nbperpage>[0-9]+)\/(?P<returnid>[0-9]+)$/");
		$this->RegisterRoute("/[vV]ipmember\/(?P<what>[^\/]+)\/(?P<parent>[^\/]+)\/(?P<pageindex>[0-9]+)\/(?P<nbperpage>[0-9]+)\/(?P<returnid>[0-9]+)$/");

		$this->CreateParameter("action", "default", $this->Lang("phelp_action"));
		$this->CreateParameter("what", "", $this->Lang("phelp_what"));
		$this->CreateParameter("alias", "", $this->Lang("phelp_alias"));
		$this->CreateParameter("showdefault", false, $this->Lang("phelp_showdefault"));
		$this->CreateParameter("parent", "", $this->Lang("phelp_parent"));
		$this->CreateParameter("limit", 0, $this->Lang("phelp_limit"));
		$this->CreateParameter("nbperpage", 0, $this->Lang("phelp_nbperpage"));
		$this->CreateParameter("orderby", 0, $this->Lang("phelp_orderby"));
		$this->CreateParameter("detailpage", "", $this->Lang("phelp_detailpage"));
		$this->CreateParameter("random", 0, $this->Lang("phelp_random"));
		$this->CreateParameter("listtemplate", "", $this->lang("phelp_listtemplate"));
		$this->CreateParameter("finaltemplate", "", $this->lang("phelp_finaltemplate"));
		$this->CreateParameter("forcelist", "0", $this->lang("phelp_forcelist"));
		$this->CreateParameter("inline", 0, $this->lang("phelp_inline"));
		$this->CreateParameter("pageindex", "", $this->lang("phelp_internal"));
	}

    function GetEventDescription($eventname)
    {
		$eventname = str_replace("vipmember", "", $eventname);
		return $this->lang("eventdesc".$eventname);
    }
    
	function InstallPostMessage()
	{
		return $this->Lang("postinstall");
	}
	function UninstallPostMessage()
	{
		return $this->Lang("postuninstall");
	}
	function UninstallPreMessage()
	{
		return $this->Lang("really_uninstall");
	}


	/*---------------------------------------------------------
	   Install()
	   When your module is installed, you may need to do some
	   setup. Typical things that happen here are the creation
	   and prepopulation of database tables, database sequences,
	   permissions, preferences, etc.
	   	   
	   For information on the creation of database tables,
	   check out the ADODB Data Dictionary page at
	   http://phplens.com/lens/adodb/docs-datadict.htm
	   
	   This function can return a string in case of any error,
	   and CMS will not consider the module installed.
	   Successful installs should return FALSE or nothing at all.
	  ---------------------------------------------------------*/
	function Install()
	{
		global $gCms;
		require "method.install.php";
	}

	/*---------------------------------------------------------
	   Uninstall()
	   Sometimes, an exceptionally unenlightened or ignorant
	   admin will wish to uninstall your module. While it
	   would be best to lay into these idiots with a cluestick,
	   we will do the magnanimous thing and remove the module
	   and clean up the database, permissions, and preferences
	   that are specific to it.
	   This is the method where we do this.
	  ---------------------------------------------------------*/
	function Uninstall()
	{
		global $gCms;
		require "method.uninstall.php";
	}


    function SearchResult($returnid, $itemid, $level = "")
    {
		$result = array();
		$wantedparam = false;
		$newparams = array();
		if($level == "items"){
			// we seek an element of the last level, and will display the detail view
			$wantedparam = "alias";
		}else{
			if($newparams["what"] = $this->get_nextlevel($level)){
			// we seek an element of another level, and will display the list view of its children
				$wantedparam = "parent";
			}
		}
		if ($wantedparam){
			$tablename = cms_db_prefix()."module_vipmember_".$level;
			$db =& $this->GetDb();
			$query = "SELECT name, alias FROM $tablename WHERE id = ?";
			$dbresult = $db->Execute( $query, array( $itemid ) );
			if ($dbresult){
				$row = $dbresult->FetchRow();
				$newparams[$wantedparam] = $row["alias"];

				//0 position is the prefix displayed in the list results.
				$result[0] = $this->GetFriendlyName();

				//1 position is the title
				$result[1] = $row["name"];
		
				//2 position is the URL to the title.
				$result[2] = $this->CreateLink($id, "default", $returnid, "", $newparams, "", true, false, "", false, $this->BuildPrettyUrls($newparams, $returnid));
			}
		}

		return $result;
	}

/* ---------------------------------------------
NOT PART OF THE NORMAL MODULE API
----------------------------------------------*/

	function plcreatealias($name){
		// creates the alias for new elements...
		// as a suggestion from AMT, the first part deals with smart quotes
 		$search = array(chr(0xe2) . chr(0x80) . chr(0x98),
						  chr(0xe2) . chr(0x80) . chr(0x99),
						  chr(0xe2) . chr(0x80) . chr(0x9c),
						  chr(0xe2) . chr(0x80) . chr(0x9d),
						  chr(0xe2) . chr(0x80) . chr(0x93),
						  chr(0xe2) . chr(0x80) . chr(0x94));
 		$name = str_replace($search, "", $name);
 		// the second part uses the cms version
 		$alias = munge_string_to_url($name, false);
 		return $alias;
	} 
		
	function checkalias($dbtable, $alias, $itemid=false, $idfield="id", $aliasfield="alias"){
		// checks if this alias already exists in the level
		$query = "SELECT ".$idfield." FROM ".cms_db_prefix().$dbtable." WHERE ".$aliasfield." = ?";
		if($itemid) $query .= " AND ".$idfield."!=".$itemid;
		$db = $this->GetDb();
		$dbresult = $db->Execute($query,array($alias));
		$targetid = 0;
		if($dbresult && $row = $dbresult->FetchRow()) $targetid = $row["id"];
		return ($targetid == 0);
	}

    function getDefaultTemplates(){
    	// returns an array of the templates that are selected as default (just so that we don't delete them)
	   $result = array();
	   $result[] = $this->GetPreference("finaltemplate");
	   $result[] = $this->GetPreference("listtemplate_items");
	   return $result;
    }

	function BuildPrettyUrls($params, $returnid=-1){
		$prettyurl = "vipmember/";
		if(isset($params["alias"])){
			$prettyurl .= "detail/".$params["alias"];
		}elseif(isset($params["parent"])){
			$prettyurl .= $params["what"]."/".$params["parent"];
		}else{
			$prettyurl .= $params["what"];
		}
		if(!isset($params["alias"]) && isset($params["pageindex"]) && isset($params["nbperpage"]))	$prettyurl .= "/".$params["pageindex"]."/".$params["nbperpage"];
		$prettyurl .= "/".$returnid;
		return $prettyurl;
	}


	function DoAction($action, $id, $params, $returnid=-1){
		global $gCms;
		
		switch($action){
			case "link":
				echo $this->CreateLink($id,"default",$returnid,"",$params,"",true);
				break;
			case "changedeftemplates":
				foreach($params as $key=>$value){
				    if($key != "submit")	   $this->setPreference($key, $value);
				}
				$params = array("active_tab"=>"templates", "module_message"=>$this->lang("message_modified"));
				// no break, natural redirect to defaultadmin
			case "defaultadmin":
				require "action.defaultadmin.php";
				break;
			case "movesomething":
				require "action.movesomething.php";
				break;
			case "toggle":
				require "action.toggle.php";
				break;
			case "deletetpl":
				$newparams = array("active_tab"=>"templates");
				$deftemplates = $this->getDefaultTemplates();
			    if(isset($params["tplname"]) && !in_array($params["tplname"], $deftemplates)){
				    if($this->DeleteTemplate($params["tplname"]))	   $newparams["module_message>"] = $this->lang("message_modified");
				}
				$this->Redirect($id, "defaultadmin", $returnid, $newparams);
				break;
			case "editTemplate":
				require "action.editTemplate.php";
				break;
			case "search":
				require "action.search.php";
				break;

			case "browsefiles":
				require "action.browsefiles.php";
				break;
			case "uploadFile":
				require "action.uploadFile.php";
				break;
			case "assignfile":
				require "action.assignfile.php";
				break;

			case "edititems":
				require "action.edititems.php";
				break;

			case "add_optionvalue":
				if(isset($params["tablename"]) && isset($params["optionname"]) && $params["optionname"] != ""){
					$tablename = cms_db_prefix()."module_vipmember_".$params["tablename"]."_options";
					$db =& $this->GetDb();
					$itemid = $db->GenID($tablename."_seq");
					$query = "INSERT INTO ".$tablename." SET id=?, name=?";
					$db->Execute($query,array($itemid,$params["optionname"]));
				}
				$this->Redirect($id, "defaultadmin", $returnid, array("active_tab"=>"fieldoptions") );
				break;
			case "delete_optionvalue":
				if(isset($params["tablename"]) && isset($params["optionid"]) && $params["optionid"] > 0){
					$tablename = cms_db_prefix()."module_vipmember_".$params["tablename"]."_options";
					$db =& $this->GetDb();
					$query = "DELETE FROM ".$tablename." WHERE id=? LIMIT 1";
					$db->Execute($query,array($params["optionid"]));
				}
				$this->Redirect($id, "defaultadmin", $returnid, array("active_tab"=>"fieldoptions") );
				break;
			case "rename_optionvalue":
				if(!isset($params["cancel"]) && isset($params["optionid"]) && isset($params["tablename"]) && isset($params["optionname"]) ){
					if(isset($params["submit"]) && $params["optionname"] != "" ){
						$tablename = cms_db_prefix()."module_vipmember_".$params["tablename"]."_options";
						$db =& $this->GetDb();
						$query = "UPDATE ".$tablename." SET name=? WHERE id=?";
						$db->Execute($query,array($params["optionname"], $params["optionid"]));
						$this->Redirect($id, "defaultadmin", $returnid, array("active_tab"=>"fieldoptions") );
					}else{
						echo "<h2>".$this->Lang("modifyanoption")."</h2>";
						echo $this->CreateFormStart($id, "rename_optionvalue", $returnid);
						echo "<p>".$this->CreateInputText($id,"optionname",$params["optionname"],40,64)."</p>";
						echo "<p>".$this->CreateInputSubmit($id, "submit", lang("submit")).$this->CreateInputSubmit($id, "cancel", lang("cancel"))."</p>";
						echo $this->CreateInputHidden($id, "optionid", $params["optionid"]).$this->CreateInputHidden($id, "tablename", $params["tablename"]);
						echo $this->CreateFormEnd();
					}
				}else{
					$this->Redirect($id, "defaultadmin", $returnid, array("active_tab"=>"fieldoptions") );
				}
				break;
			case "default":
			default:
				require "action.default.php";
				break;
		}


	}

	function DoCheckboxes($id, $name, $choices, $selected=array(), $delimiter="<br/>"){
		$output = "";
		foreach($choices as $key=>$value){
			$output .= $this->CreateInputCheckbox($id, $name."[]", $value, (in_array($value, $selected)?$value:0))." ".$key.$delimiter;
		}
		return $output;
	}

	function parsekeywords($string){
		$inside = (substr($string,0,1) == '"');
		$parts = explode('"',$string);
	
		if(count($parts) < 2){
			if($inside){
				return array(str_replace('"',"",$string));
			}else{
				return explode(" ",$string);
			}
		}
	
		$keywords = array();
		foreach($parts as $part){
			if($part != ""){
				if($inside){
					$keywords[] = $part;
				}else{
					$words = explode(" ",trim($part));
					foreach($words as $word){
						if(trim($word) != "")	$keywords[] = $word;
					}
				}
				$inside = !$inside;
			}
		}
		return $keywords;
	}


	function get_levelarray(){
		// returns an array of the levels (top to bottom)
		return array("items");
	}

	function get_modulehierarchy($level=false){
		$hierarchy = array();
			$hierarchy[1] = "items";
		return $hierarchy;
	}


	function get_moduleGetVars(){
		// unorthodox hack so that different calls of the module speak with each other
		global $_GET;
		$globalmodulevars = array();
		if(isset($_GET["mact"])) $modinfo = explode(",",$_GET["mact"]);
		if(isset($modinfo) && $modinfo[0] == "vipmember"){
			if(isset($_GET[$modinfo[1]."parent"])) $globalmodulevars["parent"]=$_GET[$modinfo[1]."parent"];
			if(isset($_GET[$modinfo[1]."what"])) $globalmodulevars["what"]=$_GET[$modinfo[1]."what"];
			if(isset($_GET[$modinfo[1]."alias"])) $globalmodulevars["alias"]=$_GET[$modinfo[1]."alias"];
			if(isset($_GET[$modinfo[1]."finalalias"])) $globalmodulevars["finalalias"]=$_GET[$modinfo[1]."finalalias"];
			if(isset($_GET[$modinfo[1]."pageindex"])) $globalmodulevars["pageindex"]=$_GET[$modinfo[1]."pageindex"];
		}
		return $globalmodulevars;
	}

	function get_objtree($curid, $curlevel="items", $field="id"){
		// this builds an object tree ($item->parent_object->parent_object...)
		// we first put all the parents in an array
		$parents = array();
		$levels = $this->get_levelarray();
		$i = count($levels);
		$started = false;
		while($curid && $i > 0){
			if($levels[$i -1] == $curlevel)	$started = true;
			if($started){
				$getfunction = "get_level_".$levels[$i -1];
				$item = $this->$getfunction(array($field=>$curid));
				$item = is_array($item)?$item[0]:$item;
				$parents[] = $item;
				$field = "id";
				$curid = isset($item->parent_id)?$item->parent_id:false;
			}
			$i--;
		}
		
		// next, we process the array of parents to build the parent tree
		$parenttree = false;
		$i = count($parents) - 1;
		while($i >= 0){
			if($parenttree){
				$newtree = $parents[$i];
				$newtree->parent_object = $parenttree;
				$parenttree = $newtree;
			}else{
				$parenttree = $parents[$i];
			}
			$i--;
		}
		return $parenttree;
	}

/* Has been replaced by the function below, which uses the object tree
	function get_module_breadcrumbs($params, $id, $returnid){
		$parentid = $params["parent"];
		$db =& $this->GetDb();
		$hierarchy = $this->get_modulehierarchy();
		$distance = $this->get_distancetolevel($params["what"]);
		$currentlevel = count($hierarchy) - $distance - 1;
		$links = array();
		while($currentlevel > 0){
			// we go through each level, from bottom to top, following the parent lead
			$query = "SELECT alias, name".($currentlevel != 1?", parent":"")." FROM ".cms_db_prefix()."module_vipmember_".$hierarchy[$currentlevel]." WHERE ".((count($links)>0)?"id":"alias")."=?";
			$dbresult = $db->Execute($query,array($parentid));
			if ($dbresult && $row = $dbresult->FetchRow()){
				if(isset($row["parent"])) $parentid = $row["parent"];
				$links[$currentlevel] = array($hierarchy[$currentlevel+1],$row["alias"],$row["name"]);
			}else{
				$currentlevel = 0;
			}
			$currentlevel--;
		}
		// once we've got all parents, we do the breadcrumbs
		$output = "";
		$parents = array();
		$delimiter = $this->Lang("breadcrumbs_delimiter");
		if(isset($params["alias"])) unset($params["alias"]);
		foreach($links as $link){
			$parents[$link[0]] = $link[1];
			$params["what"] = $link[0];
			$params["parent"] = $link[1];
			$prettyurl = $this->BuildPrettyUrls($params, $returnid);
			$output = "<span class=\"modbreadcrumbs\">".$this->CreateLink($id, "default", $returnid, $link[2], $params, "", false, false, "", false, $prettyurl)."</span>".($output == ""?"":$delimiter.$output);
		}
		// we return an array :
		// 1 => the array of parents, as we might need it for some special cases
		// 2 => the breadcrumbs as a string
		return array($parents,(($output != "")?$this->Lang("youarehere"):"").$output);
	}
*/
	function get_module_breadcrumbs($params, $id, $returnid, $item, $skip=false){
		$what = isset($params["what"])?$params["what"]:"items";
		if($skip) $what = $this->get_nextlevel($what, false);
		$delimiter = $this->Lang("breadcrumbs_delimiter");
		$newparams = array();
		if(isset($params["forcelist"]) && $params["forcelist"])	$newparams["forcelist"] = 1;
		if(isset($params["inline"]) && $params["inline"])	$newparams["inline"] = 1;

		$crumbs = "<span class=\"lastitem\">".$item->name."</span>";
		while(isset($item->parent_object->name)){
			$item = $item->parent_object;
			$newparams["what"] = $what;
			$what = $this->get_nextlevel($what, false);
			$newparams["parent"] = $item->alias;
			$prettyurl = $this->BuildPrettyUrls($newparams, $returnid);
			$crumbs = "<span>".$this->CreateLink($id, "default", $returnid, $item->name, $newparams, "", false, false, "", false, $prettyurl)."</span>".$delimiter.$crumbs;
		}

		return $crumbs;		
	}


	function countsomething($tablename,$what="id",$where=array()){
		// returns the number of elements in a table
		$db =& $this->GetDb();
		$wherestring = "";
		$wherevalues = array();
		foreach($where as $key=>$value){
			$wherestring .= ($wherestring == ""?" WHERE ":" AND ").$key."=?";
			$wherevalues[] = $value;
		}
		$query = "SELECT COUNT($what) ourcount FROM ".cms_db_prefix()."module_vipmember_$tablename".$wherestring;
		$dbresult = $db->Execute($query,$wherevalues);
		if ($dbresult && $row = $dbresult->FetchRow()){
			return $row["ourcount"];
		}else{
			return 0;
		}
	}

	function get_distancetolevel($parentname,$childname="items"){
		// get the distance between two levels (most likely between a level and the final level)
		$levels = $this->get_levelarray();
		$parentposition = false;
		$childposition = false;
		$counter = 0;
		foreach($levels as $level){
			$counter++;
			if($level == $parentname) $parentposition = $counter;
			if($level == $childname) $childposition = $counter;
		}
		if($childposition && $parentposition){
			return abs($parentposition - $childposition);
		}
	}
	function get_nextlevel($curlevel,$findchild=true){
		// return the name of the level below ($findchild=true) or above ($findchild=false)
		$levels = $this->get_levelarray();
		$i = 0;
		$wantedlevel = false;
		while($i < count($levels)){
			$next = $findchild?$i+1:$i-1;
			if($levels[$i] == $curlevel && isset($levels[$next])) $wantedlevel = $levels[$next];
			$i++;
		}
		return $wantedlevel;
	}

	function get_parents($parentname,$childname,$childid){
		// function with the would-be "sharechildren" option...
		$db =& $this->GetDb();
		$query = "SELECT ".$parentname."_id parentid FROM ".cms_db_prefix()."module_vipmember_".$parentname."_has_".$childname." WHERE ".$childname."_id=?";
		$dbresult = $db->Execute($query,array($childid));
		$parents = array();
		while ($dbresult && $row = $dbresult->FetchRow()){
			$parents[] = $row["parentid"];
		}
		return $parents;
	}

	function get_options($tablename){
		// returns the options from a predefined list
		$db =& $this->GetDb();
		$query = "SELECT * FROM ".cms_db_prefix()."module_vipmember_".$tablename;
		$dbresult = $db->Execute($query);
		$options = array();
		while ($dbresult && $row = $dbresult->FetchRow()){
			$options[$row["name"]] = $row["id"];
		}
		return $options;
	}

	function get_id2namearray($tablename){
		// returns an $id=>$name array of any table, for use with dropdown lists
		$db =& $this->GetDb();
		$query = "SELECT * FROM ".cms_db_prefix()."module_vipmember_".$tablename;
		$dbresult = $db->Execute($query);
		$id2name = array();
		while ($dbresult && $row = $dbresult->FetchRow()){
			$id2name[$row["id"]] = $row["name"];
		}
		return $id2name;
	}

	function get_pageid($alias){
		// returns the page id from an alias
		global $gCms;
		$manager =& $gCms->GetHierarchyManager();
		$node =& $manager->sureGetNodeByAlias($alias);
		if (isset($node)) {
			$content =& $node->GetContent();	
			if (isset($content))	return $content->Id();
		}else{
			$node =& $manager->sureGetNodeById($alias);
			if (isset($node)) return $alias;
		}
	}

	function addadminlinks($item,$prefix,$tablename,$child=false,$levelname,$id,$returnid,$parentdefault=false,$orderbyparent=false,$addfiles=""){
		// add the admin links to the level items
		global $gCms;
		$admintheme = $gCms->variables["admintheme"];

		$moveparams = array($prefix."id"=>$item->id, "prefix"=>$prefix, "currentorder"=>$item->item_order, "tablename"=>$tablename, "active_tab"=>$levelname);
		if($child) $moveparams["child"] = $child;
		if($orderbyparent && isset($item->parent_id)) $moveparams["parent"] = $item->parent_id;

		$item->editlink = $this->CreateLink($id, "edit".$prefix, $returnid, $item->name, array($prefix."id"=>$item->id));
		$item->deletelink = $this->CreateLink($id, "movesomething", $returnid, $admintheme->DisplayImage("icons/system/delete.gif",$this->Lang("delete"),"","","systemicon"), array_merge(array("move"=>"delete", "addfiles"=>$addfiles),$moveparams),$this->Lang("promt_delete".$levelname, $item->name));
		$item->moveuplink = $this->CreateLink($id, "movesomething", $returnid, $admintheme->DisplayImage("icons/system/arrow-u.gif",$this->Lang("up"),"","","systemicon"), array_merge(array("move"=>"up"),$moveparams));
		$item->movedownlink = $this->CreateLink($id, "movesomething", $returnid, $admintheme->DisplayImage("icons/system/arrow-d.gif",$this->Lang("down"),"","","systemicon"), array_merge(array("move"=>"down"),$moveparams));
		$item->movelinks = $item->moveuplink ." ". $item->movedownlink;
		if ($item->active == 1){
			$item->toggleactive = $this->CreateLink($id, "toggle", $returnid, $admintheme->DisplayImage("icons/system/true.gif",$this->Lang("setfalse"),"","","systemicon"), array_merge(array("what"=>"active","newval"=>0),$moveparams));
		}else{
			$item->toggleactive = $this->CreateLink($id, "toggle", $returnid, $admintheme->DisplayImage("icons/system/false.gif",$this->Lang("settrue"),"","","systemicon"), array_merge(array("what"=>"active","newval"=>1),$moveparams));
		}
		$defaultparams = array($prefix."id"=>$item->id, "prefix"=>$prefix, "tablename"=>$tablename, "active_tab"=>$levelname, "what"=>"default");
		if($parentdefault && isset($item->parent_id)){
			$defaultparams["parent"] = $item->parent_id;
			$defaultparams["byparent"] = 1;
		}
		if ($item->isdefault == 1){
			$item->toggledefault = $this->CreateLink($id, "toggle", $returnid, $admintheme->DisplayImage("icons/system/true.gif",$this->lang("setfalse"),"","","systemicon"), $defaultparams);
		}else{
			$item->toggledefault = $this->CreateLink($id, "toggle", $returnid, $admintheme->DisplayImage("icons/system/false.gif",$this->lang("settrue"),"","","systemicon"), array_merge(array("newval"=>1),$defaultparams));
		}
		return $item;
	}

	function get_level_items($where=array(),$admin=false,$id="",$returnid="",$order=false,$limit=0,$customwhere=false,$customvalues=array()){
		if(!$order)	$order = "normal";
		$db =& $this->GetDb();
		$wherestring = "";
		$wherevalues = array();
		

		if($customwhere){
			$wherestring = $customwhere;
			$wherevalues = $customvalues;
		}else{
			foreach($where as $key=>$value){
				$wherestring .= ($wherestring == ""?"":" AND ").$key."=?";
				$wherevalues[] = $value;
			}
		}
		$query = "SELECT * FROM ".cms_db_prefix()."module_vipmember_items A ".($wherestring == ""?"":" WHERE ".$wherestring)." ORDER BY item_order";

		if($limit > 0)	$query .= " LIMIT ".$limit;

		$dbresult = $db->Execute($query,$wherevalues);
		$itemlist = array();
		while ($dbresult && $row = $dbresult->FetchRow()){
			$item = new stdClass();
			foreach($row as $key=>$value){
				$item->$key = $value;
			}
			$item->phone = stripslashes($item->phone);
			$item->sex = stripslashes($item->sex);
			$item->address = stripslashes($item->address);
			$item->birthday = stripslashes($item->birthday);
			$item->name = stripslashes($item->name);
			$item->alias = stripslashes($item->alias);

			if($admin == true) $item = $this->addadminlinks($item,"items","vipmember_items",false,"items",$id,$returnid,1,1,"");

			array_push($itemlist,$item);
		}

		return $itemlist;
	}
	
function plResize($fullpath, $prefix, $newwidth, $newheight=false, $transparency=true) {
	$name = str_replace('//','/',$fullpath);
	if(!file_exists($name))	return false;
	$image_infos = getimagesize($name);
	if($image_infos[0] > $newwidth || ($newheight != false && $image_infos[1] > $newheight)){
		if($prefix == ''){
			$newname = $name;
		}else{
			$tmparr = explode('/',$name);
			$filename = $tmparr[count($tmparr) - 1];
			$newname = str_replace($filename, $prefix.$filename, $name);
		}
		$arr = split("\.",$name);
		$ext = $arr[count($arr)-1];

		if($ext=="jpeg" || $ext=="jpg"){
			$img = @imagecreatefromjpeg($name);
		} elseif($ext=="png"){
			$img = @imagecreatefrompng($name);
		} elseif($ext=="gif") {
			$img = @imagecreatefromgif($name);
		}
		if(!$img)   return false;

		$old_x = imageSX($img);
		$old_y = imageSY($img);

		$ratio = $newheight?min($newwidth / $image_infos[0], $newheight / $image_infos[1]):($newwidth / $image_infos[0]);
		$thumb_h = $image_infos[1] * $ratio;
		$thumb_w = $image_infos[0] * $ratio;

		$new_img = ImageCreateTrueColor($thumb_w, $thumb_h);
	   
		if($transparency) {
			if($ext=="png") {
				imagealphablending($new_img, false);
				$colorTransparent = imagecolorallocatealpha($new_img, 0, 0, 0, 127);
				imagefill($new_img, 0, 0, $colorTransparent);
				imagesavealpha($new_img, true);
			} elseif($ext=="gif") {
				$trnprt_indx = imagecolortransparent($img);
				if ($trnprt_indx >= 0) {
					//its transparent
					$trnprt_color = imagecolorsforindex($img, $trnprt_indx);
					$trnprt_indx = imagecolorallocate($new_img, $trnprt_color['red'], $trnprt_color['green'], $trnprt_color['blue']);
					imagefill($new_img, 0, 0, $trnprt_indx);
					imagecolortransparent($new_img, $trnprt_indx);
				}
			}
		} else {
			Imagefill($new_img, 0, 0, imagecolorallocate($new_img, 255, 255, 255));
		}
		imagecopyresampled($new_img, $img, 0,0,0,0, $thumb_w, $thumb_h, $old_x, $old_y);

		if(file_exists($newname))	@unlink($newname);

		if($ext=="jpeg" || $ext=="jpg"){
			imagejpeg($new_img, $newname);
			$return = true;
		} elseif($ext=="png"){
			imagepng($new_img, $newname);
			$return = true;
		} elseif($ext=="gif") {
			imagegif($new_img, $newname);
			$return = true;
		}
		imagedestroy($new_img);
		imagedestroy($img);
		if($return) $return = $newname;
		return $return;
	}else{
		return $name;
	}
}

}

?>
