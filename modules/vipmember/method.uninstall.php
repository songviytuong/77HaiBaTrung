<?php
if (!isset($gCms)) exit;
$db = $this->GetDb();
$dict = NewDataDictionary($db);	


	$sqlarray = $dict->DropTableSQL(cms_db_prefix()."module_vipmember_items");
	$dict->ExecuteSQLArray($sqlarray);
	$db->DropSequence(cms_db_prefix()."module_vipmember_items_seq");
	

	//$sqlarray = $dict->DropTableSQL(cms_db_prefix()."module_vipmember_templates");
	//$dict->ExecuteSQLArray($sqlarray);
	$this->DeleteTemplate("",$this->GetName());

// permissions
	$this->RemovePermission("vipmember_manage_items");

// events
	$this->RemoveEvent("vipmember_added");
	$this->RemoveEvent("vipmember_modified");
	$this->RemoveEvent("vipmember_deleted");

// put mention into the admin log
	$this->Audit( 0, $this->Lang("friendlyname"), $this->Lang("uninstalled"));

?>
