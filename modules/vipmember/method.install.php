<?php
if (!isset($gCms)) exit;
$db = $this->GetDb();
$dict = NewDataDictionary($db);	
// mysql-specific, but ignored by other database
$taboptarray = array("mysql" => "TYPE=MyISAM");
		

// Creates the items table
$flds = "
	phone C(32),
	sex C(10),
	address C(64),
	birthday C(32),
	id I KEY,
	name C(64),
	alias C(64),
	item_order I,
	active L,
	isdefault L,
    date_modified ".CMS_ADODB_DT."
	";

$sqlarray = $dict->CreateTableSQL(cms_db_prefix()."module_vipmember_items", $flds, $taboptarray);
$dict->ExecuteSQLArray($sqlarray);
$db->CreateSequence(cms_db_prefix()."module_vipmember_items_seq");


// INSERTING EXAMPLE TEMPLATES
	$template = '<h2>{$leveltitle}</h2><p>{$breadcrumbs}</p>
<ul>
{foreach from=$itemlist item="item"}
	<li {if $item->is_selected}class="active"{/if}>{$item->detaillink}</li>
{/foreach}
</ul>';
$this->SetTemplate("list_default",$template,$this->GetName());
    $this->SetPreference("listtemplate_items","list_default");
	$template = '<p>{$breadcrumbs}</p><h3>{$item->name}</h3>';
	$this->SetTemplate("final_default",$template,$this->GetName());
    $this->SetPreference("finaltemplate","final_default");


// permissions
	$this->CreatePermission("vipmember_manage_items", "vipmember: Manage items");
// events
	$this->CreateEvent("vipmember_added");
	$this->CreateEvent("vipmember_modified");
	$this->CreateEvent("vipmember_deleted");
	
// prepare information for an eventual upgrade
	$this->SetPreference("makerversion","1.7");

// put mention into the admin log
	$this->Audit( 0, $this->Lang("friendlyname"), $this->Lang("installed",$this->GetVersion()));

?>
