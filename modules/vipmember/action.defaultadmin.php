<?php
if (!isset($gCms)) exit;
$admintheme = $gCms->variables["admintheme"];
$active_tab = isset($params["active_tab"])?$params["active_tab"]:"items";

echo $this->StartTabHeaders();
	if( $this->CheckPermission("vipmember_manage_items") ) {
		echo $this->SetTabHeader("items", $this->Lang("items_plural"), "items" == $active_tab ? true : false);
	}
	if( $this->CheckPermission("Modify Templates") ) {
		echo $this->SetTabHeader("templates", $this->Lang("templates"), "templates" == $active_tab ? true : false);		
	}
echo $this->EndTabHeaders();



echo $this->StartTabContent();


if( $this->CheckPermission("vipmember_manage_items") ) {
	echo $this->StartTab("items");

			echo "<p>".$this->CreateLink($id, "edititems", $returnid, $admintheme->DisplayImage("icons/system/newobject.gif", "","","","systemicon")." ".$this->Lang("add_items"))."</p>";
			$itemlist = $this->get_level_items(isset($whereclause)?$whereclause:array(),true, $id, $returnid);
			$this->smarty->assign("itemlist", $itemlist);
			$adminshow = array(
				array($this->Lang("items_sex"),"sex"),
				array($this->Lang("name"),"editlink"),
				array($this->Lang("items_birthday"),"birthday"),
				array($this->Lang("items_phone"),"phone"),
				array($this->Lang("items_address"),"address"),
				//array($this->Lang("isdefault"),"toggledefault"),
				array($this->Lang("active"),"toggleactive"),
				//array($this->Lang("reorder"),"movelinks"),
				array($this->Lang("Actions"),"deletelink")		
				);
			$this->smarty->assign("adminshow", $adminshow);
			echo $this->ProcessTemplate("adminpanel.tpl");
			echo "<p>".$this->CreateLink($id, "edititems", $returnid, $admintheme->DisplayImage("icons/system/newobject.gif", "","","","systemicon")." ".$this->Lang("add_items"))."</p>";

	echo $this->EndTab();

}

if( $this->CheckPermission("Modify Templates") ) {
	echo $this->StartTab("templates");

    echo "<h2>".$this->Lang("defaulttemplates")."</h2>";
    echo $this->CreateFormStart($id, "changedeftemplates", $returnid);
    $templatelist = $this->ListTemplates($this->GetName());
    $deftpls = $this->getDefaultTemplates();
    $tploptions = array();
    $itemlist = array();
    foreach($templatelist as $onetpl){
	   $tploptions[$onetpl] = $onetpl;
	   $tpl = new stdClass();
	   $tpl->editlink = $this->CreateLink( $id, "editTemplate", $returnid, $onetpl, array("tplname"=>$onetpl) );
	   $tpl->deletelink = in_array($onetpl, $deftpls)?"":$this->CreateLink( $id, "deletetpl", $returnid, $admintheme->DisplayImage("icons/system/delete.gif", $this->Lang("delete"), "", "", "systemicon"), array("tplname"=>$onetpl) );
	   array_push($itemlist, $tpl);
    }

	   echo "	<div class=\"pageoverflow\">
			 <p class=\"pagetext\">".$this->Lang("deftemplatefor")." \"items\":</p>
			 <p class=\"pageinput\">".$this->CreateInputDropdown($id,"listtemplate_items",$tploptions,-1,$this->GetPreference("listtemplate_items"))."</p>
		</div>
    ";
    echo "	<div class=\"pageoverflow\">
			 <p class=\"pagetext\">".$this->Lang("defdetailtemplate").":</p>
			 <p class=\"pageinput\">".$this->CreateInputDropdown($id,"finaltemplate",$tploptions,-1,$this->GetPreference("finaltemplate"))."</p>
		</div>
    <p>".$this->CreateInputSubmit($id, "submit", lang("submit"))."</p>";
    echo $this->CreateFormEnd();

    echo "<br/><br/><hr/><h2>".$this->Lang("templates")."</h2>";
    $this->smarty->assign("itemlist", $itemlist);
    $adminshow = array(	array($this->Lang("template"), "editlink"), array($this->Lang("Actions"), "deletelink")	);
    $this->smarty->assign("adminshow", $adminshow);
    echo $this->ProcessTemplate("adminpanel.tpl");
    echo "<p>".$this->CreateLink($id, "editTemplate", $returnid, $admintheme->DisplayImage("icons/system/newobject.gif", "","","","systemicon")." ".$this->Lang("addtemplate"))."</p>";

	echo $this->EndTab();
}

echo $this->EndTabContent();

?>