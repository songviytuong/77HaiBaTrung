<?php
if(!isset($gCms)) exit;

// we'll need to know which level is the final level
$finallevel = "items";

if(isset($params["submitsearch"]) && isset($params["what"])){
	$what = $params["what"];
	$where = array();
	foreach($params as $key=>$value){
		if(substr($key,0,6) == "field_" && trim($value) != ""){
			$key = substr($key,6);
			$value = addslashes(html_entity_decode($value));
			if(!isset($params[$key."_compare"]) || $params[$key."_compare"] == 0){
				$keywords = $this->parsekeywords($value);
				foreach($keywords as $value)	$where[] = array($key, $value, false);
			}else{
				str_replace("\"","",$value);
				$where[] = array($key, $value, true);
			}
		}
	}
	$whereclause = "A.active=1";
	$wherevalues = array();
	foreach($where as $clause){
		$whereclause .= " AND A.".$clause[0];
		if($clause[2]){
			$whereclause .= "=?";
			$wherevalues[] = addslashes($clause[1]);
		}else{
			$whereclause .= " LIKE '%".addslashes($clause[1])."%'";
		}
	}
	$getfunction = "get_level_".$what;
	// we do the query
	$itemlist = $this->$getfunction(array(), false, "", "", 0, 20, $whereclause, $wherevalues);
	$newlist = array();
	$newparams = array("what"=>$what);
	$wantedparam = $what==$finallevel?"alias":"parent";
	foreach($itemlist as $item){
		$newparams[$wantedparam] = $item->alias;
		$item->detaillink = $this->CreateLink($id, "default", $returnid, $item->name, $newparams, "", false, false, "", false, $this->BuildPrettyUrls($tmpparams, $returnid));
		$item->detailurl = $this->CreateLink($id, "default", $returnid, "", $newparams, "", true, false, "", false, $this->BuildPrettyUrls($tmpparams, $returnid));
		$item->is_selected = false;
		array_push($newlist, $item);
	}
	$itemlist = $newlist;
	if(count($itemlist) > 0){
		$templatename = $this->GetPreference("listtemplate_".$what);
		$template = $this->GetTemplate($templatename, $this->GetName());
		$this->smarty->assign("itemlist",$itemlist);
		$this->smarty->assign("leveltitle",$this->Lang($what."_plural"));
		$this->smarty->assign("breadcrumbs",false);
		$this->smarty->assign("parentobj",false);
		echo $this->ProcessTemplateFromData($template);
	}else{
		echo "<p>".$this->Lang("error_noitemfound")."</p>";
	}
	echo "<p>".$this->CreateLink($id, "search", $returnid, $this->Lang("searchagain"))."</p>";

}else{
	$levelarray = $this->get_levelarray();
	$what = (isset($params["what"]) && in_array($params["what"],$levelarray))?$params["what"]:$finallevel;
	$comparechoices = array($this->Lang("contains") =>0, $this->Lang("isexactly") =>1);
	
	$fields = array();
	$fields["name"] = array($this->Lang("name"),$this->CreateInputDropdown($id,"name_compare",$comparechoices),$this->CreateInputText($id,"field_name","",30));
	switch($what){
		case "items":
			$fields["phone"] = array($this->Lang("items_phone"),$this->CreateInputDropdown($id,"phone_compare",$comparechoices), $this->CreateInputText($id,"field_phone","",30));
			$fields["sex"] = array($this->Lang("items_sex"),$this->CreateInputDropdown($id,"sex_compare",$comparechoices), $this->CreateInputText($id,"field_sex","",30));
			$fields["address"] = array($this->Lang("items_address"),$this->CreateInputDropdown($id,"address_compare",$comparechoices), $this->CreateInputText($id,"field_address","",30));
			$fields["birthday"] = array($this->Lang("items_birthday"),$this->CreateInputDropdown($id,"birthday_compare",$comparechoices), $this->CreateInputText($id,"field_birthday","",30));
			break;
		
	}
	$this->smarty->assign("searchtitle", $this->Lang("searchtitle"));
	$this->smarty->assign("what", $this->Lang($what));
	$this->smarty->assign("fields", $fields);
	$this->smarty->assign("submit", $this->CreateInputSubmit($id, "submitsearch", $this->Lang("searchbtn")));
	
	echo $this->CreateFormStart($id, "search", $returnid);
	echo $this->CreateInputHidden($id, "what", $what);
	echo $this->ProcessTemplate("search.tpl");
	echo $this->CreateFormEnd();
}
?>