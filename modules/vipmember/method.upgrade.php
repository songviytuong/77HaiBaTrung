<?php
if (!isset($gCms)) exit;

$db =& $this->GetDb();

// This does the upgrade for the CTLModuleMaker part
// Unfortunately, this is only backward compatible to CTLModuleMaker 1.6.3. I couldn't go any further behind because at that time the maker version wasn't saved anywhere

$oldmaker = $this->GetPreference("makerversion", "veryold");

switch($oldmaker){
	// BEGIN SWITCH($oldmaker)

	case "veryold":
		// the module was created with version 1.6.3 or prior.
		$this->CreateEvent("vipmember_added");
		$this->CreateEvent("vipmember_modified");
		$this->CreateEvent("vipmember_deleted");
		break;


	// END SWITCH($oldmaker)
}

$this->SetPreference("makerversion", "1.7");
?>