<?php
if(!isset($gCms)) exit;

// we need to know which level is the final level
$levelarray = $this->get_levelarray();
$finallevel = "items";

// we check what level we're watching... if none is specified, we use the final level
$what = (isset($params["what"]) && in_array($params["what"],$levelarray))?$params["what"]:$finallevel;
// we give it back to the params for pretty urls and page view
if(!isset($params["what"]) || $params["what"] == "")	$params["what"] = $what;

// we retrieve some other parameters
$forcelist = isset($params["forcelist"])?$params["forcelist"]:false;
if($what != $finallevel)	$forcelist = true;		// Always display as list when we're not on the final level
$inline = (isset($params["inline"]) && $params["inline"])?true:false;

// we check if the current page has other instances of the module which are in action. This is the way each instance
// of the module can know what the others are watching
$glob = $this->get_moduleGetVars();
if($what == $finallevel && isset($glob["finalalias"]) && !isset($glob["alias"]) && !isset($params["alias"]) ) $params["alias"] = $glob["finalalias"];

// we build the query. The $where holds elements of the WHERE clause of the query, in the form field_name=>field_value
if(isset($params["alias"]) && $params["alias"] != "") {
	$where = array("alias"=>$params["alias"]);
}elseif(isset($params["showdefault"]) && $params["showdefault"]) {
	$where = array("isdefault"=>1);
}else{
	$where = array();
}
if(!isset($params["parent"]) && isset($glob["parent"]) && isset($glob["what"]) && $glob["what"] == $what) $params["parent"] = $glob["parent"];
if(isset($params["parent"]) && $params["parent"] != "") $where["parent"] = $params["parent"];

$where["active"] = 1;
// we retrieve the function that will do the query
$getfunction = "get_level_".$what;
// we do the query
$itemlist = $this->$getfunction($where, false, "", "", isset($params["orderby"])?$params["orderby"]:0, isset($params["limit"])?$params["limit"]:0);


//  START PROCESSING

if(count($itemlist) == 1 && !$forcelist){
	
	// ################# WE ARE DISPLAYING AN ITEM IN DETAIL VIEW
	
	$item=$itemlist[0];

	// if a template has been specified, we try to retrieve it	
	$template = false;
	if(isset($params["finaltemplate"]) && $params["finaltemplate"] != ""){
		$template = $this->GetTemplate($params["finaltemplate"]);
	}

	// if no template has been specified, we retrieve the default final template
	if(!$template || $template == ""){
		$templatename = $this->GetPreference("finaltemplate");
		$template = $this->GetTemplate($templatename, $this->GetName());
	}
	// we retrieve the parent tree:
	$parenttree = $this->get_objtree( $item->parent_id, $this->get_nextlevel($what, false) );
	$item->parent_object = $parenttree;

	
	// if the item has parents, we assign links to that parent
	if(isset($item->parent_alias)){
		$prettyurl = $this->BuildPrettyUrls(array("parent"=>$item->parent_alias, "what"=>$what), $returnid);
		$item->parentlink = $this->CreateLink($id, "default", $returnid, $item->parent_name, array("parent"=>$item->parent_alias), "", false, $inline, "", false, $prettyurl);
		$item->parenturl = $this->CreateLink($id, "default", $returnid, "", array("parent"=>$item->parent_alias), "", true, $inline, "", false, $prettyurl);
	}
	
	// we retrieve a label for each of the item's field and assign it to smarty
	$labels = new StdClass();
	foreach($item as $key=>$value){
		$labels->$key = $this->Lang($what."_".$key);		
	}
	$this->smarty->assign("labels", $labels);
	
	$this->smarty->assign("item",$item);
	$this->smarty->assign("leveltitle",$this->Lang($what."_plural"));
	
	// we retrieve module breadcrumbs
	if(isset($item->parent))	$params["parent"] = $item->parent_alias;
	$breadcrumbs = $this->get_module_breadcrumbs($params, $id, $returnid, $item);
	$this->smarty->assign("breadcrumbs",$breadcrumbs);
	
	// we process the template
	echo $this->ProcessTemplateFromData($template);
	
}elseif(count($itemlist) > 0){
	
	// ################# WE ARE DISPLAYING A LIST VIEW
	$parentobj = false;
	// if we are watching items from a specific parents, we want to have the informations of this parent available
	// in the template (for example, if we want to display a category page, we might want to show the category description)
	// if it is the case, we retrieve the parent and give it to smarty
	if(isset($params["parent"]) && $params["parent"] != ""){
		$parentobj = $this->get_objtree($params["parent"], $this->get_nextlevel($what,false), "alias");
	}
	$this->smarty->assign("parentobj",$parentobj);
	
	// we retrieve some list parameters
	if($what == $finallevel && isset($glob["alias"])){
		$selectedalias = $glob["alias"];
	}elseif(isset($glob["parent"]) && isset($glob["what"]) && $glob["what"] == $this->get_nextlevel($what)){
		$selectedalias = $glob["parent"];
	}
	if(isset($glob["pageindex"]) && !isset($params["pageindex"])) $params["pageindex"] = $glob["pageindex"];
	$breadcrumbs = ($parentobj)?$this->get_module_breadcrumbs($params, $id, $returnid, $parentobj, true):"";
	//if(!isset($selectedalias)) $selectedalias = isset($breadcrumbs[0][$what])?$breadcrumbs[0][$what]:false;
	$linkreturnid = (isset($params["detailpage"]))?$this->get_pageid($params["detailpage"]):$returnid;

	// we retrieve the template
	if(isset($params["listtemplate"]) && $params["listtemplate"] != "" && $customtpl = $this->GetTemplate($params["listtemplate"], $this->GetName())){
		$template = $customtpl;
	}else{
		$templatename = $this->GetPreference("listtemplate_".$what);
		$template = $this->GetTemplate($templatename, $this->GetName());
	}

	// if RANDOM option is set, we randomly selected only a number of the items retrieved
	if(isset($params["random"]) && $params["random"] > 0){
		$newlist = array();
		$i = 1;
		$selected = array();
		$currand = rand(0,(count($itemlist)-1));
		while($i <= $params["random"]){
			while(in_array($currand, $selected))	$currand = rand(0,(count($itemlist)-1));
			$newlist[] = $itemlist[$currand];
			$selected[] = $currand;
			$i++;
		}
		$itemlist = $newlist;
	}
	
	// check if we must separate into pages
	if(isset($params["nbperpage"]) && $params["nbperpage"] > 0 && $params["nbperpage"] < count($itemlist)){
		// SEPARATING ELEMENTS INTO PAGES
		$newlist = array();
		$pageindex = (isset($params["pageindex"]) && $params["pageindex"] > 0)?$params["pageindex"]:1;
		$begin = ($pageindex - 1) * $params["nbperpage"];
		$i = $begin;
		while($i < ($begin + $params["nbperpage"])){
			if(isset($itemlist[$i])) $newlist[] = $itemlist[$i];
			$i++;
		}
		$nbpages = ceil(count($itemlist) / $params["nbperpage"] );
		$pagemenu = "";
		$newparams = $params;
		$newparams["pageindex"] = $pageindex - 1;
		if($nbpages > 1) $pagemenu .= $this->CreateLink($id, "default", $returnid, "&lt;", $newparams, "", false, true, " class=\"previouslink".($pageindex < 2?" disabled":"")."\"", false, $this->BuildPrettyUrls($newparams, $returnid));
		$i = 1;
		$links = array();
		while($i <= $nbpages && $nbpages > 1){
			$newparams = $params;
			$newparams["pageindex"] = $i;
			if($i == $pageindex){
				$links[] = '<a class="pagenumber current">'.$i."</a>";
			}else{
				$links[] = $this->CreateLink($id, "default", $returnid, $i, $newparams, "", false, true, " class=\"pagenumber\"", false, $this->BuildPrettyUrls($newparams, $returnid));
			}
			$i++;
		}
		$i = 0;
		$tmpflag = true;
		while($i < count($links)){
			if($i < 6 || $nbpages < 8){
				$pagemenu .= ($pagemenu == ""?"":$this->Lang("pagemenudelimiter")).$links[$i];
			}elseif($i == count($links)){
				$pagemenu .= $links[$i];
			}elseif($tmpflag){
				$tmpflag = false;
				$pagemenu .= "<span class=\"pagemenuoverflow\">".$this->Lang("pagemenuoverflow")."</span>";
			}
			$i++;
		}
		$newparams["pageindex"] = $pageindex + 1;
		if($pagemenu != ""){
			$pagemenu .= "&nbsp;&#124;&nbsp;".$this->CreateLink($id, "default", $returnid, "&gt;", $newparams, "", false, true, " class=\"nextlink".($pageindex < $nbpages?"":" disabled")."\"", false, $this->BuildPrettyUrls($newparams, $returnid));
			$pagemenu = "<div class=\"pagination\">".$pagemenu."</div>";
		}
		$this->smarty->assign("pagemenu",$pagemenu);
		$itemlist = $newlist;
	}
	
	// now, $itemlist really contains the items that we will display
	
	// final processing - we create the detaillinks for each item
	$wantedparam = ($what == $finallevel?"alias":"parent");
	$nextlevel = $this->get_nextlevel($what);
	if(!$nextlevel)	$nextlevel = $finallevel;
	$newparams = $params;
	$newparams["what"] = $nextlevel;
	if($nextlevel == $finallevel && isset($newparams["forcelist"]))	$newparams["forcelist"] = false;
	$newlist = array();
	foreach($itemlist as $item){
		$tmpparams = $newparams;
		$tmpparams[$wantedparam] = $item->alias;
		$item->detaillink = $this->CreateLink($id, "default", $linkreturnid, $item->name, $tmpparams, "", false, $inline, "", false, $this->BuildPrettyUrls($tmpparams, $linkreturnid));
		$item->detailurl = $this->CreateLink($id, "default", $linkreturnid, "", $tmpparams, "", true, $inline, "", false, $this->BuildPrettyUrls($tmpparams, $linkreturnid));
		$item->is_selected = ($item->alias == $selectedalias)?true:false;
		array_push($newlist, $item);
	}
	$itemlist = $newlist;
	
	// we give everything to smarty and process the template
	$this->smarty->assign("itemlist",$itemlist);
	$this->smarty->assign("leveltitle",$this->Lang($what."_plural"));
	$this->smarty->assign("breadcrumbs",$breadcrumbs);
	echo $this->ProcessTemplateFromData($template);
	
}else{
	
	// ################# WE AREN'T DISPLAYING ANYTHING AT ALL
	
	echo "<p>".(isset($params["alias"])?$this->Lang("error_notfound"):$this->Lang("error_noitemfound"))."</p>";
}

?>
