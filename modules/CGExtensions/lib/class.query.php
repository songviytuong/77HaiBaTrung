<?php

namespace CGExtensions;

abstract class query implements \ArrayAccess
{
    abstract public function __construct($parms = array());
    abstract public function OffsetGet($key);
    abstract public function OffsetSet($key,$value);
    abstract public function OffsetExists($key);

    public function OffsetUnset($key)
    {
        // do nothing
    }

    abstract public function &execute();
}

?>