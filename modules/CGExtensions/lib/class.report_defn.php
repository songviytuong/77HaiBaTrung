<?php

namespace CGExtensions;

abstract class report_defn
{
    protected $_query;
    private $_pagenum;

    public function __construct(query $query)
    {
        $this->_query = $query;
    }

    public function get_stylesheet() {}

    public function get_page()
    {
        return $this->_pagenum;
    }

    public function goto_page($page_number)
    {
        $this->_pagenum = $page_number;
    }

    abstract public function get_title();
    abstract public function get_description();
    abstract public function get_page_header();
    abstract public function get_page_footer();
    abstract public function get_report_header();
    abstract public function get_report_footer();
    abstract public function get_page_count();
    abstract public function get_page_content();
    abstract public function finished();

} // end of class

?>