<?php

namespace CGExtensions;

abstract class html_report_generator extends report_generator
{
    protected function get_document_header()
    {
        $out = '<html><head>';
        $tmp = $this->get_head_contents();
        if( $tmp ) $out .= $tmp."\n";
        $out .= '<title>'.$this->get_title().'</title>';
        $title = $this->get_title();
        if( $title ) $out .= '<title>'.htmlentities($title,ENT_QUOTES).'</title>';
        $desc = $this->get_description();
        if( $desc ) $out .= sprintf('<meta name="description" content="%s"/>',htmlentities($desc,ENT_QUOTES));
        $out .= '</head><body>';
        return $out;
    }

    protected function get_head_contents() {}
    protected function get_page_header() {}

    protected function get_page_footer()
    {
        $out = parent::get_page_footer();
        $out .= '<div style="page-break-after: always"></div>'."\n";
        return $out;
    }

    protected function get_document_footer()
    {
        // close off the body and html tags
        $out = '<!-- generated on '.strftime('%x %H:%M').' -->';
        $out .= '</body></html>';
        return $out;
    }

} // end of class

?>