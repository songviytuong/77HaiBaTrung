<?php

namespace CGExtensions;

abstract class report_generator
{
    protected $_report_defn; // \CG\report_defn;

    public function __construct(report_defn $rpt)
    {
        $this->_report_defn = $rpt;
    }

    abstract protected function get_document_header();
    abstract protected function get_document_footer();

    protected function get_title()
    {
        return $this->_report_defn->get_title();
    }

    protected function get_description()
    {
        return $this->_report_defn->get_description();
    }

    protected function get_page_header()
    {
        return $this->_report_defn->get_page_header();
    }

    protected function get_page_footer()
    {
        return $this->_report_defn->get_page_footer();
    }

    protected function get_report_header()
    {
        return $this->_report_defn->get_report_header();
    }

    protected function get_report_footer()
    {
        return $this->_report_defn->get_report_footer();
    }

    protected function get_page_count()
    {
        return $this->_report_defn->get_page_count();
    }

    protected function goto_page($page_number)
    {
        return $this->_report_defn->goto_page($page_number);
    }

    protected function get_page_content()
    {
        return $this->_report_defn->get_page_content();
    }

    public function generate()
    {
        $out = null;
        $num_pages = $this->get_page_count();
        if( $num_pages == 0 ) throw new \CmsInvalidDataException('Report would generate 0 pages');

        $out .= $this->get_document_header();
        for( $page = 0; $page < $num_pages; $page++ ) {
            $this->goto_page($page); // triggers a new resultset in the report defn.

            $out .= $this->get_page_header();
            if( $page == 0 ) $out .= $this->get_report_header();

            $out .= $this->get_page_content();

            if( $page == $num_pages - 1 ) $out .= $this->get_report_footer();
            $out .= $this->get_page_footer();
        }
        $out .= $this->get_document_footer();
        return $out;
    }
} // end of class


?>