<?php

namespace CGExtensions;

abstract class resultset
{
    protected $_rs;
    protected $_filter;

    public function __construct(query $query)
    {
        $this->_filter = $query;
    }

    public function &get_query()
    {
        return $this->_filter;
    }

    abstract protected function _query();

    public function RecordCount()
    {
        $this->_query();
        if( $this->_rs ) return $this->_rs->RecordCount();
    }

    public function MoveNext()
    {
        $this->_query();
        if( $this->_rs ) return $this->_rs->MoveNext();
    }

    public function MoveFirst()
    {
        $this->_query();
        if( $this->_rs ) return $this->_rs->MoveFirst();
    }

    public function Rewind()
    {
        return $this->MoveFirst();
    }

    public function MoveLast()
    {
        $this->_query();
        if( $this->_rs ) return $this->_rs->MoveLast();
    }

    public function EOF()
    {
        $this->_query();
        if( $this->_rs ) return $this->_rs->EOF();
        return TRUE;
    }

    public function Close()
    {
        if( $this->_rs ) return $this->_rs->Close();
    }

    public function __get($key)
    {
        if( $key == 'EOF' ) return $this->EOF();
        if( $key == 'fields' && $this->_rs && !$this->_rs->EOF() ) return $this->_rs->fields;
    }

    public function TotalMatches()
    {
        $this->_query();
        return $this->_totalmatching;
    }

    abstract protected function &get_object();
}

?>