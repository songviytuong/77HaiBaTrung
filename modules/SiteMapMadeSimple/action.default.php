<?php
#-------------------------------------------------------------------------
# Module: SiteMapMadeSimple - A module to easily create a sitemap for
#         google maps, and other search engines.
#
# Version: 1.0, calguy1000 <calguy1000@hotmail.com>
#
#-------------------------------------------------------------------------
# CMS - CMS Made Simple is (c) 2005 by Ted Kulp (wishy@cmsmadesimple.org)
# This project's homepage is: http://www.cmsmadesimple.org
# The module's homepage is: http://dev.cmsmadesimple.org/projects/skeleton/
#
#-------------------------------------------------------------------------
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# However, as a special exception to the GPL, this software is distributed
# as an addon module to CMS Made Simple.  You may not use this software
# in any Non GPL version of CMS Made simple, or in any version of CMS
# Made simple that does not indicate clearly and obviously in its admin 
# section that the site was built with CMS Made simple.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
# Or read it online: http://www.gnu.org/licenses/licenses.html#GPL
#
#-------------------------------------------------------------------------
if( !isset($gCms) ) exit;

$xmlfile = $this->GenerateSiteMap($params);

if( !$this->GetPreference('static_sitemap') ) {
  $handlers = ob_list_handlers();
  for ($cnt = 0; $cnt < sizeof($handlers); $cnt++) { ob_end_clean(); }
  header("Content-Type: application/xml");
  echo $xmlfile;
  exit;
}

// Generating a static sitemap
$fn = cms_join_path($config['root_path'],'sitemap.xml');
$f = @fopen($fn,'w');
@fwrite($f,$xmlfile);
@fclose($f);

// EOF
?>
