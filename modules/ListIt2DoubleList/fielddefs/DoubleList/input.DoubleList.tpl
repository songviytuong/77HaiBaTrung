<script src="{$doublelist_assets}/jQuery.dualListBox-1.2.min.js" language="javascript" type="text/javascript"></script>

<div class="pageoverflow">
    <p class="pagetext">{$fielddef->GetName()}{if $fielddef->IsRequired()}*{/if}:</p>

    <p class="pageinput">
        {if $fielddef->GetDesc()}({$fielddef->GetDesc()})<br/>{/if}
        <input type="hidden" name="{$actionid}customfield[{$fielddef->GetId()}]" value=""/>

    <div>
        <table>
            <tr>
                <td>
                    Filter: <input type="text" id="box1Filter"/>
                    <button type="button" id="box1Clear">X</button>
                    <br/>
                    <select id="box1View" multiple="multiple" style="height:500px;width:300px;">
                        {foreach from=$fielddef->GetOptions() key=value item=key}
                            {if $fielddef->GetOptionValue('copy_buttons') != '1'}
                                {if !in_array($value, $fielddef->GetValue("array"))}
                                    <option value="{$value}">{$key}</option>
                                {/if}
                            {else}
                                <option value="{$value}">{$key}</option>
                            {/if}
                        {/foreach}
                    </select><br/>
                    <span id="box1Counter" class="countLabel"></span>
                    <select id="box1Storage">
                    </select>
                </td>
                <td>
                    <button id="to2" type="button">&nbsp;>&nbsp;</button>
                    <button id="allTo2" type="button">&nbsp;>>&nbsp;</button>
                    <button id="allTo1" type="button">&nbsp;<<&nbsp;</button>
                    <button id="to1" type="button">&nbsp;<&nbsp;</button>
                </td>
                <td>
                    Filter: <input type="text" id="box2Filter"/>
                    <button type="button" id="box2Clear">X</button>
                    <br/>
                    <select id="box2View" name="{$actionid}customfield[{$fielddef->GetId()}][]" multiple="multiple"
                            style="height:500px;width:300px;">
                        {foreach from=$fielddef->GetOptions() key=value item=key}
                            {if in_array($value, $fielddef->GetValue("array"))}
                                <option value="{$value}" selected="selected">{$key}</option>{/if}
                        {/foreach}
                    </select><br/>
                    <span id="box2Counter" class="countLabel"></span>
                    <select id="box2Storage">
                    </select>
                </td>
            </tr>
        </table>
    </div>
    </p>

</div>

{literal}
<script language="javascript" type="text/javascript">
    jQuery(document).ready(function () {
        $.configureBoxes({
            {/literal}{if $fielddef->GetOptionValue('copy_buttons') == '1'}
                transferMode: 'copy',
            {else}
                transferMode: 'move',
            {/if}{literal}
        });
        jQuery('[name=m1_apply]').live('click', function () {
            $('#box2View option').each(function () {
                $(this).attr('selected', true);
            });
        });
        $("#m1_moduleform_1").submit(function (event) {
            $('#box2View option').each(function () {
                $(this).attr('selected', true);
            });
        });
    });
    {/literal}
</script>