<?php
//saved at 2020-06-26 18:44:55
define('MOD_ERROR_LOGGER_INFO',           false);
define('MOD_ERROR_LOGGER_WARNING',        true);
define('MOD_ERROR_LOGGER_ERROR',          true);
define('MOD_ERROR_LOGGER_EXCEPTION',      true);
define('MOD_ERROR_LOGGER_ITEMS_PER_PAGE', 150);
?>