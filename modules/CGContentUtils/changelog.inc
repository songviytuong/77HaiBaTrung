<ul>
  <li>v1.0 - June 2009
    <ul>
      <li>Initial Relase</li>
    </ul>
  </li>
  <li>v1.0.2 - July 2010
    <ul>
      <li>Ability to perserve hierarchy when copying parents and children (Ted)</li>
      <li>Added check for prop copying to keep E_NOTICE happy (Ted)></li>
      <li>Minor change to sample tag info.</li>
      <li>Adds the page selector field type.</li>
    </ul>
  </li>
  <li>v1.2 - December 2010
    <ul>
      <li>Adds the export and import code functionality.</li>
    </ul>
  </li>
  <li>v1.2.1 - December 2010
    <ul>
      <li>Minor Fixes.</li>
    </ul>
  </li>
  <li>v1.2.2 - November 2011
    <ul>
      <li>Minor Fixes.</li>
    </ul>
  </li>
  <li>v1.3 - December 2011
    <ul>
      <li>Adds the group= param to the content block.</li>
      <li>Fixes to the advanced copy functionality.</li>
    </ul>
  </li>
  <li>v1.3.2 - June 2012
    <ul>
      <li>Fixes for CMSMS 1.10.x and 1.11</li>
      <li>Moves help out of the lang file.</li>
    </ul>
  </li>
  <li>1.3.4 - January 2013
   <ul>
    <li>Minor fix to advanced copy function.</li>
    <li>Remove help and changelog from lang files.</li>
   </ul>
  </li>
  <li>1.3.4 - August 2013
   <ul>
    <li>Fixes.</li>
   </ul>
  </li>
  <li>1.4 - January 2014
    <ul>
	<li>Adds multiselect field.</li>
	<li>Adds static text field.</li>
	<li>Adds WYSIWYG option to textarea field.</li>
	<li>Bug fixes.</li>
    </ul>
  </li>
</ul>
