<?php
$lang['friendlyname']="Quản lý tài liệu";
$lang['permission']="Usage of the File Manager module";
$lang['permissionadvanced']="Advanced usage of the File Manager module";

$lang['moddescription']="Quản lý tất cả tài liệu được tải trên mục uploads";
$lang['installed']="FileManager version %s installed";
$lang['postinstall']="FileManager module was installed";
$lang['uninstalled']="FileManager module uninstalled";
$lang['really_uninstall']="Are you sure the FileManager module should be uninstalled?";
$lang['upgraded']="FileManager module was upgraded to version %s";
$lang['fileview']="Danh sách tài liệu";
$lang["switchtofileview"]="Switch to file view";
$lang['settings']="Cài đặt";
$lang['savesettings']="Lưu cài đặt";

$lang["postletexists"]="Warning! Please removed the modules/FileManager/postlet directory completely, as this has been obsolete for many, many versions.";

$lang['filename']="Tên tài liệu";
$lang['fileinfo']="Thông tin";
$lang['fileowner']="QL";
$lang['fileperms']="Quyền";
$lang['filesize']="Dung lượng";
$lang['filedate']="Ngày";
$lang['actions']="Hành động";
$lang['delete']="Xóa";
$lang["notwritable"]="Not writable";
//$lang["filetoobig"]="is too big. Upload cancelled.";
$lang["afileistoobig"]="A file is too big. Upload cancelled.";
$lang["unknown"]="Unknown";
$lang["fileoutsideuploads"]="You are not allowed to modify files outside the uploads-dir! (That requires the Advanced File Management permission)";

$lang["writable"]="Writable";
$lang["writeprotected"]="Write protected";

$lang['bytes']="bytes";
$lang['kb']="kb";
$lang['mb']="mb";

$lang["files"]="Files:";

$lang['bytesin']="bytes trong";
$lang['in']="trong";
$lang['files']="files";
$lang['file']="file";
$lang['fileno']="Tài liệu số.";
$lang['subdirs']="thư mục";
$lang['subdir']="subdirectory";
$lang['and']="và";

$lang['confirmsingledelete']="Bạn đã chắc chắn chưa?";
$lang['confirmsingledirdelete']="Bạn chắc chắn muốn xóa thư mục chưa?";
$lang["dirtreedeletesuccess"]="Tài liệu trong thư mục đã xóa thành công.";
$lang["dirtreedeletefail"]="An error occured when deleting this dir. Some of the contents may have been deleted, however.";
$lang['singlefiledeletesuccess']="Tài liệu đã được xóa";
$lang['filedeletesuccess']="đã xóa thành công";
$lang['singlefiledeletefail']="An error occurred when trying to delete the file";
$lang['filedeletefail']=" was not deleted due to an error";
$lang['singledirdeletesuccess']="The directory  was successfully deleted";
$lang['singledirdeletefail']="An error occurred when trying to delete the directory (is it empty?)";
$lang['deleteselected']="Xóa những tài liệu được chọn";
$lang['confirmselected']="Bạn đã chắc chắn chưa?";
$lang["confirmdeleteselected"]="Bạn muốn xóa những tài liệu được chọn này?";
$lang["deleteselectedcancelled"]="Deletion of selected files cancelled";
$lang["recursetext"]="Recurse into subdirs";
$lang["nofilesselected"]="Không có tài liệu nào được chọn";
$lang['moveselected']="Di chuyển những tài liệu đã chọn tới";
$lang['copyselected']="Sao chép tài liệu đã chọn tới";
$lang["selecttargetdir"]="Chọn đường dẫn đến để di chuyển/sao chép";

$lang["movedto"]="di chuyển tới";
$lang["copiedto"]="copy tới";
$lang["couldnotmove"]="không thể di chuyển";
$lang["couldnotcopy"]="không thể copy";

$lang["filesdeletedsuccessfully"]="%s file(s) was successfully deleted";
$lang["filesdeletedfiled"]="%s file(s) failed being deleted";

$lang["filesmovedsuccessfully"]="%s file(s) was successfully moved";
$lang["filesmovedfailed"]="%s file(s) failed being moved";
$lang["filescopiedsuccessfully"]="%s file(s) was successfully copied";
$lang["filescopiedfailed"]="%s file(s) failed being copied";

$lang["internalerror"]="Internal error (meaning something didn't make sense at all, please report what you did)";

$lang["rename"]="Đổi tên";
$lang["newname"]="Tên mới:";
$lang["renamesuccess"]="Thay đổi tên thành công";
$lang["renamefailure"]="Không thay đổi tên được";
$lang["renamecancelled"]="Đã hủy";

$lang["owner"]="Owner";
$lang["group"]="Group";
$lang["others"]="Others";
$lang["newpermissions"]="New permissions";
$lang["setpermissions"]="Set permissions";

$lang["permissionstyle"]="Kiểu tài liệu";
$lang["rwxstyle"]="kiểu rwx";
$lang["755style"]="kiểu 755";
$lang["quickmode"]="Quick chmod (like 777)";
$lang["chmod"]="Thay đổi quyền";
$lang["chmodselected"]="Change permissions on selected files";
$lang["newpermissions"]="New permissions:";
$lang["chmodsuccess"]="File successfully got new permissions";
$lang["chmodfailure"]="Changing permissions of file failed";
$lang["chmodcancelled"]="Changing of permissions cancelled";

$lang["newuploadfailed"]="%s file(s) failed to upload (or possibly unpack) successfully";
$lang["newuploadsuccess"]="%s file(s) was successfully uploaded (and unpacked if chosen)";

$lang["dirchmodfailmulti"]="Changing permissions on the directory failed, some of it's content may have gotten new permissions, though.";
$lang["dirchmodsuccessmulti"]="Changing permissions on the directory and it's content was successful";
$lang["dirchmodsuccess"]="The directory successfully got new permissions";
$lang["dirchmodfailure"]="Changing permissions of the directory failed";

$lang["dirnotemptyconfirm"]="This dir is not empty. Do you really want to delete it and all content, including subdirs?";
$lang["dirtreedeletecancelled"]="Deletion of dir cancelled";
$lang["imsure"]="Tôi chắc chắn";
$lang["cancel"]="Hủy";
$lang["ok"]="Thực hiện";
$lang['uploadnewfile']="Upload new file(s):";
$lang['unpackafterupload']="Giải nén sau khi tải (chỉ .tgz hoặc .zip)?";
$lang['uploadsuccess']="đã tải thành công";
$lang['uploadfail']="failed to upload successfully";
$lang['unpacksuccess']="đã giải nén thành công";
$lang['unpackfail']=" failed with this error: ";
$lang["packfileopenfail"]="Could not open the packed file for unpacking (non-supported format?)";
$lang["packfilewritefail"]="Could not open the file %s for writing";
//$lang["containsillegalchars"]="Filename contains one or more illegal characters (',\",+,*,\\,/,&,$)";
$lang["afilecontainsillegalchars"]="A filename contains one or more illegal characters (',\",+,*,\\,/,&,$). Upload cancelled.";

$lang["currentpath"]="Đường dẫn hiện thời:";
$lang['newdirname']="Tạo thư mục:";
$lang['newdirsuccess']="Thư mục được tạo thành công";
$lang['newdirfail']="Có lỗi xảy ra khi tạo thư mục mới";
$lang['direxists']="Thư mục đã tồn tại";
$lang['invalidnewdir']="Tên không chứa các ký tự đặc biệt như: /, \\ and '..' ";
$lang["filedateformat"]="m/d/Y H:m:s";
$lang["iconsize"]="Kích thước biểu tượng";
$lang["smallicons"]="Biểu tượng nhỏ";
$lang["largeicons"]="Biểu tượng lớn";
$lang["thousanddelimiter"]="Đơn vị tài liệu";
$lang["none"]="None";
$lang["space"]="space";
$lang["showthumbnails"]="Hiển thị ảnh";
$lang["showthumbnailshelp"]="Cho phép nhìn ảnh nhỏ";

$lang["uploadfilesto"]="Tải tài liệu tới:";
$lang["uploadview"]="Tải tài liệu";
$lang["uploadboxes"]="Số tài liệu cùng tải một lúc";
$lang["nothinguploaded"]="Nothing uploaded";
//$lang["newunsupportedarchive"]="Unsupported archive format";
$lang["newunsupportedarchive"]="%s is an unsupported archive format";
$lang["uploadmethod"]="Upload method";
$lang["uploaderstandard"]="Standard html input-method (allows unpacking)";
$lang["enableadvanced"]="Bật chức năng mở rộng?";
$lang["advancedhelp"]="Cho phép xem tất cả tài liệu của thư mục root";
$lang["showhiddenfileshelp"]="Chỉ cho phép khi chức năng mở rộng được Bật";
$lang["showhiddenfiles"]="Hiển thị tài liệu ẩn";
$lang["settingsconfirmsingledelete"]="Confirm deletion of a single file?";
$lang["settingssaved"]="Lưu thành công cài đặt";

$lang['help']=<<<EOF
		<h3>What does this do?</h3>
		<p>This module provides file management functions</p>
		<h3>How do I use it?</h3>
		<p>Select it from the content-menu from in the admin. </p>
EOF;

?>