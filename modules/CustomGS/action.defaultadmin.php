<?php
#-------------------------------------------------------------------------
# Module: Custom Global Settings
# Author: Rolf Tjassens, Jos
#-------------------------------------------------------------------------
# CMS - CMS Made Simple is (c) 2011 by Ted Kulp (wishy@cmsmadesimple.org)
# This project's homepage is: http://www.cmsmadesimple.org
# The module's homepage is: http://dev.cmsmadesimple.org/projects/customgs
#-------------------------------------------------------------------------
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
# Or read it online: http://www.gnu.org/licenses/licenses.html#GPL
#-------------------------------------------------------------------------

if (!isset($gCms)) exit;

/**
 * Create Admin tabs
 */ 
echo $this->StartTabHeaders();
$active_tab = empty($params['active_tab']) ? '' : $params['active_tab'];

if ( $this->CheckPermission('Custom Global Settings - Manage' || 'Custom Global Settings - Use') )
{
	echo $this->SetTabHeader("general",$this->Lang("title_general"));
}

if ( $this->CheckPermission('Custom Global Settings - Manage') )
{
	echo $this->SetTabHeader("fielddefs",$this->Lang("title_fielddefs"), ($active_tab == 'fielddefs')?true:false);
	echo $this->SetTabHeader("options",lang("options"), ($active_tab == 'options')?true:false);
}

echo $this->EndTabHeaders();


echo $this->StartTabContent();

if ( $this->CheckPermission('Custom Global Settings - Manage' || 'Custom Global Settings - Use') )
{
	echo $this->StartTab("general");
		include(dirname(__FILE__).'/function.admin_general.php');
	echo $this->EndTab();
}

if ( $this->CheckPermission('Custom Global Settings - Manage') )
{
	echo $this->StartTab("fielddefs");
		include(dirname(__FILE__).'/function.admin_fielddefs.php');
	echo $this->EndTab();

	echo $this->StartTab("options");
		include(dirname(__FILE__).'/function.admin_options.php');
	echo $this->EndTab();
}

echo $this->EndTabContent();
?>