<?php
$lang['moddescription'] = 'Ein Modul, um die Globale Einstellungen mit einigen benutzerdefinierten Parameter zu erweitern. Es bietet Ihnen eine unbegrenzte Zahl von Feldern, die als Smarty-Variable in Templates oder Seiten verwendet werden k&ouml;nnen.';
$lang['postinstall'] = 'Das CustomGS-Modul wurde erfolgreich installiert!';
$lang['uninstall_confirm'] = 'Wollen Sie das CustomGS-Modul wirklich deinstallieren?';
$lang['postuninstall'] = 'Das CustomGS-Modul wurde erfolgreich deinstalliert!';
$lang['now'] = 'jetzt';
$lang['title_general'] = 'Allgemein';
$lang['choosetime'] = 'Zeit w&auml;hlen';
$lang['time'] = 'Zeit';
$lang['title_fielddefs'] = 'Feld-Definitionen';
$lang['smartyvar'] = 'Smarty-Variable';
$lang['textfield'] = 'Textfeld';
$lang['pulldown'] = 'Aufklappliste';
$lang['checkbox'] = 'Kontrollk&auml;stchen';
$lang['radiobuttons'] = 'Radioknopf-Gruppe';
$lang['datepicker'] = 'Datumsauswahl';
$lang['datetimepicker'] = 'Datums-Zeitauswahl';
$lang['timepicker'] = 'Zeitauswahl';
$lang['colorpicker'] = 'Farbauswahl';
$lang['textarea'] = 'Textbereich';
$lang['wysiwyg'] = 'Wysiwyg';
$lang['fieldsetstart'] = 'Feldergruppe Anfang';
$lang['fieldsetend'] = 'Feldergruppe Ende';
$lang['button'] = 'Taste';
$lang['maxlength'] = 'Maximale L&auml;nge';
$lang['properties'] = 'Eigenschaften';
$lang['properties_help1'] = 'Geben Sie die Auswahl-Werte ein, jeweils in einer neuen Zeile. Unterst&uuml;tzt auch Value|OptionName Paare und/oder Smarty-Tags';
$lang['parsesmarty'] = 'Daten durch Smarty verarbeiten';
$lang['clearstylesheetcache'] = 'Zwischengespeicherten Stylesheets l&ouml;schen';
$lang['clearstylesheetcache_help'] = 'Automatisch zwischengespeicherten Stylesheets l&ouml;schen nach &Auml;nderung dieser Einstellung. VORSICHT BEI DER VERWENDUNG!';
$lang['editors'] = 'Bearbeiter';
$lang['fielddefadded'] = 'Die Feld-Definition wurde hinzugef&uuml;gt';
$lang['fielddefsupdated'] = 'Die Liste der Feld-Definitionen wurde aktualisiert';
$lang['title_custom_modulename'] = 'Benutzerdefinierte Modulname';
$lang['help_custom_modulename'] = 'Sie k&ouml;nnen den Modulnamen hier &auml;ndern. Es wird als Titel der Modul Admin-Seiten und als Men&uuml; Text verwendet.';
$lang['title_admin_section'] = 'Admin-Bereich';
$lang['help_admin_section'] = 'W&auml;hlen Sie den Admin-Bereich (von Top-Level Admin Menu), wo dieses Modul zu geh&ouml;rt. <strong>Hinweis:</strong> Benutzer mit Editor-Berechtigungen haben keinen Zugriff auf den Admin-Bereich!';
$lang['settingssaved'] = 'Die Einstellungen wurden erfolgreich gespeichert!';
$lang['event_info_OnSettingChange'] = 'Ausf&uuml;hren, nachdem die &Auml;nderungen des Einstellungen gespeichert wurden.';
$lang['event_help_OnSettingChange'] = '<p>Ausf&uuml;hren, nachdem die &Auml;nderungen der Einstellungen gespeichert wurden.</p>
<h4>Parameter</h4>
<ul>
<li>fieldid</li>
<li>name</li>
<li>alias</li>
<li>value</li>
<li>clearcache</li>
</ul>';
$lang['utma'] = '156861353.1238046435.1386149023.1386149023.1386149023.1';
$lang['utmc'] = '156861353';
$lang['utmz'] = '156861353.1386149023.1.1.utmccn=(referral)|utmcsr=forum.cmsmadesimple.org|utmcct=/index.php|utmcmd=referral';
$lang['utmb'] = '156861353';
?>