<?php
$lang['moddescription'] = 'Een module die de Algemene Instellingen van de website uitbreidt met extra instelbare parameters. U kunt een onbeperkt aantal velden defini&euml;ren die gebruikt kunnen worden als Smarty variabelen in sjablonen en op pagina&#039;s.';
$lang['postinstall'] = 'Module succesvol ge&iuml;nstalleerd!';
$lang['uninstall_confirm'] = 'Weet u zeker dat u deze module wilt de&iuml;nstalleren?';
$lang['postuninstall'] = 'De module is gede&iuml;nstalleerd!';
$lang['now'] = 'nu';
$lang['title_general'] = 'Algemeen';
$lang['choosetime'] = 'Kies tijd';
$lang['time'] = 'tijd';
$lang['title_fielddefs'] = 'Veld Definities';
$lang['smartyvar'] = 'Smarty variabele';
$lang['textfield'] = 'Tekstveld';
$lang['pulldown'] = 'Keuzemenu';
$lang['checkbox'] = 'Vinkvakje';
$lang['radiobuttons'] = 'Radiobutton groep';
$lang['datepicker'] = 'Datumkiezer';
$lang['datetimepicker'] = 'DatumTijdkiezer';
$lang['timepicker'] = 'Tijdkiezer';
$lang['colorpicker'] = 'Kleurkiezer';
$lang['textarea'] = 'Tekstblok';
$lang['wysiwyg'] = 'Wysiwyg';
$lang['fieldsetstart'] = 'Veldengroep start';
$lang['fieldsetend'] = 'Veldengroep einde';
$lang['button'] = 'Drukknop';
$lang['maxlength'] = 'Maximum lengte';
$lang['properties'] = 'Eigenschappen';
$lang['properties_help1'] = 'Vul de keuzewaarden in, elke op een nieuwe regel. Ondersteunt ook Waarde|KeuzeNaam paren en/of Smarty tags';
$lang['parsesmarty'] = 'Data via Smarty parsen';
$lang['clearstylesheetcache'] = 'Verwijder stylesheet buffer';
$lang['clearstylesheetcache_help'] = 'Automatisch de stylesheet buffer legen na het wijzigen van de instelling. GEBRUIK BEHOEDZAAM!';
$lang['editors'] = 'Wijzigingsrechten';
$lang['fielddefadded'] = 'Veld Definitie is toegevoegd';
$lang['fielddefsupdated'] = 'Veld Definitie lijst is bijgewerkt';
$lang['title_custom_modulename'] = 'Eigen Modulenaam';
$lang['help_custom_modulename'] = 'U kunt hier de Modulenaam naar eigen wens aanpassen. Het zal gebruikt worden als titel op de module beheerpagina en als menu tekst.';
$lang['title_admin_section'] = 'Admin Sectie';
$lang['help_admin_section'] = 'Kies de Admin Sectie (van het Admin topmenu) waar deze module moet worden getoond. <b>Opmerking</b>: Admin gebruikers met Editor rechten hebben geen toegang tot de Admin sectie!';
$lang['settingssaved'] = 'Instellingen opgeslagen!';
$lang['event_info_OnSettingChange'] = 'Een tag die wordt aangeroepen nadat een instelling is gewijzigd.';
$lang['event_help_OnSettingChange'] = '<p>Een tag die wordt aangeroepen nadat een instelling is gewijzigd.</p>
<h4>Parameters</h4>
<ul>
<li>fieldid</li>
<li>name</li>
<li>alias</li>
<li>value</li>
<li>clearcache</li>
</ul>';
$lang['utma'] = '156861353.1238046435.1386149023.1386149023.1386149023.1';
$lang['utmc'] = '156861353';
$lang['utmz'] = '156861353.1386149023.1.1.utmccn=(referral)|utmcsr=forum.cmsmadesimple.org|utmcct=/index.php|utmcmd=referral';
$lang['utmb'] = '156861353';
?>