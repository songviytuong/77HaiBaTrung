{$startform}
<fieldset>
	<legend>{$title_textfield} 1</legend>
	<div class="pageoverflow">
		<p class="pagetext">{$label_label}:</p>
		<p class="pageinput">{$input_title_textfield_1}</p>
	</div>
	<div class="pageoverflow">
		<p class="pagetext">{$label_help_text}:</p>
		<p class="pageinput">{$input_help_textfield_1}</p>
		<br />
	</div>
</fieldset>
<br />
<fieldset>
	<legend>{$title_textfield} 2</legend>
	<div class="pageoverflow">
		<p class="pagetext">{$label_label}:</p>
		<p class="pageinput">{$input_title_textfield_2}</p>
	</div>
	<div class="pageoverflow">
		<p class="pagetext">{$label_help_text}:</p>
		<p class="pageinput">{$input_help_textfield_2}</p>
		<br />
	</div>
</fieldset>
<br />
<fieldset>
	<legend>{$title_textfield} 3</legend>
	<div class="pageoverflow">
		<p class="pagetext">{$label_label}:</p>
		<p class="pageinput">{$input_title_textfield_3}</p>
	</div>
	<div class="pageoverflow">
		<p class="pagetext">{$label_help_text}:</p>
		<p class="pageinput">{$input_help_textfield_3}</p>
		<br />
	</div>
</fieldset>
<br />
<fieldset>
	<legend>{$title_textfield} 4</legend>
	<div class="pageoverflow">
		<p class="pagetext">{$label_label}:</p>
		<p class="pageinput">{$input_title_textfield_4}</p>
	</div>
	<div class="pageoverflow">
		<p class="pagetext">{$label_help_text}:</p>
		<p class="pageinput">{$input_help_textfield_4}</p>
		<br />
	</div>
</fieldset>
<br />
<fieldset>
	<legend>{$title_textfield} 5</legend>
	<div class="pageoverflow">
		<p class="pagetext">{$label_label}:</p>
		<p class="pageinput">{$input_title_textfield_5}</p>
	</div>
	<div class="pageoverflow">
		<p class="pagetext">{$label_help_text}:</p>
		<p class="pageinput">{$input_help_textfield_5}</p>
		<br />
	</div>
</fieldset>

<hr />
	<div class="pageoverflow">
		<p class="pagetext">&nbsp;</p>
		<p class="pageinput">{$submit}{$cancel}</p>
	</div>
{$endform}