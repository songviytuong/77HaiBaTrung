{$startform}
<fieldset>
	<legend>{$title_checkbox} 1</legend>
	<div class="pageoverflow">
		<p class="pagetext">{$label_label}:</p>
		<p class="pageinput">{$input_title_checkbox_1}</p>
	</div>
	<div class="pageoverflow">
		<p class="pagetext">{$label_help_text}:</p>
		<p class="pageinput">{$input_help_checkbox_1}</p>
		<br />
	</div>
</fieldset>
<br />
<fieldset>
	<legend>{$title_checkbox} 2</legend>
	<div class="pageoverflow">
		<p class="pagetext">{$label_label}:</p>
		<p class="pageinput">{$input_title_checkbox_2}</p>
	</div>
	<div class="pageoverflow">
		<p class="pagetext">{$label_help_text}:</p>
		<p class="pageinput">{$input_help_checkbox_2}</p>
		<br />
	</div>
</fieldset>
<br />
<fieldset>
	<legend>{$title_checkbox} 3</legend>
	<div class="pageoverflow">
		<p class="pagetext">{$label_label}:</p>
		<p class="pageinput">{$input_title_checkbox_3}</p>
	</div>
	<div class="pageoverflow">
		<p class="pagetext">{$label_help_text}:</p>
		<p class="pageinput">{$input_help_checkbox_3}</p>
		<br />
	</div>
</fieldset>
<br />
<fieldset>
	<legend>{$title_checkbox} 4</legend>
	<div class="pageoverflow">
		<p class="pagetext">{$label_label}:</p>
		<p class="pageinput">{$input_title_checkbox_4}</p>
	</div>
	<div class="pageoverflow">
		<p class="pagetext">{$label_help_text}:</p>
		<p class="pageinput">{$input_help_checkbox_4}</p>
		<br />
	</div>
</fieldset>
<br />
<fieldset>
	<legend>{$title_checkbox} 5</legend>
	<div class="pageoverflow">
		<p class="pagetext">{$label_label}:</p>
		<p class="pageinput">{$input_title_checkbox_5}</p>
	</div>
	<div class="pageoverflow">
		<p class="pagetext">{$label_help_text}:</p>
		<p class="pageinput">{$input_help_checkbox_5}</p>
		<br />
	</div>
</fieldset>
<br />
<fieldset>
	<legend>{$title_checkbox} 6</legend>
	<div class="pageoverflow">
		<p class="pagetext">{$label_label}:</p>
		<p class="pageinput">{$input_title_checkbox_6}</p>
	</div>
	<div class="pageoverflow">
		<p class="pagetext">{$label_help_text}:</p>
		<p class="pageinput">{$input_help_checkbox_6}</p>
		<br />
	</div>
</fieldset>
<br />
<fieldset>
	<legend>{$title_checkbox} 7</legend>
	<div class="pageoverflow">
		<p class="pagetext">{$label_label}:</p>
		<p class="pageinput">{$input_title_checkbox_7}</p>
	</div>
	<div class="pageoverflow">
		<p class="pagetext">{$label_help_text}:</p>
		<p class="pageinput">{$input_help_checkbox_7}</p>
		<br />
	</div>
</fieldset>

<hr />
	<div class="pageoverflow">
		<p class="pagetext">&nbsp;</p>
		<p class="pageinput">{$submit}{$cancel}</p>
	</div>
{$endform}