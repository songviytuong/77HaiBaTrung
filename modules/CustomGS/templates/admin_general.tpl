{literal}
<script type="text/javascript" src="../modules/CustomGS/lib/jquery/jquery.cookie.js"></script>
<script type="text/javascript" src="../modules/CustomGS/lib/jquery/jquery.collapsible.js"></script>
<script type="text/javascript" src="../modules/CustomGS/lib/jquery/jquery-ui-timepicker-addon.js"></script>
<script type="text/javascript" src="../modules/CustomGS/lib/jquery/colorpicker.min.js"></script>
<script type="text/javascript">
		$(function() {
			$('.cgs_collapsible').collapsible({
				cookieName: 'collapsible'
			});
{/literal}{$DP_locale}{literal}
			$('.datepicker input').datepicker({
				dateFormat: 'yy-mm-dd',
				showOtherMonths: true,
				selectOtherMonths: true
			});
			$('.datetimepicker input').datetimepicker({
				dateFormat: 'yy-mm-dd',
				showOtherMonths: true,
				selectOtherMonths: true
			});
			$('.timepicker input').timepicker({});
			$('.inputcolorpicker input').ColorPicker({
				onSubmit: function(hsb, hex, rgb, el, parent) {
					$(el).val(hex);
					$(el).ColorPickerHide();
				},
				onBeforeShow: function () {
					$(this).ColorPickerSetColor(this.value);
				}
			})
			.on('keyup', function(){
       $(this).ColorPickerSetColor(this.value);
			});
		});
</script>
{/literal}

{$startform}

{foreach from=$items item=entry}
{if $entry->type == 'fieldsetstart' || $entry->type == 'fieldsetend'}
	{$entry->fieldhtml}
  	{if !empty($entry->help)}<p class="pageinput">{$entry->help|escape}</p>{/if}
{else}
	<div class="pageoverflow">
   		<p class="pagetext">{if $entry->type != 'button'}{$entry->name|escape}:</p>
   		<p class="pageinput{$entry->fieldclass}">{/if}{$entry->fieldhtml}</p>
   		<p class="pageinput">{$entry->help|escape}</p>
	</div>
{/if}
{/foreach}

	<hr />
	<div class="pageoverflow">
		<p class="pagetext">&nbsp;</p>
		<p class="pageinput">{$submit}{$cancel}</p>
	</div>

{$endform}
