<?php
#-------------------------------------------------------------------------
# Module: Custom Global Settings
# Author: Rolf Tjassens, Jos
#-------------------------------------------------------------------------
# CMS - CMS Made Simple is (c) 2011 by Ted Kulp (wishy@cmsmadesimple.org)
# This project's homepage is: http://www.cmsmadesimple.org
# The module's homepage is: http://dev.cmsmadesimple.org/projects/customgs
#-------------------------------------------------------------------------
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
# Or read it online: http://www.gnu.org/licenses/licenses.html#GPL
#-------------------------------------------------------------------------

if (!isset($gCms)) exit;

if( !$this->CheckPermission('Custom Global Settings - Use') )
{
	echo $this->ShowErrors(lang('needpermissionto', 'Custom Global Settings - Use'));
	return;
}

$userid = get_userid();
$userops = cmsms()->GetUserOperations();
$adminperm = $userops->UserInGroup($userid, 1); // is admin

$rowarray = array();

$db = $this->GetDB();
$query = "SELECT * FROM " . cms_db_prefix() . "module_customgs ORDER BY sortorder ASC";
$result = $db->Execute($query);
if ( $result && $result->RecordCount() > 0 )
{
	while ( $row=$result->FetchRow() )
	{
		// check editor permissions
		$editperm = FALSE;
		if ( !$adminperm && !empty($row['editors']) )
		{
			$editors = explode(';', $row['editors']);
			foreach ($editors as $editor)
			{
				$editperm = $editperm || $userops->UserInGroup($userid, $editor);
			}
		}
		else
		{
			$editperm = TRUE;
		}
		if ( $editperm )
		{
			$onerow = new stdClass();

			$onerow->fieldid = $row['fieldid'];
			$onerow->name = $row['name'];
			$onerow->type = $row['type'];
			$onerow->help = $row['help'];
			$onerow->value = $row['value'];
			$onerow->fieldclass = '';

			$fieldname = 'field[' . $row['fieldid'] . ']';
			switch ( $row['type'] )
			{
				case 'textfield':
					$size = min(50, $row['properties']);
					$onerow->fieldhtml = $this->CreateInputText($id, $fieldname, $row['value'], $size, $row['properties'] );
					break;

				case 'pulldown':
				case 'radiobuttons':
					// lets parse this by smarty first
					$row['properties'] = $smarty->fetch('eval:'.$row['properties']);
					$row['properties'] = preg_replace('#<!--(.+)-->#is', '', $row['properties']); // filter out html comments
					$row['properties'] = str_replace("\r", "\n", $row['properties']);
					$row['properties'] = str_replace("\n\n", "\n", $row['properties']);
					$row['properties'] = trim($row['properties']);
					$properties = explode("\n", $row['properties']);
					$items = array();
					foreach ($properties as $property)
					{
						list($key, $value) = explode("|", $property . "|");
						$value = trim($value) == "" ? $key : $value;
						$items[$value] = $key;
					}
					if ( $row['type'] == 'pulldown' )
					{
						$onerow->fieldhtml = $this->CreateInputDropdown($id, $fieldname, $items, -1, $row['value']);
					}
					else
					{
						$onerow->fieldhtml = $this->CreateInputRadioGroup($id, $fieldname, $items, $row['value']);
					}
					break;

				case 'checkbox':
					$onerow->fieldhtml = $this->CreateInputCheckbox($id, $fieldname, '1', $row['value'], '');
					break;

				case 'datepicker':
				case 'timepicker':
					$onerow->fieldhtml = $this->CreateInputText($id, $fieldname, $row['value'], 10, 10);
					$onerow->fieldclass = ' ' . $row['type'];
					break;

				case 'datetimepicker':
					$onerow->fieldhtml = $this->CreateInputText($id, $fieldname, $row['value'], 16, 20);
					$onerow->fieldclass = ' ' . $row['type'];
					break;

				case 'textarea':
					$onerow->fieldhtml = $this->CreateTextArea(FALSE, $id, $row['value'], $fieldname);
					break;

				case 'wysiwyg':
					$onerow->fieldhtml = $this->CreateTextArea(TRUE, $id, $row['value'], $fieldname);
					break;

				case 'button':
					$onerow->fieldhtml = $this->CreateInputSubmit($id, $fieldname, $row['name']);
					break;

				case 'gbfilepicker':
					$onerow->fieldhtml = '';
					$gbfp = cms_utils::get_module('GBFilePicker');
					if ( $gbfp )
					{
						$onerow->fieldhtml = $gbfp->CreateFilePickerInput($gbfp, $id, $fieldname, $row['value'], array('dir'=>'images','mode'=>'browser'));
					}
					break;

				case 'colorpicker':
					$onerow->fieldhtml = $this->CreateInputText($id, $fieldname, $row['value'], 10, 10);
					$onerow->fieldclass = ' inputcolorpicker';
					break;

				case 'fieldsetstart':
					//$onerow->fieldhtml = $this->CreateFieldsetStart($id, $fieldname, $row['name']);
					$onerow->fieldhtml = '<fieldset class="cgs_fieldset"><legend class="cgs_collapsible" id="section' . $row['fieldid'] . '"><span></span> ' . $row['name'] . '</legend><div>';
					break;

				case 'fieldsetend':
					//$onerow->fieldhtml = $this->CreateFieldsetEnd();
					$onerow->fieldhtml = '</div></fieldset> <!-- end cgs_collapsible -->';
					break;
			}

			array_push ($rowarray, $onerow);
		}
	}
}

$smarty->assign('items', $rowarray );
$smarty->assign('submit',$this->CreateInputSubmit ($id, 'submitbutton', lang('submit')));
$smarty->assign('cancel',$this->CreateInputSubmit ($id, 'cancel', lang('cancel')));
$smarty->assign('startform', $this->CreateFormStart( $id, 'save_general', $returnid ));
$smarty->assign('endform', $this->CreateFormEnd());

if ( function_exists('cms_admin_current_language') ) setlocale(LC_TIME, cms_admin_current_language()); // for cmsms 1.10 only
for ($i = 1; $i <= 12; $i++)
{
	$timestamp=mktime(1,1,1,$i,1,2000);
	$months[] = htmlentities(strftime('%B', $timestamp));
}
$monthnames = implode("','",$months);
for ($i = 1; $i <= 7; $i++)
{
	$timestamp=mktime(1,1,1,10,$i,2000);
	$days[] = htmlentities(strftime('%A', $timestamp));
	$daysmin[] = htmlentities(substr(strftime('%a', $timestamp), 0, 2));
}
$daynames = implode("','",$days);
$daynamesmin = implode("','",$daysmin);

$DP_locale = "
	$.datepicker.regional[''] = {
		closeText: '" . lang('close') . "',
		prevText: '" . lang('previous') . "',
		nextText: '" . lang('next') . "',
		currentText: '" . $this->Lang('now') . "',
		monthNames: ['" . $monthnames . "'],
		dayNames: ['" . $daynames . "'],
		dayNamesMin: ['" . $daynamesmin . "'],
		dateFormat: 'yyyy-mm-dd',
		firstDay: 1,
		isRTL: false,
		showMonthAfterYear: false,
		yearSuffix: ''
	};
	$.datepicker.setDefaults($.datepicker.regional['']);

	$.timepicker.regional[''] = {
		timeOnlyTitle: '" . $this->Lang('choosetime') . "',
		timeText: '" . $this->Lang('time') . "',
		hourText: '" . lang('hour') . "',
		minuteText: '" . lang('minutes') . "',
		currentText: '" . $this->Lang('now') . "',
		closeText: '" . lang('close') . "',
		ampm: false
	};
	$.timepicker.setDefaults($.timepicker.regional['']);
";

$smarty->assign('DP_locale', $DP_locale);


echo $this->ProcessTemplate('admin_general.tpl');
?>