<?php
#-------------------------------------------------------------------------
# Module: Custom Global Settings
# Author: Rolf Tjassens
#-------------------------------------------------------------------------
# CMS - CMS Made Simple is (c) 2011 by Ted Kulp (wishy@cmsmadesimple.org)
# This project's homepage is: http://www.cmsmadesimple.org
# The module's homepage is: http://dev.cmsmadesimple.org/projects/customgs
#-------------------------------------------------------------------------
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
# Or read it online: http://www.gnu.org/licenses/licenses.html#GPL
#-------------------------------------------------------------------------

if (!isset($gCms)) exit;

if (!$this->VisibleToAdminUser()) {
	$this->ShowErrors($this->Lang("accessdenied"));
	return;
}

/**
 * Translated Strings
 */
$this->smarty->assign('title_checkbox', $this->Lang('title_checkbox'));

/**
 * Content defines and Form stuff for the admin
 */ 
$this->RestrictUnknownParams();
$this->RegisterModulePlugin();

$this->smarty->assign('startform', $this->CreateFormStart( $id, 'save_checkboxes', $returnid ));
$this->smarty->assign('endform', $this->CreateFormEnd());

$number_checkboxes = 7;
$current = 1;
	while ($current <= $number_checkboxes) {
		$smarty->assign('input_title_checkbox_'.$current,$this->CreateInputText($id,'input_title_checkbox_'.$current,$this->GetPreference('input_title_checkbox_'.$current,''),50,255));
		$smarty->assign('input_help_checkbox_'.$current,$this->CreateInputText($id,'input_help_checkbox_'.$current,$this->GetPreference('input_help_checkbox_'.$current,''),50,255));
		$current++;
		}

echo $this->ProcessTemplate('admin_checkboxes.tpl');
?>