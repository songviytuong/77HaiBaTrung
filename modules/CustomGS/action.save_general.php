<?php
#-------------------------------------------------------------------------
# Module: Custom Global Settings
# Author: Rolf Tjassens, Jos
#-------------------------------------------------------------------------
# CMS - CMS Made Simple is (c) 2011 by Ted Kulp (wishy@cmsmadesimple.org)
# This project's homepage is: http://www.cmsmadesimple.org
# The module's homepage is: http://dev.cmsmadesimple.org/projects/customgs
#-------------------------------------------------------------------------
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
# Or read it online: http://www.gnu.org/licenses/licenses.html#GPL
#-------------------------------------------------------------------------

if (!isset($gCms)) exit;

if( !$this->CheckPermission('Custom Global Settings - Use') ) $this->Redirect($id, "defaultadmin", $returnid);

// Get all fields and settings
$allfields = $this->GetSettings(false);
$clearcache = 0;

foreach( $allfields as $key => $value )
{

	// Only use the numeric keys
	if( is_numeric($key) )
	{
		// Save field value
		$newvalue = isset($params['field'][$key]) ? $params['field'][$key] : 0;
		$query = "UPDATE " . cms_db_prefix() . "module_customgs SET value=? WHERE fieldid=?";
		$result = $db->Execute($query, array($newvalue, $key));
		
		// Sent event only when value has changed.
		if ( $newvalue != $value )
		{
			$field = $this->GetField($key);
			$this->SendEvent('OnSettingChange', array(
						'fieldid' => $field['fieldid'],
						'name' => $field['name'],
						'alias' => str_replace('__', '_', str_replace('-', '_', munge_string_to_url($field['name']))),
						'value' => $newvalue,
						'clearcache' => $field['clearcache']
			));
			$clearcache = $clearcache || $field['clearcache'];
		}
	}
}

// Clear the stylesheet cache if required by one of the fields and only if that field has been changed
if ( $clearcache ) $this->ClearStylesheetCache();

// Show saved parameters in debug mode
debug_display($params);

// Put mention into the admin log
audit('', 'Custom Global Settings - General tab', 'Saved');

$this->Redirect($id, 'defaultadmin', $returnid, array('module_message' => $this->Lang('settingssaved'), 'active_tab' => 'general'));
?>