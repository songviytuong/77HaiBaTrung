<?php
if (!isset($gCms)) exit;
if (!$this->CheckPermission('Administrate Statistics')) {
  echo $this->lang("nopermission");
  return;
}

$this->smarty->assign('dateformattext',$this->lang("dateformat"));
$this->smarty->assign('dateformathelp',$this->lang("dateformathelp"));
$this->smarty->assign('dateformatinput',$this->CreateInputText($id,"dateformat",$this->dateformat, 50, 255));


$this->smarty->assign('charttypetext',$this->lang("charttype"));
//$charttypes=array($this->Lang("textcharts")=>"text",$this->Lang("graphiccharts")=>"graphic",$this->Lang("flashchart")=>"flash");
$charttypes=array($this->Lang("textcharts")=>"text",$this->Lang("graphiccharts")=>"graphics",$this->Lang("jqplotchart")=>"jqplot"/*,$this->Lang("pchartcharts")=>"pchart"*/);
$hc=$this->GetModuleInstance("ChartsMadeSimple");
if ($hc) $charttypes[$this->Lang("highcharts")]="highcharts";
$this->smarty->assign('charttypeinput',$this->CreateInputDropDown($id,"charttype",$charttypes,-1,$this->charttype));

$this->smarty->assign('gfxnumberstext',$this->lang("gfxnumberstype"));
$gfxnumberstypes=array($this->Lang("digidisplayer")=>"digidisplayer");
$this->smarty->assign('gfxnumbersinput',$this->CreateInputDropDown($id,"gfxnumbers",$gfxnumberstypes,-1,$this->gfxnumbers));

$this->smarty->assign('iftextchartuse',$this->lang("iftextchartuse"));
$this->smarty->assign('ifjqplotsize',$this->lang("ifjqplotsize"));
$this->smarty->assign('jqplotxinput',$this->CreateInputText($id,"jqplotx",$this->jqplotx, 5, 3));
$this->smarty->assign('jqplotyinput',$this->CreateInputText($id,"jqploty",$this->jqploty, 5, 3));

$this->smarty->assign('maxdotsinput',$this->CreateInputText($id,"maxdots",$this->GetPreference("maxdots",32), 4,4));
$this->smarty->assign('times',$this->lang("times"));

$this->smarty->assign('dotcharinput',$this->CreateInputText($id,"dotchar",$this->GetPreference("dotchar","&#8226;"), 12,12));

$this->smarty->assign('statemailtext',$this->lang("statemail"));
$this->smarty->assign('statemailinput',$this->CreateInputText($id,"statemail",$this->statemail, 50, 255));

$iconsets=array("crystalclear"=>"crystalclear","crystalcleargrey"=>"crystalcleargrey");
$this->smarty->assign('iconsettext',$this->lang("iconset"));
$this->smarty->assign('iconsetinput',$this->CreateInputDropDown($id,"iconset",$iconsets,-1,$this->iconset));

$this->smarty->assign('submitvisuals',$this->CreateInputSubmit($id,"savevisuals",$this->lang("savevisuals")));
if (isset($params["show"])) {
  $this->smarty->assign('show',$this->CreateInputHidden($id,"show",$params["show"]));
}
$this->smarty->assign('startform',$this->CreateFormStart($id,"savevisuals",$returnid,"post","",true));
$this->smarty->assign('endform',$this->CreateFormEnd());


echo $this->ProcessTemplate("visuals.tpl");
?>
