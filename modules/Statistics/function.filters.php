<?php
if (!isset($gCms)) exit;
if (!$this->CheckPermission('Administrate Statistics')) {
	echo $this->lang("nopermission");
	return;
}
$this->smarty->assign('ignorerobotstext',$this->lang("ignorerobots"));
$this->smarty->assign('ignorerobotsinput',$this->CreateInputCheckBox($id,"ignorerobots","1",$this->ignorerobots));
$this->smarty->assign('extrarobotstext',$this->lang("extrarobots"));
$this->smarty->assign('extrarobotshelp',$this->lang("extrarobotshelp"));
$this->smarty->assign('extrarobotsinput',$this->CreateInputText($id,"extrarobots",$this->extrarobots, 50, 255)." ".$this->lang("semicolonsepsimple"));

$this->smarty->assign('ignoreiptext',$this->lang("ignoreips"));
$this->smarty->assign('ignoreipinput',$this->CreateInputText($id,"ignoreips",$this->ignoreips, 50, 255)." ".$this->lang("semicolonsep"));

$this->smarty->assign('ignorepagestext',$this->lang("ignorepages"));
$this->smarty->assign('ignorepagesinput',$this->CreateInputText($id,"ignorepages",$this->ignorepages, 50, 255)." ".$this->lang("pagessemicolonsep"));

$this->smarty->assign('ignorevisitorstext',$this->lang("ignorevisitors"));
$this->smarty->assign('ignorevisitorshelp',$this->lang("visitorssemicolonsep"));
$this->smarty->assign('ignorevisitorsinput',$this->CreateInputText($id,"ignorevisitors",$this->ignorevisitors, 50, 255));

$this->smarty->assign('ignorerefererstext',$this->lang("ignorereferers"));
$this->smarty->assign('ignorereferershelp',$this->lang("ignorereferershelp"));
$this->smarty->assign('ignorereferersinput',$this->CreateInputText($id,"ignorereferers",$this->ignorereferers, 50, 255));

$this->smarty->assign('ignoreadminstext',$this->lang("ignoreadmins"));
$this->smarty->assign('ignoreadminshelp',$this->lang("ignoreadminshelp"));
$this->smarty->assign('ignoreadminsinput',$this->CreateInputCheckBox($id,"ignoreadmins","1",$this->ignoreadmins));

$this->smarty->assign('submitfilters',$this->CreateInputSubmit($id,"savefilters",$this->lang("savefilters")));
if (isset($params["show"])) {
  $this->smarty->assign('show',$this->CreateInputHidden($id,"show",$params["show"]));
}
$this->smarty->assign('startform',$this->CreateFormStart($id,"savefilters",$returnid,"post","",true));
$this->smarty->assign('endform',$this->CreateFormEnd());

echo $this->ProcessTemplate("filters.tpl");

?>

