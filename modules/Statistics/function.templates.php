<?php
if (!isset($gCms)) exit;
if (!$this->CheckPermission('Administrate Statistics')) {
  echo $this->lang("nopermission");
  return;
}

$this->smarty->assign('topxtemplatetext',$this->lang("topxtemplate"));
$this->smarty->assign('topxtemplateinput',$this->CreateTextArea(false,$id,$this->topxtemplate,"topxtemplate","pagesmalltextarea"));
$this->smarty->assign('topxtemplatehelp',$this->lang("topxtemplatehelp"));
$this->smarty->assign('topxtemplateparams',"{\$entry->index} {\$entry->fulllink} {\$entry->href} {\$entry->title} {\$entry->menutitle}  {\$entry->alias}");
$this->smarty->assign('emailtemplatetext',$this->lang("emailtemplate"));
$this->smarty->assign('emailtemplateinput',$this->CreateTextArea(false,$id,$this->emailtemplate,"emailtemplate","pagesmalltextarea"));
$this->smarty->assign('emailtemplatehelp',$this->lang("emailtemplatehelp"));
$this->smarty->assign('emailtemplateparams',"{\$total}, {\$pageviews}, {\$today}, {\$thisweek}, {\$thismonth}, {\$thisyear}, {\$topbrowser}, {\$topos}, {\$topcountry}, {\$totalsinceemail}, {\$pageviewssinceemail}");

$this->smarty->assign('submittopx',$this->CreateInputSubmit($id,"savetopxtemplate",$this->lang("savetopx")));
$this->smarty->assign('submitemail',$this->CreateInputSubmit($id,"saveemailtemplate",$this->lang("saveemail")));

$this->smarty->assign('resetemail',$this->CreateInputSubmit($id,"resetemailtemplate",$this->lang("resetemail"),"","",$this->Lang("confirmresetemail")));
$this->smarty->assign('resettopx',$this->CreateInputSubmit($id,"resettopxtemplate",$this->lang("resettopx"),"","",$this->Lang("confirmresettopx")));

if (isset($params["show"])) {
  $this->smarty->assign('show',$this->CreateInputHidden($id,"show",$params["show"]));
}
$this->smarty->assign('startform1',$this->CreateFormStart($id,"savetemplates",$returnid,"post","",true));
$this->smarty->assign('endform1',$this->CreateFormEnd());
$this->smarty->assign('startform2',$this->CreateFormStart($id,"savetemplates",$returnid,"post","",true));
$this->smarty->assign('endform2',$this->CreateFormEnd());


echo $this->ProcessTemplate("templates.tpl");
?>