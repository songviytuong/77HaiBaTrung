<?php
if (!isset($gCms)) exit;
if (!$this->CheckPermission('Administrate Statistics')) {
  echo $this->lang("nopermission");
  return;
}

$this->smarty->assign('startform',$this->CreateFormStart($id,"resetdata",$returnid,"post","",true));
$this->smarty->assign('endform',$this->CreateFormEnd());
if (isset($params["show"])) {
  $this->smarty->assign('show',$this->CreateInputHidden($id,"show",$params["show"]));
}
$this->smarty->assign('submitresetall',$this->CreateInputSubmit($id,"resetall",$this->lang("resetall"),"","",$this->Lang("confirmresetall")));
$this->smarty->assign('submitresetdata',$this->CreateInputSubmit($id,"resetdata",$this->lang("resetdata"),"","",$this->Lang("confirmresetdata")));
$this->smarty->assign('submitresetvisitors',$this->CreateInputSubmit($id,"resetvisitors",$this->lang("resetvisitors"),"","",$this->Lang("confirmresetvisitors")));

$this->smarty->assign('forcepageviews',$this->CreateInputText($id,"forcepageviews",""));
$this->smarty->assign('forcevisitors',$this->CreateInputText($id,"forcevisitors",""));

$this->smarty->assign('currentvisitors',$this->lang('currently')." ".$this->GetValue("count_total","",0));
$this->smarty->assign('currentpageviews',$this->lang('currently')." ".$this->GetValue("count_total_pageviews","",0));

$this->smarty->assign('forcepageviewstext',$this->lang('forcepageviews'));
$this->smarty->assign('forcevisitorstext',$this->lang('forcevisitors'));
$this->smarty->assign('resetdatatext',$this->lang('resetdata'));
$this->smarty->assign('resetdatahelp',$this->lang('resetdatahelp'));
$this->smarty->assign('resetvisitorshelp',$this->lang('resetvisitorshelp'));
$this->smarty->assign('resetallhelp',$this->lang('resetallhelp'));
echo $this->ProcessTemplate("resetdata.tpl");

?>
