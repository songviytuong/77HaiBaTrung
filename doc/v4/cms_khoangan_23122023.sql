/*
 Navicat Premium Data Transfer

 Source Server         : 207.148.118.139 [OLD]
 Source Server Type    : MySQL
 Source Server Version : 50741
 Source Host           : 207.148.118.139:3306
 Source Schema         : cms_khoangan

 Target Server Type    : MySQL
 Target Server Version : 50741
 File Encoding         : 65001

 Date: 23/12/2023 10:18:09
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for cms_additional_htmlblob_users
-- ----------------------------
DROP TABLE IF EXISTS `cms_additional_htmlblob_users`;
CREATE TABLE `cms_additional_htmlblob_users`  (
  `additional_htmlblob_users_id` int(11) NOT NULL,
  `user_id` int(11) NULL DEFAULT NULL,
  `htmlblob_id` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`additional_htmlblob_users_id`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = FIXED;

-- ----------------------------
-- Records of cms_additional_htmlblob_users
-- ----------------------------
INSERT INTO `cms_additional_htmlblob_users` VALUES (1, 2, 11);
INSERT INTO `cms_additional_htmlblob_users` VALUES (2, 2, 12);

-- ----------------------------
-- Table structure for cms_additional_htmlblob_users_seq
-- ----------------------------
DROP TABLE IF EXISTS `cms_additional_htmlblob_users_seq`;
CREATE TABLE `cms_additional_htmlblob_users_seq`  (
  `id` int(11) NOT NULL
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = FIXED;

-- ----------------------------
-- Records of cms_additional_htmlblob_users_seq
-- ----------------------------
INSERT INTO `cms_additional_htmlblob_users_seq` VALUES (2);

-- ----------------------------
-- Table structure for cms_additional_users
-- ----------------------------
DROP TABLE IF EXISTS `cms_additional_users`;
CREATE TABLE `cms_additional_users`  (
  `additional_users_id` int(11) NOT NULL,
  `user_id` int(11) NULL DEFAULT NULL,
  `page_id` int(11) NULL DEFAULT NULL,
  `content_id` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`additional_users_id`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = FIXED;

-- ----------------------------
-- Records of cms_additional_users
-- ----------------------------

-- ----------------------------
-- Table structure for cms_additional_users_seq
-- ----------------------------
DROP TABLE IF EXISTS `cms_additional_users_seq`;
CREATE TABLE `cms_additional_users_seq`  (
  `id` int(11) NOT NULL
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = FIXED;

-- ----------------------------
-- Records of cms_additional_users_seq
-- ----------------------------
INSERT INTO `cms_additional_users_seq` VALUES (0);

-- ----------------------------
-- Table structure for cms_admin_bookmarks
-- ----------------------------
DROP TABLE IF EXISTS `cms_admin_bookmarks`;
CREATE TABLE `cms_admin_bookmarks`  (
  `bookmark_id` int(11) NOT NULL,
  `user_id` int(11) NULL DEFAULT NULL,
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`bookmark_id`) USING BTREE,
  INDEX `cms_index_admin_bookmarks_by_user_id`(`user_id`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of cms_admin_bookmarks
-- ----------------------------

-- ----------------------------
-- Table structure for cms_admin_bookmarks_seq
-- ----------------------------
DROP TABLE IF EXISTS `cms_admin_bookmarks_seq`;
CREATE TABLE `cms_admin_bookmarks_seq`  (
  `id` int(11) NOT NULL
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = FIXED;

-- ----------------------------
-- Records of cms_admin_bookmarks_seq
-- ----------------------------
INSERT INTO `cms_admin_bookmarks_seq` VALUES (0);

-- ----------------------------
-- Table structure for cms_admin_recent_pages
-- ----------------------------
DROP TABLE IF EXISTS `cms_admin_recent_pages`;
CREATE TABLE `cms_admin_recent_pages`  (
  `id` int(11) NOT NULL,
  `user_id` int(11) NULL DEFAULT NULL,
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `access_time` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of cms_admin_recent_pages
-- ----------------------------

-- ----------------------------
-- Table structure for cms_admin_recent_pages_seq
-- ----------------------------
DROP TABLE IF EXISTS `cms_admin_recent_pages_seq`;
CREATE TABLE `cms_admin_recent_pages_seq`  (
  `id` int(11) NOT NULL
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = FIXED;

-- ----------------------------
-- Records of cms_admin_recent_pages_seq
-- ----------------------------
INSERT INTO `cms_admin_recent_pages_seq` VALUES (0);

-- ----------------------------
-- Table structure for cms_adminlog
-- ----------------------------
DROP TABLE IF EXISTS `cms_adminlog`;
CREATE TABLE `cms_adminlog`  (
  `timestamp` int(11) NULL DEFAULT NULL,
  `user_id` int(11) NULL DEFAULT NULL,
  `username` varchar(25) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `item_id` int(11) NULL DEFAULT NULL,
  `item_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `action` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `ip_addr` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  INDEX `cms_index_adminlog1`(`timestamp`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of cms_adminlog
-- ----------------------------
INSERT INTO `cms_adminlog` VALUES (1699695911, 0, '', -1, 'Automated Task Succeeded', 'ClearCacheTask', '');
INSERT INTO `cms_adminlog` VALUES (1699695911, 0, '', -1, 'Automated Task Succeeded', 'PruneAdminlogTask', '');
INSERT INTO `cms_adminlog` VALUES (1701330217, 0, '', -1, 'Automated Task Succeeded', 'ClearCacheTask', '');
INSERT INTO `cms_adminlog` VALUES (1699231154, 0, '', -1, 'Automated Task Succeeded', 'PruneAdminlogTask', '');
INSERT INTO `cms_adminlog` VALUES (1701330217, 0, '', -1, 'Automated Task Succeeded', 'PruneAdminlogTask', '');
INSERT INTO `cms_adminlog` VALUES (1699231154, 0, '', -1, 'Automated Task Succeeded', 'ClearCacheTask', '');

-- ----------------------------
-- Table structure for cms_content
-- ----------------------------
DROP TABLE IF EXISTS `cms_content`;
CREATE TABLE `cms_content`  (
  `content_id` int(11) NOT NULL,
  `content_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `type` varchar(25) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `owner_id` int(11) NULL DEFAULT NULL,
  `parent_id` int(11) NULL DEFAULT NULL,
  `template_id` int(11) NULL DEFAULT NULL,
  `item_order` int(11) NULL DEFAULT NULL,
  `hierarchy` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `default_content` tinyint(4) NULL DEFAULT NULL,
  `menu_text` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `content_alias` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `show_in_menu` tinyint(4) NULL DEFAULT NULL,
  `collapsed` tinyint(4) NULL DEFAULT NULL,
  `markup` varchar(25) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `active` tinyint(4) NULL DEFAULT NULL,
  `activehome` int(4) NOT NULL,
  `cachable` tinyint(4) NULL DEFAULT NULL,
  `id_hierarchy` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `hierarchy_path` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `prop_names` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `metadata` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `titleattribute` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `tabindex` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `accesskey` varchar(5) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `last_modified_by` int(11) NULL DEFAULT NULL,
  `create_date` datetime NULL DEFAULT NULL,
  `modified_date` datetime NULL DEFAULT NULL,
  `secure` tinyint(4) NULL DEFAULT NULL,
  `page_url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`content_id`) USING BTREE,
  INDEX `cms_index_content_by_content_alias_active`(`content_alias`, `active`) USING BTREE,
  INDEX `cms_index_content_by_default_content`(`default_content`) USING BTREE,
  INDEX `cms_index_content_by_parent_id`(`parent_id`) USING BTREE,
  INDEX `cms_index_content_by_hierarchy`(`hierarchy`) USING BTREE,
  INDEX `cms_index_content_by_idhier`(`content_id`, `hierarchy`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of cms_content
-- ----------------------------
INSERT INTO `cms_content` VALUES (63, 'Thành viên', 'content', 1, 68, 24, 4, '00001.00004', 0, 'Thành viên', 'staff', 1, NULL, 'html', 0, 0, 1, '68.63', 'vi/staff', 'content_en,target,searchable,extra1,extra2,extra3,pagedata,disable_wysiwyg', '{* Add code here that should appear in the metadata section of all new pages *}', '', '', '', 1, '2015-04-24 18:21:42', '2015-05-04 17:09:20', 0, 'staff');
INSERT INTO `cms_content` VALUES (61, 'Giới thiệu', 'content', 1, 68, 24, 2, '00001.00002', 0, 'Giới thiệu', 'about-us', 1, NULL, 'html', 1, 0, 1, '68.61', 'vi/about-us', 'target,disable_wysiwyg,pagedata,extra3,extra2,extra1,searchable,content_en', '{* Add code here that should appear in the metadata section of all new pages *}', '', '', '', 1, '2015-04-24 18:19:32', '2015-04-24 18:44:15', 0, 'about-us');
INSERT INTO `cms_content` VALUES (62, 'Menu', 'content', 1, 68, 24, 3, '00001.00003', 0, 'Menu', 'menu', 1, NULL, 'html', 1, 0, 1, '68.62', 'vi/menu', 'content_en,target,searchable,extra1,extra2,extra3,pagedata,disable_wysiwyg', '{* Add code here that should appear in the metadata section of all new pages *}', '', '', '', 1, '2015-04-24 18:20:04', '2015-04-24 18:20:04', 0, '');
INSERT INTO `cms_content` VALUES (60, 'Trang chủ', 'content', 1, 68, 24, 1, '00001.00001', 0, 'Trang chủ', 'home', 1, NULL, 'html', 1, 0, 1, '68.60', 'vi/home', 'content_en,searchable,extra1,extra2,extra3,pagedata,disable_wysiwyg,target,url', '', '', '', '', 1, '2015-04-24 17:46:48', '2015-09-18 10:20:57', 0, '');
INSERT INTO `cms_content` VALUES (64, 'Hình ảnh', 'content', 1, 68, 24, 6, '00001.00006', 0, 'Hình ảnh', 'gallery', 1, NULL, 'html', 1, 0, 1, '68.64', 'vi/gallery', 'content_en,target,searchable,extra1,extra2,extra3,pagedata,disable_wysiwyg', '{* Add code here that should appear in the metadata section of all new pages *}', '', '', '', 1, '2015-04-24 18:23:09', '2015-05-04 17:12:11', 0, '');
INSERT INTO `cms_content` VALUES (65, 'Khuyến mãi', 'content', 1, 68, 24, 7, '00001.00007', 0, 'Khuyến mãi', 'promotion', 1, NULL, 'html', 1, 0, 1, '68.65', 'vi/promotion', 'content_en,target,searchable,extra1,extra2,extra3,pagedata,disable_wysiwyg', '{* Add code here that should appear in the metadata section of all new pages *}', '', '', '', 1, '2015-04-24 18:23:28', '2015-05-04 17:14:05', 0, 'promotion');
INSERT INTO `cms_content` VALUES (66, 'Đặt bàn', 'content', 1, 68, 24, 8, '00001.00008', 0, 'Đặt bàn', 'reservations', 1, NULL, 'html', 1, 0, 1, '68.66', 'vi/reservations', 'content_en,target,searchable,extra1,extra2,extra3,pagedata,disable_wysiwyg', '{* Add code here that should appear in the metadata section of all new pages *}', '', '', '', 1, '2015-04-24 18:23:56', '2019-04-24 14:59:31', 0, 'reservations');
INSERT INTO `cms_content` VALUES (67, 'Liên hệ', 'content', 1, 68, 24, 10, '00001.00010', 0, 'Liên hệ', 'contact', 1, NULL, 'html', 1, 0, 1, '68.67', 'vi/contact', 'content_en,target,searchable,extra1,extra2,extra3,pagedata,disable_wysiwyg', '{* Add code here that should appear in the metadata section of all new pages *}', '', '', '', 1, '2015-04-25 19:23:24', '2019-04-24 15:14:43', 0, 'contact');
INSERT INTO `cms_content` VALUES (68, 'Vietnamese', 'content', 1, -1, 24, 1, '00001', 0, 'Vietnamese', 'vi', 0, NULL, 'html', 1, 0, 1, '68', 'vi', 'content_en,searchable,extra1,extra2,extra3,pagedata,disable_wysiwyg,target', '{* Add code here that should appear in the metadata section of all new pages *}', '', '', '', 1, '2015-04-27 07:34:20', '2015-04-27 07:35:21', 0, 'vi');
INSERT INTO `cms_content` VALUES (69, 'Người nổi tiếng', 'content', 1, 68, 24, 5, '00001.00005', 0, 'Người nổi tiếng', 'testimonials', 1, NULL, 'html', 1, 0, 1, '68.69', 'vi/testimonials', 'content_en,target,searchable,extra1,extra2,extra3,pagedata,disable_wysiwyg', '{* Add code here that should appear in the metadata section of all new pages *}', '', '', '', 1, '2015-05-04 17:11:50', '2015-05-04 17:12:22', 0, '');
INSERT INTO `cms_content` VALUES (70, 'Event', 'sectionheader', 1, -1, -1, 2, '00002', 0, 'Event', 'event', 1, NULL, 'html', 1, 0, 1, '70', 'event', 'content_en,searchable,extra1,extra2,extra3', '{* Add code here that should appear in the metadata section of all new pages *}', '', '', '', 1, '2015-09-11 23:33:44', '2015-09-11 23:33:44', 0, '');
INSERT INTO `cms_content` VALUES (71, 'VIP Member', 'content', 1, 70, 26, 1, '00002.00001', 0, 'VIP Member', 'vip', 1, NULL, 'html', 1, 0, 1, '70.71', 'event/vip', 'content_en,searchable,extra1,extra2,extra3,pagedata,disable_wysiwyg,target', '{* Add code here that should appear in the metadata section of all new pages *}', '', '', '', 1, '2015-09-11 23:34:22', '2015-09-15 15:18:01', 0, '');
INSERT INTO `cms_content` VALUES (72, 'Event Action', 'content', 1, 70, 27, 3, '00002.00003', 0, 'Event Action', 'event-action', 1, NULL, 'html', 1, 0, 1, '70.72', 'event/event-action', 'content_en,searchable,extra1,extra2,extra3,pagedata,disable_wysiwyg,target', '{* Add code here that should appear in the metadata section of all new pages *}', '', '', '', 1, '2015-09-12 01:04:29', '2019-04-24 14:35:37', 0, '');
INSERT INTO `cms_content` VALUES (73, 'Check', 'content', 1, 70, 29, 2, '00002.00002', 0, 'Check', 'check', 1, NULL, 'html', 1, 0, 1, '70.73', 'event/check', 'content_en,searchable,extra1,extra2,extra3,pagedata,disable_wysiwyg,target', '{* Add code here that should appear in the metadata section of all new pages *}', '', '', '', 1, '2015-09-18 14:03:39', '2019-04-24 14:28:31', 0, '');
INSERT INTO `cms_content` VALUES (74, 'Reservations Action', 'content', 1, 68, 27, 9, '00001.00009', 0, 'Reservations Action', 'reservations-action', 0, NULL, 'html', 1, 0, 1, '68.74', 'vi/reservations-action', 'content_en,searchable,extra1,extra2,extra3,pagedata,disable_wysiwyg,target', '{* Add code here that should appear in the metadata section of all new pages *}', '', '', '', 1, '2019-04-24 15:14:26', '2019-04-24 16:24:21', 0, '');

-- ----------------------------
-- Table structure for cms_content_props
-- ----------------------------
DROP TABLE IF EXISTS `cms_content_props`;
CREATE TABLE `cms_content_props`  (
  `content_id` int(11) NULL DEFAULT NULL,
  `type` varchar(25) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `prop_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `param1` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `param2` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `param3` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `content` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `create_date` datetime NULL DEFAULT NULL,
  `modified_date` datetime NULL DEFAULT NULL,
  INDEX `cms_index_content_props_by_content_id`(`content_id`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of cms_content_props
-- ----------------------------
INSERT INTO `cms_content_props` VALUES (31, 'string', 'content_en', NULL, NULL, NULL, 'asdf', '2006-07-25 21:22:31', '2006-07-25 21:22:31');
INSERT INTO `cms_content_props` VALUES (32, 'string', 'content_en', NULL, NULL, NULL, 'asdf', '2006-07-25 21:22:31', '2006-07-25 21:22:31');
INSERT INTO `cms_content_props` VALUES (15, 'string', 'target', '', '', '', '', NULL, '2009-05-13 10:12:18');
INSERT INTO `cms_content_props` VALUES (15, 'string', 'pagedata', '', '', '', '', NULL, '2009-05-13 10:12:18');
INSERT INTO `cms_content_props` VALUES (15, 'string', 'extra1', '', '', '', '', NULL, '2009-05-13 10:12:18');
INSERT INTO `cms_content_props` VALUES (15, 'string', 'extra2', '', '', '', '', NULL, '2009-05-13 10:12:18');
INSERT INTO `cms_content_props` VALUES (15, 'string', 'extra3', '', '', '', '', NULL, '2009-05-13 10:12:18');
INSERT INTO `cms_content_props` VALUES (15, 'string', 'image', '', '', '', '-1', NULL, '2009-05-13 10:12:18');
INSERT INTO `cms_content_props` VALUES (15, 'string', 'thumbnail', '', '', '', '', NULL, '2009-05-13 10:12:18');
INSERT INTO `cms_content_props` VALUES (15, 'string', 'searchable', '', '', '', '1', NULL, '2009-05-13 10:12:18');
INSERT INTO `cms_content_props` VALUES (15, 'string', 'disable_wysiwyg', '', '', '', '0', NULL, '2009-05-13 10:12:18');
INSERT INTO `cms_content_props` VALUES (15, 'string', 'image1', '', '', '', 'uploads/images/logo1.gif', NULL, '2009-05-13 10:12:18');
INSERT INTO `cms_content_props` VALUES (15, 'string', 'content_en', '', '', '', '<p>Congratulations! The installation worked. You now have a fully functional installation of CMS Made Simple and you are <em>almost</em> ready to start building your site.</p>\n<p>If you chose to install the default content, you will see numerous pages available to read.  You should read them thoroughly  as these default pages are devoted to showing you the basics of how to begin working with CMS Made Simple.  On these example pages, templates, and stylesheets many of the features of the default installation of CMS Made Simple are described and demonstrated. You can learn much about the power of CMS Made Simple by absorbing this information.</p>\n<p>To get to the Administration Console you have to login as the administrator (with the username/password you mentioned during the installation process) on your site at http://yourwebsite.com/cmsmspath/admin.  If this is your site click <a title=\"CMSMS Demo Admin Panel\" href=\"admin\">here</a> to login.</p>\n<p>Read about how to use CMS Made Simple in the {cms_selflink ext=\"http://wiki.cmsmadesimple.org/\" title=\"CMS Made Simple Documentation\" text=\"documentation\" target=\"_blank\"}. In case you need any help the community is always at your service, in the  {cms_selflink ext=\"http://forum.cmsmadesimple.org\" title=\"CMS Made Simple Forum\" text=\"forum\" target=\"_blank\"} or the {cms_selflink ext=\"http://www.cmsmadesimple.org/support/irc\" title=\"Information about the CMS Made Simple IRC channel\" text=\"IRC\" target=\"_blank\"}.</p>\n<h3>License</h3>\n<p>CMS Made Simple is released under the {cms_selflink ext=\"http://www.gnu.org/licenses/licenses.html#GPL\" title=\"General Public License\" text=\"GPL\" target=\"_blank\"} license and as such you don\'t have to leave a link back to us in these templates or on your site as much as we would like it.</p>\n<p> Some third party add-on modules may include additional license restrictions.</p>', NULL, '2009-05-13 10:12:18');
INSERT INTO `cms_content_props` VALUES (63, 'string', 'content_en', NULL, NULL, NULL, '<!-- Add code here that should appear in the content block of all new pages -->', NULL, '2015-05-04 17:09:20');
INSERT INTO `cms_content_props` VALUES (62, 'string', 'content_en', NULL, NULL, NULL, '<!-- Add code here that should appear in the content block of all new pages -->', NULL, '2015-04-24 18:20:04');
INSERT INTO `cms_content_props` VALUES (61, 'string', 'target', NULL, NULL, NULL, '', NULL, '2015-04-24 18:44:15');
INSERT INTO `cms_content_props` VALUES (62, 'string', 'target', NULL, NULL, NULL, '', NULL, '2015-04-24 18:20:04');
INSERT INTO `cms_content_props` VALUES (61, 'string', 'disable_wysiwyg', NULL, NULL, NULL, '0', NULL, '2015-04-24 18:44:15');
INSERT INTO `cms_content_props` VALUES (61, 'string', 'pagedata', NULL, NULL, NULL, '', NULL, '2015-04-24 18:44:15');
INSERT INTO `cms_content_props` VALUES (61, 'string', 'extra3', NULL, NULL, NULL, '', NULL, '2015-04-24 18:19:32');
INSERT INTO `cms_content_props` VALUES (61, 'string', 'extra2', NULL, NULL, NULL, '', NULL, '2015-04-24 18:19:32');
INSERT INTO `cms_content_props` VALUES (61, 'string', 'extra1', NULL, NULL, NULL, '', NULL, '2015-04-24 18:19:32');
INSERT INTO `cms_content_props` VALUES (61, 'string', 'searchable', NULL, NULL, NULL, '1', NULL, '2015-04-24 18:44:15');
INSERT INTO `cms_content_props` VALUES (61, 'string', 'content_en', NULL, NULL, NULL, '<!-- Add code here that should appear in the content block of all new pages -->', NULL, '2015-04-24 18:44:15');
INSERT INTO `cms_content_props` VALUES (62, 'string', 'searchable', NULL, NULL, NULL, '1', NULL, '2015-04-24 18:20:04');
INSERT INTO `cms_content_props` VALUES (62, 'string', 'extra1', NULL, NULL, NULL, '', NULL, '2015-04-24 18:20:04');
INSERT INTO `cms_content_props` VALUES (62, 'string', 'extra2', NULL, NULL, NULL, '', NULL, '2015-04-24 18:20:04');
INSERT INTO `cms_content_props` VALUES (62, 'string', 'extra3', NULL, NULL, NULL, '', NULL, '2015-04-24 18:20:04');
INSERT INTO `cms_content_props` VALUES (62, 'string', 'pagedata', NULL, NULL, NULL, '', NULL, '2015-04-24 18:20:04');
INSERT INTO `cms_content_props` VALUES (62, 'string', 'disable_wysiwyg', NULL, NULL, NULL, '0', NULL, '2015-04-24 18:20:04');
INSERT INTO `cms_content_props` VALUES (60, 'string', 'content_en', NULL, NULL, NULL, '', NULL, '2015-09-18 10:20:57');
INSERT INTO `cms_content_props` VALUES (60, 'string', 'searchable', NULL, NULL, NULL, '1', NULL, '2015-09-18 10:20:57');
INSERT INTO `cms_content_props` VALUES (60, 'string', 'extra1', NULL, NULL, NULL, '', NULL, '2015-04-24 17:47:44');
INSERT INTO `cms_content_props` VALUES (60, 'string', 'extra2', NULL, NULL, NULL, '', NULL, '2015-04-24 17:47:44');
INSERT INTO `cms_content_props` VALUES (60, 'string', 'extra3', NULL, NULL, NULL, '', NULL, '2015-04-24 17:47:44');
INSERT INTO `cms_content_props` VALUES (60, 'string', 'pagedata', NULL, NULL, NULL, '', NULL, '2015-09-18 10:20:57');
INSERT INTO `cms_content_props` VALUES (60, 'string', 'disable_wysiwyg', NULL, NULL, NULL, '0', NULL, '2015-09-18 10:20:57');
INSERT INTO `cms_content_props` VALUES (60, 'string', 'target', NULL, NULL, NULL, '', NULL, '2015-09-18 10:20:57');
INSERT INTO `cms_content_props` VALUES (63, 'string', 'target', NULL, NULL, NULL, '', NULL, '2015-05-04 17:09:20');
INSERT INTO `cms_content_props` VALUES (63, 'string', 'searchable', NULL, NULL, NULL, '1', NULL, '2015-05-04 17:09:20');
INSERT INTO `cms_content_props` VALUES (63, 'string', 'extra1', NULL, NULL, NULL, '', NULL, '2015-05-04 17:09:20');
INSERT INTO `cms_content_props` VALUES (63, 'string', 'extra2', NULL, NULL, NULL, '', NULL, '2015-05-04 17:09:20');
INSERT INTO `cms_content_props` VALUES (63, 'string', 'extra3', NULL, NULL, NULL, '', NULL, '2015-05-04 17:09:20');
INSERT INTO `cms_content_props` VALUES (63, 'string', 'pagedata', NULL, NULL, NULL, '', NULL, '2015-05-04 17:09:20');
INSERT INTO `cms_content_props` VALUES (63, 'string', 'disable_wysiwyg', NULL, NULL, NULL, '0', NULL, '2015-05-04 17:09:20');
INSERT INTO `cms_content_props` VALUES (64, 'string', 'content_en', NULL, NULL, NULL, '<!-- Add code here that should appear in the content block of all new pages -->', NULL, '2015-04-24 18:23:09');
INSERT INTO `cms_content_props` VALUES (64, 'string', 'target', NULL, NULL, NULL, '', NULL, '2015-04-24 18:23:09');
INSERT INTO `cms_content_props` VALUES (64, 'string', 'searchable', NULL, NULL, NULL, '1', NULL, '2015-04-24 18:23:09');
INSERT INTO `cms_content_props` VALUES (64, 'string', 'extra1', NULL, NULL, NULL, '', NULL, '2015-04-24 18:23:09');
INSERT INTO `cms_content_props` VALUES (64, 'string', 'extra2', NULL, NULL, NULL, '', NULL, '2015-04-24 18:23:09');
INSERT INTO `cms_content_props` VALUES (64, 'string', 'extra3', NULL, NULL, NULL, '', NULL, '2015-04-24 18:23:09');
INSERT INTO `cms_content_props` VALUES (64, 'string', 'pagedata', NULL, NULL, NULL, '', NULL, '2015-04-24 18:23:09');
INSERT INTO `cms_content_props` VALUES (64, 'string', 'disable_wysiwyg', NULL, NULL, NULL, '0', NULL, '2015-04-24 18:23:09');
INSERT INTO `cms_content_props` VALUES (65, 'string', 'content_en', NULL, NULL, NULL, '<!-- Add code here that should appear in the content block of all new pages -->', NULL, '2015-05-04 17:14:05');
INSERT INTO `cms_content_props` VALUES (65, 'string', 'target', NULL, NULL, NULL, '', NULL, '2015-05-04 17:14:05');
INSERT INTO `cms_content_props` VALUES (65, 'string', 'searchable', NULL, NULL, NULL, '1', NULL, '2015-05-04 17:14:05');
INSERT INTO `cms_content_props` VALUES (65, 'string', 'extra1', NULL, NULL, NULL, '', NULL, '2015-04-24 18:23:28');
INSERT INTO `cms_content_props` VALUES (65, 'string', 'extra2', NULL, NULL, NULL, '', NULL, '2015-04-24 18:23:28');
INSERT INTO `cms_content_props` VALUES (65, 'string', 'extra3', NULL, NULL, NULL, '', NULL, '2015-04-24 18:23:28');
INSERT INTO `cms_content_props` VALUES (65, 'string', 'pagedata', NULL, NULL, NULL, '', NULL, '2015-05-04 17:14:05');
INSERT INTO `cms_content_props` VALUES (65, 'string', 'disable_wysiwyg', NULL, NULL, NULL, '0', NULL, '2015-05-04 17:14:05');
INSERT INTO `cms_content_props` VALUES (66, 'string', 'content_en', NULL, NULL, NULL, '<!-- Add code here that should appear in the content block of all new pages -->', NULL, '2019-04-24 14:59:31');
INSERT INTO `cms_content_props` VALUES (66, 'string', 'target', NULL, NULL, NULL, '', NULL, '2019-04-24 14:59:31');
INSERT INTO `cms_content_props` VALUES (66, 'string', 'searchable', NULL, NULL, NULL, '1', NULL, '2019-04-24 14:59:31');
INSERT INTO `cms_content_props` VALUES (66, 'string', 'extra1', NULL, NULL, NULL, '', NULL, '2019-04-24 14:59:31');
INSERT INTO `cms_content_props` VALUES (66, 'string', 'extra2', NULL, NULL, NULL, '', NULL, '2019-04-24 14:59:31');
INSERT INTO `cms_content_props` VALUES (66, 'string', 'extra3', NULL, NULL, NULL, '', NULL, '2019-04-24 14:59:31');
INSERT INTO `cms_content_props` VALUES (66, 'string', 'pagedata', NULL, NULL, NULL, '', NULL, '2019-04-24 14:59:31');
INSERT INTO `cms_content_props` VALUES (66, 'string', 'disable_wysiwyg', NULL, NULL, NULL, '0', NULL, '2019-04-24 14:59:31');
INSERT INTO `cms_content_props` VALUES (67, 'string', 'content_en', NULL, NULL, NULL, '<!-- Add code here that should appear in the content block of all new pages -->', NULL, '2015-04-25 19:23:32');
INSERT INTO `cms_content_props` VALUES (67, 'string', 'target', NULL, NULL, NULL, '', NULL, '2015-04-25 19:23:32');
INSERT INTO `cms_content_props` VALUES (67, 'string', 'searchable', NULL, NULL, NULL, '1', NULL, '2015-04-25 19:23:32');
INSERT INTO `cms_content_props` VALUES (67, 'string', 'extra1', NULL, NULL, NULL, '', NULL, '2015-04-25 19:23:24');
INSERT INTO `cms_content_props` VALUES (67, 'string', 'extra2', NULL, NULL, NULL, '', NULL, '2015-04-25 19:23:24');
INSERT INTO `cms_content_props` VALUES (67, 'string', 'extra3', NULL, NULL, NULL, '', NULL, '2015-04-25 19:23:24');
INSERT INTO `cms_content_props` VALUES (67, 'string', 'pagedata', NULL, NULL, NULL, '', NULL, '2015-04-25 19:23:32');
INSERT INTO `cms_content_props` VALUES (67, 'string', 'disable_wysiwyg', NULL, NULL, NULL, '0', NULL, '2015-04-25 19:23:32');
INSERT INTO `cms_content_props` VALUES (68, 'string', 'content_en', NULL, NULL, NULL, '<!-- Add code here that should appear in the content block of all new pages -->', NULL, '2015-04-27 07:35:21');
INSERT INTO `cms_content_props` VALUES (68, 'string', 'searchable', NULL, NULL, NULL, '1', NULL, '2015-04-27 07:35:21');
INSERT INTO `cms_content_props` VALUES (68, 'string', 'extra1', NULL, NULL, NULL, '', NULL, '2015-04-27 07:35:21');
INSERT INTO `cms_content_props` VALUES (68, 'string', 'extra2', NULL, NULL, NULL, '', NULL, '2015-04-27 07:35:21');
INSERT INTO `cms_content_props` VALUES (68, 'string', 'extra3', NULL, NULL, NULL, '', NULL, '2015-04-27 07:35:21');
INSERT INTO `cms_content_props` VALUES (68, 'string', 'pagedata', NULL, NULL, NULL, '', NULL, '2015-04-27 07:35:21');
INSERT INTO `cms_content_props` VALUES (68, 'string', 'disable_wysiwyg', NULL, NULL, NULL, '0', NULL, '2015-04-27 07:35:21');
INSERT INTO `cms_content_props` VALUES (68, 'string', 'target', NULL, NULL, NULL, '', NULL, '2015-04-27 07:35:21');
INSERT INTO `cms_content_props` VALUES (69, 'string', 'content_en', NULL, NULL, NULL, '<!-- Add code here that should appear in the content block of all new pages -->', NULL, '2015-05-04 17:12:22');
INSERT INTO `cms_content_props` VALUES (69, 'string', 'target', NULL, NULL, NULL, '', NULL, '2015-05-04 17:12:22');
INSERT INTO `cms_content_props` VALUES (69, 'string', 'searchable', NULL, NULL, NULL, '1', NULL, '2015-05-04 17:12:22');
INSERT INTO `cms_content_props` VALUES (69, 'string', 'extra1', NULL, NULL, NULL, '', NULL, '2015-05-04 17:12:22');
INSERT INTO `cms_content_props` VALUES (69, 'string', 'extra2', NULL, NULL, NULL, '', NULL, '2015-05-04 17:12:22');
INSERT INTO `cms_content_props` VALUES (69, 'string', 'extra3', NULL, NULL, NULL, '', NULL, '2015-05-04 17:12:22');
INSERT INTO `cms_content_props` VALUES (69, 'string', 'pagedata', NULL, NULL, NULL, '', NULL, '2015-05-04 17:12:22');
INSERT INTO `cms_content_props` VALUES (69, 'string', 'disable_wysiwyg', NULL, NULL, NULL, '0', NULL, '2015-05-04 17:12:22');
INSERT INTO `cms_content_props` VALUES (70, 'string', 'content_en', NULL, NULL, NULL, '<!-- Add code here that should appear in the content block of all new pages -->', NULL, '2015-09-11 23:33:44');
INSERT INTO `cms_content_props` VALUES (70, 'string', 'searchable', NULL, NULL, NULL, '1', NULL, '2015-09-11 23:33:44');
INSERT INTO `cms_content_props` VALUES (70, 'string', 'extra1', NULL, NULL, NULL, '', NULL, '2015-09-11 23:33:44');
INSERT INTO `cms_content_props` VALUES (70, 'string', 'extra2', NULL, NULL, NULL, '', NULL, '2015-09-11 23:33:44');
INSERT INTO `cms_content_props` VALUES (70, 'string', 'extra3', NULL, NULL, NULL, '', NULL, '2015-09-11 23:33:44');
INSERT INTO `cms_content_props` VALUES (71, 'string', 'content_en', NULL, NULL, NULL, '<!-- Add code here that should appear in the content block of all new pages -->', NULL, '2015-09-15 15:18:01');
INSERT INTO `cms_content_props` VALUES (71, 'string', 'searchable', NULL, NULL, NULL, '1', NULL, '2015-09-15 15:18:01');
INSERT INTO `cms_content_props` VALUES (71, 'string', 'extra1', NULL, NULL, NULL, '', NULL, '2015-09-12 00:56:33');
INSERT INTO `cms_content_props` VALUES (71, 'string', 'extra2', NULL, NULL, NULL, '', NULL, '2015-09-12 00:56:33');
INSERT INTO `cms_content_props` VALUES (71, 'string', 'extra3', NULL, NULL, NULL, '', NULL, '2015-09-12 00:56:33');
INSERT INTO `cms_content_props` VALUES (71, 'string', 'pagedata', NULL, NULL, NULL, '', NULL, '2015-09-15 15:18:01');
INSERT INTO `cms_content_props` VALUES (71, 'string', 'disable_wysiwyg', NULL, NULL, NULL, '0', NULL, '2015-09-15 15:18:01');
INSERT INTO `cms_content_props` VALUES (71, 'string', 'target', NULL, NULL, NULL, '', NULL, '2015-09-15 15:18:01');
INSERT INTO `cms_content_props` VALUES (72, 'string', 'content_en', NULL, NULL, NULL, '{vipaction fullname=$smarty.post.fullname phone=$smarty.post.phone sex=$smarty.post.sex address=$smarty.post.address birthday=$smarty.post.birthday}', NULL, '2019-04-24 14:35:37');
INSERT INTO `cms_content_props` VALUES (72, 'string', 'searchable', NULL, NULL, NULL, '1', NULL, '2019-04-24 14:35:37');
INSERT INTO `cms_content_props` VALUES (72, 'string', 'extra1', NULL, NULL, NULL, '', NULL, '2015-09-12 01:05:00');
INSERT INTO `cms_content_props` VALUES (72, 'string', 'extra2', NULL, NULL, NULL, '', NULL, '2015-09-12 01:05:00');
INSERT INTO `cms_content_props` VALUES (72, 'string', 'extra3', NULL, NULL, NULL, '', NULL, '2015-09-12 01:05:00');
INSERT INTO `cms_content_props` VALUES (72, 'string', 'pagedata', NULL, NULL, NULL, '', NULL, '2019-04-24 14:35:37');
INSERT INTO `cms_content_props` VALUES (72, 'string', 'disable_wysiwyg', NULL, NULL, NULL, '1', NULL, '2019-04-24 14:35:37');
INSERT INTO `cms_content_props` VALUES (72, 'string', 'target', NULL, NULL, NULL, '', NULL, '2019-04-24 14:35:37');
INSERT INTO `cms_content_props` VALUES (60, 'string', 'url', NULL, NULL, NULL, 'http://www.khoanganxuavanay.com/#hungry-home', NULL, '2015-09-18 10:20:03');
INSERT INTO `cms_content_props` VALUES (73, 'string', 'content_en', NULL, NULL, NULL, '{cms_module module=\"vipmember\" action=\"search\" what=\"items\" listtemplate=\"list_default\"}', NULL, '2019-04-24 14:28:31');
INSERT INTO `cms_content_props` VALUES (73, 'string', 'searchable', NULL, NULL, NULL, '1', NULL, '2019-04-24 14:28:31');
INSERT INTO `cms_content_props` VALUES (73, 'string', 'extra1', NULL, NULL, NULL, '', NULL, '2015-09-18 14:06:19');
INSERT INTO `cms_content_props` VALUES (73, 'string', 'extra2', NULL, NULL, NULL, '', NULL, '2015-09-18 14:06:19');
INSERT INTO `cms_content_props` VALUES (73, 'string', 'extra3', NULL, NULL, NULL, '', NULL, '2015-09-18 14:06:19');
INSERT INTO `cms_content_props` VALUES (73, 'string', 'pagedata', NULL, NULL, NULL, '', NULL, '2019-04-24 14:28:31');
INSERT INTO `cms_content_props` VALUES (73, 'string', 'disable_wysiwyg', NULL, NULL, NULL, '0', NULL, '2019-04-24 14:28:31');
INSERT INTO `cms_content_props` VALUES (73, 'string', 'target', NULL, NULL, NULL, '', NULL, '2019-04-24 14:28:31');
INSERT INTO `cms_content_props` VALUES (74, 'string', 'content_en', NULL, NULL, NULL, '{do_reservations res_name=$smarty.post.res_name res_email=$smarty.post.res_email res_phone=$smarty.post.res_phone res_amount=$smarty.post.res_amount res_date=$smarty.post.res_date res_time=$smarty.post.res_time res_message=$smarty.post.res_message}', NULL, '2019-04-24 16:24:21');
INSERT INTO `cms_content_props` VALUES (74, 'string', 'searchable', NULL, NULL, NULL, '1', NULL, '2019-04-24 16:24:21');
INSERT INTO `cms_content_props` VALUES (74, 'string', 'extra1', NULL, NULL, NULL, '', NULL, '2019-04-24 15:14:26');
INSERT INTO `cms_content_props` VALUES (74, 'string', 'extra2', NULL, NULL, NULL, '', NULL, '2019-04-24 15:14:26');
INSERT INTO `cms_content_props` VALUES (74, 'string', 'extra3', NULL, NULL, NULL, '', NULL, '2019-04-24 15:14:26');
INSERT INTO `cms_content_props` VALUES (74, 'string', 'pagedata', NULL, NULL, NULL, '', NULL, '2019-04-24 16:24:21');
INSERT INTO `cms_content_props` VALUES (74, 'string', 'disable_wysiwyg', NULL, NULL, NULL, '1', NULL, '2019-04-24 16:24:21');
INSERT INTO `cms_content_props` VALUES (74, 'string', 'target', NULL, NULL, NULL, '', NULL, '2019-04-24 16:24:21');

-- ----------------------------
-- Table structure for cms_content_props_seq
-- ----------------------------
DROP TABLE IF EXISTS `cms_content_props_seq`;
CREATE TABLE `cms_content_props_seq`  (
  `id` int(11) NOT NULL
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = FIXED;

-- ----------------------------
-- Records of cms_content_props_seq
-- ----------------------------
INSERT INTO `cms_content_props_seq` VALUES (56);

-- ----------------------------
-- Table structure for cms_content_seq
-- ----------------------------
DROP TABLE IF EXISTS `cms_content_seq`;
CREATE TABLE `cms_content_seq`  (
  `id` int(11) NOT NULL
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = FIXED;

-- ----------------------------
-- Records of cms_content_seq
-- ----------------------------
INSERT INTO `cms_content_seq` VALUES (74);

-- ----------------------------
-- Table structure for cms_crossref
-- ----------------------------
DROP TABLE IF EXISTS `cms_crossref`;
CREATE TABLE `cms_crossref`  (
  `child_type` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `child_id` int(11) NULL DEFAULT NULL,
  `parent_type` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `parent_id` int(11) NULL DEFAULT NULL,
  `create_date` datetime NULL DEFAULT NULL,
  `modified_date` datetime NULL DEFAULT NULL,
  INDEX `cms_index_crossref_by_child_type_child_id`(`child_type`, `child_id`) USING BTREE,
  INDEX `cms_index_crossref_by_parent_type_parent_id`(`parent_type`, `parent_id`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of cms_crossref
-- ----------------------------
INSERT INTO `cms_crossref` VALUES ('global_content', 1, 'template', 15, '2009-05-10 16:57:24', '2009-05-10 16:57:24');
INSERT INTO `cms_crossref` VALUES ('global_content', 1, 'template', 16, '2009-05-09 17:04:30', '2009-05-09 17:04:30');
INSERT INTO `cms_crossref` VALUES ('global_content', 1, 'template', 20, '2009-05-09 23:57:31', '2009-05-09 23:57:31');
INSERT INTO `cms_crossref` VALUES ('global_content', 1, 'template', 18, '2009-05-09 17:19:20', '2009-05-09 17:19:20');
INSERT INTO `cms_crossref` VALUES ('global_content', 1, 'template', 17, '2009-05-09 21:20:18', '2009-05-09 21:20:18');
INSERT INTO `cms_crossref` VALUES ('global_content', 1, 'template', 21, '2009-05-10 16:59:13', '2009-05-10 16:59:13');
INSERT INTO `cms_crossref` VALUES ('global_content', 1, 'template', 22, '2009-05-11 02:01:23', '2009-05-11 02:01:23');

-- ----------------------------
-- Table structure for cms_css
-- ----------------------------
DROP TABLE IF EXISTS `cms_css`;
CREATE TABLE `cms_css`  (
  `css_id` int(11) NOT NULL,
  `css_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `css_text` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `media_type` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `media_query` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `create_date` datetime NULL DEFAULT NULL,
  `modified_date` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`css_id`) USING BTREE,
  INDEX `cms_index_css_by_css_name`(`css_name`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of cms_css
-- ----------------------------
INSERT INTO `cms_css` VALUES (30, 'Print', '/*\r\nSections that are hidden when printing the page. We only want the content printed.\r\n*/\r\n\r\n\r\nbody {\r\ncolor: #000 !important; /* we want everything in black */\r\nbackground-color:#fff !important; /* on white background */\r\nfont-family:arial; /* arial is nice to read ;) */\r\nborder:0 !important; /* no borders thanks */\r\n}\r\n\r\n/* This affects every tag */\r\n* {\r\nborder:0 !important; /* again no borders on printouts */\r\n}\r\n\r\n/* \r\nno need for accessibility on printout. \r\nMark all your elements in content you \r\ndont want to get printed with class=\"noprint\"\r\n*/\r\n.accessibility,\r\n.noprint\r\n {\r\ndisplay:none !important; \r\n}\r\n\r\n/* \r\nremove all width constraints from content area\r\n*/\r\ndiv#content,\r\ndiv#main {\r\ndisplay:block !important;\r\nwidth:100% !important;\r\nborder:0 !important;\r\npadding:1em !important;\r\n}\r\n\r\n/* hide everything else! */\r\ndiv#header,\r\ndiv#header h1 a,\r\ndiv.breadcrumbs,\r\ndiv#search,\r\ndiv#footer,\r\ndiv#menu_vert,\r\ndiv#news,\r\ndiv.right49,\r\ndiv.left49,\r\ndiv#sidebar  {\r\n   display: none !important;\r\n}\r\n\r\nimg {\r\nfloat:none; /* this makes images couse a pagebreak if it doesnt fit on the page */\r\n}', 'print', '', '2006-07-25 21:22:32', '2006-07-25 21:22:32');
INSERT INTO `cms_css` VALUES (54, 'Event', 'html, body, div, span, applet, object, iframe,\r\nh1, h2, h3, h4, h5, h6, p, blockquote, pre,\r\na, abbr, acronym, address, big, cite, code,\r\ndel, dfn, em, img, ins, kbd, q, s, samp,\r\nsmall, strike, strong, sub, sup, tt, var,\r\nb, u, i, center,\r\ndl, dt, dd, ol, ul, li,\r\nfieldset, form, label, legend,\r\ntable, caption, tbody, tfoot, thead, tr, th, td,\r\narticle, aside, canvas, details, embed,\r\nfigure, figcaption, footer, header, hgroup,\r\nmenu, nav, output, ruby, section, summary,\r\ntime, mark, audio, video {\r\n  margin: 0;\r\n  padding: 0;\r\n  border: 0;\r\n  font-size: 100%;\r\n  font: inherit;\r\n  vertical-align: baseline;\r\n}\r\n\r\narticle, aside, details, figcaption, figure,\r\nfooter, header, hgroup, menu, nav, section {\r\n  display: block;\r\n}\r\n\r\nbody {\r\n  line-height: 1;\r\n}\r\n\r\nol, ul {\r\n  list-style: none;\r\n}\r\n\r\nblockquote, q {\r\n  quotes: none;\r\n}\r\n\r\nblockquote:before, blockquote:after,\r\nq:before, q:after {\r\n  content: \'\';\r\n  content: none;\r\n}\r\n\r\ntable {\r\n  border-collapse: collapse;\r\n  border-spacing: 0;\r\n}\r\n\r\nbody {\r\n  font: 14px/20px \'Helvetica Neue\', Helvetica, Arial, sans-serif;\r\n  color: #404040;\r\n  background: #2d4259;\r\n}\r\n\r\n.register-title {\r\n  width: 70%;\r\n  line-height: 43px;\r\n  margin: 50px auto 20px;\r\n  font-size: 19px;\r\n  font-weight: 500;\r\n  color: white;\r\n  color: rgba(255, 255, 255, 0.95);\r\n  text-align: center;\r\n  text-shadow: 0 1px rgba(0, 0, 0, 0.3);\r\n  background: #d7604b;\r\n  border-radius: 3px;\r\n  background-image: -webkit-linear-gradient(top, #dc745e, #d45742);\r\n  background-image: -moz-linear-gradient(top, #dc745e, #d45742);\r\n  background-image: -o-linear-gradient(top, #dc745e, #d45742);\r\n  background-image: linear-gradient(to bottom, #dc745e, #d45742);\r\n  -webkit-box-shadow: inset 0 1px rgba(255, 255, 255, 0.1), inset 0 0 0 1px rgba(255, 255, 255, 0.05), 0 0 1px 1px rgba(0, 0, 0, 0.1), 0 1px 3px rgba(0, 0, 0, 0.3);\r\n  box-shadow: inset 0 1px rgba(255, 255, 255, 0.1), inset 0 0 0 1px rgba(255, 255, 255, 0.05), 0 0 1px 1px rgba(0, 0, 0, 0.1), 0 1px 3px rgba(0, 0, 0, 0.3);\r\n}\r\n.question\r\n{\r\nmargin-bottom:20px;\r\n\r\n}\r\n.register {\r\n  margin: 0 auto;\r\n  width: 70%;\r\n  padding: 20px;\r\n  background: #f4f4f4;\r\n  border-radius: 3px;\r\n  -webkit-box-shadow: 0 0 1px 1px rgba(0, 0, 0, 0.1), 0 1px 3px rgba(0, 0, 0, 0.3);\r\n  box-shadow: 0 0 1px 1px rgba(0, 0, 0, 0.1), 0 1px 3px rgba(0, 0, 0, 0.3);\r\n}\r\n\r\ninput {\r\n  font-family: inherit;\r\n  font-size: inherit;\r\n  color: inherit;\r\n  -webkit-box-sizing: border-box;\r\n  -moz-box-sizing: border-box;\r\n  box-sizing: border-box;\r\n}\r\n\r\n.register-input {\r\n  display: block;\r\n  width: 100%;\r\n  height: 38px;\r\n  margin-top: 2px;\r\n  font-weight: 500;\r\n  background: none;\r\n  border: 0;\r\n  border-bottom: 1px solid #d8d8d8;\r\n}\r\n.register-input:focus {\r\n  border-color: #1e9ce6;\r\n  outline: 0;\r\n}\r\n\r\n.register-button {\r\n  display: block;\r\n  width: 100%;\r\n  height: 42px;\r\n  margin-top: 25px;\r\n  font-size: 13px;\r\n  font-weight: bold;\r\n  color: #494d59;\r\n  text-align: center;\r\n  text-shadow: 0 1px rgba(255, 255, 255, 0.5);\r\n  background: #fcfcfc;\r\n  border: 1px solid;\r\n  border-color: #d8d8d8 #d1d1d1 #c3c3c3;\r\n  border-radius: 2px;\r\n  cursor: pointer;\r\n  background-image: -webkit-linear-gradient(top, #fefefe, #eeeeee);\r\n  background-image: -moz-linear-gradient(top, #fefefe, #eeeeee);\r\n  background-image: -o-linear-gradient(top, #fefefe, #eeeeee);\r\n  background-image: linear-gradient(to bottom, #fefefe, #eeeeee);\r\n  -webkit-box-shadow: inset 0 -1px rgba(0, 0, 0, 0.03), 0 1px rgba(0, 0, 0, 0.04);\r\n  box-shadow: inset 0 -1px rgba(0, 0, 0, 0.03), 0 1px rgba(0, 0, 0, 0.04);\r\n}\r\n.register-button:active {\r\n  background: #eee;\r\n  border-color: #c3c3c3 #d1d1d1 #d8d8d8;\r\n  background-image: -webkit-linear-gradient(top, #eeeeee, #fcfcfc);\r\n  background-image: -moz-linear-gradient(top, #eeeeee, #fcfcfc);\r\n  background-image: -o-linear-gradient(top, #eeeeee, #fcfcfc);\r\n  background-image: linear-gradient(to bottom, #eeeeee, #fcfcfc);\r\n  -webkit-box-shadow: inset 0 1px rgba(0, 0, 0, 0.03);\r\n  box-shadow: inset 0 1px rgba(0, 0, 0, 0.03);\r\n}\r\n.register-button:focus {\r\n  outline: 0;\r\n}\r\n\r\n.register-switch {\r\nmargin-top: 15px;\r\n  height: 32px;\r\n  margin-bottom: 15px;\r\n  padding: 4px;\r\n  background: #6db244;\r\n  border-radius: 2px;\r\n  background-image: -webkit-linear-gradient(top, #60a83a, #7dbe52);\r\n  background-image: -moz-linear-gradient(top, #60a83a, #7dbe52);\r\n  background-image: -o-linear-gradient(top, #60a83a, #7dbe52);\r\n  background-image: linear-gradient(to bottom, #60a83a, #7dbe52);\r\n  -webkit-box-shadow: inset 0 1px rgba(0, 0, 0, 0.05), inset 1px 0 rgba(0, 0, 0, 0.02), inset -1px 0 rgba(0, 0, 0, 0.02);\r\n  box-shadow: inset 0 1px rgba(0, 0, 0, 0.05), inset 1px 0 rgba(0, 0, 0, 0.02), inset -1px 0 rgba(0, 0, 0, 0.02);\r\n}\r\n\r\n.register-switch-input {\r\n  display: none;\r\n}\r\n\r\n.register-switch-label {\r\n  float: left;\r\n  width: 50%;\r\n  line-height: 32px;\r\n  color: white;\r\n  text-align: center;\r\n  text-shadow: 0 -1px rgba(0, 0, 0, 0.2);\r\n  cursor: pointer;\r\n}\r\n.register-switch-input:checked + .register-switch-label {\r\n  font-weight: 500;\r\n  color: #434248;\r\n  text-shadow: 0 1px rgba(255, 255, 255, 0.5);\r\n  background: white;\r\n  border-radius: 2px;\r\n  background-image: -webkit-linear-gradient(top, #fefefe, #eeeeee);\r\n  background-image: -moz-linear-gradient(top, #fefefe, #eeeeee);\r\n  background-image: -o-linear-gradient(top, #fefefe, #eeeeee);\r\n  background-image: linear-gradient(to bottom, #fefefe, #eeeeee);\r\n  -webkit-box-shadow: 0 1px 1px rgba(0, 0, 0, 0.15), 0 0 0 1px rgba(0, 0, 0, 0.1);\r\n  box-shadow: 0 1px 1px rgba(0, 0, 0, 0.15), 0 0 0 1px rgba(0, 0, 0, 0.1);\r\n}\r\n\r\n:-moz-placeholder {\r\n  color: #aaa;\r\n  font-weight: 300;\r\n}\r\n\r\n::-moz-placeholder {\r\n  color: #aaa;\r\n  font-weight: 300;\r\n  opacity: 1;\r\n}\r\n\r\n::-webkit-input-placeholder {\r\n  color: #aaa;\r\n  font-weight: 300;\r\n}\r\n\r\n:-ms-input-placeholder {\r\n  color: #aaa;\r\n  font-weight: 300;\r\n}\r\n\r\n::-moz-focus-inner {\r\n  border: 0;\r\n  padding: 0;\r\n}\r\n', '', '', '2015-09-11 23:47:33', '2015-09-12 00:14:11');
INSERT INTO `cms_css` VALUES (55, 'Event-2', 'html, body, div, span, applet, object, iframe,\r\nh1, h2, h3, h4, h5, h6, p, blockquote, pre,\r\na, abbr, acronym, address, big, cite, code,\r\ndel, dfn, em, img, ins, kbd, q, s, samp,\r\nsmall, strike, strong, sub, sup, tt, var,\r\nb, u, i, center,\r\ndl, dt, dd, ol, ul, li,\r\nfieldset, form, label, legend,\r\ntable, caption, tbody, tfoot, thead, tr, th, td,\r\narticle, aside, canvas, details, embed,\r\nfigure, figcaption, footer, header, hgroup,\r\nmenu, nav, output, ruby, section, summary,\r\ntime, mark, audio, video {\r\n  margin: 0;\r\n  padding: 0;\r\n  border: 0;\r\n  font-size: 100%;\r\n  font: inherit;\r\n  vertical-align: baseline;\r\n}\r\n\r\narticle, aside, details, figcaption, figure,\r\nfooter, header, hgroup, menu, nav, section {\r\n  display: block;\r\n}\r\n\r\nbody {\r\n  line-height: 1;\r\n}\r\n\r\nol, ul {\r\n  list-style: none;\r\n}\r\n\r\nblockquote, q {\r\n  quotes: none;\r\n}\r\n\r\nblockquote:before, blockquote:after,\r\nq:before, q:after {\r\n  content: \'\';\r\n  content: none;\r\n}\r\n\r\ntable {\r\n  border-collapse: collapse;\r\n  border-spacing: 0;\r\n}\r\n\r\n.about {\r\n  margin: 10px auto 10px;\r\n  padding: 8px;\r\n  width: 300px;\r\n  font: 10px/18px \'Lucida Grande\', Arial, sans-serif;\r\n  color: #666;\r\n  text-align: center;\r\n  text-shadow: 0 1px rgba(255, 255, 255, 0.25);\r\n  background: #eee;\r\n  background: rgba(250, 250, 250, 0.8);\r\n  border-radius: 4px;\r\n  background-image: -webkit-linear-gradient(top, rgba(0, 0, 0, 0), rgba(0, 0, 0, 0.1));\r\n  background-image: -moz-linear-gradient(top, rgba(0, 0, 0, 0), rgba(0, 0, 0, 0.1));\r\n  background-image: -o-linear-gradient(top, rgba(0, 0, 0, 0), rgba(0, 0, 0, 0.1));\r\n  background-image: linear-gradient(to bottom, rgba(0, 0, 0, 0), rgba(0, 0, 0, 0.1));\r\n  -webkit-box-shadow: inset 0 1px rgba(255, 255, 255, 0.3), inset 0 0 0 1px rgba(255, 255, 255, 0.1), 0 0 6px rgba(0, 0, 0, 0.2);\r\n  box-shadow: inset 0 1px rgba(255, 255, 255, 0.3), inset 0 0 0 1px rgba(255, 255, 255, 0.1), 0 0 6px rgba(0, 0, 0, 0.2);\r\n}\r\n.about a {\r\n  color: #333;\r\n  text-decoration: none;\r\n  border-radius: 2px;\r\n  -webkit-transition: background 0.1s;\r\n  -moz-transition: background 0.1s;\r\n  -o-transition: background 0.1s;\r\n  transition: background 0.1s;\r\n}\r\n.about a:hover {\r\n  text-decoration: none;\r\n  background: #fafafa;\r\n  background: rgba(255, 255, 255, 0.7);\r\n}\r\n\r\n.about-links {\r\n  height: 30px;\r\n}\r\n.about-links > a {\r\n  float: left;\r\n  width: 50%;\r\n  line-height: 30px;\r\n  font-size: 12px;\r\n}\r\n\r\n.about-author {\r\n  margin-top: 5px;\r\n}\r\n.about-author > a {\r\n  padding: 1px 3px;\r\n  margin: 0 -1px;\r\n}\r\n\r\n/*\r\n * Copyright (c) 2013 Thibaut Courouble\r\n * http://www.cssflow.com\r\n *\r\n * Licensed under the MIT License:\r\n * http://www.opensource.org/licenses/mit-license.php\r\n */\r\nbody {\r\n  font: 13px/20px \'Helvetica Neue\', Helvetica, Arial, sans-serif;\r\n  color: #404040;\r\n  background: #596778;\r\n}\r\n.sign-up {\r\n  position: relative;\r\n  margin: 20px auto;\r\n  width: 80%;\r\n  padding: 33px 25px 29px;\r\n  background: white;\r\n  border-bottom: 1px solid #c4c4c4;\r\n  border-radius: 5px;\r\n  -webkit-box-shadow: 0 1px 5px rgba(0, 0, 0, 0.25);\r\n  box-shadow: 0 1px 5px rgba(0, 0, 0, 0.25);\r\n}\r\n.sign-up:before, .sign-up:after {\r\n  content: \'\';\r\n  position: absolute;\r\n  bottom: 1px;\r\n  left: 0;\r\n  right: 0;\r\n  height: 10px;\r\n  background: inherit;\r\n  border-bottom: 1px solid #d2d2d2;\r\n  border-radius: 4px;\r\n}\r\n.sign-up:after {\r\n  bottom: 3px;\r\n  border-color: #dcdcdc;\r\n}\r\n\r\n.sign-up-title {\r\n  margin: -25px -25px 25px;\r\n  padding: 15px 25px;\r\n  line-height: 35px;\r\n  font-size: 26px;\r\n  font-weight: 400;\r\n  color: #aaa;\r\n  text-align: center;\r\n  text-shadow: 0 1px rgba(255, 255, 255, 1);\r\n  background: #F1F1F1;\r\n}\r\n.sign-up-title span {font-size:70%;}\r\n.sign-up-title:before {\r\n  content: \'\';\r\n  position: absolute;\r\n  top: 0;\r\n  left: 0;\r\n  right: 0;\r\n  height: 8px;\r\n  background: #c4e17f;\r\n  border-radius: 5px 5px 0 0;\r\n  background-image: -webkit-linear-gradient(left, #c4e17f, #c4e17f 12.5%, #f7fdca 12.5%, #f7fdca 25%, #fecf71 25%, #fecf71 37.5%, #f0776c 37.5%, #f0776c 50%, #db9dbe 50%, #db9dbe 62.5%, #c49cde 62.5%, #c49cde 75%, #669ae1 75%, #669ae1 87.5%, #62c2e4 87.5%, #62c2e4);\r\n  background-image: -moz-linear-gradient(left, #c4e17f, #c4e17f 12.5%, #f7fdca 12.5%, #f7fdca 25%, #fecf71 25%, #fecf71 37.5%, #f0776c 37.5%, #f0776c 50%, #db9dbe 50%, #db9dbe 62.5%, #c49cde 62.5%, #c49cde 75%, #669ae1 75%, #669ae1 87.5%, #62c2e4 87.5%, #62c2e4);\r\n  background-image: -o-linear-gradient(left, #c4e17f, #c4e17f 12.5%, #f7fdca 12.5%, #f7fdca 25%, #fecf71 25%, #fecf71 37.5%, #f0776c 37.5%, #f0776c 50%, #db9dbe 50%, #db9dbe 62.5%, #c49cde 62.5%, #c49cde 75%, #669ae1 75%, #669ae1 87.5%, #62c2e4 87.5%, #62c2e4);\r\n  background-image: linear-gradient(to right, #c4e17f, #c4e17f 12.5%, #f7fdca 12.5%, #f7fdca 25%, #fecf71 25%, #fecf71 37.5%, #f0776c 37.5%, #f0776c 50%, #db9dbe 50%, #db9dbe 62.5%, #c49cde 62.5%, #c49cde 75%, #669ae1 75%, #669ae1 87.5%, #62c2e4 87.5%, #62c2e4);\r\n}\r\n\r\ninput {\r\n  font-family: inherit;\r\n  color: inherit;\r\n  -webkit-box-sizing: border-box;\r\n  -moz-box-sizing: border-box;\r\n  box-sizing: border-box;\r\n}\r\n\r\n.sign-up-input {\r\n  width: 100%;\r\n  height: 50px;\r\n  margin-bottom: 25px;\r\n  padding: 0 15px 2px;\r\n  font-size: 17px;\r\n  background: white;\r\n  border: 2px solid #ebebeb;\r\n  border-radius: 4px;\r\n  -webkit-box-shadow: inset 0 -2px #ebebeb;\r\n  box-shadow: inset 0 -2px #ebebeb;\r\n}\r\n.sign-up-input:focus {\r\n  border-color: #62c2e4;\r\n  outline: none;\r\n  -webkit-box-shadow: inset 0 -2px #62c2e4;\r\n  box-shadow: inset 0 -2px #62c2e4;\r\n}\r\n.lt-ie9 .sign-up-input {\r\n  line-height: 48px;\r\n}\r\n\r\n.sign-up-button {\r\n  position: relative;\r\n  vertical-align: top;\r\n  width: 100%;\r\n  height: 54px;\r\n  padding: 0;\r\n  font-size: 22px;\r\n  color: white;\r\n  text-align: center;\r\n  text-shadow: 0 1px 2px rgba(0, 0, 0, 0.25);\r\n  background: #f0776c;\r\n  border: 0;\r\n  border-bottom: 2px solid #d76b60;\r\n  border-radius: 5px;\r\n  cursor: pointer;\r\n  -webkit-box-shadow: inset 0 -2px #d76b60;\r\n  box-shadow: inset 0 -2px #d76b60;\r\n}\r\n.sign-up-button:active {\r\n  top: 1px;\r\n  outline: none;\r\n  -webkit-box-shadow: none;\r\n  box-shadow: none;\r\n}\r\n\r\n:-moz-placeholder {\r\n  color: #ccc;\r\n  font-weight: 300;\r\n}\r\n\r\n::-moz-placeholder {\r\n  color: #ccc;\r\n  opacity: 1;\r\n  font-weight: 300;\r\n}\r\n\r\n::-webkit-input-placeholder {\r\n  color: #ccc;\r\n  font-weight: 300;\r\n}\r\n\r\n:-ms-input-placeholder {\r\n  color: #ccc;\r\n  font-weight: 300;\r\n}\r\n\r\n::-moz-focus-inner {\r\n  border: 0;\r\n  padding: 0;\r\n}\r\n', '', '', '2015-09-15 22:07:05', '2015-09-16 10:21:30');
INSERT INTO `cms_css` VALUES (56, 'Home', '.modal {\r\n    position: fixed;\r\n    top: 0;\r\n    right: 0;\r\n    bottom: 0;\r\n    left: 0;\r\n    z-index: 1050;\r\n    display: none;\r\n    overflow: hidden;\r\n    -webkit-overflow-scrolling: touch;\r\n    outline: 0\r\n}\r\n\r\n.modal.fade .modal-dialog {\r\n    -webkit-transition: -webkit-transform .3s ease-out;\r\n    -o-transition: -o-transform .3s ease-out;\r\n    transition: transform .3s ease-out;\r\n    -webkit-transform: translate(0, -60%);\r\n    -ms-transform: translate(0, -60%);\r\n    -o-transform: translate(0, -60%);\r\n    transform: translate(0, -60%)\r\n}\r\n\r\n.modal.in .modal-dialog {\r\n    -webkit-transform: translate(0, 0);\r\n    -ms-transform: translate(0, 0);\r\n    -o-transform: translate(0, 0);\r\n    transform: translate(0, 0)\r\n}\r\n\r\n.modal-open .modal {\r\n    overflow-x: hidden;\r\n    < !--overflow-y: auto-->\r\n}\r\n\r\n.modal-dialog {\r\n    position: relative;\r\n    width: auto;\r\n    margin: 10px\r\n}\r\n\r\n.modal-content {\r\n    position: relative;\r\n    background-color: #fff;\r\n    -webkit-background-clip: padding-box;\r\n    background-clip: padding-box;\r\n    border: 1px solid #999;\r\n    border: 1px solid rgba(0, 0, 0, .2);\r\n    border-radius: 6px;\r\n    outline: 0;\r\n    -webkit-box-shadow: 0 3px 9px rgba(0, 0, 0, .5);\r\n    box-shadow: 0 3px 9px rgba(0, 0, 0, .5)\r\n}\r\n\r\n.modal-backdrop {\r\n    position: fixed;\r\n    top: 0;\r\n    right: 0;\r\n    bottom: 0;\r\n    left: 0;\r\n    z-index: 1040;\r\n    background-color: #000\r\n}\r\n\r\n.modal-backdrop.fade {\r\n    filter: alpha(opacity=0);\r\n    opacity: 0\r\n}\r\n\r\n.modal-backdrop.in {\r\n    filter: alpha(opacity=50);\r\n    opacity: .5\r\n}\r\n\r\n.modal-header {\r\n    min-height: 16.43px;\r\n    padding: 15px;\r\n    border-bottom: 1px solid #e5e5e5\r\n}\r\n\r\n.modal-header .close {\r\n    margin-top: -2px\r\n}\r\n\r\n.modal-title {\r\n    margin: 0;\r\n    line-height: 1.42857143\r\n}\r\n\r\n.modal-body {\r\n    position: relative;\r\n    padding: 5px\r\n}\r\n\r\n.modal-footer {\r\n    padding: 15px;\r\n    text-align: right;\r\n    border-top: 1px solid #e5e5e5\r\n}\r\n\r\n.modal-footer .btn+.btn {\r\n    margin-bottom: 0;\r\n    margin-left: 5px\r\n}\r\n\r\n.modal-footer .btn-group .btn+.btn {\r\n    margin-left: -1px\r\n}\r\n\r\n.modal-footer .btn-block+.btn-block {\r\n    margin-left: 0\r\n}\r\n\r\n.modal-scrollbar-measure {\r\n    position: absolute;\r\n    top: -9999px;\r\n    width: 50px;\r\n    height: 50px;\r\n    overflow: scroll\r\n}\r\n\r\n@media (min-width:768px) {\r\n    .modal-dialog {\r\n        width: 600px;\r\n        margin: 100px auto\r\n    }\r\n\r\n    .modal-content {\r\n        -webkit-box-shadow: 0 5px 15px rgba(0, 0, 0, .5);\r\n        box-shadow: 0 5px 15px rgba(0, 0, 0, .5)\r\n    }\r\n\r\n    .modal-sm {\r\n        width: 300px\r\n    }\r\n\r\n\r\n}\r\n\r\n@media (min-width:992px) {\r\n    .modal-lg {\r\n        width: 900px\r\n    }\r\n\r\n    .hungry-menu-item #mnleft {\r\n        float: left !important;\r\n    }\r\n\r\n    .hungry-menu-item #mnright {\r\n        padding-top: 1rem !important;\r\n    }\r\n}\r\n\r\n@media screen and (max-width: 767px) {\r\n\r\n    .hungry-menu-item-thumbnail {\r\n        width: 100% !important;\r\n    }\r\n\r\n    #hungry-about-us {\r\n        padding: 40px 0 0 0 !important;\r\n    }\r\n\r\n    .tablet-grid-100,\r\n    .mobile-grid-100 {\r\n        margin-bottom: 0px !important;\r\n    }\r\n\r\n    #hungry-testimonials {\r\n        padding: 40px 0 0px 0;\r\n    }\r\n\r\n    .section-heading-subtitle {\r\n        font-size: 12px !important;\r\n    }\r\n\r\n    .hungry-menu-item {\r\n        margin-bottom: 0px !important;\r\n    }\r\n\r\n    .hungry-menu {\r\n        margin-bottom: 0px !important;\r\n    }\r\n\r\n    #hungry-gallery,\r\n    #hungry-slogan-01,\r\n    #hungry-slogan-02 {\r\n        padding: 30px 0;\r\n    }\r\n\r\n    #hungry-promotion {\r\n        padding: 50px 0;\r\n    }\r\n\r\n    #hungry-customer {\r\n        padding: 40px 0 0px 0;\r\n    }\r\n\r\n    .header-divider {\r\n        padding-top: 2rem;\r\n    }\r\n\r\n    .hungry-blog-meta {\r\n        float: none;\r\n        padding: 20px 0;\r\n    }\r\n\r\n    .hungry-blog-container {\r\n        margin-bottom: 0px;\r\n    }\r\n\r\n    #hungry-reservations {\r\n        padding: 40px 0 0px 0;\r\n    }\r\n\r\n\r\n    #hungry-contact {\r\n        padding: 40px 0 0px 0;\r\n    }\r\n\r\n    #site-footer {\r\n        padding-top: 20px;\r\n    }\r\n\r\n    .widget {\r\n        margin-bottom: 20px;\r\n    }\r\n\r\n}\r\n\r\n.hungry-menu-item-thumbnail {\r\n    width: 200px;\r\n    margin-top: 8px;\r\n    -webkit-border-radius: 5px;\r\n    -moz-border-radius: 5px;\r\n    border-radius: 5px;\r\n    -webkit-box-shadow: 0 0 0 5px #f1f1f1;\r\n    -moz-box-shadow: 0 0 0 5px #f1f1f1;\r\n    box-shadow: 0 0 0 5px #f1f1f1;\r\n}\r\n\r\n\r\n.hungry-thumbnail-overlay {\r\n    position: absolute;\r\n    top: 70px;\r\n    left: 60px;\r\n    width: 80px;\r\n    height: 56px;\r\n    padding-top: 24px;\r\n    background: rgba(0, 0, 0, 0.45);\r\n    text-align: center;\r\n    overflow: hidden;\r\n    opacity: 0;\r\n    -moz-opacity: 0;\r\n    -webkit-transform: scale(1.2);\r\n    -moz-transform: scale(1.2);\r\n    -ms-transform: scale(1.2);\r\n    -o-transform: scale(1.2);\r\n    transform: scale(1.2);\r\n    -webkit-border-radius: 80px;\r\n    -moz-border-radius: 80px;\r\n    border-radius: 80px;\r\n    -webkit-transition: all 0.3s ease;\r\n    -moz-transition: all 0.3s ease;\r\n    -ms-transition: all 0.3s ease;\r\n    -o-transition: all 0.3s ease;\r\n    transition: all 0.3s ease;\r\n}\r\n\r\n.hungry-menu-item-header {\r\n    z-index: -1;\r\n}\r\n\r\n.hungry-menu-item-title {\r\n    padding-left: 20px;\r\n}\r\n\r\n.hungry-menu-item-excerpt p {\r\n    float: right;\r\n}\r\n\r\n.hungry-menu-item {\r\n    font-family: arial;\r\n    margin-bottom: 20px;\r\n    position: relative;\r\n}\r\n\r\n\r\n\r\n.section-heading-alt-title span,\r\nem {\r\n    color: #fff !important;\r\n}\r\n\r\n.header-text-pre-slogan {\r\n    font-size: 45px;\r\n}\r\n\r\n.sf-menu a {\r\n    font-size: 13px;\r\n}\r\n\r\n#hungry-reservation-form input[type=\"submit\"] {\r\n    font-size: 1.2rem;\r\n    text-transform: uppercase;\r\n    font-weight: 300;\r\n}\r\n\r\n.section-heading-subtitle {\r\n    font-size: 18px;\r\n}\r\n', 'all', '', '2015-09-16 10:00:52', '2020-11-24 16:05:52');
INSERT INTO `cms_css` VALUES (57, 'Home_BK', '.modal {\r\n    position: fixed;\r\n    top: 0;\r\n    right: 0;\r\n    bottom: 0;\r\n    left: 0;\r\n    z-index: 1050;\r\n    display: none;\r\n    overflow: hidden;\r\n    -webkit-overflow-scrolling: touch;\r\n    outline: 0\r\n}\r\n\r\n.modal.fade .modal-dialog {\r\n    -webkit-transition: -webkit-transform .3s ease-out;\r\n    -o-transition: -o-transform .3s ease-out;\r\n    transition: transform .3s ease-out;\r\n    -webkit-transform: translate(0, -60%);\r\n    -ms-transform: translate(0, -60%);\r\n    -o-transform: translate(0, -60%);\r\n    transform: translate(0, -60%)\r\n}\r\n\r\n.modal.in .modal-dialog {\r\n    -webkit-transform: translate(0, 0);\r\n    -ms-transform: translate(0, 0);\r\n    -o-transform: translate(0, 0);\r\n    transform: translate(0, 0)\r\n}\r\n\r\n.modal-open .modal {\r\n    overflow-x: hidden;\r\n    < !--overflow-y: auto-->\r\n}\r\n\r\n.modal-dialog {\r\n    position: relative;\r\n    width: auto;\r\n    margin: 10px\r\n}\r\n\r\n.modal-content {\r\n    position: relative;\r\n    background-color: #fff;\r\n    -webkit-background-clip: padding-box;\r\n    background-clip: padding-box;\r\n    border: 1px solid #999;\r\n    border: 1px solid rgba(0, 0, 0, .2);\r\n    border-radius: 6px;\r\n    outline: 0;\r\n    -webkit-box-shadow: 0 3px 9px rgba(0, 0, 0, .5);\r\n    box-shadow: 0 3px 9px rgba(0, 0, 0, .5)\r\n}\r\n\r\n.modal-backdrop {\r\n    position: fixed;\r\n    top: 0;\r\n    right: 0;\r\n    bottom: 0;\r\n    left: 0;\r\n    z-index: 1040;\r\n    background-color: #000\r\n}\r\n\r\n.modal-backdrop.fade {\r\n    filter: alpha(opacity=0);\r\n    opacity: 0\r\n}\r\n\r\n.modal-backdrop.in {\r\n    filter: alpha(opacity=50);\r\n    opacity: .5\r\n}\r\n\r\n.modal-header {\r\n    min-height: 16.43px;\r\n    padding: 15px;\r\n    border-bottom: 1px solid #e5e5e5\r\n}\r\n\r\n.modal-header .close {\r\n    margin-top: -2px\r\n}\r\n\r\n.modal-title {\r\n    margin: 0;\r\n    line-height: 1.42857143\r\n}\r\n\r\n.modal-body {\r\n    position: relative;\r\n    padding: 5px\r\n}\r\n\r\n.modal-footer {\r\n    padding: 15px;\r\n    text-align: right;\r\n    border-top: 1px solid #e5e5e5\r\n}\r\n\r\n.modal-footer .btn+.btn {\r\n    margin-bottom: 0;\r\n    margin-left: 5px\r\n}\r\n\r\n.modal-footer .btn-group .btn+.btn {\r\n    margin-left: -1px\r\n}\r\n\r\n.modal-footer .btn-block+.btn-block {\r\n    margin-left: 0\r\n}\r\n\r\n.modal-scrollbar-measure {\r\n    position: absolute;\r\n    top: -9999px;\r\n    width: 50px;\r\n    height: 50px;\r\n    overflow: scroll\r\n}\r\n\r\n@media (min-width:768px) {\r\n    .modal-dialog {\r\n        width: 600px;\r\n        margin: 100px auto\r\n    }\r\n\r\n    .modal-content {\r\n        -webkit-box-shadow: 0 5px 15px rgba(0, 0, 0, .5);\r\n        box-shadow: 0 5px 15px rgba(0, 0, 0, .5)\r\n    }\r\n\r\n    .modal-sm {\r\n        width: 300px\r\n    }\r\n\r\n\r\n}\r\n\r\n@media (min-width:992px) {\r\n    .modal-lg {\r\n        width: 900px\r\n    }\r\n\r\n    .hungry-menu-item #mnleft {\r\n        float: left !important;\r\n    }\r\n\r\n    .hungry-menu-item #mnright {\r\n        padding-top: 1rem !important;\r\n    }\r\n}\r\n\r\n@media screen and (max-width: 767px) {\r\n\r\n    .hungry-menu-item-thumbnail {\r\n        width: 100% !important;\r\n    }\r\n\r\n    #hungry-about-us {\r\n        padding: 40px 0 0 0 !important;\r\n    }\r\n\r\n    .tablet-grid-100,\r\n    .mobile-grid-100 {\r\n        margin-bottom: 0px !important;\r\n    }\r\n\r\n    #hungry-testimonials {\r\n        padding: 40px 0 0px 0;\r\n    }\r\n\r\n    .section-heading-subtitle {\r\n        font-size: 12px !important;\r\n    }\r\n\r\n    .hungry-menu-item {\r\n        margin-bottom: 0px !important;\r\n    }\r\n\r\n    .hungry-menu {\r\n        margin-bottom: 0px !important;\r\n    }\r\n\r\n    #hungry-gallery,\r\n    #hungry-slogan-01,\r\n    #hungry-slogan-02 {\r\n        padding: 30px 0;\r\n    }\r\n\r\n    #hungry-promotion {\r\n        padding: 50px 0;\r\n    }\r\n\r\n    #hungry-customer {\r\n        padding: 40px 0 0px 0;\r\n    }\r\n\r\n    .header-divider {\r\n        padding-top: 2rem;\r\n    }\r\n\r\n    .hungry-blog-meta {\r\n        float: none;\r\n        padding: 20px 0;\r\n    }\r\n\r\n    .hungry-blog-container {\r\n        margin-bottom: 0px;\r\n    }\r\n\r\n    #hungry-reservations {\r\n        padding: 40px 0 0px 0;\r\n    }\r\n\r\n\r\n    #hungry-contact {\r\n        padding: 40px 0 0px 0;\r\n    }\r\n\r\n    #site-footer {\r\n        padding-top: 20px;\r\n    }\r\n\r\n    .widget {\r\n        margin-bottom: 20px;\r\n    }\r\n\r\n}\r\n\r\n.hungry-menu-item-thumbnail {\r\n    width: 200px;\r\n    margin-top: 8px;\r\n    -webkit-border-radius: 5px;\r\n    -moz-border-radius: 5px;\r\n    border-radius: 5px;\r\n    -webkit-box-shadow: 0 0 0 5px #f1f1f1;\r\n    -moz-box-shadow: 0 0 0 5px #f1f1f1;\r\n    box-shadow: 0 0 0 5px #f1f1f1;\r\n}\r\n\r\n\r\n.hungry-thumbnail-overlay {\r\n    position: absolute;\r\n    top: 70px;\r\n    left: 60px;\r\n    width: 80px;\r\n    height: 56px;\r\n    padding-top: 24px;\r\n    background: rgba(0, 0, 0, 0.45);\r\n    text-align: center;\r\n    overflow: hidden;\r\n    opacity: 0;\r\n    -moz-opacity: 0;\r\n    -webkit-transform: scale(1.2);\r\n    -moz-transform: scale(1.2);\r\n    -ms-transform: scale(1.2);\r\n    -o-transform: scale(1.2);\r\n    transform: scale(1.2);\r\n    -webkit-border-radius: 80px;\r\n    -moz-border-radius: 80px;\r\n    border-radius: 80px;\r\n    -webkit-transition: all 0.3s ease;\r\n    -moz-transition: all 0.3s ease;\r\n    -ms-transition: all 0.3s ease;\r\n    -o-transition: all 0.3s ease;\r\n    transition: all 0.3s ease;\r\n}\r\n\r\n.hungry-menu-item-header {\r\n    z-index: -1;\r\n}\r\n\r\n.hungry-menu-item-title {\r\n    padding-left: 20px;\r\n}\r\n\r\n.hungry-menu-item-excerpt p {\r\n    float: right;\r\n}\r\n\r\n.hungry-menu-item {\r\n    font-family: arial;\r\n    margin-bottom: 20px;\r\n    position: relative;\r\n}\r\n\r\n\r\n\r\n.section-heading-alt-title span,\r\nem {\r\n    color: #fff !important;\r\n}\r\n\r\n.header-text-pre-slogan {\r\n    font-size: 45px;\r\n}\r\n\r\n.sf-menu a {\r\n    font-size: 13px;\r\n}\r\n\r\n#hungry-reservation-form input[type=\"submit\"] {\r\n    font-size: 1.2rem;\r\n    text-transform: uppercase;\r\n    font-weight: 300;\r\n}\r\n\r\n.section-heading-subtitle {\r\n    font-size: 18px;\r\n}\r\n', 'all', '', '2020-11-24 16:06:05', '2020-11-24 16:06:05');

-- ----------------------------
-- Table structure for cms_css_assoc
-- ----------------------------
DROP TABLE IF EXISTS `cms_css_assoc`;
CREATE TABLE `cms_css_assoc`  (
  `assoc_to_id` int(11) NULL DEFAULT NULL,
  `assoc_css_id` int(11) NULL DEFAULT NULL,
  `assoc_type` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `create_date` datetime NULL DEFAULT NULL,
  `modified_date` datetime NULL DEFAULT NULL,
  `assoc_order` int(11) NULL DEFAULT NULL,
  INDEX `cms_index_css_assoc_by_assoc_to_id`(`assoc_to_id`) USING BTREE,
  INDEX `cms_index_css_assoc_by_assoc_css_id`(`assoc_css_id`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of cms_css_assoc
-- ----------------------------
INSERT INTO `cms_css_assoc` VALUES (25, 54, 'template', '2015-09-11 23:47:41', '2015-09-11 23:47:41', 1);
INSERT INTO `cms_css_assoc` VALUES (26, 55, 'template', '2015-09-15 22:07:22', '2015-09-15 22:07:22', 1);
INSERT INTO `cms_css_assoc` VALUES (24, 56, 'template', '2015-09-16 10:01:02', '2015-09-16 10:01:02', 1);
INSERT INTO `cms_css_assoc` VALUES (28, 56, 'template', '2015-09-17 22:56:18', '2015-09-17 22:56:18', 1);
INSERT INTO `cms_css_assoc` VALUES (29, 55, 'template', '2015-09-18 14:06:08', '2015-09-18 14:06:08', 1);

-- ----------------------------
-- Table structure for cms_css_seq
-- ----------------------------
DROP TABLE IF EXISTS `cms_css_seq`;
CREATE TABLE `cms_css_seq`  (
  `id` int(11) NOT NULL
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = FIXED;

-- ----------------------------
-- Records of cms_css_seq
-- ----------------------------
INSERT INTO `cms_css_seq` VALUES (57);

-- ----------------------------
-- Table structure for cms_event_handler_seq
-- ----------------------------
DROP TABLE IF EXISTS `cms_event_handler_seq`;
CREATE TABLE `cms_event_handler_seq`  (
  `id` int(11) NOT NULL
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = FIXED;

-- ----------------------------
-- Records of cms_event_handler_seq
-- ----------------------------
INSERT INTO `cms_event_handler_seq` VALUES (16);

-- ----------------------------
-- Table structure for cms_event_handlers
-- ----------------------------
DROP TABLE IF EXISTS `cms_event_handlers`;
CREATE TABLE `cms_event_handlers`  (
  `event_id` int(11) NULL DEFAULT NULL,
  `tag_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `module_name` varchar(160) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `removable` int(11) NULL DEFAULT NULL,
  `handler_order` int(11) NULL DEFAULT NULL,
  `handler_id` int(11) NOT NULL,
  PRIMARY KEY (`handler_id`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of cms_event_handlers
-- ----------------------------
INSERT INTO `cms_event_handlers` VALUES (38, NULL, 'MenuManager', 0, 1, 1);
INSERT INTO `cms_event_handlers` VALUES (40, NULL, 'MenuManager', 0, 1, 2);
INSERT INTO `cms_event_handlers` VALUES (38, NULL, 'Search', 0, 2, 3);
INSERT INTO `cms_event_handlers` VALUES (40, NULL, 'Search', 0, 2, 4);
INSERT INTO `cms_event_handlers` VALUES (48, NULL, 'Search', 0, 1, 5);
INSERT INTO `cms_event_handlers` VALUES (52, NULL, 'ListIt2', 0, 1, 6);
INSERT INTO `cms_event_handlers` VALUES (47, NULL, 'ListIt2', 0, 1, 7);
INSERT INTO `cms_event_handlers` VALUES (48, NULL, 'ListIt2', 0, 2, 8);
INSERT INTO `cms_event_handlers` VALUES (49, NULL, 'ListIt2', 0, 1, 9);
INSERT INTO `cms_event_handlers` VALUES (53, NULL, 'CGExtensions', 0, 1, 10);
INSERT INTO `cms_event_handlers` VALUES (53, NULL, 'HostedVideoAlbums', 1, 2, 11);
INSERT INTO `cms_event_handlers` VALUES (53, NULL, 'MleCMS', 1, 3, 12);
INSERT INTO `cms_event_handlers` VALUES (70, NULL, 'MleCMS', 1, 1, 13);
INSERT INTO `cms_event_handlers` VALUES (53, NULL, 'TinyMCE', 0, 4, 14);
INSERT INTO `cms_event_handlers` VALUES (38, NULL, 'SiteMapMadeSimple', 1, 3, 15);
INSERT INTO `cms_event_handlers` VALUES (40, NULL, 'SiteMapMadeSimple', 1, 3, 16);

-- ----------------------------
-- Table structure for cms_events
-- ----------------------------
DROP TABLE IF EXISTS `cms_events`;
CREATE TABLE `cms_events`  (
  `originator` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `event_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `event_id` int(11) NOT NULL,
  PRIMARY KEY (`event_id`) USING BTREE,
  INDEX `cms_originator`(`originator`) USING BTREE,
  INDEX `cms_event_name`(`event_name`) USING BTREE,
  INDEX `cms_event_id`(`event_id`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of cms_events
-- ----------------------------
INSERT INTO `cms_events` VALUES ('Core', 'LoginPost', 1);
INSERT INTO `cms_events` VALUES ('Core', 'LogoutPost', 2);
INSERT INTO `cms_events` VALUES ('Core', 'AddUserPre', 3);
INSERT INTO `cms_events` VALUES ('Core', 'AddUserPost', 4);
INSERT INTO `cms_events` VALUES ('Core', 'EditUserPre', 5);
INSERT INTO `cms_events` VALUES ('Core', 'EditUserPost', 6);
INSERT INTO `cms_events` VALUES ('Core', 'DeleteUserPre', 7);
INSERT INTO `cms_events` VALUES ('Core', 'DeleteUserPost', 8);
INSERT INTO `cms_events` VALUES ('Core', 'AddGroupPre', 9);
INSERT INTO `cms_events` VALUES ('Core', 'AddGroupPost', 10);
INSERT INTO `cms_events` VALUES ('Core', 'EditGroupPre', 11);
INSERT INTO `cms_events` VALUES ('Core', 'EditGroupPost', 12);
INSERT INTO `cms_events` VALUES ('Core', 'DeleteGroupPre', 13);
INSERT INTO `cms_events` VALUES ('Core', 'DeleteGroupPost', 14);
INSERT INTO `cms_events` VALUES ('Core', 'AddStylesheetPre', 15);
INSERT INTO `cms_events` VALUES ('Core', 'AddStylesheetPost', 16);
INSERT INTO `cms_events` VALUES ('Core', 'EditStylesheetPre', 17);
INSERT INTO `cms_events` VALUES ('Core', 'EditStylesheetPost', 18);
INSERT INTO `cms_events` VALUES ('Core', 'DeleteStylesheetPre', 19);
INSERT INTO `cms_events` VALUES ('Core', 'DeleteStylesheetPost', 20);
INSERT INTO `cms_events` VALUES ('Core', 'AddTemplatePre', 21);
INSERT INTO `cms_events` VALUES ('Core', 'AddTemplatePost', 22);
INSERT INTO `cms_events` VALUES ('Core', 'EditTemplatePre', 23);
INSERT INTO `cms_events` VALUES ('Core', 'EditTemplatePost', 24);
INSERT INTO `cms_events` VALUES ('Core', 'DeleteTemplatePre', 25);
INSERT INTO `cms_events` VALUES ('Core', 'DeleteTemplatePost', 26);
INSERT INTO `cms_events` VALUES ('Core', 'TemplatePreCompile', 27);
INSERT INTO `cms_events` VALUES ('Core', 'TemplatePostCompile', 28);
INSERT INTO `cms_events` VALUES ('Core', 'AddGlobalContentPre', 29);
INSERT INTO `cms_events` VALUES ('Core', 'AddGlobalContentPost', 30);
INSERT INTO `cms_events` VALUES ('Core', 'EditGlobalContentPre', 31);
INSERT INTO `cms_events` VALUES ('Core', 'EditGlobalContentPost', 32);
INSERT INTO `cms_events` VALUES ('Core', 'DeleteGlobalContentPre', 33);
INSERT INTO `cms_events` VALUES ('Core', 'DeleteGlobalContentPost', 34);
INSERT INTO `cms_events` VALUES ('Core', 'GlobalContentPreCompile', 35);
INSERT INTO `cms_events` VALUES ('Core', 'GlobalContentPostCompile', 36);
INSERT INTO `cms_events` VALUES ('Core', 'ContentEditPre', 37);
INSERT INTO `cms_events` VALUES ('Core', 'ContentEditPost', 38);
INSERT INTO `cms_events` VALUES ('Core', 'ContentDeletePre', 39);
INSERT INTO `cms_events` VALUES ('Core', 'ContentDeletePost', 40);
INSERT INTO `cms_events` VALUES ('Core', 'AddUserDefinedTagPre', 41);
INSERT INTO `cms_events` VALUES ('Core', 'AddUserDefinedTagPost', 42);
INSERT INTO `cms_events` VALUES ('Core', 'EditUserDefinedTagPre', 43);
INSERT INTO `cms_events` VALUES ('Core', 'EditUserDefinedTagPost', 44);
INSERT INTO `cms_events` VALUES ('Core', 'DeleteUserDefinedTagPre', 45);
INSERT INTO `cms_events` VALUES ('Core', 'DeleteUserDefinedTagPost', 46);
INSERT INTO `cms_events` VALUES ('Core', 'ModuleInstalled', 47);
INSERT INTO `cms_events` VALUES ('Core', 'ModuleUninstalled', 48);
INSERT INTO `cms_events` VALUES ('Core', 'ModuleUpgraded', 49);
INSERT INTO `cms_events` VALUES ('Core', 'ContentStylesheet', 50);
INSERT INTO `cms_events` VALUES ('Core', 'ContentPreCompile', 51);
INSERT INTO `cms_events` VALUES ('Core', 'ContentPostCompile', 52);
INSERT INTO `cms_events` VALUES ('Core', 'ContentPostRender', 53);
INSERT INTO `cms_events` VALUES ('Core', 'SmartyPreCompile', 54);
INSERT INTO `cms_events` VALUES ('Core', 'SmartyPostCompile', 55);
INSERT INTO `cms_events` VALUES ('Core', 'ChangeGroupAssignPre', 56);
INSERT INTO `cms_events` VALUES ('Core', 'ChangeGroupAssignPost', 57);
INSERT INTO `cms_events` VALUES ('Core', 'StylesheetPreCompile', 58);
INSERT INTO `cms_events` VALUES ('Core', 'StylesheetPostCompile', 59);
INSERT INTO `cms_events` VALUES ('Core', 'LoginFailed', 60);
INSERT INTO `cms_events` VALUES ('Core', 'TemplatePreFetch', 61);
INSERT INTO `cms_events` VALUES ('FileManager', 'OnFileUploaded', 62);
INSERT INTO `cms_events` VALUES ('News', 'NewsArticleAdded', 63);
INSERT INTO `cms_events` VALUES ('News', 'NewsArticleEdited', 64);
INSERT INTO `cms_events` VALUES ('News', 'NewsArticleDeleted', 65);
INSERT INTO `cms_events` VALUES ('News', 'NewsCategoryAdded', 66);
INSERT INTO `cms_events` VALUES ('News', 'NewsCategoryEdited', 67);
INSERT INTO `cms_events` VALUES ('News', 'NewsCategoryDeleted', 68);
INSERT INTO `cms_events` VALUES ('Search', 'SearchInitiated', 69);
INSERT INTO `cms_events` VALUES ('Search', 'SearchCompleted', 70);
INSERT INTO `cms_events` VALUES ('Search', 'SearchItemAdded', 71);
INSERT INTO `cms_events` VALUES ('Search', 'SearchItemDeleted', 72);
INSERT INTO `cms_events` VALUES ('Search', 'SearchAllItemsDeleted', 73);
INSERT INTO `cms_events` VALUES ('MleCMS', 'LangEdited', 74);
INSERT INTO `cms_events` VALUES ('MleCMS', 'BlockEdited', 75);
INSERT INTO `cms_events` VALUES ('ListIt2HeaderTexts', 'PostItemDelete', 86);
INSERT INTO `cms_events` VALUES ('ListIt2HeaderTexts', 'PreItemDelete', 85);
INSERT INTO `cms_events` VALUES ('ListIt2HeaderTexts', 'PostItemSave', 84);
INSERT INTO `cms_events` VALUES ('ListIt2HeaderTexts', 'PreItemSave', 83);
INSERT INTO `cms_events` VALUES ('ListIt2HeaderTexts', 'PreItemLoad', 87);
INSERT INTO `cms_events` VALUES ('ListIt2HeaderTexts', 'PostItemLoad', 88);
INSERT INTO `cms_events` VALUES ('ListIt2HeaderTexts', 'PreRenderAction', 89);
INSERT INTO `cms_events` VALUES ('ListIt2AboutUs', 'PreItemSave', 90);
INSERT INTO `cms_events` VALUES ('ListIt2AboutUs', 'PostItemSave', 91);
INSERT INTO `cms_events` VALUES ('ListIt2AboutUs', 'PreItemDelete', 92);
INSERT INTO `cms_events` VALUES ('ListIt2AboutUs', 'PostItemDelete', 93);
INSERT INTO `cms_events` VALUES ('ListIt2AboutUs', 'PreItemLoad', 94);
INSERT INTO `cms_events` VALUES ('ListIt2AboutUs', 'PostItemLoad', 95);
INSERT INTO `cms_events` VALUES ('ListIt2AboutUs', 'PreRenderAction', 96);
INSERT INTO `cms_events` VALUES ('ListIt2Testimonials', 'PreItemSave', 97);
INSERT INTO `cms_events` VALUES ('ListIt2Testimonials', 'PostItemSave', 98);
INSERT INTO `cms_events` VALUES ('ListIt2Testimonials', 'PreItemDelete', 99);
INSERT INTO `cms_events` VALUES ('ListIt2Testimonials', 'PostItemDelete', 100);
INSERT INTO `cms_events` VALUES ('ListIt2Testimonials', 'PreItemLoad', 101);
INSERT INTO `cms_events` VALUES ('ListIt2Testimonials', 'PostItemLoad', 102);
INSERT INTO `cms_events` VALUES ('ListIt2Testimonials', 'PreRenderAction', 103);
INSERT INTO `cms_events` VALUES ('ListIt2Social', 'PreItemSave', 104);
INSERT INTO `cms_events` VALUES ('ListIt2Social', 'PostItemSave', 105);
INSERT INTO `cms_events` VALUES ('ListIt2Social', 'PreItemDelete', 106);
INSERT INTO `cms_events` VALUES ('ListIt2Social', 'PostItemDelete', 107);
INSERT INTO `cms_events` VALUES ('ListIt2Social', 'PreItemLoad', 108);
INSERT INTO `cms_events` VALUES ('ListIt2Social', 'PostItemLoad', 109);
INSERT INTO `cms_events` VALUES ('ListIt2Social', 'PreRenderAction', 110);
INSERT INTO `cms_events` VALUES ('ListIt2Menu', 'PreItemSave', 111);
INSERT INTO `cms_events` VALUES ('ListIt2Menu', 'PostItemSave', 112);
INSERT INTO `cms_events` VALUES ('ListIt2Menu', 'PreItemDelete', 113);
INSERT INTO `cms_events` VALUES ('ListIt2Menu', 'PostItemDelete', 114);
INSERT INTO `cms_events` VALUES ('ListIt2Menu', 'PreItemLoad', 115);
INSERT INTO `cms_events` VALUES ('ListIt2Menu', 'PostItemLoad', 116);
INSERT INTO `cms_events` VALUES ('ListIt2Menu', 'PreRenderAction', 117);
INSERT INTO `cms_events` VALUES ('ListIt2Gallery', 'PreItemSave', 118);
INSERT INTO `cms_events` VALUES ('ListIt2Gallery', 'PostItemSave', 119);
INSERT INTO `cms_events` VALUES ('ListIt2Gallery', 'PreItemDelete', 120);
INSERT INTO `cms_events` VALUES ('ListIt2Gallery', 'PostItemDelete', 121);
INSERT INTO `cms_events` VALUES ('ListIt2Gallery', 'PreItemLoad', 122);
INSERT INTO `cms_events` VALUES ('ListIt2Gallery', 'PostItemLoad', 123);
INSERT INTO `cms_events` VALUES ('ListIt2Gallery', 'PreRenderAction', 124);
INSERT INTO `cms_events` VALUES ('ListIt2Staff', 'PreItemSave', 125);
INSERT INTO `cms_events` VALUES ('ListIt2Staff', 'PostItemSave', 126);
INSERT INTO `cms_events` VALUES ('ListIt2Staff', 'PreItemDelete', 127);
INSERT INTO `cms_events` VALUES ('ListIt2Staff', 'PostItemDelete', 128);
INSERT INTO `cms_events` VALUES ('ListIt2Staff', 'PreItemLoad', 129);
INSERT INTO `cms_events` VALUES ('ListIt2Staff', 'PostItemLoad', 130);
INSERT INTO `cms_events` VALUES ('ListIt2Staff', 'PreRenderAction', 131);
INSERT INTO `cms_events` VALUES ('ListIt2Slider', 'PreItemSave', 132);
INSERT INTO `cms_events` VALUES ('ListIt2Slider', 'PostItemSave', 133);
INSERT INTO `cms_events` VALUES ('ListIt2Slider', 'PreItemDelete', 134);
INSERT INTO `cms_events` VALUES ('ListIt2Slider', 'PostItemDelete', 135);
INSERT INTO `cms_events` VALUES ('ListIt2Slider', 'PreItemLoad', 136);
INSERT INTO `cms_events` VALUES ('ListIt2Slider', 'PostItemLoad', 137);
INSERT INTO `cms_events` VALUES ('ListIt2Slider', 'PreRenderAction', 138);
INSERT INTO `cms_events` VALUES ('ListIt2Blog', 'PreItemSave', 139);
INSERT INTO `cms_events` VALUES ('ListIt2Blog', 'PostItemSave', 140);
INSERT INTO `cms_events` VALUES ('ListIt2Blog', 'PreItemDelete', 141);
INSERT INTO `cms_events` VALUES ('ListIt2Blog', 'PostItemDelete', 142);
INSERT INTO `cms_events` VALUES ('ListIt2Blog', 'PreItemLoad', 143);
INSERT INTO `cms_events` VALUES ('ListIt2Blog', 'PostItemLoad', 144);
INSERT INTO `cms_events` VALUES ('ListIt2Blog', 'PreRenderAction', 145);
INSERT INTO `cms_events` VALUES ('vipmember', 'vipmember_added', 168);
INSERT INTO `cms_events` VALUES ('vipmember', 'vipmember_modified', 169);
INSERT INTO `cms_events` VALUES ('vipmember', 'vipmember_deleted', 170);
INSERT INTO `cms_events` VALUES ('CustomGS', 'OnSettingChange', 171);

-- ----------------------------
-- Table structure for cms_events_seq
-- ----------------------------
DROP TABLE IF EXISTS `cms_events_seq`;
CREATE TABLE `cms_events_seq`  (
  `id` int(11) NOT NULL
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = FIXED;

-- ----------------------------
-- Records of cms_events_seq
-- ----------------------------
INSERT INTO `cms_events_seq` VALUES (171);

-- ----------------------------
-- Table structure for cms_group_perms
-- ----------------------------
DROP TABLE IF EXISTS `cms_group_perms`;
CREATE TABLE `cms_group_perms`  (
  `group_perm_id` int(11) NOT NULL,
  `group_id` int(11) NULL DEFAULT NULL,
  `permission_id` int(11) NULL DEFAULT NULL,
  `create_date` datetime NULL DEFAULT NULL,
  `modified_date` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`group_perm_id`) USING BTREE,
  INDEX `cms_index_group_perms_by_group_id_permission_id`(`group_id`, `permission_id`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = FIXED;

-- ----------------------------
-- Records of cms_group_perms
-- ----------------------------
INSERT INTO `cms_group_perms` VALUES (176, 3, 30, '2009-05-12 08:05:26', '2009-05-12 08:05:26');
INSERT INTO `cms_group_perms` VALUES (177, 3, 25, '2009-05-12 08:05:26', '2009-05-12 08:05:26');
INSERT INTO `cms_group_perms` VALUES (178, 3, 4, '2009-05-12 08:05:26', '2009-05-12 08:05:26');
INSERT INTO `cms_group_perms` VALUES (179, 3, 31, '2009-05-12 08:05:26', '2009-05-12 08:05:26');
INSERT INTO `cms_group_perms` VALUES (180, 3, 26, '2009-05-12 08:05:26', '2009-05-12 08:05:26');
INSERT INTO `cms_group_perms` VALUES (181, 3, 22, '2009-05-12 08:05:26', '2009-05-12 08:05:26');
INSERT INTO `cms_group_perms` VALUES (182, 3, 11, '2009-05-12 08:05:26', '2009-05-12 08:05:26');
INSERT INTO `cms_group_perms` VALUES (183, 3, 32, '2009-05-12 08:05:26', '2009-05-12 08:05:26');
INSERT INTO `cms_group_perms` VALUES (184, 3, 27, '2009-05-12 08:05:26', '2009-05-12 08:05:26');
INSERT INTO `cms_group_perms` VALUES (185, 3, 24, '2009-05-12 08:05:26', '2009-05-12 08:05:26');
INSERT INTO `cms_group_perms` VALUES (186, 3, 16, '2009-05-12 08:05:26', '2009-05-12 08:05:26');
INSERT INTO `cms_group_perms` VALUES (188, 1, 50, '2015-04-10 00:18:55', '2015-04-10 00:18:55');
INSERT INTO `cms_group_perms` VALUES (193, 2, 79, '2017-05-11 12:38:22', '2017-05-11 12:38:22');
INSERT INTO `cms_group_perms` VALUES (194, 2, 80, '2017-05-11 12:38:22', '2017-05-11 12:38:22');
INSERT INTO `cms_group_perms` VALUES (195, 2, 75, '2017-05-11 12:38:22', '2017-05-11 12:38:22');
INSERT INTO `cms_group_perms` VALUES (196, 2, 121, '2017-05-11 12:38:22', '2017-05-11 12:38:22');
INSERT INTO `cms_group_perms` VALUES (197, 2, 122, '2017-05-11 12:38:22', '2017-05-11 12:38:22');
INSERT INTO `cms_group_perms` VALUES (198, 2, 117, '2017-05-11 12:38:22', '2017-05-11 12:38:22');
INSERT INTO `cms_group_perms` VALUES (199, 2, 120, '2017-05-11 12:38:22', '2017-05-11 12:38:22');
INSERT INTO `cms_group_perms` VALUES (200, 2, 103, '2017-05-11 12:38:22', '2017-05-11 12:38:22');
INSERT INTO `cms_group_perms` VALUES (201, 2, 104, '2017-05-11 12:38:22', '2017-05-11 12:38:22');
INSERT INTO `cms_group_perms` VALUES (202, 2, 99, '2017-05-11 12:38:22', '2017-05-11 12:38:22');
INSERT INTO `cms_group_perms` VALUES (203, 2, 102, '2017-05-11 12:38:22', '2017-05-11 12:38:22');
INSERT INTO `cms_group_perms` VALUES (204, 2, 73, '2017-05-11 12:38:22', '2017-05-11 12:38:22');
INSERT INTO `cms_group_perms` VALUES (205, 2, 74, '2017-05-11 12:38:22', '2017-05-11 12:38:22');
INSERT INTO `cms_group_perms` VALUES (206, 2, 69, '2017-05-11 12:38:22', '2017-05-11 12:38:22');
INSERT INTO `cms_group_perms` VALUES (207, 2, 72, '2017-05-11 12:38:22', '2017-05-11 12:38:22');
INSERT INTO `cms_group_perms` VALUES (208, 2, 97, '2017-05-11 12:38:22', '2017-05-11 12:38:22');
INSERT INTO `cms_group_perms` VALUES (209, 2, 98, '2017-05-11 12:38:22', '2017-05-11 12:38:22');
INSERT INTO `cms_group_perms` VALUES (210, 2, 93, '2017-05-11 12:38:22', '2017-05-11 12:38:22');
INSERT INTO `cms_group_perms` VALUES (211, 2, 96, '2017-05-11 12:38:22', '2017-05-11 12:38:22');
INSERT INTO `cms_group_perms` VALUES (212, 2, 115, '2017-05-11 12:38:22', '2017-05-11 12:38:22');
INSERT INTO `cms_group_perms` VALUES (213, 2, 116, '2017-05-11 12:38:22', '2017-05-11 12:38:22');
INSERT INTO `cms_group_perms` VALUES (214, 2, 111, '2017-05-11 12:38:22', '2017-05-11 12:38:22');
INSERT INTO `cms_group_perms` VALUES (215, 2, 114, '2017-05-11 12:38:22', '2017-05-11 12:38:22');
INSERT INTO `cms_group_perms` VALUES (216, 2, 91, '2017-05-11 12:38:22', '2017-05-11 12:38:22');
INSERT INTO `cms_group_perms` VALUES (217, 2, 92, '2017-05-11 12:38:22', '2017-05-11 12:38:22');
INSERT INTO `cms_group_perms` VALUES (218, 2, 87, '2017-05-11 12:38:22', '2017-05-11 12:38:22');
INSERT INTO `cms_group_perms` VALUES (219, 2, 90, '2017-05-11 12:38:22', '2017-05-11 12:38:22');
INSERT INTO `cms_group_perms` VALUES (220, 2, 109, '2017-05-11 12:38:22', '2017-05-11 12:38:22');
INSERT INTO `cms_group_perms` VALUES (221, 2, 110, '2017-05-11 12:38:22', '2017-05-11 12:38:22');
INSERT INTO `cms_group_perms` VALUES (222, 2, 105, '2017-05-11 12:38:22', '2017-05-11 12:38:22');
INSERT INTO `cms_group_perms` VALUES (223, 2, 108, '2017-05-11 12:38:22', '2017-05-11 12:38:22');
INSERT INTO `cms_group_perms` VALUES (224, 2, 85, '2017-05-11 12:38:22', '2017-05-11 12:38:22');
INSERT INTO `cms_group_perms` VALUES (225, 2, 86, '2017-05-11 12:38:22', '2017-05-11 12:38:22');
INSERT INTO `cms_group_perms` VALUES (226, 2, 81, '2017-05-11 12:38:22', '2017-05-11 12:38:22');
INSERT INTO `cms_group_perms` VALUES (227, 2, 84, '2017-05-11 12:38:22', '2017-05-11 12:38:22');
INSERT INTO `cms_group_perms` VALUES (228, 2, 138, '2017-05-11 12:38:22', '2017-05-11 12:38:22');

-- ----------------------------
-- Table structure for cms_group_perms_seq
-- ----------------------------
DROP TABLE IF EXISTS `cms_group_perms_seq`;
CREATE TABLE `cms_group_perms_seq`  (
  `id` int(11) NOT NULL
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = FIXED;

-- ----------------------------
-- Records of cms_group_perms_seq
-- ----------------------------
INSERT INTO `cms_group_perms_seq` VALUES (228);

-- ----------------------------
-- Table structure for cms_groups
-- ----------------------------
DROP TABLE IF EXISTS `cms_groups`;
CREATE TABLE `cms_groups`  (
  `group_id` int(11) NOT NULL,
  `group_name` varchar(25) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `active` tinyint(4) NULL DEFAULT NULL,
  `create_date` datetime NULL DEFAULT NULL,
  `modified_date` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`group_id`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of cms_groups
-- ----------------------------
INSERT INTO `cms_groups` VALUES (1, 'Admin', 1, '2006-07-25 21:22:32', '2006-07-25 21:22:32');
INSERT INTO `cms_groups` VALUES (2, 'Editor', 1, '2006-07-25 21:22:32', '2006-07-25 21:22:32');
INSERT INTO `cms_groups` VALUES (3, 'Designer', 1, '2006-07-25 21:22:32', '2006-07-25 21:22:32');

-- ----------------------------
-- Table structure for cms_groups_seq
-- ----------------------------
DROP TABLE IF EXISTS `cms_groups_seq`;
CREATE TABLE `cms_groups_seq`  (
  `id` int(11) NOT NULL
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = FIXED;

-- ----------------------------
-- Records of cms_groups_seq
-- ----------------------------
INSERT INTO `cms_groups_seq` VALUES (3);

-- ----------------------------
-- Table structure for cms_htmlblobs
-- ----------------------------
DROP TABLE IF EXISTS `cms_htmlblobs`;
CREATE TABLE `cms_htmlblobs`  (
  `htmlblob_id` int(11) NOT NULL,
  `htmlblob_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `html` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `description` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `use_wysiwyg` tinyint(4) NULL DEFAULT NULL,
  `owner` int(11) NULL DEFAULT NULL,
  `create_date` datetime NULL DEFAULT NULL,
  `modified_date` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`htmlblob_id`) USING BTREE,
  INDEX `cms_index_htmlblobs_by_htmlblob_name`(`htmlblob_name`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of cms_htmlblobs
-- ----------------------------
INSERT INTO `cms_htmlblobs` VALUES (2, 'keywords', 'Ngan, khoa ngan, am thuc ngan, xua va nay, ngan noi tieng, nha hang khoa ngan, khoa ngan hai bà trưng hà nội, ngan nổi tiếng hà nội', '', 1, 1, '2015-04-10 19:11:45', '2015-05-01 21:36:50');
INSERT INTO `cms_htmlblobs` VALUES (3, 'description', 'Khoa Ngan Xưa và Nay nổi tiếng tại 77 Hai Bà Trưng – một địa chỉ có từ lâu đời tại Hà Nội với đặc sản là các món ngan cực kỳ thơm ngon và hấp dẫn.', '', 1, 1, '2015-04-10 19:11:55', '2015-05-01 21:35:29');
INSERT INTO `cms_htmlblobs` VALUES (4, 'slogan', 'slogan', '', 1, 1, '2015-04-10 19:12:03', '2015-04-10 19:12:03');
INSERT INTO `cms_htmlblobs` VALUES (5, 'GooglePlus', 'https://plus.google.com/u/0/117664319647095271808/about', '', 1, 1, '2015-04-10 19:12:48', '2015-04-10 19:12:48');
INSERT INTO `cms_htmlblobs` VALUES (6, 'copyright', '<div id=\"copyright\">©{get_copyright} - <strong>Lee Peace</strong> | The Official Website All rights reserved. Designed and built with all the love in the world by <a title=\"idev\" href=\"http://www.idev.com.vn\" target=\"_blank\">iDev Webmaster</a></div>', '', 1, 1, '2015-04-10 19:13:14', '2015-04-24 17:19:29');
INSERT INTO `cms_htmlblobs` VALUES (7, 'add', '77 Hai Bà Trưng, Hoàn Kiếm, HN', '', 1, 1, '2015-04-27 12:07:00', '2015-05-08 17:45:32');
INSERT INTO `cms_htmlblobs` VALUES (8, 'email', 'khoanganxuavanay@gmail.com', '', 1, 1, '2015-05-01 00:34:19', '2015-05-01 00:34:19');
INSERT INTO `cms_htmlblobs` VALUES (9, 'hotline', '0944.27.8866', '', 1, 1, '2015-05-01 00:35:41', '2015-05-08 17:46:16');
INSERT INTO `cms_htmlblobs` VALUES (10, 'tel', '0944.27.8866 | 024.39422206', '', 1, 1, '2015-05-01 00:36:29', '2019-07-02 10:24:10');
INSERT INTO `cms_htmlblobs` VALUES (11, 'company_name', 'Công Ty TNHH Thương Mại và Dịch vụ Quang Long', '', 1, 1, '2021-08-18 11:49:24', '2021-08-18 11:49:24');
INSERT INTO `cms_htmlblobs` VALUES (12, 'mst', '0101240664', '', 1, 1, '2021-08-18 11:49:51', '2021-08-18 11:49:51');

-- ----------------------------
-- Table structure for cms_htmlblobs_seq
-- ----------------------------
DROP TABLE IF EXISTS `cms_htmlblobs_seq`;
CREATE TABLE `cms_htmlblobs_seq`  (
  `id` int(11) NOT NULL
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = FIXED;

-- ----------------------------
-- Records of cms_htmlblobs_seq
-- ----------------------------
INSERT INTO `cms_htmlblobs_seq` VALUES (12);

-- ----------------------------
-- Table structure for cms_module_cgcontentutils
-- ----------------------------
DROP TABLE IF EXISTS `cms_module_cgcontentutils`;
CREATE TABLE `cms_module_cgcontentutils`  (
  `id` int(11) NOT NULL,
  `name` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `prompt` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `value` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `type` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `attribs` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `create_date` datetime NULL DEFAULT NULL,
  `modified_date` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of cms_module_cgcontentutils
-- ----------------------------

-- ----------------------------
-- Table structure for cms_module_cge_assocdata
-- ----------------------------
DROP TABLE IF EXISTS `cms_module_cge_assocdata`;
CREATE TABLE `cms_module_cge_assocdata`  (
  `id` int(11) NOT NULL,
  `key1` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `key2` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `key3` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `key4` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `data` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `type` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `expiry` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `create_date` datetime NULL DEFAULT NULL,
  `modified_date` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of cms_module_cge_assocdata
-- ----------------------------

-- ----------------------------
-- Table structure for cms_module_cge_countries
-- ----------------------------
DROP TABLE IF EXISTS `cms_module_cge_countries`;
CREATE TABLE `cms_module_cge_countries`  (
  `id` int(11) NOT NULL,
  `code` varchar(2) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `sorting` int(11) NULL DEFAULT 0,
  PRIMARY KEY (`id`, `code`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of cms_module_cge_countries
-- ----------------------------
INSERT INTO `cms_module_cge_countries` VALUES (1, 'AF', 'Afghanistan', 1);
INSERT INTO `cms_module_cge_countries` VALUES (2, 'AL', 'Albania', 2);
INSERT INTO `cms_module_cge_countries` VALUES (3, 'DZ', 'Algeria', 3);
INSERT INTO `cms_module_cge_countries` VALUES (4, 'AS', 'American Samoa', 4);
INSERT INTO `cms_module_cge_countries` VALUES (5, 'AD', 'Andorra', 5);
INSERT INTO `cms_module_cge_countries` VALUES (6, 'AO', 'Angola', 6);
INSERT INTO `cms_module_cge_countries` VALUES (7, 'AI', 'Anguilla', 7);
INSERT INTO `cms_module_cge_countries` VALUES (8, 'AQ', 'Antarctica', 8);
INSERT INTO `cms_module_cge_countries` VALUES (9, 'AG', 'Antigua And Barbuda', 9);
INSERT INTO `cms_module_cge_countries` VALUES (10, 'AR', 'Argentina', 10);
INSERT INTO `cms_module_cge_countries` VALUES (11, 'AM', 'Armenia', 11);
INSERT INTO `cms_module_cge_countries` VALUES (12, 'AW', 'Aruba', 12);
INSERT INTO `cms_module_cge_countries` VALUES (13, 'AU', 'Australia', 13);
INSERT INTO `cms_module_cge_countries` VALUES (14, 'AT', 'Austria', 14);
INSERT INTO `cms_module_cge_countries` VALUES (15, 'AZ', 'Azerbaijan', 15);
INSERT INTO `cms_module_cge_countries` VALUES (16, 'BS', 'Bahamas', 16);
INSERT INTO `cms_module_cge_countries` VALUES (17, 'BH', 'Bahrain', 17);
INSERT INTO `cms_module_cge_countries` VALUES (18, 'BD', 'Bangladesh', 18);
INSERT INTO `cms_module_cge_countries` VALUES (19, 'BB', 'Barbados', 19);
INSERT INTO `cms_module_cge_countries` VALUES (20, 'BY', 'Belarus', 20);
INSERT INTO `cms_module_cge_countries` VALUES (21, 'BE', 'Belgium', 21);
INSERT INTO `cms_module_cge_countries` VALUES (22, 'BZ', 'Belize', 22);
INSERT INTO `cms_module_cge_countries` VALUES (23, 'BJ', 'Benin', 23);
INSERT INTO `cms_module_cge_countries` VALUES (24, 'BM', 'Bermuda', 24);
INSERT INTO `cms_module_cge_countries` VALUES (25, 'BT', 'Bhutan', 25);
INSERT INTO `cms_module_cge_countries` VALUES (26, 'BO', 'Bolivia', 26);
INSERT INTO `cms_module_cge_countries` VALUES (27, 'BA', 'Bosnia And Herzegowina', 27);
INSERT INTO `cms_module_cge_countries` VALUES (28, 'BW', 'Botswana', 28);
INSERT INTO `cms_module_cge_countries` VALUES (29, 'BV', 'Bouvet Island', 29);
INSERT INTO `cms_module_cge_countries` VALUES (30, 'BR', 'Brazil', 30);
INSERT INTO `cms_module_cge_countries` VALUES (31, 'IO', 'British Indian Ocean Territory', 31);
INSERT INTO `cms_module_cge_countries` VALUES (32, 'BN', 'Brunei Darussalam', 32);
INSERT INTO `cms_module_cge_countries` VALUES (33, 'BG', 'Bulgaria', 33);
INSERT INTO `cms_module_cge_countries` VALUES (34, 'BF', 'Burkina Faso', 34);
INSERT INTO `cms_module_cge_countries` VALUES (35, 'BI', 'Burundi', 35);
INSERT INTO `cms_module_cge_countries` VALUES (36, 'KH', 'Cambodia', 36);
INSERT INTO `cms_module_cge_countries` VALUES (37, 'CM', 'Cameroon', 37);
INSERT INTO `cms_module_cge_countries` VALUES (38, 'CA', 'Canada', 38);
INSERT INTO `cms_module_cge_countries` VALUES (39, 'CV', 'Cape Verde', 39);
INSERT INTO `cms_module_cge_countries` VALUES (40, 'KY', 'Cayman Islands', 40);
INSERT INTO `cms_module_cge_countries` VALUES (41, 'CF', 'Central African Republic', 41);
INSERT INTO `cms_module_cge_countries` VALUES (42, 'TD', 'Chad', 42);
INSERT INTO `cms_module_cge_countries` VALUES (43, 'CL', 'Chile', 43);
INSERT INTO `cms_module_cge_countries` VALUES (44, 'CN', 'China', 44);
INSERT INTO `cms_module_cge_countries` VALUES (45, 'CX', 'Christmas Island', 45);
INSERT INTO `cms_module_cge_countries` VALUES (46, 'CC', 'Cocos (Keeling) Islands', 46);
INSERT INTO `cms_module_cge_countries` VALUES (47, 'CO', 'Colombia', 47);
INSERT INTO `cms_module_cge_countries` VALUES (48, 'KM', 'Comoros', 48);
INSERT INTO `cms_module_cge_countries` VALUES (49, 'CG', 'Congo', 49);
INSERT INTO `cms_module_cge_countries` VALUES (50, 'CK', 'Cook Islands', 50);
INSERT INTO `cms_module_cge_countries` VALUES (51, 'CR', 'Costa Rica', 51);
INSERT INTO `cms_module_cge_countries` VALUES (52, 'CI', 'Cote D\'Ivoire', 52);
INSERT INTO `cms_module_cge_countries` VALUES (53, 'HR', 'Croatia (Local Name: Hrvatska)', 53);
INSERT INTO `cms_module_cge_countries` VALUES (54, 'CU', 'Cuba', 54);
INSERT INTO `cms_module_cge_countries` VALUES (55, 'CY', 'Cyprus', 55);
INSERT INTO `cms_module_cge_countries` VALUES (56, 'CZ', 'Czech Republic', 56);
INSERT INTO `cms_module_cge_countries` VALUES (57, 'DK', 'Denmark', 57);
INSERT INTO `cms_module_cge_countries` VALUES (58, 'DJ', 'Djibouti', 58);
INSERT INTO `cms_module_cge_countries` VALUES (59, 'DM', 'Dominica', 59);
INSERT INTO `cms_module_cge_countries` VALUES (60, 'DO', 'Dominican Republic', 60);
INSERT INTO `cms_module_cge_countries` VALUES (61, 'TP', 'East Timor', 61);
INSERT INTO `cms_module_cge_countries` VALUES (62, 'EC', 'Ecuador', 62);
INSERT INTO `cms_module_cge_countries` VALUES (63, 'EG', 'Egypt', 63);
INSERT INTO `cms_module_cge_countries` VALUES (64, 'SV', 'El Salvador', 64);
INSERT INTO `cms_module_cge_countries` VALUES (65, 'GQ', 'Equatorial Guinea', 65);
INSERT INTO `cms_module_cge_countries` VALUES (66, 'ER', 'Eritrea', 66);
INSERT INTO `cms_module_cge_countries` VALUES (67, 'EE', 'Estonia', 67);
INSERT INTO `cms_module_cge_countries` VALUES (68, 'ET', 'Ethiopia', 68);
INSERT INTO `cms_module_cge_countries` VALUES (69, 'FK', 'Falkland Islands (Malvinas)', 69);
INSERT INTO `cms_module_cge_countries` VALUES (70, 'FO', 'Faroe Islands', 70);
INSERT INTO `cms_module_cge_countries` VALUES (71, 'FJ', 'Fiji', 71);
INSERT INTO `cms_module_cge_countries` VALUES (72, 'FI', 'Finland', 72);
INSERT INTO `cms_module_cge_countries` VALUES (73, 'FR', 'France', 73);
INSERT INTO `cms_module_cge_countries` VALUES (74, 'GF', 'French Guiana', 74);
INSERT INTO `cms_module_cge_countries` VALUES (75, 'PF', 'French Polynesia', 75);
INSERT INTO `cms_module_cge_countries` VALUES (76, 'TF', 'French Southern Territories', 76);
INSERT INTO `cms_module_cge_countries` VALUES (77, 'GA', 'Gabon', 77);
INSERT INTO `cms_module_cge_countries` VALUES (78, 'GM', 'Gambia', 78);
INSERT INTO `cms_module_cge_countries` VALUES (79, 'GE', 'Georgia', 79);
INSERT INTO `cms_module_cge_countries` VALUES (80, 'DE', 'Germany', 80);
INSERT INTO `cms_module_cge_countries` VALUES (81, 'GH', 'Ghana', 81);
INSERT INTO `cms_module_cge_countries` VALUES (82, 'GI', 'Gibraltar', 82);
INSERT INTO `cms_module_cge_countries` VALUES (83, 'GR', 'Greece', 83);
INSERT INTO `cms_module_cge_countries` VALUES (84, 'GL', 'Greenland', 84);
INSERT INTO `cms_module_cge_countries` VALUES (85, 'GD', 'Grenada', 85);
INSERT INTO `cms_module_cge_countries` VALUES (86, 'GP', 'Guadeloupe', 86);
INSERT INTO `cms_module_cge_countries` VALUES (87, 'GU', 'Guam', 87);
INSERT INTO `cms_module_cge_countries` VALUES (88, 'GT', 'Guatemala', 88);
INSERT INTO `cms_module_cge_countries` VALUES (89, 'GN', 'Guinea', 89);
INSERT INTO `cms_module_cge_countries` VALUES (90, 'GW', 'Guinea-Bissau', 90);
INSERT INTO `cms_module_cge_countries` VALUES (91, 'GY', 'Guyana', 91);
INSERT INTO `cms_module_cge_countries` VALUES (92, 'HT', 'Haiti', 92);
INSERT INTO `cms_module_cge_countries` VALUES (93, 'HM', 'Heard And Mc Donald Islands', 93);
INSERT INTO `cms_module_cge_countries` VALUES (94, 'VA', 'Holy See (Vatican City State)', 94);
INSERT INTO `cms_module_cge_countries` VALUES (95, 'HN', 'Honduras', 95);
INSERT INTO `cms_module_cge_countries` VALUES (96, 'HK', 'Hong Kong', 96);
INSERT INTO `cms_module_cge_countries` VALUES (97, 'HU', 'Hungary', 97);
INSERT INTO `cms_module_cge_countries` VALUES (98, 'IS', 'Icel And', 98);
INSERT INTO `cms_module_cge_countries` VALUES (99, 'IN', 'India', 99);
INSERT INTO `cms_module_cge_countries` VALUES (100, 'ID', 'Indonesia', 100);
INSERT INTO `cms_module_cge_countries` VALUES (101, 'IR', 'Iran (Islamic Republic Of)', 101);
INSERT INTO `cms_module_cge_countries` VALUES (102, 'IQ', 'Iraq', 102);
INSERT INTO `cms_module_cge_countries` VALUES (103, 'IE', 'Ireland', 103);
INSERT INTO `cms_module_cge_countries` VALUES (104, 'IL', 'Israel', 104);
INSERT INTO `cms_module_cge_countries` VALUES (105, 'IT', 'Italy', 105);
INSERT INTO `cms_module_cge_countries` VALUES (106, 'JM', 'Jamaica', 106);
INSERT INTO `cms_module_cge_countries` VALUES (107, 'JP', 'Japan', 107);
INSERT INTO `cms_module_cge_countries` VALUES (108, 'JO', 'Jordan', 108);
INSERT INTO `cms_module_cge_countries` VALUES (109, 'KZ', 'Kazakhstan', 109);
INSERT INTO `cms_module_cge_countries` VALUES (110, 'KE', 'Kenya', 110);
INSERT INTO `cms_module_cge_countries` VALUES (111, 'KI', 'Kiribati', 111);
INSERT INTO `cms_module_cge_countries` VALUES (112, 'KP', 'Korea', 112);
INSERT INTO `cms_module_cge_countries` VALUES (113, 'KR', 'Korea', 113);
INSERT INTO `cms_module_cge_countries` VALUES (114, 'KW', 'Kuwait', 114);
INSERT INTO `cms_module_cge_countries` VALUES (115, 'KG', 'Kyrgyzstan', 115);
INSERT INTO `cms_module_cge_countries` VALUES (116, 'LA', 'Lao People\'S Dem Republic', 116);
INSERT INTO `cms_module_cge_countries` VALUES (117, 'LV', 'Latvia', 117);
INSERT INTO `cms_module_cge_countries` VALUES (118, 'LB', 'Lebanon', 118);
INSERT INTO `cms_module_cge_countries` VALUES (119, 'LS', 'Lesotho', 119);
INSERT INTO `cms_module_cge_countries` VALUES (120, 'LR', 'Liberia', 120);
INSERT INTO `cms_module_cge_countries` VALUES (121, 'LY', 'Libyan Arab Jamahiriya', 121);
INSERT INTO `cms_module_cge_countries` VALUES (122, 'LI', 'Liechtenstein', 122);
INSERT INTO `cms_module_cge_countries` VALUES (123, 'LT', 'Lithuania', 123);
INSERT INTO `cms_module_cge_countries` VALUES (124, 'LU', 'Luxembourg', 124);
INSERT INTO `cms_module_cge_countries` VALUES (125, 'MO', 'Macau', 125);
INSERT INTO `cms_module_cge_countries` VALUES (126, 'MK', 'Macedonia', 126);
INSERT INTO `cms_module_cge_countries` VALUES (127, 'MG', 'Madagascar', 127);
INSERT INTO `cms_module_cge_countries` VALUES (128, 'MW', 'Malawi', 128);
INSERT INTO `cms_module_cge_countries` VALUES (129, 'MY', 'Malaysia', 129);
INSERT INTO `cms_module_cge_countries` VALUES (130, 'MV', 'Maldives', 130);
INSERT INTO `cms_module_cge_countries` VALUES (131, 'ML', 'Mali', 131);
INSERT INTO `cms_module_cge_countries` VALUES (132, 'MT', 'Malta', 132);
INSERT INTO `cms_module_cge_countries` VALUES (133, 'MH', 'Marshall Islands', 133);
INSERT INTO `cms_module_cge_countries` VALUES (134, 'MQ', 'Martinique', 134);
INSERT INTO `cms_module_cge_countries` VALUES (135, 'MR', 'Mauritania', 135);
INSERT INTO `cms_module_cge_countries` VALUES (136, 'MU', 'Mauritius', 136);
INSERT INTO `cms_module_cge_countries` VALUES (137, 'YT', 'Mayotte', 137);
INSERT INTO `cms_module_cge_countries` VALUES (138, 'MX', 'Mexico', 138);
INSERT INTO `cms_module_cge_countries` VALUES (139, 'FM', 'Micronesia', 139);
INSERT INTO `cms_module_cge_countries` VALUES (140, 'MD', 'Moldova', 140);
INSERT INTO `cms_module_cge_countries` VALUES (141, 'MC', 'Monaco', 141);
INSERT INTO `cms_module_cge_countries` VALUES (142, 'MN', 'Mongolia', 142);
INSERT INTO `cms_module_cge_countries` VALUES (143, 'MS', 'Montserrat', 143);
INSERT INTO `cms_module_cge_countries` VALUES (144, 'MA', 'Morocco', 144);
INSERT INTO `cms_module_cge_countries` VALUES (145, 'MZ', 'Mozambique', 145);
INSERT INTO `cms_module_cge_countries` VALUES (146, 'MM', 'Myanmar', 146);
INSERT INTO `cms_module_cge_countries` VALUES (147, 'NA', 'Namibia', 147);
INSERT INTO `cms_module_cge_countries` VALUES (148, 'NR', 'Nauru', 148);
INSERT INTO `cms_module_cge_countries` VALUES (149, 'NP', 'Nepal', 149);
INSERT INTO `cms_module_cge_countries` VALUES (150, 'NL', 'Netherlands', 150);
INSERT INTO `cms_module_cge_countries` VALUES (151, 'AN', 'Netherlands Ant Illes', 151);
INSERT INTO `cms_module_cge_countries` VALUES (152, 'NC', 'New Caledonia', 152);
INSERT INTO `cms_module_cge_countries` VALUES (153, 'NZ', 'New Zealand', 153);
INSERT INTO `cms_module_cge_countries` VALUES (154, 'NI', 'Nicaragua', 154);
INSERT INTO `cms_module_cge_countries` VALUES (155, 'NE', 'Niger', 155);
INSERT INTO `cms_module_cge_countries` VALUES (156, 'NG', 'Nigeria', 156);
INSERT INTO `cms_module_cge_countries` VALUES (157, 'NU', 'Niue', 157);
INSERT INTO `cms_module_cge_countries` VALUES (158, 'NF', 'Norfolk Island', 158);
INSERT INTO `cms_module_cge_countries` VALUES (159, 'MP', 'Northern Mariana Islands', 159);
INSERT INTO `cms_module_cge_countries` VALUES (160, 'NO', 'Norway', 160);
INSERT INTO `cms_module_cge_countries` VALUES (161, 'OM', 'Oman', 161);
INSERT INTO `cms_module_cge_countries` VALUES (162, 'PK', 'Pakistan', 162);
INSERT INTO `cms_module_cge_countries` VALUES (163, 'PW', 'Palau', 163);
INSERT INTO `cms_module_cge_countries` VALUES (164, 'PA', 'Panama', 164);
INSERT INTO `cms_module_cge_countries` VALUES (165, 'PG', 'Papua New Guinea', 165);
INSERT INTO `cms_module_cge_countries` VALUES (166, 'PY', 'Paraguay', 166);
INSERT INTO `cms_module_cge_countries` VALUES (167, 'PE', 'Peru', 167);
INSERT INTO `cms_module_cge_countries` VALUES (168, 'PH', 'Philippines', 168);
INSERT INTO `cms_module_cge_countries` VALUES (169, 'PN', 'Pitcairn', 169);
INSERT INTO `cms_module_cge_countries` VALUES (170, 'PL', 'Poland', 170);
INSERT INTO `cms_module_cge_countries` VALUES (171, 'PT', 'Portugal', 171);
INSERT INTO `cms_module_cge_countries` VALUES (172, 'PR', 'Puerto Rico', 172);
INSERT INTO `cms_module_cge_countries` VALUES (173, 'QA', 'Qatar', 173);
INSERT INTO `cms_module_cge_countries` VALUES (174, 'RE', 'Reunion', 174);
INSERT INTO `cms_module_cge_countries` VALUES (175, 'RO', 'Romania', 175);
INSERT INTO `cms_module_cge_countries` VALUES (176, 'RU', 'Russian Federation', 176);
INSERT INTO `cms_module_cge_countries` VALUES (177, 'RW', 'Rwanda', 177);
INSERT INTO `cms_module_cge_countries` VALUES (178, 'KN', 'Saint K Itts And Nevis', 178);
INSERT INTO `cms_module_cge_countries` VALUES (179, 'LC', 'Saint Lucia', 179);
INSERT INTO `cms_module_cge_countries` VALUES (180, 'VC', 'Saint Vincent', 180);
INSERT INTO `cms_module_cge_countries` VALUES (181, 'WS', 'Samoa', 181);
INSERT INTO `cms_module_cge_countries` VALUES (182, 'SM', 'San Marino', 182);
INSERT INTO `cms_module_cge_countries` VALUES (183, 'ST', 'Sao Tome And Principe', 183);
INSERT INTO `cms_module_cge_countries` VALUES (184, 'SA', 'Saudi Arabia', 184);
INSERT INTO `cms_module_cge_countries` VALUES (185, 'SN', 'Senegal', 185);
INSERT INTO `cms_module_cge_countries` VALUES (186, 'SC', 'Seychelles', 186);
INSERT INTO `cms_module_cge_countries` VALUES (187, 'SL', 'Sierra Leone', 187);
INSERT INTO `cms_module_cge_countries` VALUES (188, 'SG', 'Singapore', 188);
INSERT INTO `cms_module_cge_countries` VALUES (189, 'SK', 'Slovakia (Slovak Republic)', 189);
INSERT INTO `cms_module_cge_countries` VALUES (190, 'SI', 'Slovenia', 190);
INSERT INTO `cms_module_cge_countries` VALUES (191, 'SB', 'Solomon Islands', 191);
INSERT INTO `cms_module_cge_countries` VALUES (192, 'SO', 'Somalia', 192);
INSERT INTO `cms_module_cge_countries` VALUES (193, 'ZA', 'South Africa', 193);
INSERT INTO `cms_module_cge_countries` VALUES (194, 'GS', 'South Georgia', 194);
INSERT INTO `cms_module_cge_countries` VALUES (195, 'ES', 'Spain', 195);
INSERT INTO `cms_module_cge_countries` VALUES (196, 'LK', 'Sri Lanka', 196);
INSERT INTO `cms_module_cge_countries` VALUES (197, 'SH', 'St. Helena', 197);
INSERT INTO `cms_module_cge_countries` VALUES (198, 'PM', 'St. Pierre And Miquelon', 198);
INSERT INTO `cms_module_cge_countries` VALUES (199, 'SD', 'Sudan', 199);
INSERT INTO `cms_module_cge_countries` VALUES (200, 'SR', 'Suriname', 200);
INSERT INTO `cms_module_cge_countries` VALUES (201, 'SJ', 'Svalbard', 201);
INSERT INTO `cms_module_cge_countries` VALUES (202, 'SZ', 'Sw Aziland', 202);
INSERT INTO `cms_module_cge_countries` VALUES (203, 'SE', 'Sweden', 203);
INSERT INTO `cms_module_cge_countries` VALUES (204, 'CH', 'Switzerland', 204);
INSERT INTO `cms_module_cge_countries` VALUES (205, 'SY', 'Syrian Arab Republic', 205);
INSERT INTO `cms_module_cge_countries` VALUES (206, 'TW', 'Taiwan', 206);
INSERT INTO `cms_module_cge_countries` VALUES (207, 'TJ', 'Tajikistan', 207);
INSERT INTO `cms_module_cge_countries` VALUES (208, 'TZ', 'Tanzania', 208);
INSERT INTO `cms_module_cge_countries` VALUES (209, 'TH', 'Thailand', 209);
INSERT INTO `cms_module_cge_countries` VALUES (210, 'TG', 'Togo', 210);
INSERT INTO `cms_module_cge_countries` VALUES (211, 'TK', 'Tokelau', 211);
INSERT INTO `cms_module_cge_countries` VALUES (212, 'TO', 'Tonga', 212);
INSERT INTO `cms_module_cge_countries` VALUES (213, 'TT', 'Trinidad And Tobago', 213);
INSERT INTO `cms_module_cge_countries` VALUES (214, 'TN', 'Tunisia', 214);
INSERT INTO `cms_module_cge_countries` VALUES (215, 'TR', 'Turkey', 215);
INSERT INTO `cms_module_cge_countries` VALUES (216, 'TM', 'Turkmenistan', 216);
INSERT INTO `cms_module_cge_countries` VALUES (217, 'TC', 'Turks And Caicos Islands', 217);
INSERT INTO `cms_module_cge_countries` VALUES (218, 'TV', 'Tuvalu', 218);
INSERT INTO `cms_module_cge_countries` VALUES (219, 'UG', 'Uganda', 219);
INSERT INTO `cms_module_cge_countries` VALUES (220, 'UA', 'Ukraine', 220);
INSERT INTO `cms_module_cge_countries` VALUES (221, 'AE', 'United Arab Emirates', 221);
INSERT INTO `cms_module_cge_countries` VALUES (222, 'GB', 'United Kingdom', 222);
INSERT INTO `cms_module_cge_countries` VALUES (223, 'US', 'United States', 223);
INSERT INTO `cms_module_cge_countries` VALUES (224, 'UM', 'United States Minor Is.', 224);
INSERT INTO `cms_module_cge_countries` VALUES (225, 'UY', 'Uruguay', 225);
INSERT INTO `cms_module_cge_countries` VALUES (226, 'UZ', 'Uzbekistan', 226);
INSERT INTO `cms_module_cge_countries` VALUES (227, 'VU', 'Vanuatu', 227);
INSERT INTO `cms_module_cge_countries` VALUES (228, 'VE', 'Venezuela', 228);
INSERT INTO `cms_module_cge_countries` VALUES (229, 'VN', 'Viet Nam', 229);
INSERT INTO `cms_module_cge_countries` VALUES (230, 'VG', 'Virgin Islands (British)', 230);
INSERT INTO `cms_module_cge_countries` VALUES (231, 'VI', 'Virgin Islands (U.S.)', 231);
INSERT INTO `cms_module_cge_countries` VALUES (232, 'WF', 'Wallis And Futuna Islands', 232);
INSERT INTO `cms_module_cge_countries` VALUES (233, 'EH', 'Western Sahara', 233);
INSERT INTO `cms_module_cge_countries` VALUES (234, 'YE', 'Yemen', 234);
INSERT INTO `cms_module_cge_countries` VALUES (235, 'YU', 'Yugoslavia', 235);
INSERT INTO `cms_module_cge_countries` VALUES (236, 'ZR', 'Zaire', 236);
INSERT INTO `cms_module_cge_countries` VALUES (237, 'ZM', 'Zambia', 237);
INSERT INTO `cms_module_cge_countries` VALUES (238, 'ZW', 'Zimbabwe', 238);

-- ----------------------------
-- Table structure for cms_module_cge_states
-- ----------------------------
DROP TABLE IF EXISTS `cms_module_cge_states`;
CREATE TABLE `cms_module_cge_states`  (
  `id` int(11) NOT NULL,
  `code` varchar(2) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `sorting` int(11) NULL DEFAULT 0,
  PRIMARY KEY (`id`, `code`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of cms_module_cge_states
-- ----------------------------
INSERT INTO `cms_module_cge_states` VALUES (1, 'AL', 'Alabama', 1);
INSERT INTO `cms_module_cge_states` VALUES (2, 'AK', 'Alaska', 2);
INSERT INTO `cms_module_cge_states` VALUES (3, 'AS', 'American Samoa', 3);
INSERT INTO `cms_module_cge_states` VALUES (4, 'AZ', 'Arizona', 4);
INSERT INTO `cms_module_cge_states` VALUES (5, 'AR', 'Arkansas', 5);
INSERT INTO `cms_module_cge_states` VALUES (6, 'CA', 'California', 6);
INSERT INTO `cms_module_cge_states` VALUES (7, 'CO', 'Colorado', 7);
INSERT INTO `cms_module_cge_states` VALUES (8, 'CT', 'Connecticut', 8);
INSERT INTO `cms_module_cge_states` VALUES (9, 'DE', 'Delaware', 9);
INSERT INTO `cms_module_cge_states` VALUES (10, 'DC', 'District of Columbia', 10);
INSERT INTO `cms_module_cge_states` VALUES (11, 'FM', 'Fed. States of Micronesia', 11);
INSERT INTO `cms_module_cge_states` VALUES (12, 'FL', 'Florida', 12);
INSERT INTO `cms_module_cge_states` VALUES (13, 'GA', 'Georgia', 13);
INSERT INTO `cms_module_cge_states` VALUES (14, 'GU', 'Guam', 14);
INSERT INTO `cms_module_cge_states` VALUES (15, 'HI', 'Hawaii', 15);
INSERT INTO `cms_module_cge_states` VALUES (16, 'ID', 'Idaho', 16);
INSERT INTO `cms_module_cge_states` VALUES (17, 'IL', 'Illinois', 17);
INSERT INTO `cms_module_cge_states` VALUES (18, 'IN', 'Indiana', 18);
INSERT INTO `cms_module_cge_states` VALUES (19, 'IA', 'Iowa', 19);
INSERT INTO `cms_module_cge_states` VALUES (20, 'KS', 'Kansas', 20);
INSERT INTO `cms_module_cge_states` VALUES (21, 'KY', 'Kentucky', 21);
INSERT INTO `cms_module_cge_states` VALUES (22, 'LA', 'Louisiana', 22);
INSERT INTO `cms_module_cge_states` VALUES (23, 'ME', 'Maine', 23);
INSERT INTO `cms_module_cge_states` VALUES (24, 'MH', 'Marshall Islands', 24);
INSERT INTO `cms_module_cge_states` VALUES (25, 'MD', 'Maryland', 25);
INSERT INTO `cms_module_cge_states` VALUES (26, 'MA', 'Massachusetts', 26);
INSERT INTO `cms_module_cge_states` VALUES (27, 'MI', 'Michigan', 27);
INSERT INTO `cms_module_cge_states` VALUES (28, 'MN', 'Minnesota', 28);
INSERT INTO `cms_module_cge_states` VALUES (29, 'MS', 'Mississippi', 29);
INSERT INTO `cms_module_cge_states` VALUES (30, 'MO', 'Missouri', 30);
INSERT INTO `cms_module_cge_states` VALUES (31, 'MT', 'Montana', 31);
INSERT INTO `cms_module_cge_states` VALUES (32, 'NE', 'Nebraska', 32);
INSERT INTO `cms_module_cge_states` VALUES (33, 'NV', 'Nevada', 33);
INSERT INTO `cms_module_cge_states` VALUES (34, 'NH', 'New Hampshire', 34);
INSERT INTO `cms_module_cge_states` VALUES (35, 'NJ', 'New Jersey', 35);
INSERT INTO `cms_module_cge_states` VALUES (36, 'NM', 'New Mexico', 36);
INSERT INTO `cms_module_cge_states` VALUES (37, 'NY', 'New York', 37);
INSERT INTO `cms_module_cge_states` VALUES (38, 'NC', 'North Carolina', 38);
INSERT INTO `cms_module_cge_states` VALUES (39, 'ND', 'North Dakota', 39);
INSERT INTO `cms_module_cge_states` VALUES (40, 'MP', 'Northern Mariana Is.', 40);
INSERT INTO `cms_module_cge_states` VALUES (41, 'OH', 'Ohio', 41);
INSERT INTO `cms_module_cge_states` VALUES (42, 'OK', 'Oklahoma', 42);
INSERT INTO `cms_module_cge_states` VALUES (43, 'OR', 'Oregon', 43);
INSERT INTO `cms_module_cge_states` VALUES (44, 'PW', 'Palau', 44);
INSERT INTO `cms_module_cge_states` VALUES (45, 'PA', 'Pennsylvania', 45);
INSERT INTO `cms_module_cge_states` VALUES (46, 'PR', 'Puerto Rico', 46);
INSERT INTO `cms_module_cge_states` VALUES (47, 'RI', 'Rhode Island', 47);
INSERT INTO `cms_module_cge_states` VALUES (48, 'SC', 'South Carolina', 48);
INSERT INTO `cms_module_cge_states` VALUES (49, 'SD', 'South Dakota', 49);
INSERT INTO `cms_module_cge_states` VALUES (50, 'TN', 'Tennessee', 50);
INSERT INTO `cms_module_cge_states` VALUES (51, 'TX', 'Texas', 51);
INSERT INTO `cms_module_cge_states` VALUES (52, 'UT', 'Utah', 52);
INSERT INTO `cms_module_cge_states` VALUES (53, 'VT', 'Vermont', 53);
INSERT INTO `cms_module_cge_states` VALUES (54, 'VA', 'Virginia', 54);
INSERT INTO `cms_module_cge_states` VALUES (55, 'VI', 'Virgin Islands', 55);
INSERT INTO `cms_module_cge_states` VALUES (56, 'WA', 'Washington', 56);
INSERT INTO `cms_module_cge_states` VALUES (57, 'WV', 'West Virginia', 57);
INSERT INTO `cms_module_cge_states` VALUES (58, 'WI', 'Wisconsin', 58);
INSERT INTO `cms_module_cge_states` VALUES (59, 'WY', 'Wyoming', 59);
INSERT INTO `cms_module_cge_states` VALUES (60, 'AB', 'Alberta', 60);
INSERT INTO `cms_module_cge_states` VALUES (61, 'BC', 'British Columbia', 61);
INSERT INTO `cms_module_cge_states` VALUES (62, 'MB', 'Manitoba', 62);
INSERT INTO `cms_module_cge_states` VALUES (63, 'NB', 'New Brunswick', 63);
INSERT INTO `cms_module_cge_states` VALUES (64, 'NU', 'Nunavut', 64);
INSERT INTO `cms_module_cge_states` VALUES (65, 'NL', 'Newfoundland', 65);
INSERT INTO `cms_module_cge_states` VALUES (66, 'NT', 'Northwest Territories', 66);
INSERT INTO `cms_module_cge_states` VALUES (67, 'NS', 'Nova Scotia', 67);
INSERT INTO `cms_module_cge_states` VALUES (68, 'ON', 'Ontario', 68);
INSERT INTO `cms_module_cge_states` VALUES (69, 'PE', 'Prince Edward Island', 69);
INSERT INTO `cms_module_cge_states` VALUES (70, 'QC', 'Quebec', 70);
INSERT INTO `cms_module_cge_states` VALUES (71, 'SK', 'Saskatchewan', 71);
INSERT INTO `cms_module_cge_states` VALUES (72, 'YT', 'Yukon', 72);

-- ----------------------------
-- Table structure for cms_module_customgs
-- ----------------------------
DROP TABLE IF EXISTS `cms_module_customgs`;
CREATE TABLE `cms_module_customgs`  (
  `fieldid` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `help` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `type` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `properties` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `clearcache` int(11) NULL DEFAULT NULL,
  `sortorder` int(11) NULL DEFAULT NULL,
  `value` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `editors` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`fieldid`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of cms_module_customgs
-- ----------------------------
INSERT INTO `cms_module_customgs` VALUES (1, 'Facebook Conversion Code', 'Sample help text for this textfield', 'textarea', '1', 1, 1, '<!-- Facebook Conversion Code for Khách hàng retargetting -->\r\n<script>\r\n    (function () {\r\n        var _fbq = window._fbq || (window._fbq = []);\r\n        if (!_fbq.loaded) {\r\n            var fbds = document.createElement(\"script\");\r\n            fbds.async = true;\r\n            fbds.src = \"//connect.facebook.net/en_US/fbds.js\";\r\n            var s = document.getElementsByTagName(\"script\")[0];\r\n            s.parentNode.insertBefore(fbds, s);\r\n            _fbq.loaded = true;\r\n        }\r\n    })();\r\n    window._fbq = window._fbq || [];\r\n    window._fbq.push([\r\n        \"track\",\r\n        \"6032962319202\",\r\n        {\r\n            value: \"0.00\",\r\n            currency: \"VND\",\r\n        },\r\n    ]);\r\n</script>\r\n<noscript><img height=\"1\" width=\"1\" alt=\"\" style=\"display: none;\" src=\"https://www.facebook.com/tr?ev=6032962319202&amp;cd[value]=0.00&amp;cd[currency]=VND&amp;noscript=1\" /></noscript>', '2');

-- ----------------------------
-- Table structure for cms_module_deps
-- ----------------------------
DROP TABLE IF EXISTS `cms_module_deps`;
CREATE TABLE `cms_module_deps`  (
  `parent_module` varchar(25) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `child_module` varchar(25) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `minimum_version` varchar(25) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `create_date` datetime NULL DEFAULT NULL,
  `modified_date` datetime NULL DEFAULT NULL
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of cms_module_deps
-- ----------------------------
INSERT INTO `cms_module_deps` VALUES ('FileManager', 'MicroTiny', '1.4.2', '2015-04-09 15:18:54', '2015-04-09 15:18:54');
INSERT INTO `cms_module_deps` VALUES ('CGExtensions', 'CGContentUtils', '1.44.5', '2015-04-11 21:23:42', '2015-04-11 21:23:42');
INSERT INTO `cms_module_deps` VALUES ('CGExtensions', 'CGSmartImage', '1.44.3', '2015-04-11 21:24:10', '2015-04-11 21:24:10');
INSERT INTO `cms_module_deps` VALUES ('CGExtensions', 'JQueryTools', '1.42.2', '2015-04-11 21:24:53', '2015-04-11 21:24:53');
INSERT INTO `cms_module_deps` VALUES ('CGExtensions', 'MleCMS', '1.31', '2015-04-11 21:25:05', '2015-04-11 21:25:05');
INSERT INTO `cms_module_deps` VALUES ('ListIt2', 'ListIt2HeaderTexts', '1.4.1', '2015-04-24 23:30:43', '2015-04-24 23:30:43');
INSERT INTO `cms_module_deps` VALUES ('ListIt2', 'ListIt2DoubleList', '1.4', '2015-04-11 21:31:43', '2015-04-11 21:31:43');
INSERT INTO `cms_module_deps` VALUES ('ListIt2', 'ListIt2AboutUs', '1.4.1', '2015-04-24 23:58:43', '2015-04-24 23:58:43');
INSERT INTO `cms_module_deps` VALUES ('ListIt2', 'ListIt2Testimonials', '1.4.1', '2015-04-25 00:35:11', '2015-04-25 00:35:11');
INSERT INTO `cms_module_deps` VALUES ('ListIt2', 'ListIt2Social', '1.4.1', '2015-04-25 22:14:04', '2015-04-25 22:14:04');
INSERT INTO `cms_module_deps` VALUES ('ListIt2', 'ListIt2Menu', '1.4.1', '2015-04-25 22:21:17', '2015-04-25 22:21:17');
INSERT INTO `cms_module_deps` VALUES ('ListIt2', 'ListIt2Gallery', '1.4.1', '2015-04-26 01:00:36', '2015-04-26 01:00:36');
INSERT INTO `cms_module_deps` VALUES ('ListIt2', 'ListIt2Staff', '1.4.1', '2015-04-27 12:52:51', '2015-04-27 12:52:51');
INSERT INTO `cms_module_deps` VALUES ('ListIt2', 'ListIt2Slider', '1.4.1', '2015-04-27 13:18:47', '2015-04-27 13:18:47');
INSERT INTO `cms_module_deps` VALUES ('ListIt2', 'ListIt2Blog', '1.4.1', '2015-04-27 14:04:17', '2015-04-27 14:04:17');
INSERT INTO `cms_module_deps` VALUES ('CGExtensions', 'SiteMapMadeSimple', '1.38.5', '2015-07-10 14:55:51', '2015-07-10 14:55:51');

-- ----------------------------
-- Table structure for cms_module_errorlogger_log
-- ----------------------------
DROP TABLE IF EXISTS `cms_module_errorlogger_log`;
CREATE TABLE `cms_module_errorlogger_log`  (
  `log_ID` int(11) NOT NULL,
  `log_time` varchar(19) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `log_type` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `log_message` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `log_location` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`log_ID`) USING BTREE,
  INDEX `workshops_1`(`log_ID`) USING BTREE,
  INDEX `workshops_2`(`log_time`) USING BTREE,
  INDEX `workshops_3`(`log_type`) USING BTREE,
  INDEX `workshops_4`(`log_message`) USING BTREE,
  INDEX `workshops_5`(`log_location`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of cms_module_errorlogger_log
-- ----------------------------

-- ----------------------------
-- Table structure for cms_module_errorlogger_log_seq
-- ----------------------------
DROP TABLE IF EXISTS `cms_module_errorlogger_log_seq`;
CREATE TABLE `cms_module_errorlogger_log_seq`  (
  `id` int(11) NOT NULL
) ENGINE = MyISAM CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = FIXED;

-- ----------------------------
-- Records of cms_module_errorlogger_log_seq
-- ----------------------------
INSERT INTO `cms_module_errorlogger_log_seq` VALUES (1);

-- ----------------------------
-- Table structure for cms_module_eventslisting_category
-- ----------------------------
DROP TABLE IF EXISTS `cms_module_eventslisting_category`;
CREATE TABLE `cms_module_eventslisting_category`  (
  `entry_id` int(11) NOT NULL,
  `short_desc` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `long_desc` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `image_url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `sort_order` int(11) NULL DEFAULT NULL,
  `active` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`entry_id`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of cms_module_eventslisting_category
-- ----------------------------

-- ----------------------------
-- Table structure for cms_module_eventslisting_category_events
-- ----------------------------
DROP TABLE IF EXISTS `cms_module_eventslisting_category_events`;
CREATE TABLE `cms_module_eventslisting_category_events`  (
  `cat_id` int(11) NULL DEFAULT NULL,
  `event_id` int(11) NULL DEFAULT NULL,
  `modified_date` datetime NULL DEFAULT NULL
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = FIXED;

-- ----------------------------
-- Records of cms_module_eventslisting_category_events
-- ----------------------------

-- ----------------------------
-- Table structure for cms_module_eventslisting_events
-- ----------------------------
DROP TABLE IF EXISTS `cms_module_eventslisting_events`;
CREATE TABLE `cms_module_eventslisting_events`  (
  `entry_id` int(11) NOT NULL,
  `short_desc` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `long_desc` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `detail_link` int(11) NULL DEFAULT NULL,
  `start_date` datetime NULL DEFAULT NULL,
  `end_date` datetime NULL DEFAULT NULL,
  `image_url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `sort_order` int(11) NULL DEFAULT NULL,
  `active` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`entry_id`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of cms_module_eventslisting_events
-- ----------------------------

-- ----------------------------
-- Table structure for cms_module_eventslisting_templates
-- ----------------------------
DROP TABLE IF EXISTS `cms_module_eventslisting_templates`;
CREATE TABLE `cms_module_eventslisting_templates`  (
  `entry_id` int(11) NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `content` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  PRIMARY KEY (`entry_id`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of cms_module_eventslisting_templates
-- ----------------------------

-- ----------------------------
-- Table structure for cms_module_listit2_fielddefs
-- ----------------------------
DROP TABLE IF EXISTS `cms_module_listit2_fielddefs`;
CREATE TABLE `cms_module_listit2_fielddefs`  (
  `type` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `originator` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `path` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `active` int(11) NULL DEFAULT NULL,
  `disabled` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`type`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of cms_module_listit2_fielddefs
-- ----------------------------
INSERT INTO `cms_module_listit2_fielddefs` VALUES ('Categories', 'ListIt2', '/home/forge/khoanganxuavanay.com/modules/ListIt2/lib/fielddefs/Categories', 1, 0);
INSERT INTO `cms_module_listit2_fielddefs` VALUES ('Checkbox', 'ListIt2', '/home/forge/khoanganxuavanay.com/modules/ListIt2/lib/fielddefs/Checkbox', 1, 0);
INSERT INTO `cms_module_listit2_fielddefs` VALUES ('CheckboxGroup', 'ListIt2', '/home/forge/khoanganxuavanay.com/modules/ListIt2/lib/fielddefs/CheckboxGroup', 1, 0);
INSERT INTO `cms_module_listit2_fielddefs` VALUES ('ContentPages', 'ListIt2', '/home/forge/khoanganxuavanay.com/modules/ListIt2/lib/fielddefs/ContentPages', 1, 0);
INSERT INTO `cms_module_listit2_fielddefs` VALUES ('Dropdown', 'ListIt2', '/home/forge/khoanganxuavanay.com/modules/ListIt2/lib/fielddefs/Dropdown', 1, 0);
INSERT INTO `cms_module_listit2_fielddefs` VALUES ('FileUpload', 'ListIt2', '/home/forge/khoanganxuavanay.com/modules/ListIt2/lib/fielddefs/FileUpload', 1, 0);
INSERT INTO `cms_module_listit2_fielddefs` VALUES ('MultiSelect', 'ListIt2', '/home/forge/khoanganxuavanay.com/modules/ListIt2/lib/fielddefs/MultiSelect', 1, 0);
INSERT INTO `cms_module_listit2_fielddefs` VALUES ('RadioGroup', 'ListIt2', '/home/forge/khoanganxuavanay.com/modules/ListIt2/lib/fielddefs/RadioGroup', 1, 0);
INSERT INTO `cms_module_listit2_fielddefs` VALUES ('SelectDateTime', 'ListIt2', '/home/forge/khoanganxuavanay.com/modules/ListIt2/lib/fielddefs/SelectDateTime', 1, 0);
INSERT INTO `cms_module_listit2_fielddefs` VALUES ('SelectFile', 'ListIt2', '/home/forge/khoanganxuavanay.com/modules/ListIt2/lib/fielddefs/SelectFile', 1, 0);
INSERT INTO `cms_module_listit2_fielddefs` VALUES ('Slider', 'ListIt2', '/home/forge/khoanganxuavanay.com/modules/ListIt2/lib/fielddefs/Slider', 1, 0);
INSERT INTO `cms_module_listit2_fielddefs` VALUES ('TextArea', 'ListIt2', '/home/forge/khoanganxuavanay.com/modules/ListIt2/lib/fielddefs/TextArea', 1, 0);
INSERT INTO `cms_module_listit2_fielddefs` VALUES ('TextInput', 'ListIt2', '/home/forge/khoanganxuavanay.com/modules/ListIt2/lib/fielddefs/TextInput', 1, 0);
INSERT INTO `cms_module_listit2_fielddefs` VALUES ('DoubleList', 'ListIt2DoubleList', '/home/forge/khoanganxuavanay.com/modules/ListIt2DoubleList/fielddefs/DoubleList', 1, 0);

-- ----------------------------
-- Table structure for cms_module_listit2_instances
-- ----------------------------
DROP TABLE IF EXISTS `cms_module_listit2_instances`;
CREATE TABLE `cms_module_listit2_instances`  (
  `module_id` int(11) NOT NULL,
  `module_name` varchar(160) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`module_id`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of cms_module_listit2_instances
-- ----------------------------
INSERT INTO `cms_module_listit2_instances` VALUES (2, 'ListIt2HeaderTexts');
INSERT INTO `cms_module_listit2_instances` VALUES (3, 'ListIt2AboutUs');
INSERT INTO `cms_module_listit2_instances` VALUES (4, 'ListIt2Testimonials');
INSERT INTO `cms_module_listit2_instances` VALUES (5, 'ListIt2Social');
INSERT INTO `cms_module_listit2_instances` VALUES (6, 'ListIt2Menu');
INSERT INTO `cms_module_listit2_instances` VALUES (7, 'ListIt2Gallery');
INSERT INTO `cms_module_listit2_instances` VALUES (8, 'ListIt2Staff');
INSERT INTO `cms_module_listit2_instances` VALUES (9, 'ListIt2Slider');
INSERT INTO `cms_module_listit2_instances` VALUES (10, 'ListIt2Blog');

-- ----------------------------
-- Table structure for cms_module_listit2aboutus_category
-- ----------------------------
DROP TABLE IF EXISTS `cms_module_listit2aboutus_category`;
CREATE TABLE `cms_module_listit2aboutus_category`  (
  `category_id` int(11) NOT NULL,
  `category_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `category_alias` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `category_description` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `parent_id` int(11) NULL DEFAULT NULL,
  `position` int(11) NULL DEFAULT NULL,
  `hierarchy` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `hierarchy_path` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `id_hierarchy` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `active` int(11) NULL DEFAULT NULL,
  `create_date` datetime NULL DEFAULT NULL,
  `modified_date` datetime NULL DEFAULT NULL,
  `key1` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `key2` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `key3` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`category_id`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of cms_module_listit2aboutus_category
-- ----------------------------

-- ----------------------------
-- Table structure for cms_module_listit2aboutus_fielddef
-- ----------------------------
DROP TABLE IF EXISTS `cms_module_listit2aboutus_fielddef`;
CREATE TABLE `cms_module_listit2aboutus_fielddef`  (
  `fielddef_id` int(11) NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `alias` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `help` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `type` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `position` int(11) NULL DEFAULT NULL,
  `required` int(11) NULL DEFAULT NULL,
  `extra` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  PRIMARY KEY (`fielddef_id`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of cms_module_listit2aboutus_fielddef
-- ----------------------------
INSERT INTO `cms_module_listit2aboutus_fielddef` VALUES (1, 'Hình ảnh lớn (430px)', 'large', '', 'FileUpload', 1, 0, '');
INSERT INTO `cms_module_listit2aboutus_fielddef` VALUES (2, 'Hình ảnh nhỏ (180px)', 'small', '', 'FileUpload', 2, 0, '');
INSERT INTO `cms_module_listit2aboutus_fielddef` VALUES (3, 'Mô tả thông tin', 'descript', '', 'TextArea', 3, 0, NULL);

-- ----------------------------
-- Table structure for cms_module_listit2aboutus_fielddef_opts
-- ----------------------------
DROP TABLE IF EXISTS `cms_module_listit2aboutus_fielddef_opts`;
CREATE TABLE `cms_module_listit2aboutus_fielddef_opts`  (
  `fielddef_id` int(11) NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `value` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  PRIMARY KEY (`fielddef_id`, `name`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of cms_module_listit2aboutus_fielddef_opts
-- ----------------------------
INSERT INTO `cms_module_listit2aboutus_fielddef_opts` VALUES (1, 'dir', 'aboutus');
INSERT INTO `cms_module_listit2aboutus_fielddef_opts` VALUES (1, 'allowed', 'pdf,gif,jpeg,jpg,png');
INSERT INTO `cms_module_listit2aboutus_fielddef_opts` VALUES (2, 'allowed', 'pdf,gif,jpeg,jpg,png');
INSERT INTO `cms_module_listit2aboutus_fielddef_opts` VALUES (3, 'max_length', '6000');
INSERT INTO `cms_module_listit2aboutus_fielddef_opts` VALUES (3, 'wysiwyg', '1');
INSERT INTO `cms_module_listit2aboutus_fielddef_opts` VALUES (1, 'image', '1');
INSERT INTO `cms_module_listit2aboutus_fielddef_opts` VALUES (1, 'size', '20');
INSERT INTO `cms_module_listit2aboutus_fielddef_opts` VALUES (2, 'dir', 'aboutus');
INSERT INTO `cms_module_listit2aboutus_fielddef_opts` VALUES (2, 'image', '1');
INSERT INTO `cms_module_listit2aboutus_fielddef_opts` VALUES (2, 'size', '20');

-- ----------------------------
-- Table structure for cms_module_listit2aboutus_fieldval
-- ----------------------------
DROP TABLE IF EXISTS `cms_module_listit2aboutus_fieldval`;
CREATE TABLE `cms_module_listit2aboutus_fieldval`  (
  `item_id` int(11) NOT NULL,
  `fielddef_id` int(11) NOT NULL,
  `value_index` int(11) NOT NULL,
  `value` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `data` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  PRIMARY KEY (`item_id`, `fielddef_id`, `value_index`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of cms_module_listit2aboutus_fieldval
-- ----------------------------
INSERT INTO `cms_module_listit2aboutus_fieldval` VALUES (1, 1, 0, 'about-main.jpg', NULL);
INSERT INTO `cms_module_listit2aboutus_fieldval` VALUES (1, 2, 0, 'about-inset.jpg', NULL);
INSERT INTO `cms_module_listit2aboutus_fieldval` VALUES (1, 3, 0, '<p>Bạn thích khám phá và có trong tay một list dài các món ngon và nổi tiếng xứ Hà Thành? Hơn nữa, bạn lại còn đặc biệt thích các món ăn được chế biến từ gia cầm như gà, vịt, ngan…? Vậy thì chắc hẳn bạn không thể bỏ qua nhà hàng Khoa Ngan Xưa và Nay nổi tiếng tại 77 Hai Bà Trưng – một địa chỉ có từ lâu đời tại Hà Nội với đặc sản là các món ngan cực kỳ thơm ngon và hấp dẫn.</p>', NULL);
INSERT INTO `cms_module_listit2aboutus_fieldval` VALUES (2, 3, 0, '<p>Với thâm niên gần 30 năm hoạt động, nhà hàng <strong>Khoa Ngan Xưa và Nay</strong> đã trở thành địa chỉ cực kỳ quen thuộc với không ít người dân Hà Nội. Sở dĩ được yêu thích như vậy bởi nhà hàng sở hữu rất nhiều bí quyết hấp dẫn các thực khách: các món ngan đặc sản mang hương vị riêng biệt, không lẫn lộn với bất kỳ nơi nào khác, không gian nhà hàng rộng rãi và đẹp mắt cùng một đội ngũ nhân viên lịch sự và chu đáo.</p>', NULL);
INSERT INTO `cms_module_listit2aboutus_fieldval` VALUES (3, 3, 0, '<p>Đến với nhà hàng Khoa Ngan Xưa và Nay, đầu tiên bạn sẽ hoàn toàn yên tâm về chất lượng vệ sinh an toàn thực phẩm. Bởi <strong>toàn bộ nguyên liệu đều được nhập từ các địa phương lân cận Hà Nội đã được kiểm dịch và lựa chọn kỹ càng</strong> – “ngan vịt của nhà hàng Khoa Ngan cũng có đầy đủ hộ chiếu, chứng minh thư như ai” – như lời bà chủ vui tính của nhà hàng đã từng nói.</p>', NULL);
INSERT INTO `cms_module_listit2aboutus_fieldval` VALUES (4, 3, 0, '<p>Đầu vào hoàn toàn đảm bảo là một của bí quyết thành công, nhưng tất nhiên, nếu thiếu hương vị thơm ngon thì vẫn không đủ để giữ chân các thực khách Hà Thành vốn khá “kỹ tính”. <strong>Chỉ sở hữu một thực đơn đơn giản, không hề cầu kỳ, không có những cái tên nghe thật “đắt” nhưng các món ăn của nhà hàng vẫn dễ dàng ghi dấu ấn trong lòng các thực khách nhờ hương vị thật khó quên.</strong></p>', NULL);
INSERT INTO `cms_module_listit2aboutus_fieldval` VALUES (5, 3, 0, '<p>Các món ngon nổi tiếng và được yêu thích nhất của nhà hàng gồm có: <strong>Lẩu ngan, ngan nướng, bún ngan, ngan hấp, tiết canh ngan, dồi ngan, sụn ngan, lẩu cháo ngan… Trong đó món ngan nướng luôn được các thực khách lựa chọn hàng đầu. </strong>Sở dĩ được yêu thích như vậy bởi <strong>món ngan nướng đặc biệt được chế biến theo công thức của người Hàn Quốc</strong> do bà chủ của nhà hàng đã cất công sang tận nơi để tìm hiểu và học hỏi.</p>', NULL);

-- ----------------------------
-- Table structure for cms_module_listit2aboutus_item
-- ----------------------------
DROP TABLE IF EXISTS `cms_module_listit2aboutus_item`;
CREATE TABLE `cms_module_listit2aboutus_item`  (
  `item_id` int(11) NOT NULL,
  `category_id` int(11) NULL DEFAULT NULL,
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `alias` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `position` int(11) NULL DEFAULT NULL,
  `active` int(11) NULL DEFAULT NULL,
  `create_time` datetime NULL DEFAULT NULL,
  `modified_time` datetime NULL DEFAULT NULL,
  `start_time` date NULL DEFAULT NULL,
  `end_time` date NULL DEFAULT NULL,
  `key1` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `key2` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `key3` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `owner` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`item_id`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of cms_module_listit2aboutus_item
-- ----------------------------
INSERT INTO `cms_module_listit2aboutus_item` VALUES (1, NULL, 'Nhà Hàng Khoa Ngan - 77 Hai Bà Trưng', 'nha-hang-khoa-ngan-77-hai-ba-trung', 1, 1, '2015-04-25 00:04:38', '2015-04-30 23:30:46', NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO `cms_module_listit2aboutus_item` VALUES (2, NULL, 'Nhà Hàng Khoa Ngan - 77 Hai Bà Trưng', 'nha-hang-khoa-ngan-77-hai-ba-trung_2', 2, 0, '2015-04-27 21:51:29', '2015-04-27 21:51:29', NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO `cms_module_listit2aboutus_item` VALUES (3, NULL, 'Nhà Hàng Khoa Ngan - 77 Hai Bà Trưng', 'nha-hang-khoa-ngan-77-hai-ba-trung_3', 3, 0, '2015-04-27 21:53:25', '2015-04-27 21:53:25', NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO `cms_module_listit2aboutus_item` VALUES (4, NULL, 'Nhà Hàng Khoa Ngan - 77 Hai Bà Trưng', 'nha-hang-khoa-ngan-77-hai-ba-trung_4', 4, 0, '2015-04-27 21:53:44', '2015-04-27 21:53:44', NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO `cms_module_listit2aboutus_item` VALUES (5, NULL, 'Nhà Hàng Khoa Ngan - 77 Hai Bà Trưng', 'nha-hang-khoa-ngan-77-hai-ba-trung_5', 5, 0, '2015-04-27 21:54:02', '2015-04-27 21:54:02', NULL, NULL, NULL, NULL, NULL, 1);

-- ----------------------------
-- Table structure for cms_module_listit2aboutus_item_categories
-- ----------------------------
DROP TABLE IF EXISTS `cms_module_listit2aboutus_item_categories`;
CREATE TABLE `cms_module_listit2aboutus_item_categories`  (
  `item_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  PRIMARY KEY (`item_id`, `category_id`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = FIXED;

-- ----------------------------
-- Records of cms_module_listit2aboutus_item_categories
-- ----------------------------

-- ----------------------------
-- Table structure for cms_module_listit2blog_category
-- ----------------------------
DROP TABLE IF EXISTS `cms_module_listit2blog_category`;
CREATE TABLE `cms_module_listit2blog_category`  (
  `category_id` int(11) NOT NULL,
  `category_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `category_alias` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `category_description` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `parent_id` int(11) NULL DEFAULT NULL,
  `position` int(11) NULL DEFAULT NULL,
  `hierarchy` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `hierarchy_path` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `id_hierarchy` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `active` int(11) NULL DEFAULT NULL,
  `create_date` datetime NULL DEFAULT NULL,
  `modified_date` datetime NULL DEFAULT NULL,
  `key1` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `key2` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `key3` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`category_id`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of cms_module_listit2blog_category
-- ----------------------------

-- ----------------------------
-- Table structure for cms_module_listit2blog_fielddef
-- ----------------------------
DROP TABLE IF EXISTS `cms_module_listit2blog_fielddef`;
CREATE TABLE `cms_module_listit2blog_fielddef`  (
  `fielddef_id` int(11) NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `alias` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `help` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `type` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `position` int(11) NULL DEFAULT NULL,
  `required` int(11) NULL DEFAULT NULL,
  `extra` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  PRIMARY KEY (`fielddef_id`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of cms_module_listit2blog_fielddef
-- ----------------------------
INSERT INTO `cms_module_listit2blog_fielddef` VALUES (1, 'Hình ảnh', 'image', '', 'FileUpload', 1, 0, NULL);
INSERT INTO `cms_module_listit2blog_fielddef` VALUES (2, 'Mô tả', 'descript', '', 'TextArea', 2, 0, NULL);
INSERT INTO `cms_module_listit2blog_fielddef` VALUES (3, 'Nguồn', 'source', '', 'TextInput', 3, 0, '');
INSERT INTO `cms_module_listit2blog_fielddef` VALUES (4, 'Vị trí', 'position', '', 'RadioGroup', 0, 0, NULL);
INSERT INTO `cms_module_listit2blog_fielddef` VALUES (5, 'Link Nguồn', 'link', '', 'TextInput', 4, 0, NULL);

-- ----------------------------
-- Table structure for cms_module_listit2blog_fielddef_opts
-- ----------------------------
DROP TABLE IF EXISTS `cms_module_listit2blog_fielddef_opts`;
CREATE TABLE `cms_module_listit2blog_fielddef_opts`  (
  `fielddef_id` int(11) NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `value` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  PRIMARY KEY (`fielddef_id`, `name`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of cms_module_listit2blog_fielddef_opts
-- ----------------------------
INSERT INTO `cms_module_listit2blog_fielddef_opts` VALUES (1, 'allowed', 'pdf,gif,jpeg,jpg,png');
INSERT INTO `cms_module_listit2blog_fielddef_opts` VALUES (1, 'dir', 'blog');
INSERT INTO `cms_module_listit2blog_fielddef_opts` VALUES (1, 'image', '1');
INSERT INTO `cms_module_listit2blog_fielddef_opts` VALUES (1, 'size', '20');
INSERT INTO `cms_module_listit2blog_fielddef_opts` VALUES (2, 'max_length', '6000');
INSERT INTO `cms_module_listit2blog_fielddef_opts` VALUES (2, 'wysiwyg', '0');
INSERT INTO `cms_module_listit2blog_fielddef_opts` VALUES (3, 'max_length', '255');
INSERT INTO `cms_module_listit2blog_fielddef_opts` VALUES (3, 'size', '50');
INSERT INTO `cms_module_listit2blog_fielddef_opts` VALUES (4, 'options', 'Left = fadeInLeft\r\nRight = fadeInRight');
INSERT INTO `cms_module_listit2blog_fielddef_opts` VALUES (4, 'separator', '');
INSERT INTO `cms_module_listit2blog_fielddef_opts` VALUES (4, 'columns', '1');
INSERT INTO `cms_module_listit2blog_fielddef_opts` VALUES (4, 'jqui_buttons', '1');
INSERT INTO `cms_module_listit2blog_fielddef_opts` VALUES (5, 'size', '50');
INSERT INTO `cms_module_listit2blog_fielddef_opts` VALUES (5, 'max_length', '255');

-- ----------------------------
-- Table structure for cms_module_listit2blog_fieldval
-- ----------------------------
DROP TABLE IF EXISTS `cms_module_listit2blog_fieldval`;
CREATE TABLE `cms_module_listit2blog_fieldval`  (
  `item_id` int(11) NOT NULL,
  `fielddef_id` int(11) NOT NULL,
  `value_index` int(11) NOT NULL,
  `value` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `data` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  PRIMARY KEY (`item_id`, `fielddef_id`, `value_index`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of cms_module_listit2blog_fieldval
-- ----------------------------
INSERT INTO `cms_module_listit2blog_fieldval` VALUES (1, 3, 0, 'Khoa Ngan', NULL);
INSERT INTO `cms_module_listit2blog_fieldval` VALUES (2, 1, 0, 'set-499.jpg', NULL);
INSERT INTO `cms_module_listit2blog_fieldval` VALUES (2, 4, 0, 'fadeInRight', NULL);
INSERT INTO `cms_module_listit2blog_fieldval` VALUES (2, 3, 0, 'Khoa Ngan', NULL);
INSERT INTO `cms_module_listit2blog_fieldval` VALUES (1, 4, 0, 'fadeInLeft', NULL);
INSERT INTO `cms_module_listit2blog_fieldval` VALUES (1, 1, 0, 'happy-deal.jpg', NULL);
INSERT INTO `cms_module_listit2blog_fieldval` VALUES (5, 1, 0, 'sum-hop-gia-dinh.jpg', NULL);
INSERT INTO `cms_module_listit2blog_fieldval` VALUES (7, 1, 0, 'khuyen-mai-dang-cap.jpg', NULL);
INSERT INTO `cms_module_listit2blog_fieldval` VALUES (6, 1, 0, 'khong-the-tin-noi.jpg', NULL);
INSERT INTO `cms_module_listit2blog_fieldval` VALUES (5, 3, 0, 'Khoa Ngan', NULL);
INSERT INTO `cms_module_listit2blog_fieldval` VALUES (6, 3, 0, 'Khoa Ngan', NULL);
INSERT INTO `cms_module_listit2blog_fieldval` VALUES (7, 3, 0, 'Khoa Ngan', NULL);
INSERT INTO `cms_module_listit2blog_fieldval` VALUES (8, 4, 0, 'fadeInLeft', NULL);
INSERT INTO `cms_module_listit2blog_fieldval` VALUES (8, 1, 0, 'set1tr.jpg', NULL);
INSERT INTO `cms_module_listit2blog_fieldval` VALUES (8, 3, 0, 'Khoa Ngan', NULL);
INSERT INTO `cms_module_listit2blog_fieldval` VALUES (9, 4, 0, 'fadeInRight', NULL);
INSERT INTO `cms_module_listit2blog_fieldval` VALUES (9, 1, 0, '2-9.jpg', NULL);
INSERT INTO `cms_module_listit2blog_fieldval` VALUES (9, 3, 0, 'Khoa Ngan', NULL);
INSERT INTO `cms_module_listit2blog_fieldval` VALUES (10, 4, 0, 'fadeInLeft', NULL);
INSERT INTO `cms_module_listit2blog_fieldval` VALUES (10, 1, 0, 'vuitet-khoangan.jpg', NULL);
INSERT INTO `cms_module_listit2blog_fieldval` VALUES (10, 3, 0, 'Khoa Ngan', NULL);
INSERT INTO `cms_module_listit2blog_fieldval` VALUES (11, 4, 0, 'fadeInLeft', NULL);
INSERT INTO `cms_module_listit2blog_fieldval` VALUES (11, 1, 0, '17200748_830282993776260_539872617_o (1).jpg', NULL);
INSERT INTO `cms_module_listit2blog_fieldval` VALUES (11, 3, 0, 'Khoa Ngan', NULL);
INSERT INTO `cms_module_listit2blog_fieldval` VALUES (12, 4, 0, 'fadeInRight', NULL);
INSERT INTO `cms_module_listit2blog_fieldval` VALUES (12, 1, 0, '17199203_830283097109583_1212651654_n.jpg', NULL);
INSERT INTO `cms_module_listit2blog_fieldval` VALUES (12, 3, 0, 'Khoa Ngan', NULL);
INSERT INTO `cms_module_listit2blog_fieldval` VALUES (12, 5, 0, 'https://www.facebook.com/khoanganxuanay/posts/1237567793026045', NULL);
INSERT INTO `cms_module_listit2blog_fieldval` VALUES (11, 5, 0, 'https://www.facebook.com/khoanganxuanay/posts/1237554159694075', NULL);
INSERT INTO `cms_module_listit2blog_fieldval` VALUES (13, 4, 0, 'fadeInLeft', NULL);
INSERT INTO `cms_module_listit2blog_fieldval` VALUES (13, 3, 0, 'Khoa Ngan', NULL);
INSERT INTO `cms_module_listit2blog_fieldval` VALUES (13, 1, 0, 'Combo-2ng---1.jpg', NULL);
INSERT INTO `cms_module_listit2blog_fieldval` VALUES (14, 4, 0, 'fadeInRight', NULL);
INSERT INTO `cms_module_listit2blog_fieldval` VALUES (14, 1, 0, 'Combo-3ng---1.jpg', NULL);
INSERT INTO `cms_module_listit2blog_fieldval` VALUES (14, 3, 0, 'Khoa Ngan', NULL);
INSERT INTO `cms_module_listit2blog_fieldval` VALUES (15, 4, 0, 'fadeInLeft', NULL);
INSERT INTO `cms_module_listit2blog_fieldval` VALUES (15, 1, 0, 'Combo-6ng---1.jpg', NULL);
INSERT INTO `cms_module_listit2blog_fieldval` VALUES (15, 3, 0, 'Khoa Ngan', NULL);

-- ----------------------------
-- Table structure for cms_module_listit2blog_item
-- ----------------------------
DROP TABLE IF EXISTS `cms_module_listit2blog_item`;
CREATE TABLE `cms_module_listit2blog_item`  (
  `item_id` int(11) NOT NULL,
  `category_id` int(11) NULL DEFAULT NULL,
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `alias` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `position` int(11) NULL DEFAULT NULL,
  `active` int(11) NULL DEFAULT NULL,
  `create_time` datetime NULL DEFAULT NULL,
  `modified_time` datetime NULL DEFAULT NULL,
  `start_time` date NULL DEFAULT NULL,
  `end_time` date NULL DEFAULT NULL,
  `key1` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `key2` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `key3` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `owner` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`item_id`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of cms_module_listit2blog_item
-- ----------------------------
INSERT INTO `cms_module_listit2blog_item` VALUES (1, NULL, 'Happy Deal', 'happy-deal', 0, 0, '2015-04-27 14:09:34', '2015-05-08 15:10:47', NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO `cms_module_listit2blog_item` VALUES (2, NULL, 'Set 499K', 'set-499k', 1, 0, '2015-04-27 14:10:09', '2015-05-08 15:11:27', NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO `cms_module_listit2blog_item` VALUES (5, NULL, 'Sum họp gia đình', 'sum-hop-gia-dinh', 2, 0, '2015-06-03 08:18:52', '2015-06-03 08:23:36', NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO `cms_module_listit2blog_item` VALUES (6, NULL, 'Không thể tin nổi', 'khong-the-tin-noi', 3, 0, '2015-06-03 08:19:39', '2015-06-03 08:23:42', NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO `cms_module_listit2blog_item` VALUES (7, NULL, 'Khuyến mại nhưng đẳng cấp', 'khuyen-mai-nhung-dang-cap', 4, 0, '2015-06-03 08:19:56', '2015-06-03 08:23:47', NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO `cms_module_listit2blog_item` VALUES (8, NULL, 'Tặng SET ăn 1 triệu', 'tang-set-an-1-trieu', 5, 0, '2015-09-17 22:26:26', '2015-09-17 22:26:26', NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO `cms_module_listit2blog_item` VALUES (9, NULL, 'Mừng Quốc Khánh 2-9', 'mung-quoc-khanh-2-9', 6, 0, '2015-09-17 22:37:11', '2015-09-17 22:37:11', NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO `cms_module_listit2blog_item` VALUES (10, NULL, 'Vui Tết Độc Lập - Tấp Nập Nhận Quà', 'vui-tet-doc-lap-tap-nap-nhan-qua', 7, 0, '2015-09-17 22:39:38', '2015-09-17 22:39:38', NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO `cms_module_listit2blog_item` VALUES (11, NULL, 'Tôn Vinh Đàn Ông', 'ton-vinh-dan-ong', 8, 0, '2017-03-07 17:45:27', '2017-03-07 17:46:55', NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO `cms_module_listit2blog_item` VALUES (12, NULL, 'Tôn Vinh Phụ Nữ', 'ton-vinh-phu-nu', 9, 0, '2017-03-07 17:46:23', '2017-03-07 17:46:23', NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO `cms_module_listit2blog_item` VALUES (13, NULL, 'Combo dành cho 02 người', 'combo-danh-cho-02-nguoi', 11, 1, '2019-07-02 09:27:00', '2019-07-02 09:33:35', NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO `cms_module_listit2blog_item` VALUES (14, NULL, 'Combo dành cho 03 người', 'combo-danh-cho-03-nguoi', 10, 1, '2019-07-02 09:34:05', '2019-07-02 09:34:05', NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO `cms_module_listit2blog_item` VALUES (15, NULL, 'Combo dành cho 06 người', 'combo-danh-cho-06-nguoi', 12, 1, '2019-07-02 09:37:05', '2019-07-02 09:37:05', NULL, NULL, NULL, NULL, NULL, 1);

-- ----------------------------
-- Table structure for cms_module_listit2blog_item_categories
-- ----------------------------
DROP TABLE IF EXISTS `cms_module_listit2blog_item_categories`;
CREATE TABLE `cms_module_listit2blog_item_categories`  (
  `item_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  PRIMARY KEY (`item_id`, `category_id`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = FIXED;

-- ----------------------------
-- Records of cms_module_listit2blog_item_categories
-- ----------------------------

-- ----------------------------
-- Table structure for cms_module_listit2gallery_category
-- ----------------------------
DROP TABLE IF EXISTS `cms_module_listit2gallery_category`;
CREATE TABLE `cms_module_listit2gallery_category`  (
  `category_id` int(11) NOT NULL,
  `category_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `category_alias` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `category_description` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `parent_id` int(11) NULL DEFAULT NULL,
  `position` int(11) NULL DEFAULT NULL,
  `hierarchy` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `hierarchy_path` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `id_hierarchy` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `active` int(11) NULL DEFAULT NULL,
  `create_date` datetime NULL DEFAULT NULL,
  `modified_date` datetime NULL DEFAULT NULL,
  `key1` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `key2` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `key3` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`category_id`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of cms_module_listit2gallery_category
-- ----------------------------
INSERT INTO `cms_module_listit2gallery_category` VALUES (1, 'Customers', 'customers', '', -1, 1, '00001', 'customers', '1', 1, '2015-09-17 22:06:06', '2015-09-17 22:06:06', NULL, NULL, NULL);
INSERT INTO `cms_module_listit2gallery_category` VALUES (2, 'Galleries', 'galleries', '', -1, 2, '00002', 'galleries', '2', 1, '2015-09-17 22:27:39', '2015-09-17 22:27:39', NULL, NULL, NULL);

-- ----------------------------
-- Table structure for cms_module_listit2gallery_fielddef
-- ----------------------------
DROP TABLE IF EXISTS `cms_module_listit2gallery_fielddef`;
CREATE TABLE `cms_module_listit2gallery_fielddef`  (
  `fielddef_id` int(11) NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `alias` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `help` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `type` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `position` int(11) NULL DEFAULT NULL,
  `required` int(11) NULL DEFAULT NULL,
  `extra` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  PRIMARY KEY (`fielddef_id`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of cms_module_listit2gallery_fielddef
-- ----------------------------
INSERT INTO `cms_module_listit2gallery_fielddef` VALUES (1, 'Hình ảnh', 'image', '560x460 | 560x960 | 1160x460 | 1160x960', 'FileUpload', 1, 0, '');
INSERT INTO `cms_module_listit2gallery_fielddef` VALUES (2, 'Wide', 'wide', 'Check khi chiều rộng của hình x2', 'Checkbox', 2, 0, '');
INSERT INTO `cms_module_listit2gallery_fielddef` VALUES (3, 'List', 'list', '', 'Categories', 3, 0, NULL);

-- ----------------------------
-- Table structure for cms_module_listit2gallery_fielddef_opts
-- ----------------------------
DROP TABLE IF EXISTS `cms_module_listit2gallery_fielddef_opts`;
CREATE TABLE `cms_module_listit2gallery_fielddef_opts`  (
  `fielddef_id` int(11) NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `value` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  PRIMARY KEY (`fielddef_id`, `name`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of cms_module_listit2gallery_fielddef_opts
-- ----------------------------
INSERT INTO `cms_module_listit2gallery_fielddef_opts` VALUES (1, 'image', '1');
INSERT INTO `cms_module_listit2gallery_fielddef_opts` VALUES (1, 'dir', 'gallery');
INSERT INTO `cms_module_listit2gallery_fielddef_opts` VALUES (1, 'allowed', 'pdf,gif,jpeg,jpg,png');
INSERT INTO `cms_module_listit2gallery_fielddef_opts` VALUES (1, 'size', '5');
INSERT INTO `cms_module_listit2gallery_fielddef_opts` VALUES (3, 'subtype', 'Dropdown');
INSERT INTO `cms_module_listit2gallery_fielddef_opts` VALUES (3, 'separator', '');
INSERT INTO `cms_module_listit2gallery_fielddef_opts` VALUES (3, 'columns', '1');
INSERT INTO `cms_module_listit2gallery_fielddef_opts` VALUES (3, 'size', '5');

-- ----------------------------
-- Table structure for cms_module_listit2gallery_fieldval
-- ----------------------------
DROP TABLE IF EXISTS `cms_module_listit2gallery_fieldval`;
CREATE TABLE `cms_module_listit2gallery_fieldval`  (
  `item_id` int(11) NOT NULL,
  `fielddef_id` int(11) NOT NULL,
  `value_index` int(11) NOT NULL,
  `value` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `data` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  PRIMARY KEY (`item_id`, `fielddef_id`, `value_index`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of cms_module_listit2gallery_fieldval
-- ----------------------------
INSERT INTO `cms_module_listit2gallery_fieldval` VALUES (1, 1, 0, 'gallery-01.jpg', NULL);
INSERT INTO `cms_module_listit2gallery_fieldval` VALUES (2, 1, 0, 'gallery-02.jpg', NULL);
INSERT INTO `cms_module_listit2gallery_fieldval` VALUES (3, 1, 0, 'gallery-03.jpg', NULL);
INSERT INTO `cms_module_listit2gallery_fieldval` VALUES (4, 1, 0, 'gallery-04.jpg', NULL);
INSERT INTO `cms_module_listit2gallery_fieldval` VALUES (5, 1, 0, 'gallery-05.jpg', NULL);
INSERT INTO `cms_module_listit2gallery_fieldval` VALUES (6, 1, 0, 'gallery-06.jpg', NULL);
INSERT INTO `cms_module_listit2gallery_fieldval` VALUES (5, 2, 0, '1', NULL);
INSERT INTO `cms_module_listit2gallery_fieldval` VALUES (7, 1, 0, 'gallery-07.jpg', NULL);
INSERT INTO `cms_module_listit2gallery_fieldval` VALUES (8, 1, 0, 'gallery-08.jpg', NULL);
INSERT INTO `cms_module_listit2gallery_fieldval` VALUES (9, 1, 0, 'gallery-09.jpg', NULL);
INSERT INTO `cms_module_listit2gallery_fieldval` VALUES (10, 1, 0, 'gallery-10.jpg', NULL);
INSERT INTO `cms_module_listit2gallery_fieldval` VALUES (10, 2, 0, '1', NULL);
INSERT INTO `cms_module_listit2gallery_fieldval` VALUES (7, 2, 0, '1', NULL);
INSERT INTO `cms_module_listit2gallery_fieldval` VALUES (11, 1, 0, 'gallery-11.jpg', NULL);
INSERT INTO `cms_module_listit2gallery_fieldval` VALUES (12, 1, 0, 'gallery-12.jpg', NULL);
INSERT INTO `cms_module_listit2gallery_fieldval` VALUES (13, 1, 0, 'gallery-13.jpg', NULL);
INSERT INTO `cms_module_listit2gallery_fieldval` VALUES (14, 1, 0, 'gallery-14.jpg', NULL);
INSERT INTO `cms_module_listit2gallery_fieldval` VALUES (15, 1, 0, 'gallery-15.jpg', NULL);
INSERT INTO `cms_module_listit2gallery_fieldval` VALUES (16, 1, 0, 'gallery-16.jpg', NULL);
INSERT INTO `cms_module_listit2gallery_fieldval` VALUES (17, 1, 0, 'gallery-17.jpg', NULL);
INSERT INTO `cms_module_listit2gallery_fieldval` VALUES (18, 1, 0, 'gallery-18.jpg', NULL);
INSERT INTO `cms_module_listit2gallery_fieldval` VALUES (19, 1, 0, 'gallery-19.jpg', NULL);
INSERT INTO `cms_module_listit2gallery_fieldval` VALUES (23, 1, 0, 'gallery-20.jpg', NULL);
INSERT INTO `cms_module_listit2gallery_fieldval` VALUES (20, 1, 0, 'gallery-td.jpg', NULL);
INSERT INTO `cms_module_listit2gallery_fieldval` VALUES (22, 1, 0, 'gallery-21.jpg', NULL);
INSERT INTO `cms_module_listit2gallery_fieldval` VALUES (24, 1, 0, 'gallery-tth.jpg', NULL);
INSERT INTO `cms_module_listit2gallery_fieldval` VALUES (25, 1, 0, 'kh01.jpg', NULL);
INSERT INTO `cms_module_listit2gallery_fieldval` VALUES (26, 1, 0, 'kh02.jpg', NULL);
INSERT INTO `cms_module_listit2gallery_fieldval` VALUES (27, 1, 0, 'kh03.jpg', NULL);
INSERT INTO `cms_module_listit2gallery_fieldval` VALUES (28, 1, 0, 'kh04.jpg', NULL);
INSERT INTO `cms_module_listit2gallery_fieldval` VALUES (29, 1, 0, 'kh05.jpg', NULL);
INSERT INTO `cms_module_listit2gallery_fieldval` VALUES (30, 1, 0, 'kh06.jpg', NULL);
INSERT INTO `cms_module_listit2gallery_fieldval` VALUES (31, 1, 0, 'kh07.jpg', NULL);
INSERT INTO `cms_module_listit2gallery_fieldval` VALUES (32, 1, 0, 'kh08.jpg', NULL);
INSERT INTO `cms_module_listit2gallery_fieldval` VALUES (33, 1, 0, 'kh09.jpg', NULL);
INSERT INTO `cms_module_listit2gallery_fieldval` VALUES (34, 1, 0, 'kh10.jpg', NULL);
INSERT INTO `cms_module_listit2gallery_fieldval` VALUES (35, 1, 0, 'kh11.jpg', NULL);
INSERT INTO `cms_module_listit2gallery_fieldval` VALUES (36, 1, 0, 'kh12.jpg', NULL);
INSERT INTO `cms_module_listit2gallery_fieldval` VALUES (37, 1, 0, 'kh13.jpg', NULL);
INSERT INTO `cms_module_listit2gallery_fieldval` VALUES (38, 1, 0, 'kh14.jpg', NULL);

-- ----------------------------
-- Table structure for cms_module_listit2gallery_item
-- ----------------------------
DROP TABLE IF EXISTS `cms_module_listit2gallery_item`;
CREATE TABLE `cms_module_listit2gallery_item`  (
  `item_id` int(11) NOT NULL,
  `category_id` int(11) NULL DEFAULT NULL,
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `alias` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `position` int(11) NULL DEFAULT NULL,
  `active` int(11) NULL DEFAULT NULL,
  `create_time` datetime NULL DEFAULT NULL,
  `modified_time` datetime NULL DEFAULT NULL,
  `start_time` date NULL DEFAULT NULL,
  `end_time` date NULL DEFAULT NULL,
  `key1` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `key2` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `key3` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `owner` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`item_id`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of cms_module_listit2gallery_item
-- ----------------------------
INSERT INTO `cms_module_listit2gallery_item` VALUES (1, NULL, 'Hình ảnh 1', 'hinh-anh-1', 0, 1, '2015-04-27 13:34:52', '2015-09-17 22:28:33', NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO `cms_module_listit2gallery_item` VALUES (2, NULL, 'Hình ảnh 2', 'hinh-anh-2', 1, 1, '2015-04-27 13:37:03', '2015-09-17 22:30:43', NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO `cms_module_listit2gallery_item` VALUES (3, NULL, 'Hình ảnh 3', 'hinh-anh-3', 3, 1, '2015-04-27 13:37:16', '2015-09-17 22:30:53', NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO `cms_module_listit2gallery_item` VALUES (4, NULL, 'Hình ảnh 4', 'hinh-anh-4', 5, 1, '2015-04-27 13:37:36', '2015-09-17 22:31:06', NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO `cms_module_listit2gallery_item` VALUES (5, NULL, 'Hình ảnh 5', 'hinh-anh-5', 4, 1, '2015-04-27 13:37:47', '2015-09-17 22:31:02', NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO `cms_module_listit2gallery_item` VALUES (6, NULL, 'Hình ảnh 6', 'hinh-anh-6', 2, 1, '2015-04-27 13:38:23', '2015-09-17 22:30:46', NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO `cms_module_listit2gallery_item` VALUES (7, NULL, 'Hình ảnh 7', 'hinh-anh-7', 6, 1, '2015-04-27 13:47:24', '2015-09-17 22:31:10', NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO `cms_module_listit2gallery_item` VALUES (8, NULL, 'Hình ảnh 8', 'hinh-anh-8', 7, 1, '2015-04-27 13:47:55', '2015-09-17 22:31:13', NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO `cms_module_listit2gallery_item` VALUES (9, NULL, 'Hình ảnh 9', 'hinh-anh-9', 8, 1, '2015-04-27 13:48:05', '2015-09-17 22:31:16', NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO `cms_module_listit2gallery_item` VALUES (10, NULL, 'Hình ảnh 10', 'hinh-anh-10', 9, 1, '2015-04-27 13:48:15', '2015-09-17 22:31:19', NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO `cms_module_listit2gallery_item` VALUES (11, NULL, 'Hình ảnh 11', 'hinh-anh-11', 10, 1, '2015-04-27 15:14:04', '2015-09-17 22:31:21', NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO `cms_module_listit2gallery_item` VALUES (12, NULL, 'Đàm Vĩnh Hưng - Khoa Ngan', 'dam-vinh-hung-khoa-ngan', 11, 1, '2015-04-27 15:14:24', '2015-09-17 22:31:26', NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO `cms_module_listit2gallery_item` VALUES (13, NULL, 'Quang Dũng - Khoa Ngan', 'quang-dung-khoa-ngan', 12, 1, '2015-04-27 15:14:35', '2015-09-17 22:31:30', NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO `cms_module_listit2gallery_item` VALUES (14, NULL, 'Bằng Kiều - Khoa Ngan', 'bang-kieu-khoa-ngan', 13, 1, '2015-04-27 15:14:48', '2015-09-17 22:31:36', NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO `cms_module_listit2gallery_item` VALUES (15, NULL, 'Hình ảnh 15', 'hinh-anh-15', 15, 1, '2015-04-27 15:15:07', '2015-09-17 22:31:42', NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO `cms_module_listit2gallery_item` VALUES (16, NULL, 'Hình ảnh 16', 'hinh-anh-16', 16, 1, '2015-04-27 15:15:17', '2015-09-17 22:31:45', NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO `cms_module_listit2gallery_item` VALUES (17, NULL, 'Khánh Phương - Khoa Ngan', 'khanh-phuong-khoa-ngan', 17, 1, '2015-04-27 15:15:28', '2015-09-17 22:31:50', NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO `cms_module_listit2gallery_item` VALUES (18, NULL, 'Hình ảnh 18', 'hinh-anh-18', 18, 1, '2015-04-27 15:15:38', '2015-09-17 22:31:58', NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO `cms_module_listit2gallery_item` VALUES (19, NULL, 'Hình ảnh 19', 'hinh-anh-19', 19, 1, '2015-04-27 15:15:46', '2015-09-17 22:32:00', NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO `cms_module_listit2gallery_item` VALUES (20, NULL, 'Tùng Dương - Khoa Ngan', 'tung-duong-khoa-ngan', 21, 1, '2015-04-27 15:15:59', '2015-09-17 22:32:06', NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO `cms_module_listit2gallery_item` VALUES (23, NULL, 'Gordon Ramsay Steak - Khoa Ngan', 'gordon-ramsay-steak-khoa-ngan', 14, 1, '2015-05-08 15:34:17', '2015-09-17 22:31:38', NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO `cms_module_listit2gallery_item` VALUES (22, NULL, 'Ramsay - Khoa Ngan', 'ramsay-khoa-ngan', 20, 1, '2015-05-08 15:24:13', '2015-09-17 22:32:06', NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO `cms_module_listit2gallery_item` VALUES (24, NULL, 'Tăng Thanh Hà - Khoa Ngan', 'tang-thanh-ha-khoa-ngan', 22, 1, '2015-09-17 21:54:05', '2015-09-17 22:32:08', NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO `cms_module_listit2gallery_item` VALUES (25, NULL, 'Khách hàng 01 - Khoa Ngan', 'khach-hang-01-khoa-ngan', 23, 1, '2015-09-17 22:08:09', '2015-09-17 22:08:09', NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO `cms_module_listit2gallery_item` VALUES (26, NULL, 'Khách hàng 02 - Khoa Ngan', 'khach-hang-02-khoa-ngan', 24, 1, '2015-09-17 22:14:47', '2015-09-17 22:14:47', NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO `cms_module_listit2gallery_item` VALUES (27, NULL, 'Khách hàng 03 - Khoa Ngan', 'khach-hang-03-khoa-ngan', 25, 1, '2015-09-17 22:17:08', '2015-09-17 22:17:08', NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO `cms_module_listit2gallery_item` VALUES (28, NULL, 'Khách hàng 04 - Khoa Ngan', 'khach-hang-04-khoa-ngan', 26, 1, '2015-09-17 22:17:26', '2015-09-17 22:17:26', NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO `cms_module_listit2gallery_item` VALUES (29, NULL, 'Khách hàng 05 - Khoa Ngan', 'khach-hang-05-khoa-ngan', 27, 1, '2015-09-17 22:17:39', '2015-09-17 22:17:39', NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO `cms_module_listit2gallery_item` VALUES (30, NULL, 'Khách hàng 06 - Khoa Ngan', 'khach-hang-06-khoa-ngan', 28, 1, '2015-09-17 22:17:55', '2015-09-17 22:17:55', NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO `cms_module_listit2gallery_item` VALUES (31, NULL, 'Khách hàng 07 - Khoa Ngan', 'khach-hang-07-khoa-ngan', 29, 1, '2015-09-17 22:18:07', '2015-09-17 22:18:07', NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO `cms_module_listit2gallery_item` VALUES (32, NULL, 'Khách hàng 08 - Khoa Ngan', 'khach-hang-08-khoa-ngan', 30, 1, '2015-09-17 22:18:17', '2015-09-17 22:18:17', NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO `cms_module_listit2gallery_item` VALUES (33, NULL, 'Khách hàng 09 - Khoa Ngan', 'khach-hang-09-khoa-ngan', 31, 1, '2015-09-17 22:18:27', '2015-09-17 22:18:27', NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO `cms_module_listit2gallery_item` VALUES (34, NULL, 'Khách hàng 10 - Khoa Ngan', 'khach-hang-10-khoa-ngan', 32, 1, '2015-09-17 22:18:41', '2015-09-17 22:18:41', NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO `cms_module_listit2gallery_item` VALUES (35, NULL, 'Khách hàng 11 - Khoa Ngan', 'khach-hang-11-khoa-ngan', 33, 1, '2015-09-17 22:18:56', '2015-09-17 22:18:56', NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO `cms_module_listit2gallery_item` VALUES (36, NULL, 'Khách hàng 12 - Khoa Ngan', 'khach-hang-12-khoa-ngan', 34, 1, '2015-09-17 22:19:19', '2015-09-17 22:19:19', NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO `cms_module_listit2gallery_item` VALUES (37, NULL, 'Khách hàng 13 - Khoa Ngan', 'khach-hang-13-khoa-ngan', 35, 1, '2015-09-17 23:00:51', '2015-09-17 23:00:51', NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO `cms_module_listit2gallery_item` VALUES (38, NULL, 'Khách hàng 14 - Khoa Ngan', 'khach-hang-14-khoa-ngan', 36, 1, '2015-09-17 23:01:33', '2015-09-17 23:01:33', NULL, NULL, NULL, NULL, NULL, 1);

-- ----------------------------
-- Table structure for cms_module_listit2gallery_item_categories
-- ----------------------------
DROP TABLE IF EXISTS `cms_module_listit2gallery_item_categories`;
CREATE TABLE `cms_module_listit2gallery_item_categories`  (
  `item_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  PRIMARY KEY (`item_id`, `category_id`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = FIXED;

-- ----------------------------
-- Records of cms_module_listit2gallery_item_categories
-- ----------------------------
INSERT INTO `cms_module_listit2gallery_item_categories` VALUES (1, 2);
INSERT INTO `cms_module_listit2gallery_item_categories` VALUES (2, 2);
INSERT INTO `cms_module_listit2gallery_item_categories` VALUES (3, 2);
INSERT INTO `cms_module_listit2gallery_item_categories` VALUES (4, 2);
INSERT INTO `cms_module_listit2gallery_item_categories` VALUES (5, 2);
INSERT INTO `cms_module_listit2gallery_item_categories` VALUES (6, 2);
INSERT INTO `cms_module_listit2gallery_item_categories` VALUES (7, 2);
INSERT INTO `cms_module_listit2gallery_item_categories` VALUES (8, 2);
INSERT INTO `cms_module_listit2gallery_item_categories` VALUES (9, 2);
INSERT INTO `cms_module_listit2gallery_item_categories` VALUES (10, 2);
INSERT INTO `cms_module_listit2gallery_item_categories` VALUES (11, 2);
INSERT INTO `cms_module_listit2gallery_item_categories` VALUES (12, 2);
INSERT INTO `cms_module_listit2gallery_item_categories` VALUES (13, 2);
INSERT INTO `cms_module_listit2gallery_item_categories` VALUES (14, 2);
INSERT INTO `cms_module_listit2gallery_item_categories` VALUES (15, 2);
INSERT INTO `cms_module_listit2gallery_item_categories` VALUES (16, 2);
INSERT INTO `cms_module_listit2gallery_item_categories` VALUES (17, 2);
INSERT INTO `cms_module_listit2gallery_item_categories` VALUES (18, 2);
INSERT INTO `cms_module_listit2gallery_item_categories` VALUES (19, 2);
INSERT INTO `cms_module_listit2gallery_item_categories` VALUES (20, 2);
INSERT INTO `cms_module_listit2gallery_item_categories` VALUES (22, 2);
INSERT INTO `cms_module_listit2gallery_item_categories` VALUES (23, 2);
INSERT INTO `cms_module_listit2gallery_item_categories` VALUES (24, 2);
INSERT INTO `cms_module_listit2gallery_item_categories` VALUES (25, 1);
INSERT INTO `cms_module_listit2gallery_item_categories` VALUES (26, 1);
INSERT INTO `cms_module_listit2gallery_item_categories` VALUES (27, 1);
INSERT INTO `cms_module_listit2gallery_item_categories` VALUES (28, 1);
INSERT INTO `cms_module_listit2gallery_item_categories` VALUES (29, 1);
INSERT INTO `cms_module_listit2gallery_item_categories` VALUES (30, 1);
INSERT INTO `cms_module_listit2gallery_item_categories` VALUES (31, 1);
INSERT INTO `cms_module_listit2gallery_item_categories` VALUES (32, 1);
INSERT INTO `cms_module_listit2gallery_item_categories` VALUES (33, 1);
INSERT INTO `cms_module_listit2gallery_item_categories` VALUES (34, 1);
INSERT INTO `cms_module_listit2gallery_item_categories` VALUES (35, 1);
INSERT INTO `cms_module_listit2gallery_item_categories` VALUES (36, 1);
INSERT INTO `cms_module_listit2gallery_item_categories` VALUES (37, 1);
INSERT INTO `cms_module_listit2gallery_item_categories` VALUES (38, 1);

-- ----------------------------
-- Table structure for cms_module_listit2headertexts_category
-- ----------------------------
DROP TABLE IF EXISTS `cms_module_listit2headertexts_category`;
CREATE TABLE `cms_module_listit2headertexts_category`  (
  `category_id` int(11) NOT NULL,
  `category_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `category_alias` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `category_description` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `parent_id` int(11) NULL DEFAULT NULL,
  `position` int(11) NULL DEFAULT NULL,
  `hierarchy` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `hierarchy_path` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `id_hierarchy` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `active` int(11) NULL DEFAULT NULL,
  `create_date` datetime NULL DEFAULT NULL,
  `modified_date` datetime NULL DEFAULT NULL,
  `key1` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `key2` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `key3` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`category_id`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of cms_module_listit2headertexts_category
-- ----------------------------

-- ----------------------------
-- Table structure for cms_module_listit2headertexts_fielddef
-- ----------------------------
DROP TABLE IF EXISTS `cms_module_listit2headertexts_fielddef`;
CREATE TABLE `cms_module_listit2headertexts_fielddef`  (
  `fielddef_id` int(11) NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `alias` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `help` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `type` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `position` int(11) NULL DEFAULT NULL,
  `required` int(11) NULL DEFAULT NULL,
  `extra` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  PRIMARY KEY (`fielddef_id`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of cms_module_listit2headertexts_fielddef
-- ----------------------------

-- ----------------------------
-- Table structure for cms_module_listit2headertexts_fielddef_opts
-- ----------------------------
DROP TABLE IF EXISTS `cms_module_listit2headertexts_fielddef_opts`;
CREATE TABLE `cms_module_listit2headertexts_fielddef_opts`  (
  `fielddef_id` int(11) NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `value` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  PRIMARY KEY (`fielddef_id`, `name`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of cms_module_listit2headertexts_fielddef_opts
-- ----------------------------

-- ----------------------------
-- Table structure for cms_module_listit2headertexts_fieldval
-- ----------------------------
DROP TABLE IF EXISTS `cms_module_listit2headertexts_fieldval`;
CREATE TABLE `cms_module_listit2headertexts_fieldval`  (
  `item_id` int(11) NOT NULL,
  `fielddef_id` int(11) NOT NULL,
  `value_index` int(11) NOT NULL,
  `value` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `data` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  PRIMARY KEY (`item_id`, `fielddef_id`, `value_index`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of cms_module_listit2headertexts_fieldval
-- ----------------------------

-- ----------------------------
-- Table structure for cms_module_listit2headertexts_item
-- ----------------------------
DROP TABLE IF EXISTS `cms_module_listit2headertexts_item`;
CREATE TABLE `cms_module_listit2headertexts_item`  (
  `item_id` int(11) NOT NULL,
  `category_id` int(11) NULL DEFAULT NULL,
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `alias` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `position` int(11) NULL DEFAULT NULL,
  `active` int(11) NULL DEFAULT NULL,
  `create_time` datetime NULL DEFAULT NULL,
  `modified_time` datetime NULL DEFAULT NULL,
  `start_time` date NULL DEFAULT NULL,
  `end_time` date NULL DEFAULT NULL,
  `key1` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `key2` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `key3` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `owner` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`item_id`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of cms_module_listit2headertexts_item
-- ----------------------------
INSERT INTO `cms_module_listit2headertexts_item` VALUES (1, NULL, 'Nhà hàng chuyên các món ăn về Ngan đầu tiên và số 1 ở Hà Nội.', 'nha-hang-chuyen-cac-mon-an-ve-ngan-dau-tien-va-so-1-o-ha-noi', 1, 0, '2015-04-24 23:31:44', '2015-05-08 15:56:18', NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO `cms_module_listit2headertexts_item` VALUES (2, NULL, 'Siêu đầu bếp Gordon Ramsay - bạn thân của David Beckam đã tới đây', 'sieu-dau-bep-gordon-ramsay-ban-than-cua-david-beckam-da-toi-day', 2, 0, '2015-04-24 23:31:54', '2015-05-08 16:35:47', NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO `cms_module_listit2headertexts_item` VALUES (3, NULL, '	Nơi thưởng thức các món ăn ngon của những người nổi tiếng', 'noi-thuong-thuc-cac-mon-an-ngon-cua-nhung-nguoi-noi-tieng', 3, 0, '2015-04-24 23:35:29', '2015-05-08 16:36:15', NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO `cms_module_listit2headertexts_item` VALUES (4, NULL, '“ngan vịt của nhà hàng Khoa Ngan cũng có đầy đủ hộ chiếu, chứng minh thư như ai” – như lời bà chủ vui tính của nhà hàng đã từng nói', 'ngan-vit-cua-nha-hang-khoa-ngan-cung-co-day-du-ho-chieu-chung-minh-thu-nhu-ai-nhu-loi-ba-chu-vui-tinh-cua-nha-hang-da-tung-noi', 4, 0, '2015-04-24 23:35:35', '2015-05-08 16:36:32', NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO `cms_module_listit2headertexts_item` VALUES (5, NULL, 'Siêu đầu bếp Gordon Ramsay đã tự tay vặt lông ngan và học làm món Ngan nướng tại đây', 'sieu-dau-bep-gordon-ramsay-da-tu-tay-vat-long-ngan-va-hoc-lam-mon-ngan-nuong-tai-day', 5, 0, '2015-05-08 16:36:46', '2015-05-08 16:36:55', NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO `cms_module_listit2headertexts_item` VALUES (6, NULL, 'Chúc Quý khách ngon miệng', 'chuc-quy-khach-ngon-mieng', 6, 0, '2015-05-23 09:10:45', '2015-05-23 09:10:45', NULL, NULL, NULL, NULL, NULL, 1);

-- ----------------------------
-- Table structure for cms_module_listit2headertexts_item_categories
-- ----------------------------
DROP TABLE IF EXISTS `cms_module_listit2headertexts_item_categories`;
CREATE TABLE `cms_module_listit2headertexts_item_categories`  (
  `item_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  PRIMARY KEY (`item_id`, `category_id`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = FIXED;

-- ----------------------------
-- Records of cms_module_listit2headertexts_item_categories
-- ----------------------------

-- ----------------------------
-- Table structure for cms_module_listit2menu_category
-- ----------------------------
DROP TABLE IF EXISTS `cms_module_listit2menu_category`;
CREATE TABLE `cms_module_listit2menu_category`  (
  `category_id` int(11) NOT NULL,
  `category_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `category_alias` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `category_description` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `parent_id` int(11) NULL DEFAULT NULL,
  `position` int(11) NULL DEFAULT NULL,
  `hierarchy` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `hierarchy_path` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `id_hierarchy` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `active` int(11) NULL DEFAULT NULL,
  `create_date` datetime NULL DEFAULT NULL,
  `modified_date` datetime NULL DEFAULT NULL,
  `key1` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `key2` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `key3` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`category_id`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of cms_module_listit2menu_category
-- ----------------------------
INSERT INTO `cms_module_listit2menu_category` VALUES (1, 'Món mới', 'food', '', -1, 2, '00002', 'food', '1', 1, '2015-04-25 22:26:28', '2015-04-27 15:27:57', NULL, NULL, NULL);
INSERT INTO `cms_module_listit2menu_category` VALUES (2, 'Đồ uống', 'drink', '', -1, 4, '00004', 'drink', '2', 1, '2015-04-25 22:26:42', '2015-04-25 22:26:42', NULL, NULL, NULL);
INSERT INTO `cms_module_listit2menu_category` VALUES (3, 'Món truyền thống', 'mon-truyen-thong', '', -1, 1, '00001', 'mon-truyen-thong', '3', 1, '2015-04-27 15:27:24', '2015-04-27 15:27:24', NULL, NULL, NULL);
INSERT INTO `cms_module_listit2menu_category` VALUES (4, 'Món phụ', 'mon-phu', '', -1, 3, '00003', 'mon-phu', '4', 1, '2015-04-28 06:21:18', '2015-04-28 06:21:18', NULL, NULL, NULL);

-- ----------------------------
-- Table structure for cms_module_listit2menu_fielddef
-- ----------------------------
DROP TABLE IF EXISTS `cms_module_listit2menu_fielddef`;
CREATE TABLE `cms_module_listit2menu_fielddef`  (
  `fielddef_id` int(11) NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `alias` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `help` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `type` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `position` int(11) NULL DEFAULT NULL,
  `required` int(11) NULL DEFAULT NULL,
  `extra` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  PRIMARY KEY (`fielddef_id`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of cms_module_listit2menu_fielddef
-- ----------------------------
INSERT INTO `cms_module_listit2menu_fielddef` VALUES (1, 'Loại', 'category', '', 'Categories', 1, 0, '');
INSERT INTO `cms_module_listit2menu_fielddef` VALUES (2, 'Mô tả', 'description', '', 'TextArea', 2, 0, '');
INSERT INTO `cms_module_listit2menu_fielddef` VALUES (3, 'Tặng kèm', 'free', '', 'TextInput', 3, 0, '');
INSERT INTO `cms_module_listit2menu_fielddef` VALUES (4, 'Hình ảnh lớn', 'large', '', 'FileUpload', 4, 0, '');
INSERT INTO `cms_module_listit2menu_fielddef` VALUES (6, 'Giá', 'price', '', 'TextInput', 6, 0, '');
INSERT INTO `cms_module_listit2menu_fielddef` VALUES (7, 'Ghi chú', 'comment', '', 'TextInput', 8, 0, '');
INSERT INTO `cms_module_listit2menu_fielddef` VALUES (9, 'Trạng thái', 'status', '', 'RadioGroup', 9, 0, '');
INSERT INTO `cms_module_listit2menu_fielddef` VALUES (10, 'Đơn vị', 'donvi', '', 'Dropdown', 7, 0, '');
INSERT INTO `cms_module_listit2menu_fielddef` VALUES (11, 'English', 'titen', '', 'TextInput', 0, 0, NULL);
INSERT INTO `cms_module_listit2menu_fielddef` VALUES (12, 'Nhiều giá', 'from', '', 'Checkbox', 5, 0, NULL);

-- ----------------------------
-- Table structure for cms_module_listit2menu_fielddef_opts
-- ----------------------------
DROP TABLE IF EXISTS `cms_module_listit2menu_fielddef_opts`;
CREATE TABLE `cms_module_listit2menu_fielddef_opts`  (
  `fielddef_id` int(11) NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `value` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  PRIMARY KEY (`fielddef_id`, `name`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of cms_module_listit2menu_fielddef_opts
-- ----------------------------
INSERT INTO `cms_module_listit2menu_fielddef_opts` VALUES (1, 'columns', '1');
INSERT INTO `cms_module_listit2menu_fielddef_opts` VALUES (1, 'separator', '');
INSERT INTO `cms_module_listit2menu_fielddef_opts` VALUES (3, 'max_length', '255');
INSERT INTO `cms_module_listit2menu_fielddef_opts` VALUES (4, 'allowed', 'pdf,gif,jpeg,jpg,png');
INSERT INTO `cms_module_listit2menu_fielddef_opts` VALUES (7, 'max_length', '255');
INSERT INTO `cms_module_listit2menu_fielddef_opts` VALUES (9, 'options', 'Hot = hot\r\nSpecial = special');
INSERT INTO `cms_module_listit2menu_fielddef_opts` VALUES (9, 'jqui_buttons', '1');
INSERT INTO `cms_module_listit2menu_fielddef_opts` VALUES (9, 'columns', '1');
INSERT INTO `cms_module_listit2menu_fielddef_opts` VALUES (4, 'dir', 'menu/');
INSERT INTO `cms_module_listit2menu_fielddef_opts` VALUES (4, 'image', '1');
INSERT INTO `cms_module_listit2menu_fielddef_opts` VALUES (4, 'size', '20');
INSERT INTO `cms_module_listit2menu_fielddef_opts` VALUES (6, 'max_length', '255');
INSERT INTO `cms_module_listit2menu_fielddef_opts` VALUES (6, 'size', '5');
INSERT INTO `cms_module_listit2menu_fielddef_opts` VALUES (2, 'wysiwyg', '1');
INSERT INTO `cms_module_listit2menu_fielddef_opts` VALUES (3, 'size', '50');
INSERT INTO `cms_module_listit2menu_fielddef_opts` VALUES (1, 'size', '5');
INSERT INTO `cms_module_listit2menu_fielddef_opts` VALUES (1, 'subtype', 'Dropdown');
INSERT INTO `cms_module_listit2menu_fielddef_opts` VALUES (9, 'separator', '');
INSERT INTO `cms_module_listit2menu_fielddef_opts` VALUES (7, 'size', '50');
INSERT INTO `cms_module_listit2menu_fielddef_opts` VALUES (10, 'options', 'Suất = suất\r\nĐĩa = đĩa\r\nPhần = phần\r\nBát = bát\r\nNồi = nồi\r\nCon = con\r\nHộp = hộp\r\n----------------\r\nChai = chai\r\nLon = lon\r\nLy = ly\r\nChum = chum');
INSERT INTO `cms_module_listit2menu_fielddef_opts` VALUES (11, 'size', '50');
INSERT INTO `cms_module_listit2menu_fielddef_opts` VALUES (11, 'max_length', '255');
INSERT INTO `cms_module_listit2menu_fielddef_opts` VALUES (2, 'max_length', '6000');

-- ----------------------------
-- Table structure for cms_module_listit2menu_fieldval
-- ----------------------------
DROP TABLE IF EXISTS `cms_module_listit2menu_fieldval`;
CREATE TABLE `cms_module_listit2menu_fieldval`  (
  `item_id` int(11) NOT NULL,
  `fielddef_id` int(11) NOT NULL,
  `value_index` int(11) NOT NULL,
  `value` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `data` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  PRIMARY KEY (`item_id`, `fielddef_id`, `value_index`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of cms_module_listit2menu_fieldval
-- ----------------------------
INSERT INTO `cms_module_listit2menu_fieldval` VALUES (10, 10, 0, 'nồi', NULL);
INSERT INTO `cms_module_listit2menu_fieldval` VALUES (11, 6, 0, '100000', NULL);
INSERT INTO `cms_module_listit2menu_fieldval` VALUES (11, 10, 0, 'bát', NULL);
INSERT INTO `cms_module_listit2menu_fieldval` VALUES (12, 6, 0, '165000', NULL);
INSERT INTO `cms_module_listit2menu_fieldval` VALUES (12, 10, 0, 'nồi', NULL);
INSERT INTO `cms_module_listit2menu_fieldval` VALUES (13, 6, 0, '214500', NULL);
INSERT INTO `cms_module_listit2menu_fieldval` VALUES (13, 10, 0, 'nồi', NULL);
INSERT INTO `cms_module_listit2menu_fieldval` VALUES (14, 6, 0, '40000', NULL);
INSERT INTO `cms_module_listit2menu_fieldval` VALUES (14, 10, 0, 'bát', NULL);
INSERT INTO `cms_module_listit2menu_fieldval` VALUES (15, 6, 0, '100000', NULL);
INSERT INTO `cms_module_listit2menu_fieldval` VALUES (10, 6, 0, '16000', NULL);
INSERT INTO `cms_module_listit2menu_fieldval` VALUES (9, 10, 0, 'bát', NULL);
INSERT INTO `cms_module_listit2menu_fieldval` VALUES (9, 6, 0, '50000', NULL);
INSERT INTO `cms_module_listit2menu_fieldval` VALUES (7, 6, 0, '120000', NULL);
INSERT INTO `cms_module_listit2menu_fieldval` VALUES (8, 6, 0, '30000', NULL);
INSERT INTO `cms_module_listit2menu_fieldval` VALUES (3, 10, 0, 'đĩa', NULL);
INSERT INTO `cms_module_listit2menu_fieldval` VALUES (4, 10, 0, 'đĩa', NULL);
INSERT INTO `cms_module_listit2menu_fieldval` VALUES (5, 10, 0, 'đĩa', NULL);
INSERT INTO `cms_module_listit2menu_fieldval` VALUES (6, 10, 0, 'đĩa', NULL);
INSERT INTO `cms_module_listit2menu_fieldval` VALUES (8, 10, 0, 'suất', NULL);
INSERT INTO `cms_module_listit2menu_fieldval` VALUES (5, 6, 0, '60000', NULL);
INSERT INTO `cms_module_listit2menu_fieldval` VALUES (4, 6, 0, '150000', NULL);
INSERT INTO `cms_module_listit2menu_fieldval` VALUES (3, 6, 0, '90000', NULL);
INSERT INTO `cms_module_listit2menu_fieldval` VALUES (15, 10, 0, 'đĩa', NULL);
INSERT INTO `cms_module_listit2menu_fieldval` VALUES (16, 6, 0, '38500', NULL);
INSERT INTO `cms_module_listit2menu_fieldval` VALUES (16, 10, 0, 'đĩa', NULL);
INSERT INTO `cms_module_listit2menu_fieldval` VALUES (17, 6, 0, '180000', NULL);
INSERT INTO `cms_module_listit2menu_fieldval` VALUES (17, 10, 0, 'đĩa', NULL);
INSERT INTO `cms_module_listit2menu_fieldval` VALUES (18, 6, 0, '180000', NULL);
INSERT INTO `cms_module_listit2menu_fieldval` VALUES (18, 10, 0, 'đĩa', NULL);
INSERT INTO `cms_module_listit2menu_fieldval` VALUES (19, 6, 0, '100000', NULL);
INSERT INTO `cms_module_listit2menu_fieldval` VALUES (19, 10, 0, 'đĩa', NULL);
INSERT INTO `cms_module_listit2menu_fieldval` VALUES (20, 6, 0, '150000', NULL);
INSERT INTO `cms_module_listit2menu_fieldval` VALUES (20, 10, 0, 'bát', NULL);
INSERT INTO `cms_module_listit2menu_fieldval` VALUES (21, 6, 0, '20000', NULL);
INSERT INTO `cms_module_listit2menu_fieldval` VALUES (21, 10, 0, 'bát', NULL);
INSERT INTO `cms_module_listit2menu_fieldval` VALUES (22, 6, 0, '15000', NULL);
INSERT INTO `cms_module_listit2menu_fieldval` VALUES (22, 10, 0, 'đĩa', NULL);
INSERT INTO `cms_module_listit2menu_fieldval` VALUES (23, 6, 0, '35000', NULL);
INSERT INTO `cms_module_listit2menu_fieldval` VALUES (23, 10, 0, 'đĩa', NULL);
INSERT INTO `cms_module_listit2menu_fieldval` VALUES (24, 6, 0, '385000', NULL);
INSERT INTO `cms_module_listit2menu_fieldval` VALUES (24, 10, 0, 'đĩa', NULL);
INSERT INTO `cms_module_listit2menu_fieldval` VALUES (25, 6, 0, '40000', NULL);
INSERT INTO `cms_module_listit2menu_fieldval` VALUES (25, 10, 0, 'bát', NULL);
INSERT INTO `cms_module_listit2menu_fieldval` VALUES (26, 6, 0, '60000', NULL);
INSERT INTO `cms_module_listit2menu_fieldval` VALUES (26, 10, 0, 'bát', NULL);
INSERT INTO `cms_module_listit2menu_fieldval` VALUES (27, 6, 0, '25000', NULL);
INSERT INTO `cms_module_listit2menu_fieldval` VALUES (27, 10, 0, 'chai', NULL);
INSERT INTO `cms_module_listit2menu_fieldval` VALUES (28, 6, 0, '20000', NULL);
INSERT INTO `cms_module_listit2menu_fieldval` VALUES (28, 10, 0, 'chai', NULL);
INSERT INTO `cms_module_listit2menu_fieldval` VALUES (29, 6, 0, '20000', NULL);
INSERT INTO `cms_module_listit2menu_fieldval` VALUES (29, 10, 0, 'chai', NULL);
INSERT INTO `cms_module_listit2menu_fieldval` VALUES (30, 6, 0, '30000', NULL);
INSERT INTO `cms_module_listit2menu_fieldval` VALUES (30, 10, 0, 'chai', NULL);
INSERT INTO `cms_module_listit2menu_fieldval` VALUES (31, 6, 0, '140000', NULL);
INSERT INTO `cms_module_listit2menu_fieldval` VALUES (31, 10, 0, 'chai', NULL);
INSERT INTO `cms_module_listit2menu_fieldval` VALUES (32, 6, 0, '60000', NULL);
INSERT INTO `cms_module_listit2menu_fieldval` VALUES (32, 10, 0, 'chai', NULL);
INSERT INTO `cms_module_listit2menu_fieldval` VALUES (51, 11, 0, 'Passion juice', NULL);
INSERT INTO `cms_module_listit2menu_fieldval` VALUES (34, 6, 0, '130000', NULL);
INSERT INTO `cms_module_listit2menu_fieldval` VALUES (34, 10, 0, 'chai', NULL);
INSERT INTO `cms_module_listit2menu_fieldval` VALUES (35, 6, 0, '20000', NULL);
INSERT INTO `cms_module_listit2menu_fieldval` VALUES (35, 10, 0, 'ly', NULL);
INSERT INTO `cms_module_listit2menu_fieldval` VALUES (36, 6, 0, '40000', NULL);
INSERT INTO `cms_module_listit2menu_fieldval` VALUES (36, 10, 0, 'ly', NULL);
INSERT INTO `cms_module_listit2menu_fieldval` VALUES (15, 12, 0, '1', NULL);
INSERT INTO `cms_module_listit2menu_fieldval` VALUES (15, 11, 0, 'Boiled siamese duck', NULL);
INSERT INTO `cms_module_listit2menu_fieldval` VALUES (22, 11, 0, 'Boiled onion & beansprouts', NULL);
INSERT INTO `cms_module_listit2menu_fieldval` VALUES (41, 11, 0, 'Lavie', NULL);
INSERT INTO `cms_module_listit2menu_fieldval` VALUES (41, 6, 0, '20000', NULL);
INSERT INTO `cms_module_listit2menu_fieldval` VALUES (41, 10, 0, 'chai', NULL);
INSERT INTO `cms_module_listit2menu_fieldval` VALUES (3, 4, 0, 'nem ngan 90k.jpg', NULL);
INSERT INTO `cms_module_listit2menu_fieldval` VALUES (4, 4, 0, 'ngan-luc-lac-150k.jpg', NULL);
INSERT INTO `cms_module_listit2menu_fieldval` VALUES (19, 4, 0, 'long-me-hap-100k.jpg', NULL);
INSERT INTO `cms_module_listit2menu_fieldval` VALUES (8, 4, 0, 'kim-chi-khoa-ngan.jpg', NULL);
INSERT INTO `cms_module_listit2menu_fieldval` VALUES (6, 4, 0, 'luoi-ngan-chien-mam-100k.jpg', NULL);
INSERT INTO `cms_module_listit2menu_fieldval` VALUES (42, 10, 0, 'suất', NULL);
INSERT INTO `cms_module_listit2menu_fieldval` VALUES (42, 9, 0, 'hot', NULL);
INSERT INTO `cms_module_listit2menu_fieldval` VALUES (42, 4, 0, 'rau-song.jpg', NULL);
INSERT INTO `cms_module_listit2menu_fieldval` VALUES (22, 4, 0, 'hanh-gia-tran.jpg', NULL);
INSERT INTO `cms_module_listit2menu_fieldval` VALUES (27, 4, 0, 'bia-heineken.jpg', NULL);
INSERT INTO `cms_module_listit2menu_fieldval` VALUES (28, 4, 0, 'bia-hanoi.jpg', NULL);
INSERT INTO `cms_module_listit2menu_fieldval` VALUES (29, 4, 0, 'bia-saigon.jpg', NULL);
INSERT INTO `cms_module_listit2menu_fieldval` VALUES (30, 4, 0, 'bia-truc-bach.jpg', NULL);
INSERT INTO `cms_module_listit2menu_fieldval` VALUES (31, 4, 0, 'ruou-vodka-nga-borsmi.jpg', NULL);
INSERT INTO `cms_module_listit2menu_fieldval` VALUES (43, 4, 0, 'dua-leo.jpg', NULL);
INSERT INTO `cms_module_listit2menu_fieldval` VALUES (43, 10, 0, 'đĩa', NULL);
INSERT INTO `cms_module_listit2menu_fieldval` VALUES (43, 7, 0, 'Tặng 1 đĩa', NULL);
INSERT INTO `cms_module_listit2menu_fieldval` VALUES (43, 9, 0, 'special', NULL);
INSERT INTO `cms_module_listit2menu_fieldval` VALUES (43, 6, 0, '10000', NULL);
INSERT INTO `cms_module_listit2menu_fieldval` VALUES (44, 4, 0, 'canh-mang-tiet-ngan-60k.jpg', NULL);
INSERT INTO `cms_module_listit2menu_fieldval` VALUES (44, 10, 0, 'bát', NULL);
INSERT INTO `cms_module_listit2menu_fieldval` VALUES (60, 9, 0, 'hot', NULL);
INSERT INTO `cms_module_listit2menu_fieldval` VALUES (21, 4, 0, 'tiet-canh-ngan.jpg', NULL);
INSERT INTO `cms_module_listit2menu_fieldval` VALUES (44, 6, 0, '60000', NULL);
INSERT INTO `cms_module_listit2menu_fieldval` VALUES (45, 4, 0, 'bun.jpg', NULL);
INSERT INTO `cms_module_listit2menu_fieldval` VALUES (45, 6, 0, '10000', NULL);
INSERT INTO `cms_module_listit2menu_fieldval` VALUES (45, 10, 0, 'đĩa', NULL);
INSERT INTO `cms_module_listit2menu_fieldval` VALUES (23, 4, 0, 'khoai-tay-chien.jpg', NULL);
INSERT INTO `cms_module_listit2menu_fieldval` VALUES (14, 4, 0, 'bun-ngan.jpg', NULL);
INSERT INTO `cms_module_listit2menu_fieldval` VALUES (18, 4, 0, 'ngan-xao-lan-180k.jpg', NULL);
INSERT INTO `cms_module_listit2menu_fieldval` VALUES (17, 4, 0, 'ngan-nuong-180k.jpg', NULL);
INSERT INTO `cms_module_listit2menu_fieldval` VALUES (7, 4, 0, 'nem-thinh-ngan-120k.jpg', NULL);
INSERT INTO `cms_module_listit2menu_fieldval` VALUES (7, 10, 0, 'đĩa', NULL);
INSERT INTO `cms_module_listit2menu_fieldval` VALUES (46, 4, 0, 'ngan-om-sau.jpg', NULL);
INSERT INTO `cms_module_listit2menu_fieldval` VALUES (46, 6, 0, '370000', NULL);
INSERT INTO `cms_module_listit2menu_fieldval` VALUES (46, 10, 0, 'nồi', NULL);
INSERT INTO `cms_module_listit2menu_fieldval` VALUES (20, 4, 0, 'long-me-xao-dua.jpg', NULL);
INSERT INTO `cms_module_listit2menu_fieldval` VALUES (47, 4, 0, 'ngan-bao-vung-120k.jpg', NULL);
INSERT INTO `cms_module_listit2menu_fieldval` VALUES (47, 6, 0, '120000', NULL);
INSERT INTO `cms_module_listit2menu_fieldval` VALUES (47, 10, 0, 'đĩa', NULL);
INSERT INTO `cms_module_listit2menu_fieldval` VALUES (48, 6, 0, '200000', NULL);
INSERT INTO `cms_module_listit2menu_fieldval` VALUES (48, 10, 0, 'chai', NULL);
INSERT INTO `cms_module_listit2menu_fieldval` VALUES (49, 6, 0, '130000', NULL);
INSERT INTO `cms_module_listit2menu_fieldval` VALUES (49, 10, 0, 'chai', NULL);
INSERT INTO `cms_module_listit2menu_fieldval` VALUES (27, 11, 0, 'Heineken Beer', NULL);
INSERT INTO `cms_module_listit2menu_fieldval` VALUES (28, 11, 0, 'Ha Noi Beer', NULL);
INSERT INTO `cms_module_listit2menu_fieldval` VALUES (29, 11, 0, 'Sai Gon Beer', NULL);
INSERT INTO `cms_module_listit2menu_fieldval` VALUES (30, 11, 0, '\"Truc Bach\" Beer', NULL);
INSERT INTO `cms_module_listit2menu_fieldval` VALUES (50, 11, 0, 'Khoa Ngan Wine', NULL);
INSERT INTO `cms_module_listit2menu_fieldval` VALUES (50, 6, 0, '100000', NULL);
INSERT INTO `cms_module_listit2menu_fieldval` VALUES (50, 10, 0, 'chum', NULL);
INSERT INTO `cms_module_listit2menu_fieldval` VALUES (31, 11, 0, 'Borsmi Vodka', NULL);
INSERT INTO `cms_module_listit2menu_fieldval` VALUES (32, 11, 0, 'Ha Noi Vodka', NULL);
INSERT INTO `cms_module_listit2menu_fieldval` VALUES (48, 11, 0, 'Crocodile Vodka', NULL);
INSERT INTO `cms_module_listit2menu_fieldval` VALUES (49, 11, 0, 'Men Vodka', NULL);
INSERT INTO `cms_module_listit2menu_fieldval` VALUES (34, 11, 0, 'South Korean Soju rice liquor', NULL);
INSERT INTO `cms_module_listit2menu_fieldval` VALUES (32, 2, 0, 'Giá: 60.000 - 120.000/chai', NULL);
INSERT INTO `cms_module_listit2menu_fieldval` VALUES (51, 6, 0, '20000', NULL);
INSERT INTO `cms_module_listit2menu_fieldval` VALUES (51, 10, 0, 'ly', NULL);
INSERT INTO `cms_module_listit2menu_fieldval` VALUES (53, 11, 0, 'Soft drink (coca,squeezed orange,waky pumpkin,red bull)', NULL);
INSERT INTO `cms_module_listit2menu_fieldval` VALUES (53, 6, 0, '20000', NULL);
INSERT INTO `cms_module_listit2menu_fieldval` VALUES (53, 10, 0, 'lon', NULL);
INSERT INTO `cms_module_listit2menu_fieldval` VALUES (15, 2, 0, '<ul>\r\n<li>100.000/ suất/ 1 người </li>\r\n<li>180.000/ suất/ 2 người</li>\r\n<li>250.000/ suất/ 3 người</li>\r\n<li>320.000/ suất/ 4 người</li>\r\n</ul>', NULL);
INSERT INTO `cms_module_listit2menu_fieldval` VALUES (25, 4, 0, 'mien-ngan.jpg', NULL);
INSERT INTO `cms_module_listit2menu_fieldval` VALUES (54, 4, 0, 'sun-ngan-rang-muoi-120k.jpg', NULL);
INSERT INTO `cms_module_listit2menu_fieldval` VALUES (54, 6, 0, '120000', NULL);
INSERT INTO `cms_module_listit2menu_fieldval` VALUES (54, 10, 0, 'đĩa', NULL);
INSERT INTO `cms_module_listit2menu_fieldval` VALUES (5, 4, 0, 'doi-ngan-chien-2.jpg', NULL);
INSERT INTO `cms_module_listit2menu_fieldval` VALUES (15, 4, 0, 'ngan-luoc.jpg', NULL);
INSERT INTO `cms_module_listit2menu_fieldval` VALUES (66, 4, 0, 'ngan quay 180k.jpg', NULL);
INSERT INTO `cms_module_listit2menu_fieldval` VALUES (55, 4, 0, 'ngan-hap-180k.jpg', NULL);
INSERT INTO `cms_module_listit2menu_fieldval` VALUES (55, 12, 0, '1', NULL);
INSERT INTO `cms_module_listit2menu_fieldval` VALUES (55, 6, 0, '180000', NULL);
INSERT INTO `cms_module_listit2menu_fieldval` VALUES (55, 10, 0, 'suất', NULL);
INSERT INTO `cms_module_listit2menu_fieldval` VALUES (6, 6, 0, '100000', NULL);
INSERT INTO `cms_module_listit2menu_fieldval` VALUES (11, 4, 0, 'mang-tiet-long-me-100k.jpg', NULL);
INSERT INTO `cms_module_listit2menu_fieldval` VALUES (20, 2, 0, '<ul>\r\n<li>150.000/ suất/ 2 người </li>\r\n<li>180.000/ suất/ 3 người</li>\r\n</ul>', NULL);
INSERT INTO `cms_module_listit2menu_fieldval` VALUES (20, 12, 0, '1', NULL);
INSERT INTO `cms_module_listit2menu_fieldval` VALUES (56, 4, 0, 'canh-chien-sot-cay.jpg', NULL);
INSERT INTO `cms_module_listit2menu_fieldval` VALUES (56, 6, 0, '100000', NULL);
INSERT INTO `cms_module_listit2menu_fieldval` VALUES (56, 10, 0, 'đĩa', NULL);
INSERT INTO `cms_module_listit2menu_fieldval` VALUES (49, 4, 0, 'ruou-vodka-men.jpg', NULL);
INSERT INTO `cms_module_listit2menu_fieldval` VALUES (48, 4, 0, 'ruou-vodka-ca-sau.jpg', NULL);
INSERT INTO `cms_module_listit2menu_fieldval` VALUES (34, 4, 0, 'ruou-soju-han-quoc.jpg', NULL);
INSERT INTO `cms_module_listit2menu_fieldval` VALUES (51, 4, 0, 'nuoc-chanh-leo.jpg', NULL);
INSERT INTO `cms_module_listit2menu_fieldval` VALUES (35, 4, 0, 'nuoc-chanh-tuoi.jpg', NULL);
INSERT INTO `cms_module_listit2menu_fieldval` VALUES (36, 4, 0, 'nuoc-cam-tuoi.jpg', NULL);
INSERT INTO `cms_module_listit2menu_fieldval` VALUES (57, 11, 0, 'Vodka Danzka', NULL);
INSERT INTO `cms_module_listit2menu_fieldval` VALUES (57, 6, 0, '130000', NULL);
INSERT INTO `cms_module_listit2menu_fieldval` VALUES (57, 10, 0, 'chai', NULL);
INSERT INTO `cms_module_listit2menu_fieldval` VALUES (58, 6, 0, '100000', NULL);
INSERT INTO `cms_module_listit2menu_fieldval` VALUES (58, 10, 0, 'đĩa', NULL);
INSERT INTO `cms_module_listit2menu_fieldval` VALUES (59, 6, 0, '35000', NULL);
INSERT INTO `cms_module_listit2menu_fieldval` VALUES (59, 10, 0, 'đĩa', NULL);
INSERT INTO `cms_module_listit2menu_fieldval` VALUES (60, 10, 0, 'suất', NULL);
INSERT INTO `cms_module_listit2menu_fieldval` VALUES (61, 6, 0, '35000', NULL);
INSERT INTO `cms_module_listit2menu_fieldval` VALUES (61, 10, 0, 'đĩa', NULL);
INSERT INTO `cms_module_listit2menu_fieldval` VALUES (62, 6, 0, '35000', NULL);
INSERT INTO `cms_module_listit2menu_fieldval` VALUES (62, 10, 0, 'phần', NULL);
INSERT INTO `cms_module_listit2menu_fieldval` VALUES (63, 6, 0, '35000', NULL);
INSERT INTO `cms_module_listit2menu_fieldval` VALUES (63, 10, 0, 'đĩa', NULL);
INSERT INTO `cms_module_listit2menu_fieldval` VALUES (59, 4, 0, 'ngo-chien.jpg', NULL);
INSERT INTO `cms_module_listit2menu_fieldval` VALUES (26, 4, 0, 'mien-tron-60k.jpg', NULL);
INSERT INTO `cms_module_listit2menu_fieldval` VALUES (58, 4, 0, 'canh-ngan-sot-chua-ngot-100k.jpg', NULL);
INSERT INTO `cms_module_listit2menu_fieldval` VALUES (64, 4, 0, 'lau-ngan-400.jpg', NULL);
INSERT INTO `cms_module_listit2menu_fieldval` VALUES (64, 6, 0, '400000', NULL);
INSERT INTO `cms_module_listit2menu_fieldval` VALUES (64, 10, 0, 'suất', NULL);
INSERT INTO `cms_module_listit2menu_fieldval` VALUES (65, 4, 0, 'hop-ngan-06-mon-590k.jpg', NULL);
INSERT INTO `cms_module_listit2menu_fieldval` VALUES (65, 6, 0, '590000', NULL);
INSERT INTO `cms_module_listit2menu_fieldval` VALUES (65, 10, 0, 'hộp', NULL);
INSERT INTO `cms_module_listit2menu_fieldval` VALUES (65, 9, 0, 'special', NULL);
INSERT INTO `cms_module_listit2menu_fieldval` VALUES (66, 6, 0, '180000', NULL);
INSERT INTO `cms_module_listit2menu_fieldval` VALUES (66, 10, 0, 'đĩa', NULL);
INSERT INTO `cms_module_listit2menu_fieldval` VALUES (67, 4, 0, 'xoi-ngan-75k.jpg', NULL);
INSERT INTO `cms_module_listit2menu_fieldval` VALUES (67, 6, 0, '75000', NULL);
INSERT INTO `cms_module_listit2menu_fieldval` VALUES (67, 10, 0, 'đĩa', NULL);

-- ----------------------------
-- Table structure for cms_module_listit2menu_item
-- ----------------------------
DROP TABLE IF EXISTS `cms_module_listit2menu_item`;
CREATE TABLE `cms_module_listit2menu_item`  (
  `item_id` int(11) NOT NULL,
  `category_id` int(11) NULL DEFAULT NULL,
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `alias` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `position` int(11) NULL DEFAULT NULL,
  `active` int(11) NULL DEFAULT NULL,
  `create_time` datetime NULL DEFAULT NULL,
  `modified_time` datetime NULL DEFAULT NULL,
  `start_time` date NULL DEFAULT NULL,
  `end_time` date NULL DEFAULT NULL,
  `key1` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `key2` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `key3` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `owner` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`item_id`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of cms_module_listit2menu_item
-- ----------------------------
INSERT INTO `cms_module_listit2menu_item` VALUES (4, NULL, 'Ngan Lúc Lắc', 'ngan-luc-lac', 4, 1, '2015-04-27 15:18:21', '2020-11-24 14:57:39', NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO `cms_module_listit2menu_item` VALUES (3, NULL, 'Nem Ngan', 'nem-ngan', 2, 1, '2015-04-27 15:17:57', '2019-07-02 11:34:06', NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO `cms_module_listit2menu_item` VALUES (5, NULL, 'Dồi Ngan Chiên', 'doi-ngan-chien', 6, 1, '2015-04-27 15:18:39', '2020-11-24 14:58:11', NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO `cms_module_listit2menu_item` VALUES (6, NULL, 'Lưỡi Ngan Chiên Mắm', 'luoi-ngan-chien-mam', 7, 1, '2015-04-27 15:18:57', '2020-11-24 14:58:34', NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO `cms_module_listit2menu_item` VALUES (7, NULL, 'Nem thính Ngan', 'nem-thinh-ngan', 9, 1, '2015-04-27 15:19:12', '2020-11-24 14:59:00', NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO `cms_module_listit2menu_item` VALUES (8, NULL, 'Kim Chi Khoa Ngan', 'kim-chi-khoa-ngan', 11, 1, '2015-04-27 15:19:43', '2019-07-02 10:27:20', NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO `cms_module_listit2menu_item` VALUES (9, NULL, 'Cháo Ngan', 'chao-ngan', 14, 1, '2015-04-27 15:23:47', '2019-07-02 10:27:32', NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO `cms_module_listit2menu_item` VALUES (10, NULL, 'Canh Kim Chi Ngan', 'canh-kim-chi-ngan', 16, 0, '2015-04-27 15:24:04', '2015-04-27 15:24:04', NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO `cms_module_listit2menu_item` VALUES (11, NULL, 'Măng tiết lòng mề', 'mang-tiet-long-me', 19, 1, '2015-04-27 15:24:27', '2020-11-24 14:59:24', NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO `cms_module_listit2menu_item` VALUES (12, NULL, 'Ngan Giả Cầy', 'ngan-gia-cay', 23, 0, '2015-04-27 15:24:46', '2015-04-27 15:24:46', NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO `cms_module_listit2menu_item` VALUES (13, NULL, 'Đáy Lẩu Cháo Ngan', 'day-lau-chao-ngan', 24, 0, '2015-04-27 15:25:23', '2015-04-27 15:25:23', NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO `cms_module_listit2menu_item` VALUES (14, NULL, 'Bún Ngan', 'bun-ngan', 26, 1, '2015-04-27 15:28:34', '2015-05-05 05:10:42', NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO `cms_module_listit2menu_item` VALUES (15, NULL, 'Ngan Luộc', 'ngan-luoc', 28, 1, '2015-04-27 15:28:58', '2015-05-05 05:28:31', NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO `cms_module_listit2menu_item` VALUES (16, NULL, 'Cải xào Nấm', 'cai-xao-nam', 32, 1, '2015-04-27 15:29:32', '2015-04-27 15:29:32', NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO `cms_module_listit2menu_item` VALUES (17, NULL, 'Ngan Nướng', 'ngan-nuong', 34, 1, '2015-04-27 15:30:05', '2020-11-24 14:57:04', NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO `cms_module_listit2menu_item` VALUES (18, NULL, 'Ngan xào lăn', 'ngan-xao-lan', 35, 1, '2015-04-27 15:30:39', '2020-11-24 15:00:59', NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO `cms_module_listit2menu_item` VALUES (19, NULL, 'Lòng Mề hấp', 'long-me-hap', 36, 1, '2015-04-27 15:30:54', '2020-11-24 15:01:30', NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO `cms_module_listit2menu_item` VALUES (20, NULL, 'Lòng Mề xào Dứa/Giá', 'long-me-xao-dua-gia', 37, 1, '2015-04-27 15:31:21', '2015-05-05 05:44:57', NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO `cms_module_listit2menu_item` VALUES (21, NULL, 'Tiết Canh Ngan', 'tiet-canh-ngan', 40, 1, '2015-04-27 15:31:44', '2020-11-24 14:51:23', NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO `cms_module_listit2menu_item` VALUES (22, NULL, 'Hành Giá trần', 'hanh-gia-tran', 41, 1, '2015-04-27 15:32:12', '2015-05-04 15:37:40', NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO `cms_module_listit2menu_item` VALUES (23, NULL, 'Khoai Tây chiên', 'khoai-tay-chien', 3, 1, '2015-04-27 15:32:29', '2015-05-05 05:45:40', NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO `cms_module_listit2menu_item` VALUES (24, NULL, 'Su Su xào', 'su-su-xao', 5, 0, '2015-04-27 15:33:02', '2015-04-27 15:33:02', NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO `cms_module_listit2menu_item` VALUES (25, NULL, 'Miến Ngan', 'mien-ngan', 27, 1, '2015-04-27 15:33:50', '2015-05-05 05:14:16', NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO `cms_module_listit2menu_item` VALUES (26, NULL, 'Miến trộn', 'mien-tron', 8, 1, '2015-04-27 15:34:20', '2019-07-02 10:08:16', NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO `cms_module_listit2menu_item` VALUES (27, NULL, 'Bia Heineken', 'bia-heineken', 10, 1, '2015-04-27 15:35:25', '2015-05-04 15:21:00', NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO `cms_module_listit2menu_item` VALUES (28, NULL, 'Bia Hà Nội', 'bia-ha-noi', 12, 1, '2015-04-27 15:37:15', '2015-05-04 15:21:07', NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO `cms_module_listit2menu_item` VALUES (29, NULL, 'Bia Sài Gòn', 'bia-sai-gon', 13, 1, '2015-04-27 15:37:32', '2015-05-04 15:21:15', NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO `cms_module_listit2menu_item` VALUES (30, NULL, 'Bia Trúc Bạch', 'bia-truc-bach', 15, 1, '2015-04-27 15:37:53', '2015-05-04 15:21:28', NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO `cms_module_listit2menu_item` VALUES (31, NULL, 'Rượu Vodka Nga Borsmi', 'ruou-vodka-nga-borsmi', 18, 1, '2015-04-27 15:38:18', '2015-05-04 15:21:48', NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO `cms_module_listit2menu_item` VALUES (32, NULL, 'Rượu Vodka Hà Nội', 'ruou-vodka-ha-noi', 20, 0, '2015-04-27 15:38:31', '2015-05-04 15:22:37', NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO `cms_module_listit2menu_item` VALUES (51, NULL, 'Nước chanh leo', 'nuoc-chanh-leo', 30, 1, '2015-05-04 15:24:46', '2015-05-08 14:56:56', NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO `cms_module_listit2menu_item` VALUES (34, NULL, 'Rượu Soju hàn Quốc', 'ruou-soju-han-quoc', 25, 1, '2015-04-27 15:39:12', '2015-05-08 14:51:58', NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO `cms_module_listit2menu_item` VALUES (35, NULL, 'Nước chanh tươi', 'nuoc-chanh-tuoi', 31, 1, '2015-04-27 15:39:25', '2015-05-08 14:59:07', NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO `cms_module_listit2menu_item` VALUES (36, NULL, 'Nước cam tươi', 'nuoc-cam-tuoi', 33, 1, '2015-04-27 15:39:46', '2015-05-08 15:02:27', NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO `cms_module_listit2menu_item` VALUES (55, NULL, 'Ngan hấp', 'ngan-hap', 29, 1, '2015-05-05 05:30:43', '2020-11-24 14:59:51', NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO `cms_module_listit2menu_item` VALUES (54, NULL, 'Sụn Ngan rang muối', 'sun-ngan-rang-muoi', 46, 1, '2015-05-05 05:18:21', '2020-11-24 15:03:06', NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO `cms_module_listit2menu_item` VALUES (41, NULL, 'Nước khoáng', 'nuoc-khoang', 39, 1, '2015-04-27 15:41:28', '2015-05-08 15:06:29', NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO `cms_module_listit2menu_item` VALUES (42, NULL, 'Rau sống', 'rau-song', 0, 1, '2015-04-27 16:13:24', '2019-05-06 13:35:44', NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO `cms_module_listit2menu_item` VALUES (43, NULL, 'Dưa chuột chẻ', 'dua-chuot-che', 1, 1, '2015-04-28 06:04:07', '2015-05-05 05:46:15', NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO `cms_module_listit2menu_item` VALUES (44, NULL, 'Canh măng tiết Ngan', 'canh-mang-tiet-ngan', 42, 1, '2015-04-28 06:13:19', '2020-11-24 15:02:12', NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO `cms_module_listit2menu_item` VALUES (45, NULL, 'Bún rối', 'bun-roi', 43, 1, '2015-04-28 06:25:50', '2015-06-03 08:09:15', NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO `cms_module_listit2menu_item` VALUES (46, NULL, 'Ngan om sấu', 'ngan-om-sau', 44, 1, '2015-04-30 23:51:28', '2015-05-05 05:39:40', NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO `cms_module_listit2menu_item` VALUES (47, NULL, 'Ngan bao vừng', 'ngan-bao-vung', 45, 1, '2015-04-30 23:57:11', '2020-11-24 15:02:41', NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO `cms_module_listit2menu_item` VALUES (48, NULL, 'Rượu Vodka cá sấu', 'ruou-vodka-ca-sau', 21, 1, '2015-05-04 15:10:21', '2015-05-08 14:48:07', NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO `cms_module_listit2menu_item` VALUES (49, NULL, 'Rượu Vodka men', 'ruou-vodka-men', 22, 1, '2015-05-04 15:12:00', '2015-05-08 14:47:49', NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO `cms_module_listit2menu_item` VALUES (50, NULL, 'Rượu nếp Chum Khoa Ngan', 'ruou-nep-chum-khoa-ngan', 17, 1, '2015-05-04 15:15:33', '2015-05-08 14:40:36', NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO `cms_module_listit2menu_item` VALUES (57, NULL, 'Rượu Vodka nhôm', 'ruou-vodka-nhom', 48, 1, '2015-05-08 15:09:05', '2015-05-08 15:09:05', NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO `cms_module_listit2menu_item` VALUES (53, NULL, 'Nước ngọt', 'nuoc-ngot', 38, 1, '2015-05-04 15:27:25', '2015-05-08 15:05:00', NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO `cms_module_listit2menu_item` VALUES (56, NULL, 'Cánh chiên sốt cay', 'canh-chien-sot-cay', 47, 1, '2015-05-05 05:48:23', '2015-05-05 05:48:23', NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO `cms_module_listit2menu_item` VALUES (58, NULL, 'Cánh Ngan sốt chua ngọt', 'canh-ngan-sot-chua-ngot', 49, 1, '2015-05-08 17:03:36', '2020-11-24 15:03:26', NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO `cms_module_listit2menu_item` VALUES (59, NULL, 'Ngô chiên', 'ngo-chien', 50, 1, '2015-06-03 08:09:51', '2019-07-02 11:14:11', NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO `cms_module_listit2menu_item` VALUES (60, NULL, 'Rau cải xào nấm', 'rau-cai-xao-nam', 51, 1, '2015-06-03 08:11:28', '2019-07-02 10:30:05', NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO `cms_module_listit2menu_item` VALUES (61, NULL, 'Rau muống xào tỏi', 'rau-muong-xao-toi', 52, 1, '2015-06-03 08:11:46', '2015-06-03 08:17:24', NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO `cms_module_listit2menu_item` VALUES (62, NULL, 'Salat', 'salat', 53, 1, '2015-06-03 08:12:15', '2015-06-03 08:17:58', NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO `cms_module_listit2menu_item` VALUES (63, NULL, 'Xu xu xào tỏi', 'xu-xu-xao-toi', 54, 1, '2015-06-03 08:12:46', '2015-06-03 08:17:32', NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO `cms_module_listit2menu_item` VALUES (64, NULL, 'Lẩu Ngan', 'lau-ngan', 55, 1, '2019-07-02 10:14:47', '2020-11-24 14:54:07', NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO `cms_module_listit2menu_item` VALUES (65, NULL, 'Hộp Ngan 06 món', 'hop-ngan-06-mon', 56, 1, '2019-07-02 10:16:00', '2020-11-24 14:55:32', NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO `cms_module_listit2menu_item` VALUES (66, NULL, 'Ngan Quay', 'ngan-quay', 57, 1, '2019-07-02 10:25:11', '2019-07-02 10:25:11', NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO `cms_module_listit2menu_item` VALUES (67, NULL, 'Xôi Ngan', 'xoi-ngan', 58, 1, '2019-07-02 10:26:45', '2020-11-24 14:55:56', NULL, NULL, NULL, NULL, NULL, 1);

-- ----------------------------
-- Table structure for cms_module_listit2menu_item_categories
-- ----------------------------
DROP TABLE IF EXISTS `cms_module_listit2menu_item_categories`;
CREATE TABLE `cms_module_listit2menu_item_categories`  (
  `item_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  PRIMARY KEY (`item_id`, `category_id`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = FIXED;

-- ----------------------------
-- Records of cms_module_listit2menu_item_categories
-- ----------------------------
INSERT INTO `cms_module_listit2menu_item_categories` VALUES (3, 1);
INSERT INTO `cms_module_listit2menu_item_categories` VALUES (4, 1);
INSERT INTO `cms_module_listit2menu_item_categories` VALUES (5, 1);
INSERT INTO `cms_module_listit2menu_item_categories` VALUES (6, 1);
INSERT INTO `cms_module_listit2menu_item_categories` VALUES (7, 1);
INSERT INTO `cms_module_listit2menu_item_categories` VALUES (8, 3);
INSERT INTO `cms_module_listit2menu_item_categories` VALUES (9, 3);
INSERT INTO `cms_module_listit2menu_item_categories` VALUES (10, 1);
INSERT INTO `cms_module_listit2menu_item_categories` VALUES (11, 3);
INSERT INTO `cms_module_listit2menu_item_categories` VALUES (12, 1);
INSERT INTO `cms_module_listit2menu_item_categories` VALUES (13, 1);
INSERT INTO `cms_module_listit2menu_item_categories` VALUES (14, 3);
INSERT INTO `cms_module_listit2menu_item_categories` VALUES (15, 3);
INSERT INTO `cms_module_listit2menu_item_categories` VALUES (16, 3);
INSERT INTO `cms_module_listit2menu_item_categories` VALUES (17, 3);
INSERT INTO `cms_module_listit2menu_item_categories` VALUES (18, 3);
INSERT INTO `cms_module_listit2menu_item_categories` VALUES (19, 3);
INSERT INTO `cms_module_listit2menu_item_categories` VALUES (20, 3);
INSERT INTO `cms_module_listit2menu_item_categories` VALUES (21, 4);
INSERT INTO `cms_module_listit2menu_item_categories` VALUES (22, 4);
INSERT INTO `cms_module_listit2menu_item_categories` VALUES (23, 4);
INSERT INTO `cms_module_listit2menu_item_categories` VALUES (24, 3);
INSERT INTO `cms_module_listit2menu_item_categories` VALUES (25, 3);
INSERT INTO `cms_module_listit2menu_item_categories` VALUES (26, 3);
INSERT INTO `cms_module_listit2menu_item_categories` VALUES (27, 2);
INSERT INTO `cms_module_listit2menu_item_categories` VALUES (28, 2);
INSERT INTO `cms_module_listit2menu_item_categories` VALUES (29, 2);
INSERT INTO `cms_module_listit2menu_item_categories` VALUES (30, 2);
INSERT INTO `cms_module_listit2menu_item_categories` VALUES (31, 2);
INSERT INTO `cms_module_listit2menu_item_categories` VALUES (32, 2);
INSERT INTO `cms_module_listit2menu_item_categories` VALUES (34, 2);
INSERT INTO `cms_module_listit2menu_item_categories` VALUES (35, 2);
INSERT INTO `cms_module_listit2menu_item_categories` VALUES (36, 2);
INSERT INTO `cms_module_listit2menu_item_categories` VALUES (41, 2);
INSERT INTO `cms_module_listit2menu_item_categories` VALUES (42, 4);
INSERT INTO `cms_module_listit2menu_item_categories` VALUES (43, 4);
INSERT INTO `cms_module_listit2menu_item_categories` VALUES (44, 3);
INSERT INTO `cms_module_listit2menu_item_categories` VALUES (45, 4);
INSERT INTO `cms_module_listit2menu_item_categories` VALUES (46, 1);
INSERT INTO `cms_module_listit2menu_item_categories` VALUES (47, 1);
INSERT INTO `cms_module_listit2menu_item_categories` VALUES (48, 2);
INSERT INTO `cms_module_listit2menu_item_categories` VALUES (49, 2);
INSERT INTO `cms_module_listit2menu_item_categories` VALUES (50, 2);
INSERT INTO `cms_module_listit2menu_item_categories` VALUES (51, 2);
INSERT INTO `cms_module_listit2menu_item_categories` VALUES (53, 2);
INSERT INTO `cms_module_listit2menu_item_categories` VALUES (54, 1);
INSERT INTO `cms_module_listit2menu_item_categories` VALUES (55, 3);
INSERT INTO `cms_module_listit2menu_item_categories` VALUES (56, 1);
INSERT INTO `cms_module_listit2menu_item_categories` VALUES (57, 2);
INSERT INTO `cms_module_listit2menu_item_categories` VALUES (58, 1);
INSERT INTO `cms_module_listit2menu_item_categories` VALUES (59, 4);
INSERT INTO `cms_module_listit2menu_item_categories` VALUES (60, 4);
INSERT INTO `cms_module_listit2menu_item_categories` VALUES (61, 4);
INSERT INTO `cms_module_listit2menu_item_categories` VALUES (62, 4);
INSERT INTO `cms_module_listit2menu_item_categories` VALUES (63, 4);
INSERT INTO `cms_module_listit2menu_item_categories` VALUES (64, 1);
INSERT INTO `cms_module_listit2menu_item_categories` VALUES (65, 1);
INSERT INTO `cms_module_listit2menu_item_categories` VALUES (66, 1);
INSERT INTO `cms_module_listit2menu_item_categories` VALUES (67, 1);

-- ----------------------------
-- Table structure for cms_module_listit2slider_category
-- ----------------------------
DROP TABLE IF EXISTS `cms_module_listit2slider_category`;
CREATE TABLE `cms_module_listit2slider_category`  (
  `category_id` int(11) NOT NULL,
  `category_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `category_alias` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `category_description` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `parent_id` int(11) NULL DEFAULT NULL,
  `position` int(11) NULL DEFAULT NULL,
  `hierarchy` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `hierarchy_path` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `id_hierarchy` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `active` int(11) NULL DEFAULT NULL,
  `create_date` datetime NULL DEFAULT NULL,
  `modified_date` datetime NULL DEFAULT NULL,
  `key1` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `key2` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `key3` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`category_id`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of cms_module_listit2slider_category
-- ----------------------------

-- ----------------------------
-- Table structure for cms_module_listit2slider_fielddef
-- ----------------------------
DROP TABLE IF EXISTS `cms_module_listit2slider_fielddef`;
CREATE TABLE `cms_module_listit2slider_fielddef`  (
  `fielddef_id` int(11) NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `alias` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `help` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `type` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `position` int(11) NULL DEFAULT NULL,
  `required` int(11) NULL DEFAULT NULL,
  `extra` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  PRIMARY KEY (`fielddef_id`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of cms_module_listit2slider_fielddef
-- ----------------------------
INSERT INTO `cms_module_listit2slider_fielddef` VALUES (2, 'Hình ảnh', 'image', '', 'FileUpload', 1, 0, NULL);

-- ----------------------------
-- Table structure for cms_module_listit2slider_fielddef_opts
-- ----------------------------
DROP TABLE IF EXISTS `cms_module_listit2slider_fielddef_opts`;
CREATE TABLE `cms_module_listit2slider_fielddef_opts`  (
  `fielddef_id` int(11) NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `value` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  PRIMARY KEY (`fielddef_id`, `name`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of cms_module_listit2slider_fielddef_opts
-- ----------------------------
INSERT INTO `cms_module_listit2slider_fielddef_opts` VALUES (2, 'allowed', 'pdf,gif,jpeg,jpg,png');
INSERT INTO `cms_module_listit2slider_fielddef_opts` VALUES (2, 'dir', 'slider');
INSERT INTO `cms_module_listit2slider_fielddef_opts` VALUES (2, 'image', '1');
INSERT INTO `cms_module_listit2slider_fielddef_opts` VALUES (2, 'size', '20');

-- ----------------------------
-- Table structure for cms_module_listit2slider_fieldval
-- ----------------------------
DROP TABLE IF EXISTS `cms_module_listit2slider_fieldval`;
CREATE TABLE `cms_module_listit2slider_fieldval`  (
  `item_id` int(11) NOT NULL,
  `fielddef_id` int(11) NOT NULL,
  `value_index` int(11) NOT NULL,
  `value` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `data` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  PRIMARY KEY (`item_id`, `fielddef_id`, `value_index`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of cms_module_listit2slider_fieldval
-- ----------------------------
INSERT INTO `cms_module_listit2slider_fieldval` VALUES (1, 2, 0, 'slide-image-01.jpg', NULL);
INSERT INTO `cms_module_listit2slider_fieldval` VALUES (2, 2, 0, 'slide-image-02.jpg', NULL);
INSERT INTO `cms_module_listit2slider_fieldval` VALUES (3, 2, 0, 'slide-image-03.jpg', NULL);

-- ----------------------------
-- Table structure for cms_module_listit2slider_item
-- ----------------------------
DROP TABLE IF EXISTS `cms_module_listit2slider_item`;
CREATE TABLE `cms_module_listit2slider_item`  (
  `item_id` int(11) NOT NULL,
  `category_id` int(11) NULL DEFAULT NULL,
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `alias` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `position` int(11) NULL DEFAULT NULL,
  `active` int(11) NULL DEFAULT NULL,
  `create_time` datetime NULL DEFAULT NULL,
  `modified_time` datetime NULL DEFAULT NULL,
  `start_time` date NULL DEFAULT NULL,
  `end_time` date NULL DEFAULT NULL,
  `key1` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `key2` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `key3` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `owner` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`item_id`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of cms_module_listit2slider_item
-- ----------------------------
INSERT INTO `cms_module_listit2slider_item` VALUES (1, NULL, 'Hình ảnh 1', 'hinh-anh-1', 1, 0, '2015-04-27 13:20:32', '2015-04-27 13:20:32', NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO `cms_module_listit2slider_item` VALUES (2, NULL, 'Hình ảnh 2', 'hinh-anh-2', 2, 1, '2015-04-27 13:20:41', '2015-04-27 13:20:41', NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO `cms_module_listit2slider_item` VALUES (3, NULL, 'Hình ảnh 3', 'hinh-anh-3', 3, 0, '2015-04-27 13:20:50', '2015-04-27 13:20:50', NULL, NULL, NULL, NULL, NULL, 1);

-- ----------------------------
-- Table structure for cms_module_listit2slider_item_categories
-- ----------------------------
DROP TABLE IF EXISTS `cms_module_listit2slider_item_categories`;
CREATE TABLE `cms_module_listit2slider_item_categories`  (
  `item_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  PRIMARY KEY (`item_id`, `category_id`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = FIXED;

-- ----------------------------
-- Records of cms_module_listit2slider_item_categories
-- ----------------------------

-- ----------------------------
-- Table structure for cms_module_listit2social_category
-- ----------------------------
DROP TABLE IF EXISTS `cms_module_listit2social_category`;
CREATE TABLE `cms_module_listit2social_category`  (
  `category_id` int(11) NOT NULL,
  `category_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `category_alias` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `category_description` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `parent_id` int(11) NULL DEFAULT NULL,
  `position` int(11) NULL DEFAULT NULL,
  `hierarchy` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `hierarchy_path` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `id_hierarchy` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `active` int(11) NULL DEFAULT NULL,
  `create_date` datetime NULL DEFAULT NULL,
  `modified_date` datetime NULL DEFAULT NULL,
  `key1` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `key2` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `key3` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`category_id`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of cms_module_listit2social_category
-- ----------------------------

-- ----------------------------
-- Table structure for cms_module_listit2social_fielddef
-- ----------------------------
DROP TABLE IF EXISTS `cms_module_listit2social_fielddef`;
CREATE TABLE `cms_module_listit2social_fielddef`  (
  `fielddef_id` int(11) NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `alias` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `help` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `type` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `position` int(11) NULL DEFAULT NULL,
  `required` int(11) NULL DEFAULT NULL,
  `extra` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  PRIMARY KEY (`fielddef_id`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of cms_module_listit2social_fielddef
-- ----------------------------
INSERT INTO `cms_module_listit2social_fielddef` VALUES (1, 'Social Link', 'link', '', 'TextInput', 1, 0, NULL);
INSERT INTO `cms_module_listit2social_fielddef` VALUES (2, 'Social Class', 'sclass', '', 'TextInput', 2, 0, NULL);

-- ----------------------------
-- Table structure for cms_module_listit2social_fielddef_opts
-- ----------------------------
DROP TABLE IF EXISTS `cms_module_listit2social_fielddef_opts`;
CREATE TABLE `cms_module_listit2social_fielddef_opts`  (
  `fielddef_id` int(11) NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `value` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  PRIMARY KEY (`fielddef_id`, `name`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of cms_module_listit2social_fielddef_opts
-- ----------------------------
INSERT INTO `cms_module_listit2social_fielddef_opts` VALUES (1, 'size', '50');
INSERT INTO `cms_module_listit2social_fielddef_opts` VALUES (1, 'max_length', '255');
INSERT INTO `cms_module_listit2social_fielddef_opts` VALUES (2, 'size', '50');
INSERT INTO `cms_module_listit2social_fielddef_opts` VALUES (2, 'max_length', '255');

-- ----------------------------
-- Table structure for cms_module_listit2social_fieldval
-- ----------------------------
DROP TABLE IF EXISTS `cms_module_listit2social_fieldval`;
CREATE TABLE `cms_module_listit2social_fieldval`  (
  `item_id` int(11) NOT NULL,
  `fielddef_id` int(11) NOT NULL,
  `value_index` int(11) NOT NULL,
  `value` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `data` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  PRIMARY KEY (`item_id`, `fielddef_id`, `value_index`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of cms_module_listit2social_fieldval
-- ----------------------------
INSERT INTO `cms_module_listit2social_fieldval` VALUES (1, 1, 0, 'https://www.facebook.com/khoanganxuanay', NULL);
INSERT INTO `cms_module_listit2social_fieldval` VALUES (1, 2, 0, 'facebook', NULL);
INSERT INTO `cms_module_listit2social_fieldval` VALUES (2, 1, 0, 'https://www.twitter.com/songviytuong', NULL);
INSERT INTO `cms_module_listit2social_fieldval` VALUES (2, 2, 0, 'twitter', NULL);
INSERT INTO `cms_module_listit2social_fieldval` VALUES (3, 1, 0, 'https://www.pinterest.com/songviytuong', NULL);
INSERT INTO `cms_module_listit2social_fieldval` VALUES (3, 2, 0, 'pinterest', NULL);
INSERT INTO `cms_module_listit2social_fieldval` VALUES (4, 2, 0, 'youtube', NULL);
INSERT INTO `cms_module_listit2social_fieldval` VALUES (4, 1, 0, 'https://www.youtube.com/watch?v=iTBJXS_jPJ0', NULL);

-- ----------------------------
-- Table structure for cms_module_listit2social_item
-- ----------------------------
DROP TABLE IF EXISTS `cms_module_listit2social_item`;
CREATE TABLE `cms_module_listit2social_item`  (
  `item_id` int(11) NOT NULL,
  `category_id` int(11) NULL DEFAULT NULL,
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `alias` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `position` int(11) NULL DEFAULT NULL,
  `active` int(11) NULL DEFAULT NULL,
  `create_time` datetime NULL DEFAULT NULL,
  `modified_time` datetime NULL DEFAULT NULL,
  `start_time` date NULL DEFAULT NULL,
  `end_time` date NULL DEFAULT NULL,
  `key1` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `key2` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `key3` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `owner` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`item_id`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of cms_module_listit2social_item
-- ----------------------------
INSERT INTO `cms_module_listit2social_item` VALUES (1, NULL, 'Follow us on Facebook', 'follow-us-on-facebook', 1, 1, '2015-04-25 22:16:02', '2015-09-18 09:08:07', NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO `cms_module_listit2social_item` VALUES (2, NULL, 'Find us in Twitter', 'find-us-in-twitter', 2, 0, '2015-04-25 22:16:16', '2015-05-01 21:17:26', NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO `cms_module_listit2social_item` VALUES (3, NULL, 'Pin us on Pinterest', 'pin-us-on-pinterest', 3, 0, '2015-04-25 22:16:28', '2015-05-01 21:17:48', NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO `cms_module_listit2social_item` VALUES (4, NULL, 'Khoa Ngan on Youtube', 'khoa-ngan-on-youtube', 4, 1, '2015-04-25 22:16:39', '2015-05-08 16:38:23', NULL, NULL, NULL, NULL, NULL, 1);

-- ----------------------------
-- Table structure for cms_module_listit2social_item_categories
-- ----------------------------
DROP TABLE IF EXISTS `cms_module_listit2social_item_categories`;
CREATE TABLE `cms_module_listit2social_item_categories`  (
  `item_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  PRIMARY KEY (`item_id`, `category_id`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = FIXED;

-- ----------------------------
-- Records of cms_module_listit2social_item_categories
-- ----------------------------

-- ----------------------------
-- Table structure for cms_module_listit2staff_category
-- ----------------------------
DROP TABLE IF EXISTS `cms_module_listit2staff_category`;
CREATE TABLE `cms_module_listit2staff_category`  (
  `category_id` int(11) NOT NULL,
  `category_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `category_alias` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `category_description` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `parent_id` int(11) NULL DEFAULT NULL,
  `position` int(11) NULL DEFAULT NULL,
  `hierarchy` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `hierarchy_path` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `id_hierarchy` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `active` int(11) NULL DEFAULT NULL,
  `create_date` datetime NULL DEFAULT NULL,
  `modified_date` datetime NULL DEFAULT NULL,
  `key1` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `key2` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `key3` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`category_id`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of cms_module_listit2staff_category
-- ----------------------------

-- ----------------------------
-- Table structure for cms_module_listit2staff_fielddef
-- ----------------------------
DROP TABLE IF EXISTS `cms_module_listit2staff_fielddef`;
CREATE TABLE `cms_module_listit2staff_fielddef`  (
  `fielddef_id` int(11) NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `alias` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `help` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `type` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `position` int(11) NULL DEFAULT NULL,
  `required` int(11) NULL DEFAULT NULL,
  `extra` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  PRIMARY KEY (`fielddef_id`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of cms_module_listit2staff_fielddef
-- ----------------------------
INSERT INTO `cms_module_listit2staff_fielddef` VALUES (1, 'Vị trí', 'staff', '', 'TextInput', 1, 0, NULL);
INSERT INTO `cms_module_listit2staff_fielddef` VALUES (2, 'Hình ảnh', 'image', '', 'FileUpload', 2, 0, NULL);
INSERT INTO `cms_module_listit2staff_fielddef` VALUES (3, 'Mô tả', 'descript', '', 'TextInput', 3, 0, NULL);
INSERT INTO `cms_module_listit2staff_fielddef` VALUES (4, 'Facebook', 'facebook', '', 'TextInput', 4, 0, NULL);
INSERT INTO `cms_module_listit2staff_fielddef` VALUES (5, 'Twitter', 'twitter', '', 'TextInput', 5, 0, NULL);
INSERT INTO `cms_module_listit2staff_fielddef` VALUES (6, 'Pinterest', 'pinterest', '', 'TextInput', 6, 0, '');
INSERT INTO `cms_module_listit2staff_fielddef` VALUES (7, 'Youtube', 'youtube', '', 'TextInput', 7, 0, NULL);

-- ----------------------------
-- Table structure for cms_module_listit2staff_fielddef_opts
-- ----------------------------
DROP TABLE IF EXISTS `cms_module_listit2staff_fielddef_opts`;
CREATE TABLE `cms_module_listit2staff_fielddef_opts`  (
  `fielddef_id` int(11) NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `value` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  PRIMARY KEY (`fielddef_id`, `name`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of cms_module_listit2staff_fielddef_opts
-- ----------------------------
INSERT INTO `cms_module_listit2staff_fielddef_opts` VALUES (1, 'size', '10');
INSERT INTO `cms_module_listit2staff_fielddef_opts` VALUES (1, 'max_length', '255');
INSERT INTO `cms_module_listit2staff_fielddef_opts` VALUES (2, 'allowed', 'pdf,gif,jpeg,jpg,png');
INSERT INTO `cms_module_listit2staff_fielddef_opts` VALUES (2, 'dir', 'staff');
INSERT INTO `cms_module_listit2staff_fielddef_opts` VALUES (2, 'image', '1');
INSERT INTO `cms_module_listit2staff_fielddef_opts` VALUES (2, 'size', '20');
INSERT INTO `cms_module_listit2staff_fielddef_opts` VALUES (3, 'size', '80');
INSERT INTO `cms_module_listit2staff_fielddef_opts` VALUES (3, 'max_length', '255');
INSERT INTO `cms_module_listit2staff_fielddef_opts` VALUES (4, 'size', '20');
INSERT INTO `cms_module_listit2staff_fielddef_opts` VALUES (4, 'max_length', '255');
INSERT INTO `cms_module_listit2staff_fielddef_opts` VALUES (5, 'size', '20');
INSERT INTO `cms_module_listit2staff_fielddef_opts` VALUES (5, 'max_length', '255');
INSERT INTO `cms_module_listit2staff_fielddef_opts` VALUES (6, 'max_length', '255');
INSERT INTO `cms_module_listit2staff_fielddef_opts` VALUES (7, 'size', '20');
INSERT INTO `cms_module_listit2staff_fielddef_opts` VALUES (7, 'max_length', '255');
INSERT INTO `cms_module_listit2staff_fielddef_opts` VALUES (6, 'size', '20');

-- ----------------------------
-- Table structure for cms_module_listit2staff_fieldval
-- ----------------------------
DROP TABLE IF EXISTS `cms_module_listit2staff_fieldval`;
CREATE TABLE `cms_module_listit2staff_fieldval`  (
  `item_id` int(11) NOT NULL,
  `fielddef_id` int(11) NOT NULL,
  `value_index` int(11) NOT NULL,
  `value` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `data` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  PRIMARY KEY (`item_id`, `fielddef_id`, `value_index`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of cms_module_listit2staff_fieldval
-- ----------------------------
INSERT INTO `cms_module_listit2staff_fieldval` VALUES (1, 1, 0, 'Head Chef', NULL);
INSERT INTO `cms_module_listit2staff_fieldval` VALUES (1, 2, 0, 'staff-01.jpg', NULL);
INSERT INTO `cms_module_listit2staff_fieldval` VALUES (1, 3, 0, 'Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh. Donec sodales sagittis magna. Aenean commodo ligula.', NULL);
INSERT INTO `cms_module_listit2staff_fieldval` VALUES (1, 4, 0, 'songviytuong', NULL);
INSERT INTO `cms_module_listit2staff_fieldval` VALUES (1, 5, 0, 'songviytuong', NULL);
INSERT INTO `cms_module_listit2staff_fieldval` VALUES (1, 6, 0, 'songviytuong', NULL);
INSERT INTO `cms_module_listit2staff_fieldval` VALUES (1, 7, 0, 'songviytuong', NULL);
INSERT INTO `cms_module_listit2staff_fieldval` VALUES (2, 1, 0, 'Bếp phó', NULL);
INSERT INTO `cms_module_listit2staff_fieldval` VALUES (2, 2, 0, 'staff-02.jpg', NULL);
INSERT INTO `cms_module_listit2staff_fieldval` VALUES (2, 3, 0, 'Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt. Duis leo', NULL);
INSERT INTO `cms_module_listit2staff_fieldval` VALUES (2, 4, 0, 'songviytuong', NULL);
INSERT INTO `cms_module_listit2staff_fieldval` VALUES (3, 1, 0, 'Bếp trưởng', NULL);
INSERT INTO `cms_module_listit2staff_fieldval` VALUES (3, 2, 0, 'staff-03.jpg', NULL);
INSERT INTO `cms_module_listit2staff_fieldval` VALUES (3, 3, 0, 'Xây dựng danh tiếng từ tài năng nấu nướng hoàn hảo, nhưng Ramsay còn có cả tiếng xấu bởi tính khí nóng nảy và hay chửi thề.', NULL);
INSERT INTO `cms_module_listit2staff_fieldval` VALUES (3, 4, 0, 'gordon-ramsay', NULL);

-- ----------------------------
-- Table structure for cms_module_listit2staff_item
-- ----------------------------
DROP TABLE IF EXISTS `cms_module_listit2staff_item`;
CREATE TABLE `cms_module_listit2staff_item`  (
  `item_id` int(11) NOT NULL,
  `category_id` int(11) NULL DEFAULT NULL,
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `alias` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `position` int(11) NULL DEFAULT NULL,
  `active` int(11) NULL DEFAULT NULL,
  `create_time` datetime NULL DEFAULT NULL,
  `modified_time` datetime NULL DEFAULT NULL,
  `start_time` date NULL DEFAULT NULL,
  `end_time` date NULL DEFAULT NULL,
  `key1` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `key2` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `key3` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `owner` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`item_id`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of cms_module_listit2staff_item
-- ----------------------------
INSERT INTO `cms_module_listit2staff_item` VALUES (1, NULL, 'John Doggett', 'john-doggett', 1, 1, '2015-04-27 12:57:16', '2015-04-27 12:57:16', NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO `cms_module_listit2staff_item` VALUES (2, NULL, 'Lee Peace', 'lee-peace', 2, 1, '2015-05-01 20:49:39', '2015-05-01 20:49:39', NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO `cms_module_listit2staff_item` VALUES (3, NULL, 'Gordon Ramsay', 'gordon-ramsay', 3, 1, '2015-05-01 20:51:56', '2015-05-01 20:51:56', NULL, NULL, NULL, NULL, NULL, 1);

-- ----------------------------
-- Table structure for cms_module_listit2staff_item_categories
-- ----------------------------
DROP TABLE IF EXISTS `cms_module_listit2staff_item_categories`;
CREATE TABLE `cms_module_listit2staff_item_categories`  (
  `item_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  PRIMARY KEY (`item_id`, `category_id`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = FIXED;

-- ----------------------------
-- Records of cms_module_listit2staff_item_categories
-- ----------------------------

-- ----------------------------
-- Table structure for cms_module_listit2testimonials_category
-- ----------------------------
DROP TABLE IF EXISTS `cms_module_listit2testimonials_category`;
CREATE TABLE `cms_module_listit2testimonials_category`  (
  `category_id` int(11) NOT NULL,
  `category_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `category_alias` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `category_description` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `parent_id` int(11) NULL DEFAULT NULL,
  `position` int(11) NULL DEFAULT NULL,
  `hierarchy` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `hierarchy_path` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `id_hierarchy` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `active` int(11) NULL DEFAULT NULL,
  `create_date` datetime NULL DEFAULT NULL,
  `modified_date` datetime NULL DEFAULT NULL,
  `key1` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `key2` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `key3` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`category_id`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of cms_module_listit2testimonials_category
-- ----------------------------

-- ----------------------------
-- Table structure for cms_module_listit2testimonials_fielddef
-- ----------------------------
DROP TABLE IF EXISTS `cms_module_listit2testimonials_fielddef`;
CREATE TABLE `cms_module_listit2testimonials_fielddef`  (
  `fielddef_id` int(11) NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `alias` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `help` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `type` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `position` int(11) NULL DEFAULT NULL,
  `required` int(11) NULL DEFAULT NULL,
  `extra` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  PRIMARY KEY (`fielddef_id`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of cms_module_listit2testimonials_fielddef
-- ----------------------------
INSERT INTO `cms_module_listit2testimonials_fielddef` VALUES (1, 'Địa chỉ', 'address', '', 'TextInput', 1, 0, NULL);
INSERT INTO `cms_module_listit2testimonials_fielddef` VALUES (2, 'Hình ảnh', 'image', '', 'FileUpload', 2, 0, NULL);
INSERT INTO `cms_module_listit2testimonials_fielddef` VALUES (3, 'Họ nói gì?', 'say', '', 'TextInput', 3, 0, NULL);
INSERT INTO `cms_module_listit2testimonials_fielddef` VALUES (4, 'Facebook', 'facebook', '', 'TextInput', 4, 0, NULL);

-- ----------------------------
-- Table structure for cms_module_listit2testimonials_fielddef_opts
-- ----------------------------
DROP TABLE IF EXISTS `cms_module_listit2testimonials_fielddef_opts`;
CREATE TABLE `cms_module_listit2testimonials_fielddef_opts`  (
  `fielddef_id` int(11) NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `value` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  PRIMARY KEY (`fielddef_id`, `name`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of cms_module_listit2testimonials_fielddef_opts
-- ----------------------------
INSERT INTO `cms_module_listit2testimonials_fielddef_opts` VALUES (1, 'size', '50');
INSERT INTO `cms_module_listit2testimonials_fielddef_opts` VALUES (1, 'max_length', '255');
INSERT INTO `cms_module_listit2testimonials_fielddef_opts` VALUES (2, 'allowed', 'pdf,gif,jpeg,jpg,png');
INSERT INTO `cms_module_listit2testimonials_fielddef_opts` VALUES (2, 'dir', 'testimonials');
INSERT INTO `cms_module_listit2testimonials_fielddef_opts` VALUES (2, 'image', '1');
INSERT INTO `cms_module_listit2testimonials_fielddef_opts` VALUES (2, 'size', '20');
INSERT INTO `cms_module_listit2testimonials_fielddef_opts` VALUES (3, 'size', '50');
INSERT INTO `cms_module_listit2testimonials_fielddef_opts` VALUES (3, 'max_length', '255');
INSERT INTO `cms_module_listit2testimonials_fielddef_opts` VALUES (4, 'size', '20');
INSERT INTO `cms_module_listit2testimonials_fielddef_opts` VALUES (4, 'max_length', '255');

-- ----------------------------
-- Table structure for cms_module_listit2testimonials_fieldval
-- ----------------------------
DROP TABLE IF EXISTS `cms_module_listit2testimonials_fieldval`;
CREATE TABLE `cms_module_listit2testimonials_fieldval`  (
  `item_id` int(11) NOT NULL,
  `fielddef_id` int(11) NOT NULL,
  `value_index` int(11) NOT NULL,
  `value` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `data` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  PRIMARY KEY (`item_id`, `fielddef_id`, `value_index`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of cms_module_listit2testimonials_fieldval
-- ----------------------------
INSERT INTO `cms_module_listit2testimonials_fieldval` VALUES (1, 1, 0, 'Sài Gòn', NULL);
INSERT INTO `cms_module_listit2testimonials_fieldval` VALUES (1, 3, 0, 'Món ăn Khoa Ngan rất là ngon...', NULL);
INSERT INTO `cms_module_listit2testimonials_fieldval` VALUES (2, 1, 0, 'Sài Gòn', NULL);
INSERT INTO `cms_module_listit2testimonials_fieldval` VALUES (2, 3, 0, 'Tôi thích ăn Bún Măng ở Nhà Hàng Khoa Ngan', NULL);
INSERT INTO `cms_module_listit2testimonials_fieldval` VALUES (1, 2, 0, 'dvh.jpg', NULL);
INSERT INTO `cms_module_listit2testimonials_fieldval` VALUES (2, 2, 0, 'quangdung.jpg', NULL);
INSERT INTO `cms_module_listit2testimonials_fieldval` VALUES (4, 1, 0, 'Hà Nội', NULL);
INSERT INTO `cms_module_listit2testimonials_fieldval` VALUES (4, 3, 0, 'Tôi hy vọng, một ngày sớm về Hà Nội để thưởng thức món Ngan của Nhà Hàng Khoa Ngan. Tôi rất ấn tượng bởi những món ăn độc đáo và mới lạ, hương vị thật hấp dẫn không thể nào quên...', NULL);
INSERT INTO `cms_module_listit2testimonials_fieldval` VALUES (4, 4, 0, '100003269729366', NULL);
INSERT INTO `cms_module_listit2testimonials_fieldval` VALUES (1, 4, 0, '100004994503021', NULL);
INSERT INTO `cms_module_listit2testimonials_fieldval` VALUES (2, 4, 0, '100000923516754', NULL);

-- ----------------------------
-- Table structure for cms_module_listit2testimonials_item
-- ----------------------------
DROP TABLE IF EXISTS `cms_module_listit2testimonials_item`;
CREATE TABLE `cms_module_listit2testimonials_item`  (
  `item_id` int(11) NOT NULL,
  `category_id` int(11) NULL DEFAULT NULL,
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `alias` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `position` int(11) NULL DEFAULT NULL,
  `active` int(11) NULL DEFAULT NULL,
  `create_time` datetime NULL DEFAULT NULL,
  `modified_time` datetime NULL DEFAULT NULL,
  `start_time` date NULL DEFAULT NULL,
  `end_time` date NULL DEFAULT NULL,
  `key1` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `key2` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `key3` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `owner` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`item_id`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of cms_module_listit2testimonials_item
-- ----------------------------
INSERT INTO `cms_module_listit2testimonials_item` VALUES (1, NULL, 'Đàm Vĩnh Hưng', 'dam-vinh-hung', 1, 1, '2015-04-25 00:38:22', '2015-09-17 23:28:08', NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO `cms_module_listit2testimonials_item` VALUES (2, NULL, 'Quang Dũng', 'quang-dung', 2, 1, '2015-04-25 00:41:56', '2015-09-17 23:28:37', NULL, NULL, NULL, NULL, NULL, 1);
INSERT INTO `cms_module_listit2testimonials_item` VALUES (4, NULL, 'Lee Peace', 'lee-peace', 3, 1, '2015-09-17 23:23:31', '2015-09-17 23:23:31', NULL, NULL, NULL, NULL, NULL, 1);

-- ----------------------------
-- Table structure for cms_module_listit2testimonials_item_categories
-- ----------------------------
DROP TABLE IF EXISTS `cms_module_listit2testimonials_item_categories`;
CREATE TABLE `cms_module_listit2testimonials_item_categories`  (
  `item_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  PRIMARY KEY (`item_id`, `category_id`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = FIXED;

-- ----------------------------
-- Records of cms_module_listit2testimonials_item_categories
-- ----------------------------

-- ----------------------------
-- Table structure for cms_module_mlecms_config
-- ----------------------------
DROP TABLE IF EXISTS `cms_module_mlecms_config`;
CREATE TABLE `cms_module_mlecms_config`  (
  `id` int(11) NOT NULL,
  `name` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `alias` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `locale` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `setlocale` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `direction` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `flag` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `extra` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `sort` int(11) NULL DEFAULT NULL,
  `modified_date` datetime NULL DEFAULT NULL,
  `created_date` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of cms_module_mlecms_config
-- ----------------------------
INSERT INTO `cms_module_mlecms_config` VALUES (1, 'English', 'en', 'en_US', NULL, '', 'images/MleCMS/usa.png', 'en_US', 2, '2015-04-11 21:53:59', '2015-04-11 21:53:59');
INSERT INTO `cms_module_mlecms_config` VALUES (3, 'Vietnamese', 'vi', 'vi_VN', NULL, '', 'images/MleCMS/vi.png', 'vi_VN', 1, '2015-04-24 22:51:44', '2015-04-24 22:42:37');

-- ----------------------------
-- Table structure for cms_module_news
-- ----------------------------
DROP TABLE IF EXISTS `cms_module_news`;
CREATE TABLE `cms_module_news`  (
  `news_id` int(11) NOT NULL,
  `news_category_id` int(11) NULL DEFAULT NULL,
  `news_title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `news_data` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `news_date` datetime NULL DEFAULT NULL,
  `summary` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `start_time` datetime NULL DEFAULT NULL,
  `end_time` datetime NULL DEFAULT NULL,
  `status` varchar(25) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `icon` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `create_date` datetime NULL DEFAULT NULL,
  `modified_date` datetime NULL DEFAULT NULL,
  `author_id` int(11) NULL DEFAULT NULL,
  `news_extra` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `news_url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`news_id`) USING BTREE,
  INDEX `cms_news_postdate`(`news_date`) USING BTREE,
  INDEX `cms_news_daterange`(`start_time`, `end_time`) USING BTREE,
  INDEX `cms_news_author`(`author_id`) USING BTREE,
  INDEX `cms_news_hier`(`news_category_id`) USING BTREE,
  INDEX `cms_news_url`(`news_url`) USING BTREE,
  INDEX `cms_news_startenddate`(`start_time`, `end_time`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of cms_module_news
-- ----------------------------
INSERT INTO `cms_module_news` VALUES (1, 1, 'News Module Installed', 'The news module was installed.  Exciting. This news article is not using the Summary field and therefore there is no link to read more. But you can click on the news heading to read only this article.', '2015-04-10 00:18:55', NULL, NULL, NULL, 'published', NULL, '2015-04-10 00:18:55', '2015-04-10 00:18:55', 1, NULL, NULL);

-- ----------------------------
-- Table structure for cms_module_news_categories
-- ----------------------------
DROP TABLE IF EXISTS `cms_module_news_categories`;
CREATE TABLE `cms_module_news_categories`  (
  `news_category_id` int(11) NOT NULL,
  `news_category_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `parent_id` int(11) NULL DEFAULT NULL,
  `hierarchy` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `long_name` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `create_date` datetime NULL DEFAULT NULL,
  `modified_date` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`news_category_id`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of cms_module_news_categories
-- ----------------------------
INSERT INTO `cms_module_news_categories` VALUES (1, 'General', -1, '00001', 'General', '2015-04-10 00:18:55', '2015-04-10 00:18:55');

-- ----------------------------
-- Table structure for cms_module_news_categories_seq
-- ----------------------------
DROP TABLE IF EXISTS `cms_module_news_categories_seq`;
CREATE TABLE `cms_module_news_categories_seq`  (
  `id` int(11) NOT NULL
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = FIXED;

-- ----------------------------
-- Records of cms_module_news_categories_seq
-- ----------------------------
INSERT INTO `cms_module_news_categories_seq` VALUES (1);

-- ----------------------------
-- Table structure for cms_module_news_fielddefs
-- ----------------------------
DROP TABLE IF EXISTS `cms_module_news_fielddefs`;
CREATE TABLE `cms_module_news_fielddefs`  (
  `id` int(11) NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `type` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `max_length` int(11) NULL DEFAULT NULL,
  `create_date` datetime NULL DEFAULT NULL,
  `modified_date` datetime NULL DEFAULT NULL,
  `item_order` int(11) NULL DEFAULT NULL,
  `public` int(11) NULL DEFAULT NULL,
  `extra` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of cms_module_news_fielddefs
-- ----------------------------

-- ----------------------------
-- Table structure for cms_module_news_fieldvals
-- ----------------------------
DROP TABLE IF EXISTS `cms_module_news_fieldvals`;
CREATE TABLE `cms_module_news_fieldvals`  (
  `news_id` int(11) NOT NULL,
  `fielddef_id` int(11) NOT NULL,
  `value` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `create_date` datetime NULL DEFAULT NULL,
  `modified_date` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`news_id`, `fielddef_id`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of cms_module_news_fieldvals
-- ----------------------------

-- ----------------------------
-- Table structure for cms_module_news_seq
-- ----------------------------
DROP TABLE IF EXISTS `cms_module_news_seq`;
CREATE TABLE `cms_module_news_seq`  (
  `id` int(11) NOT NULL
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = FIXED;

-- ----------------------------
-- Records of cms_module_news_seq
-- ----------------------------
INSERT INTO `cms_module_news_seq` VALUES (1);

-- ----------------------------
-- Table structure for cms_module_search_index
-- ----------------------------
DROP TABLE IF EXISTS `cms_module_search_index`;
CREATE TABLE `cms_module_search_index`  (
  `item_id` int(11) NULL DEFAULT NULL,
  `word` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `count` int(11) NULL DEFAULT NULL,
  INDEX `cms_index_search_count`(`count`) USING BTREE,
  INDEX `cms_index_search_index`(`word`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of cms_module_search_index
-- ----------------------------
INSERT INTO `cms_module_search_index` VALUES (1, 'home', 4);
INSERT INTO `cms_module_search_index` VALUES (1, 'uploads', 1);
INSERT INTO `cms_module_search_index` VALUES (1, 'images', 1);
INSERT INTO `cms_module_search_index` VALUES (1, 'logo1', 1);
INSERT INTO `cms_module_search_index` VALUES (1, 'gif', 1);
INSERT INTO `cms_module_search_index` VALUES (1, 'congratulations', 1);
INSERT INTO `cms_module_search_index` VALUES (1, 'installation', 4);
INSERT INTO `cms_module_search_index` VALUES (1, 'worked', 1);
INSERT INTO `cms_module_search_index` VALUES (1, 'now', 1);
INSERT INTO `cms_module_search_index` VALUES (1, 'fully', 1);
INSERT INTO `cms_module_search_index` VALUES (1, 'functional', 1);
INSERT INTO `cms_module_search_index` VALUES (1, 'cms', 6);
INSERT INTO `cms_module_search_index` VALUES (1, 'made', 6);
INSERT INTO `cms_module_search_index` VALUES (1, 'simple', 6);
INSERT INTO `cms_module_search_index` VALUES (1, 'almost', 1);
INSERT INTO `cms_module_search_index` VALUES (1, 'ready', 1);
INSERT INTO `cms_module_search_index` VALUES (1, 'start', 1);
INSERT INTO `cms_module_search_index` VALUES (1, 'building', 1);
INSERT INTO `cms_module_search_index` VALUES (1, 'site', 4);
INSERT INTO `cms_module_search_index` VALUES (1, 'chose', 1);
INSERT INTO `cms_module_search_index` VALUES (1, 'install', 1);
INSERT INTO `cms_module_search_index` VALUES (1, 'default', 3);
INSERT INTO `cms_module_search_index` VALUES (1, 'content', 1);
INSERT INTO `cms_module_search_index` VALUES (1, 'will', 1);
INSERT INTO `cms_module_search_index` VALUES (1, 'see', 1);
INSERT INTO `cms_module_search_index` VALUES (1, 'numerous', 1);
INSERT INTO `cms_module_search_index` VALUES (1, 'pages', 3);
INSERT INTO `cms_module_search_index` VALUES (1, 'available', 1);
INSERT INTO `cms_module_search_index` VALUES (1, 'read', 3);
INSERT INTO `cms_module_search_index` VALUES (1, 'should', 1);
INSERT INTO `cms_module_search_index` VALUES (1, 'thoroughly', 1);
INSERT INTO `cms_module_search_index` VALUES (1, 'devoted', 1);
INSERT INTO `cms_module_search_index` VALUES (1, 'showing', 1);
INSERT INTO `cms_module_search_index` VALUES (1, 'basics', 1);
INSERT INTO `cms_module_search_index` VALUES (1, 'begin', 1);
INSERT INTO `cms_module_search_index` VALUES (1, 'working', 1);
INSERT INTO `cms_module_search_index` VALUES (1, 'example', 1);
INSERT INTO `cms_module_search_index` VALUES (1, 'templates', 2);
INSERT INTO `cms_module_search_index` VALUES (1, 'stylesheets', 1);
INSERT INTO `cms_module_search_index` VALUES (1, 'many', 1);
INSERT INTO `cms_module_search_index` VALUES (1, 'features', 1);
INSERT INTO `cms_module_search_index` VALUES (1, 'described', 1);
INSERT INTO `cms_module_search_index` VALUES (1, 'demonstrated', 1);
INSERT INTO `cms_module_search_index` VALUES (1, 'can', 1);
INSERT INTO `cms_module_search_index` VALUES (1, 'learn', 1);
INSERT INTO `cms_module_search_index` VALUES (1, 'much', 2);
INSERT INTO `cms_module_search_index` VALUES (1, 'power', 1);
INSERT INTO `cms_module_search_index` VALUES (1, 'absorbing', 1);
INSERT INTO `cms_module_search_index` VALUES (1, 'information', 1);
INSERT INTO `cms_module_search_index` VALUES (1, 'get', 1);
INSERT INTO `cms_module_search_index` VALUES (1, 'administration', 1);
INSERT INTO `cms_module_search_index` VALUES (1, 'console', 1);
INSERT INTO `cms_module_search_index` VALUES (1, 'login', 2);
INSERT INTO `cms_module_search_index` VALUES (1, 'administrator', 1);
INSERT INTO `cms_module_search_index` VALUES (1, 'username', 1);
INSERT INTO `cms_module_search_index` VALUES (1, 'password', 1);
INSERT INTO `cms_module_search_index` VALUES (1, 'mentioned', 1);
INSERT INTO `cms_module_search_index` VALUES (1, 'process', 1);
INSERT INTO `cms_module_search_index` VALUES (1, 'http', 1);
INSERT INTO `cms_module_search_index` VALUES (1, 'yourwebsite', 1);
INSERT INTO `cms_module_search_index` VALUES (1, 'com', 1);
INSERT INTO `cms_module_search_index` VALUES (1, 'cmsmspath', 1);
INSERT INTO `cms_module_search_index` VALUES (1, 'admin', 1);
INSERT INTO `cms_module_search_index` VALUES (1, 'click', 1);
INSERT INTO `cms_module_search_index` VALUES (1, 'use', 1);
INSERT INTO `cms_module_search_index` VALUES (1, 'license', 3);
INSERT INTO `cms_module_search_index` VALUES (1, 'released', 1);
INSERT INTO `cms_module_search_index` VALUES (1, 'don\'t', 1);
INSERT INTO `cms_module_search_index` VALUES (1, 'leave', 1);
INSERT INTO `cms_module_search_index` VALUES (1, 'link', 1);
INSERT INTO `cms_module_search_index` VALUES (1, 'back', 1);
INSERT INTO `cms_module_search_index` VALUES (1, 'would', 1);
INSERT INTO `cms_module_search_index` VALUES (1, 'like', 1);
INSERT INTO `cms_module_search_index` VALUES (1, 'third', 1);
INSERT INTO `cms_module_search_index` VALUES (1, 'party', 1);
INSERT INTO `cms_module_search_index` VALUES (1, 'add', 1);
INSERT INTO `cms_module_search_index` VALUES (1, 'modules', 1);
INSERT INTO `cms_module_search_index` VALUES (1, 'may', 1);
INSERT INTO `cms_module_search_index` VALUES (1, 'include', 1);
INSERT INTO `cms_module_search_index` VALUES (1, 'additional', 1);
INSERT INTO `cms_module_search_index` VALUES (1, 'restrictions', 1);
INSERT INTO `cms_module_search_index` VALUES (34, 'news', 5);
INSERT INTO `cms_module_search_index` VALUES (34, 'module', 3);
INSERT INTO `cms_module_search_index` VALUES (34, 'installed', 3);
INSERT INTO `cms_module_search_index` VALUES (34, 'exciting', 1);
INSERT INTO `cms_module_search_index` VALUES (34, 'article', 2);
INSERT INTO `cms_module_search_index` VALUES (34, 'using', 1);
INSERT INTO `cms_module_search_index` VALUES (34, 'summary', 1);
INSERT INTO `cms_module_search_index` VALUES (34, 'field', 1);
INSERT INTO `cms_module_search_index` VALUES (34, 'therefore', 1);
INSERT INTO `cms_module_search_index` VALUES (34, 'link', 1);
INSERT INTO `cms_module_search_index` VALUES (34, 'read', 2);
INSERT INTO `cms_module_search_index` VALUES (34, 'can', 1);
INSERT INTO `cms_module_search_index` VALUES (34, 'click', 1);
INSERT INTO `cms_module_search_index` VALUES (34, 'heading', 1);
INSERT INTO `cms_module_search_index` VALUES (58, 'thiệu', 4);
INSERT INTO `cms_module_search_index` VALUES (58, 'giới', 4);
INSERT INTO `cms_module_search_index` VALUES (410, 'chủ', 4);
INSERT INTO `cms_module_search_index` VALUES (410, 'trang', 4);
INSERT INTO `cms_module_search_index` VALUES (45, 'menu', 4);
INSERT INTO `cms_module_search_index` VALUES (259, 'người', 4);
INSERT INTO `cms_module_search_index` VALUES (47, 'hình', 4);
INSERT INTO `cms_module_search_index` VALUES (47, 'ảnh', 4);
INSERT INTO `cms_module_search_index` VALUES (260, 'mãi', 4);
INSERT INTO `cms_module_search_index` VALUES (260, 'khuyến', 4);
INSERT INTO `cms_module_search_index` VALUES (212, 'thơm', 1);
INSERT INTO `cms_module_search_index` VALUES (310, 'thưởng', 1);
INSERT INTO `cms_module_search_index` VALUES (308, 'hàng', 1);
INSERT INTO `cms_module_search_index` VALUES (308, 'nhà', 1);
INSERT INTO `cms_module_search_index` VALUES (310, 'nơi', 1);
INSERT INTO `cms_module_search_index` VALUES (309, 'đầu', 1);
INSERT INTO `cms_module_search_index` VALUES (309, 'siêu', 1);
INSERT INTO `cms_module_search_index` VALUES (311, '“ngan', 1);
INSERT INTO `cms_module_search_index` VALUES (212, 'kỳ', 1);
INSERT INTO `cms_module_search_index` VALUES (212, 'cực', 1);
INSERT INTO `cms_module_search_index` VALUES (212, 'sản', 1);
INSERT INTO `cms_module_search_index` VALUES (212, 'với', 1);
INSERT INTO `cms_module_search_index` VALUES (212, 'nội', 1);
INSERT INTO `cms_module_search_index` VALUES (212, 'đời', 1);
INSERT INTO `cms_module_search_index` VALUES (212, 'lâu', 1);
INSERT INTO `cms_module_search_index` VALUES (212, 'chỉ', 1);
INSERT INTO `cms_module_search_index` VALUES (212, 'địa', 1);
INSERT INTO `cms_module_search_index` VALUES (212, '–', 1);
INSERT INTO `cms_module_search_index` VALUES (212, 'tại', 2);
INSERT INTO `cms_module_search_index` VALUES (212, 'nay', 1);
INSERT INTO `cms_module_search_index` VALUES (212, 'xưa', 1);
INSERT INTO `cms_module_search_index` VALUES (212, 'qua', 1);
INSERT INTO `cms_module_search_index` VALUES (212, 'bỏ', 1);
INSERT INTO `cms_module_search_index` VALUES (212, 'thể', 1);
INSERT INTO `cms_module_search_index` VALUES (212, 'không', 1);
INSERT INTO `cms_module_search_index` VALUES (212, 'hẳn', 1);
INSERT INTO `cms_module_search_index` VALUES (212, 'chắc', 1);
INSERT INTO `cms_module_search_index` VALUES (212, 'thì', 1);
INSERT INTO `cms_module_search_index` VALUES (212, 'vậy', 1);
INSERT INTO `cms_module_search_index` VALUES (212, 'ngan…', 1);
INSERT INTO `cms_module_search_index` VALUES (212, 'vịt', 1);
INSERT INTO `cms_module_search_index` VALUES (212, 'như', 1);
INSERT INTO `cms_module_search_index` VALUES (212, 'cầm', 1);
INSERT INTO `cms_module_search_index` VALUES (212, 'gia', 1);
INSERT INTO `cms_module_search_index` VALUES (212, 'từ', 2);
INSERT INTO `cms_module_search_index` VALUES (212, 'biến', 1);
INSERT INTO `cms_module_search_index` VALUES (212, 'chế', 1);
INSERT INTO `cms_module_search_index` VALUES (212, 'được', 1);
INSERT INTO `cms_module_search_index` VALUES (212, 'ăn', 1);
INSERT INTO `cms_module_search_index` VALUES (212, 'biệt', 1);
INSERT INTO `cms_module_search_index` VALUES (212, 'đặc', 2);
INSERT INTO `cms_module_search_index` VALUES (212, 'còn', 1);
INSERT INTO `cms_module_search_index` VALUES (212, 'lại', 1);
INSERT INTO `cms_module_search_index` VALUES (212, 'nữa', 1);
INSERT INTO `cms_module_search_index` VALUES (212, 'hơn', 1);
INSERT INTO `cms_module_search_index` VALUES (212, 'th', 1);
INSERT INTO `cms_module_search_index` VALUES (212, 'xứ', 1);
INSERT INTO `cms_module_search_index` VALUES (212, 'tiếng', 2);
INSERT INTO `cms_module_search_index` VALUES (212, 'nổi', 2);
INSERT INTO `cms_module_search_index` VALUES (212, 'ngon', 2);
INSERT INTO `cms_module_search_index` VALUES (406, 'khoa', 1);
INSERT INTO `cms_module_search_index` VALUES (405, 'ngon', 1);
INSERT INTO `cms_module_search_index` VALUES (405, 'là', 1);
INSERT INTO `cms_module_search_index` VALUES (405, 'rất', 1);
INSERT INTO `cms_module_search_index` VALUES (405, 'ngan', 1);
INSERT INTO `cms_module_search_index` VALUES (405, 'khoa', 1);
INSERT INTO `cms_module_search_index` VALUES (405, 'ăn', 1);
INSERT INTO `cms_module_search_index` VALUES (406, 'hàng', 1);
INSERT INTO `cms_module_search_index` VALUES (406, 'nhà', 1);
INSERT INTO `cms_module_search_index` VALUES (406, 'ở', 1);
INSERT INTO `cms_module_search_index` VALUES (406, 'măng', 1);
INSERT INTO `cms_module_search_index` VALUES (406, 'bún', 1);
INSERT INTO `cms_module_search_index` VALUES (406, 'ăn', 1);
INSERT INTO `cms_module_search_index` VALUES (406, 'thích', 1);
INSERT INTO `cms_module_search_index` VALUES (406, 'tôi', 1);
INSERT INTO `cms_module_search_index` VALUES (406, 'jpg', 1);
INSERT INTO `cms_module_search_index` VALUES (406, 'quangdung', 1);
INSERT INTO `cms_module_search_index` VALUES (406, 'gòn', 1);
INSERT INTO `cms_module_search_index` VALUES (406, 'sài', 1);
INSERT INTO `cms_module_search_index` VALUES (409, 'com', 1);
INSERT INTO `cms_module_search_index` VALUES (409, 'www', 1);
INSERT INTO `cms_module_search_index` VALUES (224, 'twitter', 3);
INSERT INTO `cms_module_search_index` VALUES (224, 'find', 1);
INSERT INTO `cms_module_search_index` VALUES (225, 'pinterest', 3);
INSERT INTO `cms_module_search_index` VALUES (225, 'pin', 1);
INSERT INTO `cms_module_search_index` VALUES (314, 'watch', 1);
INSERT INTO `cms_module_search_index` VALUES (314, 'com', 1);
INSERT INTO `cms_module_search_index` VALUES (78, 'mò', 1);
INSERT INTO `cms_module_search_index` VALUES (78, 'tò', 1);
INSERT INTO `cms_module_search_index` VALUES (75, 'special', 1);
INSERT INTO `cms_module_search_index` VALUES (75, 'đây', 1);
INSERT INTO `cms_module_search_index` VALUES (75, 'gì', 1);
INSERT INTO `cms_module_search_index` VALUES (75, '15000', 1);
INSERT INTO `cms_module_search_index` VALUES (75, 'qu', 1);
INSERT INTO `cms_module_search_index` VALUES (75, 'tặng', 1);
INSERT INTO `cms_module_search_index` VALUES (75, 'rồi', 1);
INSERT INTO `cms_module_search_index` VALUES (75, 'bát', 1);
INSERT INTO `cms_module_search_index` VALUES (75, 'thôi', 1);
INSERT INTO `cms_module_search_index` VALUES (75, 'bún', 1);
INSERT INTO `cms_module_search_index` VALUES (75, 'đĩa', 1);
INSERT INTO `cms_module_search_index` VALUES (75, 'cần', 1);
INSERT INTO `cms_module_search_index` VALUES (75, 'chỉ', 1);
INSERT INTO `cms_module_search_index` VALUES (75, 'đó', 1);
INSERT INTO `cms_module_search_index` VALUES (75, 'lúc', 1);
INSERT INTO `cms_module_search_index` VALUES (75, 'ngậy', 1);
INSERT INTO `cms_module_search_index` VALUES (75, 'béo', 1);
INSERT INTO `cms_module_search_index` VALUES (75, 'lừng', 1);
INSERT INTO `cms_module_search_index` VALUES (75, 'thơm', 1);
INSERT INTO `cms_module_search_index` VALUES (75, 'dùng', 1);
INSERT INTO `cms_module_search_index` VALUES (75, 'chế', 1);
INSERT INTO `cms_module_search_index` VALUES (75, 'bùi', 2);
INSERT INTO `cms_module_search_index` VALUES (75, 'mấy', 2);
INSERT INTO `cms_module_search_index` VALUES (75, 'thêm', 2);
INSERT INTO `cms_module_search_index` VALUES (75, 'muối', 1);
INSERT INTO `cms_module_search_index` VALUES (75, 'mắm', 1);
INSERT INTO `cms_module_search_index` VALUES (75, 'ngấm', 1);
INSERT INTO `cms_module_search_index` VALUES (75, 'cho', 1);
INSERT INTO `cms_module_search_index` VALUES (75, 'kỹ', 1);
INSERT INTO `cms_module_search_index` VALUES (75, 'đc', 2);
INSERT INTO `cms_module_search_index` VALUES (75, 'đã', 1);
INSERT INTO `cms_module_search_index` VALUES (75, 'vì', 1);
INSERT INTO `cms_module_search_index` VALUES (75, 'đ', 1);
INSERT INTO `cms_module_search_index` VALUES (75, 'đậm', 1);
INSERT INTO `cms_module_search_index` VALUES (75, 'sật', 1);
INSERT INTO `cms_module_search_index` VALUES (75, 'sần', 1);
INSERT INTO `cms_module_search_index` VALUES (75, 'miếng', 2);
INSERT INTO `cms_module_search_index` VALUES (75, 'gọi', 1);
INSERT INTO `cms_module_search_index` VALUES (75, 'cũng', 1);
INSERT INTO `cms_module_search_index` VALUES (75, 'chặt', 1);
INSERT INTO `cms_module_search_index` VALUES (75, 'khách', 1);
INSERT INTO `cms_module_search_index` VALUES (75, 'như', 1);
INSERT INTO `cms_module_search_index` VALUES (75, 'hầu', 1);
INSERT INTO `cms_module_search_index` VALUES (75, 'quán', 1);
INSERT INTO `cms_module_search_index` VALUES (75, 'của', 1);
INSERT INTO `cms_module_search_index` VALUES (75, 'khẩu', 1);
INSERT INTO `cms_module_search_index` VALUES (75, 'khoái', 1);
INSERT INTO `cms_module_search_index` VALUES (75, 'ăn', 3);
INSERT INTO `cms_module_search_index` VALUES (75, 'món', 2);
INSERT INTO `cms_module_search_index` VALUES (75, 'một', 1);
INSERT INTO `cms_module_search_index` VALUES (75, 'ngan', 1);
INSERT INTO `cms_module_search_index` VALUES (75, 'tiết', 3);
INSERT INTO `cms_module_search_index` VALUES (75, 'măng', 3);
INSERT INTO `cms_module_search_index` VALUES (75, 'canh', 2);
INSERT INTO `cms_module_search_index` VALUES (78, 'ít', 1);
INSERT INTO `cms_module_search_index` VALUES (78, 'không', 1);
INSERT INTO `cms_module_search_index` VALUES (78, 'khiến', 1);
INSERT INTO `cms_module_search_index` VALUES (78, 'lạ', 1);
INSERT INTO `cms_module_search_index` VALUES (78, 'kỳ', 1);
INSERT INTO `cms_module_search_index` VALUES (78, 'thấy', 1);
INSERT INTO `cms_module_search_index` VALUES (78, 'mắt', 1);
INSERT INTO `cms_module_search_index` VALUES (78, 'tận', 1);
INSERT INTO `cms_module_search_index` VALUES (78, 'được', 1);
INSERT INTO `cms_module_search_index` VALUES (78, 'tôi', 1);
INSERT INTO `cms_module_search_index` VALUES (78, 'tiên', 1);
INSERT INTO `cms_module_search_index` VALUES (78, 'đầu', 1);
INSERT INTO `cms_module_search_index` VALUES (78, 'lần', 1);
INSERT INTO `cms_module_search_index` VALUES (78, 'đây', 1);
INSERT INTO `cms_module_search_index` VALUES (78, 'thật', 1);
INSERT INTO `cms_module_search_index` VALUES (78, 'quả', 2);
INSERT INTO `cms_module_search_index` VALUES (78, 'thì', 1);
INSERT INTO `cms_module_search_index` VALUES (78, 'ong', 1);
INSERT INTO `cms_module_search_index` VALUES (78, 'nhưng', 1);
INSERT INTO `cms_module_search_index` VALUES (78, 'vịt', 1);
INSERT INTO `cms_module_search_index` VALUES (78, 'dê', 1);
INSERT INTO `cms_module_search_index` VALUES (78, 'lợn', 1);
INSERT INTO `cms_module_search_index` VALUES (78, 'đã', 1);
INSERT INTO `cms_module_search_index` VALUES (78, 'người', 2);
INSERT INTO `cms_module_search_index` VALUES (78, 'nam', 1);
INSERT INTO `cms_module_search_index` VALUES (78, 'việt', 1);
INSERT INTO `cms_module_search_index` VALUES (78, 'Ở', 1);
INSERT INTO `cms_module_search_index` VALUES (78, 'ngan', 3);
INSERT INTO `cms_module_search_index` VALUES (78, 'canh', 5);
INSERT INTO `cms_module_search_index` VALUES (78, 'tiết', 3);
INSERT INTO `cms_module_search_index` VALUES (78, 'tiet', 2);
INSERT INTO `cms_module_search_index` VALUES (78, 'jpg', 2);
INSERT INTO `cms_module_search_index` VALUES (78, 'ngan_thumb', 1);
INSERT INTO `cms_module_search_index` VALUES (78, '20000', 1);
INSERT INTO `cms_module_search_index` VALUES (78, 'hot', 1);
INSERT INTO `cms_module_search_index` VALUES (80, 'hệ', 4);
INSERT INTO `cms_module_search_index` VALUES (80, 'liên', 4);
INSERT INTO `cms_module_search_index` VALUES (405, 'món', 1);
INSERT INTO `cms_module_search_index` VALUES (405, 'jpg', 1);
INSERT INTO `cms_module_search_index` VALUES (405, 'dvh', 1);
INSERT INTO `cms_module_search_index` VALUES (405, 'gòn', 1);
INSERT INTO `cms_module_search_index` VALUES (405, 'sài', 1);
INSERT INTO `cms_module_search_index` VALUES (405, 'hưng', 1);
INSERT INTO `cms_module_search_index` VALUES (405, 'vĩnh', 1);
INSERT INTO `cms_module_search_index` VALUES (405, 'Đàm', 1);
INSERT INTO `cms_module_search_index` VALUES (83, 'vietnamese', 4);
INSERT INTO `cms_module_search_index` VALUES (84, 'john', 1);
INSERT INTO `cms_module_search_index` VALUES (84, 'doggett', 1);
INSERT INTO `cms_module_search_index` VALUES (84, 'head', 1);
INSERT INTO `cms_module_search_index` VALUES (84, 'chef', 1);
INSERT INTO `cms_module_search_index` VALUES (84, 'staff', 1);
INSERT INTO `cms_module_search_index` VALUES (84, 'jpg', 1);
INSERT INTO `cms_module_search_index` VALUES (84, 'nullam', 1);
INSERT INTO `cms_module_search_index` VALUES (84, 'quis', 1);
INSERT INTO `cms_module_search_index` VALUES (84, 'ante', 1);
INSERT INTO `cms_module_search_index` VALUES (84, 'etiam', 1);
INSERT INTO `cms_module_search_index` VALUES (84, 'sit', 2);
INSERT INTO `cms_module_search_index` VALUES (84, 'amet', 2);
INSERT INTO `cms_module_search_index` VALUES (84, 'orci', 1);
INSERT INTO `cms_module_search_index` VALUES (84, 'eget', 1);
INSERT INTO `cms_module_search_index` VALUES (84, 'eros', 1);
INSERT INTO `cms_module_search_index` VALUES (84, 'faucibus', 1);
INSERT INTO `cms_module_search_index` VALUES (84, 'tincidunt', 1);
INSERT INTO `cms_module_search_index` VALUES (84, 'duis', 1);
INSERT INTO `cms_module_search_index` VALUES (84, 'leo', 1);
INSERT INTO `cms_module_search_index` VALUES (84, 'sed', 1);
INSERT INTO `cms_module_search_index` VALUES (84, 'fringilla', 1);
INSERT INTO `cms_module_search_index` VALUES (84, 'mauris', 1);
INSERT INTO `cms_module_search_index` VALUES (84, 'nibh', 1);
INSERT INTO `cms_module_search_index` VALUES (84, 'donec', 1);
INSERT INTO `cms_module_search_index` VALUES (84, 'sodales', 1);
INSERT INTO `cms_module_search_index` VALUES (84, 'sagittis', 1);
INSERT INTO `cms_module_search_index` VALUES (84, 'magna', 1);
INSERT INTO `cms_module_search_index` VALUES (84, 'aenean', 1);
INSERT INTO `cms_module_search_index` VALUES (84, 'commodo', 1);
INSERT INTO `cms_module_search_index` VALUES (84, 'ligula', 1);
INSERT INTO `cms_module_search_index` VALUES (84, 'songviytuong', 4);
INSERT INTO `cms_module_search_index` VALUES (85, 'hình', 1);
INSERT INTO `cms_module_search_index` VALUES (85, 'ảnh', 1);
INSERT INTO `cms_module_search_index` VALUES (85, 'slide', 1);
INSERT INTO `cms_module_search_index` VALUES (85, 'image', 1);
INSERT INTO `cms_module_search_index` VALUES (85, 'jpg', 1);
INSERT INTO `cms_module_search_index` VALUES (86, 'hình', 1);
INSERT INTO `cms_module_search_index` VALUES (86, 'ảnh', 1);
INSERT INTO `cms_module_search_index` VALUES (86, 'slide', 1);
INSERT INTO `cms_module_search_index` VALUES (86, 'image', 1);
INSERT INTO `cms_module_search_index` VALUES (86, 'jpg', 1);
INSERT INTO `cms_module_search_index` VALUES (87, 'hình', 1);
INSERT INTO `cms_module_search_index` VALUES (87, 'ảnh', 1);
INSERT INTO `cms_module_search_index` VALUES (87, 'slide', 1);
INSERT INTO `cms_module_search_index` VALUES (87, 'image', 1);
INSERT INTO `cms_module_search_index` VALUES (87, 'jpg', 1);
INSERT INTO `cms_module_search_index` VALUES (376, 'gallery', 1);
INSERT INTO `cms_module_search_index` VALUES (376, 'ảnh', 1);
INSERT INTO `cms_module_search_index` VALUES (376, 'hình', 1);
INSERT INTO `cms_module_search_index` VALUES (377, 'gallery', 1);
INSERT INTO `cms_module_search_index` VALUES (377, 'ảnh', 1);
INSERT INTO `cms_module_search_index` VALUES (377, 'hình', 1);
INSERT INTO `cms_module_search_index` VALUES (380, 'jpg', 1);
INSERT INTO `cms_module_search_index` VALUES (380, 'gallery', 1);
INSERT INTO `cms_module_search_index` VALUES (380, 'ảnh', 1);
INSERT INTO `cms_module_search_index` VALUES (382, 'gallery', 1);
INSERT INTO `cms_module_search_index` VALUES (382, 'ảnh', 1);
INSERT INTO `cms_module_search_index` VALUES (382, 'hình', 1);
INSERT INTO `cms_module_search_index` VALUES (381, 'gallery', 1);
INSERT INTO `cms_module_search_index` VALUES (381, 'ảnh', 1);
INSERT INTO `cms_module_search_index` VALUES (378, 'gallery', 1);
INSERT INTO `cms_module_search_index` VALUES (378, 'ảnh', 1);
INSERT INTO `cms_module_search_index` VALUES (378, 'hình', 1);
INSERT INTO `cms_module_search_index` VALUES (381, 'hình', 1);
INSERT INTO `cms_module_search_index` VALUES (383, 'gallery', 1);
INSERT INTO `cms_module_search_index` VALUES (383, 'ảnh', 1);
INSERT INTO `cms_module_search_index` VALUES (384, 'gallery', 1);
INSERT INTO `cms_module_search_index` VALUES (384, 'ảnh', 1);
INSERT INTO `cms_module_search_index` VALUES (384, 'hình', 1);
INSERT INTO `cms_module_search_index` VALUES (385, 'gallery', 1);
INSERT INTO `cms_module_search_index` VALUES (385, 'ảnh', 1);
INSERT INTO `cms_module_search_index` VALUES (385, 'hình', 1);
INSERT INTO `cms_module_search_index` VALUES (386, 'gallery', 1);
INSERT INTO `cms_module_search_index` VALUES (386, 'ảnh', 1);
INSERT INTO `cms_module_search_index` VALUES (386, 'hình', 1);
INSERT INTO `cms_module_search_index` VALUES (383, 'hình', 1);
INSERT INTO `cms_module_search_index` VALUES (303, 'gallery', 1);
INSERT INTO `cms_module_search_index` VALUES (298, 'ngan', 1);
INSERT INTO `cms_module_search_index` VALUES (298, 'khoa', 1);
INSERT INTO `cms_module_search_index` VALUES (298, 'jpg', 1);
INSERT INTO `cms_module_search_index` VALUES (298, 'fadeinleft', 1);
INSERT INTO `cms_module_search_index` VALUES (300, 'ngan', 1);
INSERT INTO `cms_module_search_index` VALUES (300, 'khoa', 1);
INSERT INTO `cms_module_search_index` VALUES (300, 'jpg', 1);
INSERT INTO `cms_module_search_index` VALUES (300, '499', 1);
INSERT INTO `cms_module_search_index` VALUES (300, 'fadeinright', 1);
INSERT INTO `cms_module_search_index` VALUES (300, '499k', 1);
INSERT INTO `cms_module_search_index` VALUES (300, 'set', 2);
INSERT INTO `cms_module_search_index` VALUES (106, 'mô', 1);
INSERT INTO `cms_module_search_index` VALUES (106, 'jpg', 1);
INSERT INTO `cms_module_search_index` VALUES (106, 'image', 1);
INSERT INTO `cms_module_search_index` VALUES (106, 'featured', 1);
INSERT INTO `cms_module_search_index` VALUES (106, 'fadeinright', 1);
INSERT INTO `cms_module_search_index` VALUES (106, 'blog', 1);
INSERT INTO `cms_module_search_index` VALUES (107, 'https', 1);
INSERT INTO `cms_module_search_index` VALUES (107, 'ngan', 1);
INSERT INTO `cms_module_search_index` VALUES (107, 'khoa', 1);
INSERT INTO `cms_module_search_index` VALUES (107, 'tả', 1);
INSERT INTO `cms_module_search_index` VALUES (107, 'mô', 1);
INSERT INTO `cms_module_search_index` VALUES (107, 'jpg', 1);
INSERT INTO `cms_module_search_index` VALUES (107, 'image', 1);
INSERT INTO `cms_module_search_index` VALUES (298, 'deal', 2);
INSERT INTO `cms_module_search_index` VALUES (298, 'happy', 2);
INSERT INTO `cms_module_search_index` VALUES (107, 'featured', 1);
INSERT INTO `cms_module_search_index` VALUES (107, 'fadeinright', 1);
INSERT INTO `cms_module_search_index` VALUES (107, 'blog', 1);
INSERT INTO `cms_module_search_index` VALUES (106, 'tả', 1);
INSERT INTO `cms_module_search_index` VALUES (106, 'khoa', 1);
INSERT INTO `cms_module_search_index` VALUES (106, 'ngan', 1);
INSERT INTO `cms_module_search_index` VALUES (107, 'www', 1);
INSERT INTO `cms_module_search_index` VALUES (107, 'google', 1);
INSERT INTO `cms_module_search_index` VALUES (107, 'com', 1);
INSERT INTO `cms_module_search_index` VALUES (387, 'gallery', 1);
INSERT INTO `cms_module_search_index` VALUES (387, 'ảnh', 1);
INSERT INTO `cms_module_search_index` VALUES (387, 'hình', 1);
INSERT INTO `cms_module_search_index` VALUES (388, 'gallery', 1);
INSERT INTO `cms_module_search_index` VALUES (388, 'ngan', 1);
INSERT INTO `cms_module_search_index` VALUES (388, 'khoa', 1);
INSERT INTO `cms_module_search_index` VALUES (388, 'hưng', 1);
INSERT INTO `cms_module_search_index` VALUES (388, 'vĩnh', 1);
INSERT INTO `cms_module_search_index` VALUES (388, 'Đàm', 1);
INSERT INTO `cms_module_search_index` VALUES (389, 'gallery', 1);
INSERT INTO `cms_module_search_index` VALUES (389, 'ngan', 1);
INSERT INTO `cms_module_search_index` VALUES (389, 'khoa', 1);
INSERT INTO `cms_module_search_index` VALUES (389, 'dũng', 1);
INSERT INTO `cms_module_search_index` VALUES (389, 'quang', 1);
INSERT INTO `cms_module_search_index` VALUES (390, 'gallery', 1);
INSERT INTO `cms_module_search_index` VALUES (390, 'ngan', 1);
INSERT INTO `cms_module_search_index` VALUES (390, 'khoa', 1);
INSERT INTO `cms_module_search_index` VALUES (390, 'kiều', 1);
INSERT INTO `cms_module_search_index` VALUES (390, 'bằng', 1);
INSERT INTO `cms_module_search_index` VALUES (392, 'gallery', 1);
INSERT INTO `cms_module_search_index` VALUES (392, 'ảnh', 1);
INSERT INTO `cms_module_search_index` VALUES (392, 'hình', 1);
INSERT INTO `cms_module_search_index` VALUES (393, 'gallery', 1);
INSERT INTO `cms_module_search_index` VALUES (393, 'ảnh', 1);
INSERT INTO `cms_module_search_index` VALUES (393, 'hình', 1);
INSERT INTO `cms_module_search_index` VALUES (394, 'jpg', 1);
INSERT INTO `cms_module_search_index` VALUES (394, 'gallery', 1);
INSERT INTO `cms_module_search_index` VALUES (394, 'ngan', 1);
INSERT INTO `cms_module_search_index` VALUES (394, 'khoa', 1);
INSERT INTO `cms_module_search_index` VALUES (394, 'phương', 1);
INSERT INTO `cms_module_search_index` VALUES (394, 'khánh', 1);
INSERT INTO `cms_module_search_index` VALUES (395, 'gallery', 1);
INSERT INTO `cms_module_search_index` VALUES (395, 'ảnh', 1);
INSERT INTO `cms_module_search_index` VALUES (395, 'hình', 1);
INSERT INTO `cms_module_search_index` VALUES (396, 'gallery', 1);
INSERT INTO `cms_module_search_index` VALUES (396, 'ảnh', 1);
INSERT INTO `cms_module_search_index` VALUES (396, 'hình', 1);
INSERT INTO `cms_module_search_index` VALUES (398, 'jpg', 1);
INSERT INTO `cms_module_search_index` VALUES (398, 'gallery', 1);
INSERT INTO `cms_module_search_index` VALUES (398, 'ngan', 1);
INSERT INTO `cms_module_search_index` VALUES (398, 'khoa', 1);
INSERT INTO `cms_module_search_index` VALUES (398, 'dương', 1);
INSERT INTO `cms_module_search_index` VALUES (398, 'tùng', 1);
INSERT INTO `cms_module_search_index` VALUES (463, 'đĩa', 1);
INSERT INTO `cms_module_search_index` VALUES (463, '90000', 1);
INSERT INTO `cms_module_search_index` VALUES (456, '30000', 1);
INSERT INTO `cms_module_search_index` VALUES (456, 'jpg', 2);
INSERT INTO `cms_module_search_index` VALUES (456, 'ngan_thumb', 1);
INSERT INTO `cms_module_search_index` VALUES (457, '50000', 1);
INSERT INTO `cms_module_search_index` VALUES (457, 'jpg', 1);
INSERT INTO `cms_module_search_index` VALUES (457, 'ngan_thumb', 1);
INSERT INTO `cms_module_search_index` VALUES (130, 'canh', 1);
INSERT INTO `cms_module_search_index` VALUES (130, 'kim', 1);
INSERT INTO `cms_module_search_index` VALUES (130, 'chi', 1);
INSERT INTO `cms_module_search_index` VALUES (130, 'ngan', 1);
INSERT INTO `cms_module_search_index` VALUES (130, '16000', 1);
INSERT INTO `cms_module_search_index` VALUES (130, 'nồi', 1);
INSERT INTO `cms_module_search_index` VALUES (132, 'ngan', 1);
INSERT INTO `cms_module_search_index` VALUES (132, 'giả', 1);
INSERT INTO `cms_module_search_index` VALUES (132, 'cầy', 1);
INSERT INTO `cms_module_search_index` VALUES (132, '165000', 1);
INSERT INTO `cms_module_search_index` VALUES (132, 'nồi', 1);
INSERT INTO `cms_module_search_index` VALUES (133, 'Đáy', 1);
INSERT INTO `cms_module_search_index` VALUES (133, 'lẩu', 1);
INSERT INTO `cms_module_search_index` VALUES (133, 'cháo', 1);
INSERT INTO `cms_module_search_index` VALUES (133, 'ngan', 1);
INSERT INTO `cms_module_search_index` VALUES (133, '214500', 1);
INSERT INTO `cms_module_search_index` VALUES (133, 'nồi', 1);
INSERT INTO `cms_module_search_index` VALUES (262, 'bát', 1);
INSERT INTO `cms_module_search_index` VALUES (262, '40000', 1);
INSERT INTO `cms_module_search_index` VALUES (262, 'jpg', 2);
INSERT INTO `cms_module_search_index` VALUES (262, 'thumb', 1);
INSERT INTO `cms_module_search_index` VALUES (269, '100000', 1);
INSERT INTO `cms_module_search_index` VALUES (269, 'luoc', 1);
INSERT INTO `cms_module_search_index` VALUES (136, 'cải', 1);
INSERT INTO `cms_module_search_index` VALUES (136, 'xào', 1);
INSERT INTO `cms_module_search_index` VALUES (136, 'nấm', 1);
INSERT INTO `cms_module_search_index` VALUES (136, '38500', 1);
INSERT INTO `cms_module_search_index` VALUES (136, 'đĩa', 1);
INSERT INTO `cms_module_search_index` VALUES (280, '180', 1);
INSERT INTO `cms_module_search_index` VALUES (280, 'người ', 1);
INSERT INTO `cms_module_search_index` VALUES (280, 'suất', 2);
INSERT INTO `cms_module_search_index` VALUES (280, '000', 2);
INSERT INTO `cms_module_search_index` VALUES (280, '150', 1);
INSERT INTO `cms_module_search_index` VALUES (280, 'giá', 1);
INSERT INTO `cms_module_search_index` VALUES (251, 'tran_thumb', 1);
INSERT INTO `cms_module_search_index` VALUES (251, 'gia', 2);
INSERT INTO `cms_module_search_index` VALUES (251, 'hanh', 2);
INSERT INTO `cms_module_search_index` VALUES (281, '35000', 1);
INSERT INTO `cms_module_search_index` VALUES (281, 'chien', 1);
INSERT INTO `cms_module_search_index` VALUES (281, 'jpg', 2);
INSERT INTO `cms_module_search_index` VALUES (144, '385000', 1);
INSERT INTO `cms_module_search_index` VALUES (144, 'đĩa', 1);
INSERT INTO `cms_module_search_index` VALUES (263, 'ngan_thumb', 1);
INSERT INTO `cms_module_search_index` VALUES (263, 'mien', 2);
INSERT INTO `cms_module_search_index` VALUES (263, 'ngan', 2);
INSERT INTO `cms_module_search_index` VALUES (263, 'miến', 1);
INSERT INTO `cms_module_search_index` VALUES (435, '60k', 1);
INSERT INTO `cms_module_search_index` VALUES (435, 'tron', 1);
INSERT INTO `cms_module_search_index` VALUES (435, 'jpg', 2);
INSERT INTO `cms_module_search_index` VALUES (241, 'jpg', 2);
INSERT INTO `cms_module_search_index` VALUES (242, '20000', 1);
INSERT INTO `cms_module_search_index` VALUES (242, 'hanoi', 1);
INSERT INTO `cms_module_search_index` VALUES (242, 'jpg', 2);
INSERT INTO `cms_module_search_index` VALUES (243, 'chai', 1);
INSERT INTO `cms_module_search_index` VALUES (243, 'saigon', 1);
INSERT INTO `cms_module_search_index` VALUES (243, 'jpg', 2);
INSERT INTO `cms_module_search_index` VALUES (244, '30000', 1);
INSERT INTO `cms_module_search_index` VALUES (244, 'jpg', 2);
INSERT INTO `cms_module_search_index` VALUES (244, 'bach_thumb', 1);
INSERT INTO `cms_module_search_index` VALUES (245, 'jpg', 2);
INSERT INTO `cms_module_search_index` VALUES (245, 'borsmi_thumb', 1);
INSERT INTO `cms_module_search_index` VALUES (245, 'ruou', 2);
INSERT INTO `cms_module_search_index` VALUES (246, 'giá', 1);
INSERT INTO `cms_module_search_index` VALUES (246, 'noi', 1);
INSERT INTO `cms_module_search_index` VALUES (246, 'nội', 1);
INSERT INTO `cms_module_search_index` VALUES (246, 'hà', 1);
INSERT INTO `cms_module_search_index` VALUES (154, 'rượu', 1);
INSERT INTO `cms_module_search_index` VALUES (154, 'gạo', 1);
INSERT INTO `cms_module_search_index` VALUES (154, 'quốc', 1);
INSERT INTO `cms_module_search_index` VALUES (154, '165000', 1);
INSERT INTO `cms_module_search_index` VALUES (154, 'chai', 1);
INSERT INTO `cms_module_search_index` VALUES (290, 'ruou', 2);
INSERT INTO `cms_module_search_index` VALUES (290, 'liquor', 1);
INSERT INTO `cms_module_search_index` VALUES (290, 'rice', 1);
INSERT INTO `cms_module_search_index` VALUES (290, 'korean', 1);
INSERT INTO `cms_module_search_index` VALUES (292, 'tươi', 1);
INSERT INTO `cms_module_search_index` VALUES (292, 'chanh', 3);
INSERT INTO `cms_module_search_index` VALUES (292, 'nước', 1);
INSERT INTO `cms_module_search_index` VALUES (293, 'tươi', 1);
INSERT INTO `cms_module_search_index` VALUES (293, 'cam', 3);
INSERT INTO `cms_module_search_index` VALUES (293, 'nước', 1);
INSERT INTO `cms_module_search_index` VALUES (158, 'coca', 1);
INSERT INTO `cms_module_search_index` VALUES (158, '22000', 1);
INSERT INTO `cms_module_search_index` VALUES (158, 'chai', 1);
INSERT INTO `cms_module_search_index` VALUES (159, 'cam', 1);
INSERT INTO `cms_module_search_index` VALUES (159, 'ép', 1);
INSERT INTO `cms_module_search_index` VALUES (159, '22000', 1);
INSERT INTO `cms_module_search_index` VALUES (160, 'bí', 1);
INSERT INTO `cms_module_search_index` VALUES (160, 'đao', 1);
INSERT INTO `cms_module_search_index` VALUES (160, '22000', 1);
INSERT INTO `cms_module_search_index` VALUES (160, 'lon', 1);
INSERT INTO `cms_module_search_index` VALUES (161, 'bò', 1);
INSERT INTO `cms_module_search_index` VALUES (161, 'húc', 1);
INSERT INTO `cms_module_search_index` VALUES (161, '22000', 1);
INSERT INTO `cms_module_search_index` VALUES (161, 'lon', 1);
INSERT INTO `cms_module_search_index` VALUES (295, 'jpg', 1);
INSERT INTO `cms_module_search_index` VALUES (295, 'lavie', 2);
INSERT INTO `cms_module_search_index` VALUES (295, 'khoáng', 1);
INSERT INTO `cms_module_search_index` VALUES (463, 'jpg', 1);
INSERT INTO `cms_module_search_index` VALUES (463, '90k', 1);
INSERT INTO `cms_module_search_index` VALUES (463, 'ngan', 2);
INSERT INTO `cms_module_search_index` VALUES (269, 'jpg', 2);
INSERT INTO `cms_module_search_index` VALUES (269, 'luoc_thumb', 1);
INSERT INTO `cms_module_search_index` VALUES (456, 'ngan', 2);
INSERT INTO `cms_module_search_index` VALUES (456, 'khoa', 3);
INSERT INTO `cms_module_search_index` VALUES (456, 'chi', 3);
INSERT INTO `cms_module_search_index` VALUES (427, 'suất', 1);
INSERT INTO `cms_module_search_index` VALUES (427, 'song', 1);
INSERT INTO `cms_module_search_index` VALUES (427, 'jpg', 2);
INSERT INTO `cms_module_search_index` VALUES (427, 'song_thumb', 1);
INSERT INTO `cms_module_search_index` VALUES (427, 'sống', 1);
INSERT INTO `cms_module_search_index` VALUES (251, 'beansprouts', 1);
INSERT INTO `cms_module_search_index` VALUES (251, 'onion', 1);
INSERT INTO `cms_module_search_index` VALUES (251, 'boiled', 1);
INSERT INTO `cms_module_search_index` VALUES (212, 'món', 3);
INSERT INTO `cms_module_search_index` VALUES (212, 'các', 3);
INSERT INTO `cms_module_search_index` VALUES (212, 'list', 1);
INSERT INTO `cms_module_search_index` VALUES (212, 'một', 2);
INSERT INTO `cms_module_search_index` VALUES (212, 'tay', 1);
INSERT INTO `cms_module_search_index` VALUES (212, 'trong', 1);
INSERT INTO `cms_module_search_index` VALUES (212, 'có', 2);
INSERT INTO `cms_module_search_index` VALUES (212, 'phá', 1);
INSERT INTO `cms_module_search_index` VALUES (212, 'khám', 1);
INSERT INTO `cms_module_search_index` VALUES (212, 'thích', 2);
INSERT INTO `cms_module_search_index` VALUES (212, 'bạn', 3);
INSERT INTO `cms_module_search_index` VALUES (212, 'inset', 1);
INSERT INTO `cms_module_search_index` VALUES (212, 'jpg', 2);
INSERT INTO `cms_module_search_index` VALUES (212, 'main', 1);
INSERT INTO `cms_module_search_index` VALUES (212, 'trưng', 2);
INSERT INTO `cms_module_search_index` VALUES (212, 'hai', 2);
INSERT INTO `cms_module_search_index` VALUES (212, 'ngan', 3);
INSERT INTO `cms_module_search_index` VALUES (212, 'khoa', 2);
INSERT INTO `cms_module_search_index` VALUES (212, 'nh', 2);
INSERT INTO `cms_module_search_index` VALUES (182, 'nh', 4);
INSERT INTO `cms_module_search_index` VALUES (182, 'khoa', 2);
INSERT INTO `cms_module_search_index` VALUES (182, 'ngan', 3);
INSERT INTO `cms_module_search_index` VALUES (182, 'hai', 1);
INSERT INTO `cms_module_search_index` VALUES (182, 'trưng', 1);
INSERT INTO `cms_module_search_index` VALUES (182, 'với', 3);
INSERT INTO `cms_module_search_index` VALUES (182, 'thâm', 1);
INSERT INTO `cms_module_search_index` VALUES (182, 'niên', 1);
INSERT INTO `cms_module_search_index` VALUES (182, 'gần', 1);
INSERT INTO `cms_module_search_index` VALUES (182, 'năm', 1);
INSERT INTO `cms_module_search_index` VALUES (182, 'hoạt', 1);
INSERT INTO `cms_module_search_index` VALUES (182, 'động', 1);
INSERT INTO `cms_module_search_index` VALUES (182, 'xưa', 1);
INSERT INTO `cms_module_search_index` VALUES (182, 'nay', 1);
INSERT INTO `cms_module_search_index` VALUES (182, 'đã', 1);
INSERT INTO `cms_module_search_index` VALUES (182, 'trở', 1);
INSERT INTO `cms_module_search_index` VALUES (182, 'th', 1);
INSERT INTO `cms_module_search_index` VALUES (182, 'địa', 1);
INSERT INTO `cms_module_search_index` VALUES (182, 'chỉ', 1);
INSERT INTO `cms_module_search_index` VALUES (182, 'cực', 1);
INSERT INTO `cms_module_search_index` VALUES (182, 'kỳ', 2);
INSERT INTO `cms_module_search_index` VALUES (182, 'quen', 1);
INSERT INTO `cms_module_search_index` VALUES (182, 'thuộc', 1);
INSERT INTO `cms_module_search_index` VALUES (182, 'không', 3);
INSERT INTO `cms_module_search_index` VALUES (182, 'ít', 1);
INSERT INTO `cms_module_search_index` VALUES (182, 'người', 1);
INSERT INTO `cms_module_search_index` VALUES (182, 'dân', 1);
INSERT INTO `cms_module_search_index` VALUES (182, 'nội', 1);
INSERT INTO `cms_module_search_index` VALUES (182, 'sở', 2);
INSERT INTO `cms_module_search_index` VALUES (182, 'dĩ', 1);
INSERT INTO `cms_module_search_index` VALUES (182, 'được', 1);
INSERT INTO `cms_module_search_index` VALUES (182, 'yêu', 1);
INSERT INTO `cms_module_search_index` VALUES (182, 'thích', 1);
INSERT INTO `cms_module_search_index` VALUES (182, 'như', 1);
INSERT INTO `cms_module_search_index` VALUES (182, 'vậy', 1);
INSERT INTO `cms_module_search_index` VALUES (182, 'bởi', 1);
INSERT INTO `cms_module_search_index` VALUES (182, 'hữu', 1);
INSERT INTO `cms_module_search_index` VALUES (182, 'rất', 1);
INSERT INTO `cms_module_search_index` VALUES (182, 'nhiều', 1);
INSERT INTO `cms_module_search_index` VALUES (182, 'bí', 1);
INSERT INTO `cms_module_search_index` VALUES (182, 'quyết', 1);
INSERT INTO `cms_module_search_index` VALUES (182, 'hấp', 1);
INSERT INTO `cms_module_search_index` VALUES (182, 'dẫn', 1);
INSERT INTO `cms_module_search_index` VALUES (182, 'các', 2);
INSERT INTO `cms_module_search_index` VALUES (182, 'thực', 1);
INSERT INTO `cms_module_search_index` VALUES (182, 'khách', 1);
INSERT INTO `cms_module_search_index` VALUES (182, 'món', 1);
INSERT INTO `cms_module_search_index` VALUES (182, 'đặc', 1);
INSERT INTO `cms_module_search_index` VALUES (182, 'sản', 1);
INSERT INTO `cms_module_search_index` VALUES (182, 'mang', 1);
INSERT INTO `cms_module_search_index` VALUES (182, 'hương', 1);
INSERT INTO `cms_module_search_index` VALUES (182, 'vị', 1);
INSERT INTO `cms_module_search_index` VALUES (182, 'riêng', 1);
INSERT INTO `cms_module_search_index` VALUES (182, 'biệt', 1);
INSERT INTO `cms_module_search_index` VALUES (182, 'lẫn', 1);
INSERT INTO `cms_module_search_index` VALUES (182, 'lộn', 1);
INSERT INTO `cms_module_search_index` VALUES (182, 'bất', 1);
INSERT INTO `cms_module_search_index` VALUES (182, 'nơi', 1);
INSERT INTO `cms_module_search_index` VALUES (182, 'khác', 1);
INSERT INTO `cms_module_search_index` VALUES (182, 'gian', 1);
INSERT INTO `cms_module_search_index` VALUES (182, 'rộng', 1);
INSERT INTO `cms_module_search_index` VALUES (182, 'rãi', 1);
INSERT INTO `cms_module_search_index` VALUES (182, 'đẹp', 1);
INSERT INTO `cms_module_search_index` VALUES (182, 'mắt', 1);
INSERT INTO `cms_module_search_index` VALUES (182, 'cùng', 1);
INSERT INTO `cms_module_search_index` VALUES (182, 'một', 1);
INSERT INTO `cms_module_search_index` VALUES (182, 'đội', 1);
INSERT INTO `cms_module_search_index` VALUES (182, 'ngũ', 1);
INSERT INTO `cms_module_search_index` VALUES (182, 'nhân', 1);
INSERT INTO `cms_module_search_index` VALUES (182, 'viên', 1);
INSERT INTO `cms_module_search_index` VALUES (182, 'lịch', 1);
INSERT INTO `cms_module_search_index` VALUES (182, 'sự', 1);
INSERT INTO `cms_module_search_index` VALUES (182, 'chu', 1);
INSERT INTO `cms_module_search_index` VALUES (182, 'đáo', 1);
INSERT INTO `cms_module_search_index` VALUES (183, 'nh', 4);
INSERT INTO `cms_module_search_index` VALUES (183, 'khoa', 3);
INSERT INTO `cms_module_search_index` VALUES (183, 'ngan', 3);
INSERT INTO `cms_module_search_index` VALUES (183, 'hai', 1);
INSERT INTO `cms_module_search_index` VALUES (183, 'trưng', 1);
INSERT INTO `cms_module_search_index` VALUES (183, 'Đến', 1);
INSERT INTO `cms_module_search_index` VALUES (183, 'với', 1);
INSERT INTO `cms_module_search_index` VALUES (183, 'xưa', 1);
INSERT INTO `cms_module_search_index` VALUES (183, 'nay', 1);
INSERT INTO `cms_module_search_index` VALUES (183, 'đầu', 1);
INSERT INTO `cms_module_search_index` VALUES (183, 'tiên', 1);
INSERT INTO `cms_module_search_index` VALUES (183, 'bạn', 1);
INSERT INTO `cms_module_search_index` VALUES (183, 'sẽ', 1);
INSERT INTO `cms_module_search_index` VALUES (183, 'ho', 1);
INSERT INTO `cms_module_search_index` VALUES (183, 'to', 3);
INSERT INTO `cms_module_search_index` VALUES (183, 'yên', 1);
INSERT INTO `cms_module_search_index` VALUES (183, 'tâm', 1);
INSERT INTO `cms_module_search_index` VALUES (183, 'về', 1);
INSERT INTO `cms_module_search_index` VALUES (183, 'chất', 1);
INSERT INTO `cms_module_search_index` VALUES (183, 'lượng', 1);
INSERT INTO `cms_module_search_index` VALUES (183, 'vệ', 1);
INSERT INTO `cms_module_search_index` VALUES (183, 'sinh', 1);
INSERT INTO `cms_module_search_index` VALUES (183, 'thực', 1);
INSERT INTO `cms_module_search_index` VALUES (183, 'phẩm', 1);
INSERT INTO `cms_module_search_index` VALUES (183, 'bởi', 1);
INSERT INTO `cms_module_search_index` VALUES (183, 'bộ', 1);
INSERT INTO `cms_module_search_index` VALUES (183, 'nguyên', 1);
INSERT INTO `cms_module_search_index` VALUES (183, 'liệu', 1);
INSERT INTO `cms_module_search_index` VALUES (183, 'đều', 1);
INSERT INTO `cms_module_search_index` VALUES (183, 'được', 2);
INSERT INTO `cms_module_search_index` VALUES (183, 'nhập', 1);
INSERT INTO `cms_module_search_index` VALUES (183, 'từ', 1);
INSERT INTO `cms_module_search_index` VALUES (183, 'các', 1);
INSERT INTO `cms_module_search_index` VALUES (183, 'địa', 1);
INSERT INTO `cms_module_search_index` VALUES (183, 'phương', 1);
INSERT INTO `cms_module_search_index` VALUES (183, 'lân', 1);
INSERT INTO `cms_module_search_index` VALUES (183, 'cận', 1);
INSERT INTO `cms_module_search_index` VALUES (183, 'nội', 1);
INSERT INTO `cms_module_search_index` VALUES (183, 'đã', 2);
INSERT INTO `cms_module_search_index` VALUES (183, 'kiểm', 1);
INSERT INTO `cms_module_search_index` VALUES (183, 'dịch', 1);
INSERT INTO `cms_module_search_index` VALUES (183, 'lựa', 1);
INSERT INTO `cms_module_search_index` VALUES (183, 'chọn', 1);
INSERT INTO `cms_module_search_index` VALUES (183, 'kỹ', 1);
INSERT INTO `cms_module_search_index` VALUES (183, 'ng', 1);
INSERT INTO `cms_module_search_index` VALUES (183, '–', 2);
INSERT INTO `cms_module_search_index` VALUES (183, '“ngan', 1);
INSERT INTO `cms_module_search_index` VALUES (183, 'vịt', 1);
INSERT INTO `cms_module_search_index` VALUES (183, 'của', 2);
INSERT INTO `cms_module_search_index` VALUES (183, 'cũng', 1);
INSERT INTO `cms_module_search_index` VALUES (183, 'có', 1);
INSERT INTO `cms_module_search_index` VALUES (183, 'đầy', 1);
INSERT INTO `cms_module_search_index` VALUES (183, 'đủ', 1);
INSERT INTO `cms_module_search_index` VALUES (183, 'hộ', 1);
INSERT INTO `cms_module_search_index` VALUES (183, 'chiếu', 1);
INSERT INTO `cms_module_search_index` VALUES (183, 'chứng', 1);
INSERT INTO `cms_module_search_index` VALUES (183, 'minh', 1);
INSERT INTO `cms_module_search_index` VALUES (183, 'thư', 1);
INSERT INTO `cms_module_search_index` VALUES (183, 'như', 2);
INSERT INTO `cms_module_search_index` VALUES (183, 'ai”', 1);
INSERT INTO `cms_module_search_index` VALUES (183, 'lời', 1);
INSERT INTO `cms_module_search_index` VALUES (183, 'chủ', 1);
INSERT INTO `cms_module_search_index` VALUES (183, 'vui', 1);
INSERT INTO `cms_module_search_index` VALUES (183, 'tính', 1);
INSERT INTO `cms_module_search_index` VALUES (183, 'từng', 1);
INSERT INTO `cms_module_search_index` VALUES (183, 'nói', 1);
INSERT INTO `cms_module_search_index` VALUES (184, 'nh', 2);
INSERT INTO `cms_module_search_index` VALUES (184, 'khoa', 1);
INSERT INTO `cms_module_search_index` VALUES (184, 'ngan', 1);
INSERT INTO `cms_module_search_index` VALUES (184, 'hai', 1);
INSERT INTO `cms_module_search_index` VALUES (184, 'trưng', 1);
INSERT INTO `cms_module_search_index` VALUES (184, 'Đầu', 1);
INSERT INTO `cms_module_search_index` VALUES (184, 'ho', 1);
INSERT INTO `cms_module_search_index` VALUES (184, 'to', 1);
INSERT INTO `cms_module_search_index` VALUES (184, 'đảm', 1);
INSERT INTO `cms_module_search_index` VALUES (184, 'bảo', 1);
INSERT INTO `cms_module_search_index` VALUES (184, 'một', 2);
INSERT INTO `cms_module_search_index` VALUES (184, 'của', 2);
INSERT INTO `cms_module_search_index` VALUES (184, 'bí', 1);
INSERT INTO `cms_module_search_index` VALUES (184, 'quyết', 1);
INSERT INTO `cms_module_search_index` VALUES (184, 'th', 2);
INSERT INTO `cms_module_search_index` VALUES (184, 'công', 1);
INSERT INTO `cms_module_search_index` VALUES (184, 'nhưng', 2);
INSERT INTO `cms_module_search_index` VALUES (184, 'tất', 1);
INSERT INTO `cms_module_search_index` VALUES (184, 'nhiên', 1);
INSERT INTO `cms_module_search_index` VALUES (184, 'nếu', 1);
INSERT INTO `cms_module_search_index` VALUES (184, 'thiếu', 1);
INSERT INTO `cms_module_search_index` VALUES (184, 'hương', 2);
INSERT INTO `cms_module_search_index` VALUES (184, 'vị', 2);
INSERT INTO `cms_module_search_index` VALUES (184, 'thơm', 1);
INSERT INTO `cms_module_search_index` VALUES (184, 'ngon', 1);
INSERT INTO `cms_module_search_index` VALUES (184, 'thì', 1);
INSERT INTO `cms_module_search_index` VALUES (184, 'vẫn', 2);
INSERT INTO `cms_module_search_index` VALUES (184, 'không', 3);
INSERT INTO `cms_module_search_index` VALUES (184, 'đủ', 1);
INSERT INTO `cms_module_search_index` VALUES (184, 'để', 1);
INSERT INTO `cms_module_search_index` VALUES (184, 'giữ', 1);
INSERT INTO `cms_module_search_index` VALUES (184, 'chân', 1);
INSERT INTO `cms_module_search_index` VALUES (184, 'các', 3);
INSERT INTO `cms_module_search_index` VALUES (184, 'thực', 3);
INSERT INTO `cms_module_search_index` VALUES (184, 'khách', 2);
INSERT INTO `cms_module_search_index` VALUES (184, 'vốn', 1);
INSERT INTO `cms_module_search_index` VALUES (184, 'khá', 1);
INSERT INTO `cms_module_search_index` VALUES (184, '“kỹ', 1);
INSERT INTO `cms_module_search_index` VALUES (184, 'tính”', 1);
INSERT INTO `cms_module_search_index` VALUES (184, 'chỉ', 1);
INSERT INTO `cms_module_search_index` VALUES (184, 'sở', 1);
INSERT INTO `cms_module_search_index` VALUES (184, 'hữu', 1);
INSERT INTO `cms_module_search_index` VALUES (184, 'đơn', 2);
INSERT INTO `cms_module_search_index` VALUES (184, 'giản', 1);
INSERT INTO `cms_module_search_index` VALUES (184, 'hề', 1);
INSERT INTO `cms_module_search_index` VALUES (184, 'cầu', 1);
INSERT INTO `cms_module_search_index` VALUES (184, 'kỳ', 1);
INSERT INTO `cms_module_search_index` VALUES (184, 'có', 1);
INSERT INTO `cms_module_search_index` VALUES (184, 'những', 1);
INSERT INTO `cms_module_search_index` VALUES (184, 'cái', 1);
INSERT INTO `cms_module_search_index` VALUES (184, 'tên', 1);
INSERT INTO `cms_module_search_index` VALUES (184, 'nghe', 1);
INSERT INTO `cms_module_search_index` VALUES (184, 'thật', 2);
INSERT INTO `cms_module_search_index` VALUES (184, '“đắt”', 1);
INSERT INTO `cms_module_search_index` VALUES (184, 'món', 1);
INSERT INTO `cms_module_search_index` VALUES (184, 'ăn', 1);
INSERT INTO `cms_module_search_index` VALUES (184, 'dễ', 1);
INSERT INTO `cms_module_search_index` VALUES (184, 'ghi', 1);
INSERT INTO `cms_module_search_index` VALUES (184, 'dấu', 1);
INSERT INTO `cms_module_search_index` VALUES (184, 'ấn', 1);
INSERT INTO `cms_module_search_index` VALUES (184, 'trong', 1);
INSERT INTO `cms_module_search_index` VALUES (184, 'lòng', 1);
INSERT INTO `cms_module_search_index` VALUES (184, 'nhờ', 1);
INSERT INTO `cms_module_search_index` VALUES (184, 'khó', 1);
INSERT INTO `cms_module_search_index` VALUES (184, 'quên', 1);
INSERT INTO `cms_module_search_index` VALUES (185, 'nh', 3);
INSERT INTO `cms_module_search_index` VALUES (185, 'khoa', 1);
INSERT INTO `cms_module_search_index` VALUES (185, 'ngan', 10);
INSERT INTO `cms_module_search_index` VALUES (185, 'hai', 1);
INSERT INTO `cms_module_search_index` VALUES (185, 'trưng', 1);
INSERT INTO `cms_module_search_index` VALUES (185, 'các', 2);
INSERT INTO `cms_module_search_index` VALUES (185, 'món', 3);
INSERT INTO `cms_module_search_index` VALUES (185, 'ngon', 1);
INSERT INTO `cms_module_search_index` VALUES (185, 'nổi', 1);
INSERT INTO `cms_module_search_index` VALUES (185, 'tiếng', 1);
INSERT INTO `cms_module_search_index` VALUES (185, 'được', 4);
INSERT INTO `cms_module_search_index` VALUES (185, 'yêu', 2);
INSERT INTO `cms_module_search_index` VALUES (185, 'thích', 2);
INSERT INTO `cms_module_search_index` VALUES (185, 'nhất', 1);
INSERT INTO `cms_module_search_index` VALUES (185, 'của', 3);
INSERT INTO `cms_module_search_index` VALUES (185, 'gồm', 1);
INSERT INTO `cms_module_search_index` VALUES (185, 'có', 1);
INSERT INTO `cms_module_search_index` VALUES (185, 'lẩu', 2);
INSERT INTO `cms_module_search_index` VALUES (185, 'nướng', 3);
INSERT INTO `cms_module_search_index` VALUES (185, 'bún', 1);
INSERT INTO `cms_module_search_index` VALUES (185, 'hấp', 1);
INSERT INTO `cms_module_search_index` VALUES (185, 'tiết', 1);
INSERT INTO `cms_module_search_index` VALUES (185, 'canh', 1);
INSERT INTO `cms_module_search_index` VALUES (185, 'dồi', 1);
INSERT INTO `cms_module_search_index` VALUES (185, 'sụn', 1);
INSERT INTO `cms_module_search_index` VALUES (185, 'cháo', 1);
INSERT INTO `cms_module_search_index` VALUES (185, 'ngan…', 1);
INSERT INTO `cms_module_search_index` VALUES (185, 'trong', 1);
INSERT INTO `cms_module_search_index` VALUES (185, 'đó', 1);
INSERT INTO `cms_module_search_index` VALUES (185, 'luôn', 1);
INSERT INTO `cms_module_search_index` VALUES (185, 'thực', 1);
INSERT INTO `cms_module_search_index` VALUES (185, 'khách', 1);
INSERT INTO `cms_module_search_index` VALUES (185, 'lựa', 1);
INSERT INTO `cms_module_search_index` VALUES (185, 'chọn', 1);
INSERT INTO `cms_module_search_index` VALUES (185, 'đầu', 1);
INSERT INTO `cms_module_search_index` VALUES (185, 'sở', 1);
INSERT INTO `cms_module_search_index` VALUES (185, 'dĩ', 1);
INSERT INTO `cms_module_search_index` VALUES (185, 'như', 1);
INSERT INTO `cms_module_search_index` VALUES (185, 'vậy', 1);
INSERT INTO `cms_module_search_index` VALUES (185, 'bởi', 1);
INSERT INTO `cms_module_search_index` VALUES (185, 'đặc', 1);
INSERT INTO `cms_module_search_index` VALUES (185, 'biệt', 1);
INSERT INTO `cms_module_search_index` VALUES (185, 'chế', 1);
INSERT INTO `cms_module_search_index` VALUES (185, 'biến', 1);
INSERT INTO `cms_module_search_index` VALUES (185, 'theo', 1);
INSERT INTO `cms_module_search_index` VALUES (185, 'công', 2);
INSERT INTO `cms_module_search_index` VALUES (185, 'thức', 1);
INSERT INTO `cms_module_search_index` VALUES (185, 'người', 1);
INSERT INTO `cms_module_search_index` VALUES (185, 'quốc', 1);
INSERT INTO `cms_module_search_index` VALUES (185, 'chủ', 1);
INSERT INTO `cms_module_search_index` VALUES (185, 'đã', 1);
INSERT INTO `cms_module_search_index` VALUES (185, 'cất', 1);
INSERT INTO `cms_module_search_index` VALUES (185, 'sang', 1);
INSERT INTO `cms_module_search_index` VALUES (185, 'tận', 1);
INSERT INTO `cms_module_search_index` VALUES (185, 'nơi', 1);
INSERT INTO `cms_module_search_index` VALUES (185, 'để', 1);
INSERT INTO `cms_module_search_index` VALUES (185, 'tìm', 1);
INSERT INTO `cms_module_search_index` VALUES (185, 'hiểu', 1);
INSERT INTO `cms_module_search_index` VALUES (185, 'học', 1);
INSERT INTO `cms_module_search_index` VALUES (185, 'hỏi', 1);
INSERT INTO `cms_module_search_index` VALUES (241, 'heineken_thumb', 1);
INSERT INTO `cms_module_search_index` VALUES (241, 'beer', 1);
INSERT INTO `cms_module_search_index` VALUES (242, 'hanoi_thumb', 1);
INSERT INTO `cms_module_search_index` VALUES (242, 'beer', 1);
INSERT INTO `cms_module_search_index` VALUES (242, 'noi', 1);
INSERT INTO `cms_module_search_index` VALUES (243, 'saigon_thumb', 1);
INSERT INTO `cms_module_search_index` VALUES (243, 'beer', 1);
INSERT INTO `cms_module_search_index` VALUES (243, 'gon', 1);
INSERT INTO `cms_module_search_index` VALUES (244, 'beer', 1);
INSERT INTO `cms_module_search_index` VALUES (244, 'bach', 2);
INSERT INTO `cms_module_search_index` VALUES (244, 'truc', 3);
INSERT INTO `cms_module_search_index` VALUES (244, 'bạch', 1);
INSERT INTO `cms_module_search_index` VALUES (245, 'borsmi', 3);
INSERT INTO `cms_module_search_index` VALUES (245, 'nga', 3);
INSERT INTO `cms_module_search_index` VALUES (245, 'vodka', 4);
INSERT INTO `cms_module_search_index` VALUES (282, 'leo', 1);
INSERT INTO `cms_module_search_index` VALUES (282, 'jpg', 2);
INSERT INTO `cms_module_search_index` VALUES (282, 'leo_thumb', 1);
INSERT INTO `cms_module_search_index` VALUES (282, 'dua', 2);
INSERT INTO `cms_module_search_index` VALUES (282, 'chẻ', 1);
INSERT INTO `cms_module_search_index` VALUES (282, 'chuột', 1);
INSERT INTO `cms_module_search_index` VALUES (251, 'trần', 1);
INSERT INTO `cms_module_search_index` VALUES (251, 'giá', 1);
INSERT INTO `cms_module_search_index` VALUES (251, 'hành', 1);
INSERT INTO `cms_module_search_index` VALUES (427, 'rau', 3);
INSERT INTO `cms_module_search_index` VALUES (282, 'dưa', 1);
INSERT INTO `cms_module_search_index` VALUES (317, 'bun', 1);
INSERT INTO `cms_module_search_index` VALUES (317, 'jpg', 2);
INSERT INTO `cms_module_search_index` VALUES (317, 'bun_thumb', 1);
INSERT INTO `cms_module_search_index` VALUES (317, 'rối', 1);
INSERT INTO `cms_module_search_index` VALUES (317, 'bún', 1);
INSERT INTO `cms_module_search_index` VALUES (281, 'chien_thumb', 1);
INSERT INTO `cms_module_search_index` VALUES (281, 'tay', 2);
INSERT INTO `cms_module_search_index` VALUES (281, 'chiên', 1);
INSERT INTO `cms_module_search_index` VALUES (281, 'tây', 1);
INSERT INTO `cms_module_search_index` VALUES (281, 'khoai', 3);
INSERT INTO `cms_module_search_index` VALUES (262, 'bun', 2);
INSERT INTO `cms_module_search_index` VALUES (262, 'ngan', 3);
INSERT INTO `cms_module_search_index` VALUES (262, 'bún', 1);
INSERT INTO `cms_module_search_index` VALUES (212, 'hấp', 1);
INSERT INTO `cms_module_search_index` VALUES (212, 'dẫn', 1);
INSERT INTO `cms_module_search_index` VALUES (277, '370000', 1);
INSERT INTO `cms_module_search_index` VALUES (277, 'sau', 1);
INSERT INTO `cms_module_search_index` VALUES (277, 'jpg', 2);
INSERT INTO `cms_module_search_index` VALUES (277, 'sau_thumb', 1);
INSERT INTO `cms_module_search_index` VALUES (277, 'sấu', 1);
INSERT INTO `cms_module_search_index` VALUES (277, 'ngan', 3);
INSERT INTO `cms_module_search_index` VALUES (280, 'dứa', 1);
INSERT INTO `cms_module_search_index` VALUES (280, 'xào', 1);
INSERT INTO `cms_module_search_index` VALUES (280, 'mề', 1);
INSERT INTO `cms_module_search_index` VALUES (280, 'lòng', 1);
INSERT INTO `cms_module_search_index` VALUES (217, 'lee', 1);
INSERT INTO `cms_module_search_index` VALUES (217, 'peace', 1);
INSERT INTO `cms_module_search_index` VALUES (217, 'bếp', 1);
INSERT INTO `cms_module_search_index` VALUES (217, 'phó', 1);
INSERT INTO `cms_module_search_index` VALUES (217, 'staff', 1);
INSERT INTO `cms_module_search_index` VALUES (217, 'jpg', 1);
INSERT INTO `cms_module_search_index` VALUES (217, 'nullam', 1);
INSERT INTO `cms_module_search_index` VALUES (217, 'quis', 1);
INSERT INTO `cms_module_search_index` VALUES (217, 'ante', 1);
INSERT INTO `cms_module_search_index` VALUES (217, 'etiam', 1);
INSERT INTO `cms_module_search_index` VALUES (217, 'sit', 1);
INSERT INTO `cms_module_search_index` VALUES (217, 'amet', 1);
INSERT INTO `cms_module_search_index` VALUES (217, 'orci', 1);
INSERT INTO `cms_module_search_index` VALUES (217, 'eget', 1);
INSERT INTO `cms_module_search_index` VALUES (217, 'eros', 1);
INSERT INTO `cms_module_search_index` VALUES (217, 'faucibus', 1);
INSERT INTO `cms_module_search_index` VALUES (217, 'tincidunt', 1);
INSERT INTO `cms_module_search_index` VALUES (217, 'duis', 1);
INSERT INTO `cms_module_search_index` VALUES (217, 'leo', 1);
INSERT INTO `cms_module_search_index` VALUES (217, 'songviytuong', 1);
INSERT INTO `cms_module_search_index` VALUES (218, 'gordon', 2);
INSERT INTO `cms_module_search_index` VALUES (218, 'ramsay', 3);
INSERT INTO `cms_module_search_index` VALUES (218, 'bếp', 1);
INSERT INTO `cms_module_search_index` VALUES (218, 'trưởng', 1);
INSERT INTO `cms_module_search_index` VALUES (218, 'staff', 1);
INSERT INTO `cms_module_search_index` VALUES (218, 'jpg', 1);
INSERT INTO `cms_module_search_index` VALUES (218, 'xây', 1);
INSERT INTO `cms_module_search_index` VALUES (218, 'dựng', 1);
INSERT INTO `cms_module_search_index` VALUES (218, 'danh', 1);
INSERT INTO `cms_module_search_index` VALUES (218, 'tiếng', 2);
INSERT INTO `cms_module_search_index` VALUES (218, 'từ', 1);
INSERT INTO `cms_module_search_index` VALUES (218, 'tài', 1);
INSERT INTO `cms_module_search_index` VALUES (218, 'năng', 1);
INSERT INTO `cms_module_search_index` VALUES (218, 'nấu', 1);
INSERT INTO `cms_module_search_index` VALUES (218, 'nướng', 1);
INSERT INTO `cms_module_search_index` VALUES (218, 'hoàn', 1);
INSERT INTO `cms_module_search_index` VALUES (218, 'hảo', 1);
INSERT INTO `cms_module_search_index` VALUES (218, 'nhưng', 1);
INSERT INTO `cms_module_search_index` VALUES (218, 'còn', 1);
INSERT INTO `cms_module_search_index` VALUES (218, 'có', 1);
INSERT INTO `cms_module_search_index` VALUES (218, 'cả', 1);
INSERT INTO `cms_module_search_index` VALUES (218, 'xấu', 1);
INSERT INTO `cms_module_search_index` VALUES (218, 'bởi', 1);
INSERT INTO `cms_module_search_index` VALUES (218, 'tính', 1);
INSERT INTO `cms_module_search_index` VALUES (218, 'khí', 1);
INSERT INTO `cms_module_search_index` VALUES (218, 'nóng', 1);
INSERT INTO `cms_module_search_index` VALUES (218, 'nảy', 1);
INSERT INTO `cms_module_search_index` VALUES (218, 'và', 1);
INSERT INTO `cms_module_search_index` VALUES (218, 'hay', 1);
INSERT INTO `cms_module_search_index` VALUES (218, 'chửi', 1);
INSERT INTO `cms_module_search_index` VALUES (218, 'thề', 1);
INSERT INTO `cms_module_search_index` VALUES (406, 'dũng', 1);
INSERT INTO `cms_module_search_index` VALUES (406, 'quang', 1);
INSERT INTO `cms_module_search_index` VALUES (408, 'khi', 1);
INSERT INTO `cms_module_search_index` VALUES (408, 'mỗi', 1);
INSERT INTO `cms_module_search_index` VALUES (408, 'cho', 1);
INSERT INTO `cms_module_search_index` VALUES (408, 'tượng', 1);
INSERT INTO `cms_module_search_index` VALUES (408, 'ấn', 1);
INSERT INTO `cms_module_search_index` VALUES (408, 'được', 1);
INSERT INTO `cms_module_search_index` VALUES (408, 'gây', 1);
INSERT INTO `cms_module_search_index` VALUES (408, 'đã', 1);
INSERT INTO `cms_module_search_index` VALUES (408, 'thích', 1);
INSERT INTO `cms_module_search_index` VALUES (408, 'rất', 1);
INSERT INTO `cms_module_search_index` VALUES (408, 'thấy', 1);
INSERT INTO `cms_module_search_index` VALUES (408, 'cảm', 1);
INSERT INTO `cms_module_search_index` VALUES (408, 'tôi', 2);
INSERT INTO `cms_module_search_index` VALUES (408, 'ngan', 1);
INSERT INTO `cms_module_search_index` VALUES (408, 'khoa', 1);
INSERT INTO `cms_module_search_index` VALUES (408, 'hàng', 1);
INSERT INTO `cms_module_search_index` VALUES (408, 'nhà', 1);
INSERT INTO `cms_module_search_index` VALUES (408, 'tại', 1);
INSERT INTO `cms_module_search_index` VALUES (408, 'ăn', 1);
INSERT INTO `cms_module_search_index` VALUES (408, 'món', 1);
INSERT INTO `cms_module_search_index` VALUES (408, 'những', 1);
INSERT INTO `cms_module_search_index` VALUES (408, 'Ăn', 1);
INSERT INTO `cms_module_search_index` VALUES (408, 'jpg', 1);
INSERT INTO `cms_module_search_index` VALUES (408, 'duong', 1);
INSERT INTO `cms_module_search_index` VALUES (408, 'tung', 1);
INSERT INTO `cms_module_search_index` VALUES (409, 'https', 1);
INSERT INTO `cms_module_search_index` VALUES (409, 'facebook', 3);
INSERT INTO `cms_module_search_index` VALUES (409, 'follow', 1);
INSERT INTO `cms_module_search_index` VALUES (224, 'https', 1);
INSERT INTO `cms_module_search_index` VALUES (224, 'www', 1);
INSERT INTO `cms_module_search_index` VALUES (224, 'com', 1);
INSERT INTO `cms_module_search_index` VALUES (224, 'songviytuong', 1);
INSERT INTO `cms_module_search_index` VALUES (225, 'https', 1);
INSERT INTO `cms_module_search_index` VALUES (225, 'www', 1);
INSERT INTO `cms_module_search_index` VALUES (225, 'com', 1);
INSERT INTO `cms_module_search_index` VALUES (225, 'songviytuong', 1);
INSERT INTO `cms_module_search_index` VALUES (314, 'www', 1);
INSERT INTO `cms_module_search_index` VALUES (314, 'https', 1);
INSERT INTO `cms_module_search_index` VALUES (314, 'youtube', 3);
INSERT INTO `cms_module_search_index` VALUES (314, 'ngan', 1);
INSERT INTO `cms_module_search_index` VALUES (289, 'jpg', 2);
INSERT INTO `cms_module_search_index` VALUES (289, 'sau_thumb', 1);
INSERT INTO `cms_module_search_index` VALUES (289, 'ruou', 2);
INSERT INTO `cms_module_search_index` VALUES (288, 'ruou', 2);
INSERT INTO `cms_module_search_index` VALUES (288, 'men', 3);
INSERT INTO `cms_module_search_index` VALUES (288, 'vodka', 4);
INSERT INTO `cms_module_search_index` VALUES (241, 'heineken', 3);
INSERT INTO `cms_module_search_index` VALUES (241, 'bia', 2);
INSERT INTO `cms_module_search_index` VALUES (242, 'nội', 1);
INSERT INTO `cms_module_search_index` VALUES (242, 'hà', 1);
INSERT INTO `cms_module_search_index` VALUES (242, 'bia', 3);
INSERT INTO `cms_module_search_index` VALUES (243, '20000', 1);
INSERT INTO `cms_module_search_index` VALUES (243, 'sai', 1);
INSERT INTO `cms_module_search_index` VALUES (243, 'gòn', 1);
INSERT INTO `cms_module_search_index` VALUES (243, 'sài', 1);
INSERT INTO `cms_module_search_index` VALUES (243, 'bia', 3);
INSERT INTO `cms_module_search_index` VALUES (244, 'trúc', 1);
INSERT INTO `cms_module_search_index` VALUES (244, 'bia', 3);
INSERT INTO `cms_module_search_index` VALUES (286, 'wine', 1);
INSERT INTO `cms_module_search_index` VALUES (286, 'ngan', 2);
INSERT INTO `cms_module_search_index` VALUES (286, 'khoa', 3);
INSERT INTO `cms_module_search_index` VALUES (286, 'chum', 3);
INSERT INTO `cms_module_search_index` VALUES (286, 'nếp', 1);
INSERT INTO `cms_module_search_index` VALUES (286, 'rượu', 1);
INSERT INTO `cms_module_search_index` VALUES (245, 'rượu', 1);
INSERT INTO `cms_module_search_index` VALUES (246, 'vodka', 2);
INSERT INTO `cms_module_search_index` VALUES (246, 'rượu', 1);
INSERT INTO `cms_module_search_index` VALUES (289, 'crocodile', 1);
INSERT INTO `cms_module_search_index` VALUES (288, 'rượu', 1);
INSERT INTO `cms_module_search_index` VALUES (290, 'south', 1);
INSERT INTO `cms_module_search_index` VALUES (290, 'quốc', 1);
INSERT INTO `cms_module_search_index` VALUES (290, 'hàn', 1);
INSERT INTO `cms_module_search_index` VALUES (290, 'soju', 4);
INSERT INTO `cms_module_search_index` VALUES (290, 'rượu', 1);
INSERT INTO `cms_module_search_index` VALUES (241, '25000', 1);
INSERT INTO `cms_module_search_index` VALUES (241, 'chai', 1);
INSERT INTO `cms_module_search_index` VALUES (242, 'chai', 1);
INSERT INTO `cms_module_search_index` VALUES (244, 'chai', 1);
INSERT INTO `cms_module_search_index` VALUES (245, '140000', 1);
INSERT INTO `cms_module_search_index` VALUES (245, 'chai', 1);
INSERT INTO `cms_module_search_index` VALUES (246, '000', 2);
INSERT INTO `cms_module_search_index` VALUES (246, '120', 1);
INSERT INTO `cms_module_search_index` VALUES (246, 'chai', 2);
INSERT INTO `cms_module_search_index` VALUES (246, '60000', 1);
INSERT INTO `cms_module_search_index` VALUES (291, 'passion', 1);
INSERT INTO `cms_module_search_index` VALUES (291, 'leo', 2);
INSERT INTO `cms_module_search_index` VALUES (291, 'chanh', 3);
INSERT INTO `cms_module_search_index` VALUES (291, 'nước', 1);
INSERT INTO `cms_module_search_index` VALUES (248, 'nước', 1);
INSERT INTO `cms_module_search_index` VALUES (248, 'cam', 1);
INSERT INTO `cms_module_search_index` VALUES (248, 'nguyên', 1);
INSERT INTO `cms_module_search_index` VALUES (248, 'chất', 1);
INSERT INTO `cms_module_search_index` VALUES (248, 'pure', 1);
INSERT INTO `cms_module_search_index` VALUES (248, 'orange', 1);
INSERT INTO `cms_module_search_index` VALUES (248, 'juice', 1);
INSERT INTO `cms_module_search_index` VALUES (248, '50000', 1);
INSERT INTO `cms_module_search_index` VALUES (294, 'red', 1);
INSERT INTO `cms_module_search_index` VALUES (294, 'pumpkin', 1);
INSERT INTO `cms_module_search_index` VALUES (294, 'waky', 1);
INSERT INTO `cms_module_search_index` VALUES (294, 'orange', 1);
INSERT INTO `cms_module_search_index` VALUES (294, 'squeezed', 1);
INSERT INTO `cms_module_search_index` VALUES (294, 'coca', 1);
INSERT INTO `cms_module_search_index` VALUES (294, 'drink', 1);
INSERT INTO `cms_module_search_index` VALUES (294, 'soft', 1);
INSERT INTO `cms_module_search_index` VALUES (294, 'ngọt', 1);
INSERT INTO `cms_module_search_index` VALUES (294, 'nước', 1);
INSERT INTO `cms_module_search_index` VALUES (295, 'nước', 1);
INSERT INTO `cms_module_search_index` VALUES (251, 'jpg', 2);
INSERT INTO `cms_module_search_index` VALUES (251, 'tran', 1);
INSERT INTO `cms_module_search_index` VALUES (251, '15000', 1);
INSERT INTO `cms_module_search_index` VALUES (251, 'đĩa', 1);
INSERT INTO `cms_module_search_index` VALUES (269, '320', 1);
INSERT INTO `cms_module_search_index` VALUES (269, '250', 1);
INSERT INTO `cms_module_search_index` VALUES (269, 'người', 3);
INSERT INTO `cms_module_search_index` VALUES (269, '180', 1);
INSERT INTO `cms_module_search_index` VALUES (269, 'người ', 1);
INSERT INTO `cms_module_search_index` VALUES (269, 'suất', 4);
INSERT INTO `cms_module_search_index` VALUES (269, '000', 4);
INSERT INTO `cms_module_search_index` VALUES (269, '100', 1);
INSERT INTO `cms_module_search_index` VALUES (269, 'siamese', 1);
INSERT INTO `cms_module_search_index` VALUES (269, 'duck', 1);
INSERT INTO `cms_module_search_index` VALUES (269, 'boiled', 1);
INSERT INTO `cms_module_search_index` VALUES (269, 'luộc', 1);
INSERT INTO `cms_module_search_index` VALUES (269, 'ngan', 3);
INSERT INTO `cms_module_search_index` VALUES (259, 'nổi', 4);
INSERT INTO `cms_module_search_index` VALUES (259, 'tiếng', 4);
INSERT INTO `cms_module_search_index` VALUES (263, 'jpg', 2);
INSERT INTO `cms_module_search_index` VALUES (263, '40000', 1);
INSERT INTO `cms_module_search_index` VALUES (263, 'bát', 1);
INSERT INTO `cms_module_search_index` VALUES (269, 'đĩa', 1);
INSERT INTO `cms_module_search_index` VALUES (451, '180k', 1);
INSERT INTO `cms_module_search_index` VALUES (451, 'quay', 2);
INSERT INTO `cms_module_search_index` VALUES (451, 'ngan', 2);
INSERT INTO `cms_module_search_index` VALUES (463, 'nem', 2);
INSERT INTO `cms_module_search_index` VALUES (277, 'nồi', 1);
INSERT INTO `cms_module_search_index` VALUES (435, 'tron_thumb', 1);
INSERT INTO `cms_module_search_index` VALUES (280, 'người', 1);
INSERT INTO `cms_module_search_index` VALUES (280, 'long', 2);
INSERT INTO `cms_module_search_index` VALUES (280, 'xao', 2);
INSERT INTO `cms_module_search_index` VALUES (280, 'dua_thumb', 1);
INSERT INTO `cms_module_search_index` VALUES (280, 'jpg', 2);
INSERT INTO `cms_module_search_index` VALUES (280, 'dua', 1);
INSERT INTO `cms_module_search_index` VALUES (280, '150000', 1);
INSERT INTO `cms_module_search_index` VALUES (280, 'bát', 1);
INSERT INTO `cms_module_search_index` VALUES (281, 'đĩa', 1);
INSERT INTO `cms_module_search_index` VALUES (282, '10000', 1);
INSERT INTO `cms_module_search_index` VALUES (282, 'đĩa', 2);
INSERT INTO `cms_module_search_index` VALUES (282, 'tặng', 1);
INSERT INTO `cms_module_search_index` VALUES (282, 'special', 1);
INSERT INTO `cms_module_search_index` VALUES (283, 'cánh', 1);
INSERT INTO `cms_module_search_index` VALUES (283, 'chiên', 1);
INSERT INTO `cms_module_search_index` VALUES (283, 'sốt', 1);
INSERT INTO `cms_module_search_index` VALUES (283, 'cay', 2);
INSERT INTO `cms_module_search_index` VALUES (283, 'canh', 2);
INSERT INTO `cms_module_search_index` VALUES (283, 'chien', 2);
INSERT INTO `cms_module_search_index` VALUES (283, 'sot', 2);
INSERT INTO `cms_module_search_index` VALUES (283, 'cay_thumb', 1);
INSERT INTO `cms_module_search_index` VALUES (283, 'jpg', 2);
INSERT INTO `cms_module_search_index` VALUES (283, '100000', 1);
INSERT INTO `cms_module_search_index` VALUES (283, 'đĩa', 1);
INSERT INTO `cms_module_search_index` VALUES (456, 'kim', 3);
INSERT INTO `cms_module_search_index` VALUES (457, 'chao', 1);
INSERT INTO `cms_module_search_index` VALUES (457, 'ngan', 1);
INSERT INTO `cms_module_search_index` VALUES (457, 'cháo', 1);
INSERT INTO `cms_module_search_index` VALUES (286, 'ruou', 1);
INSERT INTO `cms_module_search_index` VALUES (286, 'ngan_thumb', 1);
INSERT INTO `cms_module_search_index` VALUES (286, 'jpg', 1);
INSERT INTO `cms_module_search_index` VALUES (286, '100000', 1);
INSERT INTO `cms_module_search_index` VALUES (289, 'sấu', 1);
INSERT INTO `cms_module_search_index` VALUES (289, 'cá', 1);
INSERT INTO `cms_module_search_index` VALUES (289, 'vodka', 4);
INSERT INTO `cms_module_search_index` VALUES (289, 'rượu', 1);
INSERT INTO `cms_module_search_index` VALUES (288, 'men_thumb', 1);
INSERT INTO `cms_module_search_index` VALUES (288, 'jpg', 2);
INSERT INTO `cms_module_search_index` VALUES (288, '130000', 1);
INSERT INTO `cms_module_search_index` VALUES (288, 'chai', 1);
INSERT INTO `cms_module_search_index` VALUES (289, 'sau', 1);
INSERT INTO `cms_module_search_index` VALUES (289, '200000', 1);
INSERT INTO `cms_module_search_index` VALUES (289, 'chai', 1);
INSERT INTO `cms_module_search_index` VALUES (290, 'han', 2);
INSERT INTO `cms_module_search_index` VALUES (290, 'quoc_thumb', 1);
INSERT INTO `cms_module_search_index` VALUES (290, 'jpg', 2);
INSERT INTO `cms_module_search_index` VALUES (290, 'quoc', 1);
INSERT INTO `cms_module_search_index` VALUES (290, '130000', 1);
INSERT INTO `cms_module_search_index` VALUES (290, 'chai', 1);
INSERT INTO `cms_module_search_index` VALUES (291, 'juice', 1);
INSERT INTO `cms_module_search_index` VALUES (291, 'nuoc', 2);
INSERT INTO `cms_module_search_index` VALUES (291, 'leo_thumb', 1);
INSERT INTO `cms_module_search_index` VALUES (291, 'jpg', 2);
INSERT INTO `cms_module_search_index` VALUES (291, '20000', 1);
INSERT INTO `cms_module_search_index` VALUES (292, 'nuoc', 2);
INSERT INTO `cms_module_search_index` VALUES (292, 'tuoi_thumb', 1);
INSERT INTO `cms_module_search_index` VALUES (292, 'jpg', 2);
INSERT INTO `cms_module_search_index` VALUES (292, 'tuoi', 1);
INSERT INTO `cms_module_search_index` VALUES (292, '20000', 1);
INSERT INTO `cms_module_search_index` VALUES (293, 'nuoc', 2);
INSERT INTO `cms_module_search_index` VALUES (293, 'tuoi_thumb', 1);
INSERT INTO `cms_module_search_index` VALUES (293, 'jpg', 2);
INSERT INTO `cms_module_search_index` VALUES (293, 'tuoi', 1);
INSERT INTO `cms_module_search_index` VALUES (293, '40000', 1);
INSERT INTO `cms_module_search_index` VALUES (294, 'bull', 1);
INSERT INTO `cms_module_search_index` VALUES (294, 'nuoc', 1);
INSERT INTO `cms_module_search_index` VALUES (294, 'ngot_thumb', 1);
INSERT INTO `cms_module_search_index` VALUES (294, 'jpg', 1);
INSERT INTO `cms_module_search_index` VALUES (294, '20000', 1);
INSERT INTO `cms_module_search_index` VALUES (294, 'lon', 1);
INSERT INTO `cms_module_search_index` VALUES (295, '20000', 1);
INSERT INTO `cms_module_search_index` VALUES (295, 'chai', 1);
INSERT INTO `cms_module_search_index` VALUES (296, 'rượu', 1);
INSERT INTO `cms_module_search_index` VALUES (296, 'vodka', 3);
INSERT INTO `cms_module_search_index` VALUES (296, 'nhôm', 1);
INSERT INTO `cms_module_search_index` VALUES (296, 'danzka', 1);
INSERT INTO `cms_module_search_index` VALUES (296, 'ruou', 1);
INSERT INTO `cms_module_search_index` VALUES (296, 'nhom_thumb', 1);
INSERT INTO `cms_module_search_index` VALUES (296, 'jpg', 1);
INSERT INTO `cms_module_search_index` VALUES (296, '130000', 1);
INSERT INTO `cms_module_search_index` VALUES (296, 'chai', 1);
INSERT INTO `cms_module_search_index` VALUES (303, 'ngan', 1);
INSERT INTO `cms_module_search_index` VALUES (303, 'khoa', 1);
INSERT INTO `cms_module_search_index` VALUES (303, 'steak', 1);
INSERT INTO `cms_module_search_index` VALUES (303, 'ramsay', 1);
INSERT INTO `cms_module_search_index` VALUES (303, 'gordon', 1);
INSERT INTO `cms_module_search_index` VALUES (397, 'gallery', 1);
INSERT INTO `cms_module_search_index` VALUES (397, 'ngan', 1);
INSERT INTO `cms_module_search_index` VALUES (397, 'khoa', 1);
INSERT INTO `cms_module_search_index` VALUES (397, 'ramsay', 1);
INSERT INTO `cms_module_search_index` VALUES (303, 'jpg', 1);
INSERT INTO `cms_module_search_index` VALUES (391, 'gallery', 1);
INSERT INTO `cms_module_search_index` VALUES (391, 'ngan', 1);
INSERT INTO `cms_module_search_index` VALUES (391, 'khoa', 1);
INSERT INTO `cms_module_search_index` VALUES (391, 'steak', 1);
INSERT INTO `cms_module_search_index` VALUES (391, 'ramsay', 1);
INSERT INTO `cms_module_search_index` VALUES (391, 'gordon', 1);
INSERT INTO `cms_module_search_index` VALUES (314, 'khoa', 1);
INSERT INTO `cms_module_search_index` VALUES (308, 'chuyên', 1);
INSERT INTO `cms_module_search_index` VALUES (308, 'các', 1);
INSERT INTO `cms_module_search_index` VALUES (308, 'món', 1);
INSERT INTO `cms_module_search_index` VALUES (308, 'ăn', 1);
INSERT INTO `cms_module_search_index` VALUES (308, 'về', 1);
INSERT INTO `cms_module_search_index` VALUES (308, 'ngan', 1);
INSERT INTO `cms_module_search_index` VALUES (308, 'đầu', 1);
INSERT INTO `cms_module_search_index` VALUES (308, 'tiên', 1);
INSERT INTO `cms_module_search_index` VALUES (308, 'và', 1);
INSERT INTO `cms_module_search_index` VALUES (308, 'số', 1);
INSERT INTO `cms_module_search_index` VALUES (308, 'ở', 1);
INSERT INTO `cms_module_search_index` VALUES (308, 'hà', 1);
INSERT INTO `cms_module_search_index` VALUES (308, 'nội', 1);
INSERT INTO `cms_module_search_index` VALUES (309, 'bếp', 1);
INSERT INTO `cms_module_search_index` VALUES (309, 'gordon', 1);
INSERT INTO `cms_module_search_index` VALUES (309, 'ramsay', 1);
INSERT INTO `cms_module_search_index` VALUES (309, 'bạn', 1);
INSERT INTO `cms_module_search_index` VALUES (309, 'thân', 1);
INSERT INTO `cms_module_search_index` VALUES (309, 'của', 1);
INSERT INTO `cms_module_search_index` VALUES (309, 'david', 1);
INSERT INTO `cms_module_search_index` VALUES (309, 'beckam', 1);
INSERT INTO `cms_module_search_index` VALUES (309, 'đã', 1);
INSERT INTO `cms_module_search_index` VALUES (309, 'tới', 1);
INSERT INTO `cms_module_search_index` VALUES (309, 'đây', 1);
INSERT INTO `cms_module_search_index` VALUES (310, 'thức', 1);
INSERT INTO `cms_module_search_index` VALUES (310, 'các', 1);
INSERT INTO `cms_module_search_index` VALUES (310, 'món', 1);
INSERT INTO `cms_module_search_index` VALUES (310, 'ăn', 1);
INSERT INTO `cms_module_search_index` VALUES (310, 'ngon', 1);
INSERT INTO `cms_module_search_index` VALUES (310, 'của', 1);
INSERT INTO `cms_module_search_index` VALUES (310, 'những', 1);
INSERT INTO `cms_module_search_index` VALUES (310, 'người', 1);
INSERT INTO `cms_module_search_index` VALUES (310, 'nổi', 1);
INSERT INTO `cms_module_search_index` VALUES (310, 'tiếng', 1);
INSERT INTO `cms_module_search_index` VALUES (311, 'vịt', 1);
INSERT INTO `cms_module_search_index` VALUES (311, 'của', 2);
INSERT INTO `cms_module_search_index` VALUES (311, 'nhà', 2);
INSERT INTO `cms_module_search_index` VALUES (311, 'hàng', 2);
INSERT INTO `cms_module_search_index` VALUES (311, 'khoa', 1);
INSERT INTO `cms_module_search_index` VALUES (311, 'ngan', 1);
INSERT INTO `cms_module_search_index` VALUES (311, 'cũng', 1);
INSERT INTO `cms_module_search_index` VALUES (311, 'có', 1);
INSERT INTO `cms_module_search_index` VALUES (311, 'đầy', 1);
INSERT INTO `cms_module_search_index` VALUES (311, 'đủ', 1);
INSERT INTO `cms_module_search_index` VALUES (311, 'hộ', 1);
INSERT INTO `cms_module_search_index` VALUES (311, 'chiếu', 1);
INSERT INTO `cms_module_search_index` VALUES (311, 'chứng', 1);
INSERT INTO `cms_module_search_index` VALUES (311, 'minh', 1);
INSERT INTO `cms_module_search_index` VALUES (311, 'thư', 1);
INSERT INTO `cms_module_search_index` VALUES (311, 'như', 2);
INSERT INTO `cms_module_search_index` VALUES (311, 'ai”', 1);
INSERT INTO `cms_module_search_index` VALUES (311, '–', 1);
INSERT INTO `cms_module_search_index` VALUES (311, 'lời', 1);
INSERT INTO `cms_module_search_index` VALUES (311, 'bà', 1);
INSERT INTO `cms_module_search_index` VALUES (311, 'chủ', 1);
INSERT INTO `cms_module_search_index` VALUES (311, 'vui', 1);
INSERT INTO `cms_module_search_index` VALUES (311, 'tính', 1);
INSERT INTO `cms_module_search_index` VALUES (311, 'đã', 1);
INSERT INTO `cms_module_search_index` VALUES (311, 'từng', 1);
INSERT INTO `cms_module_search_index` VALUES (311, 'nói', 1);
INSERT INTO `cms_module_search_index` VALUES (313, 'đây', 1);
INSERT INTO `cms_module_search_index` VALUES (313, 'tại', 1);
INSERT INTO `cms_module_search_index` VALUES (313, 'nướng', 1);
INSERT INTO `cms_module_search_index` VALUES (313, 'món', 1);
INSERT INTO `cms_module_search_index` VALUES (313, 'làm', 1);
INSERT INTO `cms_module_search_index` VALUES (313, 'học', 1);
INSERT INTO `cms_module_search_index` VALUES (313, 'và', 1);
INSERT INTO `cms_module_search_index` VALUES (313, 'ngan', 2);
INSERT INTO `cms_module_search_index` VALUES (313, 'lông', 1);
INSERT INTO `cms_module_search_index` VALUES (313, 'vặt', 1);
INSERT INTO `cms_module_search_index` VALUES (313, 'tay', 1);
INSERT INTO `cms_module_search_index` VALUES (313, 'tự', 1);
INSERT INTO `cms_module_search_index` VALUES (313, 'đã', 1);
INSERT INTO `cms_module_search_index` VALUES (313, 'ramsay', 1);
INSERT INTO `cms_module_search_index` VALUES (313, 'gordon', 1);
INSERT INTO `cms_module_search_index` VALUES (313, 'bếp', 1);
INSERT INTO `cms_module_search_index` VALUES (313, 'đầu', 1);
INSERT INTO `cms_module_search_index` VALUES (313, 'siêu', 1);
INSERT INTO `cms_module_search_index` VALUES (314, '=itbjxs_jpj0', 1);
INSERT INTO `cms_module_search_index` VALUES (316, 'chúc', 1);
INSERT INTO `cms_module_search_index` VALUES (316, 'quý', 1);
INSERT INTO `cms_module_search_index` VALUES (316, 'khách', 1);
INSERT INTO `cms_module_search_index` VALUES (316, 'ngon', 1);
INSERT INTO `cms_module_search_index` VALUES (316, 'miệng', 1);
INSERT INTO `cms_module_search_index` VALUES (317, '10000', 1);
INSERT INTO `cms_module_search_index` VALUES (317, 'đĩa', 1);
INSERT INTO `cms_module_search_index` VALUES (462, 'đĩa', 1);
INSERT INTO `cms_module_search_index` VALUES (462, '35000', 1);
INSERT INTO `cms_module_search_index` VALUES (462, 'jpg', 1);
INSERT INTO `cms_module_search_index` VALUES (462, 'chien', 1);
INSERT INTO `cms_module_search_index` VALUES (459, 'suất', 1);
INSERT INTO `cms_module_search_index` VALUES (459, 'jpg', 1);
INSERT INTO `cms_module_search_index` VALUES (459, 'nam', 1);
INSERT INTO `cms_module_search_index` VALUES (459, 'xao', 1);
INSERT INTO `cms_module_search_index` VALUES (459, 'cai', 1);
INSERT INTO `cms_module_search_index` VALUES (326, 'muong', 1);
INSERT INTO `cms_module_search_index` VALUES (326, 'tỏi', 1);
INSERT INTO `cms_module_search_index` VALUES (326, 'xào', 1);
INSERT INTO `cms_module_search_index` VALUES (326, 'muống', 1);
INSERT INTO `cms_module_search_index` VALUES (326, 'rau', 2);
INSERT INTO `cms_module_search_index` VALUES (328, '35000', 1);
INSERT INTO `cms_module_search_index` VALUES (328, 'jpg', 1);
INSERT INTO `cms_module_search_index` VALUES (328, 'salat', 2);
INSERT INTO `cms_module_search_index` VALUES (327, 'toi', 1);
INSERT INTO `cms_module_search_index` VALUES (327, 'xao', 1);
INSERT INTO `cms_module_search_index` VALUES (327, 'tỏi', 1);
INSERT INTO `cms_module_search_index` VALUES (327, 'xào', 1);
INSERT INTO `cms_module_search_index` VALUES (462, 'ngo', 1);
INSERT INTO `cms_module_search_index` VALUES (462, 'chiên', 1);
INSERT INTO `cms_module_search_index` VALUES (462, 'ngô', 1);
INSERT INTO `cms_module_search_index` VALUES (459, 'nấm', 1);
INSERT INTO `cms_module_search_index` VALUES (459, 'xào', 1);
INSERT INTO `cms_module_search_index` VALUES (459, 'cải', 1);
INSERT INTO `cms_module_search_index` VALUES (459, 'rau', 2);
INSERT INTO `cms_module_search_index` VALUES (326, 'xao', 1);
INSERT INTO `cms_module_search_index` VALUES (326, 'toi', 1);
INSERT INTO `cms_module_search_index` VALUES (326, 'jpg', 1);
INSERT INTO `cms_module_search_index` VALUES (326, '35000', 1);
INSERT INTO `cms_module_search_index` VALUES (326, 'đĩa', 1);
INSERT INTO `cms_module_search_index` VALUES (327, 'jpg', 1);
INSERT INTO `cms_module_search_index` VALUES (327, '35000', 1);
INSERT INTO `cms_module_search_index` VALUES (327, 'đĩa', 1);
INSERT INTO `cms_module_search_index` VALUES (328, 'phần', 1);
INSERT INTO `cms_module_search_index` VALUES (334, 'jpg', 1);
INSERT INTO `cms_module_search_index` VALUES (334, 'dinh', 1);
INSERT INTO `cms_module_search_index` VALUES (334, 'hop', 1);
INSERT INTO `cms_module_search_index` VALUES (334, 'đình', 1);
INSERT INTO `cms_module_search_index` VALUES (334, 'gia', 2);
INSERT INTO `cms_module_search_index` VALUES (334, 'họp', 1);
INSERT INTO `cms_module_search_index` VALUES (334, 'sum', 2);
INSERT INTO `cms_module_search_index` VALUES (335, 'jpg', 1);
INSERT INTO `cms_module_search_index` VALUES (335, 'noi', 1);
INSERT INTO `cms_module_search_index` VALUES (335, 'khong', 1);
INSERT INTO `cms_module_search_index` VALUES (335, 'nổi', 1);
INSERT INTO `cms_module_search_index` VALUES (335, 'tin', 2);
INSERT INTO `cms_module_search_index` VALUES (336, 'dang', 1);
INSERT INTO `cms_module_search_index` VALUES (336, 'mai', 1);
INSERT INTO `cms_module_search_index` VALUES (336, 'khuyen', 1);
INSERT INTO `cms_module_search_index` VALUES (336, 'cấp', 1);
INSERT INTO `cms_module_search_index` VALUES (336, 'đẳng', 1);
INSERT INTO `cms_module_search_index` VALUES (336, 'nhưng', 1);
INSERT INTO `cms_module_search_index` VALUES (336, 'mại', 1);
INSERT INTO `cms_module_search_index` VALUES (336, 'khuyến', 1);
INSERT INTO `cms_module_search_index` VALUES (335, 'thể', 1);
INSERT INTO `cms_module_search_index` VALUES (335, 'không', 1);
INSERT INTO `cms_module_search_index` VALUES (334, 'khoa', 1);
INSERT INTO `cms_module_search_index` VALUES (334, 'ngan', 1);
INSERT INTO `cms_module_search_index` VALUES (335, 'khoa', 1);
INSERT INTO `cms_module_search_index` VALUES (335, 'ngan', 1);
INSERT INTO `cms_module_search_index` VALUES (336, 'cap', 1);
INSERT INTO `cms_module_search_index` VALUES (336, 'jpg', 1);
INSERT INTO `cms_module_search_index` VALUES (336, 'khoa', 1);
INSERT INTO `cms_module_search_index` VALUES (336, 'ngan', 1);
INSERT INTO `cms_module_search_index` VALUES (347, 'vip', 4);
INSERT INTO `cms_module_search_index` VALUES (421, 'action', 4);
INSERT INTO `cms_module_search_index` VALUES (421, 'event', 4);
INSERT INTO `cms_module_search_index` VALUES (347, 'member', 4);
INSERT INTO `cms_module_search_index` VALUES (361, '0989466466', 1);
INSERT INTO `cms_module_search_index` VALUES (361, 'hà', 1);
INSERT INTO `cms_module_search_index` VALUES (361, 'nội', 1);
INSERT INTO `cms_module_search_index` VALUES (361, '1987', 1);
INSERT INTO `cms_module_search_index` VALUES (361, 'bình', 1);
INSERT INTO `cms_module_search_index` VALUES (399, 'tth', 1);
INSERT INTO `cms_module_search_index` VALUES (399, 'gallery', 1);
INSERT INTO `cms_module_search_index` VALUES (399, 'ngan', 1);
INSERT INTO `cms_module_search_index` VALUES (399, 'khoa', 1);
INSERT INTO `cms_module_search_index` VALUES (399, 'hà', 1);
INSERT INTO `cms_module_search_index` VALUES (399, 'thanh', 1);
INSERT INTO `cms_module_search_index` VALUES (399, 'tăng', 1);
INSERT INTO `cms_module_search_index` VALUES (363, 'khách', 1);
INSERT INTO `cms_module_search_index` VALUES (363, 'hàng', 1);
INSERT INTO `cms_module_search_index` VALUES (363, 'khoa', 1);
INSERT INTO `cms_module_search_index` VALUES (363, 'ngan', 1);
INSERT INTO `cms_module_search_index` VALUES (363, 'kh01', 1);
INSERT INTO `cms_module_search_index` VALUES (363, 'jpg', 1);
INSERT INTO `cms_module_search_index` VALUES (364, 'khách', 1);
INSERT INTO `cms_module_search_index` VALUES (364, 'hàng', 1);
INSERT INTO `cms_module_search_index` VALUES (364, 'khoa', 1);
INSERT INTO `cms_module_search_index` VALUES (364, 'ngan', 1);
INSERT INTO `cms_module_search_index` VALUES (364, 'kh02', 1);
INSERT INTO `cms_module_search_index` VALUES (364, 'jpg', 1);
INSERT INTO `cms_module_search_index` VALUES (365, 'khách', 1);
INSERT INTO `cms_module_search_index` VALUES (365, 'hàng', 1);
INSERT INTO `cms_module_search_index` VALUES (365, 'khoa', 1);
INSERT INTO `cms_module_search_index` VALUES (365, 'ngan', 1);
INSERT INTO `cms_module_search_index` VALUES (365, 'kh03', 1);
INSERT INTO `cms_module_search_index` VALUES (365, 'jpg', 1);
INSERT INTO `cms_module_search_index` VALUES (366, 'khách', 1);
INSERT INTO `cms_module_search_index` VALUES (366, 'hàng', 1);
INSERT INTO `cms_module_search_index` VALUES (366, 'khoa', 1);
INSERT INTO `cms_module_search_index` VALUES (366, 'ngan', 1);
INSERT INTO `cms_module_search_index` VALUES (366, 'kh04', 1);
INSERT INTO `cms_module_search_index` VALUES (366, 'jpg', 1);
INSERT INTO `cms_module_search_index` VALUES (367, 'khách', 1);
INSERT INTO `cms_module_search_index` VALUES (367, 'hàng', 1);
INSERT INTO `cms_module_search_index` VALUES (367, 'khoa', 1);
INSERT INTO `cms_module_search_index` VALUES (367, 'ngan', 1);
INSERT INTO `cms_module_search_index` VALUES (367, 'kh05', 1);
INSERT INTO `cms_module_search_index` VALUES (367, 'jpg', 1);
INSERT INTO `cms_module_search_index` VALUES (368, 'khách', 1);
INSERT INTO `cms_module_search_index` VALUES (368, 'hàng', 1);
INSERT INTO `cms_module_search_index` VALUES (368, 'khoa', 1);
INSERT INTO `cms_module_search_index` VALUES (368, 'ngan', 1);
INSERT INTO `cms_module_search_index` VALUES (368, 'kh06', 1);
INSERT INTO `cms_module_search_index` VALUES (368, 'jpg', 1);
INSERT INTO `cms_module_search_index` VALUES (369, 'khách', 1);
INSERT INTO `cms_module_search_index` VALUES (369, 'hàng', 1);
INSERT INTO `cms_module_search_index` VALUES (369, 'khoa', 1);
INSERT INTO `cms_module_search_index` VALUES (369, 'ngan', 1);
INSERT INTO `cms_module_search_index` VALUES (369, 'kh07', 1);
INSERT INTO `cms_module_search_index` VALUES (369, 'jpg', 1);
INSERT INTO `cms_module_search_index` VALUES (370, 'khách', 1);
INSERT INTO `cms_module_search_index` VALUES (370, 'hàng', 1);
INSERT INTO `cms_module_search_index` VALUES (370, 'khoa', 1);
INSERT INTO `cms_module_search_index` VALUES (370, 'ngan', 1);
INSERT INTO `cms_module_search_index` VALUES (370, 'kh08', 1);
INSERT INTO `cms_module_search_index` VALUES (370, 'jpg', 1);
INSERT INTO `cms_module_search_index` VALUES (371, 'khách', 1);
INSERT INTO `cms_module_search_index` VALUES (371, 'hàng', 1);
INSERT INTO `cms_module_search_index` VALUES (371, 'khoa', 1);
INSERT INTO `cms_module_search_index` VALUES (371, 'ngan', 1);
INSERT INTO `cms_module_search_index` VALUES (371, 'kh09', 1);
INSERT INTO `cms_module_search_index` VALUES (371, 'jpg', 1);
INSERT INTO `cms_module_search_index` VALUES (372, 'khách', 1);
INSERT INTO `cms_module_search_index` VALUES (372, 'hàng', 1);
INSERT INTO `cms_module_search_index` VALUES (372, 'khoa', 1);
INSERT INTO `cms_module_search_index` VALUES (372, 'ngan', 1);
INSERT INTO `cms_module_search_index` VALUES (372, 'kh10', 1);
INSERT INTO `cms_module_search_index` VALUES (372, 'jpg', 1);
INSERT INTO `cms_module_search_index` VALUES (373, 'khách', 1);
INSERT INTO `cms_module_search_index` VALUES (373, 'hàng', 1);
INSERT INTO `cms_module_search_index` VALUES (373, 'khoa', 1);
INSERT INTO `cms_module_search_index` VALUES (373, 'ngan', 1);
INSERT INTO `cms_module_search_index` VALUES (373, 'kh11', 1);
INSERT INTO `cms_module_search_index` VALUES (373, 'jpg', 1);
INSERT INTO `cms_module_search_index` VALUES (374, 'khách', 1);
INSERT INTO `cms_module_search_index` VALUES (374, 'hàng', 1);
INSERT INTO `cms_module_search_index` VALUES (374, 'khoa', 1);
INSERT INTO `cms_module_search_index` VALUES (374, 'ngan', 1);
INSERT INTO `cms_module_search_index` VALUES (374, 'kh12', 1);
INSERT INTO `cms_module_search_index` VALUES (374, 'jpg', 1);
INSERT INTO `cms_module_search_index` VALUES (375, 'tặng', 1);
INSERT INTO `cms_module_search_index` VALUES (375, 'set', 1);
INSERT INTO `cms_module_search_index` VALUES (375, 'ăn', 1);
INSERT INTO `cms_module_search_index` VALUES (375, 'triệu', 1);
INSERT INTO `cms_module_search_index` VALUES (375, 'fadeinleft', 1);
INSERT INTO `cms_module_search_index` VALUES (375, 'set1tr', 1);
INSERT INTO `cms_module_search_index` VALUES (375, 'jpg', 1);
INSERT INTO `cms_module_search_index` VALUES (375, 'khoa', 1);
INSERT INTO `cms_module_search_index` VALUES (375, 'ngan', 1);
INSERT INTO `cms_module_search_index` VALUES (376, 'jpg', 1);
INSERT INTO `cms_module_search_index` VALUES (377, 'jpg', 1);
INSERT INTO `cms_module_search_index` VALUES (378, 'jpg', 1);
INSERT INTO `cms_module_search_index` VALUES (380, 'hình', 1);
INSERT INTO `cms_module_search_index` VALUES (381, 'jpg', 1);
INSERT INTO `cms_module_search_index` VALUES (382, 'jpg', 1);
INSERT INTO `cms_module_search_index` VALUES (383, 'jpg', 1);
INSERT INTO `cms_module_search_index` VALUES (384, 'jpg', 1);
INSERT INTO `cms_module_search_index` VALUES (385, 'jpg', 1);
INSERT INTO `cms_module_search_index` VALUES (386, 'jpg', 1);
INSERT INTO `cms_module_search_index` VALUES (387, 'jpg', 1);
INSERT INTO `cms_module_search_index` VALUES (388, 'jpg', 1);
INSERT INTO `cms_module_search_index` VALUES (389, 'jpg', 1);
INSERT INTO `cms_module_search_index` VALUES (390, 'jpg', 1);
INSERT INTO `cms_module_search_index` VALUES (391, 'jpg', 1);
INSERT INTO `cms_module_search_index` VALUES (392, 'jpg', 1);
INSERT INTO `cms_module_search_index` VALUES (393, 'jpg', 1);
INSERT INTO `cms_module_search_index` VALUES (395, 'jpg', 1);
INSERT INTO `cms_module_search_index` VALUES (396, 'jpg', 1);
INSERT INTO `cms_module_search_index` VALUES (397, 'jpg', 1);
INSERT INTO `cms_module_search_index` VALUES (399, 'jpg', 1);
INSERT INTO `cms_module_search_index` VALUES (400, 'mừng', 1);
INSERT INTO `cms_module_search_index` VALUES (400, 'quốc', 1);
INSERT INTO `cms_module_search_index` VALUES (400, 'khánh', 1);
INSERT INTO `cms_module_search_index` VALUES (400, 'fadeinright', 1);
INSERT INTO `cms_module_search_index` VALUES (400, 'jpg', 1);
INSERT INTO `cms_module_search_index` VALUES (400, 'khoa', 1);
INSERT INTO `cms_module_search_index` VALUES (400, 'ngan', 1);
INSERT INTO `cms_module_search_index` VALUES (401, 'vui', 1);
INSERT INTO `cms_module_search_index` VALUES (401, 'tết', 1);
INSERT INTO `cms_module_search_index` VALUES (401, 'Độc', 1);
INSERT INTO `cms_module_search_index` VALUES (401, 'lập', 1);
INSERT INTO `cms_module_search_index` VALUES (401, 'tấp', 1);
INSERT INTO `cms_module_search_index` VALUES (401, 'nập', 1);
INSERT INTO `cms_module_search_index` VALUES (401, 'nhận', 1);
INSERT INTO `cms_module_search_index` VALUES (401, 'quà', 1);
INSERT INTO `cms_module_search_index` VALUES (401, 'fadeinleft', 1);
INSERT INTO `cms_module_search_index` VALUES (401, 'vuitet', 1);
INSERT INTO `cms_module_search_index` VALUES (401, 'khoangan', 1);
INSERT INTO `cms_module_search_index` VALUES (401, 'jpg', 1);
INSERT INTO `cms_module_search_index` VALUES (401, 'khoa', 1);
INSERT INTO `cms_module_search_index` VALUES (401, 'ngan', 1);
INSERT INTO `cms_module_search_index` VALUES (402, 'khách', 1);
INSERT INTO `cms_module_search_index` VALUES (402, 'hàng', 1);
INSERT INTO `cms_module_search_index` VALUES (402, 'khoa', 1);
INSERT INTO `cms_module_search_index` VALUES (402, 'ngan', 1);
INSERT INTO `cms_module_search_index` VALUES (402, 'kh13', 1);
INSERT INTO `cms_module_search_index` VALUES (402, 'jpg', 1);
INSERT INTO `cms_module_search_index` VALUES (403, 'khách', 1);
INSERT INTO `cms_module_search_index` VALUES (403, 'hàng', 1);
INSERT INTO `cms_module_search_index` VALUES (403, 'khoa', 1);
INSERT INTO `cms_module_search_index` VALUES (403, 'ngan', 1);
INSERT INTO `cms_module_search_index` VALUES (403, 'kh14', 1);
INSERT INTO `cms_module_search_index` VALUES (403, 'jpg', 1);
INSERT INTO `cms_module_search_index` VALUES (404, 'lee', 1);
INSERT INTO `cms_module_search_index` VALUES (404, 'peace', 1);
INSERT INTO `cms_module_search_index` VALUES (404, 'hà', 2);
INSERT INTO `cms_module_search_index` VALUES (404, 'nội', 2);
INSERT INTO `cms_module_search_index` VALUES (404, 'tôi', 2);
INSERT INTO `cms_module_search_index` VALUES (404, 'vọng', 1);
INSERT INTO `cms_module_search_index` VALUES (404, 'một', 1);
INSERT INTO `cms_module_search_index` VALUES (404, 'ngày', 1);
INSERT INTO `cms_module_search_index` VALUES (404, 'sớm', 1);
INSERT INTO `cms_module_search_index` VALUES (404, 'về', 1);
INSERT INTO `cms_module_search_index` VALUES (404, 'để', 1);
INSERT INTO `cms_module_search_index` VALUES (404, 'thưởng', 1);
INSERT INTO `cms_module_search_index` VALUES (404, 'thức', 1);
INSERT INTO `cms_module_search_index` VALUES (404, 'món', 2);
INSERT INTO `cms_module_search_index` VALUES (404, 'ngan', 2);
INSERT INTO `cms_module_search_index` VALUES (404, 'của', 1);
INSERT INTO `cms_module_search_index` VALUES (404, 'nhà', 1);
INSERT INTO `cms_module_search_index` VALUES (404, 'hàng', 1);
INSERT INTO `cms_module_search_index` VALUES (404, 'khoa', 1);
INSERT INTO `cms_module_search_index` VALUES (404, 'rất', 1);
INSERT INTO `cms_module_search_index` VALUES (404, 'ấn', 1);
INSERT INTO `cms_module_search_index` VALUES (404, 'tượng', 1);
INSERT INTO `cms_module_search_index` VALUES (404, 'bởi', 1);
INSERT INTO `cms_module_search_index` VALUES (404, 'những', 1);
INSERT INTO `cms_module_search_index` VALUES (404, 'ăn', 1);
INSERT INTO `cms_module_search_index` VALUES (404, 'độc', 1);
INSERT INTO `cms_module_search_index` VALUES (404, 'đáo', 1);
INSERT INTO `cms_module_search_index` VALUES (404, 'và', 1);
INSERT INTO `cms_module_search_index` VALUES (404, 'mới', 1);
INSERT INTO `cms_module_search_index` VALUES (404, 'lạ', 1);
INSERT INTO `cms_module_search_index` VALUES (404, 'hương', 1);
INSERT INTO `cms_module_search_index` VALUES (404, 'vị', 1);
INSERT INTO `cms_module_search_index` VALUES (404, 'thật', 1);
INSERT INTO `cms_module_search_index` VALUES (404, 'hấp', 1);
INSERT INTO `cms_module_search_index` VALUES (404, 'dẫn', 1);
INSERT INTO `cms_module_search_index` VALUES (404, 'không', 1);
INSERT INTO `cms_module_search_index` VALUES (404, 'thể', 1);
INSERT INTO `cms_module_search_index` VALUES (404, 'nào', 1);
INSERT INTO `cms_module_search_index` VALUES (404, 'quên', 1);
INSERT INTO `cms_module_search_index` VALUES (404, '100003269729366', 1);
INSERT INTO `cms_module_search_index` VALUES (405, '100004994503021', 1);
INSERT INTO `cms_module_search_index` VALUES (406, 'ngan', 1);
INSERT INTO `cms_module_search_index` VALUES (406, '100000923516754', 1);
INSERT INTO `cms_module_search_index` VALUES (408, 'nội', 2);
INSERT INTO `cms_module_search_index` VALUES (408, 'hà', 2);
INSERT INTO `cms_module_search_index` VALUES (408, 'dương', 1);
INSERT INTO `cms_module_search_index` VALUES (408, 'tùng', 1);
INSERT INTO `cms_module_search_index` VALUES (408, 'trở', 1);
INSERT INTO `cms_module_search_index` VALUES (408, 'lại', 1);
INSERT INTO `cms_module_search_index` VALUES (408, '290607124361041', 1);
INSERT INTO `cms_module_search_index` VALUES (409, 'khoanganxuanay', 1);
INSERT INTO `cms_module_search_index` VALUES (420, 'check', 4);
INSERT INTO `cms_module_search_index` VALUES (417, '17200748_830282993776260_539872617_o', 1);
INSERT INTO `cms_module_search_index` VALUES (417, 'fadeinleft', 1);
INSERT INTO `cms_module_search_index` VALUES (417, 'Ông', 1);
INSERT INTO `cms_module_search_index` VALUES (417, 'Đàn', 1);
INSERT INTO `cms_module_search_index` VALUES (417, 'vinh', 1);
INSERT INTO `cms_module_search_index` VALUES (417, 'tôn', 1);
INSERT INTO `cms_module_search_index` VALUES (416, 'tôn', 1);
INSERT INTO `cms_module_search_index` VALUES (416, 'vinh', 1);
INSERT INTO `cms_module_search_index` VALUES (416, 'phụ', 1);
INSERT INTO `cms_module_search_index` VALUES (416, 'nữ', 1);
INSERT INTO `cms_module_search_index` VALUES (416, 'fadeinright', 1);
INSERT INTO `cms_module_search_index` VALUES (416, '17199203_830283097109583_1212651654_n', 1);
INSERT INTO `cms_module_search_index` VALUES (416, 'jpg', 1);
INSERT INTO `cms_module_search_index` VALUES (416, 'khoa', 1);
INSERT INTO `cms_module_search_index` VALUES (416, 'ngan', 1);
INSERT INTO `cms_module_search_index` VALUES (416, 'https', 1);
INSERT INTO `cms_module_search_index` VALUES (416, 'www', 1);
INSERT INTO `cms_module_search_index` VALUES (416, 'facebook', 1);
INSERT INTO `cms_module_search_index` VALUES (416, 'com', 1);
INSERT INTO `cms_module_search_index` VALUES (416, 'khoanganxuanay', 1);
INSERT INTO `cms_module_search_index` VALUES (416, 'posts', 1);
INSERT INTO `cms_module_search_index` VALUES (416, '1237567793026045', 1);
INSERT INTO `cms_module_search_index` VALUES (417, 'jpg', 1);
INSERT INTO `cms_module_search_index` VALUES (417, 'khoa', 1);
INSERT INTO `cms_module_search_index` VALUES (417, 'ngan', 1);
INSERT INTO `cms_module_search_index` VALUES (417, 'https', 1);
INSERT INTO `cms_module_search_index` VALUES (417, 'www', 1);
INSERT INTO `cms_module_search_index` VALUES (417, 'facebook', 1);
INSERT INTO `cms_module_search_index` VALUES (417, 'com', 1);
INSERT INTO `cms_module_search_index` VALUES (417, 'khoanganxuanay', 1);
INSERT INTO `cms_module_search_index` VALUES (417, 'posts', 1);
INSERT INTO `cms_module_search_index` VALUES (417, '1237554159694075', 1);
INSERT INTO `cms_module_search_index` VALUES (422, 'Đặt', 4);
INSERT INTO `cms_module_search_index` VALUES (422, 'bàn', 4);
INSERT INTO `cms_module_search_index` VALUES (426, 'action', 4);
INSERT INTO `cms_module_search_index` VALUES (426, 'reservations', 4);
INSERT INTO `cms_module_search_index` VALUES (427, 'hot', 1);
INSERT INTO `cms_module_search_index` VALUES (430, 'jpg', 1);
INSERT INTO `cms_module_search_index` VALUES (430, '2ng', 1);
INSERT INTO `cms_module_search_index` VALUES (430, 'fadeinleft', 1);
INSERT INTO `cms_module_search_index` VALUES (430, 'người', 1);
INSERT INTO `cms_module_search_index` VALUES (430, 'cho', 1);
INSERT INTO `cms_module_search_index` VALUES (430, 'dành', 1);
INSERT INTO `cms_module_search_index` VALUES (430, 'combo', 2);
INSERT INTO `cms_module_search_index` VALUES (430, 'khoa', 1);
INSERT INTO `cms_module_search_index` VALUES (430, 'ngan', 1);
INSERT INTO `cms_module_search_index` VALUES (431, 'combo', 2);
INSERT INTO `cms_module_search_index` VALUES (431, 'dành', 1);
INSERT INTO `cms_module_search_index` VALUES (431, 'cho', 1);
INSERT INTO `cms_module_search_index` VALUES (431, 'người', 1);
INSERT INTO `cms_module_search_index` VALUES (431, 'fadeinright', 1);
INSERT INTO `cms_module_search_index` VALUES (431, '3ng', 1);
INSERT INTO `cms_module_search_index` VALUES (431, 'jpg', 1);
INSERT INTO `cms_module_search_index` VALUES (431, 'khoa', 1);
INSERT INTO `cms_module_search_index` VALUES (431, 'ngan', 1);
INSERT INTO `cms_module_search_index` VALUES (432, 'combo', 2);
INSERT INTO `cms_module_search_index` VALUES (432, 'dành', 1);
INSERT INTO `cms_module_search_index` VALUES (432, 'cho', 1);
INSERT INTO `cms_module_search_index` VALUES (432, 'người', 1);
INSERT INTO `cms_module_search_index` VALUES (432, 'fadeinleft', 1);
INSERT INTO `cms_module_search_index` VALUES (432, '6ng', 1);
INSERT INTO `cms_module_search_index` VALUES (432, 'jpg', 1);
INSERT INTO `cms_module_search_index` VALUES (432, 'khoa', 1);
INSERT INTO `cms_module_search_index` VALUES (432, 'ngan', 1);
INSERT INTO `cms_module_search_index` VALUES (435, 'mien', 2);
INSERT INTO `cms_module_search_index` VALUES (435, 'trộn', 1);
INSERT INTO `cms_module_search_index` VALUES (435, 'miến', 1);
INSERT INTO `cms_module_search_index` VALUES (435, '60000', 1);
INSERT INTO `cms_module_search_index` VALUES (435, 'bát', 1);
INSERT INTO `cms_module_search_index` VALUES (451, 'jpg', 1);
INSERT INTO `cms_module_search_index` VALUES (451, '180000', 1);
INSERT INTO `cms_module_search_index` VALUES (451, 'đĩa', 1);
INSERT INTO `cms_module_search_index` VALUES (456, 'suất', 1);
INSERT INTO `cms_module_search_index` VALUES (457, 'bát', 1);
INSERT INTO `cms_module_search_index` VALUES (459, 'hot', 1);

-- ----------------------------
-- Table structure for cms_module_search_items
-- ----------------------------
DROP TABLE IF EXISTS `cms_module_search_items`;
CREATE TABLE `cms_module_search_items`  (
  `id` int(11) NOT NULL,
  `module_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `content_id` int(11) NULL DEFAULT NULL,
  `extra_attr` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `expires` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `module_name`(`module_name`) USING BTREE,
  INDEX `content_id`(`content_id`) USING BTREE,
  INDEX `extra_attr`(`extra_attr`) USING BTREE,
  INDEX `cms_index_search_items`(`module_name`, `content_id`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of cms_module_search_items
-- ----------------------------
INSERT INTO `cms_module_search_items` VALUES (1, 'Search', 15, 'content', NULL);
INSERT INTO `cms_module_search_items` VALUES (34, 'News', 1, 'article', NULL);
INSERT INTO `cms_module_search_items` VALUES (45, 'Search', 62, 'content', NULL);
INSERT INTO `cms_module_search_items` VALUES (58, 'Search', 61, 'content', NULL);
INSERT INTO `cms_module_search_items` VALUES (410, 'Search', 60, 'content', NULL);
INSERT INTO `cms_module_search_items` VALUES (259, 'Search', 69, 'content', NULL);
INSERT INTO `cms_module_search_items` VALUES (47, 'Search', 64, 'content', NULL);
INSERT INTO `cms_module_search_items` VALUES (260, 'Search', 65, 'content', NULL);
INSERT INTO `cms_module_search_items` VALUES (308, 'ListIt2HeaderTexts', 1, 'title', NULL);
INSERT INTO `cms_module_search_items` VALUES (309, 'ListIt2HeaderTexts', 2, 'title', NULL);
INSERT INTO `cms_module_search_items` VALUES (310, 'ListIt2HeaderTexts', 3, 'title', NULL);
INSERT INTO `cms_module_search_items` VALUES (311, 'ListIt2HeaderTexts', 4, 'title', NULL);
INSERT INTO `cms_module_search_items` VALUES (212, 'ListIt2AboutUs', 1, 'title', NULL);
INSERT INTO `cms_module_search_items` VALUES (405, 'ListIt2Testimonials', 1, 'title', NULL);
INSERT INTO `cms_module_search_items` VALUES (406, 'ListIt2Testimonials', 2, 'title', NULL);
INSERT INTO `cms_module_search_items` VALUES (409, 'ListIt2Social', 1, 'title', NULL);
INSERT INTO `cms_module_search_items` VALUES (224, 'ListIt2Social', 2, 'title', NULL);
INSERT INTO `cms_module_search_items` VALUES (225, 'ListIt2Social', 3, 'title', NULL);
INSERT INTO `cms_module_search_items` VALUES (314, 'ListIt2Social', 4, 'title', NULL);
INSERT INTO `cms_module_search_items` VALUES (75, 'ListIt2Menu', 1, 'title', NULL);
INSERT INTO `cms_module_search_items` VALUES (78, 'ListIt2Menu', 2, 'title', NULL);
INSERT INTO `cms_module_search_items` VALUES (80, 'Search', 67, 'content', NULL);
INSERT INTO `cms_module_search_items` VALUES (83, 'Search', 68, 'content', NULL);
INSERT INTO `cms_module_search_items` VALUES (84, 'ListIt2Staff', 1, 'title', NULL);
INSERT INTO `cms_module_search_items` VALUES (85, 'ListIt2Slider', 1, 'title', NULL);
INSERT INTO `cms_module_search_items` VALUES (86, 'ListIt2Slider', 2, 'title', NULL);
INSERT INTO `cms_module_search_items` VALUES (87, 'ListIt2Slider', 3, 'title', NULL);
INSERT INTO `cms_module_search_items` VALUES (376, 'ListIt2Gallery', 1, 'title', NULL);
INSERT INTO `cms_module_search_items` VALUES (377, 'ListIt2Gallery', 2, 'title', NULL);
INSERT INTO `cms_module_search_items` VALUES (380, 'ListIt2Gallery', 3, 'title', NULL);
INSERT INTO `cms_module_search_items` VALUES (382, 'ListIt2Gallery', 4, 'title', NULL);
INSERT INTO `cms_module_search_items` VALUES (381, 'ListIt2Gallery', 5, 'title', NULL);
INSERT INTO `cms_module_search_items` VALUES (378, 'ListIt2Gallery', 6, 'title', NULL);
INSERT INTO `cms_module_search_items` VALUES (383, 'ListIt2Gallery', 7, 'title', NULL);
INSERT INTO `cms_module_search_items` VALUES (384, 'ListIt2Gallery', 8, 'title', NULL);
INSERT INTO `cms_module_search_items` VALUES (385, 'ListIt2Gallery', 9, 'title', NULL);
INSERT INTO `cms_module_search_items` VALUES (386, 'ListIt2Gallery', 10, 'title', NULL);
INSERT INTO `cms_module_search_items` VALUES (298, 'ListIt2Blog', 1, 'title', NULL);
INSERT INTO `cms_module_search_items` VALUES (300, 'ListIt2Blog', 2, 'title', NULL);
INSERT INTO `cms_module_search_items` VALUES (106, 'ListIt2Blog', 3, 'title', NULL);
INSERT INTO `cms_module_search_items` VALUES (107, 'ListIt2Blog', 4, 'title', NULL);
INSERT INTO `cms_module_search_items` VALUES (387, 'ListIt2Gallery', 11, 'title', NULL);
INSERT INTO `cms_module_search_items` VALUES (388, 'ListIt2Gallery', 12, 'title', NULL);
INSERT INTO `cms_module_search_items` VALUES (389, 'ListIt2Gallery', 13, 'title', NULL);
INSERT INTO `cms_module_search_items` VALUES (390, 'ListIt2Gallery', 14, 'title', NULL);
INSERT INTO `cms_module_search_items` VALUES (392, 'ListIt2Gallery', 15, 'title', NULL);
INSERT INTO `cms_module_search_items` VALUES (393, 'ListIt2Gallery', 16, 'title', NULL);
INSERT INTO `cms_module_search_items` VALUES (394, 'ListIt2Gallery', 17, 'title', NULL);
INSERT INTO `cms_module_search_items` VALUES (395, 'ListIt2Gallery', 18, 'title', NULL);
INSERT INTO `cms_module_search_items` VALUES (396, 'ListIt2Gallery', 19, 'title', NULL);
INSERT INTO `cms_module_search_items` VALUES (398, 'ListIt2Gallery', 20, 'title', NULL);
INSERT INTO `cms_module_search_items` VALUES (463, 'ListIt2Menu', 3, 'title', NULL);
INSERT INTO `cms_module_search_items` VALUES (469, 'ListIt2Menu', 4, 'title', NULL);
INSERT INTO `cms_module_search_items` VALUES (470, 'ListIt2Menu', 5, 'title', NULL);
INSERT INTO `cms_module_search_items` VALUES (471, 'ListIt2Menu', 6, 'title', NULL);
INSERT INTO `cms_module_search_items` VALUES (472, 'ListIt2Menu', 7, 'title', NULL);
INSERT INTO `cms_module_search_items` VALUES (456, 'ListIt2Menu', 8, 'title', NULL);
INSERT INTO `cms_module_search_items` VALUES (457, 'ListIt2Menu', 9, 'title', NULL);
INSERT INTO `cms_module_search_items` VALUES (130, 'ListIt2Menu', 10, 'title', NULL);
INSERT INTO `cms_module_search_items` VALUES (473, 'ListIt2Menu', 11, 'title', NULL);
INSERT INTO `cms_module_search_items` VALUES (132, 'ListIt2Menu', 12, 'title', NULL);
INSERT INTO `cms_module_search_items` VALUES (133, 'ListIt2Menu', 13, 'title', NULL);
INSERT INTO `cms_module_search_items` VALUES (262, 'ListIt2Menu', 14, 'title', NULL);
INSERT INTO `cms_module_search_items` VALUES (269, 'ListIt2Menu', 15, 'title', NULL);
INSERT INTO `cms_module_search_items` VALUES (136, 'ListIt2Menu', 16, 'title', NULL);
INSERT INTO `cms_module_search_items` VALUES (468, 'ListIt2Menu', 17, 'title', NULL);
INSERT INTO `cms_module_search_items` VALUES (475, 'ListIt2Menu', 18, 'title', NULL);
INSERT INTO `cms_module_search_items` VALUES (476, 'ListIt2Menu', 19, 'title', NULL);
INSERT INTO `cms_module_search_items` VALUES (280, 'ListIt2Menu', 20, 'title', NULL);
INSERT INTO `cms_module_search_items` VALUES (464, 'ListIt2Menu', 21, 'title', NULL);
INSERT INTO `cms_module_search_items` VALUES (251, 'ListIt2Menu', 22, 'title', NULL);
INSERT INTO `cms_module_search_items` VALUES (281, 'ListIt2Menu', 23, 'title', NULL);
INSERT INTO `cms_module_search_items` VALUES (144, 'ListIt2Menu', 24, 'title', NULL);
INSERT INTO `cms_module_search_items` VALUES (263, 'ListIt2Menu', 25, 'title', NULL);
INSERT INTO `cms_module_search_items` VALUES (435, 'ListIt2Menu', 26, 'title', NULL);
INSERT INTO `cms_module_search_items` VALUES (241, 'ListIt2Menu', 27, 'title', NULL);
INSERT INTO `cms_module_search_items` VALUES (242, 'ListIt2Menu', 28, 'title', NULL);
INSERT INTO `cms_module_search_items` VALUES (243, 'ListIt2Menu', 29, 'title', NULL);
INSERT INTO `cms_module_search_items` VALUES (244, 'ListIt2Menu', 30, 'title', NULL);
INSERT INTO `cms_module_search_items` VALUES (245, 'ListIt2Menu', 31, 'title', NULL);
INSERT INTO `cms_module_search_items` VALUES (246, 'ListIt2Menu', 32, 'title', NULL);
INSERT INTO `cms_module_search_items` VALUES (154, 'ListIt2Menu', 33, 'title', NULL);
INSERT INTO `cms_module_search_items` VALUES (290, 'ListIt2Menu', 34, 'title', NULL);
INSERT INTO `cms_module_search_items` VALUES (292, 'ListIt2Menu', 35, 'title', NULL);
INSERT INTO `cms_module_search_items` VALUES (293, 'ListIt2Menu', 36, 'title', NULL);
INSERT INTO `cms_module_search_items` VALUES (158, 'ListIt2Menu', 37, 'title', NULL);
INSERT INTO `cms_module_search_items` VALUES (159, 'ListIt2Menu', 38, 'title', NULL);
INSERT INTO `cms_module_search_items` VALUES (160, 'ListIt2Menu', 39, 'title', NULL);
INSERT INTO `cms_module_search_items` VALUES (161, 'ListIt2Menu', 40, 'title', NULL);
INSERT INTO `cms_module_search_items` VALUES (295, 'ListIt2Menu', 41, 'title', NULL);
INSERT INTO `cms_module_search_items` VALUES (427, 'ListIt2Menu', 42, 'title', NULL);
INSERT INTO `cms_module_search_items` VALUES (182, 'ListIt2AboutUs', 2, 'title', NULL);
INSERT INTO `cms_module_search_items` VALUES (183, 'ListIt2AboutUs', 3, 'title', NULL);
INSERT INTO `cms_module_search_items` VALUES (184, 'ListIt2AboutUs', 4, 'title', NULL);
INSERT INTO `cms_module_search_items` VALUES (185, 'ListIt2AboutUs', 5, 'title', NULL);
INSERT INTO `cms_module_search_items` VALUES (282, 'ListIt2Menu', 43, 'title', NULL);
INSERT INTO `cms_module_search_items` VALUES (477, 'ListIt2Menu', 44, 'title', NULL);
INSERT INTO `cms_module_search_items` VALUES (317, 'ListIt2Menu', 45, 'title', NULL);
INSERT INTO `cms_module_search_items` VALUES (277, 'ListIt2Menu', 46, 'title', NULL);
INSERT INTO `cms_module_search_items` VALUES (478, 'ListIt2Menu', 47, 'title', NULL);
INSERT INTO `cms_module_search_items` VALUES (217, 'ListIt2Staff', 2, 'title', NULL);
INSERT INTO `cms_module_search_items` VALUES (218, 'ListIt2Staff', 3, 'title', NULL);
INSERT INTO `cms_module_search_items` VALUES (408, 'ListIt2Testimonials', 3, 'title', NULL);
INSERT INTO `cms_module_search_items` VALUES (289, 'ListIt2Menu', 48, 'title', NULL);
INSERT INTO `cms_module_search_items` VALUES (288, 'ListIt2Menu', 49, 'title', NULL);
INSERT INTO `cms_module_search_items` VALUES (286, 'ListIt2Menu', 50, 'title', NULL);
INSERT INTO `cms_module_search_items` VALUES (291, 'ListIt2Menu', 51, 'title', NULL);
INSERT INTO `cms_module_search_items` VALUES (248, 'ListIt2Menu', 52, 'title', NULL);
INSERT INTO `cms_module_search_items` VALUES (294, 'ListIt2Menu', 53, 'title', NULL);
INSERT INTO `cms_module_search_items` VALUES (479, 'ListIt2Menu', 54, 'title', NULL);
INSERT INTO `cms_module_search_items` VALUES (474, 'ListIt2Menu', 55, 'title', NULL);
INSERT INTO `cms_module_search_items` VALUES (283, 'ListIt2Menu', 56, 'title', NULL);
INSERT INTO `cms_module_search_items` VALUES (296, 'ListIt2Menu', 57, 'title', NULL);
INSERT INTO `cms_module_search_items` VALUES (303, 'ListIt2Gallery', 21, 'title', NULL);
INSERT INTO `cms_module_search_items` VALUES (397, 'ListIt2Gallery', 22, 'title', NULL);
INSERT INTO `cms_module_search_items` VALUES (391, 'ListIt2Gallery', 23, 'title', NULL);
INSERT INTO `cms_module_search_items` VALUES (313, 'ListIt2HeaderTexts', 5, 'title', NULL);
INSERT INTO `cms_module_search_items` VALUES (480, 'ListIt2Menu', 58, 'title', NULL);
INSERT INTO `cms_module_search_items` VALUES (316, 'ListIt2HeaderTexts', 6, 'title', NULL);
INSERT INTO `cms_module_search_items` VALUES (462, 'ListIt2Menu', 59, 'title', NULL);
INSERT INTO `cms_module_search_items` VALUES (459, 'ListIt2Menu', 60, 'title', NULL);
INSERT INTO `cms_module_search_items` VALUES (326, 'ListIt2Menu', 61, 'title', NULL);
INSERT INTO `cms_module_search_items` VALUES (328, 'ListIt2Menu', 62, 'title', NULL);
INSERT INTO `cms_module_search_items` VALUES (327, 'ListIt2Menu', 63, 'title', NULL);
INSERT INTO `cms_module_search_items` VALUES (334, 'ListIt2Blog', 5, 'title', NULL);
INSERT INTO `cms_module_search_items` VALUES (335, 'ListIt2Blog', 6, 'title', NULL);
INSERT INTO `cms_module_search_items` VALUES (336, 'ListIt2Blog', 7, 'title', NULL);
INSERT INTO `cms_module_search_items` VALUES (347, 'Search', 71, 'content', NULL);
INSERT INTO `cms_module_search_items` VALUES (421, 'Search', 72, 'content', NULL);
INSERT INTO `cms_module_search_items` VALUES (361, 'vipmember', 1, 'items', NULL);
INSERT INTO `cms_module_search_items` VALUES (399, 'ListIt2Gallery', 24, 'title', NULL);
INSERT INTO `cms_module_search_items` VALUES (363, 'ListIt2Gallery', 25, 'title', NULL);
INSERT INTO `cms_module_search_items` VALUES (364, 'ListIt2Gallery', 26, 'title', NULL);
INSERT INTO `cms_module_search_items` VALUES (365, 'ListIt2Gallery', 27, 'title', NULL);
INSERT INTO `cms_module_search_items` VALUES (366, 'ListIt2Gallery', 28, 'title', NULL);
INSERT INTO `cms_module_search_items` VALUES (367, 'ListIt2Gallery', 29, 'title', NULL);
INSERT INTO `cms_module_search_items` VALUES (368, 'ListIt2Gallery', 30, 'title', NULL);
INSERT INTO `cms_module_search_items` VALUES (369, 'ListIt2Gallery', 31, 'title', NULL);
INSERT INTO `cms_module_search_items` VALUES (370, 'ListIt2Gallery', 32, 'title', NULL);
INSERT INTO `cms_module_search_items` VALUES (371, 'ListIt2Gallery', 33, 'title', NULL);
INSERT INTO `cms_module_search_items` VALUES (372, 'ListIt2Gallery', 34, 'title', NULL);
INSERT INTO `cms_module_search_items` VALUES (373, 'ListIt2Gallery', 35, 'title', NULL);
INSERT INTO `cms_module_search_items` VALUES (374, 'ListIt2Gallery', 36, 'title', NULL);
INSERT INTO `cms_module_search_items` VALUES (375, 'ListIt2Blog', 8, 'title', NULL);
INSERT INTO `cms_module_search_items` VALUES (400, 'ListIt2Blog', 9, 'title', NULL);
INSERT INTO `cms_module_search_items` VALUES (401, 'ListIt2Blog', 10, 'title', NULL);
INSERT INTO `cms_module_search_items` VALUES (402, 'ListIt2Gallery', 37, 'title', NULL);
INSERT INTO `cms_module_search_items` VALUES (403, 'ListIt2Gallery', 38, 'title', NULL);
INSERT INTO `cms_module_search_items` VALUES (404, 'ListIt2Testimonials', 4, 'title', NULL);
INSERT INTO `cms_module_search_items` VALUES (420, 'Search', 73, 'content', NULL);
INSERT INTO `cms_module_search_items` VALUES (417, 'ListIt2Blog', 11, 'title', NULL);
INSERT INTO `cms_module_search_items` VALUES (416, 'ListIt2Blog', 12, 'title', NULL);
INSERT INTO `cms_module_search_items` VALUES (422, 'Search', 66, 'content', NULL);
INSERT INTO `cms_module_search_items` VALUES (426, 'Search', 74, 'content', NULL);
INSERT INTO `cms_module_search_items` VALUES (430, 'ListIt2Blog', 13, 'title', NULL);
INSERT INTO `cms_module_search_items` VALUES (431, 'ListIt2Blog', 14, 'title', NULL);
INSERT INTO `cms_module_search_items` VALUES (432, 'ListIt2Blog', 15, 'title', NULL);
INSERT INTO `cms_module_search_items` VALUES (465, 'ListIt2Menu', 64, 'title', NULL);
INSERT INTO `cms_module_search_items` VALUES (466, 'ListIt2Menu', 65, 'title', NULL);
INSERT INTO `cms_module_search_items` VALUES (451, 'ListIt2Menu', 66, 'title', NULL);
INSERT INTO `cms_module_search_items` VALUES (467, 'ListIt2Menu', 67, 'title', NULL);

-- ----------------------------
-- Table structure for cms_module_search_items_seq
-- ----------------------------
DROP TABLE IF EXISTS `cms_module_search_items_seq`;
CREATE TABLE `cms_module_search_items_seq`  (
  `id` int(11) NOT NULL
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = FIXED;

-- ----------------------------
-- Records of cms_module_search_items_seq
-- ----------------------------
INSERT INTO `cms_module_search_items_seq` VALUES (480);

-- ----------------------------
-- Table structure for cms_module_search_words
-- ----------------------------
DROP TABLE IF EXISTS `cms_module_search_words`;
CREATE TABLE `cms_module_search_words`  (
  `word` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `count` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`word`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of cms_module_search_words
-- ----------------------------

-- ----------------------------
-- Table structure for cms_module_smarty_plugins
-- ----------------------------
DROP TABLE IF EXISTS `cms_module_smarty_plugins`;
CREATE TABLE `cms_module_smarty_plugins`  (
  `sig` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `name` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `module` varchar(160) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `type` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `callback` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `available` int(11) NULL DEFAULT NULL,
  `cachable` tinyint(4) NULL DEFAULT NULL,
  PRIMARY KEY (`sig`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of cms_module_smarty_plugins
-- ----------------------------
INSERT INTO `cms_module_smarty_plugins` VALUES ('ec39b324d6d868ab1cf8918ee22a3630', 'EventsListing', 'EventsListing', 'function', 's:15:\"function_plugin\";', 1, 0);
INSERT INTO `cms_module_smarty_plugins` VALUES ('1067553f16c8b101d638109a3192c66b', 'FileManager', 'FileManager', 'function', 's:15:\"function_plugin\";', 1, 0);
INSERT INTO `cms_module_smarty_plugins` VALUES ('5085635b76924d7abb3fe1c0411f7ec9', 'ListIt2Loader', 'ListIt2', 'function', 'a:2:{i:0;s:13:\"ListIt2Smarty\";i:1;s:6:\"loader\";}', 1, 1);
INSERT INTO `cms_module_smarty_plugins` VALUES ('3a7859b5385678a53a87f9f6d4ba6456', 'ListIt2DoubleList', 'ListIt2DoubleList', 'function', 's:15:\"function_plugin\";', 1, 0);
INSERT INTO `cms_module_smarty_plugins` VALUES ('29b6be99c1766d29c3aa133f3bfb74dd', 'MenuManager', 'MenuManager', 'function', 's:15:\"function_plugin\";', 1, 0);
INSERT INTO `cms_module_smarty_plugins` VALUES ('3a56cf7e025a641d45d9e226203be1f4', 'menu', 'MenuManager', 'function', 's:15:\"function_plugin\";', 1, 1);
INSERT INTO `cms_module_smarty_plugins` VALUES ('d84423f78fd018b58479f49bfcfd7dff', 'cms_breadcrumbs', 'MenuManager', 'function', 's:22:\"smarty_cms_breadcrumbs\";', 1, 1);
INSERT INTO `cms_module_smarty_plugins` VALUES ('b9a594680999f1eba29c9090badd6687', 'News', 'News', 'function', 's:15:\"function_plugin\";', 1, 0);
INSERT INTO `cms_module_smarty_plugins` VALUES ('0a85af65a1365dea78ed7cd5e5bec265', 'news', 'News', 'function', 's:15:\"function_plugin\";', 1, 1);
INSERT INTO `cms_module_smarty_plugins` VALUES ('10cf00fc6ca5b59a961044ef0ea9c061', 'Search', 'Search', 'function', 's:15:\"function_plugin\";', 1, 0);
INSERT INTO `cms_module_smarty_plugins` VALUES ('ef6c9a9d4dab6989b5da62e2f9a68cf6', 'search', 'Search', 'function', 's:15:\"function_plugin\";', 1, 1);
INSERT INTO `cms_module_smarty_plugins` VALUES ('e75c06fa307418f1ed588e5c86c17d6c', 'SiteMapMadeSimple', 'SiteMapMadeSimple', 'function', 's:15:\"function_plugin\";', 1, 0);
INSERT INTO `cms_module_smarty_plugins` VALUES ('a879d28f519e9d10df94bf445a4ffb80', 'Products', 'Products', 'function', 's:15:\"function_plugin\";', 1, 0);

-- ----------------------------
-- Table structure for cms_module_templates
-- ----------------------------
DROP TABLE IF EXISTS `cms_module_templates`;
CREATE TABLE `cms_module_templates`  (
  `module_name` varchar(160) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `template_name` varchar(160) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `content` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `create_date` datetime NULL DEFAULT NULL,
  `modified_date` datetime NULL DEFAULT NULL,
  INDEX `cms_index_module_templates_by_module_name_template_name`(`module_name`, `template_name`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of cms_module_templates
-- ----------------------------
INSERT INTO `cms_module_templates` VALUES ('CMSPrinting', 'linktemplate', '{if isset($imgsrc)}\n{capture assign=\'image\'}\n  <img src=\"{$imgsrc}\" title=\"{$linktext}\" alt=\"{$linktext}\" {if isset($imgclass) && $imgclass!=\'\'}class=\"{$imgclass}\"{/if} />\n{/capture}\n<a href=\"{$href}\" class=\"{$class}\" {$target} {if isset($more)}{$more}{/if} rel=\"nofollow\">{$image}</a>\n{else}\n<a href=\"{$href}\" class=\"{$class}\" {$target} {if isset($more)}{$more}{/if} rel=\"nofollow\">{$linktext}</a>\n{/if}\n', '2015-04-10 00:18:54', '2015-04-10 00:18:54');
INSERT INTO `cms_module_templates` VALUES ('CMSPrinting', 'printtemplate', '<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\"\n\"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\" xml:lang=\"en\" lang=\"en\">\n  <head>\n    <title>Printing {title}</title>\n    <meta name=\"robots\" content=\"noindex\" />\n    <base href=\"{$rooturl}\" />\n    <meta name=\"Generator\" content=\"CMS Made Simple - Copyright (C) 2004-12 Ted Kulp. All rights reserved.\" />\n    <meta http-equiv=\"Content-Type\" content=\"text/html; charset={$encoding}\" />\n\n    {cms_stylesheet media=\'print\' templateid=$templateid}\n\n    {if $overridestylesheet!=\'\'}\n    <style type=\"text/css\">\n    {$overridestylesheet}\n    </style>\n    {/if}\n    \n  </head>\n  <body style=\"background-color: white; color: black; background-image: none; text-align: left;\">	\n    {$content}\n        \n    {$printscript}\n  </body>\n</html>\n', '2015-04-10 00:18:54', '2015-04-10 00:18:54');
INSERT INTO `cms_module_templates` VALUES ('News', 'summarySample', '<!-- Start News Display Template -->\n{* This section shows a clickable list of your News categories. *}\n<ul class=\"list1\">\n{foreach from=$cats item=node}\n{if $node.depth > $node.prevdepth}\n{repeat string=\"<ul>\" times=$node.depth-$node.prevdepth}\n{elseif $node.depth < $node.prevdepth}\n{repeat string=\"</li></ul>\" times=$node.prevdepth-$node.depth}\n</li>\n{elseif $node.index > 0}</li>\n{/if}\n<li{if $node.index == 0} class=\"firstnewscat\"{/if}>\n{if $node.count > 0}\n	<a href=\"{$node.url}\">{$node.news_category_name}</a>{else}<span>{$node.news_category_name} </span>{/if}\n{/foreach}\n{repeat string=\"</li></ul>\" times=$node.depth-1}</li>\n</ul>\n\n{* this displays the category name if you\'re browsing by category *}\n{if $category_name}\n<h1>{$category_name}</h1>\n{/if}\n\n{* if you don\'t want category browsing on your summary page, remove this line and everything above it *}\n\n{if $pagecount > 1}\n  <p>\n{if $pagenumber > 1}\n{$firstpage}&nbsp;{$prevpage}&nbsp;\n{/if}\n{$pagetext}&nbsp;{$pagenumber}&nbsp;{$oftext}&nbsp;{$pagecount}\n{if $pagenumber < $pagecount}\n&nbsp;{$nextpage}&nbsp;{$lastpage}\n{/if}\n</p>\n{/if}\n{foreach from=$items item=entry}\n<div class=\"NewsSummary\">\n\n{if $entry->postdate}\n	<div class=\"NewsSummaryPostdate\">\n		{$entry->postdate|cms_date_format}\n	</div>\n{/if}\n\n<div class=\"NewsSummaryLink\">\n<a href=\"{$entry->moreurl}\" title=\"{$entry->title|cms_escape:htmlall}\">{$entry->title|cms_escape}</a>\n</div>\n\n<div class=\"NewsSummaryCategory\">\n	{$category_label} {$entry->category}\n</div>\n\n{if $entry->author}\n	<div class=\"NewsSummaryAuthor\">\n		{$author_label} {$entry->author}\n	</div>\n{/if}\n\n{if $entry->summary}\n	<div class=\"NewsSummarySummary\">\n		{eval var=$entry->summary}\n	</div>\n\n	<div class=\"NewsSummaryMorelink\">\n		[{$entry->morelink}]\n	</div>\n\n{else if $entry->content}\n\n	<div class=\"NewsSummaryContent\">\n		{eval var=$entry->content}\n	</div>\n{/if}\n\n{if isset($entry->extra)}\n    <div class=\"NewsSummaryExtra\">\n        {eval var=$entry->extra}\n	{* {cms_module module=\'Uploads\' mode=\'simpleurl\' upload_id=$entry->extravalue} *}\n    </div>\n{/if}\n{if isset($entry->fields)}\n  {foreach from=$entry->fields item=\'field\'}\n     <div class=\"NewsSummaryField\">\n        {if $field->type == \'file\'}\n          <img src=\"{$entry->file_location}/{$field->displayvalue}\"/>\n        {else}\n          {$field->name}:&nbsp;{eval var=$field->displayvalue}\n        {/if}\n     </div>\n  {/foreach}\n{/if}\n\n</div>\n{/foreach}\n<!-- End News Display Template -->\n', '2015-04-10 00:18:55', '2015-04-10 00:18:55');
INSERT INTO `cms_module_templates` VALUES ('News', 'summarySummary_Simplex', '{strip}\r\n\r\n<!-- .news-summary wrapper -->\r\n<article class=\'news-summary\'>\r\n<span class=\'heading\'><span>News</span></span>\r\n        <ul class=\'category-list cf\'>\r\n        {foreach from=$cats item=\'node\'}\r\n        {if $node.depth > $node.prevdepth}\r\n            {repeat string=\'<ul>\' times=$node.depth-$node.prevdepth}\r\n        {elseif $node.depth < $node.prevdepth}\r\n            {repeat string=\'</li></ul>\' times=$node.prevdepth-$node.depth}\r\n            </li>\r\n            {elseif $node.index > 0}</li>\r\n            {/if}\r\n            <li{if $node.index == 0} class=\'first\'{/if}>\r\n        {if $node.count > 0}\r\n                <a href=\'{$node.url}\'>{$node.news_category_name}</a>{else}<span>{$node.news_category_name} </span>{/if}\r\n        {/foreach}\r\n        {repeat string=\'</li></ul>\' times=$node.depth-1}</li>\r\n        </ul>\r\n    {foreach from=$items item=\'entry\'}\r\n    <!-- .news-article (wrapping each article) -->\r\n    <section class=\'news-article\'>\r\n        <header>\r\n            <h2><a href=\'{$entry->moreurl}\' title=\'{$entry->title|cms_escape:htmlall}\'>{$entry->title|cms_escape}</a></h2>\r\n            <div class=\'meta cf\'>\r\n                <time class=\'date\' datetime=\'{$entry->postdate|date_format:\'%Y-%m-%d\'}\'>\r\n                    <span class=\'day\'> {$entry->postdate|date_format:\'%d\'} </span>\r\n                    <span class=\'month\'> {$entry->postdate|date_format:\'%b\'} </span>\r\n                </time>\r\n                <span class=\'author\'> {$author_label} {$entry->author} </span>\r\n                <span class=\'category\'> {$category_label} {$entry->category}</span>\r\n            </div>\r\n        </header>\r\n        {if $entry->summary}\r\n            <p>{eval var=$entry->summary|strip_tags}</p>\r\n            <span class=\'more\'>{$entry->morelink} →</span>\r\n        {else if $entry->content}\r\n            <p>{eval var=$entry->content|strip_tags}</p>\r\n        {/if}\r\n    </section>\r\n    <!-- .news-article //-->\r\n    {/foreach}\r\n        <!-- news pagination -->\r\n        {if $pagecount > 1}\r\n        <span class=\'paginate\'>\r\n            {if $pagenumber > 1}\r\n                {$firstpage} {$prevpage}\r\n            {/if}\r\n                {$pagetext} {$pagenumber} {$oftext} {$pagecount}\r\n            {if $pagenumber < $pagecount}\r\n                {$nextpage} {$lastpage}\r\n            {/if}\r\n        </span>\r\n        {/if}\r\n</article>\r\n<!-- .news-summary //-->\r\n\r\n{/strip}', '2015-04-10 00:18:55', '2015-04-10 00:18:55');
INSERT INTO `cms_module_templates` VALUES ('News', 'detailSample', '{* News module entry object reference:\n   ------------------------------\n   In previous versions of News the \'object\' returned in $entry was quite simple, and a <pre>{$entry|@print_r}</pre> would output all of the available data\n   This has changed in News 2.12, the object is not quite as \'simple\' as it was in previous versions, and that method will no longer work.  Hence, below\n   you will find a referennce to the available data.\n\n   ====\n   news_article Object Reference\n   ====\n\n     Members:\n     --\n     Members can be displayed by the following syntax: {$entry->membername} or assigned to another smarty variable using {assign var=\'foo\' value=$entry->membername}.\n\n     The following members are available in the entry array:\n   \n     id (integer)           = The unique article id.\n     author_id (integer)    = The userid of the author who created the article.  This value may be negative to indicate an FEU userid.\n     title (string)         = The title of the article.\n     summary (text)         = The summary text (may be empty or unset).\n     extra (string)         = The \"extra\" data associated with the article (may be empty or unset).\n     news_url (string)      = The url segment associated with this article (may be empty or unset).\n     postdate (string)      = A string representing the news article post date.  You may filter this through cms_date_format for different display possibilities.\n     startdate (string)     = A string representing the date the article should begin to appear.  (may be empty or unset)\n     enddate (string)       = A string representing the date the article should stop appearing on the site (may be empty or unset).\n     category_id (integer)  = The unique id of the hierarchy level where this article resides (may be empty or unset)\n     status (string)        = either \'draft\' or \'published\' indicating the status of this article.\n     author (string)        = The username of the original author of the article.  If the article was created by frontend submission, this will attempt to retrieve the username from the FEU module.\n     authorname (string)    = The full name of the original author of the website. Only applicable if article was created by an administrator and that information exists in the administrators profile.\n     category (string)      = The name of the category that this article is associated with.\n     canonical (string)     = A full URL (prettified) to this articles detail view using defaults if necessary.\n     fields (associative)   = An associative array of field objects, representing the fields, and their values for this article.  See the information below on the field object definition.   In past versions of News this was a simple array, now it is an associative one.\n     customfieldsbyname     = (deprecated) - A synonym for the \'fields\' member\n     fieldsbyname           = (deprecated) - A synonym for the \'fields\' member\n     useexp (integer)       = A flag indicating wether this article is using the expiry information.\n     file_location (string) = A url containing the location where files attached the article are stored... the field value should be appended to this url.\n     \n\n   ====\n   news_field Object Reference\n   ====\n   The news_field object contains data about the fields and their values that are associated with a particular news article.\n\n     Members:\n     --------\n     id (integer)  = The id of the field definition\n     name (string) = The name of the field\n     type (string) = The type of field\n     max_length (integer) = The maximum length of the field (applicable only to text fields)\n     item_order (integer) = The order of the field\n     public (integer) = A flag indicating wether the field is public or not\n     value (mixed)    = The value of the field.\n     displayvalue (mixed) = A displayable value for the field.  This is particularly useful in the case of dropdown fields.\n\n\n   ====\n   Below, you will find the normal detail template information.  Modify this template as desired.\n*}\n\n{* set a canonical variable that can be used in the head section if process_whole_template is false in the config.php *}\n{if isset($entry->canonical)}\n  {assign var=\'canonical\' value=$entry->canonical}\n{/if}\n\n{if $entry->postdate}\n	<div id=\"NewsPostDetailDate\">\n		{$entry->postdate|cms_date_format}\n	</div>\n{/if}\n<h3 id=\"NewsPostDetailTitle\">{$entry->title|cms_escape:htmlall}</h3>\n\n<hr id=\"NewsPostDetailHorizRule\" />\n\n{if $entry->summary}\n	<div id=\"NewsPostDetailSummary\">\n		<strong>\n			{eval var=$entry->summary}\n		</strong>\n	</div>\n{/if}\n\n{if $entry->category}\n	<div id=\"NewsPostDetailCategory\">\n		{$category_label} {$entry->category}\n	</div>\n{/if}\n{if $entry->author}\n	<div id=\"NewsPostDetailAuthor\">\n		{$author_label} {$entry->author}\n	</div>\n{/if}\n\n<div id=\"NewsPostDetailContent\">\n	{eval var=$entry->content}\n</div>\n\n{if $entry->extra}\n	<div id=\"NewsPostDetailExtra\">\n		{$extra_label} {$entry->extra}\n	</div>\n{/if}\n\n{if $return_url != \"\"}\n<div id=\"NewsPostDetailReturnLink\">{$return_url}{if $category_name != \'\'} - {$category_link}{/if}</div>\n{/if}\n\n{if isset($entry->fields)}\n  {foreach from=$entry->fields item=\'field\'}\n     <div class=\"NewsDetailField\">\n        {if $field->type == \'file\'}\n	  {* this template assumes that every file uploaded is an image of some sort, because News doesn\'t distinguish *}\n          <img src=\"{$entry->file_location}/{$field->displayvalue}\"/>\n        {else}\n          {$field->name}:&nbsp;{eval var=$field->displayvalue}\n        {/if}\n     </div>\n  {/foreach}\n{/if}\n', '2015-04-10 00:18:55', '2015-04-10 00:18:55');
INSERT INTO `cms_module_templates` VALUES ('News', 'detailSimplex_Detail', '{* set a canonical variable that can be used in the head section if process_whole_template is false in the config.php *}\r\n{if isset($entry->canonical)}\r\n  {assign var=\'canonical\' value=$entry->canonical}\r\n{/if}\r\n\r\n<h2>{$entry->title|cms_escape:htmlall}</h2>\r\n{if $entry->summary}\r\n    {eval var=$entry->summary}\r\n{/if}\r\n    {eval var=$entry->content}\r\n{if $entry->extra}\r\n		{$extra_label} {$entry->extra}\r\n{/if}\r\n{if $return_url != \"\"}\r\n    <br />\r\n        <span class=\'back\'>← {$return_url}{if $category_name != \'\'} - {$category_link}{/if}</span>\r\n{/if}\r\n\r\n{if isset($entry->fields)}\r\n  {foreach from=$entry->fields item=\'field\'}\r\n     <div>\r\n        {if $field->type == \'file\'}\r\n	  {* this template assumes that every file uploaded is an image of some sort, because News doesn\'t distinguish *}\r\n          <img src=\'{$entry->file_location}/{$field->value}\' alt=\'\' />\r\n        {else}\r\n          {$field->name}: {eval var=$field->value}\r\n        {/if}\r\n     </div>\r\n  {/foreach}\r\n{/if}\r\n    <footer class=\'news-meta\'>\r\n    {if $entry->postdate}\r\n        {$entry->postdate|cms_date_format}\r\n    {/if}\r\n    {if $entry->category}\r\n        <strong>{$category_label}</strong> {$entry->category}\r\n    {/if}\r\n    {if $entry->author}\r\n        <strong>{$author_label}</strong> {$entry->author}\r\n    {/if}\r\n    </footer>', '2015-04-10 00:18:55', '2015-04-10 00:18:55');
INSERT INTO `cms_module_templates` VALUES ('News', 'formSample', '{* original form template *}\n{if isset($error)}\n  <h3><font color=\"red\">{$error}</font></h3>\n{else}\n  {if isset($message)}\n    <h3>{$message}</h3>\n  {/if}\n{/if}\n{$startform}\n	<div class=\"pageoverflow\">\n		<p class=\"pagetext\">*{$titletext}:</p>\n		<p class=\"pageinput\">{$inputtitle}</p>\n	</div>\n	<div class=\"pageoverflow\">\n		<p class=\"pagetext\">{$categorytext}:</p>\n		<p class=\"pageinput\">{$inputcategory}</p>\n	</div>\n{if !isset($hide_summary_field) or $hide_summary_field == 0}\n	<div class=\"pageoverflow\">\n		<p class=\"pagetext\">{$summarytext}:</p>\n		<p class=\"pageinput\">{$inputsummary}</p>\n	</div>\n{/if}\n	<div class=\"pageoverflow\">\n		<p class=\"pagetext\">*{$contenttext}:</p>\n		<p class=\"pageinput\">{$inputcontent}</p>\n	</div>\n	<div class=\"pageoverflow\">\n		<p class=\"pagetext\">{$extratext}:</p>\n		<p class=\"pageinput\">{$inputextra}</p>\n	</div>\n	<div class=\"pageoverflow\">\n		<p class=\"pagetext\">{$startdatetext}:</p>\n		<p class=\"pageinput\">{html_select_date prefix=$startdateprefix time=$startdate end_year=\"+15\"} {html_select_time prefix=$startdateprefix time=$startdate}</p>\n	</div>\n	<div class=\"pageoverflow\">\n		<p class=\"pagetext\">{$enddatetext}:</p>\n		<p class=\"pageinput\">{html_select_date prefix=$enddateprefix time=$enddate end_year=\"+15\"} {html_select_time prefix=$enddateprefix time=$enddate}</p>\n	</div>\n	{if isset($customfields)}\n	   {foreach from=$customfields item=\'onefield\'}\n	      <div class=\"pageoverflow\">\n		<p class=\"pagetext\">{$onefield->name}:</p>\n		<p class=\"pageinput\">{$onefield->field}</p>\n	      </div>\n	   {/foreach}\n	{/if}\n	<div class=\"pageoverflow\">\n		<p class=\"pagetext\">&nbsp;</p>\n		<p class=\"pageinput\">{$hidden}{$submit}{$cancel}</p>\n	</div>\n{$endform}\n', '2015-04-10 00:18:55', '2015-04-10 00:18:55');
INSERT INTO `cms_module_templates` VALUES ('News', 'browsecatSample', '{if $count > 0}\n<ul class=\"list1\">\n{foreach from=$cats item=node}\n{if $node.depth > $node.prevdepth}\n{repeat string=\"<ul>\" times=$node.depth-$node.prevdepth}\n{elseif $node.depth < $node.prevdepth}\n{repeat string=\"</li></ul>\" times=$node.prevdepth-$node.depth}\n</li>\n{elseif $node.index > 0}</li>\n{/if}\n<li class=\"newscategory\">\n{if $node.count > 0}\n	<a href=\"{$node.url}\">{$node.news_category_name}</a> ({$node.count}){else}<span>{$node.news_category_name} (0)</span>{/if}\n{/foreach}\n{repeat string=\"</li></ul>\" times=$node.depth-1}</li>\n</ul>\n{/if}', '2015-04-10 00:18:55', '2015-04-10 00:18:55');
INSERT INTO `cms_module_templates` VALUES ('News', 'email_template', 'A new news article has been posted to your website.  The details are as follows:\nTitle:      {$title}\nIP Address: {$ipaddress}\nSummary:    {$summary|strip_tags}\nPost Date:  {$postdate|date_format}\nStart Date: {$startdate|date_format}\nEnd Date:   {$enddate|date_format}\n', '2015-04-10 00:18:55', '2015-04-10 00:18:55');
INSERT INTO `cms_module_templates` VALUES ('Search', 'displaysearch', '\n{$startform}\n<label for=\"{$search_actionid}searchinput\">{$searchprompt}:&nbsp;</label><input type=\"text\" class=\"search-input\" id=\"{$search_actionid}searchinput\" name=\"{$search_actionid}searchinput\" size=\"20\" maxlength=\"50\" value=\"{$searchtext}\" {$hogan}/>\n{*\n<br/>\n<input type=\"checkbox\" name=\"{$search_actionid}use_or\" value=\"1\"/>\n*}\n<input class=\"search-button\" name=\"submit\" value=\"{$submittext}\" type=\"submit\" />\n{if isset($hidden)}{$hidden}{/if}\n{$endform}', '2015-04-10 00:18:57', '2015-04-10 00:18:57');
INSERT INTO `cms_module_templates` VALUES ('Search', 'displayresult', '<h3>{$searchresultsfor} &quot;{$phrase}&quot;</h3>\n{if $itemcount > 0}\n<ul>\n  {foreach from=$results item=entry}\n  <li>{$entry->title} - <a href=\"{$entry->url}\">{$entry->urltxt}</a> ({$entry->weight}%)</li>\n  {* \n     You can also instantiate custom behaviour on a module by module basis by looking at\n     the $entry->module and $entry->modulerecord fields in $entry \n      ie: {if $entry->module == \'News\'}{News action=\'detail\' article_id=$entry->modulerecord detailpage=\'News\'} \n  *}\n  {/foreach}\n</ul>\n\n<p>{$timetaken}: {$timetook}</p>\n{else}\n  <p><strong>{$noresultsfound}</strong></p>\n{/if}', '2015-04-10 00:18:57', '2015-04-10 00:18:57');
INSERT INTO `cms_module_templates` VALUES ('CGExtensions', 'cg_errormsg', '{* original template for displaying frontend errors *}\n<div class=\"{$cg_errorclass}\">{$cg_errormsg}</div>', '2015-04-10 00:49:51', '2015-04-10 00:49:51');
INSERT INTO `cms_module_templates` VALUES ('CGExtensions', 'sortablelists_Sample', '{* sortable list template *}\n\n{*\n This template provides one example of using javascript in a CMS module template.  The javascript is left here as an example of how one can interact with smarty in javascript.  You may infact want to put most of these functions into a seperate .js file and include it somewhere in your head section.\n\n You are free to modify this javascript and this template.  However, the php driver scripts look for a field named in the smarty variable {$selectarea_prefix}, and expect that to be a comma seperated list of values.\n *}\n\n\n<script type=\'text/javascript\'>\nvar allowduplicates = {$allowduplicates};\nvar selectlist = \"{$selectarea_prefix}_selectlist\";\nvar masterlist = \"{$selectarea_prefix}_masterlist\";\nvar addbtn = \"{$selectarea_prefix}_add\";\nvar rembtn = \"{$selectarea_prefix}_remove\";\nvar upbtn = \"{$selectarea_prefix}_up\";\nvar downbtn = \"{$selectarea_prefix}_down\";\nvar valuefld = \"{$selectarea_prefix}\";\nvar max_selected = {$max_selected};\n\nfunction selectarea_update_value()\n{\n  var sel_elem = document.getElementById(selectlist);\n  var val_elem = document.getElementById(valuefld);\n  var sel_idx = sel_elem.selectedIndex;\n  var opts = sel_elem.getElementsByTagName(\'option\');\n  var tmp = new Array();\n  for( i = 0; i < opts.length; i++ )\n    {\n      tmp[tmp.length] = opts[i].value;\n    }\n  var str = tmp.join(\',\');\n  val_elem.value = str;\n}\n\nfunction selectarea_handle_down()\n{\n  var sel_elem = document.getElementById(selectlist);\n  var sel_idx = sel_elem.selectedIndex;\n  var opts = sel_elem.getElementsByTagName(\'option\');\n  for( var i = opts.length - 2; i >= 0; i-- )\n    {\n      var opt = opts[i];\n      if( opt.selected )\n        {\n           var nextopt = opts[i+1];\n           opt = sel_elem.removeChild(opt);\n           nextopt = sel_elem.replaceChild(opt,nextopt);\n           sel_elem.insertBefore(nextopt,opt);\n        }\n    }\n  selectarea_update_value();\n}\n\nfunction selectarea_handle_up()\n{\n  var sel_elem = document.getElementById(selectlist);\n  var sel_idx = sel_elem.selectedIndex;\n  var opts = sel_elem.getElementsByTagName(\'option\');\n  if( sel_idx > 0 )\n    {\n      for( var i = 1; i < opts.length; i++ )\n        {\n          var opt = opts[i];\n          if( opt.selected )\n            {\n              sel_elem.removeChild(opt);\n               sel_elem.insertBefore(opt, opts[i-1]);\n            }\n        }\n    }\n  selectarea_update_value();\n}\n\nfunction selectarea_handle_remove()\n{\n  var sel_elem = document.getElementById(selectlist);\n  var sel_idx = sel_elem.selectedIndex;\n  if( sel_idx >= 0 ) {\n    var val = sel_elem.options[sel_idx].value;\n    sel_elem.remove(sel_idx);\n  }\n  selectarea_update_value();\n}\n\nfunction selectarea_handle_add()\n{\n  var mas_elem = document.getElementById(masterlist);\n  var mas_idx = mas_elem.selectedIndex;\n  var sel_elem = document.getElementById(selectlist);\n  var opts = sel_elem.getElementsByTagName(\'option\');\n  if( opts.length >= max_selected && max_selected > 0) return;\n  if( mas_idx >= 0 ) {\n      var newOpt = document.createElement(\'option\');\n      newOpt.text = mas_elem.options[mas_idx].text;\n      newOpt.value = mas_elem.options[mas_idx].value;\n      if( allowduplicates == 0 ) {\n        for( var i = 0; i < opts.length; i++ ) {\n          if( opts[i].value == newOpt.value ) return;\n        }\n      }\n      sel_elem.add(newOpt,null);\n  }\n  selectarea_update_value();\n}\n\n\nfunction selectarea_handle_select()\n{\n  var sel_elem = document.getElementById(selectlist);\n  var sel_idx = sel_elem.selectedIndex;\n  var mas_elem = document.getElementById(masterlist);\n  var mas_idx = mas_elem.selectedIndex;\n  addbtn.disabled = (mas_idx >= 0);\n  rembtn.disabled = (sel_idx >= 0);\n  addbtn.disabled = (sel_idx >= 0);\n  downbtn.disabled = (sel_idx >= 0);\n}\n\n</script>\n\n<table>\n  <tr>\n    <td>\n      {* left column - for the selected items *}\n      {$label_left}<br/>\n      <select id=\"{$selectarea_prefix}_selectlist\" size=\"10\" onchange=\"selectarea_handle_select();\">\n        {html_options options=$selectarea_selected}\n      </select><br/>\n    </td>\n    <td>\n      {* center column - for the add/delete buttons *}\n      <input type=\"submit\" id=\"{$selectarea_prefix}_add\" value=\"&lt;&lt;\" onclick=\"selectarea_handle_add(); return false;\"/><br/>\n      <input type=\"submit\" id=\"{$selectarea_prefix}_remove\" value=\"&gt;&gt;\" onclick=\"selectarea_handle_remove(); return false;\"/><br/>\n      <input type=\"submit\" id=\"{$selectarea_prefix}_up\" value=\"{$upstr}\" onclick=\"selectarea_handle_up(); return false;\"/><br/>\n      <input type=\"submit\" id=\"{$selectarea_prefix}_down\" value=\"{$downstr}\" onclick=\"selectarea_handle_down(); return false;\"/><br/>\n    </td>\n    <td>\n      {* right column - for the master list *}\n      {$label_right}<br/>\n      <select id=\"{$selectarea_prefix}_masterlist\" size=\"10\" onchange=\"selectarea_handle_select();\">\n        {html_options options=$selectarea_masterlist}\n      </select>\n    </td>\n  </tr>\n</table>\n<div><input type=\"hidden\" id=\"{$selectarea_prefix}\" name=\"{$selectarea_prefix}\" value=\"{$selectarea_selected_str}\" /></div>\n', '2015-04-10 00:49:51', '2015-04-10 00:49:51');
INSERT INTO `cms_module_templates` VALUES ('MleCMS', 'mle_templateFlags', '{if $langs_count}\r\n    {foreach from=$langs item=l name=language}\r\n    {capture assign=\"lang_href\"}{cms_selflink href=$l.alias}{/capture}\r\n    {if $lang_href}\r\n        {if $page_alias==$l.alias}\r\n            <li class=\"flags\"><a>\r\n        {if $l.flag}<img src=\"uploads/{$l.flag}\" alt=\"{$l.name}\" title=\"{$l.name}\"  />{else}{$l.name}{/if}\r\n    </a></li>\r\n{else}\r\n    <li class=\"flags\"><a {if $l.flag}style=\"-ms-filter:\'progid:DXImageTransform.Microsoft.Alpha(Opacity=50)\'; filter: alpha(opacity=50); opacity:.5;\"{/if} href=\"{$lang_href}\">\r\n{if $l.flag}<img src=\"uploads/{$l.flag}\" alt=\"{$l.name}\" title=\"{$l.name}\"  />{else}{$l.name}{/if}\r\n</a></li>\r\n{/if}\r\n{/if}\r\n{/foreach}\r\n{/if}', '2015-04-11 16:25:05', '2015-04-24 18:01:53');
INSERT INTO `cms_module_templates` VALUES ('MleCMS', 'mle_templateDropdown', '{if $langs_count}\n    <select onchange=\"location.href=options[selectedIndex].value;\">\n        {foreach from=$langs item=l name=language}\n        {capture assign=\"lang_href\"}{cms_selflink href=$l.alias}{/capture}\n        {if $lang_href}\n            {if $page_alias==$l.alias}\n                <option selected=\"selected\" value=\"{$lang_href}\">{$l.name}</option>\n            {else}\n                <option value=\"{$lang_href}\">{$l.name}</option>\n            {/if}\n        {/if}\n    {/foreach}\n</select>\n{/if}', '2015-04-11 16:25:05', '2015-04-11 16:25:05');
INSERT INTO `cms_module_templates` VALUES ('MleCMS', 'mle_templateSeparator', '{if $langs_count}\n    {foreach from=$langs item=l name=language}\n        {*separator*}\n    {if $smarty.foreach.language.first==false}/{/if}\n    \n{capture assign=\"lang_href\"}{cms_selflink href=$l.alias}{/capture}\n\n{if $lang_href}\n    {if $page_alias==$l.alias}\n        <span class=\"active\">\n            {$l.name}\n        </span>\n    {else}\n        <a   href=\"{$lang_href}\">\n            {$l.name}\n        </a>\n    {/if}\n{/if}\n{/foreach}\n{/if}', '2015-04-11 16:25:05', '2015-04-11 16:25:05');
INSERT INTO `cms_module_templates` VALUES ('ListIt2HeaderTexts', 'category_default', '<!-- categories -->\n<ul>\n{foreach from=$categories item=category}\n	<li class=\"category-{$category->alias}\">\n		<a class=\"category-name\" href=\"{$category->url}\">{$category->name} ({$category->items|count})</a>\n			{if !empty($category->description)}\n			<div class=\"category-description\">\n				{eval var=$category->description}\n			</div>\n			{/if}\n	</li>\n{/foreach}\n<ul>\n<!-- categories //-->', '2015-04-24 18:30:43', '2015-04-24 18:30:43');
INSERT INTO `cms_module_templates` VALUES ('ListIt2HeaderTexts', 'category_hierarchy', '<!-- categories hierarchy -->\n<ul class=\"categories\">\n{foreach from=$categories item=category}\n\n{if $category->depth > $category->prevdepth}\n	{repeat string=\"<ul>\" times=$category->depth-$category->prevdepth}\n{elseif $category->depth < $category->prevdepth}\n	{repeat string=\"</li></ul>\" times=$category->prevdepth-$category->depth}\n	</li>\n{elseif $category->index > 0}\n	</li>\n{/if}\n\n{if $category->current}\n<li class=\"category-{$category->alias} current{if $category->parent == true || $category->children|@count > 0} parent{/if}\">\n	<a href=\"{$category->url}\">{$category->menutext}</a>\n{else}\n<li class=\"category-{$category->alias}{if $category->parent == true || $category->children|@count > 0} parent{/if}\">\n	<a href=\"{$category->url}\">{$category->menutext}</a>\n{/if}\n\n{/foreach}\n\n	{repeat string=\"</li></ul>\" times=$category->depth-1}\n	</li>\n</ul>\n<!-- categories hierarchy //-->', '2015-04-24 18:30:43', '2015-04-24 18:30:43');
INSERT INTO `cms_module_templates` VALUES ('MenuManager', 'main_menu', '{if $count > 0}\r\n<ul class=\"sf-menu\">\r\n{foreach from=$nodelist item=node}\r\n<li><a href=\"#hungry-{$node->alias}\">{$node->menutext}</a></li>\r\n{/foreach}\r\n</ul>\r\n{/if}\r\n', '2015-04-24 18:25:43', '2015-09-18 13:58:25');
INSERT INTO `cms_module_templates` VALUES ('ListIt2HeaderTexts', 'summary_default', '<ul class=\"header-texts\">\r\n{foreach from=$items item=item}\r\n<li>{$item->title}!</li>\r\n{/foreach}\r\n</ul>', '2015-04-24 18:33:17', '2015-04-24 18:33:17');
INSERT INTO `cms_module_templates` VALUES ('ListIt2AboutUs', 'summary_default', '{foreach from=$items item=item}\r\n			<!-- START Section Images -->\r\n			<div class=\"grid-50 tablet-grid-100 mobile-grid-100\">\r\n				<div class=\"wow rotateIn\" data-wow-duration=\"2s\" data-wow-offset=\"250\">\r\n					<div class=\"about-images\">\r\n						<img class=\"about-main\" src=\"uploads/aboutus/{$item->fielddefs.large->value}\" alt=\"Large\" />\r\n						<img class=\"about-inset\" src=\"uploads/aboutus/{$item->fielddefs.small->value}\" alt=\"Small\" />\r\n					</div>\r\n				</div>\r\n			</div>\r\n			<!-- END Section Images -->\r\n			\r\n			<!-- START Section Content -->\r\n			<div class=\"grid-50 tablet-prefix-10 tablet-grid-80 tablet-suffix-10 mobile-grid-100 tablet-center mobile-center\">\r\n				<div class=\"wow fadeInRight\" data-wow-duration=\"2s\" data-wow-offset=\"250\">\r\n					<h4 class=\"header-divider\">{$item->title}</h4>\r\n					<p>{$item->fielddefs.descript->value|strip_tags:false}</p>\r\n<a class=\"hungry-button dark\" href=\"#hungry-testimonials\">Cảm nhận khách hàng</a>\r\n					<a class=\"hungry-button dark\" href=\"#hungry-menu\">Thực đơn nhà hàng</a>\r\n				</div>\r\n			</div>\r\n			<!-- END Section Content -->\r\n{/foreach}\r\n', '2015-05-01 00:31:20', '2015-05-01 00:31:20');
INSERT INTO `cms_module_templates` VALUES ('ListIt2Staff', 'summary_default', '{if $items|@count > 0}\r\n	{foreach from=$items item=item}\r\n<div class=\"grid-33 tablet-prefix-20 tablet-grid-60 tablet-suffix-20 mobile-grid-100\">\r\n				<div class=\"wow fadeInLeft\" data-wow-duration=\"2s\" data-wow-offset=\"250\">\r\n					<div class=\"hungry-staff-member\">\r\n						<header>\r\n							<img src=\"uploads/staff/{$item->fielddefs.image->value}\" class=\"hungry-staff-member-thumbnail\" alt=\"{$item->title}\" />\r\n							<h3 class=\"hungry-staff-member-title\">{$item->title}</h3>\r\n							<h4 class=\"hungry-staff-member-role\">{$item->fielddefs.staff->value}</h4>\r\n						</header>\r\n						<div class=\"hungry-staff-member-content\">\r\n							<p>{$item->fielddefs.descript->value}</p>\r\n						</div>\r\n						<footer>\r\n							<ul class=\"hungry-staff-member-social-icons\">\r\n								{if $item->fielddefs.facebook->value}<li><a href=\"https://www.facebook.com/{$item->fielddefs.facebook->value}\" class=\"team-tooltip\" title=\"Like {$item->title}!\"><i class=\"fa fa-facebook\"></i></a></li>{/if}\r\n								{if $item->fielddefs.twitter->value}<li><a href=\"https://twitter.com/{$item->fielddefs.twitter->value}\" class=\"team-tooltip\" title=\"Tweet {$item->title}!\"><i class=\"fa fa-twitter\"></i></a></li>{/if}\r\n								{if $item->fielddefs.pinterest->value}<li><a href=\"https://www.pinterest.com/{$item->fielddefs.pinterest->value}\" class=\"team-tooltip\" title=\"See {$item->title}!\"><i class=\"fa fa-instagram\"></i></a></li>{/if}\r\n							</ul>\r\n						</footer>\r\n					</div>\r\n				</div>\r\n			</div>\r\n	{/foreach}\r\n{/if}', '2015-05-01 22:14:01', '2015-05-01 22:14:01');
INSERT INTO `cms_module_templates` VALUES ('ListIt2HeaderTexts', 'summary_searchresults', '{if $items|@count > 0}\n\n{if $pagecount > 1}\n<!-- pagination -->\n<p>\n{if $pagenumber > 1}\n    {$firstpage}&nbsp;{$prevpage}\n{/if}\n{foreach from=$pagelinks item=page}\n    {$page->link}\n{/foreach}\n{if $pagenumber < $pagecount}\n    &nbsp;{$nextpage}&nbsp;{$lastpage}\n{/if}\n</p>\n<!-- pagination //-->\n{/if}\n\n{if !empty($actionparams.search)}\n    <h2>{$mod->ModLang(\'searchresultsfor\')} &quot;{$actionparams.search}&quot;</h2>\n{/if}\n\n<ul>\n{foreach from=$items item=item}\n    <li class=\"item searchresult\">\n        <a href=\"{$item->url}\">{$item->title}</a>\n    </li>\n{/foreach}\n</ul>\n\n{/if}', '2015-04-24 18:30:43', '2015-04-24 18:30:43');
INSERT INTO `cms_module_templates` VALUES ('ListIt2HeaderTexts', 'detail_default', '<!-- item -->\n<div class=\"item\">\n	<h2 class=\"item-title\">{$item->title|cms_escape}</h2>\n\n	{if !empty($item->fielddefs)}\n\n	<div class=\"item-properties\">\n		{foreach from=$item->fielddefs item=fielddef}\n\n		{*\n			Categories were moved to field definitions.\n			If you need Categories, create new Field definition with alias \"category\"\n			and Categories will be available again.\n		*}\n\n		{if $fielddef.type == \'Categories\' && ($fielddef.value != \'\')}\n\n			{* use ListIt2Loader plugin if you need Category information in default module action templates *}\n			{ListIt2Loader item=\'category\' force_array=1 value=$fielddef.value assign=\'cats\'}\n	\n			<!-- categories -->\n			<div class=\"item-category\">\n				Category: {$cats|implode:\',\'}\n			</div>\n			<!-- categories //-->\n			\n		{/if}\n\n		{if $fielddef.value && $fielddef.type != \'Categories\'}\n			{if $fielddef.type == \'SelectFile\' || $fielddef.type == \'FileUpload\'}\n				{$fielddef.name}: <a href=\"{$fielddef->GetImagePath(true)}/{$fielddef.value}\">{$fielddef.value}</a><br />\n		{elseif $fielddef.type == \'SelectDateTime\'}\n					{$fielddef.name}: {$fielddef.value|cms_date_format}<br />\n		{else}\n			{$fielddef.name}: {$fielddef.value}<br />\n			{/if}\n		{/if}\n\n		{/foreach}\n	</div>\n\n	{/if}\n	\n	<a href=\"{$return_url}\" class=\"return-link\">return</a>\n	{* or use {$return_link} to create link tag *}\n	\n</div>\n<!-- item //-->', '2015-04-24 18:30:43', '2015-04-24 18:30:43');
INSERT INTO `cms_module_templates` VALUES ('ListIt2HeaderTexts', 'search_default', '<div id=\"{$modulealias}_search\">\n	{$formstart}\n		<label for=\"{$modulealias}searchinput\">{$mod->ModLang(\'searchfor\')}:</label>&nbsp;\n		<input type=\"text\" id=\"{$modulealias}searchinput\" name=\"{$actionid}search\" size=\"20\" maxlength=\"50\" value=\"\" />\n		<input class=\"search-button\" name=\"submit\" value=\"{$mod->ModLang(\'search\')}\" type=\"submit\" />\n	{$formend}\n</div>', '2015-04-24 18:30:43', '2015-04-24 18:30:43');
INSERT INTO `cms_module_templates` VALUES ('ListIt2HeaderTexts', 'search_filter', '<div id=\"{$modulealias}_filter\">\n\n	<h3>{$filterprompt}</h3>\n\n	{$formstart}\n\n		{foreach from=$fielddefs item=fielddef}\n		<div class=\"form-row\">\n			\n		{if $fielddef.type != \'Categories\'}\n			<label for=\"filter_{$fielddef->alias}\">{$fielddef->name}</label>\n			\n			{if $fielddef.type == \'Checkbox\'}\n				<input type=\"checkbox\" name=\"{$actionid}search_{$fielddef->alias}\" id=\"filter_{$fielddef->alias}\" value=\"{$fielddef->value}\" />\n			{else}\n			\n			<select name=\"{$actionid}search_{$fielddef->alias}\" id=\"filter_{$fielddef->alias}\">\n				<option value=\'\'>{$mod->ModLang(\'all\')}</option>\n				{foreach from=$fielddef->values item=value}\n				<option>{$value}</option>\n				{/foreach}\n			</select>\n			{/if}\n			\n		{/if}\n			\n		</div>\n		{/foreach}\n	\n		<input class=\"search-button\" name=\"submit\" value=\"{$mod->ModLang(\'search\')}\" type=\"submit\" />\n\n	{$formend}\n\n</div>', '2015-04-24 18:30:43', '2015-04-24 18:30:43');
INSERT INTO `cms_module_templates` VALUES ('ListIt2HeaderTexts', 'search_multiselect', '<div id=\"{$modulealias}_filter\">\n\n	<h3>{$filterprompt}</h3>\n\n	{$formstart}\n\n		{foreach from=$fielddefs item=fielddef}\n		<div class=\"filter form-row\">\n\n		{if $fielddef.type != \'Categories\'}\n		\n			<label>{$fielddef->name}</label><br />\n			{if $fielddef.type == \'Checkbox\'}\n				\n				<input type=\"checkbox\" name=\"{$actionid}search_{$fielddef->alias}\" id=\"filter_{$fielddef->alias}\" value=\"{$fielddef->value}\" />\n			{else}\n				\n				<select name=\"{$actionid}search_{$fielddef->alias}[]\" size=\"5\" multiple=\"multiple\">\n					{foreach from=$fielddef->values item=\'value\'}\n					<option value=\"{$value}\">{$value}</option>\n					{/foreach}\n				</select>\n			{/if}\n		{/if}\n		</div>\n		{/foreach}\n\n		<input class=\"search-button\" name=\"submit\" value=\"{$mod->ModLang(\'filter\')}\" type=\"submit\" />\n\n	{$formend}\n</div>', '2015-04-24 18:30:43', '2015-04-24 18:30:43');
INSERT INTO `cms_module_templates` VALUES ('ListIt2HeaderTexts', 'archive_default', '{if $archives|@count > 0}\n\n    <!-- archives -->\n    <ul>\n        {foreach from=$archives item=\'archive\'}\n            <li><a href=\"{$archive->url}\">{$archive->month}/{$archive->year} ({$archive->count})</a> [{$archive->timestamp|cms_date_format:\'%B %Y\'}]</li>    \n        {/foreach}\n    </ul>\n    <!-- archives //-->\n    \n{/if}', '2015-04-24 18:30:43', '2015-04-24 18:30:43');
INSERT INTO `cms_module_templates` VALUES ('ListIt2AboutUs', 'summary_searchresults', '{if $items|@count > 0}\n\n{if $pagecount > 1}\n<!-- pagination -->\n<p>\n{if $pagenumber > 1}\n    {$firstpage}&nbsp;{$prevpage}\n{/if}\n{foreach from=$pagelinks item=page}\n    {$page->link}\n{/foreach}\n{if $pagenumber < $pagecount}\n    &nbsp;{$nextpage}&nbsp;{$lastpage}\n{/if}\n</p>\n<!-- pagination //-->\n{/if}\n\n{if !empty($actionparams.search)}\n    <h2>{$mod->ModLang(\'searchresultsfor\')} &quot;{$actionparams.search}&quot;</h2>\n{/if}\n\n<ul>\n{foreach from=$items item=item}\n    <li class=\"item searchresult\">\n        <a href=\"{$item->url}\">{$item->title}</a>\n    </li>\n{/foreach}\n</ul>\n\n{/if}', '2015-04-24 18:58:43', '2015-04-24 18:58:43');
INSERT INTO `cms_module_templates` VALUES ('ListIt2AboutUs', 'detail_default', 'aaa', '2015-04-24 19:08:08', '2015-04-24 19:08:08');
INSERT INTO `cms_module_templates` VALUES ('ListIt2AboutUs', 'search_default', '<div id=\"{$modulealias}_search\">\n	{$formstart}\n		<label for=\"{$modulealias}searchinput\">{$mod->ModLang(\'searchfor\')}:</label>&nbsp;\n		<input type=\"text\" id=\"{$modulealias}searchinput\" name=\"{$actionid}search\" size=\"20\" maxlength=\"50\" value=\"\" />\n		<input class=\"search-button\" name=\"submit\" value=\"{$mod->ModLang(\'search\')}\" type=\"submit\" />\n	{$formend}\n</div>', '2015-04-24 18:58:43', '2015-04-24 18:58:43');
INSERT INTO `cms_module_templates` VALUES ('ListIt2AboutUs', 'search_filter', '<div id=\"{$modulealias}_filter\">\n\n	<h3>{$filterprompt}</h3>\n\n	{$formstart}\n\n		{foreach from=$fielddefs item=fielddef}\n		<div class=\"form-row\">\n			\n		{if $fielddef.type != \'Categories\'}\n			<label for=\"filter_{$fielddef->alias}\">{$fielddef->name}</label>\n			\n			{if $fielddef.type == \'Checkbox\'}\n				<input type=\"checkbox\" name=\"{$actionid}search_{$fielddef->alias}\" id=\"filter_{$fielddef->alias}\" value=\"{$fielddef->value}\" />\n			{else}\n			\n			<select name=\"{$actionid}search_{$fielddef->alias}\" id=\"filter_{$fielddef->alias}\">\n				<option value=\'\'>{$mod->ModLang(\'all\')}</option>\n				{foreach from=$fielddef->values item=value}\n				<option>{$value}</option>\n				{/foreach}\n			</select>\n			{/if}\n			\n		{/if}\n			\n		</div>\n		{/foreach}\n	\n		<input class=\"search-button\" name=\"submit\" value=\"{$mod->ModLang(\'search\')}\" type=\"submit\" />\n\n	{$formend}\n\n</div>', '2015-04-24 18:58:43', '2015-04-24 18:58:43');
INSERT INTO `cms_module_templates` VALUES ('ListIt2AboutUs', 'search_multiselect', '<div id=\"{$modulealias}_filter\">\n\n	<h3>{$filterprompt}</h3>\n\n	{$formstart}\n\n		{foreach from=$fielddefs item=fielddef}\n		<div class=\"filter form-row\">\n\n		{if $fielddef.type != \'Categories\'}\n		\n			<label>{$fielddef->name}</label><br />\n			{if $fielddef.type == \'Checkbox\'}\n				\n				<input type=\"checkbox\" name=\"{$actionid}search_{$fielddef->alias}\" id=\"filter_{$fielddef->alias}\" value=\"{$fielddef->value}\" />\n			{else}\n				\n				<select name=\"{$actionid}search_{$fielddef->alias}[]\" size=\"5\" multiple=\"multiple\">\n					{foreach from=$fielddef->values item=\'value\'}\n					<option value=\"{$value}\">{$value}</option>\n					{/foreach}\n				</select>\n			{/if}\n		{/if}\n		</div>\n		{/foreach}\n\n		<input class=\"search-button\" name=\"submit\" value=\"{$mod->ModLang(\'filter\')}\" type=\"submit\" />\n\n	{$formend}\n</div>', '2015-04-24 18:58:43', '2015-04-24 18:58:43');
INSERT INTO `cms_module_templates` VALUES ('ListIt2AboutUs', 'category_default', '<!-- categories -->\n<ul>\n{foreach from=$categories item=category}\n	<li class=\"category-{$category->alias}\">\n		<a class=\"category-name\" href=\"{$category->url}\">{$category->name} ({$category->items|count})</a>\n			{if !empty($category->description)}\n			<div class=\"category-description\">\n				{eval var=$category->description}\n			</div>\n			{/if}\n	</li>\n{/foreach}\n<ul>\n<!-- categories //-->', '2015-04-24 18:58:43', '2015-04-24 18:58:43');
INSERT INTO `cms_module_templates` VALUES ('ListIt2AboutUs', 'category_hierarchy', '<!-- categories hierarchy -->\n<ul class=\"categories\">\n{foreach from=$categories item=category}\n\n{if $category->depth > $category->prevdepth}\n	{repeat string=\"<ul>\" times=$category->depth-$category->prevdepth}\n{elseif $category->depth < $category->prevdepth}\n	{repeat string=\"</li></ul>\" times=$category->prevdepth-$category->depth}\n	</li>\n{elseif $category->index > 0}\n	</li>\n{/if}\n\n{if $category->current}\n<li class=\"category-{$category->alias} current{if $category->parent == true || $category->children|@count > 0} parent{/if}\">\n	<a href=\"{$category->url}\">{$category->menutext}</a>\n{else}\n<li class=\"category-{$category->alias}{if $category->parent == true || $category->children|@count > 0} parent{/if}\">\n	<a href=\"{$category->url}\">{$category->menutext}</a>\n{/if}\n\n{/foreach}\n\n	{repeat string=\"</li></ul>\" times=$category->depth-1}\n	</li>\n</ul>\n<!-- categories hierarchy //-->', '2015-04-24 18:58:43', '2015-04-24 18:58:43');
INSERT INTO `cms_module_templates` VALUES ('ListIt2AboutUs', 'archive_default', '{if $archives|@count > 0}\n\n    <!-- archives -->\n    <ul>\n        {foreach from=$archives item=\'archive\'}\n            <li><a href=\"{$archive->url}\">{$archive->month}/{$archive->year} ({$archive->count})</a> [{$archive->timestamp|cms_date_format:\'%B %Y\'}]</li>    \n        {/foreach}\n    </ul>\n    <!-- archives //-->\n    \n{/if}', '2015-04-24 18:58:43', '2015-04-24 18:58:43');
INSERT INTO `cms_module_templates` VALUES ('ListIt2Testimonials', 'summary_searchresults', '{if $items|@count > 0}\n\n{if $pagecount > 1}\n<!-- pagination -->\n<p>\n{if $pagenumber > 1}\n    {$firstpage}&nbsp;{$prevpage}\n{/if}\n{foreach from=$pagelinks item=page}\n    {$page->link}\n{/foreach}\n{if $pagenumber < $pagecount}\n    &nbsp;{$nextpage}&nbsp;{$lastpage}\n{/if}\n</p>\n<!-- pagination //-->\n{/if}\n\n{if !empty($actionparams.search)}\n    <h2>{$mod->ModLang(\'searchresultsfor\')} &quot;{$actionparams.search}&quot;</h2>\n{/if}\n\n<ul>\n{foreach from=$items item=item}\n    <li class=\"item searchresult\">\n        <a href=\"{$item->url}\">{$item->title}</a>\n    </li>\n{/foreach}\n</ul>\n\n{/if}', '2015-04-24 19:35:11', '2015-04-24 19:35:11');
INSERT INTO `cms_module_templates` VALUES ('ListIt2Testimonials', 'detail_default', '<!-- item -->\n<div class=\"item\">\n	<h2 class=\"item-title\">{$item->title|cms_escape}</h2>\n\n	{if !empty($item->fielddefs)}\n\n	<div class=\"item-properties\">\n		{foreach from=$item->fielddefs item=fielddef}\n\n		{*\n			Categories were moved to field definitions.\n			If you need Categories, create new Field definition with alias \"category\"\n			and Categories will be available again.\n		*}\n\n		{if $fielddef.type == \'Categories\' && ($fielddef.value != \'\')}\n\n			{* use ListIt2Loader plugin if you need Category information in default module action templates *}\n			{ListIt2Loader item=\'category\' force_array=1 value=$fielddef.value assign=\'cats\'}\n	\n			<!-- categories -->\n			<div class=\"item-category\">\n				Category: {$cats|implode:\',\'}\n			</div>\n			<!-- categories //-->\n			\n		{/if}\n\n		{if $fielddef.value && $fielddef.type != \'Categories\'}\n			{if $fielddef.type == \'SelectFile\' || $fielddef.type == \'FileUpload\'}\n				{$fielddef.name}: <a href=\"{$fielddef->GetImagePath(true)}/{$fielddef.value}\">{$fielddef.value}</a><br />\n		{elseif $fielddef.type == \'SelectDateTime\'}\n					{$fielddef.name}: {$fielddef.value|cms_date_format}<br />\n		{else}\n			{$fielddef.name}: {$fielddef.value}<br />\n			{/if}\n		{/if}\n\n		{/foreach}\n	</div>\n\n	{/if}\n	\n	<a href=\"{$return_url}\" class=\"return-link\">return</a>\n	{* or use {$return_link} to create link tag *}\n	\n</div>\n<!-- item //-->', '2015-04-24 19:35:11', '2015-04-24 19:35:11');
INSERT INTO `cms_module_templates` VALUES ('ListIt2Testimonials', 'search_default', '<div id=\"{$modulealias}_search\">\n	{$formstart}\n		<label for=\"{$modulealias}searchinput\">{$mod->ModLang(\'searchfor\')}:</label>&nbsp;\n		<input type=\"text\" id=\"{$modulealias}searchinput\" name=\"{$actionid}search\" size=\"20\" maxlength=\"50\" value=\"\" />\n		<input class=\"search-button\" name=\"submit\" value=\"{$mod->ModLang(\'search\')}\" type=\"submit\" />\n	{$formend}\n</div>', '2015-04-24 19:35:11', '2015-04-24 19:35:11');
INSERT INTO `cms_module_templates` VALUES ('ListIt2Testimonials', 'search_filter', '<div id=\"{$modulealias}_filter\">\n\n	<h3>{$filterprompt}</h3>\n\n	{$formstart}\n\n		{foreach from=$fielddefs item=fielddef}\n		<div class=\"form-row\">\n			\n		{if $fielddef.type != \'Categories\'}\n			<label for=\"filter_{$fielddef->alias}\">{$fielddef->name}</label>\n			\n			{if $fielddef.type == \'Checkbox\'}\n				<input type=\"checkbox\" name=\"{$actionid}search_{$fielddef->alias}\" id=\"filter_{$fielddef->alias}\" value=\"{$fielddef->value}\" />\n			{else}\n			\n			<select name=\"{$actionid}search_{$fielddef->alias}\" id=\"filter_{$fielddef->alias}\">\n				<option value=\'\'>{$mod->ModLang(\'all\')}</option>\n				{foreach from=$fielddef->values item=value}\n				<option>{$value}</option>\n				{/foreach}\n			</select>\n			{/if}\n			\n		{/if}\n			\n		</div>\n		{/foreach}\n	\n		<input class=\"search-button\" name=\"submit\" value=\"{$mod->ModLang(\'search\')}\" type=\"submit\" />\n\n	{$formend}\n\n</div>', '2015-04-24 19:35:11', '2015-04-24 19:35:11');
INSERT INTO `cms_module_templates` VALUES ('ListIt2Testimonials', 'search_multiselect', '<div id=\"{$modulealias}_filter\">\n\n	<h3>{$filterprompt}</h3>\n\n	{$formstart}\n\n		{foreach from=$fielddefs item=fielddef}\n		<div class=\"filter form-row\">\n\n		{if $fielddef.type != \'Categories\'}\n		\n			<label>{$fielddef->name}</label><br />\n			{if $fielddef.type == \'Checkbox\'}\n				\n				<input type=\"checkbox\" name=\"{$actionid}search_{$fielddef->alias}\" id=\"filter_{$fielddef->alias}\" value=\"{$fielddef->value}\" />\n			{else}\n				\n				<select name=\"{$actionid}search_{$fielddef->alias}[]\" size=\"5\" multiple=\"multiple\">\n					{foreach from=$fielddef->values item=\'value\'}\n					<option value=\"{$value}\">{$value}</option>\n					{/foreach}\n				</select>\n			{/if}\n		{/if}\n		</div>\n		{/foreach}\n\n		<input class=\"search-button\" name=\"submit\" value=\"{$mod->ModLang(\'filter\')}\" type=\"submit\" />\n\n	{$formend}\n</div>', '2015-04-24 19:35:11', '2015-04-24 19:35:11');
INSERT INTO `cms_module_templates` VALUES ('ListIt2Testimonials', 'category_default', '<!-- categories -->\n<ul>\n{foreach from=$categories item=category}\n	<li class=\"category-{$category->alias}\">\n		<a class=\"category-name\" href=\"{$category->url}\">{$category->name} ({$category->items|count})</a>\n			{if !empty($category->description)}\n			<div class=\"category-description\">\n				{eval var=$category->description}\n			</div>\n			{/if}\n	</li>\n{/foreach}\n<ul>\n<!-- categories //-->', '2015-04-24 19:35:11', '2015-04-24 19:35:11');
INSERT INTO `cms_module_templates` VALUES ('ListIt2Testimonials', 'category_hierarchy', '<!-- categories hierarchy -->\n<ul class=\"categories\">\n{foreach from=$categories item=category}\n\n{if $category->depth > $category->prevdepth}\n	{repeat string=\"<ul>\" times=$category->depth-$category->prevdepth}\n{elseif $category->depth < $category->prevdepth}\n	{repeat string=\"</li></ul>\" times=$category->prevdepth-$category->depth}\n	</li>\n{elseif $category->index > 0}\n	</li>\n{/if}\n\n{if $category->current}\n<li class=\"category-{$category->alias} current{if $category->parent == true || $category->children|@count > 0} parent{/if}\">\n	<a href=\"{$category->url}\">{$category->menutext}</a>\n{else}\n<li class=\"category-{$category->alias}{if $category->parent == true || $category->children|@count > 0} parent{/if}\">\n	<a href=\"{$category->url}\">{$category->menutext}</a>\n{/if}\n\n{/foreach}\n\n	{repeat string=\"</li></ul>\" times=$category->depth-1}\n	</li>\n</ul>\n<!-- categories hierarchy //-->', '2015-04-24 19:35:11', '2015-04-24 19:35:11');
INSERT INTO `cms_module_templates` VALUES ('ListIt2Testimonials', 'archive_default', '{if $archives|@count > 0}\n\n    <!-- archives -->\n    <ul>\n        {foreach from=$archives item=\'archive\'}\n            <li><a href=\"{$archive->url}\">{$archive->month}/{$archive->year} ({$archive->count})</a> [{$archive->timestamp|cms_date_format:\'%B %Y\'}]</li>    \n        {/foreach}\n    </ul>\n    <!-- archives //-->\n    \n{/if}', '2015-04-24 19:35:11', '2015-04-24 19:35:11');
INSERT INTO `cms_module_templates` VALUES ('ListIt2Social', 'summary_default', '{if $items|@count > 0}\r\n<ul class=\"single-page-social-icons-list\">\r\n	{foreach from=$items item=item}\r\n			<li><a href=\"{$item->fielddefs.link->value}\" class=\"header-social-icon-tooltip\" title=\"{$item->title}!\"><i class=\"fa fa-{$item->fielddefs.sclass->value}\"></i></a></li>\r\n	{/foreach}\r\n</ul>\r\n{/if}', '2015-04-25 17:19:51', '2015-04-25 17:19:51');
INSERT INTO `cms_module_templates` VALUES ('ListIt2Gallery', 'summary_searchresults', '{if $items|@count > 0}\n\n{if $pagecount > 1}\n<!-- pagination -->\n<p>\n{if $pagenumber > 1}\n    {$firstpage}&nbsp;{$prevpage}\n{/if}\n{foreach from=$pagelinks item=page}\n    {$page->link}\n{/foreach}\n{if $pagenumber < $pagecount}\n    &nbsp;{$nextpage}&nbsp;{$lastpage}\n{/if}\n</p>\n<!-- pagination //-->\n{/if}\n\n{if !empty($actionparams.search)}\n    <h2>{$mod->ModLang(\'searchresultsfor\')} &quot;{$actionparams.search}&quot;</h2>\n{/if}\n\n<ul>\n{foreach from=$items item=item}\n    <li class=\"item searchresult\">\n        <a href=\"{$item->url}\">{$item->title}</a>\n    </li>\n{/foreach}\n</ul>\n\n{/if}', '2015-04-25 20:00:35', '2015-04-25 20:00:35');
INSERT INTO `cms_module_templates` VALUES ('ListIt2Gallery', 'detail_default', '<!-- item -->\n<div class=\"item\">\n	<h2 class=\"item-title\">{$item->title|cms_escape}</h2>\n\n	{if !empty($item->fielddefs)}\n\n	<div class=\"item-properties\">\n		{foreach from=$item->fielddefs item=fielddef}\n\n		{*\n			Categories were moved to field definitions.\n			If you need Categories, create new Field definition with alias \"category\"\n			and Categories will be available again.\n		*}\n\n		{if $fielddef.type == \'Categories\' && ($fielddef.value != \'\')}\n\n			{* use ListIt2Loader plugin if you need Category information in default module action templates *}\n			{ListIt2Loader item=\'category\' force_array=1 value=$fielddef.value assign=\'cats\'}\n	\n			<!-- categories -->\n			<div class=\"item-category\">\n				Category: {$cats|implode:\',\'}\n			</div>\n			<!-- categories //-->\n			\n		{/if}\n\n		{if $fielddef.value && $fielddef.type != \'Categories\'}\n			{if $fielddef.type == \'SelectFile\' || $fielddef.type == \'FileUpload\'}\n				{$fielddef.name}: <a href=\"{$fielddef->GetImagePath(true)}/{$fielddef.value}\">{$fielddef.value}</a><br />\n		{elseif $fielddef.type == \'SelectDateTime\'}\n					{$fielddef.name}: {$fielddef.value|cms_date_format}<br />\n		{else}\n			{$fielddef.name}: {$fielddef.value}<br />\n			{/if}\n		{/if}\n\n		{/foreach}\n	</div>\n\n	{/if}\n	\n	<a href=\"{$return_url}\" class=\"return-link\">return</a>\n	{* or use {$return_link} to create link tag *}\n	\n</div>\n<!-- item //-->', '2015-04-25 20:00:35', '2015-04-25 20:00:35');
INSERT INTO `cms_module_templates` VALUES ('ListIt2Social', 'summary_searchresults', '{if $items|@count > 0}\n\n{if $pagecount > 1}\n<!-- pagination -->\n<p>\n{if $pagenumber > 1}\n    {$firstpage}&nbsp;{$prevpage}\n{/if}\n{foreach from=$pagelinks item=page}\n    {$page->link}\n{/foreach}\n{if $pagenumber < $pagecount}\n    &nbsp;{$nextpage}&nbsp;{$lastpage}\n{/if}\n</p>\n<!-- pagination //-->\n{/if}\n\n{if !empty($actionparams.search)}\n    <h2>{$mod->ModLang(\'searchresultsfor\')} &quot;{$actionparams.search}&quot;</h2>\n{/if}\n\n<ul>\n{foreach from=$items item=item}\n    <li class=\"item searchresult\">\n        <a href=\"{$item->url}\">{$item->title}</a>\n    </li>\n{/foreach}\n</ul>\n\n{/if}', '2015-04-25 17:14:04', '2015-04-25 17:14:04');
INSERT INTO `cms_module_templates` VALUES ('ListIt2Social', 'detail_default', '<!-- item -->\n<div class=\"item\">\n	<h2 class=\"item-title\">{$item->title|cms_escape}</h2>\n\n	{if !empty($item->fielddefs)}\n\n	<div class=\"item-properties\">\n		{foreach from=$item->fielddefs item=fielddef}\n\n		{*\n			Categories were moved to field definitions.\n			If you need Categories, create new Field definition with alias \"category\"\n			and Categories will be available again.\n		*}\n\n		{if $fielddef.type == \'Categories\' && ($fielddef.value != \'\')}\n\n			{* use ListIt2Loader plugin if you need Category information in default module action templates *}\n			{ListIt2Loader item=\'category\' force_array=1 value=$fielddef.value assign=\'cats\'}\n	\n			<!-- categories -->\n			<div class=\"item-category\">\n				Category: {$cats|implode:\',\'}\n			</div>\n			<!-- categories //-->\n			\n		{/if}\n\n		{if $fielddef.value && $fielddef.type != \'Categories\'}\n			{if $fielddef.type == \'SelectFile\' || $fielddef.type == \'FileUpload\'}\n				{$fielddef.name}: <a href=\"{$fielddef->GetImagePath(true)}/{$fielddef.value}\">{$fielddef.value}</a><br />\n		{elseif $fielddef.type == \'SelectDateTime\'}\n					{$fielddef.name}: {$fielddef.value|cms_date_format}<br />\n		{else}\n			{$fielddef.name}: {$fielddef.value}<br />\n			{/if}\n		{/if}\n\n		{/foreach}\n	</div>\n\n	{/if}\n	\n	<a href=\"{$return_url}\" class=\"return-link\">return</a>\n	{* or use {$return_link} to create link tag *}\n	\n</div>\n<!-- item //-->', '2015-04-25 17:14:04', '2015-04-25 17:14:04');
INSERT INTO `cms_module_templates` VALUES ('ListIt2Social', 'search_default', '<div id=\"{$modulealias}_search\">\n	{$formstart}\n		<label for=\"{$modulealias}searchinput\">{$mod->ModLang(\'searchfor\')}:</label>&nbsp;\n		<input type=\"text\" id=\"{$modulealias}searchinput\" name=\"{$actionid}search\" size=\"20\" maxlength=\"50\" value=\"\" />\n		<input class=\"search-button\" name=\"submit\" value=\"{$mod->ModLang(\'search\')}\" type=\"submit\" />\n	{$formend}\n</div>', '2015-04-25 17:14:04', '2015-04-25 17:14:04');
INSERT INTO `cms_module_templates` VALUES ('ListIt2Social', 'search_filter', '<div id=\"{$modulealias}_filter\">\n\n	<h3>{$filterprompt}</h3>\n\n	{$formstart}\n\n		{foreach from=$fielddefs item=fielddef}\n		<div class=\"form-row\">\n			\n		{if $fielddef.type != \'Categories\'}\n			<label for=\"filter_{$fielddef->alias}\">{$fielddef->name}</label>\n			\n			{if $fielddef.type == \'Checkbox\'}\n				<input type=\"checkbox\" name=\"{$actionid}search_{$fielddef->alias}\" id=\"filter_{$fielddef->alias}\" value=\"{$fielddef->value}\" />\n			{else}\n			\n			<select name=\"{$actionid}search_{$fielddef->alias}\" id=\"filter_{$fielddef->alias}\">\n				<option value=\'\'>{$mod->ModLang(\'all\')}</option>\n				{foreach from=$fielddef->values item=value}\n				<option>{$value}</option>\n				{/foreach}\n			</select>\n			{/if}\n			\n		{/if}\n			\n		</div>\n		{/foreach}\n	\n		<input class=\"search-button\" name=\"submit\" value=\"{$mod->ModLang(\'search\')}\" type=\"submit\" />\n\n	{$formend}\n\n</div>', '2015-04-25 17:14:04', '2015-04-25 17:14:04');
INSERT INTO `cms_module_templates` VALUES ('ListIt2Social', 'search_multiselect', '<div id=\"{$modulealias}_filter\">\n\n	<h3>{$filterprompt}</h3>\n\n	{$formstart}\n\n		{foreach from=$fielddefs item=fielddef}\n		<div class=\"filter form-row\">\n\n		{if $fielddef.type != \'Categories\'}\n		\n			<label>{$fielddef->name}</label><br />\n			{if $fielddef.type == \'Checkbox\'}\n				\n				<input type=\"checkbox\" name=\"{$actionid}search_{$fielddef->alias}\" id=\"filter_{$fielddef->alias}\" value=\"{$fielddef->value}\" />\n			{else}\n				\n				<select name=\"{$actionid}search_{$fielddef->alias}[]\" size=\"5\" multiple=\"multiple\">\n					{foreach from=$fielddef->values item=\'value\'}\n					<option value=\"{$value}\">{$value}</option>\n					{/foreach}\n				</select>\n			{/if}\n		{/if}\n		</div>\n		{/foreach}\n\n		<input class=\"search-button\" name=\"submit\" value=\"{$mod->ModLang(\'filter\')}\" type=\"submit\" />\n\n	{$formend}\n</div>', '2015-04-25 17:14:04', '2015-04-25 17:14:04');
INSERT INTO `cms_module_templates` VALUES ('ListIt2Social', 'category_default', '<!-- categories -->\n<ul>\n{foreach from=$categories item=category}\n	<li class=\"category-{$category->alias}\">\n		<a class=\"category-name\" href=\"{$category->url}\">{$category->name} ({$category->items|count})</a>\n			{if !empty($category->description)}\n			<div class=\"category-description\">\n				{eval var=$category->description}\n			</div>\n			{/if}\n	</li>\n{/foreach}\n<ul>\n<!-- categories //-->', '2015-04-25 17:14:04', '2015-04-25 17:14:04');
INSERT INTO `cms_module_templates` VALUES ('ListIt2Social', 'category_hierarchy', '<!-- categories hierarchy -->\n<ul class=\"categories\">\n{foreach from=$categories item=category}\n\n{if $category->depth > $category->prevdepth}\n	{repeat string=\"<ul>\" times=$category->depth-$category->prevdepth}\n{elseif $category->depth < $category->prevdepth}\n	{repeat string=\"</li></ul>\" times=$category->prevdepth-$category->depth}\n	</li>\n{elseif $category->index > 0}\n	</li>\n{/if}\n\n{if $category->current}\n<li class=\"category-{$category->alias} current{if $category->parent == true || $category->children|@count > 0} parent{/if}\">\n	<a href=\"{$category->url}\">{$category->menutext}</a>\n{else}\n<li class=\"category-{$category->alias}{if $category->parent == true || $category->children|@count > 0} parent{/if}\">\n	<a href=\"{$category->url}\">{$category->menutext}</a>\n{/if}\n\n{/foreach}\n\n	{repeat string=\"</li></ul>\" times=$category->depth-1}\n	</li>\n</ul>\n<!-- categories hierarchy //-->', '2015-04-25 17:14:04', '2015-04-25 17:14:04');
INSERT INTO `cms_module_templates` VALUES ('ListIt2Social', 'archive_default', '{if $archives|@count > 0}\n\n    <!-- archives -->\n    <ul>\n        {foreach from=$archives item=\'archive\'}\n            <li><a href=\"{$archive->url}\">{$archive->month}/{$archive->year} ({$archive->count})</a> [{$archive->timestamp|cms_date_format:\'%B %Y\'}]</li>    \n        {/foreach}\n    </ul>\n    <!-- archives //-->\n    \n{/if}', '2015-04-25 17:14:04', '2015-04-25 17:14:04');
INSERT INTO `cms_module_templates` VALUES ('ListIt2Menu', 'summary_searchresults', '{if $items|@count > 0}\n\n{if $pagecount > 1}\n<!-- pagination -->\n<p>\n{if $pagenumber > 1}\n    {$firstpage}&nbsp;{$prevpage}\n{/if}\n{foreach from=$pagelinks item=page}\n    {$page->link}\n{/foreach}\n{if $pagenumber < $pagecount}\n    &nbsp;{$nextpage}&nbsp;{$lastpage}\n{/if}\n</p>\n<!-- pagination //-->\n{/if}\n\n{if !empty($actionparams.search)}\n    <h2>{$mod->ModLang(\'searchresultsfor\')} &quot;{$actionparams.search}&quot;</h2>\n{/if}\n\n<ul>\n{foreach from=$items item=item}\n    <li class=\"item searchresult\">\n        <a href=\"{$item->url}\">{$item->title}</a>\n    </li>\n{/foreach}\n</ul>\n\n{/if}', '2015-04-25 17:21:17', '2015-04-25 17:21:17');
INSERT INTO `cms_module_templates` VALUES ('ListIt2Menu', 'detail_default', '<!-- item -->\n<div class=\"item\">\n	<h2 class=\"item-title\">{$item->title|cms_escape}</h2>\n\n	{if !empty($item->fielddefs)}\n\n	<div class=\"item-properties\">\n		{foreach from=$item->fielddefs item=fielddef}\n\n		{*\n			Categories were moved to field definitions.\n			If you need Categories, create new Field definition with alias \"category\"\n			and Categories will be available again.\n		*}\n\n		{if $fielddef.type == \'Categories\' && ($fielddef.value != \'\')}\n\n			{* use ListIt2Loader plugin if you need Category information in default module action templates *}\n			{ListIt2Loader item=\'category\' force_array=1 value=$fielddef.value assign=\'cats\'}\n	\n			<!-- categories -->\n			<div class=\"item-category\">\n				Category: {$cats|implode:\',\'}\n			</div>\n			<!-- categories //-->\n			\n		{/if}\n\n		{if $fielddef.value && $fielddef.type != \'Categories\'}\n			{if $fielddef.type == \'SelectFile\' || $fielddef.type == \'FileUpload\'}\n				{$fielddef.name}: <a href=\"{$fielddef->GetImagePath(true)}/{$fielddef.value}\">{$fielddef.value}</a><br />\n		{elseif $fielddef.type == \'SelectDateTime\'}\n					{$fielddef.name}: {$fielddef.value|cms_date_format}<br />\n		{else}\n			{$fielddef.name}: {$fielddef.value}<br />\n			{/if}\n		{/if}\n\n		{/foreach}\n	</div>\n\n	{/if}\n	\n	<a href=\"{$return_url}\" class=\"return-link\">return</a>\n	{* or use {$return_link} to create link tag *}\n	\n</div>\n<!-- item //-->', '2015-04-25 17:21:17', '2015-04-25 17:21:17');
INSERT INTO `cms_module_templates` VALUES ('ListIt2Menu', 'search_default', '<div id=\"{$modulealias}_search\">\n	{$formstart}\n		<label for=\"{$modulealias}searchinput\">{$mod->ModLang(\'searchfor\')}:</label>&nbsp;\n		<input type=\"text\" id=\"{$modulealias}searchinput\" name=\"{$actionid}search\" size=\"20\" maxlength=\"50\" value=\"\" />\n		<input class=\"search-button\" name=\"submit\" value=\"{$mod->ModLang(\'search\')}\" type=\"submit\" />\n	{$formend}\n</div>', '2015-04-25 17:21:17', '2015-04-25 17:21:17');
INSERT INTO `cms_module_templates` VALUES ('ListIt2Menu', 'search_filter', '<div id=\"{$modulealias}_filter\">\n\n	<h3>{$filterprompt}</h3>\n\n	{$formstart}\n\n		{foreach from=$fielddefs item=fielddef}\n		<div class=\"form-row\">\n			\n		{if $fielddef.type != \'Categories\'}\n			<label for=\"filter_{$fielddef->alias}\">{$fielddef->name}</label>\n			\n			{if $fielddef.type == \'Checkbox\'}\n				<input type=\"checkbox\" name=\"{$actionid}search_{$fielddef->alias}\" id=\"filter_{$fielddef->alias}\" value=\"{$fielddef->value}\" />\n			{else}\n			\n			<select name=\"{$actionid}search_{$fielddef->alias}\" id=\"filter_{$fielddef->alias}\">\n				<option value=\'\'>{$mod->ModLang(\'all\')}</option>\n				{foreach from=$fielddef->values item=value}\n				<option>{$value}</option>\n				{/foreach}\n			</select>\n			{/if}\n			\n		{/if}\n			\n		</div>\n		{/foreach}\n	\n		<input class=\"search-button\" name=\"submit\" value=\"{$mod->ModLang(\'search\')}\" type=\"submit\" />\n\n	{$formend}\n\n</div>', '2015-04-25 17:21:17', '2015-04-25 17:21:17');
INSERT INTO `cms_module_templates` VALUES ('ListIt2Menu', 'search_multiselect', '<div id=\"{$modulealias}_filter\">\n\n	<h3>{$filterprompt}</h3>\n\n	{$formstart}\n\n		{foreach from=$fielddefs item=fielddef}\n		<div class=\"filter form-row\">\n\n		{if $fielddef.type != \'Categories\'}\n		\n			<label>{$fielddef->name}</label><br />\n			{if $fielddef.type == \'Checkbox\'}\n				\n				<input type=\"checkbox\" name=\"{$actionid}search_{$fielddef->alias}\" id=\"filter_{$fielddef->alias}\" value=\"{$fielddef->value}\" />\n			{else}\n				\n				<select name=\"{$actionid}search_{$fielddef->alias}[]\" size=\"5\" multiple=\"multiple\">\n					{foreach from=$fielddef->values item=\'value\'}\n					<option value=\"{$value}\">{$value}</option>\n					{/foreach}\n				</select>\n			{/if}\n		{/if}\n		</div>\n		{/foreach}\n\n		<input class=\"search-button\" name=\"submit\" value=\"{$mod->ModLang(\'filter\')}\" type=\"submit\" />\n\n	{$formend}\n</div>', '2015-04-25 17:21:17', '2015-04-25 17:21:17');
INSERT INTO `cms_module_templates` VALUES ('ListIt2Menu', 'category_hierarchy', '<!-- categories hierarchy -->\n<ul class=\"categories\">\n{foreach from=$categories item=category}\n\n{if $category->depth > $category->prevdepth}\n	{repeat string=\"<ul>\" times=$category->depth-$category->prevdepth}\n{elseif $category->depth < $category->prevdepth}\n	{repeat string=\"</li></ul>\" times=$category->prevdepth-$category->depth}\n	</li>\n{elseif $category->index > 0}\n	</li>\n{/if}\n\n{if $category->current}\n<li class=\"category-{$category->alias} current{if $category->parent == true || $category->children|@count > 0} parent{/if}\">\n	<a href=\"{$category->url}\">{$category->menutext}</a>\n{else}\n<li class=\"category-{$category->alias}{if $category->parent == true || $category->children|@count > 0} parent{/if}\">\n	<a href=\"{$category->url}\">{$category->menutext}</a>\n{/if}\n\n{/foreach}\n\n	{repeat string=\"</li></ul>\" times=$category->depth-1}\n	</li>\n</ul>\n<!-- categories hierarchy //-->', '2015-04-25 17:21:17', '2015-04-25 17:21:17');
INSERT INTO `cms_module_templates` VALUES ('ListIt2Menu', 'archive_default', '{if $archives|@count > 0}\n\n    <!-- archives -->\n    <ul>\n        {foreach from=$archives item=\'archive\'}\n            <li><a href=\"{$archive->url}\">{$archive->month}/{$archive->year} ({$archive->count})</a> [{$archive->timestamp|cms_date_format:\'%B %Y\'}]</li>    \n        {/foreach}\n    </ul>\n    <!-- archives //-->\n    \n{/if}', '2015-04-25 17:21:17', '2015-04-25 17:21:17');
INSERT INTO `cms_module_templates` VALUES ('ListIt2Gallery', 'summary_default', '{if $items|@count > 0}\r\n	{foreach from=$items item=item}\r\n<div class=\"hungry-gallery-item {if $item->fielddefs.wide->value}wide{/if} wow fadeIn\" data-wow-duration=\"2s\" data-wow-offset=\"250\">\r\n						<a class=\"image-hover {if $item->fielddefs.wide->value}wide{/if} lightbox-gallery\" href=\"uploads/gallery/{$item->fielddefs.image->value}\">\r\n							<img src=\"uploads/gallery/{$item->fielddefs.image->value}\" alt=\"{$item->title}\" />\r\n							<div class=\"image-hover-overlay\">\r\n								<i class=\"fa fa-search-plus\"></i>\r\n							</div>\r\n						</a>\r\n					</div>\r\n	{/foreach}\r\n{/if}', '2015-04-27 08:47:12', '2015-04-27 08:47:12');
INSERT INTO `cms_module_templates` VALUES ('ListIt2Menu', 'category_default', '{foreach from=$categories item=category name=foo}\r\n<div class=\"{if $smarty.foreach.foo.index % 2 == 0}grid-45 suffix-5{else}prefix-5 grid-45{/if} tablet-prefix-5 tablet-grid-90 tablet-suffix-5 mobile-grid-100\">\r\n<div class=\"hungry-menu wow zoomIn\" data-wow-duration=\"2s\" data-wow-offset=\"250\">\r\n<h2 class=\"hungry-menu-title header-divider\">{$category->name} ({$category->items|count})</h2>\r\n{ListIt2Menu category=$category->alias}\r\n</div>\r\n</div>\r\n<div class=\"clear:both;\"></div>\r\n{/foreach}', '2015-05-01 01:19:40', '2015-05-01 01:19:40');
INSERT INTO `cms_module_templates` VALUES ('ListIt2Gallery', 'search_default', '<div id=\"{$modulealias}_search\">\n	{$formstart}\n		<label for=\"{$modulealias}searchinput\">{$mod->ModLang(\'searchfor\')}:</label>&nbsp;\n		<input type=\"text\" id=\"{$modulealias}searchinput\" name=\"{$actionid}search\" size=\"20\" maxlength=\"50\" value=\"\" />\n		<input class=\"search-button\" name=\"submit\" value=\"{$mod->ModLang(\'search\')}\" type=\"submit\" />\n	{$formend}\n</div>', '2015-04-25 20:00:35', '2015-04-25 20:00:35');
INSERT INTO `cms_module_templates` VALUES ('ListIt2Gallery', 'search_filter', '<div id=\"{$modulealias}_filter\">\n\n	<h3>{$filterprompt}</h3>\n\n	{$formstart}\n\n		{foreach from=$fielddefs item=fielddef}\n		<div class=\"form-row\">\n			\n		{if $fielddef.type != \'Categories\'}\n			<label for=\"filter_{$fielddef->alias}\">{$fielddef->name}</label>\n			\n			{if $fielddef.type == \'Checkbox\'}\n				<input type=\"checkbox\" name=\"{$actionid}search_{$fielddef->alias}\" id=\"filter_{$fielddef->alias}\" value=\"{$fielddef->value}\" />\n			{else}\n			\n			<select name=\"{$actionid}search_{$fielddef->alias}\" id=\"filter_{$fielddef->alias}\">\n				<option value=\'\'>{$mod->ModLang(\'all\')}</option>\n				{foreach from=$fielddef->values item=value}\n				<option>{$value}</option>\n				{/foreach}\n			</select>\n			{/if}\n			\n		{/if}\n			\n		</div>\n		{/foreach}\n	\n		<input class=\"search-button\" name=\"submit\" value=\"{$mod->ModLang(\'search\')}\" type=\"submit\" />\n\n	{$formend}\n\n</div>', '2015-04-25 20:00:35', '2015-04-25 20:00:35');
INSERT INTO `cms_module_templates` VALUES ('ListIt2Gallery', 'search_multiselect', '<div id=\"{$modulealias}_filter\">\n\n	<h3>{$filterprompt}</h3>\n\n	{$formstart}\n\n		{foreach from=$fielddefs item=fielddef}\n		<div class=\"filter form-row\">\n\n		{if $fielddef.type != \'Categories\'}\n		\n			<label>{$fielddef->name}</label><br />\n			{if $fielddef.type == \'Checkbox\'}\n				\n				<input type=\"checkbox\" name=\"{$actionid}search_{$fielddef->alias}\" id=\"filter_{$fielddef->alias}\" value=\"{$fielddef->value}\" />\n			{else}\n				\n				<select name=\"{$actionid}search_{$fielddef->alias}[]\" size=\"5\" multiple=\"multiple\">\n					{foreach from=$fielddef->values item=\'value\'}\n					<option value=\"{$value}\">{$value}</option>\n					{/foreach}\n				</select>\n			{/if}\n		{/if}\n		</div>\n		{/foreach}\n\n		<input class=\"search-button\" name=\"submit\" value=\"{$mod->ModLang(\'filter\')}\" type=\"submit\" />\n\n	{$formend}\n</div>', '2015-04-25 20:00:35', '2015-04-25 20:00:35');
INSERT INTO `cms_module_templates` VALUES ('ListIt2Gallery', 'category_default', '<!-- categories -->\n<ul>\n{foreach from=$categories item=category}\n	<li class=\"category-{$category->alias}\">\n		<a class=\"category-name\" href=\"{$category->url}\">{$category->name} ({$category->items|count})</a>\n			{if !empty($category->description)}\n			<div class=\"category-description\">\n				{eval var=$category->description}\n			</div>\n			{/if}\n	</li>\n{/foreach}\n<ul>\n<!-- categories //-->', '2015-04-25 20:00:35', '2015-04-25 20:00:35');
INSERT INTO `cms_module_templates` VALUES ('ListIt2Gallery', 'category_hierarchy', '<!-- categories hierarchy -->\n<ul class=\"categories\">\n{foreach from=$categories item=category}\n\n{if $category->depth > $category->prevdepth}\n	{repeat string=\"<ul>\" times=$category->depth-$category->prevdepth}\n{elseif $category->depth < $category->prevdepth}\n	{repeat string=\"</li></ul>\" times=$category->prevdepth-$category->depth}\n	</li>\n{elseif $category->index > 0}\n	</li>\n{/if}\n\n{if $category->current}\n<li class=\"category-{$category->alias} current{if $category->parent == true || $category->children|@count > 0} parent{/if}\">\n	<a href=\"{$category->url}\">{$category->menutext}</a>\n{else}\n<li class=\"category-{$category->alias}{if $category->parent == true || $category->children|@count > 0} parent{/if}\">\n	<a href=\"{$category->url}\">{$category->menutext}</a>\n{/if}\n\n{/foreach}\n\n	{repeat string=\"</li></ul>\" times=$category->depth-1}\n	</li>\n</ul>\n<!-- categories hierarchy //-->', '2015-04-25 20:00:35', '2015-04-25 20:00:35');
INSERT INTO `cms_module_templates` VALUES ('ListIt2Gallery', 'archive_default', '{if $archives|@count > 0}\n\n    <!-- archives -->\n    <ul>\n        {foreach from=$archives item=\'archive\'}\n            <li><a href=\"{$archive->url}\">{$archive->month}/{$archive->year} ({$archive->count})</a> [{$archive->timestamp|cms_date_format:\'%B %Y\'}]</li>    \n        {/foreach}\n    </ul>\n    <!-- archives //-->\n    \n{/if}', '2015-04-25 20:00:35', '2015-04-25 20:00:35');
INSERT INTO `cms_module_templates` VALUES ('MleCMS', 'snippet_customer-say', '{\"vi\":\"C\\u1ea3m nh\\u1eadn c\\u1ee7a Qu\\u00fd kh\\u00e1ch h\\u00e0ng !!<br\\/><i>Khoa Ngan c\\u1eeda h\\u00e0ng ch\\u00fang t\\u00f4i lu\\u00f4n s\\u1eb5n s\\u00e0ng l\\u1eafng nghe Qu\\u00fd kh\\u00e1ch<\\/i>\",\"en\":\"\"}', '2015-04-27 07:33:21', '2015-04-27 08:13:28');
INSERT INTO `cms_module_templates` VALUES ('ListIt2Staff', 'summary_searchresults', '{if $items|@count > 0}\n\n{if $pagecount > 1}\n<!-- pagination -->\n<p>\n{if $pagenumber > 1}\n    {$firstpage}&nbsp;{$prevpage}\n{/if}\n{foreach from=$pagelinks item=page}\n    {$page->link}\n{/foreach}\n{if $pagenumber < $pagecount}\n    &nbsp;{$nextpage}&nbsp;{$lastpage}\n{/if}\n</p>\n<!-- pagination //-->\n{/if}\n\n{if !empty($actionparams.search)}\n    <h2>{$mod->ModLang(\'searchresultsfor\')} &quot;{$actionparams.search}&quot;</h2>\n{/if}\n\n<ul>\n{foreach from=$items item=item}\n    <li class=\"item searchresult\">\n        <a href=\"{$item->url}\">{$item->title}</a>\n    </li>\n{/foreach}\n</ul>\n\n{/if}', '2015-04-27 07:52:51', '2015-04-27 07:52:51');
INSERT INTO `cms_module_templates` VALUES ('ListIt2Staff', 'detail_default', '<!-- item -->\n<div class=\"item\">\n	<h2 class=\"item-title\">{$item->title|cms_escape}</h2>\n\n	{if !empty($item->fielddefs)}\n\n	<div class=\"item-properties\">\n		{foreach from=$item->fielddefs item=fielddef}\n\n		{*\n			Categories were moved to field definitions.\n			If you need Categories, create new Field definition with alias \"category\"\n			and Categories will be available again.\n		*}\n\n		{if $fielddef.type == \'Categories\' && ($fielddef.value != \'\')}\n\n			{* use ListIt2Loader plugin if you need Category information in default module action templates *}\n			{ListIt2Loader item=\'category\' force_array=1 value=$fielddef.value assign=\'cats\'}\n	\n			<!-- categories -->\n			<div class=\"item-category\">\n				Category: {$cats|implode:\',\'}\n			</div>\n			<!-- categories //-->\n			\n		{/if}\n\n		{if $fielddef.value && $fielddef.type != \'Categories\'}\n			{if $fielddef.type == \'SelectFile\' || $fielddef.type == \'FileUpload\'}\n				{$fielddef.name}: <a href=\"{$fielddef->GetImagePath(true)}/{$fielddef.value}\">{$fielddef.value}</a><br />\n		{elseif $fielddef.type == \'SelectDateTime\'}\n					{$fielddef.name}: {$fielddef.value|cms_date_format}<br />\n		{else}\n			{$fielddef.name}: {$fielddef.value}<br />\n			{/if}\n		{/if}\n\n		{/foreach}\n	</div>\n\n	{/if}\n	\n	<a href=\"{$return_url}\" class=\"return-link\">return</a>\n	{* or use {$return_link} to create link tag *}\n	\n</div>\n<!-- item //-->', '2015-04-27 07:52:51', '2015-04-27 07:52:51');
INSERT INTO `cms_module_templates` VALUES ('ListIt2Staff', 'search_default', '<div id=\"{$modulealias}_search\">\n	{$formstart}\n		<label for=\"{$modulealias}searchinput\">{$mod->ModLang(\'searchfor\')}:</label>&nbsp;\n		<input type=\"text\" id=\"{$modulealias}searchinput\" name=\"{$actionid}search\" size=\"20\" maxlength=\"50\" value=\"\" />\n		<input class=\"search-button\" name=\"submit\" value=\"{$mod->ModLang(\'search\')}\" type=\"submit\" />\n	{$formend}\n</div>', '2015-04-27 07:52:51', '2015-04-27 07:52:51');
INSERT INTO `cms_module_templates` VALUES ('ListIt2Staff', 'search_filter', '<div id=\"{$modulealias}_filter\">\n\n	<h3>{$filterprompt}</h3>\n\n	{$formstart}\n\n		{foreach from=$fielddefs item=fielddef}\n		<div class=\"form-row\">\n			\n		{if $fielddef.type != \'Categories\'}\n			<label for=\"filter_{$fielddef->alias}\">{$fielddef->name}</label>\n			\n			{if $fielddef.type == \'Checkbox\'}\n				<input type=\"checkbox\" name=\"{$actionid}search_{$fielddef->alias}\" id=\"filter_{$fielddef->alias}\" value=\"{$fielddef->value}\" />\n			{else}\n			\n			<select name=\"{$actionid}search_{$fielddef->alias}\" id=\"filter_{$fielddef->alias}\">\n				<option value=\'\'>{$mod->ModLang(\'all\')}</option>\n				{foreach from=$fielddef->values item=value}\n				<option>{$value}</option>\n				{/foreach}\n			</select>\n			{/if}\n			\n		{/if}\n			\n		</div>\n		{/foreach}\n	\n		<input class=\"search-button\" name=\"submit\" value=\"{$mod->ModLang(\'search\')}\" type=\"submit\" />\n\n	{$formend}\n\n</div>', '2015-04-27 07:52:51', '2015-04-27 07:52:51');
INSERT INTO `cms_module_templates` VALUES ('ListIt2Staff', 'search_multiselect', '<div id=\"{$modulealias}_filter\">\n\n	<h3>{$filterprompt}</h3>\n\n	{$formstart}\n\n		{foreach from=$fielddefs item=fielddef}\n		<div class=\"filter form-row\">\n\n		{if $fielddef.type != \'Categories\'}\n		\n			<label>{$fielddef->name}</label><br />\n			{if $fielddef.type == \'Checkbox\'}\n				\n				<input type=\"checkbox\" name=\"{$actionid}search_{$fielddef->alias}\" id=\"filter_{$fielddef->alias}\" value=\"{$fielddef->value}\" />\n			{else}\n				\n				<select name=\"{$actionid}search_{$fielddef->alias}[]\" size=\"5\" multiple=\"multiple\">\n					{foreach from=$fielddef->values item=\'value\'}\n					<option value=\"{$value}\">{$value}</option>\n					{/foreach}\n				</select>\n			{/if}\n		{/if}\n		</div>\n		{/foreach}\n\n		<input class=\"search-button\" name=\"submit\" value=\"{$mod->ModLang(\'filter\')}\" type=\"submit\" />\n\n	{$formend}\n</div>', '2015-04-27 07:52:51', '2015-04-27 07:52:51');
INSERT INTO `cms_module_templates` VALUES ('ListIt2Staff', 'category_default', '<!-- categories -->\n<ul>\n{foreach from=$categories item=category}\n	<li class=\"category-{$category->alias}\">\n		<a class=\"category-name\" href=\"{$category->url}\">{$category->name} ({$category->items|count})</a>\n			{if !empty($category->description)}\n			<div class=\"category-description\">\n				{eval var=$category->description}\n			</div>\n			{/if}\n	</li>\n{/foreach}\n<ul>\n<!-- categories //-->', '2015-04-27 07:52:51', '2015-04-27 07:52:51');
INSERT INTO `cms_module_templates` VALUES ('ListIt2Staff', 'category_hierarchy', '<!-- categories hierarchy -->\n<ul class=\"categories\">\n{foreach from=$categories item=category}\n\n{if $category->depth > $category->prevdepth}\n	{repeat string=\"<ul>\" times=$category->depth-$category->prevdepth}\n{elseif $category->depth < $category->prevdepth}\n	{repeat string=\"</li></ul>\" times=$category->prevdepth-$category->depth}\n	</li>\n{elseif $category->index > 0}\n	</li>\n{/if}\n\n{if $category->current}\n<li class=\"category-{$category->alias} current{if $category->parent == true || $category->children|@count > 0} parent{/if}\">\n	<a href=\"{$category->url}\">{$category->menutext}</a>\n{else}\n<li class=\"category-{$category->alias}{if $category->parent == true || $category->children|@count > 0} parent{/if}\">\n	<a href=\"{$category->url}\">{$category->menutext}</a>\n{/if}\n\n{/foreach}\n\n	{repeat string=\"</li></ul>\" times=$category->depth-1}\n	</li>\n</ul>\n<!-- categories hierarchy //-->', '2015-04-27 07:52:51', '2015-04-27 07:52:51');
INSERT INTO `cms_module_templates` VALUES ('ListIt2Staff', 'archive_default', '{if $archives|@count > 0}\n\n    <!-- archives -->\n    <ul>\n        {foreach from=$archives item=\'archive\'}\n            <li><a href=\"{$archive->url}\">{$archive->month}/{$archive->year} ({$archive->count})</a> [{$archive->timestamp|cms_date_format:\'%B %Y\'}]</li>    \n        {/foreach}\n    </ul>\n    <!-- archives //-->\n    \n{/if}', '2015-04-27 07:52:51', '2015-04-27 07:52:51');
INSERT INTO `cms_module_templates` VALUES ('ListIt2Slider', 'summary_default', '{if $items|@count > 0}\r\n{foreach from=$items item=item}\r\n<img src=\"uploads/slider/{$item->fielddefs.image->value}\" alt=\"{$item->title}\" />\r\n{/foreach}\r\n{/if}', '2015-04-27 08:22:19', '2015-04-27 08:22:19');
INSERT INTO `cms_module_templates` VALUES ('ListIt2Blog', 'summary_searchresults', '{if $items|@count > 0}\n\n{if $pagecount > 1}\n<!-- pagination -->\n<p>\n{if $pagenumber > 1}\n    {$firstpage}&nbsp;{$prevpage}\n{/if}\n{foreach from=$pagelinks item=page}\n    {$page->link}\n{/foreach}\n{if $pagenumber < $pagecount}\n    &nbsp;{$nextpage}&nbsp;{$lastpage}\n{/if}\n</p>\n<!-- pagination //-->\n{/if}\n\n{if !empty($actionparams.search)}\n    <h2>{$mod->ModLang(\'searchresultsfor\')} &quot;{$actionparams.search}&quot;</h2>\n{/if}\n\n<ul>\n{foreach from=$items item=item}\n    <li class=\"item searchresult\">\n        <a href=\"{$item->url}\">{$item->title}</a>\n    </li>\n{/foreach}\n</ul>\n\n{/if}', '2015-04-27 09:04:17', '2015-04-27 09:04:17');
INSERT INTO `cms_module_templates` VALUES ('ListIt2Blog', 'detail_default', '<!-- item -->\n<div class=\"item\">\n	<h2 class=\"item-title\">{$item->title|cms_escape}</h2>\n\n	{if !empty($item->fielddefs)}\n\n	<div class=\"item-properties\">\n		{foreach from=$item->fielddefs item=fielddef}\n\n		{*\n			Categories were moved to field definitions.\n			If you need Categories, create new Field definition with alias \"category\"\n			and Categories will be available again.\n		*}\n\n		{if $fielddef.type == \'Categories\' && ($fielddef.value != \'\')}\n\n			{* use ListIt2Loader plugin if you need Category information in default module action templates *}\n			{ListIt2Loader item=\'category\' force_array=1 value=$fielddef.value assign=\'cats\'}\n	\n			<!-- categories -->\n			<div class=\"item-category\">\n				Category: {$cats|implode:\',\'}\n			</div>\n			<!-- categories //-->\n			\n		{/if}\n\n		{if $fielddef.value && $fielddef.type != \'Categories\'}\n			{if $fielddef.type == \'SelectFile\' || $fielddef.type == \'FileUpload\'}\n				{$fielddef.name}: <a href=\"{$fielddef->GetImagePath(true)}/{$fielddef.value}\">{$fielddef.value}</a><br />\n		{elseif $fielddef.type == \'SelectDateTime\'}\n					{$fielddef.name}: {$fielddef.value|cms_date_format}<br />\n		{else}\n			{$fielddef.name}: {$fielddef.value}<br />\n			{/if}\n		{/if}\n\n		{/foreach}\n	</div>\n\n	{/if}\n	\n	<a href=\"{$return_url}\" class=\"return-link\">return</a>\n	{* or use {$return_link} to create link tag *}\n	\n</div>\n<!-- item //-->', '2015-04-27 09:04:17', '2015-04-27 09:04:17');
INSERT INTO `cms_module_templates` VALUES ('ListIt2Slider', 'summary_searchresults', '{if $items|@count > 0}\n\n{if $pagecount > 1}\n<!-- pagination -->\n<p>\n{if $pagenumber > 1}\n    {$firstpage}&nbsp;{$prevpage}\n{/if}\n{foreach from=$pagelinks item=page}\n    {$page->link}\n{/foreach}\n{if $pagenumber < $pagecount}\n    &nbsp;{$nextpage}&nbsp;{$lastpage}\n{/if}\n</p>\n<!-- pagination //-->\n{/if}\n\n{if !empty($actionparams.search)}\n    <h2>{$mod->ModLang(\'searchresultsfor\')} &quot;{$actionparams.search}&quot;</h2>\n{/if}\n\n<ul>\n{foreach from=$items item=item}\n    <li class=\"item searchresult\">\n        <a href=\"{$item->url}\">{$item->title}</a>\n    </li>\n{/foreach}\n</ul>\n\n{/if}', '2015-04-27 08:18:47', '2015-04-27 08:18:47');
INSERT INTO `cms_module_templates` VALUES ('ListIt2Slider', 'detail_default', '<!-- item -->\n<div class=\"item\">\n	<h2 class=\"item-title\">{$item->title|cms_escape}</h2>\n\n	{if !empty($item->fielddefs)}\n\n	<div class=\"item-properties\">\n		{foreach from=$item->fielddefs item=fielddef}\n\n		{*\n			Categories were moved to field definitions.\n			If you need Categories, create new Field definition with alias \"category\"\n			and Categories will be available again.\n		*}\n\n		{if $fielddef.type == \'Categories\' && ($fielddef.value != \'\')}\n\n			{* use ListIt2Loader plugin if you need Category information in default module action templates *}\n			{ListIt2Loader item=\'category\' force_array=1 value=$fielddef.value assign=\'cats\'}\n	\n			<!-- categories -->\n			<div class=\"item-category\">\n				Category: {$cats|implode:\',\'}\n			</div>\n			<!-- categories //-->\n			\n		{/if}\n\n		{if $fielddef.value && $fielddef.type != \'Categories\'}\n			{if $fielddef.type == \'SelectFile\' || $fielddef.type == \'FileUpload\'}\n				{$fielddef.name}: <a href=\"{$fielddef->GetImagePath(true)}/{$fielddef.value}\">{$fielddef.value}</a><br />\n		{elseif $fielddef.type == \'SelectDateTime\'}\n					{$fielddef.name}: {$fielddef.value|cms_date_format}<br />\n		{else}\n			{$fielddef.name}: {$fielddef.value}<br />\n			{/if}\n		{/if}\n\n		{/foreach}\n	</div>\n\n	{/if}\n	\n	<a href=\"{$return_url}\" class=\"return-link\">return</a>\n	{* or use {$return_link} to create link tag *}\n	\n</div>\n<!-- item //-->', '2015-04-27 08:18:47', '2015-04-27 08:18:47');
INSERT INTO `cms_module_templates` VALUES ('ListIt2Slider', 'search_default', '<div id=\"{$modulealias}_search\">\n	{$formstart}\n		<label for=\"{$modulealias}searchinput\">{$mod->ModLang(\'searchfor\')}:</label>&nbsp;\n		<input type=\"text\" id=\"{$modulealias}searchinput\" name=\"{$actionid}search\" size=\"20\" maxlength=\"50\" value=\"\" />\n		<input class=\"search-button\" name=\"submit\" value=\"{$mod->ModLang(\'search\')}\" type=\"submit\" />\n	{$formend}\n</div>', '2015-04-27 08:18:47', '2015-04-27 08:18:47');
INSERT INTO `cms_module_templates` VALUES ('ListIt2Slider', 'search_filter', '<div id=\"{$modulealias}_filter\">\n\n	<h3>{$filterprompt}</h3>\n\n	{$formstart}\n\n		{foreach from=$fielddefs item=fielddef}\n		<div class=\"form-row\">\n			\n		{if $fielddef.type != \'Categories\'}\n			<label for=\"filter_{$fielddef->alias}\">{$fielddef->name}</label>\n			\n			{if $fielddef.type == \'Checkbox\'}\n				<input type=\"checkbox\" name=\"{$actionid}search_{$fielddef->alias}\" id=\"filter_{$fielddef->alias}\" value=\"{$fielddef->value}\" />\n			{else}\n			\n			<select name=\"{$actionid}search_{$fielddef->alias}\" id=\"filter_{$fielddef->alias}\">\n				<option value=\'\'>{$mod->ModLang(\'all\')}</option>\n				{foreach from=$fielddef->values item=value}\n				<option>{$value}</option>\n				{/foreach}\n			</select>\n			{/if}\n			\n		{/if}\n			\n		</div>\n		{/foreach}\n	\n		<input class=\"search-button\" name=\"submit\" value=\"{$mod->ModLang(\'search\')}\" type=\"submit\" />\n\n	{$formend}\n\n</div>', '2015-04-27 08:18:47', '2015-04-27 08:18:47');
INSERT INTO `cms_module_templates` VALUES ('ListIt2Slider', 'search_multiselect', '<div id=\"{$modulealias}_filter\">\n\n	<h3>{$filterprompt}</h3>\n\n	{$formstart}\n\n		{foreach from=$fielddefs item=fielddef}\n		<div class=\"filter form-row\">\n\n		{if $fielddef.type != \'Categories\'}\n		\n			<label>{$fielddef->name}</label><br />\n			{if $fielddef.type == \'Checkbox\'}\n				\n				<input type=\"checkbox\" name=\"{$actionid}search_{$fielddef->alias}\" id=\"filter_{$fielddef->alias}\" value=\"{$fielddef->value}\" />\n			{else}\n				\n				<select name=\"{$actionid}search_{$fielddef->alias}[]\" size=\"5\" multiple=\"multiple\">\n					{foreach from=$fielddef->values item=\'value\'}\n					<option value=\"{$value}\">{$value}</option>\n					{/foreach}\n				</select>\n			{/if}\n		{/if}\n		</div>\n		{/foreach}\n\n		<input class=\"search-button\" name=\"submit\" value=\"{$mod->ModLang(\'filter\')}\" type=\"submit\" />\n\n	{$formend}\n</div>', '2015-04-27 08:18:47', '2015-04-27 08:18:47');
INSERT INTO `cms_module_templates` VALUES ('ListIt2Slider', 'category_default', '<!-- categories -->\n<ul>\n{foreach from=$categories item=category}\n	<li class=\"category-{$category->alias}\">\n		<a class=\"category-name\" href=\"{$category->url}\">{$category->name} ({$category->items|count})</a>\n			{if !empty($category->description)}\n			<div class=\"category-description\">\n				{eval var=$category->description}\n			</div>\n			{/if}\n	</li>\n{/foreach}\n<ul>\n<!-- categories //-->', '2015-04-27 08:18:47', '2015-04-27 08:18:47');
INSERT INTO `cms_module_templates` VALUES ('ListIt2Slider', 'category_hierarchy', '<!-- categories hierarchy -->\n<ul class=\"categories\">\n{foreach from=$categories item=category}\n\n{if $category->depth > $category->prevdepth}\n	{repeat string=\"<ul>\" times=$category->depth-$category->prevdepth}\n{elseif $category->depth < $category->prevdepth}\n	{repeat string=\"</li></ul>\" times=$category->prevdepth-$category->depth}\n	</li>\n{elseif $category->index > 0}\n	</li>\n{/if}\n\n{if $category->current}\n<li class=\"category-{$category->alias} current{if $category->parent == true || $category->children|@count > 0} parent{/if}\">\n	<a href=\"{$category->url}\">{$category->menutext}</a>\n{else}\n<li class=\"category-{$category->alias}{if $category->parent == true || $category->children|@count > 0} parent{/if}\">\n	<a href=\"{$category->url}\">{$category->menutext}</a>\n{/if}\n\n{/foreach}\n\n	{repeat string=\"</li></ul>\" times=$category->depth-1}\n	</li>\n</ul>\n<!-- categories hierarchy //-->', '2015-04-27 08:18:47', '2015-04-27 08:18:47');
INSERT INTO `cms_module_templates` VALUES ('ListIt2Slider', 'archive_default', '{if $archives|@count > 0}\n\n    <!-- archives -->\n    <ul>\n        {foreach from=$archives item=\'archive\'}\n            <li><a href=\"{$archive->url}\">{$archive->month}/{$archive->year} ({$archive->count})</a> [{$archive->timestamp|cms_date_format:\'%B %Y\'}]</li>    \n        {/foreach}\n    </ul>\n    <!-- archives //-->\n    \n{/if}', '2015-04-27 08:18:47', '2015-04-27 08:18:47');
INSERT INTO `cms_module_templates` VALUES ('ListIt2Blog', 'search_default', '<div id=\"{$modulealias}_search\">\n	{$formstart}\n		<label for=\"{$modulealias}searchinput\">{$mod->ModLang(\'searchfor\')}:</label>&nbsp;\n		<input type=\"text\" id=\"{$modulealias}searchinput\" name=\"{$actionid}search\" size=\"20\" maxlength=\"50\" value=\"\" />\n		<input class=\"search-button\" name=\"submit\" value=\"{$mod->ModLang(\'search\')}\" type=\"submit\" />\n	{$formend}\n</div>', '2015-04-27 09:04:17', '2015-04-27 09:04:17');
INSERT INTO `cms_module_templates` VALUES ('ListIt2Blog', 'search_filter', '<div id=\"{$modulealias}_filter\">\n\n	<h3>{$filterprompt}</h3>\n\n	{$formstart}\n\n		{foreach from=$fielddefs item=fielddef}\n		<div class=\"form-row\">\n			\n		{if $fielddef.type != \'Categories\'}\n			<label for=\"filter_{$fielddef->alias}\">{$fielddef->name}</label>\n			\n			{if $fielddef.type == \'Checkbox\'}\n				<input type=\"checkbox\" name=\"{$actionid}search_{$fielddef->alias}\" id=\"filter_{$fielddef->alias}\" value=\"{$fielddef->value}\" />\n			{else}\n			\n			<select name=\"{$actionid}search_{$fielddef->alias}\" id=\"filter_{$fielddef->alias}\">\n				<option value=\'\'>{$mod->ModLang(\'all\')}</option>\n				{foreach from=$fielddef->values item=value}\n				<option>{$value}</option>\n				{/foreach}\n			</select>\n			{/if}\n			\n		{/if}\n			\n		</div>\n		{/foreach}\n	\n		<input class=\"search-button\" name=\"submit\" value=\"{$mod->ModLang(\'search\')}\" type=\"submit\" />\n\n	{$formend}\n\n</div>', '2015-04-27 09:04:17', '2015-04-27 09:04:17');
INSERT INTO `cms_module_templates` VALUES ('ListIt2Blog', 'search_multiselect', '<div id=\"{$modulealias}_filter\">\n\n	<h3>{$filterprompt}</h3>\n\n	{$formstart}\n\n		{foreach from=$fielddefs item=fielddef}\n		<div class=\"filter form-row\">\n\n		{if $fielddef.type != \'Categories\'}\n		\n			<label>{$fielddef->name}</label><br />\n			{if $fielddef.type == \'Checkbox\'}\n				\n				<input type=\"checkbox\" name=\"{$actionid}search_{$fielddef->alias}\" id=\"filter_{$fielddef->alias}\" value=\"{$fielddef->value}\" />\n			{else}\n				\n				<select name=\"{$actionid}search_{$fielddef->alias}[]\" size=\"5\" multiple=\"multiple\">\n					{foreach from=$fielddef->values item=\'value\'}\n					<option value=\"{$value}\">{$value}</option>\n					{/foreach}\n				</select>\n			{/if}\n		{/if}\n		</div>\n		{/foreach}\n\n		<input class=\"search-button\" name=\"submit\" value=\"{$mod->ModLang(\'filter\')}\" type=\"submit\" />\n\n	{$formend}\n</div>', '2015-04-27 09:04:17', '2015-04-27 09:04:17');
INSERT INTO `cms_module_templates` VALUES ('ListIt2Blog', 'category_default', '<!-- categories -->\n<ul>\n{foreach from=$categories item=category}\n	<li class=\"category-{$category->alias}\">\n		<a class=\"category-name\" href=\"{$category->url}\">{$category->name} ({$category->items|count})</a>\n			{if !empty($category->description)}\n			<div class=\"category-description\">\n				{eval var=$category->description}\n			</div>\n			{/if}\n	</li>\n{/foreach}\n<ul>\n<!-- categories //-->', '2015-04-27 09:04:17', '2015-04-27 09:04:17');
INSERT INTO `cms_module_templates` VALUES ('ListIt2Blog', 'category_hierarchy', '<!-- categories hierarchy -->\n<ul class=\"categories\">\n{foreach from=$categories item=category}\n\n{if $category->depth > $category->prevdepth}\n	{repeat string=\"<ul>\" times=$category->depth-$category->prevdepth}\n{elseif $category->depth < $category->prevdepth}\n	{repeat string=\"</li></ul>\" times=$category->prevdepth-$category->depth}\n	</li>\n{elseif $category->index > 0}\n	</li>\n{/if}\n\n{if $category->current}\n<li class=\"category-{$category->alias} current{if $category->parent == true || $category->children|@count > 0} parent{/if}\">\n	<a href=\"{$category->url}\">{$category->menutext}</a>\n{else}\n<li class=\"category-{$category->alias}{if $category->parent == true || $category->children|@count > 0} parent{/if}\">\n	<a href=\"{$category->url}\">{$category->menutext}</a>\n{/if}\n\n{/foreach}\n\n	{repeat string=\"</li></ul>\" times=$category->depth-1}\n	</li>\n</ul>\n<!-- categories hierarchy //-->', '2015-04-27 09:04:17', '2015-04-27 09:04:17');
INSERT INTO `cms_module_templates` VALUES ('ListIt2Blog', 'archive_default', '{if $archives|@count > 0}\n\n    <!-- archives -->\n    <ul>\n        {foreach from=$archives item=\'archive\'}\n            <li><a href=\"{$archive->url}\">{$archive->month}/{$archive->year} ({$archive->count})</a> [{$archive->timestamp|cms_date_format:\'%B %Y\'}]</li>    \n        {/foreach}\n    </ul>\n    <!-- archives //-->\n    \n{/if}', '2015-04-27 09:04:17', '2015-04-27 09:04:17');
INSERT INTO `cms_module_templates` VALUES ('ListIt2Menu', 'summary_default', '{if $items|@count > 0}\r\n<ol class=\"hungry-menu-list\">	\r\n{foreach from=$items item=item}\r\n    	{if !empty($item->fielddefs)}\r\n        	{foreach from=$item->fielddefs item=fielddef}\r\n        	{if $fielddef.value && $fielddef.type != \'Categories\'}\r\n        		{if $fielddef.type == \'SelectFile\' || $fielddef.type == \'FileUpload\'}\r\n{if $fielddef.alias == \'large\'}\r\n        			{assign var=\"src\" value=\"{$fielddef->GetImagePath(true)}/{$fielddef.value}\"}\r\n{/if}\r\n        		{/if}\r\n        	{/if}\r\n        	{/foreach}\r\n        {/if}\r\n{assign var=\"noimage\" value=\"images/nopic.jpg\"}\r\n<!-- START Menu Item -->\r\n<li class=\"hungry-menu-item {$item->fielddefs.status->value|default:\'\'}\">\r\n<div id=\"mnleft\">\r\n<a href=\"{if $item->fielddefs.large->value && $item->fielddefs.large->value != null}{CGSmartImage src=\"{$src}\" alias=\"std_w800\"}{else}{$noimage}{/if}\" \r\n	   class=\"lightbox hungry-thumbnail-link\"\r\n	   title=\'{$item->fielddefs.description->value|default:\"Đang cập nhật...\"}\'>\r\n		<img src=\"{if $item->fielddefs.large->value}{CGSmartImage src=\"{$src}\" alias=\"std_300x300\"}{else}{CGSmartImage src=\"{$noimage}\" alias=\"std_300x300\"}{/if}\" class=\"hungry-menu-item-thumbnail\" alt=\"{$item->title}\" />\r\n		<div class=\"hungry-thumbnail-overlay\">\r\n			<i class=\"fa fa-search-plus\"></i>\r\n		</div>\r\n	</a>\r\n</div>\r\n<div id=\"mnright\">\r\n<div class=\"hungry-menu-item-container\">\r\n			<a href=\"{if $item->fielddefs.large->value && $item->fielddefs.large->value != null}{$imageLPatch}{else}images/nopic.jpg{/if}\" \r\n	   class=\"hungry-menu-item-header lightbox hungry-thumbnail-link\"\r\n	   title=\'{$item->fielddefs.description->value|default:\"Đang cập nhật...\"}\'>\r\n			<h3 class=\"hungry-menu-item-title\"><i class=\"fa fa-cutlery\"></i> {$item->title}<!--span style=\"float:right;padding-right:60px;text-align:right;\">{$item->fielddefs.price->value|number_format:0}<sub>vnđ</sub--></span></h3>\r\n			<h4 class=\"hungry-menu-item-price special-tooltip\" {if $item->fielddefs.comment->value}title=\"{$item->fielddefs.comment->value}!\"{/if}>{if $item->fielddefs.price->value==\"\"}Free{else}{if $item->fielddefs.from->value}from {/if}{$item->fielddefs.price->value|number_format:0}<sub>vnđ</sub>{/if}</h4>\r\n		</a>\r\n		<div class=\"hungry-menu-item-excerpt\">\r\n			<p style=\"font-style: italic;\">{$item->fielddefs.titen->value|default:\'Đang cập nhật...\'|truncate:250} <span class=\"menu-info\">{if $item->fielddefs.free->value}<i class=\"fa fa-exclamation-circle\"></i>{$item->fielddefs.free->value}!{/if}</span></p>\r\n		</div>\r\n	</div>\r\n</div>	\r\n</li>\r\n<!-- END Menu Item -->\r\n{/foreach}\r\n</ol>\r\n{/if}', '2020-06-29 11:30:48', '2020-06-29 11:30:48');
INSERT INTO `cms_module_templates` VALUES ('ListIt2Blog', 'summary_default', '{if $items|@count > 0}\r\n{foreach from=$items item=item}\r\n<div class=\"wow {if $item->fielddefs.position->value}{$item->fielddefs.position->value}{else}fadeInLeft{/if}\" data-wow-duration=\"2s\" data-wow-offset=\"250\">\r\n					<div class=\"grid-50 tablet-grid-50 mobile-grid-100\">\r\n						<div class=\"hungry-blog-block\">\r\n							<div class=\"hungry-blog-arrow\"></div>\r\n							<div class=\"hungry-blog-featured-image\">\r\n								<a class=\"image-hover lightbox {*hungry-thumbnail-link cboxElement*}\" href=\"{if $item->fielddefs.link->value}{$item->fielddefs.link->value}{else}uploads/blog/{$item->fielddefs.image->value}{/if}\" title=\"{$item->fielddefs.descript->value}\">\r\n									<img src=\"uploads/blog/{$item->fielddefs.image->value}\" alt=\"{$item->title}\" />\r\n									<div class=\"image-hover-overlay\">\r\n										<i class=\"fa fa-link\"></i>\r\n									</div>\r\n								</a>\r\n							</div>\r\n							<div class=\"hungry-blog-meta\">\r\n								<!--span class=\"hungry-blog-date\">\r\n									<time datetime=\"2015-05-01\">Bắt đầu từ 01/05/2015</time>\r\n								</span-->\r\n								<h3 class=\"hungry-blog-title\">\r\n									<a href=\"{if $item->fielddefs.link->value}{$item->fielddefs.link->value}{else}uploads/blog/{$item->fielddefs.image->value}{/if}\" class=\"lightbox{*hungry-thumbnail-link cboxElement*}\"  title=\"{$item->fielddefs.descript->value}\">{$item->title}</a>\r\n								</h3>\r\n								<span class=\"hungry-blog-author\">by <a href=\"{if $item->fielddefs.link->value}{$item->fielddefs.link->value}{else}{root_url}{/if}\">{$item->fielddefs.source->value}</a></span>\r\n							</div>\r\n						</div>\r\n					</div>\r\n				</div>\r\n{/foreach}\r\n{/if}', '2019-07-02 12:17:30', '2019-07-02 12:17:30');
INSERT INTO `cms_module_templates` VALUES ('SiteMapMadeSimple', 'xml_Sample', '<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<urlset xmlns=\"http://www.google.com/schemas/sitemap/0.9\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.google.com/schemas/sitemap/0.9 http://www.google.com/schemas/sitemap/0.9/sitemap.xsd\">{strip}\n{foreach $output as $page}\n<url>\n  <loc>{$page->url}</loc>\n  <lastmod>{$page->date|date_format:\"%Y-%m-%d\"}</lastmod>\n  <priority>{$page->priority}</priority>\n  <changefreq>{$page->frequency}</changefreq>\n</url>\n{/foreach}\n{/strip}</urlset>', '2015-07-10 15:55:51', '2015-07-10 15:55:51');
INSERT INTO `cms_module_templates` VALUES ('vipmember', 'list_default', '<h2>{$leveltitle}</h2><p>{$breadcrumbs}</p>\r\n<ul>\r\n{foreach from=$itemlist item=\"item\"}\r\n	<li {if $item->is_selected}class=\"active\"{/if}>{$item->detaillink}</li>\r\n{/foreach}\r\n</ul>', '2015-09-17 12:24:58', '2015-09-18 14:09:54');
INSERT INTO `cms_module_templates` VALUES ('vipmember', 'final_default', '<p>{$breadcrumbs}</p><h3>{$item->name}</h3>', '2015-09-17 12:24:58', '2015-09-17 12:24:58');
INSERT INTO `cms_module_templates` VALUES ('ListIt2Testimonials', 'summary_default', '{if $items|@count > 0}\r\n	{foreach from=$items item=item}\r\n{assign var=\"imagePatch\" value=\"uploads/testimonials/{$item->fielddefs.image->value}\"}\r\n{assign var=\"facebook\" value=\"{$item->fielddefs.facebook->value}\"}\r\n<!-- Testimonial 01 -->\r\n			<div class=\"wow fadeInLeft\" data-wow-duration=\"2s\" data-wow-offset=\"250\">\r\n				<div class=\"prefix-10 grid-35 suffix-5 tablet-grid-50 mobile-grid-100\">\r\n					<div class=\"hungry-testimonial\">\r\n						<blockquote>&quot;{$item->fielddefs.say->value}!&quot;</blockquote>\r\n						<a href=\"{if $item->fielddefs.image->value && $item->fielddefs.image->value != null}{$imagePatch}{else}images/nopic.png{/if}\" class=\"lightbox hungry-thumbnail-link cboxElement\"><img src=\"\r\n						{if $item->fielddefs.facebook->value && $item->fielddefs.facebook->value != null}https://graph.facebook.com/{$facebook}/picture?width=200&height=200{elseif $item->fielddefs.image->value && $item->fielddefs.image->value != null}{$imagePatch}{else}images/nopic.png{/if}\" alt=\"{$item->title}\" /></a>\r\n						<cite>{if $item->fielddefs.facebook->value && $item->fielddefs.facebook->value != null}<a href=\"https://www.facebook.com/{$facebook}\" target=\"_blank\">{$item->title}</a>{else}{$item->title}{/if} {$item->fielddefs.address->value}</cite>\r\n					</div>\r\n				</div>\r\n			</div>\r\n	{/foreach}\r\n{/if}', '2015-09-18 00:34:51', '2015-09-18 00:34:51');
INSERT INTO `cms_module_templates` VALUES ('ListIt2Menu', 'summary_reservations', '{if $items|@count > 0}\r\n<ol class=\"hungry-menu-list\">	\r\n{foreach from=$items item=item}\r\n{assign var=\"imageLPatch\" value=\"uploads/menu/{$item->fielddefs.large->value}\"}\r\n{assign var=\"imageSPatch\" value=\"uploads/menu/{$item->fielddefs.small->value}\"}\r\n<!-- START Menu Item -->\r\n<li class=\"hungry-menu-item {$item->fielddefs.status->value|default:\'\'}\">\r\n	<a href=\"{if $item->fielddefs.large->value && $item->fielddefs.large->value != null}{$imageLPatch}{else}images/nopic.jpg{/if}\" \r\n	   class=\"lightbox hungry-thumbnail-link\"\r\n	   title=\'{$item->fielddefs.description->value|default:\"Đang cập nhật...\"}\'>\r\n		<img src=\"{if $item->fielddefs.small->value}{$imageSPatch}{else}images/nomenu.jpg{/if}\" class=\"hungry-menu-item-thumbnail\" alt=\"{$item->title}\" />\r\n		<div class=\"hungry-thumbnail-overlay\">\r\n			<i class=\"fa fa-search-plus\"></i>\r\n		</div>\r\n	</a>\r\n	<div class=\"hungry-menu-item-container\">\r\n			<a href=\"{if $item->fielddefs.large->value && $item->fielddefs.large->value != null}{$imageLPatch}{else}images/nopic.jpg{/if}\" \r\n	   class=\"hungry-menu-item-header lightbox hungry-thumbnail-link\"\r\n	   title=\'{$item->fielddefs.description->value|default:\"Đang cập nhật...\"}\'>\r\n			<h3 class=\"hungry-menu-item-title\"><i class=\"fa fa-cutlery\"></i> {$item->title}<!--span style=\"float:right;padding-right:60px;text-align:right;\">{$item->fielddefs.price->value|number_format:0}<sub>vnđ</sub--></span></h3>\r\n			<h4 class=\"hungry-menu-item-price special-tooltip\" {if $item->fielddefs.comment->value}title=\"{$item->fielddefs.comment->value}!\"{/if}>{if $item->fielddefs.price->value==\"\"}Free{else}{if $item->fielddefs.from->value}from {/if}{$item->fielddefs.price->value|number_format:0}<sub>vnđ</sub>{/if}</h4>\r\n		</a>\r\n		<div class=\"hungry-menu-item-excerpt\">\r\n			<p style=\"font-style: italic;\">{$item->fielddefs.titen->value|default:\'1 Đang cập nhật...\'|truncate:250} <span class=\"menu-info\">{if $item->fielddefs.free->value}<i class=\"fa fa-exclamation-circle\"></i>{$item->fielddefs.free->value}!{/if}</span></p>\r\n		</div>\r\n	</div>\r\n</li>\r\n<!-- END Menu Item -->\r\n{/foreach}\r\n</ol>\r\n{/if}', '2019-05-06 14:42:13', '2019-05-06 14:42:16');

-- ----------------------------
-- Table structure for cms_module_vipmember
-- ----------------------------
DROP TABLE IF EXISTS `cms_module_vipmember`;
CREATE TABLE `cms_module_vipmember`  (
  `id` int(11) NOT NULL
) ENGINE = MyISAM CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = FIXED;

-- ----------------------------
-- Records of cms_module_vipmember
-- ----------------------------
INSERT INTO `cms_module_vipmember` VALUES (388);

-- ----------------------------
-- Table structure for cms_module_vipmember_items
-- ----------------------------
DROP TABLE IF EXISTS `cms_module_vipmember_items`;
CREATE TABLE `cms_module_vipmember_items`  (
  `phone` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `sex` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `address` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `birthday` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `id` int(11) NOT NULL,
  `name` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `alias` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `item_order` int(11) NULL DEFAULT NULL,
  `active` tinyint(4) NULL DEFAULT NULL,
  `isdefault` tinyint(4) NULL DEFAULT NULL,
  `date_modified` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of cms_module_vipmember_items
-- ----------------------------
INSERT INTO `cms_module_vipmember_items` VALUES ('0966605339', 'Mr.', '41 Bà Triệu', '1988', 59, 'Nguyen Ham Tien Thanh', 'Nguyen-Ham-Tien-Thanh', 7, 1, 0, '2015-10-17 01:05:13');
INSERT INTO `cms_module_vipmember_items` VALUES ('0945006789', 'Mr.', 'Giáp bát', '1989', 58, 'Hà ngọc đức', 'Ha-ngoc-duc', 6, 1, 0, '2015-10-15 19:11:14');
INSERT INTO `cms_module_vipmember_items` VALUES ('0904233469', 'Ms.', '117 tran duy hưng , phường trung hòa , Q. Cầu Giay , hà nội', '1971', 56, 'nguyen thi kim oanh', 'nguyen-thi-kim-oanh', 2, 1, 0, '2015-09-23 17:49:14');
INSERT INTO `cms_module_vipmember_items` VALUES ('0976494097', 'Ms.', 'Hà nội', '1991', 57, 'Nguyen hong nhung', 'Nguyen-hong-nhung', 2, 1, 0, '2015-09-26 17:11:17');
INSERT INTO `cms_module_vipmember_items` VALUES ('0904752808', 'Ms.', '70 Ngô Quyền, Hoàn Kiếm, Hàng Bài, Hà Nội', '1991', 60, 'Hà Thị Kim Liên', 'Ha-Thi-Kim-Lien', 8, 1, 0, '2015-10-20 15:09:00');
INSERT INTO `cms_module_vipmember_items` VALUES ('0968910761', 'Mr.', 'Đường 65_phường thảo điền. Q2 tphcm', '1987', 61, 'Nguyễn van giang', 'Nguyen-van-giang', 9, 1, 0, '2015-10-27 21:30:43');
INSERT INTO `cms_module_vipmember_items` VALUES ('0918940168', 'Ms.', 'Timescity', '1995', 62, 'Trần Huệ Anh', 'Tran-Hue-Anh', 10, 1, 0, '2015-10-31 09:34:52');
INSERT INTO `cms_module_vipmember_items` VALUES ('0912377770', 'Ms.', 'Tân Mai Hà Nội', '1979', 63, 'Nguyễn Cúc', 'Nguyen-Cuc', 11, 1, 0, '2015-11-07 18:06:39');
INSERT INTO `cms_module_vipmember_items` VALUES ('0944344357', 'Mr.', 'Thanh xuan , hà nội', '1983', 64, 'Nguyễn hữu tuấn', 'Nguyen-huu-tuan', 12, 1, 0, '2015-11-08 18:02:42');
INSERT INTO `cms_module_vipmember_items` VALUES ('0904929151', 'Mr.', 'Số 10 ngách 360/1 La Thành', '1988', 65, 'Trần Tiến Nam', 'Tran-Tien-Nam', 13, 1, 0, '2015-11-11 22:48:51');
INSERT INTO `cms_module_vipmember_items` VALUES ('0983238384', 'Mr.', 'Việt hưng, long biên, hà nội', '1985', 265, 'Nguyễn thanh tùng', 'Nguyen-thanh-tung', 212, 1, 0, '2017-07-21 10:59:58');
INSERT INTO `cms_module_vipmember_items` VALUES ('0983053391', 'Ms.', 'phú xuyên - hà nội', '1991', 67, 'Tạ Như Quỳnh', 'Ta-Nhu-Quynh', 14, 1, 0, '2015-11-19 12:57:44');
INSERT INTO `cms_module_vipmember_items` VALUES ('0934509735', 'Mr.', '43/1/34 phố khâm thiên', '1973', 68, 'nguyễn anh tuấn', 'nguye-n-anh-tua-n', 15, 1, 0, '2015-11-19 18:01:42');
INSERT INTO `cms_module_vipmember_items` VALUES ('09618232126', 'Ms.', '58 Nhiêu Tâm P5 Q5 TP. HCM', '1986', 69, 'Trần Lê Minh Tâm', 'Tran-Le-Minh-Tam', 16, 1, 0, '2015-11-20 09:03:54');
INSERT INTO `cms_module_vipmember_items` VALUES ('0168218138', 'Ms.', 'Cau giay- Ha Noi', '1989', 70, 'Nguyen thi lan', 'Nguyen-thi-lan', 17, 1, 0, '2015-11-21 01:34:08');
INSERT INTO `cms_module_vipmember_items` VALUES ('0983629282', 'Mr.', '162A Nguyễn Tuân Hà nội', '0304', 71, 'Nguyễn Mạnh Hùng', 'Nguyen-Manh-Hung', 18, 1, 0, '2015-11-22 13:29:52');
INSERT INTO `cms_module_vipmember_items` VALUES ('091626848626', 'null', 'Gshh', '', 72, 'Abc', 'Abc', 19, 1, 0, '2015-11-29 20:41:23');
INSERT INTO `cms_module_vipmember_items` VALUES ('0969728459', 'Mr.', 'Trung HoàNhân chính', '1971', 73, 'Phạm Ngọc Quang', 'Pham-Ngoc-Quang', 20, 1, 0, '2015-12-03 09:40:22');
INSERT INTO `cms_module_vipmember_items` VALUES ('0912672065', 'Mr.', '166d tran hung dao, nguyen cu trinh, q1', '1982', 74, 'Tran Hoang Khanh', 'Tran-Hoang-Khanh', 21, 1, 0, '2015-12-23 17:36:52');
INSERT INTO `cms_module_vipmember_items` VALUES ('0985885132', 'Mr.', '169 mai hắc đế-hn', '1987', 75, 'Nguyễn kim anh tuấn', 'Nguyen-kim-anh-tuan', 22, 1, 0, '2015-12-30 12:36:18');
INSERT INTO `cms_module_vipmember_items` VALUES ('0902123545', 'Ms.', 'Căn hộ W706, chung cư Golden Westlake, 151 Thụy Khuê, Phường Thụ', '1973', 76, 'Bùi Lan Hương', 'Bui-Lan-Huong', 23, 1, 0, '2016-01-03 05:50:57');
INSERT INTO `cms_module_vipmember_items` VALUES ('0964051111', 'Mr.', '58 Tran Nhan Tong, Ha Noi', '1981', 77, 'Duong Trung Hieu', 'Duong-Trung-Hieu', 24, 1, 0, '2016-01-07 23:54:35');
INSERT INTO `cms_module_vipmember_items` VALUES ('0973888955', 'Ms.', 'Đội cấn, Ba Đình, Hà Nội', '1990', 78, 'Vũ thị Hà ly', 'Vu-thi-Ha-ly', 25, 1, 0, '2016-01-23 00:36:54');
INSERT INTO `cms_module_vipmember_items` VALUES ('0934501182', 'Ms.', 'Soos 1 ngõ 71 Láng hạ - ba đình - hà nội', '1982', 82, 'Vũ Thị Kim NGân', 'Vu-Thi-Kim-NGan', 27, 1, 0, '2016-02-19 17:20:42');
INSERT INTO `cms_module_vipmember_items` VALUES ('0912871479', 'Ms.', '49 nguyễn khang cầu giấy', '1979', 83, 'Bùi Thị Hảo', 'Bui-Thi-Hao', 28, 1, 0, '2016-02-19 17:25:55');
INSERT INTO `cms_module_vipmember_items` VALUES ('01234168555', 'Mr.', 'Hà Nội', '1985', 84, 'An Trần Thiên', 'An-Tran-Thien', 28, 1, 0, '2016-02-26 11:09:51');
INSERT INTO `cms_module_vipmember_items` VALUES ('0914191598', 'null', '16 /291 vũ hữu thanh xuân bắc ', '', 85, 'lại văn dũng', 'lai-van-dung', 29, 1, 0, '2016-02-29 19:13:51');
INSERT INTO `cms_module_vipmember_items` VALUES ('0987879698', 'Ms.', 'huyentit317@gmail.com', '1989', 86, 'Phạm Thu Huyền', 'Pham-Thu-Huyen', 30, 1, 0, '2016-03-02 12:29:37');
INSERT INTO `cms_module_vipmember_items` VALUES ('0982987698', 'Mr.', '94 Ngõ Xã Đàn 2, Nam Đồng, Đống Đa, Hà Nội', '1984', 88, 'Hoàng Ngọc Thái', 'Hoang-Ngoc-Thai', 31, 1, 0, '2016-03-04 18:15:16');
INSERT INTO `cms_module_vipmember_items` VALUES ('0934202425', 'Mr.', 'My Dinh', '1986', 89, 'Nguyễn Minh Tân', 'Nguyen-Minh-Tan', 32, 1, 0, '2016-03-06 10:31:21');
INSERT INTO `cms_module_vipmember_items` VALUES ('0936577533', 'Ms.', '178 nguyễn lương bằng', '1990', 90, 'Nguyễn Tú Anh', 'Nguyen-Tu-Anh', 33, 1, 0, '2016-03-12 03:42:09');
INSERT INTO `cms_module_vipmember_items` VALUES ('0966708233', 'Ms.', 'Kdt đại thanh, thanh trì, hà nội', '1989', 92, 'Vu thi hai ha', 'Vu-thi-hai-ha', 35, 1, 0, '2016-03-31 12:14:38');
INSERT INTO `cms_module_vipmember_items` VALUES ('01672345550', 'null', '', '1992', 93, 'Phạm Đức Trung', 'Pham-duc-Trung', 35, 1, 0, '2016-03-31 20:15:21');
INSERT INTO `cms_module_vipmember_items` VALUES ('0902283799', 'Mr.', 'Hoa Bằng', '1988', 94, 'Nguyễn Văn Hưng ', 'Nguyen-Van-Hung', 36, 1, 0, '2016-04-02 17:38:16');
INSERT INTO `cms_module_vipmember_items` VALUES ('0934384483', 'Mr.', '311/H1, ngõ 78 Giải Phóng, Đống Đa, Hà Nội', '1990', 95, 'Nguyễn Ngọc Hiền', 'Nguyen-Ngoc-Hien', 37, 1, 0, '2016-04-11 03:24:08');
INSERT INTO `cms_module_vipmember_items` VALUES ('0933316388', 'Mr.', 'số 02 chùa bộc ,đống đa, hà nội', '1979', 96, 'phí minh thắng', 'phi-minh-thang', 38, 1, 0, '2016-04-11 15:40:23');
INSERT INTO `cms_module_vipmember_items` VALUES ('0933316388', 'Mr.', 'số 02 chùa bộc ,đống đa, hà nội', '1979', 97, 'phí minh thắng', 'phi-minh-thang', 39, 1, 0, '2016-04-11 15:40:24');
INSERT INTO `cms_module_vipmember_items` VALUES ('0989898007', 'Mr.', '23 Nguyễn Khang, Cầu Giấy, Hà Nội', '1985', 98, 'Phạm Ngọc Duy', 'Pham-Ngoc-Duy', 40, 1, 0, '2016-04-17 16:26:00');
INSERT INTO `cms_module_vipmember_items` VALUES ('0979549553', 'Ms.', 'So 16, Ngo 15 Ho Giam, Quoc Tu Giam, Dong Da, Hanoi', '1986', 99, 'Dinh Thi Hao', 'Dinh-Thi-Hao', 41, 1, 0, '2016-04-17 19:01:48');
INSERT INTO `cms_module_vipmember_items` VALUES ('0973288588', 'null', 'Bích Câu - Đống Đa - Hà nội', '1981', 100, 'Nguyễn Thị Bích Diệp', 'Nguyen-Thi-Bich-Diep', 42, 1, 0, '2016-04-21 15:19:38');
INSERT INTO `cms_module_vipmember_items` VALUES ('0988314613', 'Mr.', 'HOÀNG MAI HA NOI', '1984', 101, 'Đặng Đình Tuyến', 'dang-dinh-Tuyen', 43, 1, 0, '2016-04-21 17:45:48');
INSERT INTO `cms_module_vipmember_items` VALUES ('0973987400', 'Mr.', 'Đông Ngạc, Bắc Từ Liêm, Hà Nội', '1987', 102, 'Ngô Văn Tung', 'Ngo-Van-Tung', 44, 1, 0, '2016-04-25 16:13:50');
INSERT INTO `cms_module_vipmember_items` VALUES ('0977177540', 'Ms.', '99 LÊ DUẨN - HOÀN KIẾM - HÀ NỘI', '1990', 264, 'NGUYEN THI THAM', 'NGUYEN-THI-THAM', 211, 1, 0, '2017-07-17 17:20:54');
INSERT INTO `cms_module_vipmember_items` VALUES ('01649464864', 'null', 'royalcity', '1986', 104, 'Kim', 'Kim', 45, 1, 0, '2016-04-27 16:42:48');
INSERT INTO `cms_module_vipmember_items` VALUES ('0948484440', 'null', '126 hoàng sâm', '1991', 105, 'Trần Hoàng Hà', 'Tran-Hoang-Ha', 46, 1, 0, '2016-05-03 00:32:56');
INSERT INTO `cms_module_vipmember_items` VALUES ('0976745094', 'Mr.', 'Công viên cầu đôi , đông anh hà nội', '1986', 106, 'Phạm thế hiển', 'Pham-the-hien', 47, 1, 0, '2016-05-03 12:26:07');
INSERT INTO `cms_module_vipmember_items` VALUES ('0989061185', 'Ms.', 'Tô hiệu hà đông', '1985', 107, 'Phùng minh huyền', 'Phung-minh-huyen', 48, 1, 0, '2016-05-05 13:35:57');
INSERT INTO `cms_module_vipmember_items` VALUES ('0942244994', 'Mr.', 'Times City 458 Minh Khai', '1983', 108, 'Lê Quang Huy', 'Le-Quang-Huy', 49, 1, 0, '2016-05-05 16:40:15');
INSERT INTO `cms_module_vipmember_items` VALUES ('0917315565', 'Mr.', 'Số 3 Nguyễn Thượng Hiền, phường Nguyễn Du, quận Hai Bà trưng, Hà', '1988', 109, 'HOÀNG SỸ KHIÊM', 'HOaNG-Sy-KHIeM', 50, 1, 0, '2016-05-12 10:12:59');
INSERT INTO `cms_module_vipmember_items` VALUES ('0988569689', 'null', 'số 4 Bà Triệu, Hoàn Kiếm, Hà nội', '1972', 110, 'Hàn Lệ Hằng', 'Han-Le-Hang', 51, 1, 0, '2016-05-13 11:14:30');
INSERT INTO `cms_module_vipmember_items` VALUES ('0904485848', 'Ms.', 'Nghĩa Dũng', '1981', 111, 'Phan Thuỳ Dương', 'Phan-Thuy-Duong', 52, 1, 0, '2016-05-15 06:59:49');
INSERT INTO `cms_module_vipmember_items` VALUES ('0983300395', 'Ms.', 'yen hoa hanoi', '1979', 112, 'Lai thi anh Nga', 'Lai-thi-anh-Nga', 53, 1, 0, '2016-05-17 14:36:46');
INSERT INTO `cms_module_vipmember_items` VALUES ('0923937979', 'Mr.', '5 hẻm 127/56/2 hào nam, ô chợ dừa, đống đa, hà nội', '1977', 113, 'Trần Nam Tiến', 'Tran-Nam-Tien', 54, 1, 0, '2016-05-21 11:11:11');
INSERT INTO `cms_module_vipmember_items` VALUES ('977587978', 'Ms.', 'D6b02, Ngo 675 Lac Long Quan', '1985', 114, 'Nguyễn Thị Phương Nga', 'Nguyen-Thi-Phuong-Nga', 55, 1, 0, '2016-05-28 16:40:38');
INSERT INTO `cms_module_vipmember_items` VALUES ('01238413966', 'Mr.', '86 nguyễn du, hoàn kiếm, hà nội', '1991', 115, 'bùi minh đức', 'bui-minh-duc', 56, 1, 0, '2016-05-31 10:36:14');
INSERT INTO `cms_module_vipmember_items` VALUES ('0946600887', 'Ms.', '466 Hoàng Công Chất, Phú Diễn, Bắc Từ Liêm Hà Nội', '1987', 116, 'Trần Minh Phương', 'Tran-Minh-Phuong', 57, 1, 0, '2016-05-31 15:37:35');
INSERT INTO `cms_module_vipmember_items` VALUES ('0973894689', 'Ms.', '5 vọng đưc, hàng bai, hk, hn', '1982', 119, 'đoàn thi thanh mai', 'doan-thi-thanh-mai', 59, 1, 0, '2016-06-04 16:05:14');
INSERT INTO `cms_module_vipmember_items` VALUES ('0904319442', 'Ms.', 'Số 4 ngõ 208 Thuỵ Khuê  Tây Hồ ', '1977', 120, 'Phạm Phương Hoa', 'Pham-Phuong-Hoa', 59, 1, 0, '2016-06-05 17:16:45');
INSERT INTO `cms_module_vipmember_items` VALUES ('0977779695', 'Mr.', '172 Ngọc Khánh', '1984', 121, 'Phạm Đắc Nam', 'Pham-dac-Nam', 60, 1, 0, '2016-06-05 19:02:32');
INSERT INTO `cms_module_vipmember_items` VALUES ('01239230532', 'Ms.', '974 La Thành , Ngọc Khánh, BĐ HN', '1986', 122, 'Đỗ Thu Huyền', 'do-Thu-Huyen', 61, 1, 0, '2016-06-07 17:01:02');
INSERT INTO `cms_module_vipmember_items` VALUES ('0936134366', 'Ms.', '15/45 Phan đình phùng, Hà nội', '1984', 123, 'Le minh trang', 'Le-minh-trang', 62, 1, 0, '2016-06-09 17:31:50');
INSERT INTO `cms_module_vipmember_items` VALUES ('0907061985', 'Mr.', 'số nhà 25 ngõ 64 Nguyễn Lương Bằng, Đống Đa, Hà Nội', '1985', 124, 'Nguyễn Kiều Ngọc Linh', 'Nguyen-Kieu-Ngoc-Linh', 63, 1, 0, '2016-06-10 17:19:39');
INSERT INTO `cms_module_vipmember_items` VALUES ('0945939999', 'Mr.', 'Tpvinh nghe an', '1986', 125, 'Bui xuan ha', 'Bui-xuan-ha', 64, 1, 0, '2016-06-11 11:15:26');
INSERT INTO `cms_module_vipmember_items` VALUES ('0982421242', 'Ms.', '155 Trường Chinh - Hà Nội', '1991', 126, 'Nguyễn Hằng Hà', 'Nguyen-Hang-Ha', 65, 1, 0, '2016-06-15 10:14:42');
INSERT INTO `cms_module_vipmember_items` VALUES ('0912848870', 'Ms.', '501B nhà D4 Phường Phương Mai Quận Đống Đa, Hà Nội', '1981', 127, 'Bùi Thanh Thảo', 'Bui-Thanh-Thao', 66, 1, 0, '2016-06-21 17:30:39');
INSERT INTO `cms_module_vipmember_items` VALUES ('0945752799', 'Mr.', 'Ha noi', '1980', 129, 'Nguyễn văn', 'Nguyen-van', 67, 1, 0, '2016-06-24 11:26:51');
INSERT INTO `cms_module_vipmember_items` VALUES ('0966539886', 'null', 'Hà nội', '1992', 130, 'Nguyễn văn tuân', 'Nguyen-van-tuan', 68, 1, 0, '2016-06-24 19:45:27');
INSERT INTO `cms_module_vipmember_items` VALUES ('0988864026', 'Ms.', 'Ha noi', '1990', 131, 'Ha huong', 'Ha-huong', 69, 1, 0, '2016-07-04 19:57:01');
INSERT INTO `cms_module_vipmember_items` VALUES ('01646952599', 'Ms.', '48A tăng bạt hổ', '1993', 132, 'Nguyễn Thị Phương Anh', 'Nguyen-Thi-Phuong-Anh', 70, 1, 0, '2016-07-05 00:22:02');
INSERT INTO `cms_module_vipmember_items` VALUES ('0916466701', 'Ms.', 'Ha Noi', '1980', 133, 'Phan Trang', 'Phan-Trang', 71, 1, 0, '2016-07-09 14:46:36');
INSERT INTO `cms_module_vipmember_items` VALUES ('01667746666', 'Mr.', '334 kim ngưu', '1988', 134, 'Nguyễn duy hưng', 'Nguyen-duy-hung', 72, 1, 0, '2016-07-16 13:27:15');
INSERT INTO `cms_module_vipmember_items` VALUES ('0919268472', 'Ms.', 'p306-C7-trần huy liệu - giảng võ - ba đình - hà nội', '1991', 136, 'Phan Thùy Trang', 'Phan-Thuy-Trang', 74, 1, 0, '2016-07-18 16:26:41');
INSERT INTO `cms_module_vipmember_items` VALUES ('0988746046', 'Mr.', '29H, 37/27, Dịch Vọng, Cầu Giấy, Hà Nội', '1989', 137, 'Nguyễn Đăng Hải', 'Nguyen-dang-Hai', 74, 1, 0, '2016-07-20 12:12:40');
INSERT INTO `cms_module_vipmember_items` VALUES ('0983262210', 'Mr.', '2203 nhà 24T2 Hoàng Đạo Thuý - Cầu Giấy - Hà Nội', '1986', 138, 'Đinh Văn Thắng', 'dinh-Van-Thang', 75, 1, 0, '2016-07-20 17:47:31');
INSERT INTO `cms_module_vipmember_items` VALUES ('0918812490', 'Ms.', 'Hà Nội', '1990', 139, 'Trần Thị Hồng Ngọc', 'Tran-Thi-Hong-Ngoc', 76, 1, 0, '2016-07-23 18:40:42');
INSERT INTO `cms_module_vipmember_items` VALUES ('0943937464', 'Mr.', 'Tập thể viện Nhi TW', '1992', 142, 'Phạm Quốc Huy ', 'Pham-Quoc-Huy', 79, 1, 0, '2016-07-24 03:04:19');
INSERT INTO `cms_module_vipmember_items` VALUES ('0976124987', 'Mr.', 'phòng 409 - E3 - Phương Mai - Đống Đa - Hà Nội', '1987', 143, 'Nguyễn Xuân Anh', 'Nguyen-Xuan-Anh', 79, 1, 0, '2016-08-08 17:33:56');
INSERT INTO `cms_module_vipmember_items` VALUES ('0985395700', 'Ms.', '109 trần hưng đạo', '1983', 144, 'Quynh', 'Quynh', 80, 1, 0, '2016-08-11 15:41:28');
INSERT INTO `cms_module_vipmember_items` VALUES ('0988130926', 'Ms.', 'Viện Khoa học Lâm nghiệp Việt Nam - 46 Đức Thắng - Bắc Từ Liêm -', '1982', 145, 'Ms. Hà', 'Ms-Ha', 80, 1, 0, '2016-08-12 10:07:40');
INSERT INTO `cms_module_vipmember_items` VALUES ('0947658086', 'Ms.', '153 nghi tàm tây hồ', '1988', 147, 'Nguyễn Mai Chi', 'Nguyen-Mai-Chi', 81, 1, 0, '2016-08-19 17:35:42');
INSERT INTO `cms_module_vipmember_items` VALUES ('01658565835', 'Mr.', 'Hoàng Hoa Thám, Tây Hồ, Hà Nội', '1991', 148, 'Phạm Nhật Anh', 'Pham-Nhat-Anh', 82, 1, 0, '2016-08-21 02:09:53');
INSERT INTO `cms_module_vipmember_items` VALUES ('0905158333', 'null', 'Yen Hoa Cau giay', '1986', 149, 'Tran Ngoc Quy', 'Tran-Ngoc-Quy', 83, 1, 0, '2016-08-21 08:21:15');
INSERT INTO `cms_module_vipmember_items` VALUES ('0949944521', 'Ms.', '16 ngách 1 ngõ 107 đường Lĩnh Nam quận Hoàng Mai Hà Nội', '1991', 150, 'Dương Huyền Linh', 'Duong-Huyen-Linh', 84, 1, 0, '2016-08-21 16:04:22');
INSERT INTO `cms_module_vipmember_items` VALUES ('0912257559', 'Ms.', '24 Han Thuyen', '1978', 152, 'Pham Thu Nga', 'Pham-Thu-Nga', 85, 1, 0, '2016-08-29 17:30:11');
INSERT INTO `cms_module_vipmember_items` VALUES ('0983771087', 'Mr.', '49 ngọc thụy, long biên', '1987', 153, 'Phạm Đức Bình', 'Pham-duc-Binh', 86, 1, 0, '2016-08-31 17:46:58');
INSERT INTO `cms_module_vipmember_items` VALUES ('09143833332', 'Ms.', 'Quang Trung, Hà Đông', '1988', 154, 'Ta Thu Hang', 'Ta-Thu-Hang', 87, 1, 0, '2016-09-02 17:57:49');
INSERT INTO `cms_module_vipmember_items` VALUES ('0976688566', 'null', 'Tiên dương đông anh ', '1077', 155, 'Phạm thị Vân', 'Pham-thi-Van', 88, 1, 0, '2016-09-03 14:24:07');
INSERT INTO `cms_module_vipmember_items` VALUES ('0974044551', 'Ms.', 'Thanh trì, Hà nội', '1987', 156, 'Nguyễn Thị Thanh Ngà', 'Nguyen-Thi-Thanh-Nga', 89, 1, 0, '2016-09-04 00:36:36');
INSERT INTO `cms_module_vipmember_items` VALUES ('0936483880', 'Ms.', 'Phòng 205, cầu thang 2, nhà 222C ngõ 260 Đội Cấn, Ba Đình', '1980', 157, 'Vũ Hải Bình', 'Vu-Hai-Binh', 90, 1, 0, '2016-09-07 22:57:15');
INSERT INTO `cms_module_vipmember_items` VALUES ('0904305868', 'Mr.', 'Phòng 106, A14, ngõ 106 Hoàng Quốc Việt Cầu Giấy HN', '1961', 158, 'Nguyễn Xuân Sơn', 'Nguyen-Xuan-Son', 91, 1, 0, '2016-09-09 21:33:52');
INSERT INTO `cms_module_vipmember_items` VALUES ('0962535889', 'null', 'Nhà số 6 ngách 65/39 Mai Dịch- cầu giấy- hà nội', '1990', 159, 'Phan thị hương', 'Phan-thi-huong', 92, 1, 0, '2016-09-10 17:36:57');
INSERT INTO `cms_module_vipmember_items` VALUES ('0983698073', 'Ms.', 'Chung cư học viện hậu cần , ngọc thụy , long biên', '1973', 160, 'Trần thị hoa', 'Tran-thi-hoa', 93, 1, 0, '2016-09-13 21:26:28');
INSERT INTO `cms_module_vipmember_items` VALUES ('0903448553', 'Mr.', '181 Nguyễn Trãi,Hà Nội', '1984', 161, 'Nguyễn Mạnh Linh', 'Nguye-n-Ma-nh-Linh', 94, 1, 0, '2016-09-14 11:47:20');
INSERT INTO `cms_module_vipmember_items` VALUES ('01646994209', 'Ms.', '27 ngõ 466 Đê La Thành, Đống Đa, Hà Nội', '1988', 162, 'Phạm Quỳnh Anh', 'Pham-Quynh-Anh', 95, 1, 0, '2016-09-14 13:24:24');
INSERT INTO `cms_module_vipmember_items` VALUES ('01698678198', 'Ms.', '13/4 cát Linh', '1989', 163, 'Lê Thị Thu Trang', 'Le-Thi-Thu-Trang', 96, 1, 0, '2016-09-14 17:25:46');
INSERT INTO `cms_module_vipmember_items` VALUES ('01655528691', 'Ms.', 'Sen 60 Lý Thái Tổ ', '1991', 164, 'Khổng Thúy Hằng ', 'Khong-Thuy-Hang', 97, 1, 0, '2016-09-26 21:26:07');
INSERT INTO `cms_module_vipmember_items` VALUES ('01689999567', 'Ms.', 'Số 205 Nguyễn Ngọc Vũ, Trung Hoà, Cầu Giấy, HN', '1986', 167, 'O', 'O', 99, 1, 0, '2016-10-06 20:15:06');
INSERT INTO `cms_module_vipmember_items` VALUES ('0963465538', 'Ms.', 'Hiểm 173/68/41 hoàng hoa thám bảo đình hà nội', '1968', 262, 'Đào Thị Thanh Hằng ', 'da-o-Thi-Thanh-Ha-ng', 210, 1, 0, '2017-07-06 22:04:23');
INSERT INTO `cms_module_vipmember_items` VALUES ('0932012223', 'Mr.', '46/12/1 tan Hoa 2 hiep phu q 9', '1978', 169, 'Minh', 'Minh', 100, 1, 0, '2016-10-10 21:47:49');
INSERT INTO `cms_module_vipmember_items` VALUES ('0986385798', 'Mr.', 'thai ha ', '1986', 171, 'Bui Hong Minh', 'Bui-Hong-Minh', 101, 1, 0, '2016-10-16 09:53:20');
INSERT INTO `cms_module_vipmember_items` VALUES ('0982897826', 'Ms.', '147 hoang hoa tham, ba dinh, ha noi', '1988', 172, 'Đặng Ly', 'dang-Ly', 102, 1, 0, '2016-10-17 23:26:59');
INSERT INTO `cms_module_vipmember_items` VALUES ('0983431880', 'Ms.', 'số 14 nghách 158/1 phố ngọc hà ba đình hà nội', '1969', 174, '??ngmaih??ng', 'ngmaih-ng', 102, 1, 0, '2016-10-19 09:14:15');
INSERT INTO `cms_module_vipmember_items` VALUES ('0989631979', 'Ms.', 'Dinh cong ha noi', '1955', 175, 'Dao thi hoat', 'Dao-thi-hoat', 103, 1, 0, '2016-10-21 19:26:42');
INSERT INTO `cms_module_vipmember_items` VALUES ('01686545488', 'Ms.', '29 Nguyễn Bỉnh KHiêm, Hai Bà Trưng, Hà Nội', '1980', 176, 'Trần Thị Tuyết Lan', 'Tran-Thi-Tuyet-Lan', 104, 1, 0, '2016-10-26 17:14:10');
INSERT INTO `cms_module_vipmember_items` VALUES ('0914500051', 'Ms.', 'Tôn đức thắng', '1985', 177, 'Hoàng thanh Phương', 'Hoang-thanh-Phuong', 105, 1, 0, '2016-10-29 13:40:15');
INSERT INTO `cms_module_vipmember_items` VALUES ('01687508057', 'Mr.', 'Hoàng Mai, Hà Nội', '1995', 178, 'Nguyễn Ngọc Vũ', 'Nguyen-Ngoc-Vu', 106, 1, 0, '2016-11-01 13:25:41');
INSERT INTO `cms_module_vipmember_items` VALUES ('0934716965', 'Ms.', 'P404, tầng 4, toà nhà Rainbow, Văn Quán, Hà Đông', '1989', 179, 'Phạm Thị Huyền Trang', 'Pham-Thi-Huyen-Trang', 107, 1, 0, '2016-11-08 00:38:49');
INSERT INTO `cms_module_vipmember_items` VALUES ('01667102446', 'Ms.', 'Yên Hoà, Cầu Giấy, Hà Nội', '1994', 180, 'Nguyễn Thị Thu Hà', 'Nguyen-Thi-Thu-Ha', 108, 1, 0, '2016-11-13 19:20:25');
INSERT INTO `cms_module_vipmember_items` VALUES ('0933254339', 'Mr.', '254/33/99 BẾN VÂN ĐỒN  P.5, Q.4, TP.HCM', '1989', 181, 'TRẦN VĂN MINH', 'TRaN-VaN-MINH', 109, 1, 0, '2016-11-18 09:57:00');
INSERT INTO `cms_module_vipmember_items` VALUES ('0905158333', 'null', 'Yen Hoa Cau Giay', '1986', 182, 'Tran Ngoc Quy', 'Tran-Ngoc-Quy', 110, 1, 0, '2016-11-18 18:07:18');
INSERT INTO `cms_module_vipmember_items` VALUES ('0904368418', 'null', '168 Hào Nam', '', 183, '1985', '1985', 111, 1, 0, '2016-11-26 19:05:01');
INSERT INTO `cms_module_vipmember_items` VALUES ('0989181291', 'Mr.', 'Ngọc thuỵ long biên hà nội', '1991', 185, 'Hoàng văn thà', 'Hoang-van-tha', 112, 1, 0, '2016-11-30 09:48:44');
INSERT INTO `cms_module_vipmember_items` VALUES ('0913542001', 'Ms.', '34 Đại Cồ Việt, Hai Bà Trưng, Hà Nội', '1988', 186, 'Nguyễn Thị Thu Cúc', 'Nguyen-Thi-Thu-Cuc', 113, 1, 0, '2016-11-30 11:57:43');
INSERT INTO `cms_module_vipmember_items` VALUES ('01669972666', 'Ms.', 'Thường Tín - Hà Nội', '1983', 189, 'Dương Thị Minh Toàn', 'Duong-Thi-Minh-Toan', 115, 1, 0, '2016-12-07 09:39:47');
INSERT INTO `cms_module_vipmember_items` VALUES ('0983911828', 'Ms.', 'Số 10 ngõ Yên Thế, Văn Miếu, Đống Đa, Hà Nội', '1983', 191, 'Quang Thu Nguyệt', 'Quang-Thu-Nguyet', 116, 1, 0, '2016-12-16 13:32:27');
INSERT INTO `cms_module_vipmember_items` VALUES ('01649544541', 'Mr.', '564 nguyễn văn cừ, long biên', '1991', 192, 'Lê Văn Cường', 'Le-Van-Cuong', 117, 1, 0, '2016-12-27 09:21:45');
INSERT INTO `cms_module_vipmember_items` VALUES ('09876655555444', 'Mr.', '123 gdgsh', '1977', 193, 'thien', 'thien', 118, 1, 0, '2016-12-30 19:24:56');
INSERT INTO `cms_module_vipmember_items` VALUES ('0936386623', 'Ms.', 'Ngõ 249 đội cấn ba đình hà nội', '1984', 261, 'Nguyễn Huyền Anh', 'Nguye-n-Huye-n-Anh', 209, 1, 0, '2017-07-06 22:02:53');
INSERT INTO `cms_module_vipmember_items` VALUES ('0989669411', 'Ms.', '273 Kim mã ba đình hà nội ', '1987', 195, 'Nguyễn Minh Trang', 'Nguye-n-Minh-Trang', 119, 1, 0, '2017-01-03 11:14:30');
INSERT INTO `cms_module_vipmember_items` VALUES ('0936687588', 'Mr.', 'Hà Nội', '1993', 196, 'Ph?m Anh Vinh', 'Ph-m-Anh-Vinh', 120, 1, 0, '2017-01-08 11:06:11');
INSERT INTO `cms_module_vipmember_items` VALUES ('0936687588', 'Mr.', 'Hà Nội', '1993', 197, 'Ph?m Anh Vinh', 'Ph-m-Anh-Vinh', 121, 1, 0, '2017-01-08 11:06:12');
INSERT INTO `cms_module_vipmember_items` VALUES ('01674533429', 'Ms.', '3b, quang trung, hoàn kiếm, hà nội', '1991', 198, 'Hoàng Thị Thanh Nga', 'Hoang-Thi-Thanh-Nga', 122, 1, 0, '2017-01-20 12:21:53');
INSERT INTO `cms_module_vipmember_items` VALUES ('0912955100', 'null', '101 C2 khu đô thị mới Đại kim, hoàng mai, Hà nội', '1978', 199, 'Trần Việt Cường', 'Tran-Viet-Cuong', 122, 1, 0, '2017-01-26 00:48:13');
INSERT INTO `cms_module_vipmember_items` VALUES ('0904669636', 'Mr.', 'Số 93 ngách 72/73 Quan Nhân', '1986', 202, 'Đặng Quang Tùng', 'dang-Quang-Tung', 124, 1, 0, '2017-02-08 00:39:15');
INSERT INTO `cms_module_vipmember_items` VALUES ('0923739888', 'Mr.', 'Phòng 510 D8, Thành Công, Hà Nội ', '1977', 203, 'Đào Mạnh Chiến ', 'dao-Manh-Chien', 124, 1, 0, '2017-02-13 21:35:42');
INSERT INTO `cms_module_vipmember_items` VALUES ('01682322999', 'Mr.', '35 ngách 165/23 Dương Quảng Hàm, Quan Hoa, Cầu Giấy, HN', '1982', 204, 'Vũ Xuân Vinh', 'Vu-Xuan-Vinh', 125, 1, 0, '2017-02-14 16:16:05');
INSERT INTO `cms_module_vipmember_items` VALUES ('0942777866', 'Ms.', 'Số nhà 87, ngõ 171 Nguyễn Ngọc Vũ, Cầu Giấy, Hà Nội', '1991', 206, 'Ninh Loan', 'Ninh-Loan', 126, 1, 0, '2017-02-22 10:27:20');
INSERT INTO `cms_module_vipmember_items` VALUES ('0948057157', 'Ms.', '23 Pha nChu Trinh', '1989', 207, 'Nguyen Phuong Van', 'Nguyen-Phuong-Van', 127, 1, 0, '2017-02-27 12:25:40');
INSERT INTO `cms_module_vipmember_items` VALUES ('0983652703', 'Ms.', '34 cua nam', '1975', 208, 'Thai HIen', 'Thai-HIen', 128, 1, 0, '2017-03-03 13:05:19');
INSERT INTO `cms_module_vipmember_items` VALUES ('0983587233', 'null', 'Hà nội', '', 209, 'Nguyễn Thành', 'Nguyen-Thanh', 129, 1, 0, '2017-03-03 16:30:57');
INSERT INTO `cms_module_vipmember_items` VALUES ('0984221194', 'Mr.', 'Dân tiến khoái châu hưng yên ', '1994', 210, 'Nguyễn tuấn anh', 'Nguyen-tuan-anh', 130, 1, 0, '2017-03-10 18:50:09');
INSERT INTO `cms_module_vipmember_items` VALUES ('0977787583', 'Ms.', 'Số 1 Trần Thánh Tông, Hai Bà Trưng, HN', '1984', 212, 'Lê Thị Linh', 'Le-Thi-Linh', 131, 1, 0, '2017-03-16 11:27:48');
INSERT INTO `cms_module_vipmember_items` VALUES ('0906186944', 'Ms.', '609 Trương định ', '1969', 214, 'dương thị kim loan', 'duong-thi-kim-loan', 133, 1, 0, '2017-03-28 10:07:36');
INSERT INTO `cms_module_vipmember_items` VALUES ('01677796811', 'Mr.', 'Tầng 7, tòa nhà Prime Center số 53 Quang Trung, phường Nguyễn Du', '1988', 215, 'Nguyễn Thanh Tùng', 'Nguyen-Thanh-Tung', 133, 1, 0, '2017-03-29 13:46:34');
INSERT INTO `cms_module_vipmember_items` VALUES ('01696847289', 'null', '229 Phố Vọng', '2000', 216, 'Dinh thu huong', 'Dinh-thu-huong', 134, 1, 0, '2017-04-07 19:05:38');
INSERT INTO `cms_module_vipmember_items` VALUES ('0986333436', 'Ms.', 'Hà Nội', '1986', 217, 'Doãn Thị Thanh Mai', 'Doan-Thi-Thanh-Mai', 135, 1, 0, '2017-04-13 17:13:48');
INSERT INTO `cms_module_vipmember_items` VALUES ('0904548986', 'Ms.', 'Viện Bạch Mai', '1986', 218, 'Nguyễn Thị Phượng ', 'Nguyen-Thi-Phuong', 136, 1, 0, '2017-04-13 20:38:31');
INSERT INTO `cms_module_vipmember_items` VALUES ('01639596621', 'Mr.', 'Mễ Trì Hà Nội', '1993', 219, 'nguyễn thanh hiệu', 'nguyen-thanh-hieu', 137, 1, 0, '2017-04-19 22:53:57');
INSERT INTO `cms_module_vipmember_items` VALUES ('0972230983', 'null', 'Từ Sơn - Bắc Ninh', '1983', 220, 'Nguyễn Khắc Chính', 'Nguyen-Khac-Chinh', 138, 1, 0, '2017-04-19 23:30:27');
INSERT INTO `cms_module_vipmember_items` VALUES ('0978119900', 'Mr.', '6 nguyễn công hoan', '1988', 221, 'Nguyễn Trung Hiếu', 'Nguyen-Trung-Hieu', 139, 1, 0, '2017-04-20 09:30:46');
INSERT INTO `cms_module_vipmember_items` VALUES ('0974642249', 'Mr.', '629 phúc diễn hà nội', '1988', 224, 'nguyễn tràng lợi', 'nguyen-trang-loi', 142, 1, 0, '2017-04-23 13:15:16');
INSERT INTO `cms_module_vipmember_items` VALUES ('0936862928', 'Ms.', 'Thanh Xuân Hà Nội', '1990', 225, 'Phan Thị Hoa', 'Phan-Thi-Hoa', 142, 1, 0, '2017-04-24 18:07:46');
INSERT INTO `cms_module_vipmember_items` VALUES ('01648624551', 'Ms.', 'Ha noi', '1995', 226, 'Nguyen Thi Hang', 'Nguyen-Thi-Hang', 143, 1, 0, '2017-04-24 19:38:44');
INSERT INTO `cms_module_vipmember_items` VALUES ('0936386632', 'Ms.', 'Số 8 phạm hùng hà nội ', '1977', 259, 'Nguyễn Xuân Chung', 'Nguye-n-Xuan-Chung', 207, 1, 0, '2017-07-06 22:00:15');
INSERT INTO `cms_module_vipmember_items` VALUES ('0983696216', 'Mr.', 'Thanh Tri Ha noi', '1982', 228, 'Tran manh hong', 'Tran-manh-hong', 143, 1, 0, '2017-04-26 09:58:15');
INSERT INTO `cms_module_vipmember_items` VALUES ('0945885583', 'Mr.', '65B - Tôn Đức Thắng - Hà Nội', '1983', 229, 'Vũ Đại Thắng', 'Vu-dai-Thang', 144, 1, 0, '2017-04-26 12:46:10');
INSERT INTO `cms_module_vipmember_items` VALUES ('0975819662', 'Mr.', '362 Trần Khát CHân', '1987', 230, 'Nguyễn Quốc Phương', 'Nguyen-Quoc-Phuong', 145, 1, 0, '2017-04-27 10:35:13');
INSERT INTO `cms_module_vipmember_items` VALUES ('0932316526', 'Ms.', '46 Trần Kim Xuyến- Trung Hòa- Hà Nội', '1989', 231, 'Mai Thị Hoài Thanh', 'Mai-Thi-Hoai-Thanh', 146, 1, 0, '2017-04-27 16:36:24');
INSERT INTO `cms_module_vipmember_items` VALUES ('01275720168', 'Ms.', '35 ngõ 18 định công thượng', '1986', 232, 'Đặng Tuyết Ly', 'dang-Tuyet-Ly', 147, 1, 0, '2017-04-28 11:09:33');
INSERT INTO `cms_module_vipmember_items` VALUES ('0948588338', 'Mr.', '120 hoàng quốc việt', '1990', 233, 'Lê anh dũng', 'Le-anh-dung', 148, 1, 0, '2017-05-03 20:28:42');
INSERT INTO `cms_module_vipmember_items` VALUES ('01644712676', 'Ms.', '54A-Nguyễn Chí Thanh-Hà Nội', '1993', 234, 'Văn Thị Thúy Hằng', 'Van-Thi-Thuy-Hang', 149, 1, 0, '2017-05-04 09:02:36');
INSERT INTO `cms_module_vipmember_items` VALUES ('0979814865', 'Ms.', 'Số 8 ngõ 84 đường Nghi Tàm, Tây Hồ, Hà Nội', '1996', 236, 'Lê Khánh Linh', 'Le-Khanh-Linh', 150, 1, 0, '2017-05-12 15:55:54');
INSERT INTO `cms_module_vipmember_items` VALUES ('0912070702', 'Ms.', '815 Giải Phóng', '1976', 237, 'Phạm Vân Nga', 'Pham-Van-Nga', 151, 1, 0, '2017-05-12 17:22:20');
INSERT INTO `cms_module_vipmember_items` VALUES ('0868729599', 'Mr.', 'Royal City', '1991', 238, 'Adam Loc', 'Adam-Loc', 152, 1, 0, '2017-05-13 15:30:26');
INSERT INTO `cms_module_vipmember_items` VALUES ('0948100186', 'Ms.', '2405b1 chung cư hoà bình green city, 505 minh khai Hà nội', '1986', 239, 'Nguyễn Thuỳ linh ', 'Nguyen-Thuy-linh', 153, 1, 0, '2017-05-13 20:45:52');
INSERT INTO `cms_module_vipmember_items` VALUES ('0980961218', 'Ms.', 'Thượng Thanh Long Biên', '1981', 240, 'Ngô Phương Dung', 'Ngo-Phuong-Dung', 154, 1, 0, '2017-05-23 11:42:18');
INSERT INTO `cms_module_vipmember_items` VALUES ('0949426669', 'Mr.', 'Số 45A tổ 16 Hạ đình', '1991', 241, 'Nguyễn Tiến Dũng', 'Nguyen-Tien-Dung', 155, 1, 0, '2017-05-23 18:10:40');
INSERT INTO `cms_module_vipmember_items` VALUES ('0965766666', 'Ms.', '260-262 Bà triệu - Q hai bà trưng - Hn ', '1982', 242, 'Pham Quynh', 'Pham-Quynh', 156, 1, 0, '2017-05-26 21:08:01');
INSERT INTO `cms_module_vipmember_items` VALUES ('0962910606', 'Mr.', '609 Trương Định Hà Nội', '1978', 243, 'Trịnh Trọng Đat', 'Trinh-Trong-dat', 157, 1, 0, '2017-05-29 13:08:32');
INSERT INTO `cms_module_vipmember_items` VALUES ('0973580043', 'Mr.', '52 Lương Yên', '1990', 244, 'Nguyễn Đức Hạnh', 'Nguyen-duc-Hanh', 158, 1, 0, '2017-05-31 18:01:57');
INSERT INTO `cms_module_vipmember_items` VALUES ('0904870123', 'Ms.', '131 bui thi xuan', '1982', 245, 'Ngo Que Huong', 'Ngo-Que-Huong', 159, 1, 0, '2017-06-01 15:56:45');
INSERT INTO `cms_module_vipmember_items` VALUES ('01202804946', 'Mr.', 'Chau thanh_tien giang', '1990', 266, 'Le nguyen phuc loc', 'Le-nguyen-phuc-loc', 213, 1, 0, '2017-07-21 16:10:01');
INSERT INTO `cms_module_vipmember_items` VALUES ('0936386632', 'Mr.', 'Số 8 phạm hùng hà nội', '1977', 260, 'Nguyễn Xuân Chung ', 'Nguye-n-Xuan-Chung', 208, 1, 0, '2017-07-06 22:01:39');
INSERT INTO `cms_module_vipmember_items` VALUES ('0906969688', 'Mr.', 'số 50 ngõ 100 trần duy hưng', '1988', 248, 'Trần Xuân Bách', 'Tran-Xuan-Bach', 161, 1, 0, '2017-06-11 20:44:59');
INSERT INTO `cms_module_vipmember_items` VALUES ('01296952639', 'Ms.', '29 Hang Than', '1997', 249, 'Nguyen Khanh Linh', 'Nguyen-Khanh-Linh', 161, 1, 0, '2017-06-12 20:45:07');
INSERT INTO `cms_module_vipmember_items` VALUES ('01227279966', 'Mr.', 'Ngõ 196 Cầu Giấy Hà Nội', '1992', 250, 'Nguyễn Việt Hùng', 'Nguyen-Viet-Hung', 162, 1, 0, '2017-06-14 09:47:29');
INSERT INTO `cms_module_vipmember_items` VALUES ('0982739958', 'Ms.', 'Kim Giang thanh xuân hn', '1984', 251, 'Phan Thuỳ anh', 'Phan-Thuy-anh', 163, 1, 0, '2017-06-14 10:05:49');
INSERT INTO `cms_module_vipmember_items` VALUES ('0935395665', 'Ms.', 'Hai Bà Trưng Hà Nội', '1987', 252, 'Lê Thị Thu Nguyệt', 'Le-Thi-Thu-Nguyet', 164, 1, 0, '2017-06-17 13:54:03');
INSERT INTO `cms_module_vipmember_items` VALUES ('0902198619', 'Mr.', 'Ngõ 53 - Quan Nhân - Thanh Xuân - Hà Nội', '1982', 253, 'Trần Quang Anh', 'Tran-Quang-Anh', 165, 1, 0, '2017-06-18 06:30:36');
INSERT INTO `cms_module_vipmember_items` VALUES ('01695248557', 'Ms.', '', '2007', 254, 'tongkhanhlinh', 'tongkhanhlinh', 166, 1, 0, '2017-06-19 08:47:07');
INSERT INTO `cms_module_vipmember_items` VALUES ('0973823335', 'Mr.', 'Số 11 ngõ 127 tổ dân phố 3 phùng khoang- phường trung văn - quận', '1983', 255, 'Nguyễn văn tín', 'Nguyen-van-tin', 167, 1, 0, '2017-06-21 17:40:25');
INSERT INTO `cms_module_vipmember_items` VALUES ('0972052688', 'Ms.', 'Số 11A Ngõ 2 Quang Trung, Hà Đông, Hà Nội', '1982', 256, 'Nguyễn Thị Bích Thuỷ', 'Nguyen-Thi-Bich-Thuy', 168, 1, 0, '2017-06-21 18:16:20');
INSERT INTO `cms_module_vipmember_items` VALUES ('0906153186', 'Mr.', '25 lý thường kiệt', '1986', 257, 'Nguyễn Văn Huy', 'Nguyen-Van-Huy', 169, 1, 0, '2017-06-22 10:47:44');
INSERT INTO `cms_module_vipmember_items` VALUES ('0945995588', 'null', 'cầu giấy', '1976', 258, 'phạm thị Thu Hoài', 'pham-thi-Thu-Hoai', 170, 1, 0, '2017-06-23 19:34:21');
INSERT INTO `cms_module_vipmember_items` VALUES ('0982343743', 'Ms.', 'Hanoi', '1984', 267, 'Tran Thu Giang', 'Tran-Thu-Giang', 214, 1, 0, '2017-07-21 18:57:15');
INSERT INTO `cms_module_vipmember_items` VALUES ('0902071115', 'Mr.', 'Giai phóng Hà nội', '1978', 269, 'Phạm khanh huy', 'Pha-m-khanh-huy', 215, 1, 0, '2017-08-05 12:50:44');
INSERT INTO `cms_module_vipmember_items` VALUES ('01277243278', 'Ms.', 'Hà Đông, Hà nội ', '1991', 270, 'BÙi Thị THu Phương', 'Bui-Thi-THu-Phuong', 216, 1, 0, '2017-08-11 10:32:22');
INSERT INTO `cms_module_vipmember_items` VALUES ('0904512698', 'Ms.', '31, ngõ 32, Phan Văn Trường, Hà Nội', '1978', 339, 'Đinh Thị Quỳnh Dung', 'dinh-Thi-Quynh-Dung', 283, 1, 0, '2019-07-20 11:29:07');
INSERT INTO `cms_module_vipmember_items` VALUES ('0903491599', 'Mr.', '60 Trần Phú, Ba Đình, Hà Nội', '', 274, 'NHÂM NGỌC HIỂN', 'NHaM-NGoC-HIeN', 218, 1, 0, '2018-01-02 13:54:38');
INSERT INTO `cms_module_vipmember_items` VALUES ('0934599957', 'Ms.', 'Linh lang cong vi', '1988', 275, 'Bui thi hanh', 'Bui-thi-hanh', 218, 1, 0, '2018-01-06 12:09:56');
INSERT INTO `cms_module_vipmember_items` VALUES ('0936688085', 'null', 'Hoàn kiếm -hn', '1988', 276, 'Nguyễn cẩm vân', 'Nguyen-cam-van', 219, 1, 0, '2018-01-13 19:48:49');
INSERT INTO `cms_module_vipmember_items` VALUES ('0915393959', 'null', 'số 2 Láng Hạ', '', 278, 'Nguyễn Thị Hồng Vân', 'Nguyen-Thi-Hong-Van', 220, 1, 0, '2018-02-09 10:49:20');
INSERT INTO `cms_module_vipmember_items` VALUES ('0946960868', 'null', 'Ngõ chợ Khâm Thiên, Đống Đa, Hà Nội', '1989', 279, 'Nguyễn Trung Hiếu', 'Nguyen-Trung-Hieu', 221, 1, 0, '2018-02-10 21:26:00');
INSERT INTO `cms_module_vipmember_items` VALUES ('0972373597', 'Ms.', 'A2711, 219 Trung Kính, Hà Nội ', '2000', 281, 'Tăng Trần Diệu Linh', 'Tang-Tran-Dieu-Linh', 222, 1, 0, '2018-02-18 23:42:14');
INSERT INTO `cms_module_vipmember_items` VALUES ('0912951968  ', 'Ms.', '216  phố  Huế  ', '1969', 282, 'Phạm  nga  ', 'Pham-nga', 223, 1, 0, '2018-02-20 08:40:27');
INSERT INTO `cms_module_vipmember_items` VALUES ('0918657159', 'Mr.', 'B', '1988', 283, 'A', 'A', 224, 1, 0, '2018-02-26 12:36:45');
INSERT INTO `cms_module_vipmember_items` VALUES ('0918657159', 'Mr.', 'Tầng 16 Tòa nhà VIT 519 Kim Mã', '1988', 284, 'Trương Quang Quý', 'Truong-Quang-Quy', 225, 1, 0, '2018-02-28 15:49:38');
INSERT INTO `cms_module_vipmember_items` VALUES ('0942361661', 'Mr.', 'Lô36 LK3 - đường lê xuân điệp - Khu đấu giá mậu lương - phường k', '1987', 285, 'Nguyễn Huy Lộc', 'Nguyen-Huy-Loc', 226, 1, 0, '2018-03-07 19:46:25');
INSERT INTO `cms_module_vipmember_items` VALUES ('0901552211', 'Ms.', '212 hàng bông', '1994', 287, 'Bùi thị hồng', 'Bui-thi-hong', 227, 1, 0, '2018-03-14 12:44:17');
INSERT INTO `cms_module_vipmember_items` VALUES ('0987095588', 'Ms.', 'Bt1 C02 khu đô thị splendora an khánh HN', '1986', 288, 'Thanh Ngoc', 'Thanh-Ngoc', 228, 1, 0, '2018-03-14 22:29:18');
INSERT INTO `cms_module_vipmember_items` VALUES ('0902115275', 'Mr.', '860 Bạch Đằng,  Hà Nội ', '1975', 290, 'NGUYỄN QUANG ĐẠO ', 'NGUY-N-QUANG-daO', 229, 1, 0, '2018-03-16 02:03:42');
INSERT INTO `cms_module_vipmember_items` VALUES ('0988631925', 'null', '75 tam trinh', '', 291, 'Pham ha giang', 'Pham-ha-giang', 230, 1, 0, '2018-03-25 19:48:26');
INSERT INTO `cms_module_vipmember_items` VALUES ('0986363888', 'Ms.', '33 Chùa Láng', '1988', 292, 'Tran Thanh Hang', 'Tran-Thanh-Hang', 231, 1, 0, '2018-03-26 12:37:16');
INSERT INTO `cms_module_vipmember_items` VALUES ('0982425794', 'Mr.', '212 - 94/23 - 94 - Tân Mai - Hà Nôi', '1994', 293, 'Nguyễn Hữu Đức Tùng', 'Nguyen-Huu-duc-Tung', 232, 1, 0, '2018-03-31 20:38:23');
INSERT INTO `cms_module_vipmember_items` VALUES ('0944384657', 'Ms.', '20 ngô quyền, hoàn kiếm, hà nội', '1990', 295, 'Vũ Kim Cúc', 'Vu-Kim-Cuc', 233, 1, 0, '2018-04-03 14:13:32');
INSERT INTO `cms_module_vipmember_items` VALUES ('0915089669', 'Ms.', 'Số 17 ngõ 175 Định Công', '1978', 296, 'Võ Thu Trang', 'Vo-Thu-Trang', 234, 1, 0, '2018-04-03 18:16:24');
INSERT INTO `cms_module_vipmember_items` VALUES ('0979900825', 'Ms.', 'P1801 CT1, CC Bắc Hà, Mộ Lao, Hà Đông. Hà Nội', '1974', 297, 'Đoàn Thị Minh Thiêm', 'doan-Thi-Minh-Thiem', 235, 1, 0, '2018-04-06 10:55:45');
INSERT INTO `cms_module_vipmember_items` VALUES ('0906216167', 'Ms.', '283 khuong trung', '1989', 299, 'Vu thi viet hong', 'Vu-thi-viet-hong', 236, 1, 0, '2018-04-28 16:10:43');
INSERT INTO `cms_module_vipmember_items` VALUES ('0979963139', 'Ms.', '101 Văn Cao', '1987', 326, 'Nguyễn Ngọc Mai Anh', 'Nguyen-Ngoc-Mai-Anh', 272, 1, 0, '2019-05-18 01:32:47');
INSERT INTO `cms_module_vipmember_items` VALUES ('0961526991', 'Ms.', 'Chung cư đại thanh', '1992', 327, 'Vũ thị mai phương', 'Vu-thi-mai-phuong', 273, 1, 0, '2019-05-18 13:37:14');
INSERT INTO `cms_module_vipmember_items` VALUES ('0973045279', '', 'Từ Liêm, Hà Nội', '1983', 329, 'Hoàng văn Tú', 'Hoang-van-Tu', 274, 1, 0, '2019-06-22 17:23:03');
INSERT INTO `cms_module_vipmember_items` VALUES ('0989030825', 'Mr.', 'Vietnam', '1992', 316, 'Tungnam', 'Tungnam', 238, 1, 0, '2018-08-28 21:00:22');
INSERT INTO `cms_module_vipmember_items` VALUES ('0948720386', 'Mr.', 'MBBank 21 Cát Linh, Đống Đa, Hà Nội', '1987', 340, 'Nguyễn Thanh Hà', 'Nguyen-Thanh-Ha', 284, 1, 0, '2019-07-21 16:49:25');
INSERT INTO `cms_module_vipmember_items` VALUES ('0979900825', 'Ms.', 'P1801 CT1, chung cư Bắc Hà, Mộ Lao, Hà Đông, Hà Nội', '1974', 341, 'Đoàn Thị Minh Thiêm', 'doan-Thi-Minh-Thiem', 285, 1, 0, '2019-08-01 14:52:52');
INSERT INTO `cms_module_vipmember_items` VALUES ('0936442834', 'Ms.', '590 đường láng, hà nội', '1986', 342, 'Nguyễn thị thanh hoa', 'Nguyen-thi-thanh-hoa', 286, 1, 0, '2019-08-03 12:38:06');
INSERT INTO `cms_module_vipmember_items` VALUES ('0983377021', 'Mr.', 'Xóm bạc kim nỗ đông anh hà nội', '1987', 343, 'Nguyễn duy tuấn', 'Nguyen-duy-tuan', 287, 1, 0, '2019-08-07 13:46:15');
INSERT INTO `cms_module_vipmember_items` VALUES ('0918927718', 'Mr.', '27 Trung kính', '1985', 344, 'PHÙNG Văn Thái', 'PHuNG-Van-Thai', 288, 1, 0, '2019-10-06 10:35:33');
INSERT INTO `cms_module_vipmember_items` VALUES ('0386368700', 'Mr.', 'Số 4 Ngách 12 Ngõ 165 Dương Quảng Hàm - Quan Hoa - Cầu Giấy - Hà', '1991', 346, 'Vương Duy Sơn', 'Vuong-Duy-Son', 290, 1, 0, '2019-10-19 11:26:20');
INSERT INTO `cms_module_vipmember_items` VALUES ('0376122326', 'Ms.', '168 kim giang', '1994', 347, 'Nguyễn thị hiền', 'Nguyen-thi-hien', 291, 1, 0, '2019-11-10 10:38:22');
INSERT INTO `cms_module_vipmember_items` VALUES ('0968006710', 'Ms.', '286 Nguyễn Xiển - Thanh Xuân - Hà Nội', '1994', 353, 'Lê Kim Yến', 'Le-Kim-Yen', 297, 1, 0, '2019-12-26 18:18:39');
INSERT INTO `cms_module_vipmember_items` VALUES ('0979354797', 'Mr.', 'Udic Complex, Hoàng Đạo Thúy, Hà Nội', '1992', 354, 'Hoàng Ngọc Sơn', 'Hoang-Ngoc-Son', 298, 1, 0, '2019-12-28 10:50:05');
INSERT INTO `cms_module_vipmember_items` VALUES ('0976761542', 'Ms.', '17T1 đường Hoàng Đạo Thúy, Trung Hòa, Cầu giấy', '1994', 355, 'Dương Thùy Dung', 'Duong-Thuy-Dung', 299, 1, 0, '2019-12-28 11:13:52');
INSERT INTO `cms_module_vipmember_items` VALUES ('0903277659', 'Ms.', 'Nguyễn xiển', '1980', 360, 'Lã Thúy Quỳnh', 'La-Thuy-Quynh', 304, 1, 0, '2020-04-29 23:42:17');
INSERT INTO `cms_module_vipmember_items` VALUES ('0979877394', 'Ms.', 'Số 218 Lô C5 Đại Kim, Hoàng Mai, Hà Nội', '1993', 361, 'Nguyễn Thị Thảo', 'Nguyen-Thi-Thao', 305, 1, 0, '2020-05-08 10:25:20');
INSERT INTO `cms_module_vipmember_items` VALUES ('0336739933', 'Ms.', '79 Yết Kiêu gia lộc hải dương', '1998', 363, 'Lê thùy dung ', 'Le-thuy-dung', 307, 1, 0, '2020-05-22 15:46:20');
INSERT INTO `cms_module_vipmember_items` VALUES ('0332257782', 'Ms.', 'Hà Nội', '1996', 365, 'Đặng Thị Phương Linh', 'dang-Thi-Phuong-Linh', 309, 1, 0, '2020-06-08 19:04:16');
INSERT INTO `cms_module_vipmember_items` VALUES ('0332257782', 'Ms.', 'Hà Nội', '1996', 366, 'Đặng Thị Phương Linh', 'dang-Thi-Phuong-Linh', 310, 1, 0, '2020-06-08 19:04:16');
INSERT INTO `cms_module_vipmember_items` VALUES ('0332257782', 'Ms.', 'Hà Nội', '1996', 367, 'Đặng Thị Phương Linh', 'dang-Thi-Phuong-Linh', 311, 1, 0, '2020-06-08 19:04:17');
INSERT INTO `cms_module_vipmember_items` VALUES ('0332257782', 'Ms.', 'Hà Nội', '1996', 368, 'Đặng Thị Phương Linh', 'dang-Thi-Phuong-Linh', 312, 1, 0, '2020-06-08 19:04:17');
INSERT INTO `cms_module_vipmember_items` VALUES ('0985818485', 'Mr.', 'Thành trì Hà nội', '1981', 369, 'Nguyễn hữu thọ', 'Nguyen-huu-tho', 313, 1, 0, '2020-06-16 23:28:45');
INSERT INTO `cms_module_vipmember_items` VALUES ('', '', '', '', 371, '', '', 319, 1, 0, '2020-11-27 20:51:27');
INSERT INTO `cms_module_vipmember_items` VALUES ('', '', '', '', 372, '', '', 320, 1, 0, '2021-01-03 04:56:50');
INSERT INTO `cms_module_vipmember_items` VALUES ('', '', '', '', 373, '', '', 321, 1, 0, '2021-01-15 23:59:02');
INSERT INTO `cms_module_vipmember_items` VALUES ('', '', '', '', 374, '', '', 322, 1, 0, '2021-01-26 02:23:42');
INSERT INTO `cms_module_vipmember_items` VALUES ('', '', '', '', 375, '', '', 323, 1, 0, '2021-02-06 13:26:06');
INSERT INTO `cms_module_vipmember_items` VALUES ('', '', '', '', 376, '', '', 324, 1, 0, '2021-03-02 15:40:40');
INSERT INTO `cms_module_vipmember_items` VALUES ('', '', '', '', 377, '', '', 325, 1, 0, '2021-03-06 04:14:01');
INSERT INTO `cms_module_vipmember_items` VALUES ('', '', '', '', 378, '', '', 326, 1, 0, '2021-04-11 20:16:12');
INSERT INTO `cms_module_vipmember_items` VALUES ('', '', '', '', 379, '', '', 327, 1, 0, '2021-04-16 16:32:11');
INSERT INTO `cms_module_vipmember_items` VALUES ('', '', '', '', 380, '', '', 328, 1, 0, '2021-04-21 12:09:09');
INSERT INTO `cms_module_vipmember_items` VALUES ('', '', '', '', 381, '', '', 329, 1, 0, '2021-05-04 12:05:23');
INSERT INTO `cms_module_vipmember_items` VALUES ('', '', '', '', 382, '', '', 330, 1, 0, '2021-05-13 09:56:23');
INSERT INTO `cms_module_vipmember_items` VALUES ('', '', '', '', 383, '', '', 331, 1, 0, '2021-05-20 02:49:45');
INSERT INTO `cms_module_vipmember_items` VALUES ('', '', '', '', 384, '', '', 332, 1, 0, '2021-05-24 00:00:20');
INSERT INTO `cms_module_vipmember_items` VALUES ('', '', '', '', 385, '', '', 333, 1, 0, '2021-06-04 05:44:35');
INSERT INTO `cms_module_vipmember_items` VALUES ('', '', '', '', 386, '', '', 334, 1, 0, '2021-06-24 19:12:22');
INSERT INTO `cms_module_vipmember_items` VALUES ('', '', '', '', 387, '', '', 335, 1, 0, '2021-07-22 22:02:27');
INSERT INTO `cms_module_vipmember_items` VALUES ('', '', '', '', 388, '', '', 336, 1, 0, '2021-07-30 18:35:56');

-- ----------------------------
-- Table structure for cms_module_vipmember_items_seq
-- ----------------------------
DROP TABLE IF EXISTS `cms_module_vipmember_items_seq`;
CREATE TABLE `cms_module_vipmember_items_seq`  (
  `id` int(11) NOT NULL
) ENGINE = MyISAM CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = FIXED;

-- ----------------------------
-- Records of cms_module_vipmember_items_seq
-- ----------------------------
INSERT INTO `cms_module_vipmember_items_seq` VALUES (336);

-- ----------------------------
-- Table structure for cms_modules
-- ----------------------------
DROP TABLE IF EXISTS `cms_modules`;
CREATE TABLE `cms_modules`  (
  `module_name` varchar(160) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `status` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `version` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `admin_only` tinyint(4) NULL DEFAULT 0,
  `active` tinyint(4) NULL DEFAULT NULL,
  `allow_fe_lazyload` tinyint(4) NULL DEFAULT NULL,
  `allow_admin_lazyload` tinyint(4) NULL DEFAULT NULL,
  PRIMARY KEY (`module_name`) USING BTREE,
  INDEX `cms_index_modules_by_module_name`(`module_name`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of cms_modules
-- ----------------------------
INSERT INTO `cms_modules` VALUES ('CMSMailer', 'installed', '5.2.2', 0, 1, 1, 0);
INSERT INTO `cms_modules` VALUES ('CMSPrinting', 'installed', '1.0.5', 0, 1, 0, 1);
INSERT INTO `cms_modules` VALUES ('FileManager', 'installed', '1.4.5', 0, 1, 1, 0);
INSERT INTO `cms_modules` VALUES ('MenuManager', 'installed', '1.8.6', 0, 1, 0, 0);
INSERT INTO `cms_modules` VALUES ('MicroTiny', 'installed', '1.2.9', 0, 1, 1, 1);
INSERT INTO `cms_modules` VALUES ('ModuleManager', 'installed', '1.5.8', 1, 1, 0, 1);
INSERT INTO `cms_modules` VALUES ('News', 'installed', '2.15', 0, 1, 1, 1);
INSERT INTO `cms_modules` VALUES ('Search', 'installed', '1.7.12', 0, 1, 1, 1);
INSERT INTO `cms_modules` VALUES ('ThemeManager', 'installed', '1.1.8', 1, 1, 0, 1);
INSERT INTO `cms_modules` VALUES ('ListIt2', 'installed', '1.4.1', 0, 1, 0, 0);
INSERT INTO `cms_modules` VALUES ('CGExtensions', 'installed', '1.45', 0, 1, 0, 0);
INSERT INTO `cms_modules` VALUES ('CGContentUtils', 'installed', '1.4.5', 1, 1, 0, 0);
INSERT INTO `cms_modules` VALUES ('CGSimpleSmarty', 'installed', '1.7.4', 0, 1, 0, 0);
INSERT INTO `cms_modules` VALUES ('CGSmartImage', 'installed', '1.17.1', 0, 1, 0, 0);
INSERT INTO `cms_modules` VALUES ('CTLModuleMaker', 'installed', '1.7', 0, 1, 0, 0);
INSERT INTO `cms_modules` VALUES ('JQueryTools', 'installed', '1.3.4', 0, 1, 0, 0);
INSERT INTO `cms_modules` VALUES ('MleCMS', 'installed', '1.11.4', 0, 1, 0, 1);
INSERT INTO `cms_modules` VALUES ('TinyMCE', 'installed', '2.9.12', 0, 1, 0, 0);
INSERT INTO `cms_modules` VALUES ('ListIt2HeaderTexts', 'installed', '1.4.1', 0, 1, 0, 0);
INSERT INTO `cms_modules` VALUES ('ListIt2DoubleList', 'installed', '1.0.3', 0, 1, 1, 0);
INSERT INTO `cms_modules` VALUES ('ListIt2AboutUs', 'installed', '1.4.1', 0, 1, 0, 0);
INSERT INTO `cms_modules` VALUES ('ListIt2Testimonials', 'installed', '1.4.1', 0, 1, 0, 0);
INSERT INTO `cms_modules` VALUES ('ListIt2Social', 'installed', '1.4.1', 0, 1, 0, 0);
INSERT INTO `cms_modules` VALUES ('ListIt2Menu', 'installed', '1.4.1', 0, 1, 0, 0);
INSERT INTO `cms_modules` VALUES ('ListIt2Gallery', 'installed', '1.4.1', 0, 1, 0, 0);
INSERT INTO `cms_modules` VALUES ('ListIt2Staff', 'installed', '1.4.1', 0, 1, 0, 0);
INSERT INTO `cms_modules` VALUES ('ListIt2Slider', 'installed', '1.4.1', 0, 1, 0, 0);
INSERT INTO `cms_modules` VALUES ('ListIt2Blog', 'installed', '1.4.1', 0, 1, 0, 0);
INSERT INTO `cms_modules` VALUES ('SiteMapMadeSimple', 'installed', '1.2.8', 0, 1, 1, 1);
INSERT INTO `cms_modules` VALUES ('ErrorLogger', 'installed', '1.5.1', 0, 1, 0, 0);
INSERT INTO `cms_modules` VALUES ('vipmember', 'installed', '1.0', 0, 1, 0, 0);
INSERT INTO `cms_modules` VALUES ('CustomGS', 'installed', '2.0', 0, 1, 0, 0);

-- ----------------------------
-- Table structure for cms_permissions
-- ----------------------------
DROP TABLE IF EXISTS `cms_permissions`;
CREATE TABLE `cms_permissions`  (
  `permission_id` int(11) NOT NULL,
  `permission_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `permission_text` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `create_date` datetime NULL DEFAULT NULL,
  `modified_date` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`permission_id`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of cms_permissions
-- ----------------------------
INSERT INTO `cms_permissions` VALUES (1, 'Add Pages', 'Add Pages', '2006-07-25 21:22:33', '2006-07-25 21:22:33');
INSERT INTO `cms_permissions` VALUES (2, 'Add Groups', 'Add Groups', '2006-07-25 21:22:33', '2006-07-25 21:22:33');
INSERT INTO `cms_permissions` VALUES (4, 'Add Templates', 'Add Templates', '2006-07-25 21:22:33', '2006-07-25 21:22:33');
INSERT INTO `cms_permissions` VALUES (5, 'Add Users', 'Add Users', '2006-07-25 21:22:33', '2006-07-25 21:22:33');
INSERT INTO `cms_permissions` VALUES (6, 'Modify Any Page', 'Modify Any Page', '2006-07-25 21:22:33', '2006-07-25 21:22:33');
INSERT INTO `cms_permissions` VALUES (7, 'Modify Groups', 'Modify Groups', '2006-07-25 21:22:33', '2006-07-25 21:22:33');
INSERT INTO `cms_permissions` VALUES (8, 'Modify Group Assignments', 'Modify Group Assignments', '2006-07-25 21:22:33', '2006-07-25 21:22:33');
INSERT INTO `cms_permissions` VALUES (9, 'Modify Permissions', 'Modify Permissions for Groups', '2006-07-25 21:22:33', '2006-07-25 21:22:33');
INSERT INTO `cms_permissions` VALUES (11, 'Modify Templates', 'Modify Templates', '2006-07-25 21:22:33', '2006-07-25 21:22:33');
INSERT INTO `cms_permissions` VALUES (12, 'Modify Users', 'Modify Users', '2006-07-25 21:22:33', '2006-07-25 21:22:33');
INSERT INTO `cms_permissions` VALUES (13, 'Remove Pages', 'Remove Pages', '2006-07-25 21:22:33', '2006-07-25 21:22:33');
INSERT INTO `cms_permissions` VALUES (14, 'Remove Groups', 'Remove Groups', '2006-07-25 21:22:33', '2006-07-25 21:22:33');
INSERT INTO `cms_permissions` VALUES (16, 'Remove Templates', 'Remove Templates', '2006-07-25 21:22:33', '2006-07-25 21:22:33');
INSERT INTO `cms_permissions` VALUES (17, 'Remove Users', 'Remove Users', '2006-07-25 21:22:33', '2006-07-25 21:22:33');
INSERT INTO `cms_permissions` VALUES (18, 'Modify Modules', 'Modify Modules', '2006-07-25 21:22:33', '2006-07-25 21:22:33');
INSERT INTO `cms_permissions` VALUES (20, 'Modify Files', 'Modify Files', '2006-07-25 21:22:33', '2006-07-25 21:22:33');
INSERT INTO `cms_permissions` VALUES (21, 'Modify Site Preferences', 'Modify Site Preferences', '2006-07-25 21:22:33', '2006-07-25 21:22:33');
INSERT INTO `cms_permissions` VALUES (22, 'Modify Stylesheets', 'Modify Stylesheets', '2006-07-25 21:22:33', '2006-07-25 21:22:33');
INSERT INTO `cms_permissions` VALUES (23, 'Add Stylesheets', 'Add Stylesheets', '2006-07-25 21:22:33', '2006-07-25 21:22:33');
INSERT INTO `cms_permissions` VALUES (24, 'Remove Stylesheets', 'Remove Stylesheets', '2006-07-25 21:22:33', '2006-07-25 21:22:33');
INSERT INTO `cms_permissions` VALUES (25, 'Add Stylesheet Assoc', 'Add Stylesheet Associations', '2006-07-25 21:22:33', '2006-07-25 21:22:33');
INSERT INTO `cms_permissions` VALUES (26, 'Modify Stylesheet Assoc', 'Modify Stylesheet Associations', '2006-07-25 21:22:33', '2006-07-25 21:22:33');
INSERT INTO `cms_permissions` VALUES (27, 'Remove Stylesheet Assoc', 'Remove Stylesheet Associations', '2006-07-25 21:22:33', '2006-07-25 21:22:33');
INSERT INTO `cms_permissions` VALUES (28, 'Modify User-defined Tags', 'Modify User defined Tags', '2006-07-25 21:22:33', '2006-07-25 21:22:33');
INSERT INTO `cms_permissions` VALUES (29, 'Clear Admin Log', 'Clear Admin Log', '2006-07-25 21:22:33', '2006-07-25 21:22:33');
INSERT INTO `cms_permissions` VALUES (30, 'Add Global Content Blocks', 'Add Global Content Blocks', '2006-07-25 21:22:33', '2006-07-25 21:22:33');
INSERT INTO `cms_permissions` VALUES (31, 'Modify Global Content Blocks', 'Modify Global Content Blocks', '2006-07-25 21:22:33', '2006-07-25 21:22:33');
INSERT INTO `cms_permissions` VALUES (32, 'Remove Global Content Blocks', 'Remove Global Content Blocks', '2006-07-25 21:22:33', '2006-07-25 21:22:33');
INSERT INTO `cms_permissions` VALUES (35, 'Modify Events', 'Modify Events', '2006-01-27 20:06:58', '2006-01-27 20:06:58');
INSERT INTO `cms_permissions` VALUES (36, 'View Tag Help', 'View Tag Help', '2006-01-27 20:06:58', '2006-01-27 20:06:58');
INSERT INTO `cms_permissions` VALUES (44, 'Manage All Content', 'Manage All Content', '2009-05-06 15:04:11', '2009-05-06 15:04:11');
INSERT INTO `cms_permissions` VALUES (46, 'Reorder Content', 'Reorder Content', '2009-05-06 15:04:11', '2009-05-06 15:04:11');
INSERT INTO `cms_permissions` VALUES (47, 'Use FileManager Advanced', 'Advanced usage of the File Manager module', '2015-04-10 00:18:54', '2015-04-10 00:18:54');
INSERT INTO `cms_permissions` VALUES (48, 'Manage Menu', 'Manage Menu', '2015-04-10 00:18:54', '2015-04-10 00:18:54');
INSERT INTO `cms_permissions` VALUES (49, 'MicroTiny View HTML Source', 'MicroTiny View HTML Source', '2015-04-10 00:18:54', '2015-04-10 00:18:54');
INSERT INTO `cms_permissions` VALUES (50, 'Modify News', 'Modify News', '2015-04-10 00:18:55', '2015-04-10 00:18:55');
INSERT INTO `cms_permissions` VALUES (51, 'Approve News', 'Approve News For Frontend Display', '2015-04-10 00:18:55', '2015-04-10 00:18:55');
INSERT INTO `cms_permissions` VALUES (52, 'Delete News', 'Delete News Articles', '2015-04-10 00:18:55', '2015-04-10 00:18:55');
INSERT INTO `cms_permissions` VALUES (53, 'Manage Themes', 'Manage Themes', '2015-04-10 00:18:57', '2015-04-10 00:18:57');
INSERT INTO `cms_permissions` VALUES (54, 'Use CTL Module Maker', 'Use CTL Module Maker', '2015-04-11 16:24:27', '2015-04-11 16:24:27');
INSERT INTO `cms_permissions` VALUES (55, 'manage translator_mle', 'manage translator_mle', '2015-04-11 16:25:05', '2015-04-11 16:25:05');
INSERT INTO `cms_permissions` VALUES (56, 'manage mle_cms', 'manage mle_cms', '2015-04-11 16:25:05', '2015-04-11 16:25:05');
INSERT INTO `cms_permissions` VALUES (57, 'manage snippet_mle', 'manage snippet_mle', '2015-04-11 16:25:05', '2015-04-11 16:25:05');
INSERT INTO `cms_permissions` VALUES (58, 'manage block_mle', 'manage block_mle', '2015-04-11 16:25:05', '2015-04-11 16:25:05');
INSERT INTO `cms_permissions` VALUES (59, 'allowadvancedprofile', 'Allow usage of advanced profile in TinyMCE', '2015-04-11 16:25:31', '2015-04-11 16:25:31');
INSERT INTO `cms_permissions` VALUES (72, 'listit2headertexts_remove_item', 'listit2headertexts: Remove items', '2015-04-24 18:30:43', '2015-04-24 18:30:43');
INSERT INTO `cms_permissions` VALUES (71, 'listit2headertexts_modify_option', 'listit2headertexts: Modify Options', '2015-04-24 18:30:43', '2015-04-24 18:30:43');
INSERT INTO `cms_permissions` VALUES (70, 'listit2headertexts_modify_category', 'listit2headertexts: Modify Categories', '2015-04-24 18:30:43', '2015-04-24 18:30:43');
INSERT INTO `cms_permissions` VALUES (69, 'listit2headertexts_modify_item', 'listit2headertexts: Modify Items', '2015-04-24 18:30:43', '2015-04-24 18:30:43');
INSERT INTO `cms_permissions` VALUES (73, 'listit2headertexts_approve_item', 'listit2headertexts: Approve items', '2015-04-24 18:30:43', '2015-04-24 18:30:43');
INSERT INTO `cms_permissions` VALUES (74, 'listit2headertexts_modify_all_item', 'listit2headertexts: Modify all items', '2015-04-24 18:30:43', '2015-04-24 18:30:43');
INSERT INTO `cms_permissions` VALUES (75, 'listit2aboutus_modify_item', 'listit2aboutus: Modify Items', '2015-04-24 18:58:43', '2015-04-24 18:58:43');
INSERT INTO `cms_permissions` VALUES (76, 'listit2aboutus_modify_category', 'listit2aboutus: Modify Categories', '2015-04-24 18:58:43', '2015-04-24 18:58:43');
INSERT INTO `cms_permissions` VALUES (77, 'listit2aboutus_modify_option', 'listit2aboutus: Modify Options', '2015-04-24 18:58:43', '2015-04-24 18:58:43');
INSERT INTO `cms_permissions` VALUES (78, 'listit2aboutus_remove_item', 'listit2aboutus: Remove items', '2015-04-24 18:58:43', '2015-04-24 18:58:43');
INSERT INTO `cms_permissions` VALUES (79, 'listit2aboutus_approve_item', 'listit2aboutus: Approve items', '2015-04-24 18:58:43', '2015-04-24 18:58:43');
INSERT INTO `cms_permissions` VALUES (80, 'listit2aboutus_modify_all_item', 'listit2aboutus: Modify all items', '2015-04-24 18:58:43', '2015-04-24 18:58:43');
INSERT INTO `cms_permissions` VALUES (81, 'listit2testimonials_modify_item', 'listit2testimonials: Modify Items', '2015-04-24 19:35:11', '2015-04-24 19:35:11');
INSERT INTO `cms_permissions` VALUES (82, 'listit2testimonials_modify_category', 'listit2testimonials: Modify Categories', '2015-04-24 19:35:11', '2015-04-24 19:35:11');
INSERT INTO `cms_permissions` VALUES (83, 'listit2testimonials_modify_option', 'listit2testimonials: Modify Options', '2015-04-24 19:35:11', '2015-04-24 19:35:11');
INSERT INTO `cms_permissions` VALUES (84, 'listit2testimonials_remove_item', 'listit2testimonials: Remove items', '2015-04-24 19:35:11', '2015-04-24 19:35:11');
INSERT INTO `cms_permissions` VALUES (85, 'listit2testimonials_approve_item', 'listit2testimonials: Approve items', '2015-04-24 19:35:11', '2015-04-24 19:35:11');
INSERT INTO `cms_permissions` VALUES (86, 'listit2testimonials_modify_all_item', 'listit2testimonials: Modify all items', '2015-04-24 19:35:11', '2015-04-24 19:35:11');
INSERT INTO `cms_permissions` VALUES (87, 'listit2social_modify_item', 'listit2social: Modify Items', '2015-04-25 17:14:04', '2015-04-25 17:14:04');
INSERT INTO `cms_permissions` VALUES (88, 'listit2social_modify_category', 'listit2social: Modify Categories', '2015-04-25 17:14:04', '2015-04-25 17:14:04');
INSERT INTO `cms_permissions` VALUES (89, 'listit2social_modify_option', 'listit2social: Modify Options', '2015-04-25 17:14:04', '2015-04-25 17:14:04');
INSERT INTO `cms_permissions` VALUES (90, 'listit2social_remove_item', 'listit2social: Remove items', '2015-04-25 17:14:04', '2015-04-25 17:14:04');
INSERT INTO `cms_permissions` VALUES (91, 'listit2social_approve_item', 'listit2social: Approve items', '2015-04-25 17:14:04', '2015-04-25 17:14:04');
INSERT INTO `cms_permissions` VALUES (92, 'listit2social_modify_all_item', 'listit2social: Modify all items', '2015-04-25 17:14:04', '2015-04-25 17:14:04');
INSERT INTO `cms_permissions` VALUES (93, 'listit2menu_modify_item', 'listit2menu: Modify Items', '2015-04-25 17:21:17', '2015-04-25 17:21:17');
INSERT INTO `cms_permissions` VALUES (94, 'listit2menu_modify_category', 'listit2menu: Modify Categories', '2015-04-25 17:21:17', '2015-04-25 17:21:17');
INSERT INTO `cms_permissions` VALUES (95, 'listit2menu_modify_option', 'listit2menu: Modify Options', '2015-04-25 17:21:17', '2015-04-25 17:21:17');
INSERT INTO `cms_permissions` VALUES (96, 'listit2menu_remove_item', 'listit2menu: Remove items', '2015-04-25 17:21:17', '2015-04-25 17:21:17');
INSERT INTO `cms_permissions` VALUES (97, 'listit2menu_approve_item', 'listit2menu: Approve items', '2015-04-25 17:21:17', '2015-04-25 17:21:17');
INSERT INTO `cms_permissions` VALUES (98, 'listit2menu_modify_all_item', 'listit2menu: Modify all items', '2015-04-25 17:21:17', '2015-04-25 17:21:17');
INSERT INTO `cms_permissions` VALUES (99, 'listit2gallery_modify_item', 'listit2gallery: Modify Items', '2015-04-25 20:00:36', '2015-04-25 20:00:36');
INSERT INTO `cms_permissions` VALUES (100, 'listit2gallery_modify_category', 'listit2gallery: Modify Categories', '2015-04-25 20:00:36', '2015-04-25 20:00:36');
INSERT INTO `cms_permissions` VALUES (101, 'listit2gallery_modify_option', 'listit2gallery: Modify Options', '2015-04-25 20:00:36', '2015-04-25 20:00:36');
INSERT INTO `cms_permissions` VALUES (102, 'listit2gallery_remove_item', 'listit2gallery: Remove items', '2015-04-25 20:00:36', '2015-04-25 20:00:36');
INSERT INTO `cms_permissions` VALUES (103, 'listit2gallery_approve_item', 'listit2gallery: Approve items', '2015-04-25 20:00:36', '2015-04-25 20:00:36');
INSERT INTO `cms_permissions` VALUES (104, 'listit2gallery_modify_all_item', 'listit2gallery: Modify all items', '2015-04-25 20:00:36', '2015-04-25 20:00:36');
INSERT INTO `cms_permissions` VALUES (105, 'listit2staff_modify_item', 'listit2staff: Modify Items', '2015-04-27 07:52:51', '2015-04-27 07:52:51');
INSERT INTO `cms_permissions` VALUES (106, 'listit2staff_modify_category', 'listit2staff: Modify Categories', '2015-04-27 07:52:51', '2015-04-27 07:52:51');
INSERT INTO `cms_permissions` VALUES (107, 'listit2staff_modify_option', 'listit2staff: Modify Options', '2015-04-27 07:52:51', '2015-04-27 07:52:51');
INSERT INTO `cms_permissions` VALUES (108, 'listit2staff_remove_item', 'listit2staff: Remove items', '2015-04-27 07:52:51', '2015-04-27 07:52:51');
INSERT INTO `cms_permissions` VALUES (109, 'listit2staff_approve_item', 'listit2staff: Approve items', '2015-04-27 07:52:51', '2015-04-27 07:52:51');
INSERT INTO `cms_permissions` VALUES (110, 'listit2staff_modify_all_item', 'listit2staff: Modify all items', '2015-04-27 07:52:51', '2015-04-27 07:52:51');
INSERT INTO `cms_permissions` VALUES (111, 'listit2slider_modify_item', 'listit2slider: Modify Items', '2015-04-27 08:18:47', '2015-04-27 08:18:47');
INSERT INTO `cms_permissions` VALUES (112, 'listit2slider_modify_category', 'listit2slider: Modify Categories', '2015-04-27 08:18:47', '2015-04-27 08:18:47');
INSERT INTO `cms_permissions` VALUES (113, 'listit2slider_modify_option', 'listit2slider: Modify Options', '2015-04-27 08:18:47', '2015-04-27 08:18:47');
INSERT INTO `cms_permissions` VALUES (114, 'listit2slider_remove_item', 'listit2slider: Remove items', '2015-04-27 08:18:47', '2015-04-27 08:18:47');
INSERT INTO `cms_permissions` VALUES (115, 'listit2slider_approve_item', 'listit2slider: Approve items', '2015-04-27 08:18:47', '2015-04-27 08:18:47');
INSERT INTO `cms_permissions` VALUES (116, 'listit2slider_modify_all_item', 'listit2slider: Modify all items', '2015-04-27 08:18:47', '2015-04-27 08:18:47');
INSERT INTO `cms_permissions` VALUES (117, 'listit2blog_modify_item', 'listit2blog: Modify Items', '2015-04-27 09:04:17', '2015-04-27 09:04:17');
INSERT INTO `cms_permissions` VALUES (118, 'listit2blog_modify_category', 'listit2blog: Modify Categories', '2015-04-27 09:04:17', '2015-04-27 09:04:17');
INSERT INTO `cms_permissions` VALUES (119, 'listit2blog_modify_option', 'listit2blog: Modify Options', '2015-04-27 09:04:17', '2015-04-27 09:04:17');
INSERT INTO `cms_permissions` VALUES (120, 'listit2blog_remove_item', 'listit2blog: Remove items', '2015-04-27 09:04:17', '2015-04-27 09:04:17');
INSERT INTO `cms_permissions` VALUES (121, 'listit2blog_approve_item', 'listit2blog: Approve items', '2015-04-27 09:04:17', '2015-04-27 09:04:17');
INSERT INTO `cms_permissions` VALUES (122, 'listit2blog_modify_all_item', 'listit2blog: Modify all items', '2015-04-27 09:04:17', '2015-04-27 09:04:17');
INSERT INTO `cms_permissions` VALUES (123, 'ErrorLogger_View', 'ErrorLogger - View', '2015-09-11 23:29:50', '2015-09-11 23:29:50');
INSERT INTO `cms_permissions` VALUES (124, 'ErrorLogger_Edit', 'ErrorLogger - Edit', '2015-09-11 23:29:50', '2015-09-11 23:29:50');
INSERT INTO `cms_permissions` VALUES (138, 'vipmember_manage_items', 'vipmember: Manage items', '2015-09-17 12:24:58', '2015-09-17 12:24:58');
INSERT INTO `cms_permissions` VALUES (139, 'Custom Global Settings - Use', 'Custom Global Settings - Use', '2020-06-29 11:10:02', '2020-06-29 11:10:02');
INSERT INTO `cms_permissions` VALUES (140, 'Custom Global Settings - Manage', 'Custom Global Settings - Manage', '2020-06-29 11:10:02', '2020-06-29 11:10:02');

-- ----------------------------
-- Table structure for cms_permissions_seq
-- ----------------------------
DROP TABLE IF EXISTS `cms_permissions_seq`;
CREATE TABLE `cms_permissions_seq`  (
  `id` int(11) NOT NULL
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = FIXED;

-- ----------------------------
-- Records of cms_permissions_seq
-- ----------------------------
INSERT INTO `cms_permissions_seq` VALUES (140);

-- ----------------------------
-- Table structure for cms_routes
-- ----------------------------
DROP TABLE IF EXISTS `cms_routes`;
CREATE TABLE `cms_routes`  (
  `term` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `key1` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `key2` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `key3` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `data` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `created` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`term`, `key1`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of cms_routes
-- ----------------------------
INSERT INTO `cms_routes` VALUES ('about-us', '__CONTENT__', '61', NULL, 'O:8:\"CmsRoute\":2:{s:15:\"\0CmsRoute\0_data\";a:4:{s:4:\"term\";s:8:\"about-us\";s:8:\"absolute\";b:1;s:4:\"key1\";s:11:\"__CONTENT__\";s:4:\"key2\";s:2:\"61\";}s:18:\"\0CmsRoute\0_results\";N;}', '2019-07-02 11:47:17');
INSERT INTO `cms_routes` VALUES ('promotion', '__CONTENT__', '65', NULL, 'O:8:\"CmsRoute\":2:{s:15:\"\0CmsRoute\0_data\";a:4:{s:4:\"term\";s:9:\"promotion\";s:8:\"absolute\";b:1;s:4:\"key1\";s:11:\"__CONTENT__\";s:4:\"key2\";s:2:\"65\";}s:18:\"\0CmsRoute\0_results\";N;}', '2019-07-02 11:47:17');
INSERT INTO `cms_routes` VALUES ('reservations', '__CONTENT__', '66', NULL, 'O:8:\"CmsRoute\":2:{s:15:\"\0CmsRoute\0_data\";a:4:{s:4:\"term\";s:12:\"reservations\";s:8:\"absolute\";b:1;s:4:\"key1\";s:11:\"__CONTENT__\";s:4:\"key2\";s:2:\"66\";}s:18:\"\0CmsRoute\0_results\";N;}', '2019-07-02 11:47:17');
INSERT INTO `cms_routes` VALUES ('contact', '__CONTENT__', '67', NULL, 'O:8:\"CmsRoute\":2:{s:15:\"\0CmsRoute\0_data\";a:4:{s:4:\"term\";s:7:\"contact\";s:8:\"absolute\";b:1;s:4:\"key1\";s:11:\"__CONTENT__\";s:4:\"key2\";s:2:\"67\";}s:18:\"\0CmsRoute\0_results\";N;}', '2019-07-02 11:47:17');
INSERT INTO `cms_routes` VALUES ('vi', '__CONTENT__', '68', NULL, 'O:8:\"CmsRoute\":2:{s:15:\"\0CmsRoute\0_data\";a:4:{s:4:\"term\";s:2:\"vi\";s:8:\"absolute\";b:1;s:4:\"key1\";s:11:\"__CONTENT__\";s:4:\"key2\";s:2:\"68\";}s:18:\"\0CmsRoute\0_results\";N;}', '2019-07-02 11:47:17');

-- ----------------------------
-- Table structure for cms_siteprefs
-- ----------------------------
DROP TABLE IF EXISTS `cms_siteprefs`;
CREATE TABLE `cms_siteprefs`  (
  `sitepref_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `sitepref_value` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `create_date` datetime NULL DEFAULT NULL,
  `modified_date` datetime NULL DEFAULT NULL
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of cms_siteprefs
-- ----------------------------
INSERT INTO `cms_siteprefs` VALUES ('CGExtensions_mapi_pref_cache_autoclean_last', '1701330217', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('enablesitedownmessage', '0', '2006-07-25 21:22:33', '2006-07-25 21:22:33');
INSERT INTO `cms_siteprefs` VALUES ('sitedownmessage', '<p>Site is currently down for maintenance.</p>', '2006-07-25 21:22:33', '2006-07-25 21:22:33');
INSERT INTO `cms_siteprefs` VALUES ('sitedownmessagetemplate', '-1', '2006-07-25 21:22:33', '2006-07-25 21:22:33');
INSERT INTO `cms_siteprefs` VALUES ('useadvancedcss', '1', '2006-07-25 21:22:33', '2006-07-25 21:22:33');
INSERT INTO `cms_siteprefs` VALUES ('metadata', '{if isset($pagetitle) && !empty($pagetitle)}\r\n<title>{$pagetitle} - {sitename} | {global_content name=\'add\'}</title>\r\n{else}\r\n<title>{if $page_alias==home}{sitename}{else}{title} - {sitename}{/if} - {global_content name=\'add\'}</title>\r\n{/if}\r\n<meta charset=\"utf-8\">\r\n<meta name=\"viewport\" content=\"width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1\">\r\n<link rel=\"shortcut icon\" href=\"favicon.ico\">\r\n\r\n<!--Meta SEO-->\r\n{global_content name=\'copyright\' assign=\"coo\"}\r\n<meta name=\"Generator\" content=\"{$coo|strip_tags:false}\" />\r\n<meta http-equiv=\"Expires\" content=\"0\" />\r\n<meta name=\"Resource-type\" content=\"Document\" />\r\n<meta name=\"Language\" content=\"Vietnamese, English\" />\r\n<meta name=\"Keywords\" content=\"{if $keywords}{$keywords}{else}{global_content name=\'keywords\'}{/if}\" />\r\n<meta name=\"Description\" content=\"{if $descript}{$descript|strip_tags|truncate:\'160\':\"...\":false}{else if $description}{$description|strip_tags|truncate:\'160\':\"...\":false}{else}{global_content name=\'Description\'|strip_tags|truncate:\'160\':\"...\":false}{/if}\" />\r\n<meta name=\"Identifier-URL\" content=\"{root_url}\" />\r\n<meta name=\"Original-source\" content=\"{if isset($canonical)}{$canonical}{elseif isset($content_obj)}{$content_obj->GetURL()}{/if}\" />\r\n<meta name=\"Revised\" content=\"{modified_date format=\"%d/%m/%Y - %H:%M:%S\"}\" />\r\n<meta name=\"Robots\" content=\"Index, Follow\" />\r\n<meta name=\"Revisit-After\" content=\"1 Days\" />\r\n<meta name=\"Rating\" content=\"search engine optimization\" />\r\n<meta name=\"Copyright\" content=\"{sitename} | songviytuong@gmail.com\" />\r\n<meta name=\"Distribution\" content=\"Global\" />\r\n<link rel=\"author\" href=\"{global_content name=\'GooglePlus\'}\" />\r\n\r\n<!-- Social: Facebook -->\r\n<meta property=\"og:url\" content=\"{root_url}/{$smarty.get.page}\" />\r\n<meta property=\"og:image\" content=\"{if $metaimage}{$metaimage}{else}{root_url}/images/avata.jpg{/if}\" />\r\n<meta property=\"og:title\" content=\"{if $pagetitle}{$pagetitle}{else}{if $page_alias==home}{sitename}{else}{title} - {sitename}{/if} | The Official Website{/if}\" />\r\n<meta property=\"og:description\" content=\"{if $descript}{$descript|strip_tags|truncate:\'160\':\"...\":false}{else if $description}{$description|strip_tags|truncate:\'160\':\"...\":false}{else}{global_content name=\'Description\'|strip_tags|truncate:\'160\':\"...\":false}{/if}\" />\r\n\r\n<!-- Social: Twitter -->\r\n<meta name=\"twitter:card\" content=\"summary_large_image\">\r\n<meta name=\"twitter:site\" content=\"@songviytuong\">\r\n<meta name=\"twitter:creator\" content=\"Songviytuong\">\r\n<meta name=\"twitter:title\" content=\"{if $pagetitle}{$pagetitle}{else}{if $page_alias==home}{sitename}{else}{title} - {sitename}{/if} | The Official Website{/if}\">\r\n<meta name=\"twitter:description\" content=\"{if $descript}{$descript|strip_tags|truncate:\'160\':\"...\":false}{else if $description}{$description|strip_tags|truncate:\'160\':\"...\":false}{else}{global_content name=\'Description\'|strip_tags|truncate:\'160\':\"...\":false}{/if}\">\r\n<meta name=\"twitter:image:src\" content=\"{if $metaimage}{$metaimage}{else}{root_url}/images/avata.jpg{/if}\">\r\n<!--End Meta SEO-->\r\n\r\n	<link href=\"https://fonts.googleapis.com/css?family=Ubuntu:400,700%7CDroid+Serif:400,400italic,700,700italic%7CAguafina+Script\" rel=\"stylesheet\" type=\"text/css\">\r\n<link rel=\"stylesheet\" href=\"css/formalize.css\" media=\"all\">\r\n	<link rel=\"stylesheet\" href=\"css/unsemantic-grid.css\" media=\"all\">\r\n	<link rel=\"stylesheet\" href=\"css/font-awesome.min.css\" media=\"all\">\r\n	<link rel=\"stylesheet\" href=\"css/animate.mod.min.css\" media=\"all\">\r\n	<link rel=\"stylesheet\" href=\"css/tooltipster.css\" media=\"all\">\r\n	<link rel=\"stylesheet\" href=\"css/jquery-ui.min.css\" media=\"all\">\r\n	<link rel=\"stylesheet\" href=\"css/style.css\" media=\"all\">\r\n	<script type=\"text/javascript\" src=\"js/jquery-1.11.1.min.js\"></script>\r\n	<script type=\"text/javascript\" src=\"js/jquery-ui.min.js\"></script>', '2006-07-25 21:22:33', '2006-07-25 21:22:33');
INSERT INTO `cms_siteprefs` VALUES ('xmlmodulerepository', '', '2006-07-25 21:22:33', '2006-07-25 21:22:33');
INSERT INTO `cms_siteprefs` VALUES ('logintheme', 'OneEleven', '2006-07-25 21:22:33', '2006-07-25 21:22:33');
INSERT INTO `cms_siteprefs` VALUES ('global_umask', '022', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('frontendlang', 'en_US', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('frontendwysiwyg', 'TinyMCE', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('nogcbwysiwyg', '1', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('urlcheckversion', '', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('defaultdateformat', '', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('css_max_age', '0', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('backendwysiwyg', 'TinyMCE', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('disablesafemodewarning', '1', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('allowparamcheckwarnings', '0', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('enablenotifications', '1', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('page_active', '1', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('page_showinmenu', '1', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('page_cachable', '1', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('page_metadata', '{* Add code here that should appear in the metadata section of all new pages *}', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('defaultpagecontent', '<!-- Add code here that should appear in the content block of all new pages -->', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('default_parent_page', '-1', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('additional_editors', '', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('page_searchable', '1', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('page_extra1', '', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('page_extra2', '', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('page_extra3', '', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('sitedownexcludes', '', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('clear_vc_cache', '0', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('sitename', 'Khoa Ngan Xưa &amp; Nay', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('sitemask', 'mFftPpaDMZ7hAYS9', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('CMSMailer_mapi_pref_mailer', 'smtp', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('CMSMailer_mapi_pref_host', 'localhost', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('CMSMailer_mapi_pref_port', '25', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('CMSMailer_mapi_pref_from', 'root@localhost', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('CMSMailer_mapi_pref_fromuser', 'CMS Administrator', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('CMSMailer_mapi_pref_sendmail', '/usr/sbin/sendmail', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('CMSMailer_mapi_pref_timeout', '1000', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('CMSMailer_mapi_pref_smtpauth', '0', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('CMSMailer_mapi_pref_username', '', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('CMSMailer_mapi_pref_password', '', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('CMSMailer_mapi_pref_secure', '', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('CMSPrinting_mapi_pref_overridestyle', '/*\nYou can put css stuff here, which will be inserted in the header after calling the cmsms-stylesheets.\nProvided you don\'t remove the {$overridestylesheet} in PrintTemplate, of course.\n\nAny suggestions for default content in this stylesheet?\n\nHave fun!\n*/', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('FileManager_mapi_pref_iconsize', '32px', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('FileManager_mapi_pref_uploadboxes', '5', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('FileManager_mapi_pref_showhiddenfiles', '0', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('ModuleManager_mapi_pref_module_repository', 'http://www.cmsmadesimple.org/ModuleRepository/request/v2/', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('News_mapi_pref_default_summary_template_contents', '<!-- Start News Display Template -->\n{* This section shows a clickable list of your News categories. *}\n<ul class=\"list1\">\n{foreach from=$cats item=node}\n{if $node.depth > $node.prevdepth}\n{repeat string=\"<ul>\" times=$node.depth-$node.prevdepth}\n{elseif $node.depth < $node.prevdepth}\n{repeat string=\"</li></ul>\" times=$node.prevdepth-$node.depth}\n</li>\n{elseif $node.index > 0}</li>\n{/if}\n<li{if $node.index == 0} class=\"firstnewscat\"{/if}>\n{if $node.count > 0}\n	<a href=\"{$node.url}\">{$node.news_category_name}</a>{else}<span>{$node.news_category_name} </span>{/if}\n{/foreach}\n{repeat string=\"</li></ul>\" times=$node.depth-1}</li>\n</ul>\n\n{* this displays the category name if you\'re browsing by category *}\n{if $category_name}\n<h1>{$category_name}</h1>\n{/if}\n\n{* if you don\'t want category browsing on your summary page, remove this line and everything above it *}\n\n{if $pagecount > 1}\n  <p>\n{if $pagenumber > 1}\n{$firstpage}&nbsp;{$prevpage}&nbsp;\n{/if}\n{$pagetext}&nbsp;{$pagenumber}&nbsp;{$oftext}&nbsp;{$pagecount}\n{if $pagenumber < $pagecount}\n&nbsp;{$nextpage}&nbsp;{$lastpage}\n{/if}\n</p>\n{/if}\n{foreach from=$items item=entry}\n<div class=\"NewsSummary\">\n\n{if $entry->postdate}\n	<div class=\"NewsSummaryPostdate\">\n		{$entry->postdate|cms_date_format}\n	</div>\n{/if}\n\n<div class=\"NewsSummaryLink\">\n<a href=\"{$entry->moreurl}\" title=\"{$entry->title|cms_escape:htmlall}\">{$entry->title|cms_escape}</a>\n</div>\n\n<div class=\"NewsSummaryCategory\">\n	{$category_label} {$entry->category}\n</div>\n\n{if $entry->author}\n	<div class=\"NewsSummaryAuthor\">\n		{$author_label} {$entry->author}\n	</div>\n{/if}\n\n{if $entry->summary}\n	<div class=\"NewsSummarySummary\">\n		{eval var=$entry->summary}\n	</div>\n\n	<div class=\"NewsSummaryMorelink\">\n		[{$entry->morelink}]\n	</div>\n\n{else if $entry->content}\n\n	<div class=\"NewsSummaryContent\">\n		{eval var=$entry->content}\n	</div>\n{/if}\n\n{if isset($entry->extra)}\n    <div class=\"NewsSummaryExtra\">\n        {eval var=$entry->extra}\n	{* {cms_module module=\'Uploads\' mode=\'simpleurl\' upload_id=$entry->extravalue} *}\n    </div>\n{/if}\n{if isset($entry->fields)}\n  {foreach from=$entry->fields item=\'field\'}\n     <div class=\"NewsSummaryField\">\n        {if $field->type == \'file\'}\n          <img src=\"{$entry->file_location}/{$field->displayvalue}\"/>\n        {else}\n          {$field->name}:&nbsp;{eval var=$field->displayvalue}\n        {/if}\n     </div>\n  {/foreach}\n{/if}\n\n</div>\n{/foreach}\n<!-- End News Display Template -->\n', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('News_mapi_pref_current_summary_template', 'Sample', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('News_mapi_pref_default_detail_template_contents', '{* News module entry object reference:\n   ------------------------------\n   In previous versions of News the \'object\' returned in $entry was quite simple, and a <pre>{$entry|@print_r}</pre> would output all of the available data\n   This has changed in News 2.12, the object is not quite as \'simple\' as it was in previous versions, and that method will no longer work.  Hence, below\n   you will find a referennce to the available data.\n\n   ====\n   news_article Object Reference\n   ====\n\n     Members:\n     --\n     Members can be displayed by the following syntax: {$entry->membername} or assigned to another smarty variable using {assign var=\'foo\' value=$entry->membername}.\n\n     The following members are available in the entry array:\n   \n     id (integer)           = The unique article id.\n     author_id (integer)    = The userid of the author who created the article.  This value may be negative to indicate an FEU userid.\n     title (string)         = The title of the article.\n     summary (text)         = The summary text (may be empty or unset).\n     extra (string)         = The \"extra\" data associated with the article (may be empty or unset).\n     news_url (string)      = The url segment associated with this article (may be empty or unset).\n     postdate (string)      = A string representing the news article post date.  You may filter this through cms_date_format for different display possibilities.\n     startdate (string)     = A string representing the date the article should begin to appear.  (may be empty or unset)\n     enddate (string)       = A string representing the date the article should stop appearing on the site (may be empty or unset).\n     category_id (integer)  = The unique id of the hierarchy level where this article resides (may be empty or unset)\n     status (string)        = either \'draft\' or \'published\' indicating the status of this article.\n     author (string)        = The username of the original author of the article.  If the article was created by frontend submission, this will attempt to retrieve the username from the FEU module.\n     authorname (string)    = The full name of the original author of the website. Only applicable if article was created by an administrator and that information exists in the administrators profile.\n     category (string)      = The name of the category that this article is associated with.\n     canonical (string)     = A full URL (prettified) to this articles detail view using defaults if necessary.\n     fields (associative)   = An associative array of field objects, representing the fields, and their values for this article.  See the information below on the field object definition.   In past versions of News this was a simple array, now it is an associative one.\n     customfieldsbyname     = (deprecated) - A synonym for the \'fields\' member\n     fieldsbyname           = (deprecated) - A synonym for the \'fields\' member\n     useexp (integer)       = A flag indicating wether this article is using the expiry information.\n     file_location (string) = A url containing the location where files attached the article are stored... the field value should be appended to this url.\n     \n\n   ====\n   news_field Object Reference\n   ====\n   The news_field object contains data about the fields and their values that are associated with a particular news article.\n\n     Members:\n     --------\n     id (integer)  = The id of the field definition\n     name (string) = The name of the field\n     type (string) = The type of field\n     max_length (integer) = The maximum length of the field (applicable only to text fields)\n     item_order (integer) = The order of the field\n     public (integer) = A flag indicating wether the field is public or not\n     value (mixed)    = The value of the field.\n     displayvalue (mixed) = A displayable value for the field.  This is particularly useful in the case of dropdown fields.\n\n\n   ====\n   Below, you will find the normal detail template information.  Modify this template as desired.\n*}\n\n{* set a canonical variable that can be used in the head section if process_whole_template is false in the config.php *}\n{if isset($entry->canonical)}\n  {assign var=\'canonical\' value=$entry->canonical}\n{/if}\n\n{if $entry->postdate}\n	<div id=\"NewsPostDetailDate\">\n		{$entry->postdate|cms_date_format}\n	</div>\n{/if}\n<h3 id=\"NewsPostDetailTitle\">{$entry->title|cms_escape:htmlall}</h3>\n\n<hr id=\"NewsPostDetailHorizRule\" />\n\n{if $entry->summary}\n	<div id=\"NewsPostDetailSummary\">\n		<strong>\n			{eval var=$entry->summary}\n		</strong>\n	</div>\n{/if}\n\n{if $entry->category}\n	<div id=\"NewsPostDetailCategory\">\n		{$category_label} {$entry->category}\n	</div>\n{/if}\n{if $entry->author}\n	<div id=\"NewsPostDetailAuthor\">\n		{$author_label} {$entry->author}\n	</div>\n{/if}\n\n<div id=\"NewsPostDetailContent\">\n	{eval var=$entry->content}\n</div>\n\n{if $entry->extra}\n	<div id=\"NewsPostDetailExtra\">\n		{$extra_label} {$entry->extra}\n	</div>\n{/if}\n\n{if $return_url != \"\"}\n<div id=\"NewsPostDetailReturnLink\">{$return_url}{if $category_name != \'\'} - {$category_link}{/if}</div>\n{/if}\n\n{if isset($entry->fields)}\n  {foreach from=$entry->fields item=\'field\'}\n     <div class=\"NewsDetailField\">\n        {if $field->type == \'file\'}\n	  {* this template assumes that every file uploaded is an image of some sort, because News doesn\'t distinguish *}\n          <img src=\"{$entry->file_location}/{$field->displayvalue}\"/>\n        {else}\n          {$field->name}:&nbsp;{eval var=$field->displayvalue}\n        {/if}\n     </div>\n  {/foreach}\n{/if}\n', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('News_mapi_pref_current_detail_template', 'Sample', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('News_mapi_pref_default_form_template_contents', '{* original form template *}\n{if isset($error)}\n  <h3><font color=\"red\">{$error}</font></h3>\n{else}\n  {if isset($message)}\n    <h3>{$message}</h3>\n  {/if}\n{/if}\n{$startform}\n	<div class=\"pageoverflow\">\n		<p class=\"pagetext\">*{$titletext}:</p>\n		<p class=\"pageinput\">{$inputtitle}</p>\n	</div>\n	<div class=\"pageoverflow\">\n		<p class=\"pagetext\">{$categorytext}:</p>\n		<p class=\"pageinput\">{$inputcategory}</p>\n	</div>\n{if !isset($hide_summary_field) or $hide_summary_field == 0}\n	<div class=\"pageoverflow\">\n		<p class=\"pagetext\">{$summarytext}:</p>\n		<p class=\"pageinput\">{$inputsummary}</p>\n	</div>\n{/if}\n	<div class=\"pageoverflow\">\n		<p class=\"pagetext\">*{$contenttext}:</p>\n		<p class=\"pageinput\">{$inputcontent}</p>\n	</div>\n	<div class=\"pageoverflow\">\n		<p class=\"pagetext\">{$extratext}:</p>\n		<p class=\"pageinput\">{$inputextra}</p>\n	</div>\n	<div class=\"pageoverflow\">\n		<p class=\"pagetext\">{$startdatetext}:</p>\n		<p class=\"pageinput\">{html_select_date prefix=$startdateprefix time=$startdate end_year=\"+15\"} {html_select_time prefix=$startdateprefix time=$startdate}</p>\n	</div>\n	<div class=\"pageoverflow\">\n		<p class=\"pagetext\">{$enddatetext}:</p>\n		<p class=\"pageinput\">{html_select_date prefix=$enddateprefix time=$enddate end_year=\"+15\"} {html_select_time prefix=$enddateprefix time=$enddate}</p>\n	</div>\n	{if isset($customfields)}\n	   {foreach from=$customfields item=\'onefield\'}\n	      <div class=\"pageoverflow\">\n		<p class=\"pagetext\">{$onefield->name}:</p>\n		<p class=\"pageinput\">{$onefield->field}</p>\n	      </div>\n	   {/foreach}\n	{/if}\n	<div class=\"pageoverflow\">\n		<p class=\"pagetext\">&nbsp;</p>\n		<p class=\"pageinput\">{$hidden}{$submit}{$cancel}</p>\n	</div>\n{$endform}\n', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('News_mapi_pref_current_form_template', 'Sample', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('News_mapi_pref_default_browsecat_template_contents', '{if $count > 0}\n<ul class=\"list1\">\n{foreach from=$cats item=node}\n{if $node.depth > $node.prevdepth}\n{repeat string=\"<ul>\" times=$node.depth-$node.prevdepth}\n{elseif $node.depth < $node.prevdepth}\n{repeat string=\"</li></ul>\" times=$node.prevdepth-$node.depth}\n</li>\n{elseif $node.index > 0}</li>\n{/if}\n<li class=\"newscategory\">\n{if $node.count > 0}\n	<a href=\"{$node.url}\">{$node.news_category_name}</a> ({$node.count}){else}<span>{$node.news_category_name} (0)</span>{/if}\n{/foreach}\n{repeat string=\"</li></ul>\" times=$node.depth-1}</li>\n</ul>\n{/if}', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('News_mapi_pref_current_browsecat_template', 'Sample', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('News_mapi_pref_email_subject', 'A new News article has been posted', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('News_mapi_pref_allowed_upload_types', 'gif,png,jpeg,jpg', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('News_mapi_pref_auto_create_thumbnails', 'gif,png,jpeg,jpg', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('Search_mapi_pref_stopwords', 'i, me, my, myself, we, our, ours, ourselves, you, your, yours, \nyourself, yourselves, he, him, his, himself, she, her, hers, \nherself, it, its, itself, they, them, their, theirs, themselves, \nwhat, which, who, whom, this, that, these, those, am, is, are, \nwas, were, be, been, being, have, has, had, having, do, does, \ndid, doing, a, an, the, and, but, if, or, because, as, until, \nwhile, of, at, by, for, with, about, against, between, into, \nthrough, during, before, after, above, below, to, from, up, down, \nin, out, on, off, over, under, again, further, then, once, here, \nthere, when, where, why, how, all, any, both, each, few, more, \nmost, other, some, such, no, nor, not, only, own, same, so, \nthan, too, very', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('Search_mapi_pref_usestemming', 'false', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('Search_mapi_pref_searchtext', 'Enter Search...', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('__NOTIFICATIONS__', 'a:1:{i:0;O:8:\"stdClass\":5:{s:8:\"priority\";i:1;s:4:\"link\";s:114:\"https://khoanganxuavanay.com/admin/moduleinterface.php?mact=CMSMailer,m1_,defaultadmin,0&amp;_sx_=1a5ed1e3d3085038\";s:4:\"html\";s:463:\"Your mail settings have not been configured.  This could interfere with the ability of your website to send email messages.  You should go to <a href=\"https://khoanganxuavanay.com/admin/moduleinterface.php?mact=CMSMailer,m1_,defaultadmin,0&amp;_sx_=1a5ed1e3d3085038\">Extensions >> CMSMailer</a> and configure the mail settings with the information provided by your host.&nbsp;This test is generated on an infrequent basis.  It may take some time for it to go away\";s:4:\"name\";s:9:\"CMSMailer\";s:12:\"friendlyname\";s:9:\"CMSMailer\";}}', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('PruneAdminlog_lastexecute', '1701330217', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('pseudocron_lastrun', '1701330217', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('cms_is_uptodate', '1', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('lastcmsversioncheck', '1428703195', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('MicroTiny_mapi_pref_show_statusbar', '1', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('MicroTiny_mapi_pref_allow_resize', '1', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('MicroTiny_mapi_pref_strip_background', '1', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('MicroTiny_mapi_pref_force_blackonwhite', '0', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('MicroTiny_mapi_pref_allowimages', '1', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('MicroTiny_mapi_pref_css_styles', '', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('checkversion', '1', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('allow_browser_cache', '1', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('browser_cache_expiry', '60', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('auto_clear_cache_age', '7', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('pseudocron_granularity', '60', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('adminlog_lifetime', '2678400', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('thumbnail_width', '96', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('thumbnail_height', '96', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('searchmodule', 'Search', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('ListIt2_mapi_pref_allow_autoscan', '0', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('ListIt2_mapi_pref_allow_autoinstall', '1', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('CGExtensions_mapi_pref_cache_autoclean_last', '1701330217', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('CGExtensions_mapi_pref_imageextensions', 'jpg,png,gif', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('CGExtensions_mapi_pref_thumbnailsize', '75', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('CGExtensions_mapi_pref_watermark_text', 'Vietnam Muslim Tour', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('CGExtensions_mapi_pref_watermark_textsize', '12', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('CGExtensions_mapi_pref_watermark_angle', '0', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('CGExtensions_mapi_pref_watermark_font', 'ARIAL.TTF', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('CGExtensions_mapi_pref_watermark_bgcolor', '#FFFFFF', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('CGExtensions_mapi_pref_watermark_textcolor', '#000000', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('CGExtensions_mapi_pref_watermark_transparent', '1', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('CGExtensions_mapi_pref_dflt_sortablelist_template_content', '{* sortable list template *}\n\n{*\n This template provides one example of using javascript in a CMS module template.  The javascript is left here as an example of how one can interact with smarty in javascript.  You may infact want to put most of these functions into a seperate .js file and include it somewhere in your head section.\n\n You are free to modify this javascript and this template.  However, the php driver scripts look for a field named in the smarty variable {$selectarea_prefix}, and expect that to be a comma seperated list of values.\n *}\n\n\n<script type=\'text/javascript\'>\nvar allowduplicates = {$allowduplicates};\nvar selectlist = \"{$selectarea_prefix}_selectlist\";\nvar masterlist = \"{$selectarea_prefix}_masterlist\";\nvar addbtn = \"{$selectarea_prefix}_add\";\nvar rembtn = \"{$selectarea_prefix}_remove\";\nvar upbtn = \"{$selectarea_prefix}_up\";\nvar downbtn = \"{$selectarea_prefix}_down\";\nvar valuefld = \"{$selectarea_prefix}\";\nvar max_selected = {$max_selected};\n\nfunction selectarea_update_value()\n{\n  var sel_elem = document.getElementById(selectlist);\n  var val_elem = document.getElementById(valuefld);\n  var sel_idx = sel_elem.selectedIndex;\n  var opts = sel_elem.getElementsByTagName(\'option\');\n  var tmp = new Array();\n  for( i = 0; i < opts.length; i++ )\n    {\n      tmp[tmp.length] = opts[i].value;\n    }\n  var str = tmp.join(\',\');\n  val_elem.value = str;\n}\n\nfunction selectarea_handle_down()\n{\n  var sel_elem = document.getElementById(selectlist);\n  var sel_idx = sel_elem.selectedIndex;\n  var opts = sel_elem.getElementsByTagName(\'option\');\n  for( var i = opts.length - 2; i >= 0; i-- )\n    {\n      var opt = opts[i];\n      if( opt.selected )\n        {\n           var nextopt = opts[i+1];\n           opt = sel_elem.removeChild(opt);\n           nextopt = sel_elem.replaceChild(opt,nextopt);\n           sel_elem.insertBefore(nextopt,opt);\n        }\n    }\n  selectarea_update_value();\n}\n\nfunction selectarea_handle_up()\n{\n  var sel_elem = document.getElementById(selectlist);\n  var sel_idx = sel_elem.selectedIndex;\n  var opts = sel_elem.getElementsByTagName(\'option\');\n  if( sel_idx > 0 )\n    {\n      for( var i = 1; i < opts.length; i++ )\n        {\n          var opt = opts[i];\n          if( opt.selected )\n            {\n              sel_elem.removeChild(opt);\n               sel_elem.insertBefore(opt, opts[i-1]);\n            }\n        }\n    }\n  selectarea_update_value();\n}\n\nfunction selectarea_handle_remove()\n{\n  var sel_elem = document.getElementById(selectlist);\n  var sel_idx = sel_elem.selectedIndex;\n  if( sel_idx >= 0 ) {\n    var val = sel_elem.options[sel_idx].value;\n    sel_elem.remove(sel_idx);\n  }\n  selectarea_update_value();\n}\n\nfunction selectarea_handle_add()\n{\n  var mas_elem = document.getElementById(masterlist);\n  var mas_idx = mas_elem.selectedIndex;\n  var sel_elem = document.getElementById(selectlist);\n  var opts = sel_elem.getElementsByTagName(\'option\');\n  if( opts.length >= max_selected && max_selected > 0) return;\n  if( mas_idx >= 0 ) {\n      var newOpt = document.createElement(\'option\');\n      newOpt.text = mas_elem.options[mas_idx].text;\n      newOpt.value = mas_elem.options[mas_idx].value;\n      if( allowduplicates == 0 ) {\n        for( var i = 0; i < opts.length; i++ ) {\n          if( opts[i].value == newOpt.value ) return;\n        }\n      }\n      sel_elem.add(newOpt,null);\n  }\n  selectarea_update_value();\n}\n\n\nfunction selectarea_handle_select()\n{\n  var sel_elem = document.getElementById(selectlist);\n  var sel_idx = sel_elem.selectedIndex;\n  var mas_elem = document.getElementById(masterlist);\n  var mas_idx = mas_elem.selectedIndex;\n  addbtn.disabled = (mas_idx >= 0);\n  rembtn.disabled = (sel_idx >= 0);\n  addbtn.disabled = (sel_idx >= 0);\n  downbtn.disabled = (sel_idx >= 0);\n}\n\n</script>\n\n<table>\n  <tr>\n    <td>\n      {* left column - for the selected items *}\n      {$label_left}<br/>\n      <select id=\"{$selectarea_prefix}_selectlist\" size=\"10\" onchange=\"selectarea_handle_select();\">\n        {html_options options=$selectarea_selected}\n      </select><br/>\n    </td>\n    <td>\n      {* center column - for the add/delete buttons *}\n      <input type=\"submit\" id=\"{$selectarea_prefix}_add\" value=\"&lt;&lt;\" onclick=\"selectarea_handle_add(); return false;\"/><br/>\n      <input type=\"submit\" id=\"{$selectarea_prefix}_remove\" value=\"&gt;&gt;\" onclick=\"selectarea_handle_remove(); return false;\"/><br/>\n      <input type=\"submit\" id=\"{$selectarea_prefix}_up\" value=\"{$upstr}\" onclick=\"selectarea_handle_up(); return false;\"/><br/>\n      <input type=\"submit\" id=\"{$selectarea_prefix}_down\" value=\"{$downstr}\" onclick=\"selectarea_handle_down(); return false;\"/><br/>\n    </td>\n    <td>\n      {* right column - for the master list *}\n      {$label_right}<br/>\n      <select id=\"{$selectarea_prefix}_masterlist\" size=\"10\" onchange=\"selectarea_handle_select();\">\n        {html_options options=$selectarea_masterlist}\n      </select>\n    </td>\n  </tr>\n</table>\n<div><input type=\"hidden\" id=\"{$selectarea_prefix}\" name=\"{$selectarea_prefix}\" value=\"{$selectarea_selected_str}\" /></div>\n', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('CGExtensions_mapi_pref_dflt_sortablelist_template', 'Sample', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('cf669c2c49b7eeaf80506380723ea25f6', 'a:15:{s:8:\"cg_cmsms\";O:29:\"CGExtensions\\jsloader\\libdefn\":1:{s:36:\"\0CGExtensions\\jsloader\\libdefn\0_data\";a:2:{s:4:\"name\";s:8:\"cg_cmsms\";s:8:\"callback\";a:2:{i:0;s:13:\"cge_jshandler\";i:1;s:4:\"load\";}}}s:6:\"cgform\";O:29:\"CGExtensions\\jsloader\\libdefn\":1:{s:36:\"\0CGExtensions\\jsloader\\libdefn\0_data\";a:4:{s:4:\"name\";s:6:\"cgform\";s:6:\"module\";s:11:\"JQueryTools\";s:6:\"jsfile\";a:1:{i:0;s:102:\"F:\\OneDrive\\Website\\htdocs\\@\\vietnammuslimtourkaka\\modules\\JQueryTools/lib/cgform.lib/jquery.cgform.js\";}s:7:\"depends\";a:1:{i:0;s:6:\"jquery\";}}}s:7:\"cluetip\";O:29:\"CGExtensions\\jsloader\\libdefn\":1:{s:36:\"\0CGExtensions\\jsloader\\libdefn\0_data\";a:5:{s:4:\"name\";s:7:\"cluetip\";s:6:\"module\";s:11:\"JQueryTools\";s:6:\"jsfile\";a:3:{i:0;s:112:\"F:\\OneDrive\\Website\\htdocs\\@\\vietnammuslimtourkaka\\modules\\JQueryTools/lib/cluetip.lib/lib/jquery.hoverIntent.js\";i:1;s:113:\"F:\\OneDrive\\Website\\htdocs\\@\\vietnammuslimtourkaka\\modules\\JQueryTools/lib/cluetip.lib/lib/jquery.bgiframe.min.js\";i:2;s:108:\"F:\\OneDrive\\Website\\htdocs\\@\\vietnammuslimtourkaka\\modules\\JQueryTools/lib/cluetip.lib/jquery.cluetip.min.js\";}s:7:\"cssfile\";a:1:{i:0;s:105:\"F:\\OneDrive\\Website\\htdocs\\@\\vietnammuslimtourkaka\\modules\\JQueryTools/lib/cluetip.lib/jquery.cluetip.css\";}s:7:\"depends\";a:1:{i:0;s:6:\"jquery\";}}}s:8:\"combobox\";O:29:\"CGExtensions\\jsloader\\libdefn\":1:{s:36:\"\0CGExtensions\\jsloader\\libdefn\0_data\";a:4:{s:4:\"name\";s:8:\"combobox\";s:6:\"module\";s:11:\"JQueryTools\";s:6:\"jsfile\";a:1:{i:0;s:113:\"F:\\OneDrive\\Website\\htdocs\\@\\vietnammuslimtourkaka\\modules\\JQueryTools/lib/combobox.lib/jquery.custom-combobox.js\";}s:7:\"depends\";a:1:{i:0;s:2:\"ui\";}}}s:8:\"fancybox\";O:29:\"CGExtensions\\jsloader\\libdefn\":1:{s:36:\"\0CGExtensions\\jsloader\\libdefn\0_data\";a:5:{s:4:\"name\";s:8:\"fancybox\";s:6:\"module\";s:11:\"JQueryTools\";s:6:\"jsfile\";a:2:{i:0;s:111:\"F:\\OneDrive\\Website\\htdocs\\@\\vietnammuslimtourkaka\\modules\\JQueryTools/lib/fancybox.lib/jquery.fancybox.pack.js\";i:1;s:119:\"F:\\OneDrive\\Website\\htdocs\\@\\vietnammuslimtourkaka\\modules\\JQueryTools/lib/fancybox.lib/jquery.mousewheel-3.0.6.pack.js\";}s:7:\"cssfile\";a:1:{i:0;s:107:\"F:\\OneDrive\\Website\\htdocs\\@\\vietnammuslimtourkaka\\modules\\JQueryTools/lib/fancybox.lib/jquery.fancybox.css\";}s:7:\"depends\";a:1:{i:0;s:6:\"jquery\";}}}s:10:\"fileupload\";O:29:\"CGExtensions\\jsloader\\libdefn\":1:{s:36:\"\0CGExtensions\\jsloader\\libdefn\0_data\";a:4:{s:4:\"name\";s:10:\"fileupload\";s:6:\"module\";s:11:\"JQueryTools\";s:6:\"jsfile\";a:2:{i:0;s:119:\"F:\\OneDrive\\Website\\htdocs\\@\\vietnammuslimtourkaka\\modules\\JQueryTools/lib/fileupload.lib/js/jquery.iframe-transport.js\";i:1;s:113:\"F:\\OneDrive\\Website\\htdocs\\@\\vietnammuslimtourkaka\\modules\\JQueryTools/lib/fileupload.lib/js/jquery.fileupload.js\";}s:7:\"depends\";a:2:{i:0;s:6:\"jquery\";i:1;s:2:\"ui\";}}}s:4:\"form\";O:29:\"CGExtensions\\jsloader\\libdefn\":1:{s:36:\"\0CGExtensions\\jsloader\\libdefn\0_data\";a:4:{s:4:\"name\";s:4:\"form\";s:6:\"module\";s:11:\"JQueryTools\";s:6:\"jsfile\";a:1:{i:0;s:98:\"F:\\OneDrive\\Website\\htdocs\\@\\vietnammuslimtourkaka\\modules\\JQueryTools/lib/form.lib/jquery.form.js\";}s:7:\"depends\";a:1:{i:0;s:6:\"jquery\";}}}s:6:\"jquery\";O:29:\"CGExtensions\\jsloader\\libdefn\":1:{s:36:\"\0CGExtensions\\jsloader\\libdefn\0_data\";a:3:{s:4:\"name\";s:6:\"jquery\";s:6:\"module\";s:11:\"JQueryTools\";s:6:\"jsfile\";a:2:{i:0;s:106:\"F:\\OneDrive\\Website\\htdocs\\@\\vietnammuslimtourkaka\\modules\\JQueryTools/lib/jquery.lib/jquery-1.11.1.min.js\";i:1;s:113:\"F:\\OneDrive\\Website\\htdocs\\@\\vietnammuslimtourkaka\\modules\\JQueryTools/lib/jquery.lib/jquery-migrate-1.2.1.min.js\";}}}s:11:\"jquerytools\";O:29:\"CGExtensions\\jsloader\\libdefn\":1:{s:36:\"\0CGExtensions\\jsloader\\libdefn\0_data\";a:4:{s:4:\"name\";s:11:\"jquerytools\";s:6:\"module\";s:11:\"JQueryTools\";s:7:\"cssfile\";a:1:{i:0;s:106:\"F:\\OneDrive\\Website\\htdocs\\@\\vietnammuslimtourkaka\\modules\\JQueryTools/lib/jquerytools.lib/JQueryTools.css\";}s:7:\"depends\";a:1:{i:0;s:11:\"tablesorter\";}}}s:4:\"json\";O:29:\"CGExtensions\\jsloader\\libdefn\":1:{s:36:\"\0CGExtensions\\jsloader\\libdefn\0_data\";a:4:{s:4:\"name\";s:4:\"json\";s:6:\"module\";s:11:\"JQueryTools\";s:6:\"jsfile\";a:1:{i:0;s:106:\"F:\\OneDrive\\Website\\htdocs\\@\\vietnammuslimtourkaka\\modules\\JQueryTools/lib/json.lib/jquery.json-2.4.min.js\";}s:7:\"depends\";a:1:{i:0;s:6:\"jquery\";}}}s:7:\"jsviews\";O:29:\"CGExtensions\\jsloader\\libdefn\":1:{s:36:\"\0CGExtensions\\jsloader\\libdefn\0_data\";a:4:{s:4:\"name\";s:7:\"jsviews\";s:6:\"module\";s:11:\"JQueryTools\";s:6:\"jsfile\";a:1:{i:0;s:101:\"F:\\OneDrive\\Website\\htdocs\\@\\vietnammuslimtourkaka\\modules\\JQueryTools/lib/jsviews.lib/jsviews.min.js\";}s:7:\"depends\";a:1:{i:0;s:6:\"jquery\";}}}s:8:\"lightbox\";O:29:\"CGExtensions\\jsloader\\libdefn\":1:{s:36:\"\0CGExtensions\\jsloader\\libdefn\0_data\";a:5:{s:4:\"name\";s:8:\"lightbox\";s:6:\"module\";s:11:\"JQueryTools\";s:6:\"jsfile\";a:1:{i:0;s:114:\"F:\\OneDrive\\Website\\htdocs\\@\\vietnammuslimtourkaka\\modules\\JQueryTools/lib/lightbox.lib/jquery.lightbox-0.5.min.js\";}s:7:\"cssfile\";a:1:{i:0;s:115:\"F:\\OneDrive\\Website\\htdocs\\@\\vietnammuslimtourkaka\\modules\\JQueryTools/lib/lightbox.lib/css/jquery.lightbox-0.5.css\";}s:7:\"depends\";a:1:{i:0;s:6:\"jquery\";}}}s:11:\"tablesorter\";O:29:\"CGExtensions\\jsloader\\libdefn\":1:{s:36:\"\0CGExtensions\\jsloader\\libdefn\0_data\";a:4:{s:4:\"name\";s:11:\"tablesorter\";s:6:\"module\";s:11:\"JQueryTools\";s:6:\"jsfile\";a:2:{i:0;s:116:\"F:\\OneDrive\\Website\\htdocs\\@\\vietnammuslimtourkaka\\modules\\JQueryTools/lib/tablesorter.lib/jquery.tablesorter.min.js\";i:1;s:109:\"F:\\OneDrive\\Website\\htdocs\\@\\vietnammuslimtourkaka\\modules\\JQueryTools/lib/tablesorter.lib/jquery.metadata.js\";}s:7:\"depends\";a:1:{i:0;s:6:\"jquery\";}}}s:2:\"ui\";O:29:\"CGExtensions\\jsloader\\libdefn\":1:{s:36:\"\0CGExtensions\\jsloader\\libdefn\0_data\";a:5:{s:4:\"name\";s:2:\"ui\";s:6:\"module\";s:11:\"JQueryTools\";s:6:\"jsfile\";a:1:{i:0;s:98:\"F:\\OneDrive\\Website\\htdocs\\@\\vietnammuslimtourkaka\\modules\\JQueryTools/lib/ui.lib/jquery-ui.min.js\";}s:7:\"cssfile\";a:1:{i:0;s:99:\"F:\\OneDrive\\Website\\htdocs\\@\\vietnammuslimtourkaka\\modules\\JQueryTools/lib/ui.lib/jquery-ui.min.css\";}s:7:\"depends\";a:1:{i:0;s:6:\"jquery\";}}}s:13:\"cg_datepicker\";O:29:\"CGExtensions\\jsloader\\libdefn\":1:{s:36:\"\0CGExtensions\\jsloader\\libdefn\0_data\";a:2:{s:4:\"name\";s:13:\"cg_datepicker\";s:8:\"callback\";s:36:\"\\JQueryTools\\datepicker_plugin::load\";}}}', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('CGSmartImage_mapi_pref_embed_mode', 'sizelimit', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('CGSmartImage_mapi_pref_embed_size', '32', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('CGSmartImage_mapi_pref_embed_type', 'png,jpg,gif', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('CGSmartImage_mapi_pref_image_url_prefix', '', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('CGSmartImage_mapi_pref_image_url_hascachedir', '0', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('CGSmartImage_mapi_pref_cache_age', '0', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('CGSmartImage_mapi_pref_responsive', '1', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('CGSmartImage_mapi_pref_progressive', '1', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('CGSmartImage_mapi_pref_aliases', 'a:4:{s:13:\"std_thumbnail\";a:2:{s:4:\"name\";s:13:\"std_thumbnail\";s:7:\"options\";s:39:\"filter_watermark=1 width=150 height=150\";}s:11:\"std_200x200\";a:2:{s:4:\"name\";s:11:\"std_200x200\";s:7:\"options\";s:66:\"filter_croptofit=\'200,200,c,1\' notag=\'1\' noembed=\'1\' quality=\'100\'\";}s:8:\"std_w800\";a:2:{s:4:\"name\";s:8:\"std_w800\";s:7:\"options\";s:57:\"filter_resize=\'w,800\' notag=\'1\' noembed=\'1\' quality=\'100\'\";}s:11:\"std_300x300\";a:2:{s:4:\"name\";s:11:\"std_300x300\";s:7:\"options\";s:66:\"filter_croptofit=\'300,300,c,1\' notag=\'1\' noembed=\'1\' quality=\'100\'\";}}', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('CTLModuleMaker_mapi_pref_checkversion', '1', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('MleCMS_mapi_pref_mle_hierarchy_switch', '1', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('MleCMS_mapi_pref_mle_auto_redirect', '0', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('MleCMS_mapi_pref_default_mle_template', '{if $langs_count}\n    {foreach from=$langs item=l name=language}\n    {capture assign=\"lang_href\"}{cms_selflink href=$l.alias}{/capture}\n    {if $lang_href}\n        {if $page_alias==$l.alias}\n            <span class=\"active\">\n        {if $l.flag}<img src=\"uploads/{$l.flag}\" alt=\"{$l.name}\" title=\"{$l.name}\"  />{else}{$l.name}{/if}\n    </span>\n{else}\n    <a   {if $l.flag}style=\"-ms-filter:\'progid:DXImageTransform.Microsoft.Alpha(Opacity=50)\'; filter: alpha(opacity=50); opacity:.5;\"{/if} href=\"{$lang_href}\">\n{if $l.flag}<img src=\"uploads/{$l.flag}\" alt=\"{$l.name}\" title=\"{$l.name}\"  />{else}{$l.name}{/if}\n</a>\n{/if}\n{/if}\n{/foreach}\n{/if}', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('MleCMS_mapi_pref_current_mle_template', 'Flags', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('TinyMCE_mapi_pref_skin', 'default', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('TinyMCE_mapi_pref_source_formatting', '1', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('TinyMCE_mapi_pref_editor_width', '800', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('TinyMCE_mapi_pref_editor_width_auto', '1', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('TinyMCE_mapi_pref_editor_width_unit', 'px', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('TinyMCE_mapi_pref_editor_height', '400', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('TinyMCE_mapi_pref_editor_height_auto', '1', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('TinyMCE_mapi_pref_editor_height_unit', 'px', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('TinyMCE_mapi_pref_show_path', '1', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('TinyMCE_mapi_pref_striptags', 'true', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('TinyMCE_mapi_pref_imagebrowserstyle', 'both', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('TinyMCE_mapi_pref_allowscaling', '0', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('TinyMCE_mapi_pref_scalingwidth', '800', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('TinyMCE_mapi_pref_scalingheight', '600', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('TinyMCE_mapi_pref_filepickerstyle', 'both', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('TinyMCE_mapi_pref_fpwidth', '700', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('TinyMCE_mapi_pref_fpheight', '500', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('TinyMCE_mapi_pref_toolbar1', 'cut,paste,pastetext,pasteword,copy,separator,justifyleft,justifycenter,justifyright,justifyfull,separator,styleselect,formatselect,fontselect,fontsizeselect', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('TinyMCE_mapi_pref_toolbar2', 'bold,italic,underline,strikethrough,advhr,separator,bullist,numlist,separator,outdent,indent,separator,undo,redo,separator,customdropdown,cmslinker,link,unlink,anchor,image,charmap,cleanup,separator,forecolor,backcolor,separator,code,spellchecker,fullscreen,help', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('TinyMCE_mapi_pref_toolbar3', '', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('TinyMCE_mapi_pref_allow_tables', '1', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('TinyMCE_mapi_pref_allowupload', '0', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('TinyMCE_mapi_pref_showtogglebutton', '1', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('TinyMCE_mapi_pref_advanced_toolbar1', 'cut,paste,pastetext,pasteword,copy,separator,justifyleft,justifycenter,justifyright,justifyfull,separator,styleselect,formatselect,fontselect,fontsizeselect', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('TinyMCE_mapi_pref_advanced_toolbar2', 'bold,italic,underline,strikethrough,advhr,separator,bullist,numlist,separator,outdent,indent,separator,undo,redo,separator,customdropdown,cmslinker,link,unlink,anchor,image,charmap,cleanup,separator,forecolor,backcolor,separator,code,spellchecker,fullscreen,help', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('TinyMCE_mapi_pref_advanced_toolbar3', '', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('TinyMCE_mapi_pref_advanced_allow_tables', '1', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('TinyMCE_mapi_pref_advanced_allowupload', '0', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('TinyMCE_mapi_pref_advanced_showtogglebutton', '1', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('TinyMCE_mapi_pref_front_toolbar1', 'cut,paste,pastetext,pasteword,copy,separator,justifyleft,justifycenter,justifyright,justifyfull,separator,styleselect,formatselect,fontselect,fontsizeselect', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('TinyMCE_mapi_pref_front_toolbar2', 'bold,italic,underline,strikethrough,advhr,separator,bullist,numlist,separator,outdent,indent,separator,undo,redo,separator,cmslinker,link,unlink,anchor,image,charmap,cleanup,separator,forecolor,backcolor,separator,code,spellchecker,fullscreen,help', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('TinyMCE_mapi_pref_front_toolbar3', '', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('TinyMCE_mapi_pref_front_allow_tables', '1', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('TinyMCE_mapi_pref_front_showtogglebutton', '1', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('TinyMCE_mapi_pref_plugins', 'paste,advimage,advlink,contextmenu,inlinepopups,spellchecker', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('TinyMCE_mapi_pref_newlinestyle', 'p', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('TinyMCE_mapi_pref_usehtml5scheme', '0', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('TinyMCE_mapi_pref_usecompression', '0', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('TinyMCE_mapi_pref_entityencoding', 'raw', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('TinyMCE_mapi_pref_bodycss', '', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('TinyMCE_mapi_pref_forcedrootblock', 'false', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('TinyMCE_mapi_pref_customdropdown', 'Insert CMS version info|{cms_version} {cms_versionname}\n---|---\nInsert Smarty {literal} around selection|{literal}|{/literal}', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('TinyMCE_mapi_pref_extraconfig', '', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('TinyMCE_mapi_pref_forcecleanpaste', '1', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('TinyMCE_mapi_pref_startenabled', '1', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('TinyMCE_mapi_pref_loadcmslinker', '1', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('TinyMCE_mapi_pref_cmslinkerstyle', 'selflink', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('TinyMCE_mapi_pref_cmslinkeradds', '', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('TinyMCE_mapi_pref_usestaticconfig', '0', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('TinyMCE_mapi_pref_ignoremodifyfiles', '0', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('TinyMCE_mapi_pref_dropdownblockformats', 'h1,h2,h3,h4,h5,h6,blockquote,dt,dd,code,samp', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('PruneAdminlog_lastexecute', '1701330217', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('pseudocron_lastrun', '1701330217', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('ListIt2HeaderTexts_mapi_pref_listit2headertexts_default_search_template', 'default', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('ListIt2HeaderTexts_mapi_pref_listit2headertexts_default_category_template', 'default', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('ListIt2HeaderTexts_mapi_pref_listit2headertexts_default_archive_template', 'default', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('ListIt2HeaderTexts_mapi_pref_sortorder', 'ASC', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('ListIt2HeaderTexts_mapi_pref_listit2headertexts_default_detail_template', 'default', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('ListIt2HeaderTexts_mapi_pref_listit2headertexts_default_summary_template', 'default', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('ListIt2HeaderTexts_mapi_pref_url_prefix', 'listit2headertexts', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('ListIt2HeaderTexts_mapi_pref_adminsection', 'content', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('ListIt2HeaderTexts_mapi_pref_friendlyname', 'ListIt2HeaderTexts', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('ListIt2HeaderTexts_mapi_pref_item_singular', 'Item', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('ListIt2HeaderTexts_mapi_pref_item_plural', 'Items', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('ListIt2HeaderTexts_mapi_pref_item_title', 'Title', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('ListIt2AboutUs_mapi_pref_listit2aboutus_default_summary_template', 'default', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('ListIt2AboutUs_mapi_pref_listit2aboutus_default_detail_template', 'default', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('ListIt2AboutUs_mapi_pref_listit2aboutus_default_search_template', 'default', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('ListIt2AboutUs_mapi_pref_listit2aboutus_default_category_template', 'default', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('ListIt2AboutUs_mapi_pref_listit2aboutus_default_archive_template', 'default', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('ListIt2AboutUs_mapi_pref_sortorder', 'ASC', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('ListIt2AboutUs_mapi_pref_adminsection', 'content', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('ListIt2AboutUs_mapi_pref_url_prefix', 'about-us', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('ListIt2AboutUs_mapi_pref_friendlyname', 'ListIt2AboutUs', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('ListIt2AboutUs_mapi_pref_item_singular', 'Item', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('ListIt2AboutUs_mapi_pref_item_plural', 'Items', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('ListIt2AboutUs_mapi_pref_item_title', 'Title', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('ListIt2AboutUs_mapi_pref_moddescription', 'AboutUs allows you to create lists that you can display throughout your website.', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('ListIt2AboutUs_mapi_pref_display_create_date', '0', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('ListIt2AboutUs_mapi_pref_item_cols', 'alias,create_time,modified_time', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('ListIt2AboutUs_mapi_pref_items_per_page', '20', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('ListIt2AboutUs_mapi_pref_display_inline', '1', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('ListIt2AboutUs_mapi_pref_subcategory', '1', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('ListIt2AboutUs_mapi_pref_detailpage', NULL, NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('ListIt2AboutUs_mapi_pref_summarypage', NULL, NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('ListIt2AboutUs_mapi_pref_reindex_search', '1', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('ListIt2Testimonials_mapi_pref_listit2testimonials_default_summary_template', 'default', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('ListIt2Testimonials_mapi_pref_listit2testimonials_default_detail_template', 'default', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('ListIt2Testimonials_mapi_pref_listit2testimonials_default_search_template', 'default', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('ListIt2Testimonials_mapi_pref_listit2testimonials_default_category_template', 'default', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('ListIt2Testimonials_mapi_pref_listit2testimonials_default_archive_template', 'default', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('ListIt2Testimonials_mapi_pref_sortorder', 'ASC', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('ListIt2Testimonials_mapi_pref_adminsection', 'content', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('ListIt2Testimonials_mapi_pref_url_prefix', 'testimonials', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('ListIt2Testimonials_mapi_pref_friendlyname', 'ListIt2Testimonials', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('ListIt2Testimonials_mapi_pref_item_singular', 'Item', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('ListIt2Testimonials_mapi_pref_item_plural', 'Items', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('ListIt2Testimonials_mapi_pref_item_title', 'Title', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('ListIt2Testimonials_mapi_pref_moddescription', 'ListItExtended allows you to create lists that you can display throughout your website.', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('ListIt2Testimonials_mapi_pref_display_create_date', '0', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('ListIt2Testimonials_mapi_pref_item_cols', 'create_time,modified_time,address,say', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('ListIt2Testimonials_mapi_pref_items_per_page', '20', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('ListIt2Testimonials_mapi_pref_display_inline', '1', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('ListIt2Testimonials_mapi_pref_subcategory', '1', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('ListIt2Testimonials_mapi_pref_detailpage', NULL, NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('ListIt2Testimonials_mapi_pref_summarypage', NULL, NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('ListIt2Testimonials_mapi_pref_reindex_search', '1', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('ListIt2Social_mapi_pref_listit2social_default_summary_template', 'default', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('ListIt2Social_mapi_pref_listit2social_default_detail_template', 'default', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('ListIt2Social_mapi_pref_listit2social_default_search_template', 'default', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('ListIt2Social_mapi_pref_listit2social_default_category_template', 'default', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('ListIt2Social_mapi_pref_listit2social_default_archive_template', 'default', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('ListIt2Social_mapi_pref_sortorder', 'ASC', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('ListIt2Social_mapi_pref_adminsection', 'content', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('ListIt2Social_mapi_pref_url_prefix', 'listit2social', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('ListIt2Social_mapi_pref_friendlyname', 'ListIt2Social', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('ListIt2Social_mapi_pref_item_singular', 'Item', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('ListIt2Social_mapi_pref_item_plural', 'Items', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('ListIt2Social_mapi_pref_item_title', 'Title', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('ListIt2Menu_mapi_pref_listit2menu_default_summary_template', 'default', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('ListIt2Menu_mapi_pref_listit2menu_default_detail_template', 'default', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('ListIt2Menu_mapi_pref_listit2menu_default_search_template', 'default', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('ListIt2Menu_mapi_pref_listit2menu_default_category_template', 'default', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('ListIt2Menu_mapi_pref_listit2menu_default_archive_template', 'default', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('ListIt2Menu_mapi_pref_sortorder', 'ASC', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('ListIt2Menu_mapi_pref_adminsection', 'content', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('ListIt2Menu_mapi_pref_url_prefix', 'menu', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('ListIt2Menu_mapi_pref_friendlyname', 'ListIt2Menu', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('ListIt2Menu_mapi_pref_item_singular', 'Item', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('ListIt2Menu_mapi_pref_item_plural', 'Items', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('ListIt2Menu_mapi_pref_item_title', 'Title', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('ListIt2Menu_mapi_pref_moddescription', 'ListItExtended allows you to create lists that you can display throughout your website.', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('ListIt2Menu_mapi_pref_display_create_date', '0', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('ListIt2Menu_mapi_pref_item_cols', 'alias,category,free,large,price,donvi', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('ListIt2Menu_mapi_pref_items_per_page', '50', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('ListIt2Menu_mapi_pref_display_inline', '1', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('ListIt2Menu_mapi_pref_subcategory', '1', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('ListIt2Menu_mapi_pref_detailpage', NULL, NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('ListIt2Menu_mapi_pref_summarypage', NULL, NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('ListIt2Menu_mapi_pref_reindex_search', '1', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('ListIt2Gallery_mapi_pref_listit2gallery_default_summary_template', 'default', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('ListIt2Gallery_mapi_pref_listit2gallery_default_detail_template', 'default', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('ListIt2Gallery_mapi_pref_listit2gallery_default_search_template', 'default', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('ListIt2Gallery_mapi_pref_listit2gallery_default_category_template', 'default', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('ListIt2Gallery_mapi_pref_listit2gallery_default_archive_template', 'default', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('ListIt2Gallery_mapi_pref_sortorder', 'ASC', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('ListIt2Gallery_mapi_pref_adminsection', 'content', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('ListIt2Gallery_mapi_pref_url_prefix', 'listit2gallery', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('ListIt2Gallery_mapi_pref_friendlyname', 'ListIt2Gallery', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('ListIt2Gallery_mapi_pref_item_singular', 'Item', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('ListIt2Gallery_mapi_pref_item_plural', 'Items', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('ListIt2Gallery_mapi_pref_item_title', 'Title', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('ListIt2Staff_mapi_pref_listit2staff_default_summary_template', 'default', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('ListIt2Staff_mapi_pref_listit2staff_default_detail_template', 'default', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('ListIt2Staff_mapi_pref_listit2staff_default_search_template', 'default', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('ListIt2Staff_mapi_pref_listit2staff_default_category_template', 'default', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('ListIt2Staff_mapi_pref_listit2staff_default_archive_template', 'default', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('ListIt2Staff_mapi_pref_sortorder', 'ASC', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('ListIt2Staff_mapi_pref_adminsection', 'content', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('ListIt2Staff_mapi_pref_url_prefix', 'listit2staff', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('ListIt2Staff_mapi_pref_friendlyname', 'ListIt2Staff', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('ListIt2Staff_mapi_pref_item_singular', 'Item', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('ListIt2Staff_mapi_pref_item_plural', 'Items', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('ListIt2Staff_mapi_pref_item_title', 'Title', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('ListIt2Slider_mapi_pref_listit2slider_default_summary_template', 'default', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('ListIt2Slider_mapi_pref_listit2slider_default_detail_template', 'default', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('ListIt2Slider_mapi_pref_listit2slider_default_search_template', 'default', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('ListIt2Slider_mapi_pref_listit2slider_default_category_template', 'default', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('ListIt2Slider_mapi_pref_listit2slider_default_archive_template', 'default', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('ListIt2Slider_mapi_pref_sortorder', 'ASC', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('ListIt2Slider_mapi_pref_adminsection', 'content', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('ListIt2Slider_mapi_pref_url_prefix', 'listit2slider', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('ListIt2Slider_mapi_pref_friendlyname', 'ListIt2Slider', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('ListIt2Slider_mapi_pref_item_singular', 'Item', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('ListIt2Slider_mapi_pref_item_plural', 'Items', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('ListIt2Slider_mapi_pref_item_title', 'Title', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('ListIt2Gallery_mapi_pref_moddescription', 'ListItExtended allows you to create lists that you can display throughout your website.', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('ListIt2Gallery_mapi_pref_display_create_date', '0', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('ListIt2Gallery_mapi_pref_item_cols', 'create_time,modified_time,wide', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('ListIt2Gallery_mapi_pref_items_per_page', '50', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('ListIt2Gallery_mapi_pref_display_inline', '1', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('ListIt2Gallery_mapi_pref_subcategory', '1', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('ListIt2Gallery_mapi_pref_detailpage', NULL, NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('ListIt2Gallery_mapi_pref_summarypage', NULL, NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('ListIt2Gallery_mapi_pref_reindex_search', '1', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('ListIt2Blog_mapi_pref_listit2blog_default_summary_template', 'default', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('ListIt2Blog_mapi_pref_listit2blog_default_detail_template', 'default', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('ListIt2Blog_mapi_pref_listit2blog_default_search_template', 'default', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('ListIt2Blog_mapi_pref_listit2blog_default_category_template', 'default', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('ListIt2Blog_mapi_pref_listit2blog_default_archive_template', 'default', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('ListIt2Blog_mapi_pref_sortorder', 'ASC', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('ListIt2Blog_mapi_pref_adminsection', 'content', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('ListIt2Blog_mapi_pref_url_prefix', 'listit2blog', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('ListIt2Blog_mapi_pref_friendlyname', 'ListIt2Blog', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('ListIt2Blog_mapi_pref_item_singular', 'Item', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('ListIt2Blog_mapi_pref_item_plural', 'Items', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('ListIt2Blog_mapi_pref_item_title', 'Title', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('ListIt2Blog_mapi_pref_moddescription', 'ListItExtended allows you to create lists that you can display throughout your website.', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('ListIt2Blog_mapi_pref_display_create_date', '0', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('ListIt2Blog_mapi_pref_item_cols', 'alias,create_time,modified_time,source', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('ListIt2Blog_mapi_pref_items_per_page', '20', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('ListIt2Blog_mapi_pref_display_inline', '0', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('ListIt2Blog_mapi_pref_subcategory', '0', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('ListIt2Blog_mapi_pref_detailpage', NULL, NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('ListIt2Blog_mapi_pref_summarypage', NULL, NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('ListIt2Blog_mapi_pref_reindex_search', '0', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('TinyMCE_mapi_pref_showfilemanagement', '1', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('TinyMCE_mapi_pref_restrictdirs', '0', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('TinyMCE_mapi_pref_advanced_showfilemanagement', '0', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('__listcontent_timelock__', '1556090083', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('SiteMapMadeSimple_mapi_pref_xml_template_text', '<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<urlset xmlns=\"http://www.google.com/schemas/sitemap/0.9\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.google.com/schemas/sitemap/0.9 http://www.google.com/schemas/sitemap/0.9/sitemap.xsd\">{strip}\n{foreach $output as $page}\n<url>\n  <loc>{$page->url}</loc>\n  <lastmod>{$page->date|date_format:\"%Y-%m-%d\"}</lastmod>\n  <priority>{$page->priority}</priority>\n  <changefreq>{$page->frequency}</changefreq>\n</url>\n{/foreach}\n{/strip}</urlset>', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('SiteMapMadeSimple_mapi_pref_dflt_xml', 'Sample', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('SiteMapMadeSimple_mapi_pref_allow_hidden', '0', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('SiteMapMadeSimple_mapi_pref_change_frequency', 'auto', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('SiteMapMadeSimple_mapi_pref_static_sitemap', '1', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('SiteMapMadeSimple_mapi_pref_dynamic_update', '1', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('SiteMapMadeSimple_mapi_pref_priority_field', '', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('ClearCache_lastexecute', '1701330217', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('CTLModuleMaker_mapi_pref_autosave', '1', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('CTLModuleMaker_mapi_pref_innerdebug', '0', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('CTLModuleMaker_mapi_pref_doupgrade', '1', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('CTLModuleMaker_mapi_pref_allowsinglelevel', '1', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('vipmember_mapi_pref_listtemplate_items', 'list_default', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('vipmember_mapi_pref_finaltemplate', 'final_default', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('FileManager_mapi_pref_advancedmode', '1', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('FileManager_mapi_pref_showthumbnails', '1', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('FileManager_mapi_pref_permissionstyle', 'xxx', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('FileManager_mapi_pref_thousanddelimiter', '.', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('FileManager_mapi_pref_create_thumbnails', '0', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('vipmember_mapi_pref_makerversion', '1.7', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('vipmember_mapi_pref_action', 'changedeftemplates', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('CGSmartImage_mapi_pref_embed_sizelimit', '0', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('CGSmartImage_mapi_pref_embed_types', '', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('CGSmartImage_mapi_pref_croptofit_default_loc', 'c', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('CGSmartImage_mapi_pref_cache_path', 'uploads/_CGSmartImage', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('CGSmartImage_mapi_pref_silent', '0', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('CGSmartImage_mapi_pref_checkmemory', '1', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('CGSmartImage_mapi_pref_force_extension', '1', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('CGSmartImage_mapi_pref_responsive_breakpoints', '', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('ModuleManager_mapi_pref_dl_chunksize', '256', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('ModuleManager_mapi_pref_latestdepends', '1', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('ModuleManager_mapi_pref_disable_caching', '0', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('CustomGS_mapi_pref_input_custom_modulename', 'Custom Global Settings', NULL, NULL);
INSERT INTO `cms_siteprefs` VALUES ('CustomGS_mapi_pref_admin_section', 'content', NULL, NULL);

-- ----------------------------
-- Table structure for cms_templates
-- ----------------------------
DROP TABLE IF EXISTS `cms_templates`;
CREATE TABLE `cms_templates`  (
  `template_id` int(11) NOT NULL,
  `template_name` varchar(160) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `template_content` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `stylesheet` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `encoding` varchar(25) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `active` tinyint(4) NULL DEFAULT NULL,
  `default_template` tinyint(4) NULL DEFAULT NULL,
  `create_date` datetime NULL DEFAULT NULL,
  `modified_date` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`template_id`) USING BTREE,
  INDEX `cms_index_templates_by_template_name`(`template_name`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of cms_templates
-- ----------------------------
INSERT INTO `cms_templates` VALUES (24, 'Home', '{strip}\r\n{capture}{content default=\'\' wysiwyg=\'false\'}{/capture}\r\n{get_root_page_alias assign=\'page_lang\'}\r\n{/strip}\r\n<!DOCTYPE html>\r\n<html lang=\"{$page_lang}\">\r\n    <head>\r\n        {metadata}\r\n        {cms_stylesheet}\r\n        <!-- Latest compiled and minified JavaScript -->\r\n        <script src=\"https://stackpath.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js\"></script>\r\n        {literal}\r\n            <script type=\"text/javascript\">\r\n                $(document).ready(function () {\r\n                    $(\".home\").trigger(\'click\');\r\n                    $(\'#myModal\').modal(\'show\');\r\n                });\r\n\r\n                $(function () {\r\n                    $(\'a[href*=\"#\"]:not([href=\"#\"])\').click(function () {\r\n                        if (location.pathname.replace(/^\\//, \'\') == this.pathname.replace(/^\\//, \'\') && location.hostname == this.hostname) {\r\n                            var target = $(this.hash);\r\n                            target = target.length ? target : $(\'[name=\' + this.hash.slice(1) + \']\');\r\n                            if (target.length) {\r\n                                $(\'html, body\').animate({\r\n                                    scrollTop: target.offset().top\r\n                                }, 1000);\r\n                                return false;\r\n                            }\r\n                        }\r\n                    });\r\n                });\r\n            </script>\r\n        {/literal}\r\n        {$CustomGS.Facebook_Conversion_Code}\r\n    </head>\r\n\r\n    <body id=\"hungry-home\" class=\"home\">\r\n\r\n        <div class=\"modal\" id=\"myModal\" tabindex=\"-1\" style=\"z-index:99999\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">\r\n            <div class=\"modal-dialog\">\r\n                <div class=\"modal-content\">\r\n                    <div class=\"modal-body\">\r\n                        <a href=\"{cms_selflink href=vip}\"><img src=\"event/vip.jpg\" alt=\"VIP\" style=\"width:100%;\" /></a>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n\r\n        <div id=\"hungry-preloader-container\">\r\n            <div class=\"hungry-preloader\">\r\n                <span class=\"bubble-01\"></span>\r\n                <span class=\"bubble-02\"></span>\r\n                <span class=\"bubble-03\"></span>\r\n            </div>\r\n        </div>\r\n\r\n        <header id=\"single-page-header\">\r\n            <div class=\"site-navbar\">\r\n                <div class=\"grid-container\">\r\n                    <div class=\"grid-20 tablet-grid-50 mobile-grid-50\">\r\n                        <a href=\"{root_url}\">\r\n                            <img class=\"site-logo\" src=\"images/assets/logo.png\" alt=\"{sitename}-Logo\" />\r\n                        </a>\r\n                    </div>\r\n\r\n                    <div class=\"nav-container grid-80 tablet-grid-50 mobile-grid-50\">\r\n                        <div class=\"mobile-nav\">\r\n                            <i class=\"fa fa-bars\"></i>\r\n                        </div>\r\n\r\n                        <nav class=\"main-navigation\" role=\"navigation\">\r\n                            <div class=\"mobile-header hide-on-desktop\">\r\n                                <h2>{sitename}</h2>\r\n                                <div class=\"mobile-close\">\r\n                                    <i class=\"fa fa-times\"></i>\r\n                                </div>\r\n                            </div>\r\n                            {menu template=\"main_menu\" childrenof=\"vi\"}\r\n                        </nav>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n\r\n            <div class=\"single-page-header-content\">\r\n                <!--Slider Images -->\r\n                <div class=\"cycle-slideshow\" data-cycle-swipe=\"true\" data-cycle-swipe-fx=\"fade\" data-cycle-fx=\"fade\" data-cycle-speed=\"1200\" data-cycle-timeout=\"16000\">\r\n                    <!-- Images -->\r\n                    {ListIt2Slider}\r\n                    <!-- Prev/Next Buttons -->\r\n                    <div class=\"cycle-prev\"><i class=\"fa fa-chevron-left\"></i></div>\r\n                    <div class=\"cycle-next\"><i class=\"fa fa-chevron-right\"></i></div>\r\n                </div>\r\n                <!-- END Slider Images -->\r\n\r\n                <!-- START Slider Texts -->\r\n                <div class=\"single-page-header-text\">\r\n                    <!-- Pre-slogan -->\r\n                    <div class=\"tilt-left\">\r\n                        <h3 class=\"header-text-pre-slogan\">{sitename}<em>&hellip;</em></h3>\r\n                    </div>\r\n                    <!-- Slogan Rotator -->\r\n                    <div class=\"tlt\">\r\n                        {ListIt2HeaderTexts}\r\n                    </div>\r\n                    <!-- Divider -->\r\n                    <div class=\"header-text-divider\"></div>\r\n                </div>\r\n                <!-- END Slider Texts -->\r\n            </div>\r\n\r\n            <!-- START Social Icons -->\r\n            <div class=\"single-page-social-icons\">\r\n                {ListIt2Social}\r\n            </div>\r\n            <!-- END Social Icons -->\r\n        </header>\r\n\r\n        <!-- START Main Content -->\r\n        <main class=\"site-content\" role=\"main\">\r\n            <!-- START Section - About Us -->\r\n            <section id=\"hungry-about-us\" class=\"section-container aboutthis\">\r\n                <!-- START Section Heading -->\r\n                <div class=\"wow fadeInDown\" data-wow-duration=\"2s\" data-wow-offset=\"250\">\r\n                    <header class=\"section-heading\">\r\n                        <h2 class=\"section-heading-title\">About Us</h2>\r\n                        <div class=\"section-heading-subtitle-container tilt-left\">\r\n                            <h4 class=\"section-heading-subtitle\">Bạn biết gì về nhà hàng Khoa Ngan?</h4>\r\n                        </div>\r\n                    </header>\r\n                </div>\r\n                <!-- END Section Heading -->\r\n                <div class=\"grid-container\">\r\n                    {ListIt2AboutUs orderby=\"rand\" pagelimit=\"1\"}\r\n                </div>\r\n            </section>\r\n            <!-- END Section - About Us -->\r\n\r\n            <section id=\"hungry-testimonials\" class=\"section-container parallax\">\r\n                <div class=\"grid-container\">\r\n                    <div class=\"wow fadeIn\" data-wow-duration=\"2s\" data-wow-offset=\"250\">\r\n                        <h1 class=\"section-heading-alt-title\">{MleCMS name=\"snippet_customer-say\"}<span>&hellip;</span></h1>\r\n                    </div>\r\n                    {ListIt2Testimonials}\r\n                </div>\r\n            </section>\r\n\r\n            <!-- START Section - Menu -->\r\n            <section id=\"hungry-menu\" class=\"section-container\">\r\n                <!-- START Section Heading -->\r\n                <div class=\"wow fadeInDown\" data-wow-duration=\"2s\" data-wow-offset=\"250\">\r\n                    <header class=\"section-heading\">\r\n                        <h2 class=\"section-heading-title\">Our Menu</h2>\r\n                        <div class=\"section-heading-subtitle-container tilt-left\">\r\n                            <h4 class=\"section-heading-subtitle\">Tươi nhất, ngon nhất...</h4>\r\n                        </div>\r\n                    </header>\r\n                </div>\r\n                <!-- END Section Heading -->\r\n\r\n                <div class=\"grid-container\">\r\n                    {ListIt2Menu action=\"category\"}\r\n                </div>\r\n            </section>\r\n            <!-- END Section - Menu -->\r\n\r\n            <!-- START Section - Slogan 01 -->\r\n            <section id=\"hungry-slogan-01\" class=\"section-container parallax\">\r\n                <div class=\"grid-container\">\r\n                    <div class=\"grid-100 tablet-grid-100 mobile-grid-100 no-margin\">\r\n                        <div class=\"wow zoomIn\" data-wow-duration=\"2s\" data-wow-offset=\"250\">\r\n                            <div class=\"hungry-slogan\">\r\n                                <h2 class=\"hungry-slogan-text\">“Ngan Vịt của nhà hàng Khoa Ngan cũng có đầy đủ <em>hộ chiếu, chứng minh thư</em> như ai”<br />\r\n                                    – như lời bà chủ vui tính của nhà hàng đã từng nói!</h2>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </section>\r\n            <!-- END Section - Slogan 01 -->\r\n\r\n            <!-- START Section - Staff -->\r\n            <!--section id=\"hungry-staff\" class=\"section-container\">\r\n                    <div class=\"wow fadeInDown\" data-wow-duration=\"2s\" data-wow-offset=\"250\">\r\n                            <header class=\"section-heading\">\r\n                                    <h2 class=\"section-heading-title\">Our Staff</h2>\r\n                                    <div class=\"section-heading-subtitle-container tilt-left\">\r\n                                            <h4 class=\"section-heading-subtitle\">The Friendliest People</h4>\r\n                                    </div>\r\n                            </header>\r\n                    </div>\r\n                    <div class=\"grid-container\">\r\n                    ListIt2Staff\r\n                    </div>\r\n            </section-->\r\n            <!-- END Section - Staff -->\r\n\r\n            <!-- START Section - Gallery -->\r\n            <section id=\"hungry-gallery\" class=\"section-container\">\r\n                <!-- START Section Heading -->\r\n                <div class=\"wow fadeInDown\" data-wow-duration=\"2s\" data-wow-offset=\"250\">\r\n                    <header class=\"section-heading\">\r\n                        <h2 class=\"section-heading-title\">The Gallery</h2>\r\n                        <div class=\"section-heading-subtitle-container tilt-left\">\r\n                            <h4 class=\"section-heading-subtitle\">{sitename} 2017!</h4>\r\n                        </div>\r\n                    </header>\r\n                </div>\r\n                <!-- END Section Heading -->\r\n                <!-- START Main Gallery -->\r\n                <div class=\"grid-container\">\r\n                    <div class=\"grid-100 tablet-grid-100 mobile-grid-100\">\r\n                        <div class=\"hungry-gallery\">\r\n                            {ListIt2Gallery category=\"Galleries\"}\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n                <!-- END Main Gallery -->\r\n            </section>\r\n            <!-- END Section - Gallery -->\r\n            <!-- START Section - Slogan 02 -->\r\n            <a href=\"#\" class=\"clickable-slogan\">\r\n                <section id=\"hungry-slogan-02\" class=\"section-container parallax\">\r\n                    <div class=\"grid-container\">\r\n\r\n                        <div class=\"grid-100 tablet-grid-100 mobile-grid-100 no-margin\">\r\n                            <div class=\"wow zoomIn\" data-wow-duration=\"2s\" data-wow-offset=\"250\">\r\n                                <div class=\"hungry-slogan\">\r\n                                    <h2 class=\"hungry-slogan-text\">Chính bạn là Thượng Đế...<br />\r\n                                        <span>Call: <em>{global_content name=\'hotline\'}</em> ngay!</span></h2>\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n\r\n                    </div>\r\n                </section>\r\n            </a>\r\n            <!-- END Section - Slogan 02 -->\r\n            <!-- START Section - Customer -->\r\n            <section id=\"hungry-customer\" class=\"section-container\">\r\n                <!-- START Section Heading -->\r\n                <div class=\"wow fadeInDown\" data-wow-duration=\"2s\" data-wow-offset=\"250\">\r\n                    <header class=\"section-heading\">\r\n                        <h2 class=\"section-heading-title\">The Customer</h2>\r\n                        <div class=\"section-heading-subtitle-container tilt-left\">\r\n                            <h4 class=\"section-heading-subtitle\">{sitename} 2017!</h4>\r\n                        </div>\r\n                    </header>\r\n                </div>\r\n                <!-- END Section Heading -->\r\n\r\n                <!-- START Main Gallery -->\r\n                <div class=\"grid-container\">\r\n                    <div class=\"grid-100 tablet-grid-100 mobile-grid-100\">\r\n                        <div class=\"hungry-gallery\">\r\n                            {ListIt2Gallery category=\"Customers\"}\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n                <!-- END Main Gallery -->\r\n            </section>\r\n            <!-- END Section - Gallery -->\r\n\r\n            <!-- START Section - Slogan 02 -->\r\n            <a href=\"#\" class=\"clickable-slogan\">\r\n                <section id=\"hungry-slogan-02\" class=\"section-container parallax\">\r\n                    <div class=\"grid-container\">\r\n\r\n                        <div class=\"grid-100 tablet-grid-100 mobile-grid-100 no-margin\">\r\n                            <div class=\"wow zoomIn\" data-wow-duration=\"2s\" data-wow-offset=\"250\">\r\n                                <div class=\"hungry-slogan\">\r\n                                    <h2 class=\"hungry-slogan-text\">Hãy tới và thưởng thức, Khoa Ngan <em>{global_content name=\'add\'}</em><br />\r\n                                        <span>Hotline: <em>{global_content name=\'hotline\'}</em></span></h2>\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n\r\n                    </div>\r\n                </section>\r\n            </a>\r\n            <!-- END Section - Slogan 02 -->\r\n\r\n            <!-- START Section - Blog -->\r\n            <section id=\"hungry-promotion\" class=\"section-container parallax\">\r\n                <!-- START Section Heading -->\r\n                <div class=\"wow fadeInDown\" data-wow-duration=\"2s\" data-wow-offset=\"250\">\r\n                    <header class=\"section-heading\">\r\n                        <h2 class=\"section-heading-title\">Khoa Ngan Promotion</h2>\r\n                        <div class=\"section-heading-subtitle-container tilt-left\">\r\n                            <h4 class=\"section-heading-subtitle\">Bắt đầu từ 01/06/2019!</h4>\r\n                        </div>\r\n                    </header>\r\n                </div>\r\n                <!-- END Section Heading -->\r\n                <div class=\"grid-container\">\r\n                    <div class=\"hungry-blog-container\">\r\n                        {ListIt2Blog}\r\n                        <br class=\"clear\" />\r\n                    </div>\r\n                    <!-- \"View Blog\" Button -->\r\n                    <div class=\"wow fadeInUp\" data-wow-duration=\"2s\" data-wow-offset=\"250\">\r\n                        <div class=\"grid-100 tablet-grid-100 mobile-grid-100 aligncenter\">\r\n                            <a class=\"hungry-button dark aligncenter\" href=\"{cms_selflink href=vip}\">Đăng ký để nhận nhiều Ưu đãi</a>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </section>\r\n            <!-- END Section - Blog -->\r\n\r\n            <!-- START Section - Reservations -->\r\n            <section id=\"hungry-reservations\" class=\"section-container\">\r\n                <div class=\"wow fadeInDown\" data-wow-duration=\"2s\" data-wow-offset=\"250\">\r\n                    <header class=\"section-heading\">\r\n                        <h2 class=\"section-heading-title\">Reservations</h2>\r\n                        <div class=\"section-heading-subtitle-container tilt-left\">\r\n                            <h4 class=\"section-heading-subtitle\">Liên hệ đặt bàn!</h4>\r\n                        </div>\r\n                    </header>\r\n                </div>\r\n                <div class=\"grid-container\">\r\n                    <div class=\"prefix-10 grid-80 suffix-10 tablet-grid-100 mobile-grid-100\">\r\n                        <form name=\"frm\" id=\"hungry-reservation-form\" action=\"\" method=\"post\">\r\n                            <fieldset>\r\n                                <legend class=\"form-title\">Booking Form<span>Vui lòng nhập thông tin để Khoa Ngan liên hệ với bạn<em>*</em></span></legend>\r\n                                <div class=\"grid-50 tablet-grid-50 mobile-grid-100\">\r\n                                    <p>\r\n                                        <label for=\"res_name\">Họ và tên<span>*</span></label>\r\n                                        <input type=\"text\" name=\"res_name\" id=\"res_name\" value=\"\" required>\r\n                                    </p>\r\n                                    <p>\r\n                                        <label for=\"res_email\">E-mail<span>*</span></label>\r\n                                        <input type=\"email\" name=\"res_email\" id=\"res_email\" value=\"\" required>\r\n                                    </p>\r\n                                    <p>\r\n                                        <label for=\"res_phone\">Điện thoại<span>*</span></label>\r\n                                        <input type=\"tel\" name=\"res_phone\" id=\"res_phone\" value=\"\" required>\r\n                                    </p>\r\n                                    <p>\r\n                                        <label for=\"res_amount\">Có bao nhiêu người cùng bạn thưởng thức?<span>*</span></label>\r\n                                        <select name=\"res_amount\" id=\"res_amount\" required>\r\n                                            <option value=\"\" selected>Vui lòng chọn...</option>\r\n                                            <option value=\"1\">1</option>\r\n                                            <option value=\"2\">2</option>\r\n                                            <option value=\"3\">3</option>\r\n                                            <option value=\"4\">4</option>\r\n                                            <option value=\"5\">5</option>\r\n                                            <option value=\"6\">6</option>\r\n                                            <option value=\"7\">7</option>\r\n                                            <option value=\"8\">8</option>\r\n                                            <option value=\"9\">9</option>\r\n                                            <option value=\"More than 9\">Hơn 9 người</option>\r\n                                        </select>\r\n                                    <div class=\"multiselect\">\r\n                                        <div class=\"selectBox\" onclick=\"showCheckboxes()\">\r\n                                            <select disabled>\r\n                                                <option>Bạn muốn thưởng thức?</option>\r\n                                            </select>\r\n                                            <div class=\"overSelect\"></div>\r\n                                        </div>\r\n                                        <div id=\"checkboxes\">\r\n                                            {*ListIt2Menu category=\"food\" template_summary=\"reservations\"*}\r\n                                            <!--<label for=\"one\">\r\n                                              <input type=\"checkbox\" name=\"menu[]\" id=\"one\" />First checkbox <span>120K</span></label>\r\n                                            <label for=\"two\">\r\n                                              <input type=\"checkbox\" name=\"menu[]\" id=\"two\" />Second checkbox</label>\r\n                                            <label for=\"three\">\r\n                                              <input type=\"checkbox\" name=\"menu[]\" id=\"three\" />Third checkbox</label>-->\r\n                                        </div>\r\n                                    </div>\r\n                                    </p>\r\n                                </div>\r\n\r\n                                <div class=\"grid-50 tablet-grid-50 mobile-grid-100\">\r\n                                    <p>\r\n                                        <label for=\"res_date\">Vào ngày<span>*</span></label>\r\n                                        <input type=\"date\" name=\"res_date\" id=\"res_date\" value=\"\" required>\r\n                                    </p>\r\n                                    <p>\r\n                                        <label for=\"res_time\">Thời gian<span>*</span></label>\r\n                                        <select name=\"res_time\" id=\"res_time\" required>\r\n                                            <option value=\"\" selected>Vui lòng chọn...</option>\r\n                                            <optgroup label=\"Afternoon Reservations\">\r\n                                                <option value=\"7-9am\">7-9pm</option>\r\n                                                <option value=\"9-12am\">9:12am</option>\r\n                                                <option value=\"1pm\">1pm</option>\r\n                                                <option value=\"1:30pm\">1:30pm</option>\r\n                                            </optgroup>\r\n                                            <optgroup label=\"Evening Reservations\">\r\n                                                <option value=\"6pm\">6pm</option>\r\n                                                <option value=\"6:30pm\">6:30pm</option>\r\n                                                <option value=\"7pm\">7pm</option>\r\n                                                <option value=\"7:30pm\">7:30pm</option>\r\n                                                <option value=\"8pm\">8pm</option>\r\n                                                <option value=\"8:30pm\">8:30pm</option>\r\n                                                <option value=\"9pm\">9pm</option>\r\n                                            </optgroup>\r\n                                        </select>\r\n                                    </p>\r\n                                    <label for=\"res_email\">Thông tin thêm</label>\r\n                                    <textarea name=\"res_message\" id=\"res_message\" cols=\"8\" rows=\"8\"></textarea>\r\n                                </div>\r\n\r\n                                <div class=\"grid-50 tablet-grid-50 mobile-grid-100\">\r\n                                    <input type=\"submit\" name=\"res-submit\" id=\"res-submit\" value=\"Đặt bàn ngay!\" />\r\n                                    <div>\r\n                                        <div id=\"hungry-reservation-form-outcome\"></div>\r\n                                    </div>\r\n                                </div>\r\n                            </fieldset>\r\n                        </form>\r\n                    </div>\r\n                </div>\r\n            </section>\r\n            <!-- END Section - Reservations -->\r\n            <section id=\"hungry-contact\">\r\n                <!-- START Section Heading -->\r\n                <div class=\"wow fadeInDown\" data-wow-duration=\"2s\" data-wow-offset=\"250\">\r\n                    <header class=\"section-heading\">\r\n                        <h2 class=\"section-heading-title\">Contact Us</h2>\r\n                        <div class=\"section-heading-subtitle-container tilt-left\">\r\n                            <h4 class=\"section-heading-subtitle\">Khoa Ngan - 77 Hai Bà Trưng</h4>\r\n                        </div>\r\n                    </header>\r\n                </div>\r\n                <!-- END Section Heading -->\r\n                <!-- START Site Footer -->\r\n                <footer id=\"site-footer\">\r\n                    <!-- START Widget Area -->\r\n                    <div class=\"widget-area grid-container\">\r\n                        <!-- START Widget Column 01 -->\r\n                        <div class=\"widget-column widget-column-01 grid-33 tablet-grid-33 mobile-grid-100\">\r\n                            <!-- Contact Details Widget -->\r\n                            <aside class=\"widget widget-hungry-contact-details\">\r\n                                <h3 class=\"widget-title\">Contact Us</h3>\r\n                                <p>(Liên hệ)</p>\r\n                                <div class=\"contact-details\">\r\n                                    <div class=\"contact-phone\">\r\n                                        <i class=\"fa fa-phone-square\"></i>\r\n                                        <a class=\"phone-number-link\" href=\"tel:+84944278866\">0944.27.8866</a><a class=\"phone-number-link\" href=\"tel:+842439422206\">024.39422206</a>\r\n                                    </div>\r\n                                    <div class=\"contact-email\">\r\n                                        <i class=\"fa fa-envelope-square\"></i>\r\n                                        <a class=\"email-link\" href=\"mailto:{global_content name=\'email\'}\">{global_content name=\'email\'}</a>\r\n                                    </div>\r\n<div class=\"contact-address\">\r\n                                        <i class=\"fa fa-caret-square-o-down\"></i>\r\n                                        <address>{global_content name=\'company_name\'}</address>\r\n                                    </div>\r\n                                    <div class=\"contact-address\">\r\n                                        <!--i class=\"fa fa-caret-square-o-down\"></i-->\r\n                                        <address>Địa chỉ: {global_content name=\'add\'}</address>\r\n                                    </div>\r\n<div class=\"contact-address\">\r\n                                        <!--i class=\"fa fa-caret-square-o-down\"></i-->\r\n                                        <address>MST: {global_content name=\'mst\'}</address>\r\n                                    </div>\r\n                                </div>\r\n                            </aside>\r\n                        </div>\r\n                        <!-- END Widget Column 01 -->\r\n\r\n                        <!-- START Widget Column 02 -->\r\n                        <div class=\"widget-column widget-column-02 grid-33 tablet-grid-33 mobile-grid-100\">\r\n                            <aside class=\"widget widget-hungry-latest-recipes\">\r\n                                <h3 class=\"widget-title\">Facebook</h3>\r\n                                <p>(Liên kết mạng xã hội)</p>\r\n                                <iframe src=\"https://www.facebook.com/plugins/likebox.php?href=https://www.facebook.com/khoanganxuanay?ref=hl&amp;width=300&amp;colorscheme=light&amp;show_faces=true&amp;connections=12&amp;stream=false&amp;header=false&amp;height=150\" scrolling=\"no\" frameborder=\"0\" scrolling=\"no\" style=\"overflow: hidden; height: 150px; width: 300px;background:#fff;\"></iframe>\r\n                                <ul class=\"latest-recipes\">\r\n                                    <!--li class=\"recipe\">\r\n                                                    <a href=\"#\"><img class=\"recipe-thumbnail\" src=\"images/demo/widget-recipe-thumbnails/thumbnail-01.jpg\" alt=\"Recipe Thumbnail Image\" /></a>\r\n                                                    <h6 class=\"recipe-title\"><a href=\"#\">Angus Steak Burger</a></h6>\r\n                                                    <p class=\"recipe-description\">Aenean commodo ligula eget.</p>\r\n                                            </li-->\r\n                                </ul>\r\n                            </aside>\r\n                        </div>\r\n                        <div class=\"widget-column widget-column-03 grid-33 tablet-grid-33 mobile-grid-100\">\r\n                            <aside class=\"widget widget-hungry-opening-times\">\r\n                                <h3 class=\"widget-title\">Opening Times</h3>\r\n                                <p>(Giờ mở cửa)</p>\r\n                                <ul class=\"opening-times\">\r\n                                    <li>Phục vụ tất cả các ngày trong tuần<span>7am - 11pm</span></li>\r\n                                </ul>\r\n                            </aside>\r\n                        </div>\r\n                    </div>\r\n                    <div id=\"bottom-footer\">\r\n                        <div class=\"grid-container\">\r\n                            <!-- Footer Logo -->\r\n                            <div class=\"footer-logo grid-50 tablet-grid-100 mobile-grid-100\">\r\n                                <a href=\"{root_url}\"><img class=\"footer-logo-image\" src=\"images/assets/footer-logo.png\" alt=\"{sitename}\" /></a>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </footer>\r\n            </section>\r\n\r\n        </main>\r\n        <!-- END Main Content -->\r\n        <div id=\"btt\">\r\n            <i class=\"fa fa-angle-up\"></i>\r\n        </div>\r\n        {literal}\r\n            <style>\r\n                .multiselect {\r\n\r\n                }\r\n\r\n                .selectBox {\r\n                    position: relative;\r\n                }\r\n\r\n                .selectBox select {\r\n                    width: 100%;\r\n                    font-weight: bold;\r\n                }\r\n\r\n                .overSelect {\r\n                    position: absolute;\r\n                    left: 0;\r\n                    right: 0;\r\n                    top: 0;\r\n                    bottom: 0;\r\n                }\r\n\r\n                #checkboxes {\r\n                    display: none;\r\n\r\n                }\r\n\r\n                #checkboxes input {\r\n                    margin:10px;\r\n                    height:auto!important;\r\n                    vertical-align: middle;\r\n                }\r\n\r\n                #checkboxes span {\r\n                    float:right;\r\n                    margin-right:20px;\r\n                    text-align:right;\r\n                }\r\n\r\n                #checkboxes label {\r\n                    display: block;\r\n                }\r\n\r\n                #checkboxes label:hover {\r\n                    background-color: #1e90ff;\r\n                }\r\n            </style>{/literal}\r\n            <script type=\"text/javascript\" src=\"js/jquery-main.js\"></script>\r\n            <script type=\"text/javascript\" src=\"js/jquery-custom.js\"></script>\r\n            <script>\r\n                var expanded = false;\r\n                function showCheckboxes() {\r\n                    var checkboxes = document.getElementById(\"checkboxes\");\r\n                    if (!expanded) {\r\n                        checkboxes.style.display = \"block\";\r\n                        expanded = true;\r\n                    } else {\r\n                        checkboxes.style.display = \"none\";\r\n                        expanded = false;\r\n                    }\r\n                }\r\n            </script>\r\n            <script src=\"https://uhchat.net/code.php?f=e2c65a\"></script>\r\n        </body>\r\n\r\n    </html>', '', '', 1, 1, '2015-04-24 17:23:18', '2023-03-09 23:40:49');
INSERT INTO `cms_templates` VALUES (25, 'Event', '{process_pagedata}<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\r\n<html xmlns=\"http://www.w3.org/1999/xhtml\" xml:lang=\"en\" >\r\n<head>\r\n<title>{sitename} - {title}</title>\r\n<meta name=\"viewport\" content=\"width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1\">\r\n{cms_stylesheet}\r\n<meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge,chrome=1\">\r\n<!--[if lt IE 9]><script src=\"//html5shim.googlecode.com/svn/trunk/html5.js\"></script><![endif]-->\r\n</head>\r\n<body>\r\n{content}\r\n  <h1 class=\"register-title\">THÀNH VIÊN VIP</h1>\r\n  <form class=\"register\"><span class=\"question\">Bạn đã ăn tại nhà hàng Khoa Ngan Xưa và nay?</span>\r\n    <div class=\"register-switch\">\r\n\r\n      <input type=\"radio\" name=\"sex\" value=\"M\" id=\"sex_m\" class=\"register-switch-input\" checked>\r\n      <label for=\"sex_m\" class=\"register-switch-label\">Đã ăn</label>\r\n      <input type=\"radio\" name=\"sex\" value=\"F\" id=\"sex_f\" class=\"register-switch-input\">\r\n      <label for=\"sex_f\" class=\"register-switch-label\">Chưa ăn</label>\r\n    </div>\r\n    <input type=\"text\" class=\"register-input\" placeholder=\"Họ tên\">\r\n<input type=\"text\" class=\"register-input\" placeholder=\"Điện thoại\">\r\n    <input type=\"email\" class=\"register-input\" placeholder=\"Email\">\r\n    <input type=\"submit\" value=\"ĐĂNG KÝ\" class=\"register-button\">\r\n  </form>\r\n</body>\r\n</html>', '', '', 1, 0, '2015-09-11 23:41:55', '2015-09-12 00:14:11');
INSERT INTO `cms_templates` VALUES (26, 'Event-2', '<!DOCTYPE html>\r\n<html lang=\"en\">\r\n<head>\r\n  <meta charset=\"utf-8\">\r\n  <meta name=\"viewport\" content=\"width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1\">\r\n  <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge,chrome=1\">\r\n  <title>Đăng ký thành viên VIP - Khoa Ngan Xưa và Nay</title>\r\n  <script type=\"text/javascript\" src=\"js/jquery.js\"></script>\r\n  <script type=\"text/javascript\" src=\"js/frmValid.js\"></script>\r\n{cms_stylesheet}\r\n</head>\r\n<body>\r\n<form name=\"frm\" id=\"frm\" method=\"get\" class=\"sign-up\" autocomplete=\"off\">\r\n<h1 class=\"sign-up-title\">VIP MEMBER <br/> <span>www.khoanganxuavanay.com</span></h1>\r\n<select class=\"sign-up-input\" id=\"sex\" name=\"sex\">\r\n<option value=\"-1\" disabled selected>Giới tính</option>\r\n<option value=\"Mr.\">Nam</option>\r\n<option value=\"Ms.\">Nữ</option>\r\n</select>\r\n<input type=\"text\" id=\"birthday\" onkeypress=\"return isNumberKey(event)\" name=\"birthday\" class=\"sign-up-input\" placeholder=\"Năm sinh\" maxlength=\"4\"  style=\"width:120px\">\r\n    <input type=\"text\" name=\"fullname\" id=\"fullname\" class=\"sign-up-input\" placeholder=\"Họ và tên\" autofocus>\r\n    <input type=\"text\" id=\"phone\" name=\"phone\" onkeypress=\"return isNumberKey(event)\" class=\"sign-up-input\" placeholder=\"Điện thoại\">\r\n<input type=\"text\" id=\"address\" name=\"address\" class=\"sign-up-input\" placeholder=\"Địa chỉ\">\r\n    <input type=\"text\" name=\"math\" id=\"math\" class=\"sign-up-input\" maxlength=\"2\" placeholder=\"77\" style=\"width:55px;\"><label for=\"math\"> Vui lòng nhập mã xác nhận.</label>\r\n    <input type=\"button\" value=\"Đăng ký\" class=\"sign-up-button\">\r\n  </form>\r\n{literal}\r\n<script>\r\nfunction isNumberKey(evt)\r\n    {\r\n       var charCode = (evt.which) ? evt.which : event.keyCode;\r\n       if(charCode == 59 || charCode == 46)\r\n        return true;\r\n       if (charCode > 31 && (charCode < 48 || charCode > 57))\r\n          return false;\r\n       return true;\r\n    }\r\n	$().ready(function() {\r\n		$(\'.sign-up-button\').click(function(e) {\r\n		filter_array = new Array(\r\n			\'fullname:Bạn vui lòng nhập họ và tên của bạn.\',\r\n			\'phone:Bạn vui lòng cho biết số điện thoại.\',\r\n			\'math:Bạn nhập vào số 77 để hoàn tất đăng ký.\'\r\n		);\r\n		valid = new Array(\r\n			\'required\',\r\n			\'number\',\r\n			\'key\'\r\n		);\r\n		resultOrder = formValid(\'frm\', filter_array, valid, \'alertbox\');\r\n		if (resultOrder) {\r\n			var url = \"{/literal}{cms_selflink href=\'event-action\'}{literal}?fullname=\"+$(\'#fullname\').val()+\"&phone=\"+$(\'#phone\').val()+\"&sex=\"+$(\'#sex\').val()+\"&address=\"+$(\'#address\').val()+\"&birthday=\"+$(\'#birthday\').val();\r\n			$.post(url, $(\"#frm\").serialize(), function(resp){\r\nconsole.log(resp);\r\n			$(\"input[type=text]\").val(\"\");\r\nalert(\'Cảm ơn bạn đã đăng ký thông tin nhận thẻ VIP của nhà hàng Khoa Ngan! Khoan Ngan sẽ liên lạc với bạn trong thời gian sớm nhất.\');\r\n				window.location = \'{/literal}{root_url}{literal}\';\r\n			});\r\n		}\r\n	});\r\n});\r\n</script>\r\n<script src=\"http://www.eyecon.ro/bootstrap-datepicker/js/jquery.js\"></script>\r\n    <script src=\"http://www.eyecon.ro/bootstrap-datepicker/js/bootstrap-datepicker.js\"></script>\r\n{/literal}\r\n{content}\r\n  <div class=\"about\">\r\n    <p class=\"about-author\">\r\n      &copy; {get_copyright start=\"2015\"} <a href=\"http://www.khoanganxuavanay.com\" target=\"_blank\">Khoa Ngan Xưa và Nay</a> - Khoa Ngan Restaurant<br>\r\n      Webmaster by Lee Peace\r\n    </p>\r\n  </div>\r\n</body>\r\n</html>', '', '', 1, 0, '2015-09-12 00:56:16', '2019-04-24 16:02:24');
INSERT INTO `cms_templates` VALUES (27, 'Blank', '{content}', '', '', 1, 0, '2015-09-12 01:04:50', '2015-09-12 01:04:50');
INSERT INTO `cms_templates` VALUES (28, 'Home_bk', '{strip}\r\n{capture}{content default=\'\' wysiwyg=\'false\'}{/capture}\r\n{get_root_page_alias assign=\'page_lang\'}\r\n{/strip}\r\n{if $page_lang == \"en\"}\r\n	{assign var=\"cur_lang\" value=\"\"}\r\n{else}\r\n	{assign var=\"cur_lang\" value=\"-{$page_lang}\"}\r\n{/if}\r\n<!DOCTYPE html>\r\n<html lang=\"{$page_lang}\">\r\n<head>\r\n{metadata}\r\n<script type=\'text/javascript\' src=\'http://code.jquery.com/jquery-1.9.1.js\'></script>\r\n{cms_stylesheet}\r\n<script type=\'text/javascript\' src=\"http://getbootstrap.com/dist/js/bootstrap.js\"></script>\r\n<style>\r\n.modal-backdrop {\r\n    z-index: -1;\r\n    background-color: #000;\r\n}\r\n\r\n.modal-backdrop.in {\r\n    opacity: .95;\r\n    filter: alpha(opacity=95);\r\n}\r\n\r\n.modal {\r\n    z-index: 88888;\r\n}\r\n\r\n.modal-dialog {\r\n    z-index: 99999;\r\n}\r\n\r\n.modal-content {\r\n    width: auto;\r\n    background-color: #f9fafb;\r\n    border: 2px solid #ebedef;\r\n    border-radius: 7px;\r\n    -webkit-background-clip: border-box;\r\n    -moz-background-clip: border-box;\r\n    background-clip: border-box;\r\n    -webkit-box-shadow: -14px 14px 0 0 rgba(0,0,0,0.35);\r\n    box-shadow: -14px 14px 0 0 rgba(0,0,0,0.35);\r\ntop:7%;\r\nmargin:20px;\r\n}\r\n\r\n@media (max-width: 767px) {\r\n    .modal-content {\r\n        width: auto;\r\n    }\r\n}\r\n\r\n@media (max-width: 480px) {\r\n    .modal-content {\r\n        -webkit-box-shadow: none;\r\n        box-shadow: none;\r\n    }\r\n}\r\n@media only screen and (min-width : 1824px) {\r\n\r\n}\r\n@media (min-width: 768px) and (max-width: 991px) {\r\n\r\n}\r\n@media (max-width:767px) {  \r\n\r\n}\r\n@media (max-width:640px){\r\n\r\n}\r\n@media (max-width:620px){\r\n   \r\n}\r\n@media (max-width:520px){\r\n   \r\n}\r\n@media (max-width:480px) {\r\n    \r\n}\r\n.modal-header {\r\n    padding: 17px 19px 15px 24px;\r\n    border-bottom: 1px solid #ebedef;\r\n}\r\n\r\n.modal-header .close {\r\n    margin: 5px 0 0;\r\n    padding: 0;\r\n    font-size: 18px;\r\n    line-height: 1;\r\n    color: #34495e;\r\n}\r\n\r\n.modal-title {\r\n    margin: 0;\r\n    font-size: 24px;\r\n    line-height: 30px;\r\n}\r\n\r\n.modal-body {\r\n    padding: 10px 10px;\r\n}\r\n\r\n.modal-body p {\r\n    font-size: 16px;\r\n    line-height: 1.625;\r\n}\r\n\r\n.modal-footer {\r\n    padding: 19px 22px 20px;\r\n    margin-top: 0;\r\n    background-color: #ebedef;\r\n    border-top: none;\r\n    border-radius: 0 0 7px 7px;\r\n}\r\n\r\n.modal-footer .btn + .btn {\r\n    margin-left: 12px;\r\n}\r\n\r\n@media (max-width: 480px) {\r\n    .modal-footer .btn {\r\n        display: block;\r\n        min-width: auto;\r\n        margin-bottom: 15px;\r\n    }\r\n\r\n    .modal-footer .btn:last-child {\r\n        margin-bottom: 0;\r\n    }\r\n\r\n    .modal-footer .btn + .btn {\r\n        margin-left: 0;\r\n    }\r\n}\r\n\r\n@media screen and (min-width: 768px) {\r\n    .modal-dialog {\r\n        left: 0%;\r\n        right: auto;\r\n        width: 624px;\r\n    }\r\n}\r\n</style>\r\n{literal}\r\n<script type=\"text/javascript\">\r\n$(document).ready(function(){\r\n  $(\'#myModal\').modal(\'show\')\r\n});\r\n</script>\r\n{/literal}\r\n</head>\r\n<body id=\"hungry-home\" class=\"home\">\r\n<div class=\"modal in\">visible on page load.</div>\r\n<div id=\"hungry-preloader-container\">\r\n	<div class=\"hungry-preloader\">\r\n		<span class=\"bubble-01\"></span>\r\n		<span class=\"bubble-02\"></span>\r\n		<span class=\"bubble-03\"></span>\r\n	</div>\r\n</div>\r\n\r\n<header id=\"single-page-header\">\r\n	<div class=\"site-navbar\">\r\n		<div class=\"grid-container\">\r\n			<div class=\"grid-20 tablet-grid-50 mobile-grid-50\">\r\n				<a href=\"{root_url}\">\r\n					<img class=\"site-logo\" src=\"images/assets/logo.png\" alt=\"{sitename}-Logo\" />\r\n				</a>\r\n			</div>\r\n\r\n			<div class=\"nav-container grid-80 tablet-grid-50 mobile-grid-50\">\r\n				<div class=\"mobile-nav\">\r\n					<i class=\"fa fa-bars\"></i>\r\n				</div>\r\n			\r\n				<nav class=\"main-navigation\" role=\"navigation\">\r\n					<div class=\"mobile-header hide-on-desktop\">\r\n						<h2>{sitename}</h2>\r\n						<div class=\"mobile-close\">\r\n							<i class=\"fa fa-times\"></i>\r\n						</div>\r\n					</div>\r\n					{menu template=\"main_menu\" childrenof=\"vi\"}\r\n				</nav>\r\n			</div>\r\n		</div>\r\n	</div>\r\n	\r\n	<div class=\"single-page-header-content\">\r\n		<!--Slider Images -->\r\n		<div class=\"cycle-slideshow\"\r\n			 data-cycle-swipe=\"true\"\r\n			 data-cycle-swipe-fx=\"fade\"\r\n			 data-cycle-fx=\"fade\"\r\n			 data-cycle-speed=\"1200\"\r\n			 data-cycle-timeout=\"16000\">\r\n			<!-- Images -->\r\n			{ListIt2Slider}\r\n			<!-- Prev/Next Buttons -->\r\n			<div class=\"cycle-prev\"><i class=\"fa fa-chevron-left\"></i></div>\r\n			<div class=\"cycle-next\"><i class=\"fa fa-chevron-right\"></i></div>\r\n		</div>\r\n		<!-- END Slider Images -->\r\n	\r\n		<!-- START Slider Texts -->\r\n		<div class=\"single-page-header-text\">\r\n			<!-- Pre-slogan -->\r\n			<div class=\"tilt-left\">\r\n				<h3 class=\"header-text-pre-slogan\">{sitename}<em>&hellip;</em></h3>\r\n			</div>	\r\n			<!-- Slogan Rotator -->\r\n			<div class=\"tlt\">\r\n			{ListIt2HeaderTexts}\r\n			</div>\r\n			<!-- Divider -->\r\n			<div class=\"header-text-divider\"></div>\r\n		</div>\r\n		<!-- END Slider Texts -->\r\n	</div>\r\n	\r\n	<!-- START Social Icons -->\r\n	<div class=\"single-page-social-icons\">\r\n		{ListIt2Social}\r\n	</div>\r\n	<!-- END Social Icons -->\r\n	\r\n</header>\r\n<!-- END Site Header -->\r\n\r\n<!-- START Main Content -->\r\n<main class=\"site-content\" role=\"main\">\r\n	\r\n	<!-- START Section - About Us -->\r\n	<section id=\"hungry-about-us\" class=\"section-container aboutthis\">\r\n		\r\n		<!-- START Section Heading -->\r\n		<div class=\"wow fadeInDown\" data-wow-duration=\"2s\" data-wow-offset=\"250\">\r\n			<header class=\"section-heading\">\r\n				<h2 class=\"section-heading-title\">About Us</h2>\r\n				<div class=\"section-heading-subtitle-container tilt-left\">\r\n					<h4 class=\"section-heading-subtitle\">Bạn biết gì về nhà hàng Khoa Ngan?</h4>\r\n				</div>\r\n			</header>\r\n		</div>\r\n		<!-- END Section Heading -->\r\n		\r\n		<div class=\"grid-container\">\r\n		{ListIt2AboutUs orderby=\"rand\" pagelimit=\"1\"}\r\n		</div>\r\n		\r\n	</section>\r\n	<!-- END Section - About Us -->\r\n	\r\n	<section id=\"hungry-testimonials\" class=\"section-container parallax\">\r\n		<div class=\"grid-container\">\r\n			<div class=\"wow fadeIn\" data-wow-duration=\"2s\" data-wow-offset=\"250\">\r\n				<h1 class=\"section-heading-alt-title\">{MleCMS name=\"snippet_customer-say\"}<span>&hellip;</span></h1>\r\n			</div>\r\n			{ListIt2Testimonials}\r\n		</div>\r\n	</section>\r\n	\r\n	<!-- START Section - Menu -->\r\n	<section id=\"hungry-menu\" class=\"section-container\">\r\n	\r\n		<!-- START Section Heading -->\r\n		<div class=\"wow fadeInDown\" data-wow-duration=\"2s\" data-wow-offset=\"250\">\r\n			<header class=\"section-heading\">\r\n				<h2 class=\"section-heading-title\">Our Menu</h2>\r\n				<div class=\"section-heading-subtitle-container tilt-left\">\r\n					<h4 class=\"section-heading-subtitle\">Tươi nhất, ngon nhất...</h4>\r\n				</div>\r\n			</header>\r\n		</div>\r\n		<!-- END Section Heading -->\r\n	\r\n		<div class=\"grid-container\">\r\n			{ListIt2Menu action=\"category\"}\r\n		</div>\r\n	\r\n	</section>\r\n	<!-- END Section - Menu -->\r\n	\r\n	<!-- START Section - Slogan 01 -->\r\n	<section id=\"hungry-slogan-01\" class=\"section-container parallax\">\r\n		<div class=\"grid-container\">\r\n		\r\n			<div class=\"grid-100 tablet-grid-100 mobile-grid-100 no-margin\">\r\n				<div class=\"wow zoomIn\" data-wow-duration=\"2s\" data-wow-offset=\"250\">\r\n					<div class=\"hungry-slogan\">\r\n						<h2 class=\"hungry-slogan-text\">	“Ngan Vịt của nhà hàng Khoa Ngan cũng có đầy đủ <em>hộ chiếu, chứng minh thư</em> như ai”<br />\r\n						– như lời bà chủ vui tính của nhà hàng đã từng nói!</h2>\r\n					</div>\r\n				</div>\r\n			</div>\r\n		\r\n		</div>\r\n	</section>\r\n	<!-- END Section - Slogan 01 -->\r\n	\r\n	<!-- START Section - Staff -->\r\n	<!--section id=\"hungry-staff\" class=\"section-container\">\r\n		<div class=\"wow fadeInDown\" data-wow-duration=\"2s\" data-wow-offset=\"250\">\r\n			<header class=\"section-heading\">\r\n				<h2 class=\"section-heading-title\">Our Staff</h2>\r\n				<div class=\"section-heading-subtitle-container tilt-left\">\r\n					<h4 class=\"section-heading-subtitle\">The Friendliest People</h4>\r\n				</div>\r\n			</header>\r\n		</div>\r\n		<div class=\"grid-container\">\r\n		ListIt2Staff\r\n		</div>\r\n	</section-->\r\n	<!-- END Section - Staff -->\r\n	\r\n	\r\n	\r\n	<!-- START Section - Gallery -->\r\n	<section id=\"hungry-gallery\" class=\"section-container\">\r\n	\r\n		<!-- START Section Heading -->\r\n		<div class=\"wow fadeInDown\" data-wow-duration=\"2s\" data-wow-offset=\"250\">\r\n			<header class=\"section-heading\">\r\n				<h2 class=\"section-heading-title\">The Gallery</h2>\r\n				<div class=\"section-heading-subtitle-container tilt-left\">\r\n					<h4 class=\"section-heading-subtitle\">{sitename} 2015!</h4>\r\n				</div>\r\n			</header>\r\n		</div>\r\n		<!-- END Section Heading -->\r\n	\r\n		<!-- START Main Gallery -->\r\n		<div class=\"grid-container\">\r\n			<div class=\"grid-100 tablet-grid-100 mobile-grid-100\">\r\n				<div class=\"hungry-gallery\">\r\n				{ListIt2Gallery}\r\n				</div>\r\n			</div>\r\n		</div>\r\n		<!-- END Main Gallery -->\r\n		\r\n	</section>\r\n	<!-- END Section - Gallery -->\r\n	\r\n	<!-- START Section - Slogan 02 -->\r\n	<a href=\"#\" class=\"clickable-slogan\">\r\n		<section id=\"hungry-slogan-02\" class=\"section-container parallax\">\r\n			<div class=\"grid-container\">\r\n			\r\n				<div class=\"grid-100 tablet-grid-100 mobile-grid-100 no-margin\">\r\n					<div class=\"wow zoomIn\" data-wow-duration=\"2s\" data-wow-offset=\"250\">\r\n						<div class=\"hungry-slogan\">\r\n							<h2 class=\"hungry-slogan-text\">Hãy tới và thưởng thức, Khoa Ngan <em>{global_content name=\'add\'}</em><br />\r\n							<span>Hotline: <em>{global_content name=\'hotline\'}</em></span></h2>\r\n						</div>\r\n					</div>\r\n				</div>\r\n				\r\n			</div>\r\n		</section>\r\n	</a>\r\n	<!-- END Section - Slogan 02 -->\r\n	\r\n	<!-- START Section - Blog -->\r\n	<section id=\"hungry-promotion\" class=\"section-container parallax\">\r\n	\r\n		<!-- START Section Heading -->\r\n		<div class=\"wow fadeInDown\" data-wow-duration=\"2s\" data-wow-offset=\"250\">\r\n			<header class=\"section-heading\">\r\n				<h2 class=\"section-heading-title\">Khoa Ngan Promotion</h2>\r\n				<div class=\"section-heading-subtitle-container tilt-left\">\r\n					<h4 class=\"section-heading-subtitle\">Bắt đầu từ 01/05/2015!</h4>\r\n				</div>\r\n			</header>\r\n		</div>\r\n		<!-- END Section Heading -->\r\n			\r\n		<div class=\"grid-container\">\r\n			<div class=\"hungry-blog-container\">\r\n				{ListIt2Blog}\r\n				<br class=\"clear\" />\r\n			</div>\r\n			\r\n			<!-- \"View Blog\" Button -->\r\n			<div class=\"wow fadeInUp\" data-wow-duration=\"2s\" data-wow-offset=\"250\">\r\n				<div class=\"grid-100 tablet-grid-100 mobile-grid-100 aligncenter\">\r\n					<a class=\"hungry-button dark aligncenter\" href=\"#\">View the Blog</a>\r\n				</div>\r\n			</div>\r\n		\r\n		</div>\r\n		\r\n	</section>\r\n	<!-- END Section - Blog -->\r\n	\r\n	<!-- START Section - Reservations -->\r\n	<!--section id=\"hungry-reservations\" class=\"section-container\">\r\n	\r\n\r\n		<div class=\"wow fadeInDown\" data-wow-duration=\"2s\" data-wow-offset=\"250\">\r\n			<header class=\"section-heading\">\r\n				<h2 class=\"section-heading-title\">Reservations</h2>\r\n				<div class=\"section-heading-subtitle-container tilt-left\">\r\n					<h4 class=\"section-heading-subtitle\">Book Your Meal Today!</h4>\r\n				</div>\r\n			</header>\r\n		</div>\r\n\r\n	\r\n		<div class=\"grid-container\">\r\n			<div class=\"prefix-10 grid-80 suffix-10 tablet-grid-100 mobile-grid-100\">\r\n			\r\n\r\n				<form id=\"hungry-reservation-form\" action=\"php/process-reservation.php\" method=\"post\">\r\n					<fieldset>\r\n						\r\n						<legend class=\"form-title\">Booking Form<span>Please fill out all required<em>*</em> fields. Thanks!</span></legend>\r\n						\r\n						<div class=\"grid-50 tablet-grid-50 mobile-grid-100\">\r\n						\r\n		\r\n							<p>\r\n								<label for=\"res_name\">Name of your Party<span>*</span></label>\r\n								<input type=\"text\" name=\"res_name\" id=\"res_name\" value=\"\" required>\r\n							</p>\r\n							\r\n\r\n							<p>\r\n								<label for=\"res_email\">Your Email Address<span>*</span></label>\r\n								<input type=\"email\" name=\"res_email\" id=\"res_email\" value=\"\" required>\r\n							</p>\r\n							\r\n			\r\n							<p>\r\n								<label for=\"res_phone\">Your Contact Number<span>*</span></label>\r\n								<input type=\"tel\" name=\"res_phone\" id=\"res_phone\" value=\"\" required>\r\n							</p>\r\n							\r\n	\r\n							<p>\r\n								<label for=\"res_amount\">How many people in your Party?<span>*</span></label>\r\n								<select name=\"res_amount\" id=\"res_amount\" required>\r\n									<option value=\"\" selected>Please Choose...</option>\r\n									<option value=\"1\">1</option>\r\n									<option value=\"2\">2</option>\r\n									<option value=\"3\">3</option>\r\n									<option value=\"4\">4</option>\r\n									<option value=\"5\">5</option>\r\n									<option value=\"6\">6</option>\r\n									<option value=\"7\">7</option>\r\n									<option value=\"8\">8</option>\r\n									<option value=\"9\">9</option>\r\n									<option value=\"More than 9\">More than 9</option>\r\n								</select>\r\n							</p>\r\n						</div>\r\n						\r\n						<div class=\"grid-50 tablet-grid-50 mobile-grid-100\">\r\n						\r\n		\r\n							<p>\r\n								<label for=\"res_date\">Date of Booking<span>*</span></label>\r\n								<input type=\"date\" name=\"res_date\" id=\"res_date\" value=\"\" required>\r\n							</p>\r\n							\r\n			\r\n							<p>\r\n								<label for=\"res_time\">Time of Booking<span>*</span></label>\r\n								<select name=\"res_time\" id=\"res_time\" required>\r\n									<option value=\"\" selected>Please Choose...</option>\r\n									<optgroup label=\"Afternoon Reservations\">\r\n										<option value=\"12pm\">12pm</option>\r\n										<option value=\"12:30pm\">12:30pm</option>\r\n										<option value=\"1pm\">1pm</option>\r\n										<option value=\"1:30pm\">1:30pm</option>\r\n									</optgroup>\r\n									<optgroup label=\"Evening Reservations\">\r\n										<option value=\"6pm\">6pm</option>\r\n										<option value=\"6:30pm\">6:30pm</option>\r\n										<option value=\"7pm\">7pm</option>\r\n										<option value=\"7:30pm\">7:30pm</option>\r\n										<option value=\"8pm\">8pm</option>\r\n										<option value=\"8:30pm\">8:30pm</option>\r\n										<option value=\"9pm\">9pm</option>\r\n									</optgroup>\r\n								</select>\r\n							</p>\r\n\r\n							<label for=\"res_email\">Optional Message</label>\r\n							<textarea name=\"res_message\" id=\"res_message\" cols=\"8\" rows=\"8\"></textarea>\r\n							\r\n						</div>\r\n				\r\n						<div class=\"grid-100 tablet-grid-100 mobile-grid-100\">\r\n						\r\n\r\n							<input type=\"submit\" name=\"res-submit\" id=\"res-submit\" value=\"Find me a Table!\" />\r\n						</div>\r\n				\r\n					</fieldset>\r\n				</form>\r\n\r\n\r\n				<div id=\"hungry-reservation-form-outcome\"></div>\r\n			\r\n			</div>\r\n		</div>\r\n		\r\n	</section>\r\n	<!-- END Section - Reservations -->\r\n	\r\n</main>\r\n<!-- END Main Content -->\r\n<section id=\"hungry-contact\">\r\n		<!-- START Section Heading -->\r\n		<div class=\"wow fadeInDown\" data-wow-duration=\"2s\" data-wow-offset=\"250\">\r\n			<header class=\"section-heading\">\r\n				<h2 class=\"section-heading-title\">Contact Us</h2>\r\n				<div class=\"section-heading-subtitle-container tilt-left\">\r\n					<h4 class=\"section-heading-subtitle\">Khoa Ngan - 77 Hai Bà Trưng</h4>\r\n				</div>\r\n			</header>\r\n		</div>\r\n		<!-- END Section Heading -->\r\n<!-- START Site Footer -->\r\n<footer id=\"site-footer\">\r\n\r\n	<!-- START Widget Area -->\r\n	<div class=\"widget-area grid-container\">\r\n	\r\n		<!-- START Widget Column 01 -->\r\n		<div class=\"widget-column widget-column-01 grid-33 tablet-grid-33 mobile-grid-100\">\r\n			\r\n			<!-- Contact Details Widget -->\r\n			<aside class=\"widget widget-hungry-contact-details\">\r\n				<h3 class=\"widget-title\">Contact Us</h3>\r\n				<p>(Liên hệ)</p>\r\n				<div class=\"contact-details\">\r\n					<div class=\"contact-phone\">\r\n						<i class=\"fa fa-phone-square\"></i>\r\n						<a class=\"phone-number-link\" href=\"#\">{global_content name=\'tel\'}</a>\r\n					</div>\r\n					<div class=\"contact-email\">\r\n						<i class=\"fa fa-envelope-square\"></i>\r\n						<a class=\"email-link\" href=\"mailto:{global_content name=\'email\'}\">{global_content name=\'email\'}</a>\r\n					</div>\r\n					<div class=\"contact-address\">\r\n						<i class=\"fa fa-caret-square-o-down\"></i>\r\n						<address>{global_content name=\'add\'}\r\n						</address>\r\n					</div>\r\n				</div>\r\n			</aside>\r\n\r\n		</div>\r\n		<!-- END Widget Column 01 -->\r\n		\r\n		<!-- START Widget Column 02 -->\r\n		<div class=\"widget-column widget-column-02 grid-33 tablet-grid-33 mobile-grid-100\">\r\n			<aside class=\"widget widget-hungry-latest-recipes\">\r\n				<h3 class=\"widget-title\">Facebook</h3>\r\n				<p>(Liên kết mạng xã hội)</p>\r\n<iframe src=\"http://www.facebook.com/plugins/likebox.php?href=https://www.facebook.com/khoanganxuanay?ref=hl&amp;width=300&amp;colorscheme=light&amp;show_faces=true&amp;connections=12&amp;stream=false&amp;header=false&amp;height=100\" scrolling=\"no\" frameborder=\"0\" scrolling=\"no\" style=\"overflow: hidden; height: 100px; width: 300px;background:#fff;\"></iframe>\r\n				<ul class=\"latest-recipes\">\r\n					<!--li class=\"recipe\">\r\n						<a href=\"#\"><img class=\"recipe-thumbnail\" src=\"images/demo/widget-recipe-thumbnails/thumbnail-01.jpg\" alt=\"Recipe Thumbnail Image\" /></a>\r\n						<h6 class=\"recipe-title\"><a href=\"#\">Angus Steak Burger</a></h6>\r\n						<p class=\"recipe-description\">Aenean commodo ligula eget.</p>\r\n					</li-->\r\n				</ul>\r\n			</aside>\r\n		</div>\r\n		<div class=\"widget-column widget-column-03 grid-33 tablet-grid-33 mobile-grid-100\">\r\n			<aside class=\"widget widget-hungry-opening-times\">\r\n				<h3 class=\"widget-title\">Opening Times</h3>\r\n				<p>(Giờ mở cửa)</p>\r\n				<ul class=\"opening-times\">\r\n					<li>Phục vụ tất cả các ngày trong tuần<span>7am - 11pm</span></li>\r\n				</ul>\r\n			</aside>\r\n		</div>\r\n	</div>\r\n	<div id=\"bottom-footer\">\r\n		<div class=\"grid-container\">\r\n			<!-- Footer Logo -->\r\n			<div class=\"footer-logo grid-50 tablet-grid-100 mobile-grid-100\">\r\n				<a href=\"{root_url}\"><img class=\"footer-logo-image\" src=\"images/assets/footer-logo.png\" alt=\"{sitename}\" /></a>\r\n			</div>\r\n		</div>\r\n	</div>\r\n</footer>\r\n</section>\r\n<div id=\"btt\">\r\n	<i class=\"fa fa-angle-up\"></i>\r\n</div>\r\n<div class=\"modal\" id=\"myModal\" tabindex=\"-1\" style=\"z-index:99999\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">\r\n  <div class=\"modal-dialog\">\r\n    <div class=\"modal-content\">\r\n      <div class=\"modal-body\">\r\n        <a href=\"{cms_selflink href=vip}\"><img src=\"event/vip.jpg\" alt=\"VIP\" style=\"width:100%;\"/></a>\r\n      </div>\r\n    </div><!-- /.modal-content -->\r\n  </div><!-- /.modal-dialog -->\r\n</div><!-- /.modal -->\r\n<script type=\"text/javascript\" src=\"js/jquery-main.js\"></script>\r\n<script type=\"text/javascript\" src=\"js/jquery-custom.js\"></script>\r\n{literal}\r\n<!-- Facebook Conversion Code for Khách hàng retargetting -->\r\n<script>(function() {\r\nvar _fbq = window._fbq || (window._fbq = []);\r\nif (!_fbq.loaded) {\r\nvar fbds = document.createElement(\'script\');\r\nfbds.async = true;\r\nfbds.src = \'//connect.facebook.net/en_US/fbds.js\';\r\nvar s = document.getElementsByTagName(\'script\')[0];\r\ns.parentNode.insertBefore(fbds, s);\r\n_fbq.loaded = true;\r\n}\r\n})();\r\nwindow._fbq = window._fbq || [];\r\nwindow._fbq.push([\'track\', \'6032962319202\', {\'value\':\'0.00\',\'currency\':\'VND\'}]);\r\n</script>\r\n<noscript><img height=\"1\" width=\"1\" alt=\"\" style=\"display:none\" src=\"https://www.facebook.com/tr?ev=6032962319202&amp;cd[value]=0.00&amp;cd[currency]=VND&amp;noscript=1\" /></noscript>{/literal}\r\n</body>\r\n</html>', '', '', 1, 0, '2015-09-17 22:56:18', '2020-11-24 16:05:52');
INSERT INTO `cms_templates` VALUES (29, 'Event-2-check', '<!DOCTYPE html>\r\n<html lang=\"en\">\r\n<head>\r\n  <meta charset=\"utf-8\">\r\n  <meta name=\"viewport\" content=\"width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1\">\r\n  <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge,chrome=1\">\r\n  <title>Đăng ký thành viên VIP - Khoa Ngan Xưa và Nay</title>\r\n  <script type=\"text/javascript\" src=\"js/jquery.js\"></script>\r\n  <script type=\"text/javascript\" src=\"js/frmValid.js\"></script>\r\n{cms_stylesheet}\r\n</head>\r\n<body>\r\n<form name=\"frm\" id=\"frm\" method=\"get\" class=\"sign-up\" autocomplete=\"off\">\r\n<h1 class=\"sign-up-title\">VIP MEMBER <br/> <span>www.khoanganxuavanay.com</span></h1>\r\n<select class=\"sign-up-input\" id=\"sex\">\r\n<option value=\"-1\" disabled selected>Giới tính</option>\r\n<option value=\"Mr.\">Nam</option>\r\n<option value=\"Ms.\">Nữ</option>\r\n</select>\r\n<input type=\"text\" id=\"birthday\" onkeypress=\"return isNumberKey(event)\" name=\"birthday\" class=\"sign-up-input\" placeholder=\"Năm sinh\" maxlength=\"4\"  style=\"width:120px\">\r\n    <input type=\"text\" id=\"fullname\" class=\"sign-up-input\" placeholder=\"Họ và tên\" autofocus>\r\n    <input type=\"text\" id=\"phone\" name=\"phone\" onkeypress=\"return isNumberKey(event)\" class=\"sign-up-input\" placeholder=\"Điện thoại\">\r\n<input type=\"text\" id=\"address\" name=\"address\" class=\"sign-up-input\" placeholder=\"Địa chỉ\">\r\n    <input type=\"text\" name=\"math\" id=\"math\" class=\"sign-up-input\" maxlength=\"2\" placeholder=\"77\" style=\"width:55px;\"><label for=\"math\"> Vui lòng nhập mã xác nhận.</label>\r\n    <input type=\"button\" value=\"Đăng ký\" class=\"sign-up-button\">\r\n  </form>\r\n{literal}\r\n<script>\r\nfunction isNumberKey(evt)\r\n    {\r\n       var charCode = (evt.which) ? evt.which : event.keyCode;\r\n       if(charCode == 59 || charCode == 46)\r\n        return true;\r\n       if (charCode > 31 && (charCode < 48 || charCode > 57))\r\n          return false;\r\n       return true;\r\n    }\r\n	$().ready(function() {\r\n		$(\'.sign-up-button\').click(function(e) {\r\n		filter_array = new Array(\r\n			\'fullname:Bạn vui lòng nhập họ và tên của bạn.\',\r\n			\'phone:Bạn vui lòng cho biết số điện thoại.\',\r\n			\'math:Bạn nhập vào số 77 để hoàn tất đăng ký.\'\r\n		);\r\n		valid = new Array(\r\n			\'required\',\r\n			\'number\',\r\n			\'key\'\r\n		);\r\n		resultOrder = formValid(\'frm\', filter_array, valid, \'alertbox\');\r\n		if (resultOrder) {\r\n			var url = \"{/literal}{cms_selflink href=\'event-action\'}{literal}?fullname=\"+$(\'#fullname\').val()+\"&phone=\"+$(\'#phone\').val()+\"&sex=\"+$(\'#sex\').val()+\"&address=\"+$(\'#address\').val()+\"&birthday=\"+$(\'#birthday\').val();\r\n			$.get(url, $(\"#frm\").serialize(), function(resp){\r\nconsole.log(resp);\r\n			$(\"input[type=text]\").val(\"\");\r\nalert(\'Cảm ơn bạn đã đăng ký thông tin nhận thẻ VIP của nhà hàng Khoa Ngan! Chúng tôi sẽ liên lạc với bạn trong thời gian sớm nhất.\');\r\n				window.location = \'{/literal}{root_url}{literal}\';\r\n			});\r\n		}\r\n	});\r\n});\r\n</script>\r\n<script src=\"http://www.eyecon.ro/bootstrap-datepicker/js/jquery.js\"></script>\r\n    <script src=\"http://www.eyecon.ro/bootstrap-datepicker/js/bootstrap-datepicker.js\"></script>\r\n{/literal}\r\n{content}\r\n  <div class=\"about\">\r\n    <p class=\"about-author\">\r\n      &copy; 2015 <a href=\"http://www.khoanganxuavanay.com\" target=\"_blank\">Khoa Ngan Xưa và Nay</a> - Khoa Ngan Restaurant<br>\r\n      Webmaster by <a href=\"http://www.songviytuong.com\" target=\"_blank\">Lee Peace</a>\r\n    </p>\r\n  </div>\r\n</body>\r\n</html>', '', '', 1, 0, '2015-09-18 14:06:08', '2015-09-18 14:06:08');

-- ----------------------------
-- Table structure for cms_templates_seq
-- ----------------------------
DROP TABLE IF EXISTS `cms_templates_seq`;
CREATE TABLE `cms_templates_seq`  (
  `id` int(11) NOT NULL
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = FIXED;

-- ----------------------------
-- Records of cms_templates_seq
-- ----------------------------
INSERT INTO `cms_templates_seq` VALUES (29);

-- ----------------------------
-- Table structure for cms_user_groups
-- ----------------------------
DROP TABLE IF EXISTS `cms_user_groups`;
CREATE TABLE `cms_user_groups`  (
  `group_id` int(11) NULL DEFAULT NULL,
  `user_id` int(11) NULL DEFAULT NULL,
  `create_date` datetime NULL DEFAULT NULL,
  `modified_date` datetime NULL DEFAULT NULL
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = FIXED;

-- ----------------------------
-- Records of cms_user_groups
-- ----------------------------
INSERT INTO `cms_user_groups` VALUES (1, 1, '2006-07-25 21:22:33', '2006-07-25 21:22:33');
INSERT INTO `cms_user_groups` VALUES (2, 2, NULL, NULL);

-- ----------------------------
-- Table structure for cms_userplugins
-- ----------------------------
DROP TABLE IF EXISTS `cms_userplugins`;
CREATE TABLE `cms_userplugins`  (
  `userplugin_id` int(11) NOT NULL,
  `userplugin_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `code` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `description` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `create_date` datetime NULL DEFAULT NULL,
  `modified_date` datetime NULL DEFAULT NULL
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of cms_userplugins
-- ----------------------------
INSERT INTO `cms_userplugins` VALUES (1, 'get_user_agent', '//Code to show the user\'s user agent information.\r\necho $_SERVER[\"HTTP_USER_AGENT\"];', 'Code to show the users user agent information', '2006-07-25 21:22:33', '2015-04-10 19:15:16');
INSERT INTO `cms_userplugins` VALUES (2, 'get_copyright', '//set start to date your site was published\r\n$startCopyRight=\'2015\';\r\n\r\n// check if start year is this year\r\nif(date(\'Y\') == $startCopyRight){\r\n// it was, just print this year\r\n    echo $startCopyRight;\r\n}else{\r\n// it wasnt, print startyear and this year delimited with a dash\r\n    echo $startCopyRight.\'-\'. date(\'Y\');\r\n}', 'Code to output copyright information', '2006-07-25 21:22:33', '2015-04-10 19:15:08');
INSERT INTO `cms_userplugins` VALUES (3, 'get_home_id', '$hm = cmsms()->getHierarchyManager();\r\n$position = substr($smarty->get_template_vars(\'position\'), 0, 1) . \'.1\';\r\n$node = $hm->getNodeByHierarchy($position);\r\n$id = $node->getId();\r\nif($params[\'assign\'])\r\n{\r\n    $smarty->assign($params[\'assign\'], $id);\r\n}\r\nelse\r\n{\r\n    echo $id;\r\n}', '', '2015-04-10 19:16:26', '2015-04-10 19:16:26');
INSERT INTO `cms_userplugins` VALUES (4, 'get_root_page_alias', '$manager = cmsms()->GetHierarchyManager();\r\n$var = isset($params[\'assign\']) ? $params[\'assign\'] : \'root_page_alias\';\r\n$result = \"NO RESULT\";\r\n$thisPage = $smarty->get_template_vars(\'content_id\');\r\n$currentNode = $manager->sureGetNodeById($thisPage);\r\nwhile( isset($currentNode) && $currentNode->getLevel() >= 0 )\r\n{\r\n    $currentContent =& $currentNode->getContent();\r\n    $result = $currentContent->Alias();\r\n    $currentNode =& $currentNode->getParentNode();\r\n}\r\n$smarty->assign($var,$result);\r\n', '', '2015-04-10 19:16:39', '2015-04-10 19:16:39');
INSERT INTO `cms_userplugins` VALUES (5, 'get_tiny_url', '$ch = curl_init();\r\ncurl_setopt($ch, CURLOPT_URL, \"http://tinyurl.com/api-create.php?url=\".$params[\'surl\']);\r\ncurl_setopt($ch, CURLOPT_RETURNTRANSFER,1);\r\ncurl_setopt($ch, CURLOPT_HEADER, 0);\r\n$html = curl_exec ($ch);\r\ncurl_close ($ch);\r\nreturn $html;', '', '2015-04-10 19:16:55', '2015-04-10 19:16:55');
INSERT INTO `cms_userplugins` VALUES (6, 'get_product_count', '$gCms = cmsms();\r\n$config = $gCms->GetConfig();\r\n$db =& $gCms->GetDb();\r\n$qCount = \"select count(*) as cnt from cms_module_products\";\r\n$r = $db->Execute($qCount);\r\n$rowCount = $r->FetchRow();\r\necho $rowCount[\'cnt\'];', '', '2015-04-10 19:17:09', '2015-04-10 19:17:09');
INSERT INTO `cms_userplugins` VALUES (7, 'get_time_elapsed', '$time_ago = $params[\'ptime\'];\r\n$cur_time 	= time();\r\n$time_elapsed 	= $cur_time - strtotime($time_ago);\r\n$seconds 	= $time_elapsed ;\r\n	$minutes 	= round($time_elapsed / 60  );\r\n	$hours 		= round($time_elapsed / 3600 );\r\n	$days 		= round($time_elapsed / 86400 );\r\n	$weeks 		= round($time_elapsed / 604800 );\r\n	$months 	= round($time_elapsed / 2600640 );\r\n	$years 		= round($time_elapsed / 31207680 );\r\n	// Seconds\r\n	if($seconds <= 60){\r\n		return \"$seconds seconds ago\";\r\n	}\r\n	//Minutes\r\n	else if($minutes <=60){\r\n		if($minutes==1){\r\n			return \"one minute ago\";\r\n		}\r\n		else{\r\n			return \"$minutes minutes ago\";\r\n		}\r\n	}\r\n	//Hours\r\n	else if($hours <=24){\r\n		if($hours==1){\r\n			return \"an hour ago\";\r\n		}else{\r\n			return \"$hours hours ago\";\r\n		}\r\n	}\r\n	//Days\r\n	else if($days <= 7){\r\n		if($days==1){\r\n			return \"yesterday\";\r\n		}else{\r\n			return \"$days days ago\";\r\n		}\r\n	}\r\n	//Weeks\r\n	else if($weeks <= 4.3){\r\n		if($weeks==1){\r\n			return \"a week ago\";\r\n		}else{\r\n			return \"$weeks weeks ago\";\r\n		}\r\n	}\r\n	//Months\r\n	else if($months <=12){\r\n		if($months==1){\r\n			return \"a month ago\";\r\n		}else{\r\n			return \"$months months ago\";\r\n		}\r\n	}\r\n	//Years\r\n	else{\r\n		if($years==1){\r\n			return \"one year ago\";\r\n		}else{\r\n			return \"$years years ago\";\r\n		}\r\n	}', '', '2015-04-10 19:17:26', '2015-04-10 19:17:26');
INSERT INTO `cms_userplugins` VALUES (8, 'get_activehome', '$gCms = cmsms();\r\n$config = $gCms->GetConfig();\r\n// Get Languages Created By Lee\r\n$db =& $gCms->GetDb();\r\n$query = \"SELECT * FROM cms_content where activehome=1\";\r\n$results = $db->Execute($query);\r\n$result = \"\";\r\nwhile ($results && $row = $results->FetchRow())\r\n		{\r\n$result .= $row[\"content_alias\"];\r\n$result .= \",\";\r\n}\r\necho substr($result,0,(strrpos($result,\",\"))).\"\";', '', '2015-04-11 19:06:01', '2015-04-11 19:06:01');
INSERT INTO `cms_userplugins` VALUES (9, 'get_nextprv', '$total = $params[\'total\'];\r\nif ($total > 1){\r\n$j = $params[\'curr\'];\r\n$next = $j + 1;\r\n$prev = $j - 1;\r\n$last=$total;\r\n\r\n$begin = $j - 1;\r\n$end = $j + 1;\r\n\r\nif($begin <= 0)\r\n{\r\n	$begin = 1;\r\n}\r\nif($end >= $total)\r\n{\r\n	$end = $total;\r\n}\r\necho\'<div style=\"text-align:center;cursor:pointer;\"><nav align=\"center\" style=\"display: block; height: 50px;\"><ul class=\"pagination pagination-sm\" style=\"margin: 10px auto !important; float: none !important; text-align: center;\">\';\r\n// show first page\r\n	echo \'<li \'.(($j != 1)?\'onclick=\"loadevent(0)\':\'\').\'\"><a>&laquo;</a></li>\';\r\n	// show prev page\r\n	echo \'<li \'.(($j != 1 && $begin >= 1)?\'onclick=\"loadevent(\'.$prev.\')\':\'\').\'\"><a>&#8249;</a></li>\';\r\n// show number link\r\n	for($i=$begin;$i<=$end;$i++){\r\n		if($j != $i){\r\n			echo \'<li onclick=\"loadevent(\'.$i.\')\"><a>\'.$i.\'</a></li>\';\r\n		} else {\r\n			echo\'<li class=\"active\" onload=\"loadevent(\'.$i.\')\"><a>\'.$i.\'</a></li>\';\r\n		}\r\n	}\r\n// show next page\r\n	echo \'<li \'.(($j != $total && $end <= $total)?\'onclick=\"loadevent(\'.$next.\')\':\'\').\'\"><a>&#8250;</a></li>\';\r\n	// show last page\r\n	echo\'<li \'.(($j != $total)?\'onclick=\"loadevent(\'.$last.\')\':\'\').\'\"><a>&raquo;</a></li>\';\r\necho\'</ul></nav></div>\';\r\n}', '', '2015-04-11 19:06:26', '2015-04-11 19:06:26');
INSERT INTO `cms_userplugins` VALUES (10, 'get_hierarchy_from_proid', '/*$params[\'productid\']*/\r\n/*$params[\'assign\']*/\r\n/* returns array of hierarchy or empty string */\r\n\r\nif (!empty($params[\'productid\']) &&\r\n   !empty($params[\'assign\'])){\r\n   $smarty = cmsms()->GetSmarty();\r\n   $db = cmsms()->GetDB();\r\n   $hierarchy = \'\';\r\n\r\n   $Products = cmsms()->modules[\'Products\'][\'object\'];\r\n   if (!empty($Products)){\r\n      $query = \'SELECT hierarchy_id FROM \'.cms_db_prefix().\'module_products_prodtohier WHERE product_id = ?\';\r\n      $hierarchy_id = $db->GetOne($query, array(intval($params[\'productid\'])));\r\n      $hierarchy = $Products->GetHierarchyInfo(intval($hierarchy_id));\r\n   }\r\n   $smarty->assign($params[\'assign\'], $hierarchy);\r\n}\r\n', '', '2015-04-11 19:06:39', '2015-04-11 19:06:39');
INSERT INTO `cms_userplugins` VALUES (11, 'get_dynamic_size', '// Settings\r\n$small_size = 109;\r\n$large_size = 370;\r\n$path = $params[\'path\'];\r\n$file = $params[\'file\'];\r\n// Get Params\r\n$image = $path . \'/\' . $file;\r\n// If the small file already exists, we\'re done with the heavy processing.\r\nif (!file_exists($small_image)) {\r\n  list($width, $height) = getimagesize($image);\r\n// Get new sizes\r\n  $img = imagecreatefromjpeg($image);\r\n  $imgratio=$width/$height;\r\n  if ($imgratio > 1) {\r\n    $small_width = $small_size;\r\n    $small_height = $small_size / $imgratio;\r\n  } else {\r\n    $small_height = $small_size;\r\n    $small_width = $small_size * $imgratio;\r\n  }\r\n \r\n// Generate small image\r\n \r\n  $resized_img = imagecreatetruecolor($small_width, $small_height);\r\n  imagecopyresampled($resized_img, $img, 0, 0, 0, 0, $small_width, $small_height, $width, $height);\r\n  imagejpeg($resized_img, $path.\"/preview_109_\".$file,100);\r\n}\r\n\r\nif (!file_exists($large_image)) {\r\n  list($width, $height) = getimagesize($image);\r\n\r\n \r\n// Get new sizes\r\n  $img = imagecreatefromjpeg($image);\r\n  $imgratio=$width/$height;\r\n  if ($imgratio > 1) {\r\n    $large_width = $large_size;\r\n    $large_height = $large_size / $imgratio;\r\n  } else {\r\n    $large_height = $large_size;\r\n    $large_width = $large_size * $imgratio;\r\n  }\r\n \r\n// Generate large image\r\n \r\n  $resized_img = imagecreatetruecolor($large_width, $large_height);\r\n  imagecopyresampled($resized_img, $img, 0, 0, 0, 0, $large_width, $large_height, $width, $height);\r\n  imagejpeg($resized_img, $path.\"/preview_370_\".$file,100);\r\n}\r\n', '', '2015-04-11 19:06:52', '2015-04-11 19:06:52');
INSERT INTO `cms_userplugins` VALUES (12, 'get_page_alias_from_url', '/** Get page alias from url\r\n * @params string $params[\'url\']\r\n */\r\n$cntnt = cmsms()->GetContentOperations();\r\nforeach ($cntnt->GetAllContent() as $page) {\r\n    if ($page->GetURL() == $params[\'url\']) {\r\n        $return = $page->Alias();\r\n        break;\r\n    }\r\n}\r\nif(!empty($params[\'assign\'])){\r\n        $smarty->assign(trim($params[\'assign\']), $return);\r\n} else {\r\n        return $return;\r\n}', '', '2015-04-11 19:07:30', '2015-04-11 19:07:30');
INSERT INTO `cms_userplugins` VALUES (13, 'get_guest_ip', '$smarty = cmsms()->GetSmarty();\r\n$ip = stripslashes($_SERVER[\'REMOTE_ADDR\']);\r\n$smarty->assign(\'ip\', $ip);\r\n', '', '2015-04-11 19:08:41', '2015-04-11 19:08:41');
INSERT INTO `cms_userplugins` VALUES (14, 'get_currency', '//$amount = urlencode($amount);\r\n$from_Currency = VND;\r\n$to_Currency = USD;\r\n$amo = isset($params[\'amo\']) ? $params[\'amo\'] : \'\';\r\n$var = isset($params[\'assign\']) ? $params[\'assign\'] : \'\';\r\n$url = \"http://www.google.com/finance/converter?a=$amo&from=$from_Currency&to=$to_Currency\";\r\n$ch = curl_init();\r\n$timeout = 0;\r\ncurl_setopt ($ch, CURLOPT_URL, $url);\r\ncurl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);\r\ncurl_setopt ($ch, CURLOPT_USERAGENT, \"Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1)\");\r\ncurl_setopt ($ch, CURLOPT_CONNECTTIMEOUT, $timeout);\r\n$rawdata = curl_exec($ch);\r\ncurl_close($ch);\r\n$data = explode(\'bld>\', $rawdata);\r\n$data = explode($to_Currency, $data[1]);\r\n$result = round($data[0], 2);\r\n$smarty->assign($var,$result);', '', '2015-04-25 18:22:50', '2015-04-25 18:22:50');
INSERT INTO `cms_userplugins` VALUES (15, 'vipaction', '$gCms = cmsms();\r\n$hm = $gCms->GetHierarchyManager();\r\n$db = $gCms->GetDb();\r\n$fullname = isset($params[\'fullname\']) && !empty($params[\'fullname\'])?$params[\'fullname\']:\'\';\r\n$phone = isset($params[\'phone\']) && !empty($params[\'phone\'])?$params[\'phone\']:\'\';\r\n$sex = isset($params[\'sex\']) && !empty($params[\'sex\'])?$params[\'sex\']:\'\';\r\n$birthday = isset($params[\'birthday\']) && !empty($params[\'birthday\'])?$params[\'birthday\']:\'\';\r\n$address = isset($params[\'address\']) && !empty($params[\'address\'])?$params[\'address\']:\'\';\r\n$search = array(chr(0xe2) . chr(0x80) . chr(0x98),\r\n						  chr(0xe2) . chr(0x80) . chr(0x99),\r\n						  chr(0xe2) . chr(0x80) . chr(0x9c),\r\n						  chr(0xe2) . chr(0x80) . chr(0x9d),\r\n						  chr(0xe2) . chr(0x80) . chr(0x93),\r\n						  chr(0xe2) . chr(0x80) . chr(0x94));\r\n$name = str_replace($search, \"\", $fullname);\r\n// the second part uses the cms version\r\n$alias = munge_string_to_url($name, false);\r\n\r\n$item_order = $db->GenID(cms_db_prefix().\"module_vipmember_items_seq\");\r\n$itemid = $db->GenID(cms_db_prefix().\"module_vipmember\");\r\n$datet = str_replace(\"\'\",\"\",$db->DBTimeStamp(time()));\r\n$query = \"INSERT INTO \".cms_db_prefix().\"module_vipmember_items (`phone`, `sex`, `address`, `birthday`, `id`, `name`, `alias`, `item_order`, `active`, `isdefault`, `date_modified`) VALUES (\'$phone\', \'$sex\', \'$address\', \'$birthday\', \'$itemid\', \'$fullname\', \'$alias\', \'$item_order\', \'1\', \'0\', \'$datet\')\";\r\n//echo $query;\r\n$db->Execute($query);', '', '2015-09-15 14:47:28', '2015-09-15 23:00:33');
INSERT INTO `cms_userplugins` VALUES (16, 'do_reservations', '$gCms = cmsms();\r\n$hm = $gCms->GetHierarchyManager();\r\n$db = $gCms->GetDb();\r\n$res_name= isset($params[\'res_name\']) && !empty($params[\'res_name\'])?$params[\'res_name\']:\'\';\r\n$res_email= isset($params[\'res_email\']) && !empty($params[\'res_email\'])?$params[\'res_email\']:\'\';\r\n$res_phone= isset($params[\'res_phone\']) && !empty($params[\'res_phone\'])?$params[\'res_phone\']:\'\';\r\n$res_amount= isset($params[\'res_amount\']) && !empty($params[\'res_amount\'])?$params[\'res_amount\']:\'\';\r\n$res_date= isset($params[\'res_date\']) && !empty($params[\'res_date\'])?$params[\'res_date\']:\'\';\r\n$res_time= isset($params[\'res_time\']) && !empty($params[\'res_time\'])?$params[\'res_time\']:\'\';\r\n$res_message= isset($params[\'res_message\']) && !empty($params[\'res_message\'])?$params[\'res_message\']:\'\';\r\n$email_notification = \'Quý khách đã đặt với thông tin như sau:<br/><br/>- Số người: \' . $res_amount. \'<br/>\'\r\n. \'- Ngày: \' . $res_date. \'<br/>\'\r\n. \'- Lúc: \' . $res_time. \'<br/>\'\r\n            . \'- Thông tin thêm: \' . $res_message. \'<br/><br/>Khoa Ngan sẽ liên hệ với bạn trong thời gian sớm nhất.\';\r\n    $tmpl = <<<EOT\r\n        $email_notification\r\nEOT;\r\n\r\n$cmsmailer = cms_utils::get_module(\'CMSMailer\');\r\n    $cmsmailer->AddAddress($res_email, $res_name);\r\n    $cmsmailer->SetFromName(\'Khoa Ngan\');\r\n    $cmsmailer->SetSubject(\'Cảm ơn Quý khách đã đặt bàn tại nhà hàng Khoa Ngan!\');\r\n    $cmsmailer->SetBody($tmpl);\r\n    $cmsmailer->IsHTML(true);\r\n    $cmsmailer->Send();\r\n    $cmsmailer->ClearAllRecipients();\r\n    $email_admin_notification = \'Thông tin khách hàng đặt bàn tại nhà hàng Khoa Ngan.<br/><br/>\'\r\n            . \'- Họ và tên: \' . $res_name . \'<br/>\'\r\n            . \'- Điện thoại: \' . $res_phone . \'<br/>\'\r\n. \'- Email: \' . $res_email. \'<br/>\'\r\n. \'- Số người: \' . $res_amount. \'<br/>\'\r\n. \'- Ngày: \' . $res_date. \'<br/>\'\r\n. \'- Lúc: \' . $res_time. \'<br/>\'\r\n            . \'- Thông tin thêm: \' . $res_message. \'<br/>\';\r\n    $tmplCC = <<<EOT\r\n        $email_admin_notification\r\nEOT;\r\n    $cmsmailer->AddAddress(\'maixuanquanghbt@gmail.com\', \'Administrator\');\r\n    $cmsmailer->AddBCC(\'quynhchihtv@gmail.com\', \'Administrator\');\r\n    $cmsmailer->SetFromName(\'Khoa Ngan - 77 Hai Bà Trưng\');\r\n    $cmsmailer->SetSubject(\'.:: Thông tin Đặt Bàn ::.\');\r\n    $cmsmailer->SetBody($tmplCC);\r\n    $cmsmailer->IsHTML(true);\r\n    $cmsmailer->Send();\r\n\r\necho \'<div class=\"grid-100 tablet-grid-100 mobile-grid-100\">\r\n<div class=\"reservation-confirmed\">\r\n<i class=\"fa fa-thumbs-up\"></i>\r\n<h4 class=\"reservation-confirmed-title\">Cảm ơn Quý khách!</h4>\r\n<p class=\"reservation-confirmed-text\">Khoa Ngan đã nhận được thông tin đặt bàn của Quý khách lúc <em>\'.$res_time.\'</em> ngày <em>\'.$res_date.\'</em>. Khoa Ngan sẽ liên lạc với Quý khách sớm nhất!</p>\r\n</div>\r\n</div>\';', '', '2019-04-24 15:35:08', '2019-04-24 16:21:30');

-- ----------------------------
-- Table structure for cms_userplugins_seq
-- ----------------------------
DROP TABLE IF EXISTS `cms_userplugins_seq`;
CREATE TABLE `cms_userplugins_seq`  (
  `id` int(11) NOT NULL
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = FIXED;

-- ----------------------------
-- Records of cms_userplugins_seq
-- ----------------------------
INSERT INTO `cms_userplugins_seq` VALUES (16);

-- ----------------------------
-- Table structure for cms_userprefs
-- ----------------------------
DROP TABLE IF EXISTS `cms_userprefs`;
CREATE TABLE `cms_userprefs`  (
  `user_id` int(11) NOT NULL,
  `preference` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `value` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `type` varchar(25) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of cms_userprefs
-- ----------------------------
INSERT INTO `cms_userprefs` VALUES (1, 'use_wysiwyg', '1', NULL);
INSERT INTO `cms_userprefs` VALUES (1, 'default_cms_language', 'en_US', NULL);
INSERT INTO `cms_userprefs` VALUES (1, 'date_format_string', '%x %X', NULL);
INSERT INTO `cms_userprefs` VALUES (1, 'admintheme', 'OneEleven', NULL);
INSERT INTO `cms_userprefs` VALUES (1, 'bookmarks', '0', NULL);
INSERT INTO `cms_userprefs` VALUES (1, 'recent', 'on', NULL);
INSERT INTO `cms_userprefs` VALUES (1, 'indent', '1', NULL);
INSERT INTO `cms_userprefs` VALUES (1, 'ajax', '0', NULL);
INSERT INTO `cms_userprefs` VALUES (1, 'paging', '0', NULL);
INSERT INTO `cms_userprefs` VALUES (1, 'hide_help_links', '1', NULL);
INSERT INTO `cms_userprefs` VALUES (1, 'wysiwyg', 'TinyMCE', NULL);
INSERT INTO `cms_userprefs` VALUES (1, 'gcb_wysiwyg', '1', NULL);
INSERT INTO `cms_userprefs` VALUES (1, 'syntaxhighlighter', '-1', NULL);
INSERT INTO `cms_userprefs` VALUES (1, 'enablenotifications', '0', NULL);
INSERT INTO `cms_userprefs` VALUES (1, 'default_parent', '-1', NULL);
INSERT INTO `cms_userprefs` VALUES (1, 'homepage', '', NULL);
INSERT INTO `cms_userprefs` VALUES (1, 'listtemplates_pagelimit', '20', NULL);
INSERT INTO `cms_userprefs` VALUES (1, 'liststylesheets_pagelimit', '20', NULL);
INSERT INTO `cms_userprefs` VALUES (1, 'listgcbs_pagelimit', '20', NULL);
INSERT INTO `cms_userprefs` VALUES (1, 'ignoredmodules', '', NULL);
INSERT INTO `cms_userprefs` VALUES (1, 'filemanager_cwd', '', NULL);
INSERT INTO `cms_userprefs` VALUES (1, 'collapse', '0=1.68=1.70=1.', NULL);
INSERT INTO `cms_userprefs` VALUES (2, 'wysiwyg', '-1', NULL);
INSERT INTO `cms_userprefs` VALUES (2, 'default_cms_language', 'vi_VN', NULL);
INSERT INTO `cms_userprefs` VALUES (2, 'admintheme', 'OneEleven', NULL);
INSERT INTO `cms_userprefs` VALUES (2, 'bookmarks', '0', NULL);
INSERT INTO `cms_userprefs` VALUES (2, 'recent', 'on', NULL);
INSERT INTO `cms_userprefs` VALUES (1, 'changegroupassign_group', '2', NULL);
INSERT INTO `cms_userprefs` VALUES (2, 'gcb_wysiwyg', '1', NULL);
INSERT INTO `cms_userprefs` VALUES (2, 'syntaxhighlighter', '-1', NULL);
INSERT INTO `cms_userprefs` VALUES (2, 'hide_help_links', '0', NULL);
INSERT INTO `cms_userprefs` VALUES (2, 'indent', '1', NULL);
INSERT INTO `cms_userprefs` VALUES (2, 'enablenotifications', '1', NULL);
INSERT INTO `cms_userprefs` VALUES (2, 'paging', '0', NULL);
INSERT INTO `cms_userprefs` VALUES (2, 'date_format_string', '%x %X', NULL);
INSERT INTO `cms_userprefs` VALUES (2, 'default_parent', '', NULL);
INSERT INTO `cms_userprefs` VALUES (2, 'homepage', '', NULL);
INSERT INTO `cms_userprefs` VALUES (2, 'listtemplates_pagelimit', '20', NULL);
INSERT INTO `cms_userprefs` VALUES (2, 'liststylesheets_pagelimit', '20', NULL);
INSERT INTO `cms_userprefs` VALUES (2, 'listgcbs_pagelimit', '20', NULL);
INSERT INTO `cms_userprefs` VALUES (2, 'ignoredmodules', '', NULL);

-- ----------------------------
-- Table structure for cms_users
-- ----------------------------
DROP TABLE IF EXISTS `cms_users`;
CREATE TABLE `cms_users`  (
  `user_id` int(11) NOT NULL,
  `username` varchar(25) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `password` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `admin_access` tinyint(4) NULL DEFAULT NULL,
  `first_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `last_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `email` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `active` tinyint(4) NULL DEFAULT NULL,
  `create_date` datetime NULL DEFAULT NULL,
  `modified_date` datetime NULL DEFAULT NULL
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of cms_users
-- ----------------------------
INSERT INTO `cms_users` VALUES (1, 'master', 'bc21b718a26e76821bc0ade503ace1b1', 1, 'Thanh', 'Le', 'songviytuong@gmail.com', 1, '2006-07-25 21:22:33', '2019-04-19 18:16:47');
INSERT INTO `cms_users` VALUES (2, 'khoangan', '67a7ab6b5d2f0c95623fc2abbbf92e26', 1, 'Khoa', 'Ngan', '', 1, '2015-09-15 18:02:27', '2020-11-24 16:12:21');

-- ----------------------------
-- Table structure for cms_users_seq
-- ----------------------------
DROP TABLE IF EXISTS `cms_users_seq`;
CREATE TABLE `cms_users_seq`  (
  `id` int(11) NOT NULL
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = FIXED;

-- ----------------------------
-- Records of cms_users_seq
-- ----------------------------
INSERT INTO `cms_users_seq` VALUES (2);

-- ----------------------------
-- Table structure for cms_version
-- ----------------------------
DROP TABLE IF EXISTS `cms_version`;
CREATE TABLE `cms_version`  (
  `version` int(11) NULL DEFAULT NULL
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = FIXED;

-- ----------------------------
-- Records of cms_version
-- ----------------------------
INSERT INTO `cms_version` VALUES (37);

SET FOREIGN_KEY_CHECKS = 1;
