{strip}
{capture}{content default='' wysiwyg='false'}{/capture}
{get_root_page_alias assign='page_lang'}
{/strip}
<!DOCTYPE html>
<html lang="{$page_lang}">
    <head>
        {metadata}
        {cms_stylesheet}
        <!-- Latest compiled and minified JavaScript -->
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>
        {literal}
            <script type="text/javascript">
                $(document).ready(function () {
                    $(".home").trigger('click');
                    $('#myModal').modal('show');
                });

                $(function () {
                    $('a[href*="#"]:not([href="#"])').click(function () {
                        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
                            var target = $(this.hash);
                            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
                            if (target.length) {
                                $('html, body').animate({
                                    scrollTop: target.offset().top
                                }, 1000);
                                return false;
                            }
                        }
                    });
                });
            </script>
        {/literal}
        {$CustomGS.Facebook_Conversion_Code}
    </head>

    <body id="hungry-home" class="home">

        <div class="modal" id="myModal" tabindex="-1" style="z-index:99999" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body">
                        <a href="{cms_selflink href=vip}"><img src="event/vip.jpg" alt="VIP" style="width:100%;" /></a>
                    </div>
                </div>
            </div>
        </div>

        <div id="hungry-preloader-container">
            <div class="hungry-preloader">
                <span class="bubble-01"></span>
                <span class="bubble-02"></span>
                <span class="bubble-03"></span>
            </div>
        </div>

        <header id="single-page-header">
            <div class="site-navbar">
                <div class="grid-container">
                    <div class="grid-20 tablet-grid-50 mobile-grid-50">
                        <a href="{root_url}">
                            <img class="site-logo" src="images/assets/logo.png" alt="{sitename}-Logo" />
                        </a>
                    </div>

                    <div class="nav-container grid-80 tablet-grid-50 mobile-grid-50">
                        <div class="mobile-nav">
                            <i class="fa fa-bars"></i>
                        </div>

                        <nav class="main-navigation" role="navigation">
                            <div class="mobile-header hide-on-desktop">
                                <h2>{sitename}</h2>
                                <div class="mobile-close">
                                    <i class="fa fa-times"></i>
                                </div>
                            </div>
                            {menu template="main_menu" childrenof="vi"}
                        </nav>
                    </div>
                </div>
            </div>

            <div class="single-page-header-content">
                <!--Slider Images -->
                <div class="cycle-slideshow" data-cycle-swipe="true" data-cycle-swipe-fx="fade" data-cycle-fx="fade" data-cycle-speed="1200" data-cycle-timeout="16000">
                    <!-- Images -->
                    {ListIt2Slider}
                    <!-- Prev/Next Buttons -->
                    <div class="cycle-prev"><i class="fa fa-chevron-left"></i></div>
                    <div class="cycle-next"><i class="fa fa-chevron-right"></i></div>
                </div>
                <!-- END Slider Images -->

                <!-- START Slider Texts -->
                <div class="single-page-header-text">
                    <!-- Pre-slogan -->
                    <div class="tilt-left">
                        <h3 class="header-text-pre-slogan">{sitename}<em>&hellip;</em></h3>
                    </div>
                    <!-- Slogan Rotator -->
                    <div class="tlt">
                        {ListIt2HeaderTexts}
                    </div>
                    <!-- Divider -->
                    <div class="header-text-divider"></div>
                </div>
                <!-- END Slider Texts -->
            </div>

            <!-- START Social Icons -->
            <div class="single-page-social-icons">
                {ListIt2Social}
            </div>
            <!-- END Social Icons -->
        </header>

        <!-- START Main Content -->
        <main class="site-content" role="main">
            <!-- START Section - About Us -->
            <section id="hungry-about-us" class="section-container aboutthis">
                <!-- START Section Heading -->
                <div class="wow fadeInDown" data-wow-duration="2s" data-wow-offset="250">
                    <header class="section-heading">
                        <h2 class="section-heading-title">About Us</h2>
                        <div class="section-heading-subtitle-container tilt-left">
                            <h4 class="section-heading-subtitle">Bạn biết gì về nhà hàng Khoa Ngan?</h4>
                        </div>
                    </header>
                </div>
                <!-- END Section Heading -->
                <div class="grid-container">
                    {ListIt2AboutUs orderby="rand" pagelimit="1"}
                </div>
            </section>
            <!-- END Section - About Us -->

            <section id="hungry-testimonials" class="section-container parallax">
                <div class="grid-container">
                    <div class="wow fadeIn" data-wow-duration="2s" data-wow-offset="250">
                        <h1 class="section-heading-alt-title">{MleCMS name="snippet_customer-say"}<span>&hellip;</span></h1>
                    </div>
                    {ListIt2Testimonials}
                </div>
            </section>

            <!-- START Section - Menu -->
            <section id="hungry-menu" class="section-container">
                <!-- START Section Heading -->
                <div class="wow fadeInDown" data-wow-duration="2s" data-wow-offset="250">
                    <header class="section-heading">
                        <h2 class="section-heading-title">Our Menu</h2>
                        <div class="section-heading-subtitle-container tilt-left">
                            <h4 class="section-heading-subtitle">Tươi nhất, ngon nhất...</h4>
                        </div>
                    </header>
                </div>
                <!-- END Section Heading -->

                <div class="grid-container">
                    {ListIt2Menu action="category"}
                </div>
            </section>
            <!-- END Section - Menu -->

            <!-- START Section - Slogan 01 -->
            <section id="hungry-slogan-01" class="section-container parallax">
                <div class="grid-container">
                    <div class="grid-100 tablet-grid-100 mobile-grid-100 no-margin">
                        <div class="wow zoomIn" data-wow-duration="2s" data-wow-offset="250">
                            <div class="hungry-slogan">
                                <h2 class="hungry-slogan-text">“Ngan Vịt của nhà hàng Khoa Ngan cũng có đầy đủ <em>hộ chiếu, chứng minh thư</em> như ai”<br />
                                    – như lời bà chủ vui tính của nhà hàng đã từng nói!</h2>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- END Section - Slogan 01 -->

            <!-- START Section - Staff -->
            <!--section id="hungry-staff" class="section-container">
                    <div class="wow fadeInDown" data-wow-duration="2s" data-wow-offset="250">
                            <header class="section-heading">
                                    <h2 class="section-heading-title">Our Staff</h2>
                                    <div class="section-heading-subtitle-container tilt-left">
                                            <h4 class="section-heading-subtitle">The Friendliest People</h4>
                                    </div>
                            </header>
                    </div>
                    <div class="grid-container">
                    ListIt2Staff
                    </div>
            </section-->
            <!-- END Section - Staff -->

            <!-- START Section - Gallery -->
            <section id="hungry-gallery" class="section-container">
                <!-- START Section Heading -->
                <div class="wow fadeInDown" data-wow-duration="2s" data-wow-offset="250">
                    <header class="section-heading">
                        <h2 class="section-heading-title">The Gallery</h2>
                        <div class="section-heading-subtitle-container tilt-left">
                            <h4 class="section-heading-subtitle">{sitename} 2017!</h4>
                        </div>
                    </header>
                </div>
                <!-- END Section Heading -->
                <!-- START Main Gallery -->
                <div class="grid-container">
                    <div class="grid-100 tablet-grid-100 mobile-grid-100">
                        <div class="hungry-gallery">
                            {ListIt2Gallery category="Galleries"}
                        </div>
                    </div>
                </div>
                <!-- END Main Gallery -->
            </section>
            <!-- END Section - Gallery -->
            <!-- START Section - Slogan 02 -->
            <a href="#" class="clickable-slogan">
                <section id="hungry-slogan-02" class="section-container parallax">
                    <div class="grid-container">

                        <div class="grid-100 tablet-grid-100 mobile-grid-100 no-margin">
                            <div class="wow zoomIn" data-wow-duration="2s" data-wow-offset="250">
                                <div class="hungry-slogan">
                                    <h2 class="hungry-slogan-text">Chính bạn là Thượng Đế...<br />
                                        <span>Call: <em>{global_content name='hotline'}</em> ngay!</span></h2>
                                </div>
                            </div>
                        </div>

                    </div>
                </section>
            </a>
            <!-- END Section - Slogan 02 -->
            <!-- START Section - Customer -->
            <section id="hungry-customer" class="section-container">
                <!-- START Section Heading -->
                <div class="wow fadeInDown" data-wow-duration="2s" data-wow-offset="250">
                    <header class="section-heading">
                        <h2 class="section-heading-title">The Customer</h2>
                        <div class="section-heading-subtitle-container tilt-left">
                            <h4 class="section-heading-subtitle">{sitename} 2017!</h4>
                        </div>
                    </header>
                </div>
                <!-- END Section Heading -->

                <!-- START Main Gallery -->
                <div class="grid-container">
                    <div class="grid-100 tablet-grid-100 mobile-grid-100">
                        <div class="hungry-gallery">
                            {ListIt2Gallery category="Customers"}
                        </div>
                    </div>
                </div>
                <!-- END Main Gallery -->
            </section>
            <!-- END Section - Gallery -->

            <!-- START Section - Slogan 02 -->
            <a href="#" class="clickable-slogan">
                <section id="hungry-slogan-02" class="section-container parallax">
                    <div class="grid-container">

                        <div class="grid-100 tablet-grid-100 mobile-grid-100 no-margin">
                            <div class="wow zoomIn" data-wow-duration="2s" data-wow-offset="250">
                                <div class="hungry-slogan">
                                    <h2 class="hungry-slogan-text">Hãy tới và thưởng thức, Khoa Ngan <em>{global_content name='add'}</em><br />
                                        <span>Hotline: <em>{global_content name='hotline'}</em></span></h2>
                                </div>
                            </div>
                        </div>

                    </div>
                </section>
            </a>
            <!-- END Section - Slogan 02 -->

            <!-- START Section - Blog -->
            <section id="hungry-promotion" class="section-container parallax">
                <!-- START Section Heading -->
                <div class="wow fadeInDown" data-wow-duration="2s" data-wow-offset="250">
                    <header class="section-heading">
                        <h2 class="section-heading-title">Khoa Ngan Promotion</h2>
                        <div class="section-heading-subtitle-container tilt-left">
                            <h4 class="section-heading-subtitle">Bắt đầu từ 01/06/2019!</h4>
                        </div>
                    </header>
                </div>
                <!-- END Section Heading -->
                <div class="grid-container">
                    <div class="hungry-blog-container">
                        {ListIt2Blog}
                        <br class="clear" />
                    </div>
                    <!-- "View Blog" Button -->
                    <div class="wow fadeInUp" data-wow-duration="2s" data-wow-offset="250">
                        <div class="grid-100 tablet-grid-100 mobile-grid-100 aligncenter">
                            <a class="hungry-button dark aligncenter" href="{cms_selflink href=vip}">Đăng ký để nhận nhiều Ưu đãi</a>
                        </div>
                    </div>
                </div>
            </section>
            <!-- END Section - Blog -->

            <!-- START Section - Reservations -->
            <section id="hungry-reservations" class="section-container">
                <div class="wow fadeInDown" data-wow-duration="2s" data-wow-offset="250">
                    <header class="section-heading">
                        <h2 class="section-heading-title">Reservations</h2>
                        <div class="section-heading-subtitle-container tilt-left">
                            <h4 class="section-heading-subtitle">Liên hệ đặt bàn!</h4>
                        </div>
                    </header>
                </div>
                <div class="grid-container">
                    <div class="prefix-10 grid-80 suffix-10 tablet-grid-100 mobile-grid-100">
                        <form name="frm" id="hungry-reservation-form" action="" method="post">
                            <fieldset>
                                <legend class="form-title">Booking Form<span>Vui lòng nhập thông tin để Khoa Ngan liên hệ với bạn<em>*</em></span></legend>
                                <div class="grid-50 tablet-grid-50 mobile-grid-100">
                                    <p>
                                        <label for="res_name">Họ và tên<span>*</span></label>
                                        <input type="text" name="res_name" id="res_name" value="" required>
                                    </p>
                                    <p>
                                        <label for="res_email">E-mail<span>*</span></label>
                                        <input type="email" name="res_email" id="res_email" value="" required>
                                    </p>
                                    <p>
                                        <label for="res_phone">Điện thoại<span>*</span></label>
                                        <input type="tel" name="res_phone" id="res_phone" value="" required>
                                    </p>
                                    <p>
                                        <label for="res_amount">Có bao nhiêu người cùng bạn thưởng thức?<span>*</span></label>
                                        <select name="res_amount" id="res_amount" required>
                                            <option value="" selected>Vui lòng chọn...</option>
                                            <option value="1">1</option>
                                            <option value="2">2</option>
                                            <option value="3">3</option>
                                            <option value="4">4</option>
                                            <option value="5">5</option>
                                            <option value="6">6</option>
                                            <option value="7">7</option>
                                            <option value="8">8</option>
                                            <option value="9">9</option>
                                            <option value="More than 9">Hơn 9 người</option>
                                        </select>
                                    <div class="multiselect">
                                        <div class="selectBox" onclick="showCheckboxes()">
                                            <select disabled>
                                                <option>Bạn muốn thưởng thức?</option>
                                            </select>
                                            <div class="overSelect"></div>
                                        </div>
                                        <div id="checkboxes">
                                            {*ListIt2Menu category="food" template_summary="reservations"*}
                                            <!--<label for="one">
                                              <input type="checkbox" name="menu[]" id="one" />First checkbox <span>120K</span></label>
                                            <label for="two">
                                              <input type="checkbox" name="menu[]" id="two" />Second checkbox</label>
                                            <label for="three">
                                              <input type="checkbox" name="menu[]" id="three" />Third checkbox</label>-->
                                        </div>
                                    </div>
                                    </p>
                                </div>

                                <div class="grid-50 tablet-grid-50 mobile-grid-100">
                                    <p>
                                        <label for="res_date">Vào ngày<span>*</span></label>
                                        <input type="date" name="res_date" id="res_date" value="" required>
                                    </p>
                                    <p>
                                        <label for="res_time">Thời gian<span>*</span></label>
                                        <select name="res_time" id="res_time" required>
                                            <option value="" selected>Vui lòng chọn...</option>
                                            <optgroup label="Afternoon Reservations">
                                                <option value="7-9am">7-9pm</option>
                                                <option value="9-12am">9:12am</option>
                                                <option value="1pm">1pm</option>
                                                <option value="1:30pm">1:30pm</option>
                                            </optgroup>
                                            <optgroup label="Evening Reservations">
                                                <option value="6pm">6pm</option>
                                                <option value="6:30pm">6:30pm</option>
                                                <option value="7pm">7pm</option>
                                                <option value="7:30pm">7:30pm</option>
                                                <option value="8pm">8pm</option>
                                                <option value="8:30pm">8:30pm</option>
                                                <option value="9pm">9pm</option>
                                            </optgroup>
                                        </select>
                                    </p>
                                    <label for="res_email">Thông tin thêm</label>
                                    <textarea name="res_message" id="res_message" cols="8" rows="8"></textarea>
                                </div>

                                <div class="grid-50 tablet-grid-50 mobile-grid-100">
                                    <input type="submit" name="res-submit" id="res-submit" value="Đặt bàn ngay!" />
                                    <div>
                                        <div id="hungry-reservation-form-outcome"></div>
                                    </div>
                                </div>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </section>
            <!-- END Section - Reservations -->
            <section id="hungry-contact">
                <!-- START Section Heading -->
                <div class="wow fadeInDown" data-wow-duration="2s" data-wow-offset="250">
                    <header class="section-heading">
                        <h2 class="section-heading-title">Contact Us</h2>
                        <div class="section-heading-subtitle-container tilt-left">
                            <h4 class="section-heading-subtitle">Khoa Ngan - 77 Hai Bà Trưng</h4>
                        </div>
                    </header>
                </div>
                <!-- END Section Heading -->
                <!-- START Site Footer -->
                <footer id="site-footer">
                    <!-- START Widget Area -->
                    <div class="widget-area grid-container">
                        <!-- START Widget Column 01 -->
                        <div class="widget-column widget-column-01 grid-33 tablet-grid-33 mobile-grid-100">
                            <!-- Contact Details Widget -->
                            <aside class="widget widget-hungry-contact-details">
                                <h3 class="widget-title">Contact Us</h3>
                                <p>(Liên hệ)</p>
                                <div class="contact-details">
                                    <div class="contact-phone">
                                        <i class="fa fa-phone-square"></i>
                                        <a class="phone-number-link" href="tel:+84944278866">0944.27.8866</a><a class="phone-number-link" href="tel:+842439422206">024.39422206</a>
                                    </div>
                                    <div class="contact-email">
                                        <i class="fa fa-envelope-square"></i>
                                        <a class="email-link" href="mailto:{global_content name='email'}">{global_content name='email'}</a>
                                    </div>
                                    <div class="contact-address">
                                        <i class="fa fa-caret-square-o-down"></i>
                                        <address>{global_content name='add'}</address>
                                    </div>
                                </div>
                            </aside>
                        </div>
                        <!-- END Widget Column 01 -->

                        <!-- START Widget Column 02 -->
                        <div class="widget-column widget-column-02 grid-33 tablet-grid-33 mobile-grid-100">
                            <aside class="widget widget-hungry-latest-recipes">
                                <h3 class="widget-title">Facebook</h3>
                                <p>(Liên kết mạng xã hội)</p>
                                <iframe src="http://www.facebook.com/plugins/likebox.php?href=https://www.facebook.com/khoanganxuanay?ref=hl&amp;width=300&amp;colorscheme=light&amp;show_faces=true&amp;connections=12&amp;stream=false&amp;header=false&amp;height=150" scrolling="no" frameborder="0" scrolling="no" style="overflow: hidden; height: 150px; width: 300px;background:#fff;"></iframe>
                                <ul class="latest-recipes">
                                    <!--li class="recipe">
                                                    <a href="#"><img class="recipe-thumbnail" src="images/demo/widget-recipe-thumbnails/thumbnail-01.jpg" alt="Recipe Thumbnail Image" /></a>
                                                    <h6 class="recipe-title"><a href="#">Angus Steak Burger</a></h6>
                                                    <p class="recipe-description">Aenean commodo ligula eget.</p>
                                            </li-->
                                </ul>
                            </aside>
                        </div>
                        <div class="widget-column widget-column-03 grid-33 tablet-grid-33 mobile-grid-100">
                            <aside class="widget widget-hungry-opening-times">
                                <h3 class="widget-title">Opening Times</h3>
                                <p>(Giờ mở cửa)</p>
                                <ul class="opening-times">
                                    <li>Phục vụ tất cả các ngày trong tuần<span>7am - 11pm</span></li>
                                </ul>
                            </aside>
                        </div>
                    </div>
                    <div id="bottom-footer">
                        <div class="grid-container">
                            <!-- Footer Logo -->
                            <div class="footer-logo grid-50 tablet-grid-100 mobile-grid-100">
                                <a href="{root_url}"><img class="footer-logo-image" src="images/assets/footer-logo.png" alt="{sitename}" /></a>
                            </div>
                        </div>
                    </div>
                </footer>
            </section>

        </main>
        <!-- END Main Content -->
        <div id="btt">
            <i class="fa fa-angle-up"></i>
        </div>
        {literal}
            <style>
                .multiselect {

                }

                .selectBox {
                    position: relative;
                }

                .selectBox select {
                    width: 100%;
                    font-weight: bold;
                }

                .overSelect {
                    position: absolute;
                    left: 0;
                    right: 0;
                    top: 0;
                    bottom: 0;
                }

                #checkboxes {
                    display: none;

                }

                #checkboxes input {
                    margin:10px;
                    height:auto!important;
                    vertical-align: middle;
                }

                #checkboxes span {
                    float:right;
                    margin-right:20px;
                    text-align:right;
                }

                #checkboxes label {
                    display: block;
                }

                #checkboxes label:hover {
                    background-color: #1e90ff;
                }
            </style>{/literal}
            <script type="text/javascript" src="js/jquery-main.js"></script>
            <script type="text/javascript" src="js/jquery-custom.js"></script>
            <script>
                var expanded = false;
                function showCheckboxes() {
                    var checkboxes = document.getElementById("checkboxes");
                    if (!expanded) {
                        checkboxes.style.display = "block";
                        expanded = true;
                    } else {
                        checkboxes.style.display = "none";
                        expanded = false;
                    }
                }
            </script>
            <script src="https://uhchat.net/code.php?f=e2c65a"></script>
        </body>

    </html>